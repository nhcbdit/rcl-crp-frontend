package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaVsaPortCallMod extends RrcStandardMod{
    

    private String vsaPortCallId;       //    PK_VSA_PORT_CALL_ID 
    private String voyageHeaderId;      //    FK_VOYAGE_HEADER_ID
    private String bsaPortCallId;       //    FK_BSA_PORT_CALL_ID
    private String proformaRefNo;        //    FK_PROFORMA_REF_NO
    private String dnPort;              //    DN_PORT VARCHAR2
    private String portSequence;        //    FK_PORT_SEQUENCE_NO
    private String dnTerminal;          //    DN_TERMINAL
    private String startingVoyno;       //    FK_STARTING_VOYNO
    private String slotTeu;             //    SLOT_TEU
    private String slotTons;            //    SLOT_TONS
    private String slotReferPlugs;      //    SLOT_REEFER_PLUGS
    private String avgCocTeuWeight;     //    AVG_COC_TEU_WEIGHT
    private String avgSocTeuWeight;     //    AVG_SOC_TEU_WEIGHT
    private String minTeu;              //    MIN_TEU INTEGER
    private Timestamp recordAddDate;    // RECORD_ADD_DATE
    private Timestamp recordChangeDate; // RECORD_ADD_DATE
    private String portGroup;           // 
    
    private String direction;
    private String startDate;
    private String endDate;
    private String tolerant; // TOLERANT
    private String flag;              // INSERT,UPDATE,DELTE
    private String startingVessel;
    
     public BsaVsaPortCallMod() {
         this("0","","","","","","","","","","","","", "", RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0),"","","","","","","");
     } 
    
     public BsaVsaPortCallMod(String vsaPortCallId, String voyageHeaderId, String bsaPortCallId,
              String proformaRefNo, String dnPort, String portSequence, String dnTerminal, 
              String startingVoyno, String slotTeu, String slotTons, String slotReferPlugs, 
              String avgCocTeuWeight, String avgSocTeuWeight,String minTeu, String recordStatus, 
              String recordAddUser, Timestamp recordAddDate, String recordChangeUser, Timestamp recordChangeDate, String direction, String startDate, String endDate, String tolerant, String flag,String portGroup,String startingVessel)
          {
                  super(recordStatus, recordAddUser, recordChangeUser);
                  this.vsaPortCallId = vsaPortCallId;
                  this.voyageHeaderId = voyageHeaderId;
                  this.bsaPortCallId = bsaPortCallId;
                  this.proformaRefNo = proformaRefNo;
                  this.dnPort = dnPort;
                  this.portSequence = portSequence;
                  this.dnTerminal = dnTerminal;
                  this.startingVoyno = startingVoyno;
                  this.slotTeu = slotTeu;
                  this.slotTons = slotTons;
                  this.slotReferPlugs = slotReferPlugs;
                  this.avgCocTeuWeight = avgCocTeuWeight;
                  this.avgSocTeuWeight = avgSocTeuWeight;
                  this.minTeu = minTeu;
                  this.recordAddDate = recordAddDate;
                  this.recordChangeDate = recordChangeDate;
                  this.direction = direction;
                  this.startDate = startDate;
                  this.endDate = endDate;
                  this.tolerant = tolerant;
                  this.flag = flag;
                  this.portGroup = portGroup;
                  this.startingVessel = startingVessel;
          }
          
    public BsaVsaPortCallMod(BsaVsaPortCallMod mod) {
        this(mod.getVsaPortCallId(),
             mod.getVoyageHeaderId(),
             mod.getBsaPortCallId(),
             mod.getProformaRefNo(),
             mod.getDnPort(),
             mod.getPortSequence(),
             mod.getDnTerminal(),
             mod.getStartingVoyno(),
             mod.getSlotTeu(),
             mod.getSlotTons(),
             mod.getSlotReferPlugs(),
             mod.getAvgCocTeuWeight(),
             mod.getAvgSocTeuWeight(),
             mod.getMinTeu(),
             mod.getRecordStatus(),
             mod.getRecordAddUser(),
             mod.getRecordAddDate(),
             mod.getRecordChangeUser(),
             mod.getRecordChangeDate(),
             mod.getDirection(),
             mod.getStartDate(),
             mod.getEndDate(),
             mod.getTolerant(),
             mod.getFlag(),
             mod.getPortGroup(),
             mod.getStartingVessel());
    } 
    
    public boolean equals(Object object) {
        if (object instanceof BsaVsaPortCallMod) {
            BsaVsaPortCallMod mod = (BsaVsaPortCallMod)object;
            if (!this.vsaPortCallId.equals(mod.getVsaPortCallId())){
                return false;
            }else if (!this.voyageHeaderId.equals(mod.getVoyageHeaderId())){
                return false;
            }else if (!this.bsaPortCallId.equals(mod.getBsaPortCallId())){
                return false;
            }else if (!this.proformaRefNo.equals(mod.getProformaRefNo())){
                return false;
            }else if (!this.dnPort.equals(mod.getDnPort())){
                return false;
            }else if (!this.portSequence.equals(mod.getPortSequence())){
                return false;
            }else if (!this.dnTerminal.equals(mod.getDnTerminal())){
                return false;
            }else if (!this.startingVoyno.equals(mod.getStartingVoyno())){
                return false;
            }else if (!this.slotTeu.equals(mod.getSlotTeu())){
                return false;
            }else if (!this.slotTons.equals(mod.getSlotTons())){
                return false;
            }else if (!this.slotReferPlugs.equals(mod.getSlotReferPlugs())){
                return false;
            }else if (!this.avgCocTeuWeight.equals(mod.getAvgCocTeuWeight())){
                return false;
            }else if (!this.avgSocTeuWeight.equals(mod.getAvgSocTeuWeight())){
                return false;
            }else if (!this.minTeu.equals(mod.getMinTeu())){
                return false;
            }else if (!this.recordAddDate.equals(mod.getRecordAddDate())) {
                return false;
            }else if (!this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                return false;
            }else if (!this.direction.equals(mod.getRecordChangeDate())) {
                return false;
            }else if (!this.startDate.equals(mod.getStartDate())) {
                return false;
            }else if (!this.endDate.equals(mod.endDate)) {
                return false;
            }else if (!this.tolerant.equals(mod.getTolerant())) {
                return false;
            }else if (!this.flag.equals(mod.getFlag())) {
                return false;
            }else if (!this.portGroup.equals(mod.getPortGroup())) {
                return false;
            }else if (!this.startingVessel.equals(mod.getStartingVessel())) {
                return false;
            }  
        } else {
            return false;
        }
        return true;
    }

    public void setVsaPortCallId(String vsaPortCallId) {
        this.vsaPortCallId = vsaPortCallId;
    }

    public String getVsaPortCallId() {
        return vsaPortCallId;
    }

    public void setVoyageHeaderId(String voyageHeaderId) {
        this.voyageHeaderId = voyageHeaderId;
    }

    public String getVoyageHeaderId() {
        return voyageHeaderId;
    }

    public void setBsaPortCallId(String bsaPortCallId) {
        this.bsaPortCallId = bsaPortCallId;
    }

    public String getBsaPortCallId() {
        return bsaPortCallId;
    }

    public void setProformaRefNo(String proformaRefNo) {
        this.proformaRefNo = proformaRefNo;
    }

    public String getProformaRefNo() {
        return proformaRefNo;
    }

    public void setDnPort(String dnPort) {
        this.dnPort = dnPort;
    }

    public String getDnPort() {
        return dnPort;
    }

    public void setPortSequence(String portSequence) {
        this.portSequence = portSequence;
    }

    public String getPortSequence() {
        return portSequence;
    }

    public void setDnTerminal(String dnTerminal) {
        this.dnTerminal = dnTerminal;
    }

    public String getDnTerminal() {
        return dnTerminal;
    }

    public void setStartingVoyno(String startingVoyno) {
        this.startingVoyno = startingVoyno;
    }

    public String getStartingVoyno() {
        return startingVoyno;
    }

    public void setSlotTeu(String slotTeu) {
        this.slotTeu = slotTeu;
    }

    public String getSlotTeu() {
        return slotTeu;
    }

    public void setSlotTons(String slotTons) {
        this.slotTons = slotTons;
    }

    public String getSlotTons() {
        return slotTons;
    }

    public void setSlotReferPlugs(String slotReferPlugs) {
        this.slotReferPlugs = slotReferPlugs;
    }

    public String getSlotReferPlugs() {
        return slotReferPlugs;
    }

    public void setAvgCocTeuWeight(String avgCocTeuWeight) {
        this.avgCocTeuWeight = avgCocTeuWeight;
    }

    public String getAvgCocTeuWeight() {
        return avgCocTeuWeight;
    }

    public void setAvgSocTeuWeight(String avgSocTeuWeight) {
        this.avgSocTeuWeight = avgSocTeuWeight;
    }

    public String getAvgSocTeuWeight() {
        return avgSocTeuWeight;
    }

    public void setMinTeu(String minTeu) {
        this.minTeu = minTeu;
    }

    public String getMinTeu() {
        return minTeu;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setTolerant(String tolerant) {
        this.tolerant = tolerant;
    }

    public String getTolerant() {
        return tolerant;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setPortGroup(String portGroup) {
        this.portGroup = portGroup;
    }

    public String getPortGroup() {
        return portGroup;
    }

    public void setStartingVessel(String startingVessel) {
        this.startingVessel = startingVessel;
    }

    public String getStartingVessel() {
        return startingVessel;
    }
}
