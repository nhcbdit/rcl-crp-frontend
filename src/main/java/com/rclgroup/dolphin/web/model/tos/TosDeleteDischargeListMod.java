/*-----------------------------------------------------------------------------------------------------------  
TosDeleteDischargeListMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 24/02/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class TosDeleteDischargeListMod extends RrcStandardMod {
    
    private String vessel;
    private String voyage;
    private String pod;
    private String podTerminal;
    private String flagExecute;
    private String returnResult;
    private Timestamp recordChangeDate;
    
    public TosDeleteDischargeListMod() {
        super();
    }
    
    public TosDeleteDischargeListMod(String vessel, String voyage, String pod, String podTerminal, String flagExecute, String returnResult, Timestamp recordChangeDate) {
        this.vessel = vessel;
        this.voyage = voyage;
        this.pod = pod;
        this.podTerminal = podTerminal;
        this.flagExecute = flagExecute;
        this.returnResult = returnResult;
        this.recordChangeDate = recordChangeDate;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getFlagExecute() {
        return flagExecute;
    }

    public void setFlagExecute(String flagExecute) {
        this.flagExecute = flagExecute;
    }

    public String getReturnResult() {
        return returnResult;
    }

    public void setReturnResult(String returnResult) {
        this.returnResult = returnResult;
    }
    
    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}

