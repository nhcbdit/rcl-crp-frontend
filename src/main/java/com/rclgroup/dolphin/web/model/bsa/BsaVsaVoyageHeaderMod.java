/*-----------------------------------------------------------------------------------------------------------  
BsaVsaVoyageHeaderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 15/03/12
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaVsaVoyageHeaderMod extends RrcStandardMod{

    
     
    private String voyageHeaderId;      // PK_VOYAGE_HEADER_ID
    private String bsaServiceVariantId; // FK_BSA_SERVICE_VARIANT_ID
    private String bsaModelId;          // FK_BSA_MODEL_ID
    private String vssProformaId;       // FK_VSS_PROFORMA_ID
    private String vssVesselAssigmentId;// FK_VSS_VESSEL_ASSIGNMENT_ID
    private String serviceGroupCode;    // DN_SERVICE_GROUP_CODE
    private String service;             // FK_SERVICE
    private String vessel;              // FK_VESSEL
    private String voyage;              // FK_VOYAGE
    private String direction;           // FK_DIRECTION
    private String status;              // VOYAGE_STATUS
    private String effectiveDate;       // EFFECTIVE_DATE
    private String expiryDate;          // EXPIRY_DATE
    private Timestamp recordAddDate;    // RECORD_ADD_DATE
    private Timestamp recordChangeDate; // RECORD_CHANGE_DATE
    
    
    private String modelName;
    private String variant;
    private String vesselName;
    private String tolelant;
    
    private String minDate;
    private String maxDate;
    
     public BsaVsaVoyageHeaderMod() {
     //,"","","0.0","","","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0),"",""
         this("0","","","","","","","","","","","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0),"","","","","","");
     }
     
    public BsaVsaVoyageHeaderMod(String voyageHeaderId, String bsaServiceVariantId, String bsaModelId,
        String vssProformaId, String vssVesselAssigmentId, String serviceGroupCode, String service, 
        String vessel, String voyage, String direction, String status, 
        String effectiveDate, String expiryDate, String recordStatus, 
        String recordAddUser, Timestamp recordAddDate, String recordChangeUser, Timestamp recordChangeDate,String modelName, String variant, String vesselName,String tolelant
        , String minDate, String maxDate)
    {
            super(recordStatus, recordAddUser, recordChangeUser);
            this.voyageHeaderId = voyageHeaderId;
            this.bsaServiceVariantId = bsaServiceVariantId;
            this.bsaModelId = bsaModelId;
            this.vssProformaId = vssProformaId;
            this.vssVesselAssigmentId = vssVesselAssigmentId;
            this.serviceGroupCode = serviceGroupCode;
            this.service = service;
            this.vessel = vessel;
            this.voyage = voyage;
            this.direction = direction;
            this.status = status;
            this.effectiveDate = effectiveDate;
            this.expiryDate = expiryDate;    
            this.recordAddDate = recordAddDate;
            this.recordChangeDate = recordChangeDate;
            this.modelName = modelName;
            this.variant = variant;
            this.vesselName = vesselName;
            this.tolelant = tolelant;
            this.minDate = minDate;
            this.maxDate = maxDate;
    }
    
    public BsaVsaVoyageHeaderMod(BsaVsaVoyageHeaderMod mod) {
       this(mod.getVoyageHeaderId(),
            mod.getBsaServiceVariantId(),
            mod.getBsaModelId(),
            mod.getVssProformaId(),
            mod.getVssVesselAssigmentId(),
            mod.getServiceGroupCode(),
            mod.getService(),
            mod.getVessel(),
            mod.getVoyage(),
            mod.getDirection(),
            mod.getStatus(),
            mod.getEffectiveDate(),
            mod.getExpiryDate(),              
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate(),
            mod.getModelName(),
            mod.getVariant(),
            mod.getVesselName(),
            mod.getTolelant(),
            mod.getMinDate(),
            mod.getMaxDate());
    }
    
    
    public boolean equals(Object object) {
        if (object instanceof BsaBsaServiceVariantMod) {
            BsaVsaVoyageHeaderMod mod = (BsaVsaVoyageHeaderMod)object;
            if (!this.voyageHeaderId.equals(mod.getVoyageHeaderId())){
                return false;
            } else if(!this.bsaServiceVariantId.equals(mod.getBsaServiceVariantId())) {
                return false;
            } else if (!this.bsaModelId.equals(mod.getBsaModelId())) {
                return false;
            } else if(!this.vssProformaId.equals(mod.getVssProformaId())) {
                return false;
            } else if(!this.vssVesselAssigmentId.equals(mod.getVssVesselAssigmentId())) {
                return false;
            } else if (!this.service.equals(mod.getService())) {
                return false;
            } else if(!this.vessel.equals(mod.getVessel())) {
                return false;
            } else if(!this.voyage.equals(mod.getVoyage())) {
                return false;
            } else if(!this.direction.equals(mod.getDirection())) {
                return false;
            } else if (!this.status.equals(mod.getStatus())) {
                    return false;
            } else if (this.expiryDate.equals(mod.getExpiryDate())) {
                    return false;    
            } else if (!this.recordAddDate.equals(mod.getRecordAddDate())) {
                return false;
            } else if (!this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                return false;
            } else if (!this.modelName.equals(mod.getModelName())) {
                return false;
            } else if (!this.variant.equals(mod.getVariant())) {
                return false;
            } else if (!this.vesselName.equals(mod.getVesselName())) {
                return false;
            } else if (!this.tolelant.equals(mod.getTolelant())) {
                return false;
            } 
        } else {
            return false;
        }
        return true;
    }

    public void setVoyageHeaderId(String voyageHeaderId) {
        this.voyageHeaderId = voyageHeaderId;
    }

    public String getVoyageHeaderId() {
        return voyageHeaderId;
    }

    public void setBsaServiceVariantId(String bsaServiceVariantId) {
        this.bsaServiceVariantId = bsaServiceVariantId;
    }

    public String getBsaServiceVariantId() {
        return bsaServiceVariantId;
    }

    public void setBsaModelId(String bsaModelId) {
        this.bsaModelId = bsaModelId;
    }

    public String getBsaModelId() {
        return bsaModelId;
    }

    public void setVssProformaId(String vssProformaId) {
        this.vssProformaId = vssProformaId;
    }

    public String getVssProformaId() {
        return vssProformaId;
    }

    public void setVssVesselAssigmentId(String vssVesselAssigmentId) {
        this.vssVesselAssigmentId = vssVesselAssigmentId;
    }

    public String getVssVesselAssigmentId() {
        return vssVesselAssigmentId;
    }

    public void setServiceGroupCode(String serviceGroupCode) {
        this.serviceGroupCode = serviceGroupCode;
    }

    public String getServiceGroupCode() {
        return serviceGroupCode;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVariant() {
        return variant;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setTolelant(String tolelant) {
        this.tolelant = tolelant;
    }

    public String getTolelant() {
        return tolelant;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public String getMaxDate() {
        return maxDate;
    }
}
