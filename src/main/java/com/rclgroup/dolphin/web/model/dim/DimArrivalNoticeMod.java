/*-----------------------------------------------------------------------------------------------------------  
DimArrivalNoticeMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/12/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class DimArrivalNoticeMod extends RrcStandardMod {
    
    private String service;
    private String vessel;
    private String voyage;
    private String direction;
    private String pol;
    private String pot;
    private String pod;
    private String podTerminal;
    private String blId;
    private String blType;
    private String blCreateBy;
    private String blNo;
    private String hblNo;
    private String cocSoc;
    private String inStatus;
    private String outStatus;
    private String shipType;
    private String remarks;
    
    public DimArrivalNoticeMod() {
        service = "";
        vessel = "";
        voyage = "";
        direction = "";
        pol = "";
        pot = "";
        pod = "";
        podTerminal = "";
        blId = "";
        blType = "";
        blCreateBy = "";
        blNo = "";
        hblNo = "";
        cocSoc = "";
        inStatus = "";
        outStatus = "";
        shipType = "";
        remarks = "";
    }


    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPot() {
        return pot;
    }

    public void setPot(String pot) {
        this.pot = pot;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getBlId() {
        return blId;
    }

    public void setBlId(String blId) {
        this.blId = blId;
    }

    public String getBlType() {
        return blType;
    }

    public void setBlType(String blType) {
        this.blType = blType;
    }

    public String getBlCreateBy() {
        return blCreateBy;
    }

    public void setBlCreateBy(String blCreateBy) {
        this.blCreateBy = blCreateBy;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getHblNo() {
        return hblNo;
    }

    public void setHblNo(String hblNo) {
        this.hblNo = hblNo;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getInStatus() {
        return inStatus;
    }

    public void setInStatus(String inStatus) {
        this.inStatus = inStatus;
    }

    public String getOutStatus() {
        return outStatus;
    }

    public void setOutStatus(String outStatus) {
        this.outStatus = outStatus;
    }

    public String getShipType() {
        return shipType;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
