/*-----------------------------------------------------------------------------------------------------------  
FarShipperCodeMappingMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 15/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class FarShipperCodeMappingMod extends RrcStandardMod {
    
    private String pkShipperMapId;
    private String billToParty;
    private String shipperCode;
    private String shipperName;
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;
    
    public FarShipperCodeMappingMod() {
        super();
        pkShipperMapId = "";
        billToParty = "";
        shipperCode = "";
        shipperName = "";
        recordAddDate = null;
        recordChangeDate = null;
    }

    public String getPkShipperMapId() {
        return pkShipperMapId;
    }

    public void setPkShipperMapId(String pkShipperMapId) {
        this.pkShipperMapId = pkShipperMapId;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getShipperCode() {
        return shipperCode;
    }

    public void setShipperCode(String shipperCode) {
        this.shipperCode = shipperCode;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}
