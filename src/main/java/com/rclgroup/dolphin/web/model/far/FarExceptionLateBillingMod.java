/*-----------------------------------------------------------------------------------------------------------  
FarExceptionLateBillingMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2010
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 28/06/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.far;

public class FarExceptionLateBillingMod {
    private String collectFsc;
    private String pol;
    private String pod;
    private String eta;   
    private String etd;
    private String bl;
    private String invNo;
    private String instructBillDte;
    private String invPrintDte;
    private String instructBillDur;
    private String invPrintDur;   
    private String amt;
    private String userName;
    private String sessionId;

    public FarExceptionLateBillingMod() {
        collectFsc = "";
        pol = "";
        pod = "";
        eta = "";       
        etd="";
        bl="";
        invNo="";
        instructBillDte = "";
        invPrintDte = "";
        instructBillDur ="";
        invPrintDur = "";
        amt = "";
        userName  = "";
        sessionId  = "";
  
    }

    public void setCollectFsc(String collectFsc) {
        this.collectFsc = collectFsc;
    }

    public String getCollectFsc() {
        return collectFsc;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setEta(String eta) {
        this.eta = eta.trim();
        if ( this.eta.equals("//")){
            this.eta = " ";
        }
    }

    public String getEta() {
        return eta;
    }

    public void setEtd(String etd) {
        this.etd = etd.trim();
        if ( this.etd.equals("//")){
            this.etd = " ";
        }
    }

    public String getEtd() {
        return etd;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setInstructBillDte(String instructBillDte) {
        this.instructBillDte = instructBillDte.trim();
        if ( this.instructBillDte.equals("//")){
            this.instructBillDte = " ";
        }
    }

    public String getInstructBillDte() {
        return instructBillDte;
    }

    public void setInvPrintDte(String invPrintDte) {
        this.invPrintDte = invPrintDte.trim();
        if ( this.invPrintDte.equals("//")){
            this.invPrintDte = " ";
        }
    }

    public String getInvPrintDte() {
        return invPrintDte;
    }

    public void setInstructBillDur(String instructBillDur) {
        this.instructBillDur = instructBillDur;
    }

    public String getInstructBillDur() {
        return instructBillDur;
    }

    public void setInvPrintDur(String invPrintDur) {
        this.invPrintDur = invPrintDur;
    }

    public String getInvPrintDur() {
        return invPrintDur;
    }

    public void setAmtUnInstr(String amt) {
        this.amt = amt;
    }

    public String getAmtUnInstr() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getAmt() {
        return amt;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }
}
