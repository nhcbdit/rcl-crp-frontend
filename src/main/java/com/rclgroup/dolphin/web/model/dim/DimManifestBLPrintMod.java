/*--
--------------------------------------------------------
DimManifestBLPrintingMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
Author Sukit    
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
## 25/08/10         KIT        N/A         Created
01 10/11/10         KIT        435         Wrong display data
02 21/06/12 NIP    PD_CR_20120425-01 DEX_DIM_Add function to select some BL
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimManifestBLPrintMod extends RrcStandardMod {
 
    private String sessionId;
    private String blNo;
    private String blId;  
    private String blIdDesc;
    private String blCreateDate;
    private String blCreateDateConvert;
    private String blStatus;
    private String blStatusDesc;
    private String pol;
    private String polTerminal;
    private String pot;
    private String pod;
    private String podTerminal;
    private String printFlag;  
    private String printFlagDesc; 
    private String userName;
    private String exportSts; // ##01
    private String importSts;  // ##01
    private String selectBl;//##02
     
    public DimManifestBLPrintMod() {
        sessionId = "";
        blNo = "";
        blId = "";
        blIdDesc = "";
        blCreateDate = "" ;
        blCreateDateConvert = "";
        blStatus = "";
        blStatusDesc = "";
        pol = "";
        polTerminal = "";
        pot = "";
        pod = "";  
        podTerminal = "";  
        printFlag="";
        printFlagDesc="";
        userName="";
        exportSts="";
        importSts="";
        selectBl="";
    }


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setBlId(String blId) {
        this.blId = blId;
    }

    public String getBlId() {
        return blId;
    }

    public void setBlIdDesc(String blIdDesc) {
        this.blIdDesc = blIdDesc;
    }

    public String getBlIdDesc() {
        return blIdDesc;
    }

    public void setBlCreateDate(String blCreateDate) {
        this.blCreateDate = blCreateDate;
    }

    public String getBlCreateDate() {
        return blCreateDate;
    }

    public void setBlCreateDateConvert(String blCreateDateConvert) {
        this.blCreateDateConvert = blCreateDateConvert;
    }

    public String getBlCreateDateConvert() {
        return blCreateDateConvert;
    }

    public void setBlStatus(String blStatus) {
        this.blStatus = blStatus;
    }

    public String getBlStatus() {
        return blStatus;
    }

    public void setBlStatusDesc(String blStatusDesc) {
        this.blStatusDesc = blStatusDesc;
    }

    public String getBlStatusDesc() {
        return blStatusDesc;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPolTerminal(String polTerminal) {
        this.polTerminal = polTerminal;
    }

    public String getPolTerminal() {
        return polTerminal;
    }

    public void setPot(String pot) {
        this.pot = pot;
    }

    public String getPot() {
        return pot;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setPrintFlag(String printFlag) {
        this.printFlag = printFlag;
    }

    public String getPrintFlag() {
        return printFlag;
    }

    public void setPrintFlagDesc(String printFlagDesc) {
        this.printFlagDesc = printFlagDesc;
    }

    public String getPrintFlagDesc() {
        return printFlagDesc;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
/*   BEGIN 01*/
    public void setExportSts(String exportSts) {
        this.exportSts = exportSts;
    }

    public String getExportSts() {
        return getDesStatus(this.exportSts);
    }

    public void setImportSts(String importSts) {
        this.importSts = importSts;
    }

    public String getImportSts() {
        return getDesStatus(this.importSts);
    }
    
    private String getDesStatus(String status){

        switch (Integer.parseInt(status)) {
                   case 1 : status = "Entry"; break;
                   case 2:  status = "Confirmed"; break;
                   case 3:  status = "Printed"; break;
                   case 4:  status = "Manifested"; break;
                   case 5:  status = "Invoiced"; break;
                   case 6:  status = "Waitlised"; break;
                   case 7:  status = "MultiPart(B/L)"; break;
                   case 9:  status = "Cancelled"; break;
        }
        return status;
    }
    /*   END 01*/

    public void setSelectBl(String selectBl) {
        this.selectBl = selectBl;
    }

    public String getSelectBl() {
        return selectBl;
    }
}
