package com.rclgroup.dolphin.web.model.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BkgBookingAvailabilityReportMod extends RrcStandardMod{
    
    
    private String service;
    private String vessel;
    private String voyage;
    private String direction;    
    private String pol;
    private String dateFrom;
    private String dateTo;
    private String socCoc;
    private String reportUrl;
    private String userId;
    private String sessionId;


    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setSocCoc(String socCoc) {
        this.socCoc = socCoc;
    }

    public String getSocCoc() {
        return socCoc;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }
}
