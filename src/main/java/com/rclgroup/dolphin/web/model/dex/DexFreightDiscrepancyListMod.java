package com.rclgroup.dolphin.web.model.dex;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DexFreightDiscrepancyListMod extends RrcStandardMod{
    private String sessionId;
    private String userName;
    private String invDateFrom;
    private String invDateTo;
    private String blDateFrom;
    private String blDateTo;
    private String service;
    private String vessel;
    private String voyage;
    private String line;  
    private String region;  
    private String agent;  
    private String fsc;  
    private String pol;  
    private String pod;
    private String bl; 
    private String dir; 
    private String compareCase;
    private String qtn;
    private String inv;
    
    public DexFreightDiscrepancyListMod() {
        sessionId="";
        userName="";
        invDateFrom="";
        invDateTo="";
        blDateFrom="";
        blDateTo="";
        service="";
        vessel="";
        voyage="";
        line="";  
        region="";  
        agent="";  
        fsc="";  
        pol="";  
        pod="";
        bl=""; 
        dir=""; 
        compareCase="";
        qtn="";
        inv="";
    }


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setInvDateFrom(String invDateFrom) {
        this.invDateFrom = invDateFrom;
    }

    public String getInvDateFrom() {
        return invDateFrom;
    }

    public void setInvDateTo(String invDateTo) {
        this.invDateTo = invDateTo;
    }

    public String getInvDateTo() {
        return invDateTo;
    }

    public void setBlDateFrom(String blDateFrom) {
        this.blDateFrom = blDateFrom;
    }

    public String getBlDateFrom() {
        return blDateFrom;
    }

    public void setBlDateTo(String blDateTo) {
        this.blDateTo = blDateTo;
    }

    public String getBlDateTo() {
        return blDateTo;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDir() {
        return dir;
    }

    public void setCompareCase(String compareCase) {
        this.compareCase = compareCase;
    }

    public String getCompareCase() {
        return compareCase;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setQtn(String qtn) {
        this.qtn = qtn;
    }

    public String getQtn() {
        return qtn;
    }

    public void setInv(String inv) {
        this.inv = inv;
    }

    public String getInv() {
        return inv;
    }
}
