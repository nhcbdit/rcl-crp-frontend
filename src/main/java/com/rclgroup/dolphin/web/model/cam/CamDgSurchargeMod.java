/*------------------------------------------------------
CamDgSurchargeMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Wuttitorn Wuttijiaranai 04/12/2009
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamDgSurchargeMod extends RrcStandardMod {
    private String surchargeCode;
    private String surchargeDescription;
    
    public CamDgSurchargeMod() {
        setSurchargeCode("");
        setSurchargeDescription("");
    }

    public String getSurchargeCode() {
        return surchargeCode;
    }

    public void setSurchargeCode(String surchargeCode) {
        this.surchargeCode = surchargeCode;
    }

    public String getSurchargeDescription() {
        return surchargeDescription;
    }

    public void setSurchargeDescription(String surchargeDescription) {
        this.surchargeDescription = surchargeDescription;
    }
    
}
