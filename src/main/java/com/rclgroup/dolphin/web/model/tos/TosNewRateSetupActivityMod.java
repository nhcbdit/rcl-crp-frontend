package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewRateSetupActivityMod extends RrcStandardMod {
    private String line;
    private String port;
    private String terminal;
    private String opr_code;
    private String activity_code;
    private String seq_no;
    private String sub_seq_no;
    private String parent_activity_code;
    private String description;
    private String rate_basis;
    private String volume_units;
    private String volume_moves;
    private String volume_tons;
    private String charge_code;
    private String auto_yn;
    private String load_disch_flag;
    private String include_yn;
    private String record_status;
    private String disch_activity;
    private String reefer_yn;
    private String recovery_yn;
    private String recovery_chg_code;
    private String catg_based_yn;
    private boolean include;
    
    public TosNewRateSetupActivityMod() {
        super();
        
        line = "";
        port = "";
        terminal = "";
        opr_code = "";
        activity_code = "";
        seq_no = "";
        sub_seq_no = "";
        parent_activity_code = "";
        description = "";
        rate_basis = "";
        volume_units = "";
        volume_moves = "";
        volume_tons = "";
        charge_code = "";
        auto_yn = "";
        load_disch_flag = "";
        include_yn = "";
        record_status = "";
        disch_activity = "";
        reefer_yn = "";
        recovery_yn = "";
        recovery_chg_code = "";
        catg_based_yn = "";
        include = true;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setOpr_code(String opr_code) {
        this.opr_code = opr_code;
    }

    public String getOpr_code() {
        return opr_code;
    }

    public void setActivity_code(String activity_code) {
        this.activity_code = activity_code;
    }

    public String getActivity_code() {
        return activity_code;
    }

    public void setSeq_no(String seq_no) {
        this.seq_no = seq_no;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public void setSub_seq_no(String sub_seq_no) {
        this.sub_seq_no = sub_seq_no;
    }

    public String getSub_seq_no() {
        return sub_seq_no;
    }

    public void setParent_activity_code(String parent_activity_code) {
        this.parent_activity_code = parent_activity_code;
    }

    public String getParent_activity_code() {
        return parent_activity_code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setRate_basis(String rate_basis) {
        this.rate_basis = rate_basis;
    }

    public String getRate_basis() {
        return rate_basis;
    }

    public void setVolume_units(String volume_units) {
        this.volume_units = volume_units;
    }

    public String getVolume_units() {
        return volume_units;
    }

    public void setVolume_moves(String volume_moves) {
        this.volume_moves = volume_moves;
    }

    public String getVolume_moves() {
        return volume_moves;
    }

    public void setVolume_tons(String volume_tons) {
        this.volume_tons = volume_tons;
    }

    public String getVolume_tons() {
        return volume_tons;
    }

    public void setCharge_code(String charge_code) {
        this.charge_code = charge_code;
    }

    public String getCharge_code() {
        return charge_code;
    }

    public void setAuto_yn(String auto_yn) {
        this.auto_yn = auto_yn;
    }

    public String getAuto_yn() {
        return auto_yn;
    }

    public void setLoad_disch_flag(String load_disch_flag) {
        this.load_disch_flag = load_disch_flag;
    }

    public String getLoad_disch_flag() {
        return load_disch_flag;
    }

    public void setInclude_yn(String include_yn) {
        this.include_yn = include_yn;
    }

    public String getInclude_yn() {
        return include_yn;
    }

    public void setRecord_status(String record_status) {
        this.record_status = record_status;
    }

    public String getRecord_status() {
        return record_status;
    }

    public void setDisch_activity(String disch_activity) {
        this.disch_activity = disch_activity;
    }

    public String getDisch_activity() {
        return disch_activity;
    }

    public void setReefer_yn(String reefer_yn) {
        this.reefer_yn = reefer_yn;
    }

    public String getReefer_yn() {
        return reefer_yn;
    }

    public void setRecovery_yn(String recovery_yn) {
        this.recovery_yn = recovery_yn;
    }

    public String getRecovery_yn() {
        return recovery_yn;
    }

    public void setRecovery_chg_code(String recovery_chg_code) {
        this.recovery_chg_code = recovery_chg_code;
    }

    public String getRecovery_chg_code() {
        return recovery_chg_code;
    }

    public void setCatg_based_yn(String catg_based_yn) {
        this.catg_based_yn = catg_based_yn;
    }

    public String getCatg_based_yn() {
        return catg_based_yn;
    }

    public void setInclude(boolean include) {
        this.include = include;
    }
}
