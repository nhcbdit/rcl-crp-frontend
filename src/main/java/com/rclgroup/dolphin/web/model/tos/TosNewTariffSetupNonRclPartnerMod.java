package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupNonRclPartnerMod extends RrcStandardMod {
    private String vesselOpr;
    private String seqNo;
    private String oprType;
    private String oprTypeDesc;
    private String vendorCode;
    private String vendorDesc;
    private String payTo;
    private String tosRateSeq; 
    private String effDate;
    private String expDate;
    private String rateRef;
    private String buyTerm;
    private String tosRateSeqNo;
    private int rowStatus;
    private boolean isDelete;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewTariffSetupNonRclPartnerMod() {
        super();
        vesselOpr = "";
        seqNo = "";
        oprType = "";
        oprTypeDesc = "";
        vendorCode = "";
        vendorDesc = "";
        payTo = "";
        tosRateSeq = "";
        effDate = "";
        expDate = "";
        rateRef = "";
        buyTerm = "";
        isDelete = false;
        tosRateSeqNo = "";
        rowStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setVesselOpr(String vesselOpr) {
        this.vesselOpr = vesselOpr;
    }

    public String getVesselOpr() {
        return vesselOpr;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprTypeDesc(String oprTypeDesc) {
        this.oprTypeDesc = oprTypeDesc;
    }

    public String getOprTypeDesc() {
        return oprTypeDesc;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorDesc(String vendorDesc) {
        this.vendorDesc = vendorDesc;
    }

    public String getVendorDesc() {
        return vendorDesc;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public String getPayTo() {
        return payTo;
    }

    public void setTosRateSeq(String tosRateSeq) {
        this.tosRateSeq = tosRateSeq;
    }

    public String getTosRateSeq() {
        return tosRateSeq;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setRateRef(String rateRef) {
        this.rateRef = rateRef;
    }

    public String getRateRef() {
        return rateRef;
    }

    public void setBuyTerm(String buyTerm) {
        this.buyTerm = buyTerm;
    }

    public String getBuyTerm() {
        return buyTerm;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setTosRateSeqNo(String tosRateSeqNo) {
        this.tosRateSeqNo = tosRateSeqNo;
    }

    public String getTosRateSeqNo() {
        return tosRateSeqNo;
    }

    public void setRowStatus(int rowStatus) {
        this.rowStatus = rowStatus;
    }

    public int getRowStatus() {
        return rowStatus;
    }
}
