/*-----------------------------------------------------------------------------------------------------------  
CamRseScheduleNumberMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 10/02/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseScheduleNumberMod extends RrcStandardMod {
    
    private String scheduleNumberId;
    private String serviceCode;
    private String currentNumber;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    
    public CamRseScheduleNumberMod() {
        super();
    }

    public String getScheduleNumberId() {
        return scheduleNumberId;
    }

    public void setScheduleNumberId(String scheduleNumberId) {
        this.scheduleNumberId = scheduleNumberId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(String currentNumber) {
        this.currentNumber = currentNumber;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}


