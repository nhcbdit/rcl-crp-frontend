/*--------------------------------------------------------
DimInwardContainerMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Kitti Pongsirisakun 08/07/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimInwardContainerMod extends RrcStandardMod {
    private String shipAgentCode;           //1
    private String voyageNo;                //2 
    private String arrivalDep1;             //3
    private String arrivalDep2;             //4 
    private String containerNo;             //5
    private String grossWeg;                //6 
    private String unitOfWef;               //7
    private String sizeTypeContainer;       //8
    private String callSign;                //9
    private String lastPort;                //10
    private String nextPort;                //11
    private String blNo;                    //12
    private String stowagePos;              //13
    private String loadingPort;             //14
    private String originalLoadPort;        //15
    private String dischargePort;           //16
    private String tsDischargePort;         //17
    private String finalPort;               //18
    private String placeOfRec;              //19
    private String placeOfDev;              //20
    private String consigneeOper;           //21
    private String containerMat;            //22
    private String status;                  //23
    private String reeferTemp;              //24
    private String imoClass;                //25
    private String overHeight;              //26
    private String overLength;              //27
    private String overWidthR;              //28
    private String overWidthL;              //29
    private String directDev;               //30
    private String secondVesCallsign;       //31
    private String secondVoyNo;             //32
    private String consigneeName;           //33
    private String typeOfContainer;         //34
    private String containerAction;         //35
    private String moveMent;                //36
    private String freeTxtRmk;              //37
    private String processIndicator;        //38
    private String portOfLodOfDoc;          //39
    private String ediUserId;               //40
    private String dateTransmit;            //41
    private String timeTransmit;            //42
    public DimInwardContainerMod() {
        shipAgentCode = "" ;           //1
        voyageNo = "" ;                //2 
        arrivalDep1 = "" ;             //3
        arrivalDep2 = "" ;             //4 
        containerNo = "" ;             //5
        grossWeg = "" ;                //6 
        unitOfWef = "" ;               //7
        sizeTypeContainer = "" ;       //8
        callSign = "" ;                //9
        lastPort = "" ;                //10
        nextPort = "" ;                //11
        blNo = "" ;                    //12
        stowagePos = "" ;              //13
        loadingPort = "" ;             //14
        originalLoadPort = "" ;        //15
        dischargePort = "" ;           //16
        tsDischargePort = "" ;         //17
        finalPort = "" ;               //18
        placeOfRec = "" ;              //19
        placeOfDev = "" ;              //20
        consigneeOper = "" ;           //21
        containerMat = "" ;            //22
        status = "" ;                  //23
        reeferTemp = "" ;              //24
        imoClass = "" ;                //25
        overHeight = "" ;              //26
        overLength = "" ;              //27
        overWidthR = "" ;              //28
        overWidthL = "" ;              //29
        directDev = "" ;               //30
        secondVesCallsign = "" ;       //31
        secondVoyNo = "" ;             //32
        consigneeName = "" ;           //33
        typeOfContainer = "" ;         //34
        containerAction = "" ;         //35
        moveMent = "" ;                //36
        freeTxtRmk = "" ;              //37
        processIndicator = "" ;        //38
        portOfLodOfDoc = "" ;          //39
        ediUserId = "" ;               //40
        dateTransmit = "" ;            //41
        timeTransmit = "" ; 
    }

    public void setShipAgentCode(String shipAgentCode) {
        this.shipAgentCode = shipAgentCode;
    }

    public String getShipAgentCode() {
        return shipAgentCode;
    }

    public void setVoyageNo(String voyageNo) {
        this.voyageNo = voyageNo;
    }

    public String getVoyageNo() {
        return voyageNo;
    }

    public void setArrivalDep1(String arrivalDep1) {
        this.arrivalDep1 = arrivalDep1;
    }

    public String getArrivalDep1() {
        return arrivalDep1;
    }

    public void setArrivalDep2(String arrivalDep2) {
        this.arrivalDep2 = arrivalDep2;
    }

    public String getArrivalDep2() {
        return arrivalDep2;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setGrossWeg(String grossWeg) {
        this.grossWeg = grossWeg;
    }

    public String getGrossWeg() {
        return grossWeg;
    }

    public void setUnitOfWef(String unitOfWef) {
        this.unitOfWef = unitOfWef;
    }

    public String getUnitOfWef() {
        return unitOfWef;
    }

    public void setSizeTypeContainer(String sizeTypeContainer) {
        this.sizeTypeContainer = sizeTypeContainer;
    }

    public String getSizeTypeContainer() {
        return sizeTypeContainer;
    }

    public void setCallSign(String callSign) {
        this.callSign = callSign;
    }

    public String getCallSign() {
        return callSign;
    }

    public void setLastPort(String lastPort) {
        this.lastPort = lastPort;
    }

    public String getLastPort() {
        return lastPort;
    }

    public void setNextPort(String nextPort) {
        this.nextPort = nextPort;
    }

    public String getNextPort() {
        return nextPort;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setStowagePos(String stowagePos) {
        this.stowagePos = stowagePos;
    }

    public String getStowagePos() {
        return stowagePos;
    }

    public void setLoadingPort(String loadingPort) {
        this.loadingPort = loadingPort;
    }

    public String getLoadingPort() {
        return loadingPort;
    }

    public void setOriginalLoadPort(String originalLoadPort) {
        this.originalLoadPort = originalLoadPort;
    }

    public String getOriginalLoadPort() {
        return originalLoadPort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setTsDischargePort(String tsDischargePort) {
        this.tsDischargePort = tsDischargePort;
    }

    public String getTsDischargePort() {
        return tsDischargePort;
    }

    public void setFinalPort(String finalPort) {
        this.finalPort = finalPort;
    }

    public String getFinalPort() {
        return finalPort;
    }

    public void setPlaceOfRec(String placeOfRec) {
        this.placeOfRec = placeOfRec;
    }

    public String getPlaceOfRec() {
        return placeOfRec;
    }

    public void setPlaceOfDev(String placeOfDev) {
        this.placeOfDev = placeOfDev;
    }

    public String getPlaceOfDev() {
        return placeOfDev;
    }

    public void setConsigneeOper(String consigneeOper) {
        this.consigneeOper = consigneeOper;
    }

    public String getConsigneeOper() {
        return consigneeOper;
    }

    public void setContainerMat(String containerMat) {
        this.containerMat = containerMat;
    }

    public String getContainerMat() {
        return containerMat;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setReeferTemp(String reeferTemp) {
        this.reeferTemp = reeferTemp;
    }

    public String getReeferTemp() {
        return reeferTemp;
    }

    public void setImoClass(String imoClass) {
        this.imoClass = imoClass;
    }

    public String getImoClass() {
        return imoClass;
    }

    public void setOverHeight(String overHeight) {
        this.overHeight = overHeight;
    }

    public String getOverHeight() {
        return overHeight;
    }

    public void setOverLength(String overLength) {
        this.overLength = overLength;
    }

    public String getOverLength() {
        return overLength;
    }

    public void setOverWidthR(String overWidthR) {
        this.overWidthR = overWidthR;
    }

    public String getOverWidthR() {
        return overWidthR;
    }

    public void setOverWidthL(String overWidthL) {
        this.overWidthL = overWidthL;
    }

    public String getOverWidthL() {
        return overWidthL;
    }

    public void setDirectDev(String directDev) {
        this.directDev = directDev;
    }

    public String getDirectDev() {
        return directDev;
    }

    public void setSecondVesCallsign(String secondVesCallsign) {
        this.secondVesCallsign = secondVesCallsign;
    }

    public String getSecondVesCallsign() {
        return secondVesCallsign;
    }

    public void setSecondVoyNo(String secondVoyNo) {
        this.secondVoyNo = secondVoyNo;
    }

    public String getSecondVoyNo() {
        return secondVoyNo;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setTypeOfContainer(String typeOfContainer) {
        this.typeOfContainer = typeOfContainer;
    }

    public String getTypeOfContainer() {
        return typeOfContainer;
    }

    public void setContainerAction(String containerAction) {
        this.containerAction = containerAction;
    }

    public String getContainerAction() {
        return containerAction;
    }

    public void setMoveMent(String moveMent) {
        this.moveMent = moveMent;
    }

    public String getMoveMent() {
        return moveMent;
    }

    public void setFreeTxtRmk(String freeTxtRmk) {
        this.freeTxtRmk = freeTxtRmk;
    }

    public String getFreeTxtRmk() {
        return freeTxtRmk;
    }

    public void setProcessIndicator(String processIndicator) {
        this.processIndicator = processIndicator;
    }

    public String getProcessIndicator() {
        return processIndicator;
    }

    public void setPortOfLodOfDoc(String portOfLodOfDoc) {
        this.portOfLodOfDoc = portOfLodOfDoc;
    }

    public String getPortOfLodOfDoc() {
        return portOfLodOfDoc;
    }

    public void setEdiUserId(String ediUserId) {
        this.ediUserId = ediUserId;
    }

    public String getEdiUserId() {
        return ediUserId;
    }

    public void setDateTransmit(String dateTransmit) {
        this.dateTransmit = dateTransmit;
    }

    public String getDateTransmit() {
        return dateTransmit;
    }

    public void setTimeTransmit(String timeTransmit) {
        this.timeTransmit = timeTransmit;
    }

    public String getTimeTransmit() {
        return timeTransmit;
    }
}
