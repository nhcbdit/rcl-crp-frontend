/*-----------------------------------------------------------------------------------------------------------  
CamRseServiceMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 25/08/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 23/09/11 Dhruv                     Getter/Setter mathod defined for OfflineFlag column.
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseServiceMod extends RrcStandardMod {
    
    private String serviceId;
    private String serviceCode;
    private String serviceName;
    private String description;
    private String serviceType;
    private String moduleCode;
    private String moduleDescr;
    private String subtype;
    private String serviceFile;
    private String servicePkg;
    private String offlineFlg; //##01
    private String grpMandatoryCd;
    private String serviceFileFormat;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public CamRseServiceMod() {
        super();
        
        serviceId = "";
        serviceCode = "";
        serviceName = "";
        description = "";
        serviceType = "";
        moduleCode = "";
        subtype = "";
        serviceFile = "";
        offlineFlg = ""; //##01
        recordAddDate = null; 
        recordChangeDate = null;
        grpMandatoryCd = "";
        moduleDescr = "";
        servicePkg = "";
        serviceFileFormat ="";
    }
    
    public CamRseServiceMod(CamRseServiceMod copy) {
        super();
        if (copy != null) {
            this.serviceId = copy.getServiceId();
            this.serviceCode = copy.getServiceCode();
            this.serviceName = copy.getServiceName();
            this.description = copy.getDescription();
            this.serviceType = copy.getServiceType();
            this.moduleCode = copy.getModuleCode();
            this.subtype = copy.getSubtype();
            this.serviceFile = copy.getServiceFile();
            this.offlineFlg  = copy.getOfflineFlg(); //##01
            this.recordAddDate = copy.getRecordAddDate();
            this.recordChangeDate = copy.getRecordChangeDate();
            this.recordStatus = copy.getRecordStatus();
            this.recordAddUser = copy.getRecordAddUser();
            this.recordChangeUser = copy.getRecordChangeUser();
            this.grpMandatoryCd = copy.getGrpMandatoryCd();
        }
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getServiceFile() {
        return serviceFile;
    }

    public void setServiceFile(String serviceFile) {
        this.serviceFile = serviceFile;
    }
    
    //##01 - Starts
    public String getOfflineFlg(){
        return offlineFlg;
    }
    
    public void setOfflineFlg(String offlineFlg){
        this.offlineFlg = offlineFlg;
    }
    //##01 - Ends
    
    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public void setGrpMandatoryCd(String grpMandatoryCd) {
        this.grpMandatoryCd = grpMandatoryCd;
    }

    public String getGrpMandatoryCd() {
        return grpMandatoryCd;
    }

    public void setModuleDescr(String moduleDescr) {
        this.moduleDescr = moduleDescr;
    }

    public String getModuleDescr() {
        return moduleDescr;
    }

    public void setServicePkg(String servicePkg) {
        this.servicePkg = servicePkg;
    }

    public String getServicePkg() {
        return servicePkg;
    }


    public void setServiceFileFormat(String serviceFileFormat) {
        this.serviceFileFormat = serviceFileFormat;
    }

    public String getServiceFileFormat() {
        return serviceFileFormat;
    }
}


