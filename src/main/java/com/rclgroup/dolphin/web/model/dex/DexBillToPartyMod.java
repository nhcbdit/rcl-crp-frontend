package com.rclgroup.dolphin.web.model.dex;

public class DexBillToPartyMod {
    public DexBillToPartyMod() {
    }
    private String seqNo;
    private String partyCode;
    private String partyName;
    private String responseFscCode;
    private String responseFscCountry;
    private String bypassFscCode;
    private String bypassFscCountry;
    private String reason;
    private String status;
    
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSeqNo() {
        return seqNo;
    }
    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setResponseFscCode(String responseFscCode) {
        this.responseFscCode = responseFscCode;
    }

    public String getResponseFscCode() {
        return responseFscCode;
    }

    public void setResponseFscCountry(String responseFscCountry) {
        this.responseFscCountry = responseFscCountry;
    }

    public String getResponseFscCountry() {
        return responseFscCountry;
    }

    public void setBypassFscCode(String bypassFscCode) {
        this.bypassFscCode = bypassFscCode;
    }

    public String getBypassFscCode() {
        return bypassFscCode;
    }

    public void setBypassFscCountry(String bypassFscCountry) {
        this.bypassFscCountry = bypassFscCountry;
    }

    public String getBypassFscCountry() {
        return bypassFscCountry;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
