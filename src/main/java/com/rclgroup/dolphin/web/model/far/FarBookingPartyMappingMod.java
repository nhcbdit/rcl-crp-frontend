/*-----------------------------------------------------------------------------------------------------------  
FarBookingPartyMappingMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 20/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class FarBookingPartyMappingMod extends RrcStandardMod {
    
    private String pkBookingPartyMapId;
    private String billToParty;
    private String bookingParty;
    private String bookingPartyName;
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;
    
    public FarBookingPartyMappingMod() {
        super();
        pkBookingPartyMapId = "";
        billToParty = "";
        bookingParty = "";
        bookingPartyName = "";
        recordAddDate = null;
        recordChangeDate = null;
    }

    public String getPkBookingPartyMapId() {
        return pkBookingPartyMapId;
    }

    public void setPkBookingPartyMapId(String pkBookingPartyMapId) {
        this.pkBookingPartyMapId = pkBookingPartyMapId;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getBookingParty() {
        return bookingParty;
    }

    public void setBookingParty(String bookingParty) {
        this.bookingParty = bookingParty;
    }

    public String getBookingPartyName() {
        return bookingPartyName;
    }

    public void setBookingPartyName(String bookingPartyName) {
        this.bookingPartyName = bookingPartyName;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}
