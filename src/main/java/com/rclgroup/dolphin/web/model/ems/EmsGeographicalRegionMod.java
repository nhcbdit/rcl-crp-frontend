/*-----------------------------------------------------------------------------------------------------------  
EmsAreaMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Manop Wanngam 05/11/07 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 29/04/08  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.model.ems;

 import com.rclgroup.dolphin.web.common.RrcStandardMod;


 public class EmsGeographicalRegionMod extends RrcStandardMod {
     private String regionCode;
     private String regionName;
     private String regionStatus;

     public EmsGeographicalRegionMod() {
         regionCode = "";
         regionName = "";
         regionStatus = "";
     }
     
      public String getRegionCode() {
         return regionCode;
     }

     public void setRegionCode(String regionCode) {
         this.regionCode = regionCode;
     }

     public String getRegionName() {
         return regionName;
     }

     public void setRegionName(String regionName) {
         this.regionName = regionName;
     }

     public String getRegionStatus() {
         return regionStatus;
     }

     public void setRegionStatus(String regionStatus) {
         this.regionStatus = regionStatus;
     }
 }
