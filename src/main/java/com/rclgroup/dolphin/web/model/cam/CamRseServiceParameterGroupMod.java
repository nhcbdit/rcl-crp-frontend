/*-----------------------------------------------------------------------------------------------------------  
CamRseServiceParameterGroupMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 19/01/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01   21/06/13    SUJ                        get group parameter by sql type
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseServiceParameterGroupMod extends RrcStandardMod {
    
    private String parameterGroupId;
    private String parameterGroupCode;
    private String parameterGroupName;
    private String parameterGroupType;
    //#01
    private String sqlStatement;
    private String elementName;
    private String elementValue;
    private String seqNo;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    private String recordChangeDateStr;
    
    private boolean isDelete;
    
    /**
    * Constructor method used for setting default value.
    */
    public CamRseServiceParameterGroupMod() {
        parameterGroupId        = "";
        parameterGroupCode      = "";
        parameterGroupName      = "";
        parameterGroupType      = "";
        sqlStatement            = "";
        elementName             = "";
        elementValue            = "";
        seqNo                   = "";
        recordChangeDateStr     = "";
    }

    /**
    * The method used for getting parameter group id from object
    * @return String parameter group id
    */
    public String getParameterGroupId() {
        return parameterGroupId;
    }
    
    /**
    * The method used for setting parameter group id to object
    * @param parameterGroupId is parameter group id 
    */
    public void setParameterGroupId(String parameterGroupId) {
        this.parameterGroupId = parameterGroupId;
    }
    
    /**
    * The method used for getting parameter group code from object
    * @return String parameter group code
    */
    public String getParameterGroupCode() {
        return parameterGroupCode;
    }

    /**
    * The method used for setting parameter group code to object
    * @param parameterGroupCode is parameter group code 
    */
    public void setParameterGroupCode(String parameterGroupCode) {
        this.parameterGroupCode = parameterGroupCode;
    }

    /**
    * The method used for getting parameter group name from object
    * @return String parameter group name
    */
    public String getParameterGroupName() {
        return parameterGroupName;
    }

    /**
    * The method used for setting parameter group name to object
    * @param parameterGroupName is parameter group name 
    */
    public void setParameterGroupName(String parameterGroupName) {
        this.parameterGroupName = parameterGroupName;
    }

    /**
    * The method used for getting element name from object
    * @return String element name
    */
    public String getElementName() {
        return elementName;
    }

    /**
     * The method used for setting element name to object
     * @param elementName is element name 
    */
    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    /**
    * The method used for getting element value from object
    * @return String element name
    */
    public String getElementValue() {
        return elementValue;
    }

    /**
     * The method used for setting element value to object
     * @param elementValue is element value 
    */
    public void setElementValue(String elementValue) {
        this.elementValue = elementValue;
    }

    /**
    * The method used for getting sequnce no from object
    * @return String sequnce no
    */
    public String getSeqNo() {
        return seqNo;
    }

    /**
    * The method used for setting sequnce no name to object
    * @param seqNo is sequnce no
    */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    /**
    * The method used for getting record add date from object
    * @return Timestamp , time when added record
    */
    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    /**
    * The method used for setting record add date to object
    * @param recordAddDate is time when added record
    */
    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    /**
    * The method used for getting record updated date from object
    * @return Timestamp , time when updated record
    */
    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    /**
    * The method used for setting parameter group name to object
    * @param recordChangeDate is time when updated record
    */
    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    /**
    * The method used for setting parameter group name to object
    * @param recordChangeDateStr is time when updated record in string format
    */
    public void setRecordChangeDateStr(String recordChangeDateStr) {
        this.recordChangeDateStr = recordChangeDateStr;
    }
    
    /**
    * The method used for getting record updated date from object
    * @return String , record updated date in string format
    */
    public String getRecordChangeDateStr() {
        return recordChangeDateStr;
    }


    /**
    * The method used for setting delete flag to object
    * @param isDelete is boolean for show deleting flag
    */
    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
    
    /**
    * The method used for getting delete flag from object
    * @return String , true if can delete
    */
    public boolean isDelete() {
        return isDelete;
    }

    /**
    * The method used for setting parameter group type to object
    * @param parameterGroupType is parameter group type 
    */
    public void setParameterGroupType(String parameterGroupType) {
        this.parameterGroupType = parameterGroupType;
    }

    /**
    * The method used for getting parameter group type from object
    * @return String group type
    */
    public String getParameterGroupType() {
        return parameterGroupType;
    }
    
    /**
    * The method used for setting sql statement to object
    * @param sqlStatement is sql statement
    */
    //#01
    public void setSqlStatement(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }
    
    /**
    * The method used for getting sql statement from object
    * @return String sql statement
    */
    //#01
    public String getSqlStatement() {
        return sqlStatement;
    }
}


