/*-----------------------------------------------------------------------------------------------------------  
EmsEquipmentSysValidateMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/04/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class EmsEquipmentSysValidateMod extends RrcStandardMod {
    
    private String regionCode;          // REGION_CODE
    private String errorCode;		// ERROR_CODE 
    private String errorDescription;	// ERROR_DESCRIPTION
    private String errorType;		// ERROR_TYPE
    private String alertFlag;		// ALERT_FLAG
    private String status;		// STATUS

    public EmsEquipmentSysValidateMod() {
        super();
    }

    public EmsEquipmentSysValidateMod(String regionCode, String errorCode, String errorDescription, String errorType, String alertFlag, String status) {
        super();
        this.regionCode = regionCode;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.errorType = errorType;
        this.alertFlag = alertFlag;
        this.status = status;
    }
    
    public boolean equals(Object object) {
        if (object instanceof EmsEquipmentSysValidateMod) {
            EmsEquipmentSysValidateMod mod = (EmsEquipmentSysValidateMod) object;
            if (!this.regionCode.equals(mod.getRegionCode())) {
                return false;
            } else if (!this.errorCode.equals(mod.getErrorCode())) {
                return false;
            } else if (!this.errorDescription.equals(mod.getErrorDescription())) {
                return false;
            } else if (!this.errorType.equals(mod.getErrorType())) {
                return false;
            } else if (!this.alertFlag.equals(mod.getAlertFlag())) {
                return false;
            } else if (!this.status.equals(mod.getStatus())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getAlertFlag() {
        return alertFlag;
    }

    public void setAlertFlag(String alertFlag) {
        this.alertFlag = alertFlag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
          
}

