/*-----------------------------------------------------------------------------------------------------------  
VmsProformaAgencyMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2010 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 02/09/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.vms;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class VmsPerformaAgencyMod extends RrcStandardMod {
    
    private String performaNo;
    private String fsc; 
    private String status;


    public void setPerformaNo(String performaNo) {
        this.performaNo = performaNo;
    }

    public String getPerformaNo() {
        return performaNo;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}


