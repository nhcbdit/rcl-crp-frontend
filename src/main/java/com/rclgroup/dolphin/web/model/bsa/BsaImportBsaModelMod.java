/*-----------------------------------------------------------------------------------------------------------  
BsaImportBsaModelMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Apiwan Tadee 31/01/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class BsaImportBsaModelMod extends RrcStandardMod {
    
    private String localContainer;
    private String connectingMloVessel;
    private String connectingMloVoyage;  
    private String mloPod1; 
    private String mloPod2;
    private String mloPod3;
    private String placeofDelivery;
    private String tightConFlag1;
    private String tightConFlag2;
    private String tightConFlag3;
    
    private String containerNo;
    private String bookingNo;
    private String polPod;
    private String terminal;
    
    private String errorMessageOfRow;
    
    public BsaImportBsaModelMod() {
        super();
    }

    public BsaImportBsaModelMod(BsaImportBsaModelMod bean) {
        this.localContainer = bean.getLocalContainer();
        this.connectingMloVessel = bean.getConnectingMloVessel();
        this.connectingMloVoyage = bean.getConnectingMloVoyage();  
        this.mloPod1 = bean.getMloPod1(); 
        this.mloPod2 = bean.getMloPod2();
        this.mloPod3 = bean.getMloPod3();
        this.placeofDelivery = bean.getPlaceofDelivery();
    }

    public boolean equals(Object object) {
        if (object instanceof BsaImportBsaModelMod) {
            BsaImportBsaModelMod getObj = (BsaImportBsaModelMod)object;
            if(this.localContainer.equals(getObj.getLocalContainer()))
                return false;
            if(this.connectingMloVessel.equals(getObj.getConnectingMloVessel()))
                return false;
            if(this.connectingMloVoyage.equals(getObj.getConnectingMloVoyage()))
                return false;
            if(this.mloPod1.equals(getObj.getMloPod1()))
                return false;
            if(this.mloPod2.equals(getObj.getMloPod2()))
                return false;
            if(this.mloPod3.equals(getObj.getMloPod3()))
                return false;
            if(this.placeofDelivery.equals(getObj.getPlaceofDelivery()))
                return false;
            if(this.tightConFlag1.equals(getObj.getPlaceofDelivery()))
                return false;
            if(this.tightConFlag1.equals(getObj.getPlaceofDelivery()))
                return false;
            if(this.tightConFlag1.equals(getObj.getPlaceofDelivery()))
                return false;
        } else {
            return false;
        }
        return true;
    }

    public void setLocalContainer(String localContainer) {
        this.localContainer = localContainer;
    }

    public String getLocalContainer() {
        return localContainer;
    }

    public void setConnectingMloVessel(String connectingMloVessel) {
        this.connectingMloVessel = connectingMloVessel;
    }

    public String getConnectingMloVessel() {
        return connectingMloVessel;
    }

    public void setConnectingMloVoyage(String connectingMloVoyage) {
        this.connectingMloVoyage = connectingMloVoyage;
    }

    public String getConnectingMloVoyage() {
        return connectingMloVoyage;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setPolPod(String polPod) {
        this.polPod = polPod;
    }

    public String getPolPod() {
        return polPod;
    }

    public void setErrorMessageOfRow(String errorMessageOfRow) {
        this.errorMessageOfRow = errorMessageOfRow;
    }

    public String getErrorMessageOfRow() {
        return errorMessageOfRow;
    }

    public void setMloPod1(String mloPod1) {
        this.mloPod1 = mloPod1;
    }

    public String getMloPod1() {
        return mloPod1;
    }

    public void setMloPod2(String mloPod2) {
        this.mloPod2 = mloPod2;
    }

    public String getMloPod2() {
        return mloPod2;
    }

    public void setMloPod3(String mloPod3) {
        this.mloPod3 = mloPod3;
    }

    public String getMloPod3() {
        return mloPod3;
    }

    public void setPlaceofDelivery(String placeofDelivery) {
        this.placeofDelivery = placeofDelivery;
    }

    public String getPlaceofDelivery() {
        return placeofDelivery;
    }

    public void setTightConFlag1(String tightConFlag1) {
        this.tightConFlag1 = tightConFlag1;
    }

    public String getTightConFlag1() {
        return tightConFlag1;
    }

    public void setTightConFlag2(String tightConFlag2) {
        this.tightConFlag2 = tightConFlag2;
    }

    public String getTightConFlag2() {
        return tightConFlag2;
    }

    public void setTightConFlag3(String tightConFlag3) {
        this.tightConFlag3 = tightConFlag3;
    }

    public String getTightConFlag3() {
        return tightConFlag3;
    }
}


