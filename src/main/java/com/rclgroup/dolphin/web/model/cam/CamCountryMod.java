/*-----------------------------------------------------------------------------------------------------------  
CamCountryMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 10/10/07   
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 18/10/07 MNW                       Added listForHelpScreen() 
02 24/04/07  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamCountryMod extends RrcStandardMod {
    private String countryCode;
    private String countryName;
    private String countryStatus;

    public CamCountryMod() {
        countryCode = "";
        countryName = "";
        countryStatus = "";
    }
      
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryStatus() {
        return countryStatus;
    }

    public void setCountryStatus(String countryStatus) {
        this.countryStatus = countryStatus;
    }
}
