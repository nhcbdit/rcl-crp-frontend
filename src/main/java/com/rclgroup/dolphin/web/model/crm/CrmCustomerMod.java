package com.rclgroup.dolphin.web.model.crm;

public class CrmCustomerMod {

    private String customerCode;
    private String description;
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String cityName;
    private String stateCode;
    private String stateName;
    private String zipCode;
    private String telephone;
    private String faxNo;
    private String email;
    private String countryCode;
    private String countryName;
    private String consolidated;

    public CrmCustomerMod() {
        customerCode = "";
        description = "";
        address1 = "";
        address2 = "";
        address3 = "";
        address4 = "";
        cityName = "";
        stateCode = "";
        stateName = "";
        zipCode = "";
        telephone = "";
        faxNo = "";
        email = "";
        countryCode = "";
        countryName = "";
        consolidated = "";
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }
   

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getAddress4() {
        return address4;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress2() {
        return address2;
    }

    public void setConsolidated(String consolidated) {
        this.consolidated = consolidated;
    }

    public String getConsolidated() {
        return consolidated;
    }
}
