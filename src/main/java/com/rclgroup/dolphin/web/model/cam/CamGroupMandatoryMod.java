package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

/*------------------------------------------------------
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sujittra 19/02/2013 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

public class CamGroupMandatoryMod extends RrcStandardMod{
   
    
    private long  pkCamGroupMandatoryId;
    private String groupMandatoryCode;
    private String mandatorySet;
    private String parameterName;
    private String  selectState;
  //  private String  isUnselDisable;
   // private String  isSelMandatory;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    private String recodeChangeDateStr;
    
    private boolean isDelete;


    public CamGroupMandatoryMod() {

        groupMandatoryCode  ="";
        mandatorySet        ="";
        parameterName       ="";
        selectState      ="";
      //  isSelMandatory      ="";
        recodeChangeDateStr="";
           
    }

    public void setPkCamGroupMandatoryId(long pkCamGroupMandatoryId) {
        this.pkCamGroupMandatoryId = pkCamGroupMandatoryId;
    }

    public long getPkCamGroupMandatoryId() {
        return pkCamGroupMandatoryId;
    }

    public void setGroupMandatoryCode(String groupMandatoryCode) {
        this.groupMandatoryCode = groupMandatoryCode;
    }

    public String getGroupMandatoryCode() {
        return groupMandatoryCode;
    }

    public void setMandatorySet(String mandatorySet) {
        this.mandatorySet = mandatorySet;
    }

    public String getMandatorySet() {
        return mandatorySet;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterName() {
        return parameterName;
    }
/*
    public void setIsUnselDisable(String isUnselDisable) {
        this.isUnselDisable = isUnselDisable;
    }

    public String getIsUnselDisable() {
        return isUnselDisable;
    }
*/

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
    
    public void setRecodeChangeDateStr(String recodeChangeDateStr) {
        this.recodeChangeDateStr = recodeChangeDateStr;
    }

    public String getRecodeChangeDateStr() {
        return recodeChangeDateStr;
    }


    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isDelete() {
        return isDelete;
    }
/*
    public void setIsSelMandatory(String isSelMandatory) {
        this.isSelMandatory = isSelMandatory;
    }

    public String getIsSelMandatory() {
        return isSelMandatory;
    }*/

    public void setSelectState(String selectState) {
        this.selectState = selectState;
    }

    public String getSelectState() {
        return selectState;
    }
}
