/*-----------------------------------------------------------------------------------------------------------  
VmsVendorMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 28/04/2010  
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  

-----------------------------------------------------------------------------------------------------------*/


package com.rclgroup.dolphin.web.model.vms;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class VmsVendorMod extends RrcStandardMod {
    private String vendorCode;
    private String vendorName;
    private String city;
    private String State;
    private String country;
    private String zipCode;
    private String phone;
    private String fax;
    private String email;
    private String contactName;
    private String title;
    private String status;
    public VmsVendorMod() {
        vendorCode = "";
        vendorName = "";
        city = "";
        State = "";
        country = "";
        zipCode = "";
        phone = "";
        fax = "";
        email = "";
        contactName = "";
        title = "";
        status = "";        
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setState(String state) {
        this.State = state;
    }

    public String getState() {
        return State;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFax() {
        return fax;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
