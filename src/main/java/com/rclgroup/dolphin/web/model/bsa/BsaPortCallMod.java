/*-----------------------------------------------------------------------------------------------------------  
BsaPortCallMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 26/05/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Timestamp;

public class BsaPortCallMod extends RrcStandardMod {

    private String portCallID;        // PORTCALL ID
    private String variantID;         // SERVICE VARIANT ID
    private String service;           // SERVICE 
    private String proformaRefNo;     // PROFORMA REF NO
    private String portSeqFrom;       // PORT SEQUENCE FROM
    private String dirFrom;           // DIRECTION FROM
    private String portSeqTo;         // PORT SEQUENCE TO
    private String dirTo;             // DIRECTION TO
    private String port;              // PORT
    private String portGrp;           // PORT GROUP
    private String sub;               // SUB
    private String subCode;           // SUB CODE
    private String supportPortGrpId;  // SUB SUPPORTED PORT GROUP ID
    private String dir;               // DIRECT
    private String loadDis;           // LOAD DISCH
    private String tranShip;          // TRANSHIPMENT
    private String trunkInd;          // TRUNK IND 
    private String slotTeu;           // SLOT TEU
    private String slotTon;           // SLOT TON
    private String slotRef;           // SLOT REEFER
    private String avgCoc;            // AVG COC TEU WEIGHT
    private String avgSoc;            // AVG SOC TEU WEIGHT
    private String minTeu;            // MIN TEU LEVEL
    private String flag;              // INSERT,UPDATE,DELTE
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;
    private String tolelant;          // TOLELANT
    public BsaPortCallMod() {
         this("0","0","","","","0","",
              "","","0","","","","",
              "","","",
              "0","0","0","0.0","0.0",
              "0","", RrcStandardMod.RECORD_STATUS_ACTIVE,
              "",new Timestamp(0),"",new Timestamp(0),"0.0");
    }
    public BsaPortCallMod(String portCallID,String variantID,String service,String proformaRefNo,String port,String portSeqFrom,String dirFrom,
                          String supportPortGrpId, String portGrp, String portSeqTo,  String dirTo,String loadDis,  String tranShip, String trunkInd,  
                          String sub,String subCode, String dir,        
                          String slotTeu, String slotTon, String slotRef, String avgCoc,  String avgSoc, 
                          String minTeu, String flag, String recordStatus,
                          String recordAddUser, Timestamp recordAddDate, String recordChangeUser, Timestamp recordChangeDate,String tolelant) 
    {
        super(recordStatus, recordAddUser, recordChangeUser);
        this.portCallID       = portCallID;
        this.variantID        = variantID;
        this.service          = service;
        this.proformaRefNo    = proformaRefNo;
        this.port             = port;
        this.portSeqFrom      = portSeqFrom;
        this.dirFrom          = dirFrom;
        this.supportPortGrpId = supportPortGrpId;
        this.portGrp          = portGrp;
        this.portSeqTo        = portSeqTo;
        this.dirTo            = dirTo;
        this.loadDis          = loadDis;
        this.tranShip         = tranShip;
        this.trunkInd         = trunkInd;
        this.sub              = sub;
        this.subCode          = subCode;
        this.dir              = dir;   
        this.slotTeu          = slotTeu;
        this.slotTon          = slotTon;
        this.slotRef          = slotRef;
        this.avgCoc           = avgCoc;
        this.avgSoc           = avgSoc;
        this.minTeu           = minTeu;  
        this.flag             = flag;
        this.recordAddDate    = recordAddDate;
        this.recordChangeDate = recordChangeDate;
        this.tolelant         = tolelant;  // Add
    }
    
    public BsaPortCallMod(BsaPortCallMod mod) {
       this(mod.getPortCallID(),
            mod.getVariantID(),
            mod.getService(),
            mod.getProformaRefNo(),
            mod.getPort(),  
            mod.getPortSeqFrom(),
            mod.getDirFrom(),
            mod.getSupportPortGrpId(),
            mod.getPortGrp(),     
            mod.getPortSeqTo(),
            mod.getDirTo(),
            mod.getLoadDis(),
            mod.getTranShip(),
            mod.getTrunkInd(),
            mod.getSub(),
            mod.getSubCode(),          
            mod.getDir(), 
            mod.getSlotTeu(),
            mod.getSlotTon(),
            mod.getSlotRef(),
            mod.getAvgCoc(),            
            mod.getAvgSoc(), 
            mod.getMinTeu(),
            mod.getFlag(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate(),
            mod.getTolelant()); // Add
    }
    public boolean equals(Object object) {
        if (object instanceof BsaPortCallMod) {
            BsaPortCallMod mod = (BsaPortCallMod)object;
            if (!this.portCallID.equals(mod.getPortCallID())) {
                return false;
            } else if (!this.variantID.equals(mod.getVariantID())) {
                return false;
            }else if (!this.port.equals(mod.getPort())) {
                return false;
            } else if (!this.portGrp.equals(mod.getPortGrp())) {
                return false;
            } else if (!this.subCode.equals(mod.getSubCode())) {
                return false;
            } else if (!this.dir.equals(mod.getDir())) {
                return false;
            } else if (!this.loadDis.equals(mod.getLoadDis())) {
                return false;
            } else if (!this.tranShip.equals(mod.getTranShip())) {
                return false;
            } else if (!this.trunkInd.equals(mod.getTrunkInd())) {
                return false;
            } else if (!this.tolelant.equals(mod.getTolelant())) {
                return false;
            } else if (!this.slotTeu.equals(mod.getSlotTeu())) {
                return false;
            } else if (!this.slotTon.equals(mod.getSlotTon())) {
                return false;
            } else if (!this.slotRef.equals(mod.getSlotRef())) {
                return false;
            } else if (!this.avgCoc.equals(mod.getAvgCoc())) {
                return false;
            } else if (!this.avgSoc.equals(mod.getAvgSoc())) {
                return false;
            } else if (!this.minTeu.equals(mod.getMinTeu())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    public void setSlotTeu(String slotTeu) {
        this.slotTeu = slotTeu;
    }

    public String getSlotTeu() {
        return slotTeu;
    }

    public void setSlotTon(String slotTon) {
        this.slotTon = slotTon;
    }

    public String getSlotTon() {
        return slotTon;
    }

    public void setSlotRef(String slotRef) {
        this.slotRef = slotRef;
    }

    public String getSlotRef() {
        return slotRef;
    }

    public void setAvgCoc(String avgCoc) {
        this.avgCoc = avgCoc;
    }

    public String getAvgCoc() {
        return avgCoc;
    }

    public void setAvgSoc(String avgSoc) {
        this.avgSoc = avgSoc;
    }

    public String getAvgSoc() {
        return avgSoc;
    }

    public void setMinTeu(String minTeu) {
        this.minTeu = minTeu;
    }

    public String getMinTeu() {
        return minTeu;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setPortGrp(String portGrp) {
        this.portGrp = portGrp;
    }

    public String getPortGrp() {
        return portGrp;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getSub() {
        return sub;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDir() {
        return dir;
    }

    public void setLoadDis(String loadDis) {
        this.loadDis = loadDis;
    }

    public String getLoadDis() {
        return loadDis;
    }

    public void setTranShip(String tranShip) {
        this.tranShip = tranShip;
    }

    public String getTranShip() {
        return tranShip;
    }

    public void setTrunkInd(String trunkInd) {
        this.trunkInd = trunkInd;
    }

    public String getTrunkInd() {
        return trunkInd;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setPortCallID(String portCallID) {
        this.portCallID = portCallID;
    }

    public String getPortCallID() {
        return portCallID;
    }

    public void setVariantID(String variantID) {
        this.variantID = variantID;
    }

    public String getVariantID() {
        return variantID;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setProformaRefNo(String proformaRefNo) {
        this.proformaRefNo = proformaRefNo;
    }

    public String getProformaRefNo() {
        return proformaRefNo;
    }

    public void setPortSeqFrom(String portSeqFrom) {
        this.portSeqFrom = portSeqFrom;
    }

    public String getPortSeqFrom() {
        return portSeqFrom;
    }

    public void setDirFrom(String dirFrom) {
        this.dirFrom = dirFrom;
    }

    public String getDirFrom() {
        return dirFrom;
    }

    public void setPortSeqTo(String portSeqTo) {
        this.portSeqTo = portSeqTo;
    }

    public String getPortSeqTo() {
        return portSeqTo;
    }

    public void setDirTo(String dirTo) {
        this.dirTo = dirTo;
    }

    public String getDirTo() {
        return dirTo;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSupportPortGrpId(String supportPortGrpId) {
        this.supportPortGrpId = supportPortGrpId;
    }

    public String getSupportPortGrpId() {
        return supportPortGrpId;
    }

    public void setTolelant(String tolelant) {
        this.tolelant = tolelant;
    }

    public String getTolelant() {
        return tolelant;
    }
}
