/*-----------------------------------------------------------------------------------------------------------  
CamAgentMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 24/10/2007  
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 09/11/07  SPD                      status, listForHelpScreen 
02 24/04/08  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamAgentMod extends RrcStandardMod {
    private String lineCode;
    private String regionCode; // = trade
    private String countryCode;
    private String agentCode;
    private String agentName; // = company name
    private String status; // = record status

    public CamAgentMod() {
        lineCode = "";
        regionCode = "";
        agentCode = "";
        agentName = "";
        status = "";
    }
 
    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;
    }

    public String getLineCode() {
        return lineCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }
}

