package com.rclgroup.dolphin.web.model.vss;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class VssLrsMod extends RrcStandardMod {
    private String lrsNo;
    private String service;
    public VssLrsMod() {
        lrsNo = "";
        service = "";
    }

    public void setLrsNo(String lrsNo) {
        this.lrsNo = lrsNo;
    }

    public String getLrsNo() {
        return lrsNo;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }
}
