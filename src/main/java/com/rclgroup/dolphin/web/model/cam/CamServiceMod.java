/*-----------------------------------------------------------------------------------------------------------  
CamServiceMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamServiceMod extends RrcStandardMod {
    private String serviceCode;
    private String serviceName;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public CamServiceMod(String serviceCode
                        ,String serviceName
                        ,String recordStatus
                        ,String recordAddUser
                        ,Timestamp recordAddDate
                        ,String recordChangeUser
                        ,Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public CamServiceMod() {
        this("","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0)); 
    }
    
    public CamServiceMod(CamServiceMod mod){
       this(mod.getServiceCode()
           ,mod.getServiceName()
           ,mod.getRecordStatus()
           ,mod.getRecordAddUser()
           ,mod.getRecordAddDate()
           ,mod.getRecordChangeUser()
           ,mod.getRecordChangeDate());
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}


