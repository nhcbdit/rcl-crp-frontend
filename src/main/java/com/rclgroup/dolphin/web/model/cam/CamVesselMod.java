/*-----------------------------------------------------------------------------------------------------------  
CamVesselMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamVesselMod extends RrcStandardMod {
    private String vesselCode;
    private String vesselName;
    private String operatorCode;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public CamVesselMod(String vesselCode
                        ,String vesselName
                        ,String operatorCode
                        ,String recordStatus
                        ,String recordAddUser
                        ,Timestamp recordAddDate
                        ,String recordChangeUser
                        ,Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.vesselCode = vesselCode;
        this.vesselName = vesselName;
        this.operatorCode = operatorCode;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public CamVesselMod() {
        this("","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0)); 
    }
    
    public CamVesselMod(CamVesselMod mod){
       this(mod.getVesselCode()
           ,mod.getVesselName()
           ,mod.getOperatorCode()
           ,mod.getRecordStatus()
           ,mod.getRecordAddUser()
           ,mod.getRecordAddDate()
           ,mod.getRecordChangeUser()
           ,mod.getRecordChangeDate());
    }


    public void setVesselCode(String vesselCode) {
        this.vesselCode = vesselCode;
    }

    public String getVesselCode() {
        return vesselCode;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}


