/*-----------------------------------------------------------------------------------------------------------  
EmsActivityMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 10/06/2008   
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class EmsActivityMod extends RrcStandardMod {
    private String activityCode;
    private String activityName;
    private String activityStatus;

    public EmsActivityMod() {
        activityCode = "";
        activityName = "";
        activityStatus = "";
    }


    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getActivityStatus() {
        return activityStatus;
    }
}
