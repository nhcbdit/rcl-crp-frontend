package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BsaBsaServiceVariantServiceMod  extends RrcStandardMod{
    private String import_id;
    private String model_name;
    private String fk_service;
    private String fk_proforma_ref_no;
    private String variant_code;
    private String bsa_vessel_type;
    private String usage_rule;
    private String calc_frequency;
    private String calc_duration;
    private String def_slot_teu;
    private String def_slot_tons;
    private String def_slot_reefer_plugs;
    private String def_avg_coc_teu_weight;
    private String def_avg_soc_teu_weight;
    private String def_min_teu;
    private String sv_record_status;
    private String expiry_date;
    private String effective_date;
    private String sv_tolerant;
    private String service_variant_type;
    private String errorMessageOfRow;

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setFk_service(String fk_service) {
        this.fk_service = fk_service;
    }

    public String getFk_service() {
        return fk_service;
    }

    public void setFk_proforma_ref_no(String fk_proforma_ref_no) {
        this.fk_proforma_ref_no = fk_proforma_ref_no;
    }

    public String getFk_proforma_ref_no() {
        return fk_proforma_ref_no;
    }

    public void setVariant_code(String variant_code) {
        this.variant_code = variant_code;
    }

    public String getVariant_code() {
        return variant_code;
    }

    public void setBsa_vessel_type(String bsa_vessel_type) {
        this.bsa_vessel_type = bsa_vessel_type;
    }

    public String getBsa_vessel_type() {
        return bsa_vessel_type;
    }

    public void setUsage_rule(String usage_rule) {
        this.usage_rule = usage_rule;
    }

    public String getUsage_rule() {
        return usage_rule;
    }

    public void setCalc_frequency(String calc_frequency) {
        this.calc_frequency = calc_frequency;
    }

    public String getCalc_frequency() {
        return calc_frequency;
    }

    public void setCalc_duration(String calc_duration) {
        this.calc_duration = calc_duration;
    }

    public String getCalc_duration() {
        return calc_duration;
    }

    public void setDef_slot_teu(String def_slot_teu) {
        this.def_slot_teu = def_slot_teu;
    }

    public String getDef_slot_teu() {
        return def_slot_teu;
    }

    public void setDef_slot_tons(String def_slot_tons) {
        this.def_slot_tons = def_slot_tons;
    }

    public String getDef_slot_tons() {
        return def_slot_tons;
    }

    public void setDef_slot_reefer_plugs(String def_slot_reefer_plugs) {
        this.def_slot_reefer_plugs = def_slot_reefer_plugs;
    }

    public String getDef_slot_reefer_plugs() {
        return def_slot_reefer_plugs;
    }

    public void setDef_avg_coc_teu_weight(String def_avg_coc_teu_weight) {
        this.def_avg_coc_teu_weight = def_avg_coc_teu_weight;
    }

    public String getDef_avg_coc_teu_weight() {
        return def_avg_coc_teu_weight;
    }

    public void setDef_avg_soc_teu_weight(String def_avg_soc_teu_weight) {
        this.def_avg_soc_teu_weight = def_avg_soc_teu_weight;
    }

    public String getDef_avg_soc_teu_weight() {
        return def_avg_soc_teu_weight;
    }

    public void setDef_min_teu(String def_min_teu) {
        this.def_min_teu = def_min_teu;
    }

    public String getDef_min_teu() {
        return def_min_teu;
    }

    public void setSv_record_status(String sv_record_status) {
        this.sv_record_status = sv_record_status;
    }

    public String getSv_record_status() {
        return sv_record_status;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }

    public String getEffective_date() {
        return effective_date;
    }


    public void setSv_tolerant(String sv_tolerant) {
        this.sv_tolerant = sv_tolerant;
    }

    public String getSv_tolerant() {
        return sv_tolerant;
    }

    public void setService_variant_type(String service_variant_type) {
        this.service_variant_type = service_variant_type;
    }

    public String getService_variant_type() {
        return service_variant_type;
    }

    public void setImport_id(String import_id) {
        this.import_id = import_id;
    }

    public String getImport_id() {
        return import_id;
    }

    public void setErrorMessageOfRow(String errorMessageOfRow) {
        this.errorMessageOfRow = errorMessageOfRow;
    }

    public String getErrorMessageOfRow() {
        return errorMessageOfRow;
    }
}
