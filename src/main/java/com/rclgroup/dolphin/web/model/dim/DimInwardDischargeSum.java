/*--------------------------------------------------------
DimInwardDischargeDetailMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Kitti Pongsirisakun 24/11/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimInwardDischargeSum extends RrcStandardMod{
    private String amount ;
    private String size;
    private String type;
    private String containerSts;
    private String containerCode;
    private String containerCodeDesc;
    private String podTerminal;

    public DimInwardDischargeSum() {
        this.amount = "";
        this.size = "";
        this.type = "";
        this.containerSts = "";
        this.containerCode = "";
        this.podTerminal = "";
        this.containerCodeDesc = "";
    }
    
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setContainerSts(String containerSts) {
        this.containerSts = containerSts;
    }

    public String getContainerSts() {
        return containerSts;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public String getContainerCode() {
        return containerCode;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setContainerCodeDesc(String containerCodeDesc) {
        this.containerCodeDesc = containerCodeDesc;
    }

    public String getContainerCodeDesc() {
        return containerCodeDesc;
    }
}

