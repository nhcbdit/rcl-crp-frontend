/*-----------------------------------------------------------------------------------------------------------  
TosStatisticReportMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 23/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class TosStatisticReportMod extends RrcStandardMod {

    private String sessionId;
    private String username;
    private String condition;
    private String port;
    private String terminal;
    private String fromDate;
    private String toDate;
    
    public TosStatisticReportMod() {
        super();
    }
    
    public TosStatisticReportMod(String sessionId, String username, String condition, String port, String terminal, String fromDate, String toDate) {
        this.sessionId = sessionId;
        this.username = username;
        this.condition = condition;
        this.port = port;
        this.terminal = terminal;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
