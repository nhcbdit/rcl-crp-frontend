/*-----------------------------------------------------------------------------------------------------------  
IjsJobOrderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 28/04/2010  
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  

-----------------------------------------------------------------------------------------------------------*/


package com.rclgroup.dolphin.web.model.ijs;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class IjsJobOrderMod extends RrcStandardMod {
    private String joNo;
    private String joVer;
    private String joType;
    private String vendorCode;
    private String joStatus;    
    private String status;
    public IjsJobOrderMod() {
        joNo = "";
        joVer = "";
        joType = "";
        vendorCode = "";
        joStatus = "";
        status = "";
    }

    public void setJoNo(String joNo) {
        this.joNo = joNo;
    }

    public String getJoNo() {
        return joNo;
    }

    public void setJoVer(String joVer) {
        this.joVer = joVer;
    }

    public String getJoVer() {
        return joVer;
    }

    public void setJoType(String joType) {
        this.joType = joType;
    }

    public String getJoType() {
        return joType;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setJoStatus(String joStatus) {
        this.joStatus = joStatus;
    }

    public String getJoStatus() {
        return joStatus;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
