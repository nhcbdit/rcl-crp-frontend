package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BsaBsaServiceVariantPortPairMod  extends RrcStandardMod{
    private String import_id;
    private String model_name;
    private String fk_service;
    private String fk_proforma_ref_no;
    private String variant_code;
    private String pol_port_group_flag; 
    private String pol_port_code;
    private String pol_port_seq_no; 
    private String pol_transhipment_flag; 
    private String pol_call_level;
    private String pod_port_group_flag; 
    private String pod_port_code;
    private String pod_port_seq_no; 
    private String pod_transhipment_flag; 
    private String pod_call_level;
    private String ts_record;
    private String ts_indicator;
    private String ts_service;
    private String ts_variant;
    private String ts_port_code;
    private String ts_port_group;
    private String port_pair_tolelant;
    private String coc_teu_laden;
    private String coc_ton_laden;
    private String coc_20gp;
    private String coc_40gp;
    private String coc_45hc;
    private String coc_reefer_plugs; 
    private String coc_teu_mt;
    private String coc_20mt;
    private String coc_40mt;
    private String soc_teu_laden;
    private String soc_ton_laden;
    private String soc_teu_mt;
    private String port_pair_record_status;
    private String errorMessageOfRow;
    private String ts_proforma_no;
    private String pol_port_group_code;
    private String pod_port_group_code;

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setFk_service(String fk_service) {
        this.fk_service = fk_service;
    }

    public String getFk_service() {
        return fk_service;
    }

    public void setVariant_code(String variant_code) {
        this.variant_code = variant_code;
    }

    public String getVariant_code() {
        return variant_code;
    }

    public void setPol_port_group_flag(String pol_port_group_flag) {
        this.pol_port_group_flag = pol_port_group_flag;
    }

    public String getPol_port_group_flag() {
        return pol_port_group_flag;
    }

    public void setPol_port_code(String pol_port_code) {
        this.pol_port_code = pol_port_code;
    }

    public String getPol_port_code() {
        return pol_port_code;
    }

    public void setPol_port_seq_no(String pol_port_seq_no) {
        this.pol_port_seq_no = pol_port_seq_no;
    }

    public String getPol_port_seq_no() {
        return pol_port_seq_no;
    }

    public void setPol_transhipment_flag(String pol_transhipment_flag) {
        this.pol_transhipment_flag = pol_transhipment_flag;
    }

    public String getPol_transhipment_flag() {
        return pol_transhipment_flag;
    }

    public void setPol_call_level(String pol_call_level) {
        this.pol_call_level = pol_call_level;
    }

    public String getPol_call_level() {
        return pol_call_level;
    }

    public void setPod_port_group_flag(String pod_port_group_flag) {
        this.pod_port_group_flag = pod_port_group_flag;
    }

    public String getPod_port_group_flag() {
        return pod_port_group_flag;
    }

    public void setPod_port_code(String pod_port_code) {
        this.pod_port_code = pod_port_code;
    }

    public String getPod_port_code() {
        return pod_port_code;
    }

    public void setPod_port_seq_no(String pod_port_seq_no) {
        this.pod_port_seq_no = pod_port_seq_no;
    }

    public String getPod_port_seq_no() {
        return pod_port_seq_no;
    }

    public void setPod_transhipment_flag(String pod_transhipment_flag) {
        this.pod_transhipment_flag = pod_transhipment_flag;
    }

    public String getPod_transhipment_flag() {
        return pod_transhipment_flag;
    }

    public void setPod_call_level(String pod_call_level) {
        this.pod_call_level = pod_call_level;
    }

    public String getPod_call_level() {
        return pod_call_level;
    }

    public void setTs_record(String ts_record) {
        this.ts_record = ts_record;
    }

    public String getTs_record() {
        return ts_record;
    }

    public void setTs_indicator(String ts_indicator) {
        this.ts_indicator = ts_indicator;
    }

    public String getTs_indicator() {
        return ts_indicator;
    }

    public void setTs_service(String ts_service) {
        this.ts_service = ts_service;
    }

    public String getTs_service() {
        return ts_service;
    }

    public void setTs_variant(String ts_variant) {
        this.ts_variant = ts_variant;
    }

    public String getTs_variant() {
        return ts_variant;
    }

    public void setTs_port_code(String ts_port_code) {
        this.ts_port_code = ts_port_code;
    }

    public String getTs_port_code() {
        return ts_port_code;
    }

    public void setTs_port_group(String ts_port_group) {
        this.ts_port_group = ts_port_group;
    }

    public String getTs_port_group() {
        return ts_port_group;
    }

    public void setPort_pair_tolelant(String port_pair_tolelant) {
        this.port_pair_tolelant = port_pair_tolelant;
    }

    public String getPort_pair_tolelant() {
        return port_pair_tolelant;
    }

    public void setCoc_teu_laden(String coc_teu_laden) {
        this.coc_teu_laden = coc_teu_laden;
    }

    public String getCoc_teu_laden() {
        return coc_teu_laden;
    }

    public void setCoc_ton_laden(String coc_ton_laden) {
        this.coc_ton_laden = coc_ton_laden;
    }

    public String getCoc_ton_laden() {
        return coc_ton_laden;
    }

    public void setCoc_20gp(String coc_20gp) {
        this.coc_20gp = coc_20gp;
    }

    public String getCoc_20gp() {
        return coc_20gp;
    }

    public void setCoc_40gp(String coc_40gp) {
        this.coc_40gp = coc_40gp;
    }

    public String getCoc_40gp() {
        return coc_40gp;
    }

    public void setCoc_45hc(String coc_45hc) {
        this.coc_45hc = coc_45hc;
    }

    public String getCoc_45hc() {
        return coc_45hc;
    }

    public void setCoc_reefer_plugs(String coc_reefer_plugs) {
        this.coc_reefer_plugs = coc_reefer_plugs;
    }

    public String getCoc_reefer_plugs() {
        return coc_reefer_plugs;
    }

    public void setCoc_teu_mt(String coc_teu_mt) {
        this.coc_teu_mt = coc_teu_mt;
    }

    public String getCoc_teu_mt() {
        return coc_teu_mt;
    }

    public void setCoc_20mt(String coc_20mt) {
        this.coc_20mt = coc_20mt;
    }

    public String getCoc_20mt() {
        return coc_20mt;
    }

    public void setCoc_40mt(String coc_40mt) {
        this.coc_40mt = coc_40mt;
    }

    public String getCoc_40mt() {
        return coc_40mt;
    }

    public void setSoc_teu_laden(String soc_teu_laden) {
        this.soc_teu_laden = soc_teu_laden;
    }

    public String getSoc_teu_laden() {
        return soc_teu_laden;
    }

    public void setSoc_ton_laden(String soc_ton_laden) {
        this.soc_ton_laden = soc_ton_laden;
    }

    public String getSoc_ton_laden() {
        return soc_ton_laden;
    }

    public void setSoc_teu_mt(String soc_teu_mt) {
        this.soc_teu_mt = soc_teu_mt;
    }

    public String getSoc_teu_mt() {
        return soc_teu_mt;
    }

    public void setPort_pair_record_status(String port_pair_record_status) {
        this.port_pair_record_status = port_pair_record_status;
    }

    public String getPort_pair_record_status() {
        return port_pair_record_status;
    }

    public void setFk_proforma_ref_no(String fk_proforma_ref_no) {
        this.fk_proforma_ref_no = fk_proforma_ref_no;
    }

    public String getFk_proforma_ref_no() {
        return fk_proforma_ref_no;
    }

    public void setImport_id(String import_id) {
        this.import_id = import_id;
    }

    public String getImport_id() {
        return import_id;
    }

    public void setErrorMessageOfRow(String errorMessageOfRow) {
        this.errorMessageOfRow = errorMessageOfRow;
    }

    public String getErrorMessageOfRow() {
        return errorMessageOfRow;
    }

    public void setTs_proforma_no(String ts_proforma_no) {
        this.ts_proforma_no = ts_proforma_no;
    }

    public String getTs_proforma_no() {
        return ts_proforma_no;
    }

    public void setPol_port_group_code(String pol_port_group_code) {
        this.pol_port_group_code = pol_port_group_code;
    }

    public String getPol_port_group_code() {
        return pol_port_group_code;
    }

    public void setPod_port_group_code(String pod_port_group_code) {
        this.pod_port_group_code = pod_port_group_code;
    }

    public String getPod_port_group_code() {
        return pod_port_group_code;
    }
}
