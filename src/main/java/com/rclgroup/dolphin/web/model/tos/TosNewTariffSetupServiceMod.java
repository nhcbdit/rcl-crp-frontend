/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupServiceMod.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Panadda P. 28/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupServiceMod extends RrcStandardMod {

    private String serviceCd;
    private String serviceDesc;
    private boolean isDelete;
    private int rowStatus;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewTariffSetupServiceMod() {
        super();
        serviceCd = "";
        serviceDesc = "";
        isDelete = false;
        rowStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setServiceCd(String serviceCd) {
        this.serviceCd = serviceCd;
    }

    public String getServiceCd() {
        return serviceCd;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setRowStatus(int rowStatus) {
        this.rowStatus = rowStatus;
    }

    public int getRowStatus() {
        return rowStatus;
    }
}
