/*-----------------------------------------------------------------------------------------------------------  
EmsLessorMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 03/07/2008   
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class EmsLessorMod extends RrcStandardMod {
    private String LessorCode;
    private String LessorName;
    private String LessorStatus;

    public EmsLessorMod() {
        LessorCode = "";
        LessorName = "";
        LessorStatus = "";
    }


    public void setLessorCode(String LessorCode) {
        this.LessorCode = LessorCode;
    }

    public String getLessorCode() {
        return LessorCode;
    }

    public void setLessorName(String LessorName) {
        this.LessorName = LessorName;
    }

    public String getLessorName() {
        return LessorName;
    }

    public void setLessorStatus(String LessorStatus) {
        this.LessorStatus = LessorStatus;
    }

    public String getLessorStatus() {
        return LessorStatus;
    }
}
