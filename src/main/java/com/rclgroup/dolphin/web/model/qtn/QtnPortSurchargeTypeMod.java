/*------------------------------------------------------
QtnPortSurchargeTypeMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
 Author Sopon Dee-udomvongsa 02/09/2008 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class QtnPortSurchargeTypeMod extends RrcStandardMod{
    private String SurchargeTypeStatus;
    private String shipmentRequired;
    private String sizeRequired;
    private String surchargeDescription;
    private String surchargeType;
    private String termRequired;
     
    public QtnPortSurchargeTypeMod() {
        SurchargeTypeStatus = "";
        shipmentRequired =  "";
        sizeRequired = "";
        surchargeDescription = "";
        surchargeType= "";
        termRequired = "";
    }

    public String getSurchargeTypeStatus() {
        return SurchargeTypeStatus;
    }

    public void setSurchargeTypeStatus(String SurchargeTypeStatus) {
        this.SurchargeTypeStatus = SurchargeTypeStatus;
    }

    public String getShipmentRequired() {
        return shipmentRequired;
    }

    public void setShipmentRequired(String shipmentRequired) {
        this.shipmentRequired = shipmentRequired;
    }

    public String getSizeRequired() {
        return sizeRequired;
    }

    public void setSizeRequired(String sizeRequired) {
        this.sizeRequired = sizeRequired;
    }

    public String getSurchargeDescription() {
        return surchargeDescription;
    }

    public void setSurchargeDescription(String surchargeDescription) {
        this.surchargeDescription = surchargeDescription;
    }

    public String getSurchargeType() {
        return surchargeType;
    }

    public void setSurchargeType(String surchargeType) {
        this.surchargeType = surchargeType;
    }

    public String getTermRequired() {
        return termRequired;
    }

    public void setTermRequired(String termRequired) {
        this.termRequired = termRequired;
    }
}






