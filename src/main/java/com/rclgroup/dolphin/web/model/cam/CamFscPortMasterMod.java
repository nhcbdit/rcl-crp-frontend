/*------------------------------------------------------
CamFscPortMasterMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Wuttitorn Wuttijiaranai 19/08/09  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamFscPortMasterMod extends RrcStandardMod {
    private String lineCode;
    private String regionCode;
    private String agentCode;
    private String fscCode;
    private String countryCode;
    private String pointCode;
    private String portPointFlag;
    private String portName;
    private String portType;
    
    public CamFscPortMasterMod() {
        lineCode = "";
        regionCode = "";
        agentCode = "";
        fscCode = "";
        countryCode = "";
        pointCode = "";
        portPointFlag = "";
        portName = "";
        portType = "";
    }

    public String getLineCode() {
        return lineCode;
    }

    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getFscCode() {
        return fscCode;
    }

    public void setFscCode(String fscCode) {
        this.fscCode = fscCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getPortPointFlag() {
        return portPointFlag;
    }

    public void setPortPointFlag(String portPointFlag) {
        this.portPointFlag = portPointFlag;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }
}


