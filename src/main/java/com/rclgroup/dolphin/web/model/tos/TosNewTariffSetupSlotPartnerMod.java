/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupSlotPartnerMod.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Dhruv Parekh 30/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupSlotPartnerMod extends RrcStandardMod{
    
    private String vesselOpr;
    private String slotPartner;
    private String slotPartnerDesc;
    private String oprType;
    private String oprTypeDesc;
    private String effDate;
    private String expDate;
    private String proforma;
    private String recoveryRateRef;
    private String tosRateSeqNo;
    private String seqNo;
    private boolean isDelete;
    private int rowStatus;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewTariffSetupSlotPartnerMod() {
        super();
        vesselOpr = "";
        slotPartner = "";
        slotPartnerDesc = "";
        oprType = "";
        oprTypeDesc = "";
        effDate = "";
        expDate = "";
        proforma = "";
        recoveryRateRef = "";
        tosRateSeqNo = "";
        seqNo = "";
        isDelete = false;
        rowStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setVesselOpr(String vesselOpr) {
        this.vesselOpr = vesselOpr;
    }

    public String getVesselOpr() {
        return vesselOpr;
    }

    public void setSlotPartner(String slotPartner) {
        this.slotPartner = slotPartner;
    }

    public String getSlotPartner() {
        return slotPartner;
    }

    public void setSlotPartnerDesc(String slotPartnerDesc) {
        this.slotPartnerDesc = slotPartnerDesc;
    }

    public String getSlotPartnerDesc() {
        return slotPartnerDesc;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprTypeDesc(String oprTypeDesc) {
        this.oprTypeDesc = oprTypeDesc;
    }

    public String getOprTypeDesc() {
        return oprTypeDesc;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setProforma(String proforma) {
        this.proforma = proforma;
    }

    public String getProforma() {
        return proforma;
    }

    public void setRecoveryRateRef(String recoveryRateRef) {
        this.recoveryRateRef = recoveryRateRef;
    }

    public String getRecoveryRateRef() {
        return recoveryRateRef;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setTosRateSeqNo(String tosRateSeqNo) {
        this.tosRateSeqNo = tosRateSeqNo;
    }

    public String getTosRateSeqNo() {
        return tosRateSeqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setRowStatus(int rowStatus) {
        this.rowStatus = rowStatus;
    }

    public int getRowStatus() {
        return rowStatus;
    }
}
