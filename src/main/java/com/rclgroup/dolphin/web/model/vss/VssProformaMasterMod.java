/*-----------------------------------------------------------------------------------------------------------  
VssProformaMasterMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 13/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.vss;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class VssProformaMasterMod extends RrcStandardMod {
    
    private String vssProformaId; // PK_VSS_PROFORMA_ID
    private String serviceCode; // FK_SERVICE
    private String proformaReferenceNo; // FK_PROFORMA_REF_NO
    private String designedNoOfVessels; // DESIGNED_NO_OF_VESSELS
    private String validFrom; // VALID_FROM
    private String validTo; // VALID_TO
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    
    private String serviceDescription;

    public VssProformaMasterMod(String vssProformaId,
                                String serviceCode,
                                String proformaReferenceNo,
                                String designedNoOfVessels,
                                String validFrom,
                                String validTo,
                                String recordStatus,
                                String recordAddUser,
                                Timestamp recordAddDate,
                                String recordChangeUser,
                                Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.vssProformaId = vssProformaId;
        this.serviceCode = serviceCode;
        this.proformaReferenceNo = proformaReferenceNo;
        this.designedNoOfVessels = designedNoOfVessels;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
     
    public VssProformaMasterMod() {
        this("0","","","0","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0)); 
    }
    
    public VssProformaMasterMod(VssProformaMasterMod mod){
       this(mod.getVssProformaId(),
            mod.getServiceCode(),
            mod.getProformaReferenceNo(),
            mod.getDesignedNoOfVessels(),
            mod.getValidFrom(),
            mod.getValidTo(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate());
    }
    
    public boolean equals(Object object){
        if(object instanceof VssProformaMasterMod){
            VssProformaMasterMod mod = (VssProformaMasterMod)object;
            if(!this.vssProformaId.equals(mod.getVssProformaId())){
                return false;
            }else if(!this.serviceCode.equals(mod.getServiceCode())){
                return false;
            }else if(!this.proformaReferenceNo.equals(mod.getProformaReferenceNo())){
                return false;
            }else if(!this.designedNoOfVessels.equals(mod.getDesignedNoOfVessels())){
                return false;
            }else if(!this.validFrom.equals(mod.getValidFrom())){
                return false;
            }else if(!this.validTo.equals(mod.getValidTo())){
                return false;
            }else if(!this.recordStatus.equals(mod.getRecordStatus())){
                return false;
            }
        }else{
            return false;
        }
        return true;
    }
    
    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setVssProformaId(String vssProformaId) {
        this.vssProformaId = vssProformaId;
    }

    public String getVssProformaId() {
        return vssProformaId;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setProformaReferenceNo(String proformaReferenceNo) {
        this.proformaReferenceNo = proformaReferenceNo;
    }

    public String getProformaReferenceNo() {
        return proformaReferenceNo;
    }

    public void setDesignedNoOfVessels(String designedNoOfVessels) {
        this.designedNoOfVessels = designedNoOfVessels;
    }

    public String getDesignedNoOfVessels() {
        return designedNoOfVessels;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}


