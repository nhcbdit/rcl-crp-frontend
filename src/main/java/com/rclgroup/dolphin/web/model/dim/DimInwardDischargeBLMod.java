/*--------------------------------------------------------
DimInwardDischargeBLMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Kitti Pongsirisakun 23/11/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dim;

public class DimInwardDischargeBLMod {
    private String blNo;
    private String pol;
    private String pod;
    private String podTerminal;
    private String containerCode;    
    private String containerCodeDesc;  
    private String containerSts;
    private String blHead;;
    private String summary;
    public DimInwardDischargeBLMod() {
    
    this.blNo = "";
    this.pol = "";
    this.pod = "";
    this.podTerminal = "";
    this.containerCode = "";
    this.containerSts = "";
    this.blHead = "";
    this.summary = "";
    this.containerCodeDesc = "";
    
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setContainerSts(String containerSts) {
        this.containerSts = containerSts;
    }

    public String getContainerSts() {
        return containerSts;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public void setBlHead(String blHead) {
        this.blHead = blHead;
    }

    public String getBlHead() {
        return blHead;
    }

    public void setContainerCode(String containerCode) {
        this.containerCode = containerCode;
    }

    public String getContainerCode() {
        return containerCode;
    }

    public void setContainerCodeDesc(String containerCodeDesc) {
        this.containerCodeDesc = containerCodeDesc;
    }

    public String getContainerCodeDesc() {
        return containerCodeDesc;
    }
}
