/*------------------------------------------------------
QtnExportToExcelSurchargeMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2008
--------------------------------------------------------
 Author Kitti Pongsirisakun 01/12/2008 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class QtnExportToExcelSurchargeMod extends RrcStandardMod{

    private String surchargeCode;
    private double surchargeValue;
    private String surcharePlaceOfSettle;
    private String surchargeCur;
    private String surchargeInc;
    private String surchargeSubToChg;
    
    public QtnExportToExcelSurchargeMod() {
        surchargeCode = "";
        surchargeValue = 0;
        surcharePlaceOfSettle = "";
        surchargeCur = "";
        surchargeInc = "";
        surchargeSubToChg = "";
    }

    public void setSurchargeCode(String surchargeCode) {
        this.surchargeCode = surchargeCode;
    }

    public String getSurchargeCode() {
        return surchargeCode;
    }

    public void setSurchargeValue(double surchargeValue) {
        this.surchargeValue = surchargeValue;
    }

    public double getSurchargeValue() {
        return surchargeValue;
    }

    public void setSurcharePlaceOfSettle(String surcharePlaceOfSettle) {
        this.surcharePlaceOfSettle = surcharePlaceOfSettle;
    }

    public String getSurcharePlaceOfSettle() {
        return surcharePlaceOfSettle;
    }

    public void setSurchargeCur(String surchargeCur) {
        this.surchargeCur = surchargeCur;
    }

    public String getSurchargeCur() {
        return surchargeCur;
    }

    public void setSurchargeInc(String surchargeInc) {
        this.surchargeInc = surchargeInc;
    }

    public String getSurchargeInc() {
        return surchargeInc;
    }

    public void setSurchargeSubToChg(String surchargeSubToChg) {
        this.surchargeSubToChg = surchargeSubToChg;
    }

    public String getSurchargeSubToChg() {
        return surchargeSubToChg;
    }
}
