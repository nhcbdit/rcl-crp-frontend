package com.rclgroup.dolphin.web.model.dim;

import java.util.List;

public class DimManifestBLPrintExcelMod {
    private String vesselCallSign; 
    private String voyageNo;
    private String carrierCode;
    private String portOfDischargeCode;
    private String portOfDischargeSuffix;
    private String estimateDateOfArrival;
    private String cyOperator;
    private String retentionClassification;
    private String blNo;
    private String portOfLoadingCode;
    private String finalDestinationCode;
    private String finalDestinationName;
    private String placeOfTransferCode;
    private String placeOfTransferName;
    private String consignorName;
    private String consignorAddress;
    private String consignorPostCode;
    private String consignorCountryCode;
    private String consignorTelephoneNo;
    private String consigneeName;
    private String consigneeAddress;
    private String consigneePostCode;
    private String consigneeCountryCode;
    private String consigneeTelephoneNo;
    private String notifyPartyName1;
    private String notifyPartyAddress1;
    private String notifyPartyPostCode1;
    private String notifyPartyCountry1;
    private String notifyPartyTelephone1;
    private String notifyPartyName2;
    private String notifyPartyAddress2;
    private String notifyPartyPostCode2;
    private String notifyPartyCountry2;
    private String notifyPartyTelephone2;
    private String freightCharges;
    private String freightCurrencyCode;
    private String transship;
    private String transshipReason;
    private String transshipDuration;
    private String remarks;
    private String descriptionOfGoods;
    private String specialCargoSign;
    private String numberOfPackage;
    private String unitCodeOfPackage;
    private String grossWeight;
    private String unitCodeOfWeight;
    private String netWeight;
    private String unitCodeOfNet;
    private String measurement;
    private String unitCodeOfMeasurement;
    private String marksAndNos;
    private List<DimManifestBLPrintContainerExcelMod> containerList;

    public void setVesselCallSign(String vesselCallSign) {
        this.vesselCallSign = vesselCallSign;
    }

    public String getVesselCallSign() {
        return vesselCallSign;
    }

    public void setVoyageNo(String voyageNo) {
        this.voyageNo = voyageNo;
    }

    public String getVoyageNo() {
        return voyageNo;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setPortOfDischargeCode(String portOfDischargeCode) {
        this.portOfDischargeCode = portOfDischargeCode;
    }

    public String getPortOfDischargeCode() {
        return portOfDischargeCode;
    }

    public void setPortOfDischargeSuffix(String portOfDischargeSuffix) {
        this.portOfDischargeSuffix = portOfDischargeSuffix;
    }

    public String getPortOfDischargeSuffix() {
        return portOfDischargeSuffix;
    }

    public void setEstimateDateOfArrival(String estimateDateOfArrival) {
        this.estimateDateOfArrival = estimateDateOfArrival;
    }

    public String getEstimateDateOfArrival() {
        return estimateDateOfArrival;
    }

    public void setCyOperator(String cyOperator) {
        this.cyOperator = cyOperator;
    }

    public String getCyOperator() {
        return cyOperator;
    }

    public void setRetentionClassification(String retentionClassification) {
        this.retentionClassification = retentionClassification;
    }

    public String getRetentionClassification() {
        return retentionClassification;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setPortOfLoadingCode(String portOfLoadingCode) {
        this.portOfLoadingCode = portOfLoadingCode;
    }

    public String getPortOfLoadingCode() {
        return portOfLoadingCode;
    }

    public void setFinalDestinationCode(String finalDestinationCode) {
        this.finalDestinationCode = finalDestinationCode;
    }

    public String getFinalDestinationCode() {
        return finalDestinationCode;
    }

    public void setFinalDestinationName(String finalDestinationName) {
        this.finalDestinationName = finalDestinationName;
    }

    public String getFinalDestinationName() {
        return finalDestinationName;
    }

    public void setPlaceOfTransferCode(String placeOfTransferCode) {
        this.placeOfTransferCode = placeOfTransferCode;
    }

    public String getPlaceOfTransferCode() {
        return placeOfTransferCode;
    }

    public void setPlaceOfTransferName(String placeOfTransferName) {
        this.placeOfTransferName = placeOfTransferName;
    }

    public String getPlaceOfTransferName() {
        return placeOfTransferName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorPostCode(String consignorPostCode) {
        this.consignorPostCode = consignorPostCode;
    }

    public String getConsignorPostCode() {
        return consignorPostCode;
    }

    public void setConsignorCountryCode(String consignorCountryCode) {
        this.consignorCountryCode = consignorCountryCode;
    }

    public String getConsignorCountryCode() {
        return consignorCountryCode;
    }

    public void setConsignorTelephoneNo(String consignorTelephoneNo) {
        this.consignorTelephoneNo = consignorTelephoneNo;
    }

    public String getConsignorTelephoneNo() {
        return consignorTelephoneNo;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneePostCode(String consigneePostCode) {
        this.consigneePostCode = consigneePostCode;
    }

    public String getConsigneePostCode() {
        return consigneePostCode;
    }

    public void setConsigneeCountryCode(String consigneeCountryCode) {
        this.consigneeCountryCode = consigneeCountryCode;
    }

    public String getConsigneeCountryCode() {
        return consigneeCountryCode;
    }

    public void setConsigneeTelephoneNo(String consigneeTelephoneNo) {
        this.consigneeTelephoneNo = consigneeTelephoneNo;
    }

    public String getConsigneeTelephoneNo() {
        return consigneeTelephoneNo;
    }

    public void setNotifyPartyName1(String notifyPartyName1) {
        this.notifyPartyName1 = notifyPartyName1;
    }

    public String getNotifyPartyName1() {
        return notifyPartyName1;
    }

    public void setNotifyPartyAddress1(String notifyPartyAddress1) {
        this.notifyPartyAddress1 = notifyPartyAddress1;
    }

    public String getNotifyPartyAddress1() {
        return notifyPartyAddress1;
    }

    public void setNotifyPartyPostCode1(String notifyPartyPostCode1) {
        this.notifyPartyPostCode1 = notifyPartyPostCode1;
    }

    public String getNotifyPartyPostCode1() {
        return notifyPartyPostCode1;
    }

    public void setNotifyPartyCountry1(String notifyPartyCountry1) {
        this.notifyPartyCountry1 = notifyPartyCountry1;
    }

    public String getNotifyPartyCountry1() {
        return notifyPartyCountry1;
    }

    public void setNotifyPartyTelephone1(String notifyPartyTelephone1) {
        this.notifyPartyTelephone1 = notifyPartyTelephone1;
    }

    public String getNotifyPartyTelephone1() {
        return notifyPartyTelephone1;
    }

    public void setNotifyPartyName2(String notifyPartyName2) {
        this.notifyPartyName2 = notifyPartyName2;
    }

    public String getNotifyPartyName2() {
        return notifyPartyName2;
    }

    public void setNotifyPartyAddress2(String notifyPartyAddress2) {
        this.notifyPartyAddress2 = notifyPartyAddress2;
    }

    public String getNotifyPartyAddress2() {
        return notifyPartyAddress2;
    }

    public void setNotifyPartyPostCode2(String notifyPartyPostCode2) {
        this.notifyPartyPostCode2 = notifyPartyPostCode2;
    }

    public String getNotifyPartyPostCode2() {
        return notifyPartyPostCode2;
    }

    public void setNotifyPartyCountry2(String notifyPartyCountry2) {
        this.notifyPartyCountry2 = notifyPartyCountry2;
    }

    public String getNotifyPartyCountry2() {
        return notifyPartyCountry2;
    }

    public void setNotifyPartyTelephone2(String notifyPartyTelephone2) {
        this.notifyPartyTelephone2 = notifyPartyTelephone2;
    }

    public String getNotifyPartyTelephone2() {
        return notifyPartyTelephone2;
    }

    public void setFreightCharges(String freightCharges) {
        this.freightCharges = freightCharges;
    }

    public String getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCurrencyCode(String freightCurrencyCode) {
        this.freightCurrencyCode = freightCurrencyCode;
    }

    public String getFreightCurrencyCode() {
        return freightCurrencyCode;
    }

    public void setTransship(String transship) {
        this.transship = transship;
    }

    public String getTransship() {
        return transship;
    }

    public void setTransshipReason(String transshipReason) {
        this.transshipReason = transshipReason;
    }

    public String getTransshipReason() {
        return transshipReason;
    }

    public void setTransshipDuration(String transshipDuration) {
        this.transshipDuration = transshipDuration;
    }

    public String getTransshipDuration() {
        return transshipDuration;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setDescriptionOfGoods(String descriptionOfGoods) {
        this.descriptionOfGoods = descriptionOfGoods;
    }

    public String getDescriptionOfGoods() {
        return descriptionOfGoods;
    }

    public void setSpecialCargoSign(String specialCargoSign) {
        this.specialCargoSign = specialCargoSign;
    }

    public String getSpecialCargoSign() {
        return specialCargoSign;
    }

    public void setNumberOfPackage(String numberOfPackage) {
        this.numberOfPackage = numberOfPackage;
    }

    public String getNumberOfPackage() {
        return numberOfPackage;
    }

    public void setUnitCodeOfPackage(String unitCodeOfPackage) {
        this.unitCodeOfPackage = unitCodeOfPackage;
    }

    public String getUnitCodeOfPackage() {
        return unitCodeOfPackage;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setUnitCodeOfWeight(String unitCodeOfWeight) {
        this.unitCodeOfWeight = unitCodeOfWeight;
    }

    public String getUnitCodeOfWeight() {
        return unitCodeOfWeight;
    }

    public void setNetWeight(String netWeight) {
        this.netWeight = netWeight;
    }

    public String getNetWeight() {
        return netWeight;
    }

    public void setUnitCodeOfNet(String unitCodeOfNet) {
        this.unitCodeOfNet = unitCodeOfNet;
    }

    public String getUnitCodeOfNet() {
        return unitCodeOfNet;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setUnitCodeOfMeasurement(String unitCodeOfMeasurement) {
        this.unitCodeOfMeasurement = unitCodeOfMeasurement;
    }

    public String getUnitCodeOfMeasurement() {
        return unitCodeOfMeasurement;
    }

    public void setMarksAndNos(String marksAndNos) {
        this.marksAndNos = marksAndNos;
    }

    public String getMarksAndNos() {
        return marksAndNos;
    }

    public void setContainerList(List<DimManifestBLPrintContainerExcelMod> containerList) {
        this.containerList = containerList;
    }

    public List<DimManifestBLPrintContainerExcelMod> getContainerList() {
        return containerList;
    }
}
