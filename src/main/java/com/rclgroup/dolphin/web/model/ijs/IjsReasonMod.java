package com.rclgroup.dolphin.web.model.ijs;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class IjsReasonMod extends RrcStandardMod {
    private String reasonCode;
    private String desc;
    private String category;    
    private String status;
    public IjsReasonMod() {
        reasonCode = "";
        desc = "";
        category = "";
        status = "";
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
