package com.rclgroup.dolphin.web.model.vms;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class VmsAgencyCommissionExceptionMod extends RrcStandardMod {
    
    private String userId;
    private int sessionId; 
    private String reportType;
    private String reportSum;
    private String excType;
    private String excOpt;
    private String agent;
    private String fsc;
    private int dateFrom;
    private int dateTo;
    

    public VmsAgencyCommissionExceptionMod() {
        userId = "";
        sessionId = 0;
        reportType = "";
        reportSum = "";
        excType = "";
        excOpt = "";
        agent = "";
        fsc = "";
        dateFrom = 0;
        dateTo = 0;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setExcType(String excType) {
        this.excType = excType;
    }

    public String getExcType() {
        return excType;
    }

    public void setExcOpt(String excOpt) {
        this.excOpt = excOpt;
    }

    public String getExcOpt() {
        return excOpt;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setDateFrom(int dateFrom) {
        this.dateFrom = dateFrom;
    }

    public int getDateFrom() {
        return dateFrom;
    }

    public void setDateTo(int dateTo) {
        this.dateTo = dateTo;
    }

    public int getDateTo() {
        return dateTo;
    }

    public void setReportSum(String reportSum) {
        this.reportSum = reportSum;
    }

    public String getReportSum() {
        return reportSum;
    }
}
