package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosApprovalInternalPSEditMod extends RrcStandardMod{
    
    private String proformaRef;
    private String vendorCode;
    private String vendorName;
    
    private String activity;
    private String changeCode;
    private String remark;
    private String size;
    private String type;
    private String unit;
    private String adjustUnit;
    private String currency;
    private String tariffRate;
    private String tariffAmount;
    private String actualCostRate;
    private String actualCostAmount;
    private int tosProSeq;
    private String tosProRef;
    private String approveStatus; 
    
    public TosApprovalInternalPSEditMod() {
        super();
    }


    public void setProformaRef(String proformaRef) {
        this.proformaRef = proformaRef;
    }

    public String getProformaRef() {
        return proformaRef;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivity() {
        return activity;
    }

    public void setChangeCode(String changeCode) {
        this.changeCode = changeCode;
    }

    public String getChangeCode() {
        return changeCode;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setAdjustUnit(String adjustUnit) {
        this.adjustUnit = adjustUnit;
    }

    public String getAdjustUnit() {
        return adjustUnit;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setTariffRate(String tariffRate) {
        this.tariffRate = tariffRate;
    }

    public String getTariffRate() {
        return tariffRate;
    }

    public void setTariffAmount(String tariffAmount) {
        this.tariffAmount = tariffAmount;
    }

    public String getTariffAmount() {
        return tariffAmount;
    }

    public void setActualCostRate(String actualCostRate) {
        this.actualCostRate = actualCostRate;
    }

    public String getActualCostRate() {
        return actualCostRate;
    }

    public void setActualCostAmount(String actualCostAmount) {
        this.actualCostAmount = actualCostAmount;
    }

    public String getActualCostAmount() {
        return actualCostAmount;
    }

    public void setTosProSeq(int tosProSeq) {
        this.tosProSeq = tosProSeq;
    }

    public int getTosProSeq() {
        return tosProSeq;
    }

    public void setTosProRef(String tosProRef) {
        this.tosProRef = tosProRef;
    }

    public String getTosProRef() {
        return tosProRef;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getApproveStatus() {
        return approveStatus;
    }
}
