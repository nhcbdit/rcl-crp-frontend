/*-----------------------------------------------------------------------------------------------------------  
CamUtilityHeaderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 08/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamUtilityHeaderMod extends RrcStandardMod {
    
    private String utilityHdrId;
    private String moduleCode;
    private String country;
    private String countryName;
    private String screenCode;
    private String fsc;
    private String utilityName;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public CamUtilityHeaderMod() {
        super();
        utilityHdrId = "";
        moduleCode = "";
        country = "";
        countryName = "";
        screenCode = "";
        fsc = "";
        utilityName = "";
        recordAddDate = null;
        recordChangeDate = null;
    }

    public CamUtilityHeaderMod(CamUtilityHeaderMod bean) {
        super(bean.getRecordStatus(), bean.getRecordAddUser(), bean.getRecordChangeUser());
        this.utilityHdrId = bean.getUtilityHdrId();
        this.moduleCode = bean.getModuleCode();
        this.country = bean.getCountry();
        this.countryName = bean.getCountryName();
        this.screenCode = bean.getScreenCode();
        this.fsc = bean.getFsc();
        this.utilityName = bean.getUtilityName();
        this.recordAddDate = bean.getRecordAddDate();
        this.recordChangeDate = bean.getRecordChangeDate();
    }

    public boolean equals(Object object) {
        if (object instanceof CamUtilityHeaderMod) {
            CamUtilityHeaderMod mod = (CamUtilityHeaderMod) object;
            if (!this.utilityHdrId.equals(mod.getUtilityHdrId())) {
                return false;
            } else if (!this.moduleCode.equals(mod.getModuleCode())) {
                return false;
            } else if (!this.country.equals(mod.getCountry())) {
                return false;
            } else if (!this.screenCode.equals(mod.getScreenCode())) {
                return false;
            } else if (!this.fsc.equals(mod.getFsc())) {
                return false;
            } else if (!this.utilityName.equals(mod.getUtilityName())) {
                return false;
            } else if (!this.recordStatus.equals(mod.getRecordStatus())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public String getUtilityHdrId() {
        return utilityHdrId;
    }

    public void setUtilityHdrId(String utilityHdrId) {
        this.utilityHdrId = utilityHdrId;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getScreenCode() {
        return screenCode;
    }

    public void setScreenCode(String screenCode) {
        this.screenCode = screenCode;
    }

    public String getFsc() {
        return fsc;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getUtilityName() {
        return utilityName;
    }

    public void setUtilityName(String utilityName) {
        this.utilityName = utilityName;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}


