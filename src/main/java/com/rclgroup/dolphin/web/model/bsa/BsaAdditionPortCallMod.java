/*-----------------------------------------------------------------------------------------------------------  
BsaPortCallMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 26/05/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Timestamp;

public class BsaAdditionPortCallMod extends RrcStandardMod {

    private String portCallID;        // PORTCALL ID
    private String variantID;         // SERVICE VARIANT ID
    private String addPort;              // PORT
    private String addLoadDisFlag;           // LOAD DISCH FLAG
    private String addTranShipFlag;          // TRANSHIPMENT FLAG
    private String addTolelant;           // TOLELANT
    private String addSlotTeu;           // SLOT TEU
    private String addSlotTon;           // SLOT TON
    private String addMinTeu;            // MIN TEU LEVEL
    private String flag;              // INSERT,UPDATE,DELTE
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;
    
    public BsaAdditionPortCallMod cloneObject(Object object) {
        BsaAdditionPortCallMod mod = null;
        if (object != null && object instanceof BsaAdditionPortCallMod) {
            BsaAdditionPortCallMod bean = (BsaAdditionPortCallMod) object;
            mod = new BsaAdditionPortCallMod();
            mod.setPortCallID(bean.getPortCallID());
            mod.setVariantID(bean.getVariantID());
            mod.setAddPort(bean.getAddPort());
            mod.setAddLoadDisFlag(bean.getAddLoadDisFlag());
            mod.setAddTranShipFlag(bean.getAddTranShipFlag());
            mod.setAddTolelant(bean.getAddTolelant());
            mod.setAddSlotTeu(bean.getAddSlotTeu());
            mod.setAddSlotTon(bean.getAddSlotTon());
            mod.setAddMinTeu(bean.getAddMinTeu());
            mod.setRecordStatus(bean.getRecordStatus());
            mod.setRecordAddUser(bean.getRecordAddUser());
            mod.setRecordAddDate(bean.getRecordAddDate());
            mod.setRecordChangeUser(bean.getRecordChangeUser());
            mod.setRecordChangeDate(bean.getRecordChangeDate());         
        }
        return mod;
    }

    public BsaAdditionPortCallMod() {
         this("0","0",
              "",
              "","",
              "0.0","0","0",
              "0",
              "", RrcStandardMod.RECORD_STATUS_ACTIVE,
               "",new Timestamp(0),"",new Timestamp(0));
    }
    public BsaAdditionPortCallMod(String portCallID,String variantID,
           String addPort,
           String addLoadDisFlag,  String addTranShipFlag, 
           String addTolelant, String addSlotTeu, String addSlotTon,
           String addMinTeu,  
           String flag, String recordStatus,
           String recordAddUser, Timestamp recordAddDate, String recordChangeUser, Timestamp recordChangeDate) 
    {
        super(recordStatus, recordAddUser, recordChangeUser);
        this.portCallID       = portCallID;
        this.variantID        = variantID;
        this.addPort             = addPort;
        this.addLoadDisFlag      = addLoadDisFlag;
        this.addTranShipFlag     = addTranShipFlag;
        this.addTolelant         = addTolelant;
        this.addSlotTeu          = addSlotTeu;
        this.addSlotTon          = addSlotTon;
        this.addMinTeu          = addMinTeu;  
        this.flag             = flag;
        this.recordAddDate    = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public BsaAdditionPortCallMod(BsaAdditionPortCallMod mod) {
       this(mod.getPortCallID(),
            mod.getVariantID(),
            mod.getAddPort(),  
            mod.getAddLoadDisFlag(),
            mod.getAddTranShipFlag(),
            mod.getAddTolelant(),
            mod.getAddSlotTeu(),
            mod.getAddSlotTon(),
            mod.getAddMinTeu(),
            mod.getFlag(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate());
    }
    public boolean equals(Object object) {
        if (object instanceof BsaAdditionPortCallMod) {
            BsaAdditionPortCallMod mod = (BsaAdditionPortCallMod)object;
            if (!this.portCallID.equals(mod.getPortCallID())) {
                return false;
            } else if (!this.variantID.equals(mod.getVariantID())) {
                return false;
            }else if (!this.addPort.equals(mod.getAddPort())) {
                return false;
            } else if (!this.addTolelant.equals(mod.getAddTolelant())) {
                return false;   
            } else if (!this.addSlotTeu.equals(mod.getAddSlotTeu())) {
                return false;
            } else if (!this.addSlotTon.equals(mod.getAddSlotTon())) {
                return false;
            } else if (!this.addMinTeu.equals(mod.getAddMinTeu())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
   

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setPortCallID(String portCallID) {
        this.portCallID = portCallID;
    }

    public String getPortCallID() {
        return portCallID;
    }

    public void setVariantID(String variantID) {
        this.variantID = variantID;
    }

    public String getVariantID() {
        return variantID;
    }


    public void setAddPort(String addPort) {
        this.addPort = addPort;
    }

    public String getAddPort() {
        return addPort;
    }

    public void setAddTolelant(String addTolelant) {
        this.addTolelant = addTolelant;
    }

    public String getAddTolelant() {
        return addTolelant;
    }

    public void setAddSlotTeu(String addSlotTeu) {
        this.addSlotTeu = addSlotTeu;
    }

    public String getAddSlotTeu() {
        return addSlotTeu;
    }

    public void setAddSlotTon(String addSlotTon) {
        this.addSlotTon = addSlotTon;
    }

    public String getAddSlotTon() {
        return addSlotTon;
    }

    public void setAddMinTeu(String addMinTeu) {
        this.addMinTeu = addMinTeu;
    }

    public String getAddMinTeu() {
        return addMinTeu;
    }

    public void setAddLoadDisFlag(String addLoadDisFlag) {
        this.addLoadDisFlag = addLoadDisFlag;
    }

    public String getAddLoadDisFlag() {
        return addLoadDisFlag;
    }

    public void setAddTranShipFlag(String addTranShipFlag) {
        this.addTranShipFlag = addTranShipFlag;
    }

    public String getAddTranShipFlag() {
        return addTranShipFlag;
    }
}
