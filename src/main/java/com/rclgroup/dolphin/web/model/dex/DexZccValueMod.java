package com.rclgroup.dolphin.web.model.dex;

public class DexZccValueMod {
    public DexZccValueMod() {
    }
    private String zccCode;
    private String zccDescription;

    public void setZccCode(String zccCode) {
        this.zccCode = zccCode;
    }

    public String getZccCode() {
        return zccCode;
    }

    public void setZccDescription(String zccDescription) {
        this.zccDescription = zccDescription;
    }

    public String getZccDescription() {
        return zccDescription;
    }
}
