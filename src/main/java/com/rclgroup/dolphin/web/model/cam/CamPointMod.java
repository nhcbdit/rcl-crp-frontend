/*------------------------------------------------------
CamPointMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 18/07/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamPointMod extends RrcStandardMod {
    private String PointCode;
    private String PointName;
    private String PointZone;
    private String PointControlZone;
    private String PointArea;
    private String PointRegion;
    private String PointCountryCode;
    private String PointStatus;
   
    public CamPointMod() {
        PointCode = "";
        PointName = "";
        PointZone = "";
        PointControlZone = "";
        PointArea = "";
        PointRegion = "";
        PointCountryCode = "";
        PointStatus = "";
    }

    public void setPointCode(String pointCode) {
        this.PointCode = pointCode;
    }

    public String getPointCode() {
        return PointCode;
    }

    public void setPointName(String pointName) {
        this.PointName = pointName;
    }

    public String getPointName() {
        return PointName;
    }

    public void setPointZone(String pointZone) {
        this.PointZone = pointZone;
    }

    public String getPointZone() {
        return PointZone;
    }

    public void setPointControlZone(String pointControlZone) {
        this.PointControlZone = pointControlZone;
    }

    public String getPointControlZone() {
        return PointControlZone;
    }

    public void setPointArea(String pointArea) {
        this.PointArea = pointArea;
    }

    public String getPointArea() {
        return PointArea;
    }

    public void setPointRegion(String pointRegion) {
        this.PointRegion = pointRegion;
    }

    public String getPointRegion() {
        return PointRegion;
    }

    public void setPointCountryCode(String pointCountryCode) {
        this.PointCountryCode = pointCountryCode;
    }

    public String getPointCountryCode() {
        return PointCountryCode;
    }

    public void setPointStatus(String pointStatus) {
        this.PointStatus = pointStatus;
    }

    public String getPointStatus() {
        return PointStatus;
    }
}
