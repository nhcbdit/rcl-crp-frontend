/*-----------------------------------------------------------------------------------------------------------  
VssProformaVesselAssignmentMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 18/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.vss;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class VssProformaVesselAssignmentMod extends RrcStandardMod {
    private String vssVesselAssignmentId; // PK_VSS_VESSEL_ASSIGNMENT_ID
    private String vssProformaId; // FK_VSS_PROFORMA_ID
    private String vesselSeqNo; // VESSEL_SEQ_NO
    private String vesselCode; // FK_VESSEL_CODE
    private String vesselName; // Read only vessel name for vessel code
    private String bsaVesselType; // BSA_VESSEL_TYPE
    private String validFrom; // VALID_FROM
    private String validTo; // VALID_TO
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    
    public VssProformaVesselAssignmentMod cloneObject(Object object) {
        VssProformaVesselAssignmentMod mod = null;
        if (object != null && object instanceof VssProformaVesselAssignmentMod) {
            VssProformaVesselAssignmentMod bean = (VssProformaVesselAssignmentMod) object;
            mod = new VssProformaVesselAssignmentMod();
            mod.setVssVesselAssignmentId(bean.getVssVesselAssignmentId());
            mod.setVssProformaId(bean.getVssProformaId());
            mod.setVesselSeqNo(bean.getVesselSeqNo());
            mod.setVesselCode(bean.getVesselCode());
            mod.setVesselName(bean.getVesselName());
            mod.setBsaVesselType(bean.getBsaVesselType());
            mod.setValidFrom(bean.getValidFrom());
            mod.setValidTo(bean.getValidTo());
            mod.setRecordAddUser(bean.getRecordAddUser());
            mod.setRecordAddDate(bean.getRecordAddDate());
            mod.setRecordChangeUser(bean.getRecordChangeUser());
            mod.setRecordChangeDate(bean.getRecordChangeDate());
            mod.setRecordStatus(bean.getRecordStatus());
        }
        return mod;
    }

    public VssProformaVesselAssignmentMod(String vssVesselAssignmentId,
                                          String vssProformaId,
                                          String vesselSeqNo,
                                          String vesselCode,
                                          String vesselName, 
                                          String bsaVesselType,
                                          String validFrom,
                                          String validTo,
                                          String recordStatus,
                                          String recordAddUser,
                                          Timestamp recordAddDate,
                                          String recordChangeUser,
                                          Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.vssVesselAssignmentId = vssVesselAssignmentId;
        this.vssProformaId = vssProformaId;
        this.vesselSeqNo = vesselSeqNo;
        this.vesselCode = vesselCode;     
        this.vesselName = vesselName;
        this.bsaVesselType = bsaVesselType;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
     
    public VssProformaVesselAssignmentMod() {
        this("0","","","","","","","",RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0));
    }
    
    public VssProformaVesselAssignmentMod(VssProformaVesselAssignmentMod mod){
       this(mod.getVssVesselAssignmentId(),
            mod.getVssProformaId(),
            mod.getVesselSeqNo(),
            mod.getVesselCode(),
            mod.getVesselName(),
            mod.getBsaVesselType(),
            mod.getValidFrom(),
            mod.getValidTo(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate());
    }
    
    public boolean equals(Object object){
        if(object instanceof VssProformaVesselAssignmentMod){
            VssProformaVesselAssignmentMod mod = (VssProformaVesselAssignmentMod)object;
            if(!this.vssVesselAssignmentId.equals(mod.getVssVesselAssignmentId())){
                return false;
            }else if(!this.vssProformaId.equals(mod.getVssProformaId())){
                return false;
            }else if(!this.vesselSeqNo.equals(mod.getVesselSeqNo())){
                return false;
            }else if(!this.vesselCode.equals(mod.getVesselCode())){
                return false;
            }else if(!this.bsaVesselType.equals(mod.getBsaVesselType())){
                return false;
            }else if(!this.validFrom.equals(mod.getValidFrom())){
                return false;
            }else if(!this.validTo.equals(mod.getValidTo())){
                return false;
            }else if(!this.recordStatus.equals(mod.getRecordStatus())){
                return false;
            }
        }else{
            return false;
        }
        return true;
    } 

    public void setVssVesselAssignmentId(String vssVesselAssignmentId) {
        this.vssVesselAssignmentId = vssVesselAssignmentId;
    }

    public String getVssVesselAssignmentId() {
        return vssVesselAssignmentId;
    }

    public void setVssProformaId(String vssProformaId) {
        this.vssProformaId = vssProformaId;
    }

    public String getVssProformaId() {
        return vssProformaId;
    }

    public void setVesselSeqNo(String vesselSeqNo) {
        this.vesselSeqNo = vesselSeqNo;
    }

    public String getVesselSeqNo() {
        return vesselSeqNo;
    }

    public void setVesselCode(String vesselCode) {
        this.vesselCode = vesselCode;
    }

    public String getVesselCode() {
        return vesselCode;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setBsaVesselType(String bsaVesselType) {
        this.bsaVesselType = bsaVesselType;
    }

    public String getBsaVesselType() {
        return bsaVesselType;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}


