package com.rclgroup.dolphin.web.model.dim;

public class DimManifestBLPrintContainerExcelMod {
    private String blNo;
    private String specialCargo;
    private String containerInformation;
    private String equipmentNo;
    private String sealNo;
    private String fullEmpty;
    private String equipSize;
    private String equipType;
    private String transferType;
    private String ownershipType;
    private String vanningType;

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setContainerInformation(String containerInformation) {
        this.containerInformation = containerInformation;
    }

    public String getContainerInformation() {
        return containerInformation;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public void setSealNo(String sealNo) {
        this.sealNo = sealNo;
    }

    public String getSealNo() {
        return sealNo;
    }

    public void setFullEmpty(String fullEmpty) {
        this.fullEmpty = fullEmpty;
    }

    public String getFullEmpty() {
        return fullEmpty;
    }

    public void setEquipSize(String equipSize) {
        this.equipSize = equipSize;
    }

    public String getEquipSize() {
        return equipSize;
    }

    public void setEquipType(String equipType) {
        this.equipType = equipType;
    }

    public String getEquipType() {
        return equipType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setOwnershipType(String ownershipType) {
        this.ownershipType = ownershipType;
    }

    public String getOwnershipType() {
        return ownershipType;
    }

    public void setVanningType(String vanningType) {
        this.vanningType = vanningType;
    }

    public String getVanningType() {
        return vanningType;
    }

    public void setSpecialCargo(String specialCargo) {
        this.specialCargo = specialCargo;
    }

    public String getSpecialCargo() {
        return specialCargo;
    }
}
