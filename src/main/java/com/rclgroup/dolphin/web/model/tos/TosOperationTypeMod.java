package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosOperationTypeMod extends RrcStandardMod {
    private String oprCode;
    private String oprDescr;
    
    public TosOperationTypeMod() {
        super();
        oprCode = "";
        oprDescr = "";
    }
     
    public TosOperationTypeMod(String oprCode, String oprDescr) {        
        this.oprCode = oprCode;
        this.oprDescr = oprDescr;
    }   

    public void setOprCode(String oprCode) {
        this.oprCode = oprCode;
    }

    public String getOprCode() {
        return oprCode;
    }

    public void setOprDescr(String oprDescr) {
        this.oprDescr = oprDescr;
    }

    public String getOprDescr() {
        return oprDescr;
    }
}
