/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotMaintenanceDetailMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 15/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.dao.ems.EmsEquipmentTypeDao;

import java.sql.Timestamp;

public class EmsPickUpReturnDepotMaintenanceDetailMod extends RrcStandardMod{
    private String multiDepotDtlId;
    private String fkMultiDepotHdrId;
    private String eqSize;
    private String eqType;
    private short eqTypeOk;
    private String status;    
    private Timestamp recordAddDate;    
    private Timestamp recordChangeDate;
    private String porrFlag;
    private String activityFlagOri; //I = insert, R = Read from DB, D = delete
    private String activityFlag; //I = insert, R = Read from DB, D = delete
    
    public static String ACTIVITY_INS = "I";
    public static String ACTIVITY_READ = "R";
    public static String ACTIVITY_DEL = "D";
    
    
    public static String SIZE_ALL = "**";
    public static String SIZE_20 = "20";
    public static String SIZE_40 = "40";
    public static String SIZE_45 = "45";
    protected static final short UNCHECKED = 0;
    protected static final short CHECKED_OK = 1;
    protected static final short CHECKED_NOT_OK = -1;
    
    private EmsEquipmentTypeDao emsEquipmentTypeDao;
    
    public EmsPickUpReturnDepotMaintenanceDetailMod(String multiDepotDtlId,   String fkMultiDepotHdrId,
                    String eqSize,         String eqType,                     short eqTypeOk,
                    String activityFlag,   String activityFlagOri,            String status,                                       
                    Timestamp recordAddDate, Timestamp recordChangeDate) {
        super();
        this.multiDepotDtlId = "";
        this.fkMultiDepotHdrId = "";        
        this.eqSize = eqSize;
        this.eqType = eqType;
        this.eqTypeOk = eqTypeOk;
        this.status = status;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
        this.porrFlag = "";
        this.activityFlag = activityFlag;
        this.activityFlagOri = activityFlagOri; 
    }
    
    public EmsPickUpReturnDepotMaintenanceDetailMod(EmsPickUpReturnDepotMaintenanceDetailMod mod){
         this(mod.multiDepotDtlId,mod.fkMultiDepotHdrId, mod.eqSize,mod.eqType,mod.eqTypeOk,mod.activityFlag,mod.activityFlagOri,mod.status,mod.recordAddDate,mod.recordChangeDate);
    }
    
    public EmsPickUpReturnDepotMaintenanceDetailMod() {
        this("","","","",UNCHECKED,"I","I","",new Timestamp(0),new Timestamp(0));
    }

    public void setEqSize(String eqSize) {
        this.eqSize = eqSize;
    }

    public String getEqSize() {
        return eqSize;
    }

    public void setEqType(String eqType) {
        this.eqType = eqType.toUpperCase();  
//        if(this.eqType!=null && this.eqType.length()>0){
//            boolean isValid = emsEquipmentTypeDao.isValid(this.eqType);
//            if (isValid) {
//                eqTypeOk = CHECKED_OK;
//            } else {
//                eqTypeOk = CHECKED_NOT_OK;
//            }  
//        } 
    }

    public String getEqType() {
        return eqType;
    }

    public void setMultiDepotDtlId(String multiDepotDtlId) {
        this.multiDepotDtlId = multiDepotDtlId;
    }

    public String getMultiDepotDtlId() {
        return multiDepotDtlId;
    }

    public void setFkMultiDepotHdrId(String fkMultiDepotHdrId) {
        this.fkMultiDepotHdrId = fkMultiDepotHdrId;
    }

    public String getFkMultiDepotHdrId() {
        return fkMultiDepotHdrId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setPorrFlag(String porrFlag) {
        this.porrFlag = porrFlag;
    }

    public String getPorrFlag() {
        return porrFlag;
    }

    public void setActivityFlagOri(String activityFlagOri) {
        this.activityFlagOri = activityFlagOri;
    }

    public String getActivityFlagOri() {
        return activityFlagOri;
    }

    public void setActivityFlag(String activityFlag) {
        this.activityFlag = activityFlag;
    }

    public String getActivityFlag() {
        return activityFlag;
    }

    public void setEmsEquipmentTypeDao(EmsEquipmentTypeDao emsEquipmentTypeDao) {
        this.emsEquipmentTypeDao = emsEquipmentTypeDao;
    }
    public boolean isEqTypeOk(){
    if (!eqType.equals("")){
        if(eqTypeOk == CHECKED_OK){
            return true;
        }else{
            return false;
        }        
    }else {return true;}
    }
}
