/*-----------------------------------------------------------------------------------------------------------  
CamUtilityDetailMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 08/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamUtilityDetailMod extends RrcStandardMod {
    
    private String utilityDtlId;
    private String utilityHdrId;
    private String detailName;
    private String utilityType;
    private String fileName;
    private String textMessage;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    private String fileUploadName;

    public CamUtilityDetailMod() {
        super();
        utilityDtlId = "";
        utilityHdrId = "";
        detailName = "";
        utilityType = "";
        fileName = "";
        textMessage = "";
        recordAddDate = null;
        recordChangeDate = null;
    }

    public CamUtilityDetailMod(CamUtilityDetailMod bean) {
        super(bean.getRecordStatus(), bean.getRecordAddUser(), bean.getRecordChangeUser());
        this.utilityDtlId = bean.getUtilityDtlId();
        this.utilityHdrId = bean.getUtilityHdrId();
        this.detailName = bean.getDetailName();
        this.utilityType = bean.getUtilityType();
        this.fileName = bean.getFileName();
        this.textMessage = bean.getTextMessage();
        this.recordAddDate = bean.getRecordAddDate();
        this.recordChangeDate = bean.getRecordChangeDate();
    }
    
    public boolean equals(Object object) {
        if (object instanceof CamUtilityDetailMod) {
            CamUtilityDetailMod mod = (CamUtilityDetailMod) object;
            if (!this.utilityDtlId.equals(mod.getUtilityDtlId())) {
                return false;
            } else if (!this.utilityHdrId.equals(mod.getUtilityHdrId())) {
                return false;
            } else if (!this.detailName.equals(mod.getDetailName())) {
                return false;
            } else if (!this.utilityType.equals(mod.getUtilityType())) {
                return false;
            } else if (!this.fileName.equals(mod.getFileName())) {
                return false;
            } else if (!this.textMessage.equals(mod.getTextMessage())) {
                return false;
            } else if (!this.recordStatus.equals(mod.getRecordStatus())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public String getUtilityDtlId() {
        return utilityDtlId;
    }

    public void setUtilityDtlId(String utilityDtlId) {
        this.utilityDtlId = utilityDtlId;
    }

    public String getUtilityHdrId() {
        return utilityHdrId;
    }

    public void setUtilityHdrId(String utilityHdrId) {
        this.utilityHdrId = utilityHdrId;
    }

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public String getUtilityType() {
        return utilityType;
    }

    public void setUtilityType(String utilityType) {
        this.utilityType = utilityType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public void setFileUploadName(String fileUploadName) {
        this.fileUploadName = fileUploadName;
    }

    public String getFileUploadName() {
        return fileUploadName;
    }
}


