/*-----------------------------------------------------------------------------------------------------------  
CamContentTypeMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 03/04/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Date;

public class CamContentTypeMod extends RrcStandardMod {
    private String code; // CONTENT_CODE 
    private String type; // CONTENT_TYPE
    private String description; // CONTENT_DESC
    private String moduleName; // MODULE_NAME
    private Date recordAddDate;
    private Date recordChangeDate;
     
    public CamContentTypeMod() {
        code = "";
        type = "";
        description = "";
        moduleName = "";
        recordAddDate = new Date(0);
        recordChangeDate = new Date(0);
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleName() {
        return moduleName;
    }
    
    public void setRecordAddDate(Date recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Date getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Date recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Date getRecordChangeDate() {
        return recordChangeDate;
    }
}

