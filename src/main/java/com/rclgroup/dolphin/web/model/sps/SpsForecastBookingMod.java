package com.rclgroup.dolphin.web.model.sps;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.dao.dex.DexVesselScheduleDao;

public class SpsForecastBookingMod extends RrcStandardMod {
   
    private String reportType;
    private String service;
    private String vessel;
    private String voyage;
    private String direction;     
    private String pol;
    private String fromDate;
    private String toDate;
    private String sessionId;
    private String userName;
    
    public SpsForecastBookingMod() {
    
    reportType= "";
    service   = "";
    vessel    = "";
    voyage    = "";
    direction = "";
    pol       = "";
    fromDate  = "";
    toDate    = "";
    sessionId = "";
    userName  = "";
    
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }


    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
