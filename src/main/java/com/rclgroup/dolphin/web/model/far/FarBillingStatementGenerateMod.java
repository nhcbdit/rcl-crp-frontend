/*--------------------------------------------------------
FarBillingStatementGenerateMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Wuttitorn Wuttijiaranai 05/08/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class FarBillingStatementGenerateMod extends RrcStandardMod {
    private long batchNumber;
    private String billNo;
    private int billVersion;
    private int billNoCurrent;

    public FarBillingStatementGenerateMod() {
        batchNumber = 0;
        billNo = "";
        billVersion = 0;
        billNoCurrent = 0;
    }

    public long getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(long batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public int getBillVersion() {
        return billVersion;
    }

    public void setBillVersion(int billVersion) {
        this.billVersion = billVersion;
    }
    
    public int getBillNoCurrent() {
        return billNoCurrent;
    }

    public void setBillNoCurrent(int billNoCurrent) {
        this.billNoCurrent = billNoCurrent;
    }

}
