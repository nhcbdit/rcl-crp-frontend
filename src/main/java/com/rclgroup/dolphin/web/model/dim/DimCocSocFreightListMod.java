package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimCocSocFreightListMod extends RrcStandardMod {
    private String sessionId;
    private String username;
    private String recordAddUser;
    private String fromDate;
    private String toDate;
    private String service;
    private String vessle;
    private String voyage;
    private String line;
    private String region;
    private String agent;
    private String collFsc;
    private String cocSoc;
    private String pol;
    private String pod;
    private String bl;
    private String polTer;
    private String direction;
    private String bound;
    private String revenue;
    private String blStatus;
    private String fscCodeOfUser;
    private String report;
    private String searchBy;
    private String invoyageSessionId;
    
    public DimCocSocFreightListMod() {
        sessionId="";
        username="";
        recordAddUser="";
        fromDate="";
        toDate="";
        service="";
        vessle="";
        voyage="";
        line="";
        region="";
        agent="";
        collFsc="";
        cocSoc="";
        pol="";
        pod="";
        bl="";
        polTer="";
        direction="";
        fscCodeOfUser="";
        report="";
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setRecordAddUser(String recordAddUser) {
        this.recordAddUser = recordAddUser;
    }

    public String getRecordAddUser() {
        return recordAddUser;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessle(String vessle) {
        this.vessle = vessle;
    }

    public String getVessle() {
        return vessle;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }
    
    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setBound(String bound) {
        this.bound = bound;
    }

    public String getBound() {
        return bound;
    }

    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }

    public String getRevenue() {
        return revenue;
    }

    public void setBlStatus(String blStatus) {
        this.blStatus = blStatus;
    }

    public String getBlStatus() {
        return blStatus;
    }

    public void setCollFsc(String collFsc) {
        this.collFsc = collFsc;
    }

    public String getCollFsc() {
        return collFsc;
    }

    public void setFscCodeOfUser(String fscCodeOfUser) {
        this.fscCodeOfUser = fscCodeOfUser;
    }

    public String getFscCodeOfUser() {
        return fscCodeOfUser;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getReport() {
        return report;
    }

    public void setPolTer(String polTer) {
        this.polTer = polTer;
    }

    public String getPolTer() {
        return polTer;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setInvoyageSessionId(String invoyageSessionId) {
        this.invoyageSessionId = invoyageSessionId;
    }

    public String getInvoyageSessionId() {
        return invoyageSessionId;
    }
}
