/*-----------------------------------------------------------------------------------------------------------  
CamRseScriptTosQtnBackdatingMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 18/04/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import java.sql.Timestamp;


public class CamRseScriptTosQtnBackdatingMod extends CamRseScriptMod {
    
    private String qtnno;
    private String effdate;
  
    
    public CamRseScriptTosQtnBackdatingMod() {
        super();
    }
    
    public CamRseScriptTosQtnBackdatingMod(String qtnno, String effdate, String flagExecute, String returnResult, Timestamp recordChangeDate) {
        super(flagExecute, returnResult, recordChangeDate);
        this.qtnno = qtnno;
        this.effdate = effdate;        
    }
    
    public CamRseScriptTosQtnBackdatingMod(String qtnno, String effdate) {
        super();
        this.qtnno = qtnno;
        this.effdate = effdate; 
    }

    public String getQtnno() {
        return qtnno;
    }

    public void setQtnno(String qtnno) {
        this.qtnno = qtnno;
    }

    public String getEffdate() {
        return effdate;
    }

    public void setEffdate(String effdate) {
        this.effdate = effdate;
    }    

}

