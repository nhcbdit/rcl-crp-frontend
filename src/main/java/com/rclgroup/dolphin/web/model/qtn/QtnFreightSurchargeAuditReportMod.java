/*------------------------------------------------------
QtnFreightSurchargeAuditReportMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2008
--------------------------------------------------------
 Author Kitti Pongsirisakun 01/12/2008 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef-        -ShortDescription-
01 12/12/13 RAJA    PD_CR_20131129     Modify Freight Surcharge Audit Report
02 06/05/14 RAJA    PD_CR_20140311     Ancillary Charge Summary Report
04 25/05/15 SARAWUT Ticket No 427773   Add 2 variable for select more fileds idp_013.BSEFUL and idp_013.killed_slots
05 16/01/17 ONSINEE Ticket No          Add Column oFrtUsd and Modify Script oFrt

--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class QtnFreightSurchargeAuditReportMod extends RrcStandardMod {
    public QtnFreightSurchargeAuditReportMod() {
    }
    
    private String userId;
    private String sessionId;
    private String reportBy;
    private String etdFrom;
    private String etdTo;
    private String service;
    private String vessel;
    private String voyage;
    private String direction;
    private String cocSoc;
    private String pol;
    private String polTerminal;
    private String line;
    private String region;
    private String agent;
    private String fsc;
    private String surchargeCode;// format xxx,xxx,xxx
    
    // for serach searchQtnFreightSurchargeAuditData
    private String etdDate;
    private String quotationNum;
    private String contractParty;
    private String shipperName;
    private String cneeName;
    private String BlNum;
    private String svc;
    private String vsl;
    private String voy;
    private String por;
    private String polQtn;
    private String pod;
    private String del;
    private String containerSize;
    private String containerType;
    private String commodity;
    private String commodityGroup;
    private String ladenEmpty;
    private String shipmentTerm;
    private String volumnUnit;
    private String volumnTeus;
    private String OFrt;
    private String OFrtUsd; // ##05
    private String surchargeCodeQtn;
    private String rate;
    private String countSurchargeCode;
    private String currency;
    private String commodityDesc;//##01
    // start : 02
    private String podTerminal;
    private String etaFrom;
    private String etaTo;
    private String amount;
    private String weight;
    private String rank; 
    private String etaDate;
    private String CommodityGroupCode;
    private String CommodityGroupDesc;
    private String amount_usd;
    private String laden;
    private String voidSlot;
    
   
   // end : 02 
   
    
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setReportBy(String reportBy) {
        this.reportBy = reportBy;
    }

    public String getReportBy() {
        return reportBy;
    }

    public void setEtdFrom(String etdFrom) {
        this.etdFrom = etdFrom;
    }

    public String getEtdFrom() {
        return etdFrom;
    }

    public void setEtdTo(String etdTo) {
        this.etdTo = etdTo;
    }

    public String getEtdTo() {
        return etdTo;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }
    
    public void setPolTerminal(String polTerminal) {
        this.polTerminal = polTerminal;
    }

    public String getPolTerminal() {
        return polTerminal;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setSurchargeCode(String surchargeCode) {
        this.surchargeCode = surchargeCode;
    }

    public String getSurchargeCode() {
        return surchargeCode;
    }

    public void setQuotationNum(String quotationNum) {
        this.quotationNum = quotationNum;
    }

    public String getQuotationNum() {
        return quotationNum;
    }

    public void setContractParty(String contractParty) {
        this.contractParty = contractParty;
    }

    public String getContractParty() {
        return contractParty;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setCneeName(String cneeName) {
        this.cneeName = cneeName;
    }

    public String getCneeName() {
        return cneeName;
    }

    public void setBlNum(String blNum) {
        this.BlNum = blNum;
    }

    public String getBlNum() {
        return BlNum;
    }

    public void setSvc(String svc) {
        this.svc = svc;
    }

    public String getSvc() {
        return svc;
    }

    public void setVsl(String vsl) {
        this.vsl = vsl;
    }

    public String getVsl() {
        return vsl;
    }

    public void setVoy(String voy) {
        this.voy = voy;
    }

    public String getVoy() {
        return voy;
    }

    public void setPor(String por) {
        this.por = por;
    }

    public String getPor() {
        return por;
    }

    public void setPolQtn(String polQtn) {
        this.polQtn = polQtn;
    }

    public String getPolQtn() {
        return polQtn;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setDel(String del) {
        this.del = del;
    }

    public String getDel() {
        return del;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodityGroup(String commodityGroup) {
        this.commodityGroup = commodityGroup;
    }

    public String getCommodityGroup() {
        return commodityGroup;
    }

    public void setLadenEmpty(String ladenEmpty) {
        this.ladenEmpty = ladenEmpty;
    }

    public String getLadenEmpty() {
        return ladenEmpty;
    }

    public void setShipmentTerm(String shipmentTerm) {
        this.shipmentTerm = shipmentTerm;
    }

    public String getShipmentTerm() {
        return shipmentTerm;
    }

    public void setVolumnUnit(String volumnUnit) {
        this.volumnUnit = volumnUnit;
    }

    public String getVolumnUnit() {
        return volumnUnit;
    }

    public void setVolumnTeus(String volumnTeus) {
        this.volumnTeus = volumnTeus;
    }

    public String getVolumnTeus() {
        return volumnTeus;
    }

    public void setOFrt(String oFrt) {
        this.OFrt = oFrt;
    }

    public String getOFrt() {
        return OFrt;
    }
    //BEGIN ##05
    public void setOFrtUsd(String oFrtUsd) {
        this.OFrtUsd = oFrtUsd;
    }

    public String getOFrtUsd() {
        return OFrtUsd;
    }
    //END ##05
    public void setSurchargeCodeQtn(String surchargeCodeQtn) {
        this.surchargeCodeQtn = surchargeCodeQtn;
    }

    public String getSurchargeCodeQtn() {
        return surchargeCodeQtn;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setCountSurchargeCode(String countSurchargeCode) {
        this.countSurchargeCode = countSurchargeCode;
    }

    public String getCountSurchargeCode() {
        return countSurchargeCode;
    }

    public void setEtdDate(String etdDate) {
        this.etdDate = etdDate;
    }

    public String getEtdDate() {
        return etdDate;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCommodityDesc(String commodityDesc) {
        this.commodityDesc = commodityDesc;
    }

    public String getCommodityDesc() {
        return commodityDesc;
    }
 
    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setEtaFrom(String etaFrom) {
        this.etaFrom = etaFrom;
    }

    public String getEtaFrom() {
        return etaFrom;
    }

    public void setEtaTo(String etaTo) {
        this.etaTo = etaTo;
    }

    public String getEtaTo() {
        return etaTo;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

   

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getRank() {
        return rank;
    }


    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight() {
        return weight;
    }

    public void setEtaDate(String etaDate) {
        this.etaDate = etaDate;
    }

    public String getEtaDate() {
        return etaDate;
    }

    public void setCommodityGroupCode(String commodityGroupCode) {
        this.CommodityGroupCode = commodityGroupCode;
    }

    public String getCommodityGroupCode() {
        return CommodityGroupCode;
    }

    public void setCommodityGroupDesc(String commodityGroupDesc) {
        this.CommodityGroupDesc = commodityGroupDesc;
    }

    public String getCommodityGroupDesc() {
        return CommodityGroupDesc;
    }

    public void setAmount_usd(String amount_usd) {
        this.amount_usd = amount_usd;
    }

    public String getAmount_usd() {
        return amount_usd;
    }

    public void setLaden(String laden) {
        this.laden = laden;
    }

    public String getLaden() {
        return laden;
    }

    public void setVoidSlot(String voidSlot) {
        this.voidSlot = voidSlot;
    }

    public String getVoidSlot() {
        return voidSlot;
    }
}
