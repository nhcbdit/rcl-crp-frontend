package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamPeriodParameterMod extends RrcStandardMod {
    
    private String periodParameterId;
    private String periodParameterCode;
    private String noOfDay;
    
    private String recordChangeDateStr;
    
   // private boolean isDelete;
    
    public CamPeriodParameterMod() {
        periodParameterId ="";
        periodParameterCode ="";
        noOfDay ="";
        recordChangeDateStr ="";
    }


    public void setPeriodParameterId(String periodParameterId) {
        this.periodParameterId = periodParameterId;
    }

    public String getPeriodParameterId() {
        return periodParameterId;
    }

    public void setPeriodParameterCode(String periodParameterCode) {
        this.periodParameterCode = periodParameterCode;
    }

    public String getPeriodParameterCode() {
        return periodParameterCode;
    }

    public void setNoOfDay(String noOfDay) {
        this.noOfDay = noOfDay;
    }

    public String getNoOfDay() {
        return noOfDay;
    }

    public void setRecordChangeDateStr(String recordChangeDateStr) {
        this.recordChangeDateStr = recordChangeDateStr;
    }

    public String getRecordChangeDateStr() {
        return recordChangeDateStr;
    }

  /*  public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isDelete() {
        return isDelete;
    }
    */
}
