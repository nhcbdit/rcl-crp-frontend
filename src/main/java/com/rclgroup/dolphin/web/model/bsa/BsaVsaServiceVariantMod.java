/*-----------------------------------------------------------------------------------------------------------  
VsaVsaServiceVariantMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class BsaVsaServiceVariantMod extends RrcStandardMod {
    
    private String vsaServiceVariantId; // BSA_SERVICE_VARIANT_ID
    private String vsaModelId;          // BSA_MODEL_ID 
    private String service;             // SERVICE
    private String variantCode;         // VARIANT_CODE
    private String proformaId;          // PROFORMA_ID
    private String vsaVesselType;       // BSA_VESSEL_TYPE
    private String noVessels;           // NO_VESSELS
    private String rotationDuration;    // ROTATION_DURATION
    private String usageRule;           // USAGE_RULE
    private String calcFrequency;       // CALC_FREQUENCY
    private String calcDuration;        // CALC_DURATION
    private String defSlotTeu;          // DEF_SLOT_TEU
    private String defSlotTons;         // DEF_SLOT_TONS
    private String defSlotReefer;       // DEF_SLOT_REEFER
    private String defAvgCocTeuWeight;  // DEF_AVG_COC_TEU_WEIGHT
    private String defAvgSocTeuWeight;  // DEF_AVG_SOC_TEU_WEIGHT
    private String defMinTeu;           // DEF_MIN_TEU
    private String overbookingPerc;     // OVERBOOKING_PERC
    private String refVariant;          // REFERENCE VARIANT
    private String refVesselType;       // REFERENCE VESSEL TYPE
    private String effectiveDate;       // EFFECTIVE DATE
    private String expireDate;          // EXPIRE DATE   
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;
    
    private String modelName;
    private String proformaRefNo;
    
    private String vessel;
    private String vesselName;
    private String voyage;
    private String direction;

    public BsaVsaServiceVariantMod() {
        this("0","0","","","","","0","0","","0","0","","","","","","","0.0","","","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0),"","","","","","");
    }

    public BsaVsaServiceVariantMod(String vsaServiceVariantId, String vsaModelId, String service,
        String variantCode, String proformaId, String vsaVesselType, String noVessels, 
        String rotationDuration, String usageRule, String calcFrequency, String calcDuration, 
        String defSlotTeu, String defSlotTons, String defSlotReefer, String defAvgCocTeuWeight,
        String defAvgSocTeuWeight, String defMinTeu, String overbookingPerc,
        String refVariant, String refVesselType, String effectiveDate, String expireDate, String recordStatus, 
        String recordAddUser, Timestamp recordAddDate, String recordChangeUser, Timestamp recordChangeDate,
        String modelName, String proformaRefNo, String vessel, String vesselName, String voyage, String direction) 
    {
        super(recordStatus, recordAddUser, recordChangeUser);
        this.vsaServiceVariantId = vsaServiceVariantId;
        this.vsaModelId = vsaModelId;
        this.service = service;
        this.variantCode = variantCode;
        this.proformaId = proformaId;
        this.vsaVesselType = vsaVesselType;
        this.noVessels = noVessels;
        this.rotationDuration = rotationDuration;
        this.usageRule = usageRule;
        this.calcFrequency = calcFrequency;
        this.calcDuration = calcDuration;
        this.defSlotTeu = defSlotTeu;
        this.defSlotTons = defSlotTons;
        this.defSlotReefer = defSlotReefer;
        this.defAvgCocTeuWeight = defAvgCocTeuWeight;
        this.defAvgSocTeuWeight = defAvgSocTeuWeight;
        this.defMinTeu = defMinTeu;
        this.overbookingPerc = overbookingPerc;        
        this.refVariant = refVariant;
        this.refVesselType = refVesselType;
        this.effectiveDate = effectiveDate;
        this.expireDate = expireDate;      
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
        this.modelName = modelName;
        this.proformaRefNo = proformaRefNo;        
        this.vessel = vessel;
        this.vesselName = vesselName;
        this.voyage = voyage;
        this.direction = direction;
    }
    
    public BsaVsaServiceVariantMod(BsaVsaServiceVariantMod mod) {
       this(mod.getVsaServiceVariantId(),
            mod.getVsaModelId(),
            mod.getService(),
            mod.getVariantCode(),
            mod.getProformaId(),
            mod.getVsaVesselType(),
            mod.getNoVessels(),
            mod.getRotationDuration(),
            mod.getUsageRule(),
            mod.getCalcFrequency(),
            mod.getCalcDuration(),            
            mod.getDefSlotTeu(), 
            mod.getDefSlotTons(),
            mod.getDefSlotReefer(),
            mod.getDefAvgCocTeuWeight(),
            mod.getDefAvgSocTeuWeight(),
            mod.getDefMinTeu(),
            mod.getOverbookingPerc(),           
            mod.getRefVariant(),
            mod.getRefVesselType(),
            mod.getEffectiveDate(),
            mod.getExpireDate(),                      
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate(),
            mod.getModelName(),
            mod.getProformaRefNo(),
            mod.getVessel(),
            mod.getVesselName(),
            mod.getVoyage(),
            mod.getDirection() );
    }
    public boolean equals(Object object) {
        if (object instanceof BsaVsaServiceVariantMod) {
            BsaVsaServiceVariantMod mod = (BsaVsaServiceVariantMod)object;
            if (!this.vsaServiceVariantId.equals(mod.getVsaServiceVariantId())) {
                return false;
            } else if (!this.vsaModelId.equals(mod.getVsaModelId())) {
                return false;
            } else if (!this.service.equals(mod.getService())) {
                return false;
            } else if (!this.variantCode.equals(mod.getVariantCode())) {
                return false;
            } else if (!this.proformaId.equals(mod.getProformaId())) {
                return false;
            } else if (!this.vsaVesselType.equals(mod.getVsaVesselType())) {
                return false;
            } else if (!this.noVessels.equals(mod.getNoVessels())) {
                return false;
            } else if (!this.rotationDuration.equals(mod.getRotationDuration())) {
                return false;
            } else if (!this.usageRule.equals(mod.getUsageRule())) {
                return false;
            } else if (!this.calcFrequency.equals(mod.getCalcFrequency())) {
                return false;
            } else if (!this.calcDuration.equals(mod.getCalcDuration())) {
                return false;
            } else if (!this.defSlotTeu.equals(mod.getDefSlotTeu())) {
                return false;
            } else if (!this.defSlotTons.equals(mod.getDefSlotTons())) {
                return false;
            } else if (!this.defSlotReefer.equals(mod.getDefSlotReefer())) {
                return false;
            } else if (!this.defAvgCocTeuWeight.equals(mod.getDefAvgCocTeuWeight())) {
                return false;
            } else if (!this.defAvgSocTeuWeight.equals(mod.getDefAvgSocTeuWeight())) {
                return false;
            } else if (!this.defMinTeu.equals(mod.getDefMinTeu())) {
                return false;
            } else if (this.overbookingPerc.equals(mod.getOverbookingPerc())) {
                return false;               
            } else if (!this.refVariant.equals(mod.getRefVariant())) {
                    return false;
            } else if (!this.refVesselType.equals(mod.getRefVesselType())) {
                    return false;
            } else if (!this.effectiveDate.equals(mod.getEffectiveDate())) {
                    return false;
            } else if (this.expireDate.equals(mod.getExpireDate())) {
                    return false;                    
            } else if (!this.recordAddDate.equals(mod.getRecordAddDate())) {
                return false;
            } else if (!this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                return false;
            } else if (!this.modelName.equals(mod.getModelName())) {
                return false;
            } else if (!this.proformaRefNo.equals(mod.getProformaRefNo())) {
                return false;
            } else if(!this.vessel.equals(mod.getVessel())){
                return false;
            } else if(!this.vesselName.equals(mod.getVesselName())){
                return false;
            } else if(!this.voyage.equals(mod.getVoyage())){
                return false;
            } else if(!this.direction.equals(mod.getDirection())){
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    public String getVsaServiceVariantId() {
        return vsaServiceVariantId;
    }

    public void setVsaServiceVariantId(String vsaServiceVariantId) {
        this.vsaServiceVariantId = vsaServiceVariantId;
    }

    public String getVsaModelId() {
        return vsaModelId;
    }

    public void setVsaModelId(String vsaModelId) {
        this.vsaModelId = vsaModelId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getVariantCode() {
        return variantCode;
    }

    public void setVariantCode(String variantCode) {
        this.variantCode = variantCode;
    }

    public String getProformaId() {
        return proformaId;
    }

    public void setProformaId(String proformaId) {
        this.proformaId = proformaId;
    }

    public String getVsaVesselType() {
        return vsaVesselType;
    }

    public void setVsaVesselType(String vsaVesselType) {
        this.vsaVesselType = vsaVesselType;
    }

    public String getNoVessels() {
        return noVessels;
    }

    public void setNoVessels(String noVessels) {
        this.noVessels = noVessels;
    }

    public String getRotationDuration() {
        return rotationDuration;
    }

    public void setRotationDuration(String rotationDuration) {
        this.rotationDuration = rotationDuration;
    }

    public String getUsageRule() {
        return usageRule;
    }

    public void setUsageRule(String usageRule) {
        this.usageRule = usageRule;
    }

    public String getCalcFrequency() {
        return calcFrequency;
    }

    public void setCalcFrequency(String calcFrequency) {
        this.calcFrequency = calcFrequency;
    }

    public String getCalcDuration() {
        return calcDuration;
    }

    public void setCalcDuration(String calcDuration) {
        this.calcDuration = calcDuration;
    }

    public String getDefSlotTeu() {
        return defSlotTeu;
    }

    public void setDefSlotTeu(String defSlotTeu) {
        this.defSlotTeu = defSlotTeu;
    }

    public String getDefSlotTons() {
        return defSlotTons;
    }

    public void setDefSlotTons(String defSlotTons) {
        this.defSlotTons = defSlotTons;
    }

    public String getDefSlotReefer() {
        return defSlotReefer;
    }

    public void setDefSlotReefer(String defSlotReefer) {
        this.defSlotReefer = defSlotReefer;
    }

    public String getDefAvgCocTeuWeight() {
        return defAvgCocTeuWeight;
    }

    public void setDefAvgCocTeuWeight(String defAvgCocTeuWeight) {
        this.defAvgCocTeuWeight = defAvgCocTeuWeight;
    }

    public String getDefAvgSocTeuWeight() {
        return defAvgSocTeuWeight;
    }

    public void setDefAvgSocTeuWeight(String defAvgSocTeuWeight) {
        this.defAvgSocTeuWeight = defAvgSocTeuWeight;
    }

    public String getDefMinTeu() {
        return defMinTeu;
    }

    public void setDefMinTeu(String defMinTeu) {
        this.defMinTeu = defMinTeu;
    }

    public String getOverbookingPerc() {
        return overbookingPerc;
    }
    
    public void setOverbookingPerc(String overbookingPerc) {
        this.overbookingPerc = overbookingPerc;
    }
    
    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getProformaRefNo() {
        return proformaRefNo;
    }

    public void setProformaRefNo(String proformaRefNo) {
        this.proformaRefNo = proformaRefNo;
    }

    public void setRefVariant(String refVariant) {
        this.refVariant = refVariant;
    }

    public String getRefVariant() {
        return refVariant;
    }

    public void setRefVesselType(String refVesselType) {
        this.refVesselType = refVesselType;
    }

    public String getRefVesselType() {
        return refVesselType;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getExpireDate() {
        return expireDate;
    }
    
    public void setVessel(String vessel){
        this.vessel = vessel;
    }
    
    public String getVessel(){
        return vessel;
    }
    
    public void setVesselName(String vesselName){
        this.vesselName = vesselName;
    }
    
    public String getVesselName(){
        return vesselName;
    }
    
    public void setVoyage(String voyage){
        this.voyage = voyage;
    }
    
    public String getVoyage(){
        return voyage;
    }
    
    public void setDirection(String direction){
        this.direction = direction;
    }
    
    public String getDirection(){
        return direction;
    }
}

