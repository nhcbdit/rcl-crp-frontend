/*--------------------------------------------------------
FarPoMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author -
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
01  22/09/14       BHAMOH1   382          Add properties invoiceDate, invoicePrintDate
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class FarPoMod extends RrcStandardMod {
    private String fsc;                 //COLL_FSC
    private String billToParty;         //BILL_TO_CUSTOMER
    private String billToPartyName;     //CUNAME
    private String etaEtd;              //SAIL_DATE
    private String service;             //SERVICE
    private String vessel;              //VESSEL
    private String voyage;              //VOYAGE
    private String pol;                 //PORT_OF_LOAD
    private String pod;                 //PORT_OF_DISCHARGE
    private String blNo;                //APP_REFERENCE
    private String invNo;               //INVOICE_NO
    private String poNo;                //CUST_REFERENCE
    private String inOutXt;             //IB_OB_XT
    private String remarks;             //REMARKS
    private String invoiceDate;         //INVOICE_DATE              //#01 BHAMOH1
    private String invoicePrintDate;    //INVOICE_PRINTING_DATE     //#01 BHAMOH1
    
    
    public FarPoMod() {
        this.fsc = "";
        this.billToParty = "";
        this.etaEtd = "";
        this.service = "";
        this.vessel = "";
        this.voyage = "";
        this.pol = "";
        this.pod = "";
        this.blNo = "";
        this.invNo = "";
        this.poNo = "";
        this.inOutXt = "";
        this.remarks = "";   
        this.invoiceDate = "";
        this.invoicePrintDate = "";
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setBillToPartyName(String billToPartyName) {
        this.billToPartyName = billToPartyName;
    }

    public String getBillToPartyName() {
        return billToPartyName;
    }

    public void setEtaEtd(String etaEtd) {
        this.etaEtd = etaEtd;
    }

    public String getEtaEtd() {
        return etaEtd;
    }

    public void setService(String service) {
        this.service = service;
    }
    
    
    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getPoNo() {
        return poNo;
    }

    public void setInOutXt(String inOutXt) {
        this.inOutXt = inOutXt;
    }

    public String getInOutXt() {
        return inOutXt;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoicePrintDate(String invoicePrintDate) {
        this.invoicePrintDate = invoicePrintDate;
    }

    public String getInvoicePrintDate() {
        return invoicePrintDate;
    }
}
