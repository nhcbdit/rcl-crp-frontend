/*-----------------------------------------------------------------------------------------------------------  
DimManifestMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 19/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimManifestMod extends RrcStandardMod {
    
    private String booking;
    private String blNo;
    private String blId;
    private String hblNo;
    private int blCreationDate;
    private String exportStatus;
    private String importStatus;
    private String porCode;
    private String polCode;
    private String potCode;
    private String podCode;
    private String delCode;
    private String finalDelCode;
    private String service;
    private String voyageCode;
    private String vesselCode;
    private String direction;
    private String consigneeCode;
    private String dischargeTerminal;
    
    public DimManifestMod() {
        booking = "";
        blNo = "";
        blId = "";
        hblNo = "";
        exportStatus = "";
        importStatus = "";
        porCode = "";
        polCode = "";
        potCode = "";
        podCode = "";
        delCode = "";
        finalDelCode = "";
        service = "";
        voyageCode = "";
        vesselCode = "";
        direction = "";
        consigneeCode = "";
        dischargeTerminal = "";
    }

    public void setBooking(String booking) {
        this.booking = booking;
    }

    public String getBooking() {
        return booking;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setHblNo(String hblNo) {
        this.hblNo = hblNo;
    }

    public String getHblNo() {
        return hblNo;
    }
    
    public void setExportStatus(String exportStatus) {
        this.exportStatus = exportStatus;
    }

    public String getExportStatus() {
        return exportStatus;
    }

    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    public String getImportStatus() {
        return importStatus;
    }

    public void setBlId(String blId) {
        this.blId = blId;
    }

    public String getBlId() {
        return blId;
    }

    public void setPolCode(String polCode) {
        this.polCode = polCode;
    }

    public String getPolCode() {
        return polCode;
    }

    public void setPotCode(String potCode) {
        this.potCode = potCode;
    }

    public String getPotCode() {
        return potCode;
    }

    public void setDelCode(String delCode) {
        this.delCode = delCode;
    }

    public String getDelCode() {
        return delCode;
    }

    public void setPodCode(String podCode) {
        this.podCode = podCode;
    }

    public String getPodCode() {
        return podCode;
    }

    public void setPorCode(String porCode) {
        this.porCode = porCode;
    }

    public String getPorCode() {
        return porCode;
    }

    public void setBlCreationDate(int blCreationDate) {
        this.blCreationDate = blCreationDate;
    }

    public int getBlCreationDate() {
        return blCreationDate;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVoyageCode(String voyageCode) {
        this.voyageCode = voyageCode;
    }

    public String getVoyageCode() {
        return voyageCode;
    }

    public void setVesselCode(String vesselCode) {
        this.vesselCode = vesselCode;
    }

    public String getVesselCode() {
        return vesselCode;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setFinalDelCode(String finalDelCode) {
        this.finalDelCode = finalDelCode;
    }

    public String getFinalDelCode() {
        return finalDelCode;
    }

    public void setDischargeTerminal(String dischargeTerminal) {
        this.dischargeTerminal = dischargeTerminal;
    }

    public String getDischargeTerminal() {
        return dischargeTerminal;
    }
}
