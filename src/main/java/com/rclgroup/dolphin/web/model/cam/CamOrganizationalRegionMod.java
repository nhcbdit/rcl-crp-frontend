/*-----------------------------------------------------------------------------------------------------------  
CamOrganizationalRegionMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 24/10/2007  
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 24/04/07  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamOrganizationalRegionMod extends RrcStandardMod {
    private String lineCode;
    private String regionCode; // = trade
    private String regionName; // = company name
    private String status; // = record status

    public CamOrganizationalRegionMod() {
        lineCode = "";
        regionCode = "";
        regionName = "";
        status = "";
    }
 
    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;
    }

    public String getLineCode() {
        return lineCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
