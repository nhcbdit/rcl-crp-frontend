/*-----------------------------------------------------------------------------------------------------------  
TosReopenLoadListMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 24/02/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

import java.util.Calendar;


public class TosReopenLoadListMod extends RrcStandardMod {
    
    private String vessel;
    private String voyage;
    private String pol;
    private String polTerminal;
    private String flagExecute;
    private String returnResult;
    private Timestamp recordChangeDate;
    
    public TosReopenLoadListMod() {
        super();
    }
    
    public TosReopenLoadListMod(String vessel, String voyage, String pol, String polTerminal, String flagExecute, String returnResult, Timestamp recordChangeDate) {
        this.vessel = vessel;
        this.voyage = voyage;
        this.pol = pol;
        this.polTerminal = polTerminal;
        this.flagExecute = flagExecute;
        this.returnResult = returnResult;
        this.recordChangeDate = recordChangeDate;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPolTerminal() {
        return polTerminal;
    }

    public void setPolTerminal(String polTerminal) {
        this.polTerminal = polTerminal;
    }

    public String getFlagExecute() {
        return flagExecute;
    }

    public void setFlagExecute(String flagExecute) {
        this.flagExecute = flagExecute;
    }

    public String getReturnResult() {
        return returnResult;
    }

    public void setReturnResult(String returnResult) {
        this.returnResult = returnResult;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}

