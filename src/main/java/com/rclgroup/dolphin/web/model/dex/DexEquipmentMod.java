/*-----------------------------------------------------------------------------------------------------------  
DexEquipmentMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 20/05/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.dex;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class DexEquipmentMod extends RrcStandardMod {
    
    private String equipmentNo;     // EQUIPMENT_NO
    private String category;        // CATEGORY
    private String equipmentSize;   // EQUIPMENT_SIZE
    private String equipmentType;   // EQUIPMENT_TYPE
    private String activity;        // ACTIVITY
    private String activityDate;    // CURRENT_ACTIVITY_DATE
    private String port;            // CURRENT_PORT
    private String status;          // CURRENT_STATUS
    
    public DexEquipmentMod() {
        super();
    }

    public DexEquipmentMod(String equipmentNo, String category, String equipmentSize, String equipmentType, String activity, String activityDate, String port, String status) {
        super();
        this.equipmentNo = equipmentNo;
        this.category = category;
        this.equipmentSize = equipmentSize;
        this.equipmentType = equipmentType;
        this.activity = activity;
        this.activityDate = activityDate;
        this.port = port;
        this.status = status;
    }
    
    public boolean equals(Object object) {
        if (object instanceof DexEquipmentMod) {
            DexEquipmentMod mod = (DexEquipmentMod) object;
            if (!this.equipmentNo.equals(mod.getEquipmentNo())) {
                return false;
            } else if (!this.category.equals(mod.getCategory())) {
                return false;
            } else if (!this.equipmentSize.equals(mod.getEquipmentSize())) {
                return false;
            } else if (!this.equipmentType.equals(mod.getEquipmentType())) {
                return false;
            } else if (!this.activity.equals(mod.getActivity())) {
                return false;
            } else if (!this.activityDate.equals(mod.getActivityDate())) {
                return false;
            } else if (!this.port.equals(mod.getPort())) {
                return false;
            } else if (!this.status.equals(mod.getStatus())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public String getEquipmentNo() {
        return equipmentNo;
    }

    public void setEquipmentNo(String equipmentNo) {
        this.equipmentNo = equipmentNo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEquipmentSize() {
        return equipmentSize;
    }

    public void setEquipmentSize(String equipmentSize) {
        this.equipmentSize = equipmentSize;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

