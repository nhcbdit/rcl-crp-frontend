package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamRseReportFacilityConfigSearchMod extends RrcStandardMod {
    private String serviceId;
    private String serviceCode;
    private String serviceName;
    private String moduleCode;
    private String moduleName;
    private String subtype;
    private String serviceFile;
    private String grpMandatoryCd;
    private String description;
    private String adhocProcName;
    private String fileFlag; 
    private String offlineFlag;
    private String serviceFileFormat;
    private String locPermission;
    private String locPermType;
    private String locPermCode;
    private String status;
    
    public CamRseReportFacilityConfigSearchMod() {
        serviceId = "";
        serviceCode = "";
        serviceName = "";
        moduleCode = "";
        moduleName = "";
        subtype = "";
        serviceFile = "";
        status = "";
        description = "";
        grpMandatoryCd = "";
        adhocProcName = "";
        fileFlag = "N";
        offlineFlag = "N";
        locPermission = "";
        locPermType = "";
        locPermCode = "";
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setServiceFile(String serviceFile) {
        this.serviceFile = serviceFile;
    }

    public String getServiceFile() {
        return serviceFile;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setGrpMandatoryCd(String grpMandatoryCd) {
        this.grpMandatoryCd = grpMandatoryCd;
    }

    public String getGrpMandatoryCd() {
        return grpMandatoryCd;
    }

    public void setAdhocProcName(String adhocProcName) {
        this.adhocProcName = adhocProcName;
    }

    public String getAdhocProcName() {
        return adhocProcName;
    }

    public void setFileFlag(String fileFlag) {
        this.fileFlag = fileFlag;
    }

    public String getFileFlag() {
        return fileFlag;
    }

    public void setOfflineFlag(String offlineFlag) {
        this.offlineFlag = offlineFlag;
}

    public String getOfflineFlag() {
        return offlineFlag;
    }

    public void setServiceFileFormat(String serviceFileFormat) {
        this.serviceFileFormat = serviceFileFormat;
    }

    public String getServiceFileFormat() {
        return serviceFileFormat;
    }
    
    public void setLocPermission(String locPermission) {
        this.locPermission = locPermission;
    }

    public String getLocPermission() {
        return locPermission;
    }

    public void setLocPermType(String locPermType) {
        this.locPermType = locPermType;
    }

    public String getLocPermType() {
        return locPermType;
    }

    public void setLocPermCode(String locPermCode) {
        this.locPermCode = locPermCode;
    }

    public String getLocPermCode() {
        return locPermCode;
    }
}
