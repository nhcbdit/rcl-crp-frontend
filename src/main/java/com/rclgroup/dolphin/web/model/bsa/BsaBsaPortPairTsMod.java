package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaBsaPortPairTsMod extends RrcStandardMod {
    private String bsaModelId; // BSA_MODEL_ID 
    private String tsIndic; // TS_INDIC
    private String bsaService; // SERVICE
    private String bsaVariant; // V_CODE
    private String bsaPort; // PORT_CODE
    private String bsaGroup; // GROUP_CODE
    private String bsaPortGroupFlag; // GROUP_FLAG    
    private String bsaSubGroupFlag; //SUB_GROUP_FLAG
    private String status;// RECORD_STATUS
    
    static final String SEARCH_TS_INDIC = "TS_INDIC";
    static final String SEARCH_SERVICE = "SERVICE";
    static final String SEARCH_VARIANT = "V_CODE";
    static final String SEARCH_PORT = "PORT_CODE";
    static final String SEARCH_GROUP = "GROUP_CODE";
    static final String SEARCH_STATUS = "RECODR_STATUS";

    public BsaBsaPortPairTsMod(String bsaModelId,
                          String tsIndic,
                          String bsaService,
                          String bsaVariant,
                          String bsaPort,
                          String bsaGroup,
                          String bsaPortGroupFlag,
                          String bsaSubGroupFlag,
                          String status) {        
        this.bsaModelId = bsaModelId;
        this.tsIndic = tsIndic;
        this.bsaService = bsaService;
        this.bsaVariant = bsaVariant;
        this.bsaPort = bsaPort;
        this.bsaGroup = bsaGroup;
        this.bsaPortGroupFlag = bsaPortGroupFlag;
        this.bsaSubGroupFlag = bsaSubGroupFlag;
        this.status = status;        
    }
    
    public BsaBsaPortPairTsMod() {
        this("","","","","","","","","");
    }
    
    public BsaBsaPortPairTsMod(BsaBsaPortPairTsMod mod){
       this(mod.getBsaModelId(),
            mod.getTsIndic(),
            mod.getBsaService(),
            mod.getBsaVariant(),
            mod.getBsaPort(),
            mod.getBsaGroup(),
            mod.getBsaPortGroupFlag(),
            mod.getBsaSubGroupFlag(),
            mod.getStatus());
    }
    
    public boolean equals(Object object){
        if(object instanceof BsaBsaPortPairTsMod){
            BsaBsaPortPairTsMod mod = (BsaBsaPortPairTsMod)object;
            if(!this.bsaModelId.equals(mod.getBsaModelId())){
                return false;
            }else if(!this.tsIndic.equals(mod.getTsIndic())){
                return false;
            }else if(!this.bsaService.equals(mod.getBsaService())){
                return false;
            }else if(!this.bsaVariant.equals(mod.getBsaVariant())){
                return false;
            }else if(!this.bsaPort.equals(mod.getBsaPort())){
                return false;
            }else if(!this.bsaGroup.equals(mod.getBsaGroup())){
                return false;
            }else if(!this.bsaPortGroupFlag.equals(mod.getBsaPortGroupFlag())){
                return false;        
            }else if(!this.bsaSubGroupFlag.equals(mod.getBsaSubGroupFlag())){
                return false;     
            }else if(!this.status.equals(mod.getStatus())){
                return false;   
            }
        }else{
            return false;
        }
        return true;
    } 
    
    public void setBsaModelId(String bsaModelId) {
        this.bsaModelId = bsaModelId;
    }

    public String getBsaModelId() {
        return bsaModelId;
    }

    public void setTsIndic(String tsIndic) {
        this.tsIndic = tsIndic;
    }

    public String getTsIndic() {
        return tsIndic;
    }

    public void setBsaService(String bsaService) {
        this.bsaService = bsaService;
    }

    public String getBsaService() {
        return bsaService;
    }

    public void setBsaVariant(String bsaVariant) {
        this.bsaVariant = bsaVariant;
    }

    public String getBsaVariant() {
        return bsaVariant;
    }

    public void setBsaPort(String bsaPort) {
        this.bsaPort = bsaPort;
    }

    public String getBsaPort() {
        return bsaPort;
    }

    public void setBsaGroup(String bsaGroup) {
        this.bsaGroup = bsaGroup;
    }

    public String getBsaGroup() {
        return bsaGroup;
    }

    public void setBsaPortGroupFlag(String bsaPortGroupFlag) {
        this.bsaPortGroupFlag = bsaPortGroupFlag;
    }

    public String getBsaPortGroupFlag() {
        return bsaPortGroupFlag;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBsaSubGroupFlag(String bsaSubGroupFlag) {
        this.bsaSubGroupFlag = bsaSubGroupFlag;
    }

    public String getBsaSubGroupFlag() {
        return bsaSubGroupFlag;
    }
}
