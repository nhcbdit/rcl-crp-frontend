package com.rclgroup.dolphin.web.model.dex;

public class DexBypassFsc {
    public DexBypassFsc() {
    }
    private String bypassFscName;
    private String bypassFscCountry;

    public void setBypassFscName(String bypassFscName) {
        this.bypassFscName = bypassFscName;
    }

    public String getBypassFscName() {
        return bypassFscName;
    }

    public void setBypassFscCountry(String bypassFscCountry) {
        this.bypassFscCountry = bypassFscCountry;
    }

    public String getBypassFscCountry() {
        return bypassFscCountry;
    }
}
