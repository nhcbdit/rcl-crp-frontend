package com.rclgroup.dolphin.web.model.dex;

public class DexBlFscMod {
    public DexBlFscMod() {
    }
    private String blCompany;
    private String responseFsc;
    private String blCountry;

    public void setBlCompany(String blCompany) {
        this.blCompany = blCompany;
    }

    public String getBlCompany() {
        return blCompany;
    }

    public void setResponseFsc(String responseFsc) {
        this.responseFsc = responseFsc;
    }

    public String getResponseFsc() {
        return responseFsc;
    }

    public void setBlCountry(String blCountry) {
        this.blCountry = blCountry;
    }

    public String getBlCountry() {
        return blCountry;
    }
}
