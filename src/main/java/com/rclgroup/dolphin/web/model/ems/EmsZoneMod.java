/*-----------------------------------------------------------------------------------------------------------  
EmsAreaMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Manop Wanngam 05/11/07 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 29/04/08  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/


 package com.rclgroup.dolphin.web.model.ems;

 import com.rclgroup.dolphin.web.common.RrcStandardMod;

 public class EmsZoneMod extends RrcStandardMod {
     private String zoneCode;
     private String zoneName;
     private String regionCode;
     private String areaCode;
     private String controlZoneCode;
     private String zoneStatus;

     public EmsZoneMod() {
         zoneCode = "";
         zoneName = "";
         regionCode = "";
         areaCode = "";
         controlZoneCode = "";
         zoneStatus = "";
     }
     
     public String getZoneCode() {
         return zoneCode;
     }

     public void setZoneCode(String zoneCode) {
         this.zoneCode = zoneCode;
     }

     public String getZoneName() {
         return zoneName;
     }

     public void setZoneName(String zoneName) {
         this.zoneName = zoneName;
     }

     public String getZoneStatus() {
         return zoneStatus;
     }

     public void setZoneStatus(String zoneStatus) {
         this.zoneStatus = zoneStatus;
     }

    public void setControlZoneCode(String controlZoneCode) {
        this.controlZoneCode = controlZoneCode;
    }

    public String getControlZoneCode() {
        return controlZoneCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return areaCode;
    }
}
