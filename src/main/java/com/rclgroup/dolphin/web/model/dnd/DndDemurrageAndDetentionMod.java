/*-----------------------------------------------------------------------------------------------------------  
DndDemurrageAndDetentionMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 22/02/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.dnd;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import java.sql.Timestamp;

public class DndDemurrageAndDetentionMod extends RrcStandardMod {

    private String blNo;
    private String sessionId;
    private Timestamp recordAddDate;
    
    public DndDemurrageAndDetentionMod() {
        this("","","",new Timestamp(0));
    }
    
    public DndDemurrageAndDetentionMod(String blNo,String sessionId,String recordAddUser,Timestamp recordAddDate){
        super("",recordAddUser,"");
        this.blNo = blNo;
        this.sessionId = sessionId;
        this.recordAddUser = recordAddUser;
        this.recordAddDate = recordAddDate;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }
}
