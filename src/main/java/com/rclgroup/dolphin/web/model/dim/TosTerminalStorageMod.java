package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosTerminalStorageMod extends RrcStandardMod {
    
    private String serviceCode;
    private String vesselCode;
    private String voyageCode;
    private String direction;
    private String portCode;
    private String terminalCode;
    private int ataDate;
    private int atdDate;
    private String billingStatus;
    
    public TosTerminalStorageMod() {
        serviceCode = "";
        vesselCode = "";
        voyageCode = "";
        portCode = "";
        terminalCode = "";
        ataDate = 0;
        atdDate = 0 ;
        direction = "";
        billingStatus ="";
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setVoyageCode(String voyageCode) {
        this.voyageCode = voyageCode;
    }

    public String getVoyageCode() {
        return voyageCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    public String getTerminalCode() {
        return terminalCode;
    }

    public void setVesselCode(String vesselCode) {
        this.vesselCode = vesselCode;
    }

    public String getVesselCode() {
        return vesselCode;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setAtaDate(int ataDate) {
        this.ataDate = ataDate;
    }

    public int getAtaDate() {
        return ataDate;
    }

    public void setAtdDate(int atdDate) {
        this.atdDate = atdDate;
    }

    public int getAtdDate() {
        return atdDate;
    }

    public void setBillingStatus(String billingStatus) {
        this.billingStatus = billingStatus;
    }

    public String getBillingStatus() {
        return billingStatus;
    }
}
