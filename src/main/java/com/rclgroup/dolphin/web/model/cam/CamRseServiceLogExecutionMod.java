/*-----------------------------------------------------------------------------------------------------------  
CamRseServiceLogExecutionMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 17/02/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseServiceLogExecutionMod extends RrcStandardMod {
    
    private String logId;
    private String serviceId;
    private String scheduleId;
    private String executeType;
    private String executeBy;
    private String executeDatetime;
    private String executeFile;
    private String generateFile;
    private String runDuration;
    private String eventCode;
    private String eventDescription;
    private String emailAddress;
    private String logStatus;
    private String errorReason;
    private String ticketNo;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public CamRseServiceLogExecutionMod() {
        super();

        logId = "";
        serviceId = "";
        scheduleId = "";
        executeType = "";
        executeBy = "";
        executeDatetime = "";
        executeFile = "";
        generateFile = "";
        runDuration = "";
        eventCode = "";
        eventDescription = "";
        emailAddress = "";
        logStatus = "";
        errorReason = "";
        ticketNo = "";
        recordAddDate = null;
        recordChangeDate = null;
    }

    public CamRseServiceLogExecutionMod(CamRseServiceLogExecutionMod copy) {
        super();
        if (copy != null) {
            this.logId = copy.getLogId();
            this.serviceId = copy.getServiceId();
            this.scheduleId = copy.getScheduleId();
            this.executeType = copy.getExecuteType();
            this.executeBy = copy.getExecuteBy();
            this.executeDatetime = copy.getExecuteDatetime();
            this.executeFile = copy.getExecuteFile();
            this.generateFile = copy.getGenerateFile();
            this.runDuration = copy.getRunDuration();
            this.eventCode = copy.getEventCode();
            this.eventDescription = copy.getEventDescription();
            this.emailAddress = copy.getEmailAddress();
            this.logStatus = copy.getLogStatus();
            this.errorReason = copy.getErrorReason();
            this.ticketNo = copy.getTicketNo();
            this.recordAddDate = copy.getRecordAddDate();
            this.recordChangeDate = copy.getRecordChangeDate();
            this.recordStatus = copy.getRecordStatus();
            this.recordAddUser = copy.getRecordAddUser();
            this.recordChangeUser = copy.getRecordChangeUser();
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public String getExecuteBy() {
        return executeBy;
    }

    public void setExecuteBy(String executeBy) {
        this.executeBy = executeBy;
    }

    public String getExecuteDatetime() {
        return executeDatetime;
    }

    public void setExecuteDatetime(String executeDatetime) {
        this.executeDatetime = executeDatetime;
    }

    public String getExecuteFile() {
        return executeFile;
    }

    public void setExecuteFile(String executeFile) {
        this.executeFile = executeFile;
    }

    public String getGenerateFile() {
        return generateFile;
    }

    public void setGenerateFile(String generateFile) {
        this.generateFile = generateFile;
    }

    public String getRunDuration() {
        return runDuration;
    }

    public void setRunDuration(String runDuration) {
        this.runDuration = runDuration;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(String logStatus) {
        this.logStatus = logStatus;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(String errorReason) {
        this.errorReason = errorReason;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}


