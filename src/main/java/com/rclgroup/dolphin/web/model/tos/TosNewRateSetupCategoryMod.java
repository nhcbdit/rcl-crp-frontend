package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewRateSetupCategoryMod extends RrcStandardMod {
    private String line;  
    private String port;  
    private String terminal;  
    private String opr_code; 
    private String catg_col;                                         
    private String seq_no;  
    private String description;  
    private String rate_basis;  
    private String charge_code;  
    private String link_column;  
    private String teu_factor;  
    private String record_status;
    
    public TosNewRateSetupCategoryMod() {
        super();
        
        line = "";  
        port = "";  
        terminal = "";  
        opr_code = ""; 
        catg_col = "";
        seq_no = "";  
        description = "";  
        rate_basis = "";  
        charge_code = "";  
        link_column = "";  
        teu_factor = "";  
        record_status = "";
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setOpr_code(String opr_code) {
        this.opr_code = opr_code;
    }

    public String getOpr_code() {
        return opr_code;
    }

    public void setCatg_col(String catg_col) {
        this.catg_col = catg_col;
    }

    public String getCatg_col() {
        return catg_col;
    }

    public void setSeq_no(String seq_no) {
        this.seq_no = seq_no;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setRate_basis(String rate_basis) {
        this.rate_basis = rate_basis;
    }

    public String getRate_basis() {
        return rate_basis;
    }

    public void setCharge_code(String charge_code) {
        this.charge_code = charge_code;
    }

    public String getCharge_code() {
        return charge_code;
    }

    public void setLink_column(String link_column) {
        this.link_column = link_column;
    }

    public String getLink_column() {
        return link_column;
    }

    public void setTeu_factor(String teu_factor) {
        this.teu_factor = teu_factor;
    }

    public String getTeu_factor() {
        return teu_factor;
    }

    public void setRecord_status(String record_status) {
        this.record_status = record_status;
    }

    public String getRecord_status() {
        return record_status;
    }
}
