/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotAssignmentMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 01/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class EmsPickUpReturnDepotAssignmentMod extends RrcStandardMod {
    private String pointCode;
    private String pointName;
    private String depotCode;
    private String depotName;
    private String countryCode;
    private String country;
    private String fsc;
    private String status;

    public EmsPickUpReturnDepotAssignmentMod() {
        pointCode = "";
        pointName = "";
        depotCode = "";
        depotName = "";
        countryCode = "";
        country = "";
        fsc = "";
        status = "";
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointName() {
        return pointName;
    }

    public void setDepotCode(String depotCode) {
        this.depotCode = depotCode;
    }

    public String getDepotCode() {
        return depotCode;
    }

    public void setDepotName(String depotName) {
        this.depotName = depotName;
    }

    public String getDepotName() {
        return depotName;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
