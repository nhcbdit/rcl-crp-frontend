/*------------------------------------------------------
QtnPortSurchargeMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
 Author Sopon Dee-udomvongsa 02/09/2008 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class QtnPortSurchargeMod extends RrcStandardMod{
    private String longDescription;
    private String shortDescription;
    private String surchargeCode;
    private String surchargeCodeStatus;
    private String surchargeTypeCode;

    public QtnPortSurchargeMod() {
        longDescription = "";
        shortDescription = "";
        surchargeCode = "";
        surchargeCodeStatus = "";
        surchargeTypeCode = "";
    }
    
    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getSurchargeCode() {
        return surchargeCode;
    }

    public void setSurchargeCode(String surchargeCode) {
        this.surchargeCode = surchargeCode;
    }

    public String getSurchargeCodeStatus() {
        return surchargeCodeStatus;
    }

    public void setSurchargeCodeStatus(String surchargeCodeStatus) {
        this.surchargeCodeStatus = surchargeCodeStatus;
    }

    public String getSurchargeTypeCode() {
        return surchargeTypeCode;
    }

    public void setSurchargeTypeCode(String surchargeTypeCode) {
        this.surchargeTypeCode = surchargeTypeCode;
    }
}

