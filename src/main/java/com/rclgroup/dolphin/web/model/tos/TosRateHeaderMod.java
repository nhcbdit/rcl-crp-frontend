/*-----------------------------------------------------------------------------------------------------------  
TosRateHeaderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 06/06/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosRateHeaderMod extends RrcStandardMod{

    private String oprType;
    private String description;
    private String rateRef;
    public TosRateHeaderMod() {
        super();
        oprType = "";
        description = "";
        rateRef = "";
    }
    
    
    public TosRateHeaderMod(String oprType, String description, String rateRef) {
        this.oprType = oprType;
        this.description = description;
        this.rateRef = rateRef;

    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOprType() {
        return oprType;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setRateRef(String rateRef) {
        this.rateRef = rateRef;
    }

    public String getRateRef() {
        return rateRef;
    }
}
