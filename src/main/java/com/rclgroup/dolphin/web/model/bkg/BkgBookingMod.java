package com.rclgroup.dolphin.web.model.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BkgBookingMod extends RrcStandardMod {
    private String bookingNo;
    private String bookingDate;
    private String bookingStatus;
    public BkgBookingMod() {
        bookingNo = "";
        bookingDate = "";
        bookingStatus = "";    
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }
    
}
