package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamRseScriptFacilityConfigParameterMod extends RrcStandardMod {
    private int seqNo;
    private int paramId;
    
    private String labelName;
    private String paramName;
    private String inptName;
    private String inptType;
    private String grpName;
    private String dfltValue;
    private String vrfyName;
    private String paramType;
    private String lkupName;
    private String lkupType;
    private String lkupReturn;
    private String mandFlag;
    private String grpMandatoryCode;
    private String periodParameterCode;
    
    private boolean isDelete;
    private int dataStatus;

    public int INSERT;
    public int UPDATE;

    public CamRseScriptFacilityConfigParameterMod() {
        //seqNo = null;
        //paramId = null;
        labelName = "";
        paramName = "";
        inptName = "";
        inptType = "";
        grpName = "";
        dfltValue = "";
        vrfyName = "";
        paramType = "";
        lkupName = "";
        lkupType = "";
        lkupReturn = "";
        mandFlag = "";
        grpMandatoryCode = "";
        periodParameterCode="";
        
        isDelete = false;
        dataStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDataStatus(int dataStatus) {
        this.dataStatus = dataStatus;
    }

    public int getDataStatus() {
        return dataStatus;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setInptName(String inptName) {
        this.inptName = inptName;
    }

    public String getInptName() {
        return inptName;
    }

    public void setInptType(String inptType) {
        this.inptType = inptType;
    }

    public String getInptType() {
        return inptType;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setDfltValue(String dfltValue) {
        this.dfltValue = dfltValue;
    }

    public String getDfltValue() {
        return dfltValue;
    }

    public void setVrfyName(String vrfyName) {
        this.vrfyName = vrfyName;
    }

    public String getVrfyName() {
        return vrfyName;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamType() {
        return paramType;
    }

    public void setLkupName(String lkupName) {
        this.lkupName = lkupName;
    }

    public String getLkupName() {
        return lkupName;
    }

    public void setLkupType(String lkupType) {
        this.lkupType = lkupType;
    }

    public String getLkupType() {
        return lkupType;
    }

    public void setLkupReturn(String lkupReturn) {
        this.lkupReturn = lkupReturn;
    }

    public String getLkupReturn() {
        return lkupReturn;
    }

    public void setMandFlag(String mandFlag) {
        this.mandFlag = mandFlag;
    }

    public String getMandFlag() {
        return mandFlag;
    }

    public void setParamId(int paramId) {
        this.paramId = paramId;
    }

    public int getParamId() {
        return paramId;
    }
/*
    public void setGrpMandatoryFlg(String grpMandatoryFlg) {
        this.grpMandatoryFlg = grpMandatoryFlg;
    }

    public String getGrpMandatoryFlg() {
        return grpMandatoryFlg;
    }

*/

    public void setPeriodParameterCode(String periodParameterCode) {
        this.periodParameterCode = periodParameterCode;
    }

    public String getPeriodParameterCode() {
        return periodParameterCode;
    }

    public void setGrpMandatoryCode(String grpMandatoryCode) {
        this.grpMandatoryCode = grpMandatoryCode;
    }

    public String getGrpMandatoryCode() {
        return grpMandatoryCode;
    }
}
