/*-----------------------------------------------------------------------------------------------------------  
VssServiceProformaMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.vss;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class VssServiceProformaMod extends RrcStandardMod {
    private String serviceCode;
    private String serviceDescription;
    private String simulationReferenceNo;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public VssServiceProformaMod(String serviceCode
                        ,String serviceDescription
                        ,String simulationReferenceNo
                        ,String recordStatus
                        ,String recordAddUser
                        ,Timestamp recordAddDate
                        ,String recordChangeUser
                        ,Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.serviceCode = serviceCode;
        this.serviceDescription = serviceDescription;
        this.simulationReferenceNo = simulationReferenceNo;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public VssServiceProformaMod() {
        this("","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0)); 
    }
    
    public VssServiceProformaMod(VssServiceProformaMod mod){
       this(mod.getServiceCode()
           ,mod.getServiceDescription()
           ,mod.getSimulationReferenceNo()
           ,mod.getRecordStatus()
           ,mod.getRecordAddUser()
           ,mod.getRecordAddDate()
           ,mod.getRecordChangeUser()
           ,mod.getRecordChangeDate());
    }


    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }
    
    public void setSimulationReferenceNo(String simulationReferenceNo) {
        this.simulationReferenceNo = simulationReferenceNo;
    }

    public String getSimulationReferenceNo() {
        return simulationReferenceNo;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}


