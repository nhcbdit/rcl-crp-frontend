package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaBsaPortPairTsMod2 extends RrcStandardMod {

    private String tsIndic; // TS_INDIC
    private String bsaService; // SERVICE
    private String bsaProformaRefNo; // PROFORMA_REFNO
    private String bsaPort; // PORT_CODE
    private String bsaVariant; // VARIANT_CODE
    private String bsaGroup;
    private String status;// RECORD_STATUS
    
    static final String SEARCH_TS_INDIC = "TS_INDIC";
    static final String SEARCH_SERVICE = "SERVICE";
    static final String SEARCH_PROFORMA = "PROFORMA_REFNO";
    static final String SEARCH_VARIANT = "V_CODE";
    static final String SEARCH_PORT = "PORT_CODE";


    public BsaBsaPortPairTsMod2(String tsIndic,
                          String bsaService,
                          String bsaProformaRefNo,
                          String bsaPort,
                          String bsaVariant,
                          String bsaGroup,
                          String status) {        
        this.tsIndic = tsIndic;
        this.bsaService = bsaService;
        this.bsaVariant = bsaVariant;
        this.bsaPort = bsaPort;
        this.bsaProformaRefNo = bsaProformaRefNo;
        this.bsaGroup = bsaGroup;
        this.status = status;        
    }
    
    public BsaBsaPortPairTsMod2() {
        this("","","","","","","");
    }
    
    public BsaBsaPortPairTsMod2(BsaBsaPortPairTsMod2 mod){
       this(mod.getTsIndic(),
            mod.getBsaService(),
            mod.getBsaVariant(),
            mod.getBsaPort(),
            mod.getBsaProformaRefNo(),
            mod.getBsaGroup(),
            mod.getStatus());
    }
    
    public boolean equals(Object object){
        if(object instanceof BsaBsaPortPairTsMod2){
            BsaBsaPortPairTsMod2 mod = (BsaBsaPortPairTsMod2)object;
            if(!this.tsIndic.equals(mod.getTsIndic())){
                return false;
            }else if(!this.bsaService.equals(mod.getBsaService())){
                return false;
            }else if(!this.bsaVariant.equals(mod.getBsaVariant())){
                return false;
            }else if(!this.bsaPort.equals(mod.getBsaPort())){
                return false;
            }else if(!this.bsaGroup.equals(mod.getBsaGroup())){
                return false;
            }else if(!this.bsaProformaRefNo.equals(mod.getBsaProformaRefNo())){
                return false;        
            }else if(!this.status.equals(mod.getStatus())){
                return false;   
            }
        }else{
            return false;
        }
        return true;
    } 

    public void setTsIndic(String tsIndic) {
        this.tsIndic = tsIndic;
    }

    public String getTsIndic() {
        return tsIndic;
    }

    public void setBsaService(String bsaService) {
        this.bsaService = bsaService;
    }

    public String getBsaService() {
        return bsaService;
    }

    public void setBsaVariant(String bsaVariant) {
        this.bsaVariant = bsaVariant;
    }

    public String getBsaVariant() {
        return bsaVariant;
    }

    public void setBsaPort(String bsaPort) {
        this.bsaPort = bsaPort;
    }

    public String getBsaPort() {
        return bsaPort;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBsaProformaRefNo(String bsaProformaRefNo) {
        this.bsaProformaRefNo = bsaProformaRefNo;
    }

    public String getBsaProformaRefNo() {
        return bsaProformaRefNo;
    }

    public void setBsaGroup(String bsaGroup) {
        this.bsaGroup = bsaGroup;
    }

    public String getBsaGroup() {
        return bsaGroup;
    }
}
