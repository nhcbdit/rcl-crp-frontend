package com.rclgroup.dolphin.web.model.fap;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class FapLiftingMod extends RrcStandardMod{
    private String sessionId;
    private String username;
    private String reportType;
    private String period;
    private String company;
    private String pol;
    private String pod;
    
    public FapLiftingMod(){
        sessionId = "";
        username = "";
        
        reportType = "";
        period = "";
        company = "";
        pol = "";
        pod = "";
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReportType() {
        return reportType;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriod() {
        return period;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }
}
