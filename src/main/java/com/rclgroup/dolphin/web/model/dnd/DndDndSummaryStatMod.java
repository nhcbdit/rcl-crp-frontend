/*-----------------------------------------------------------------------------------------------------------  
DndDndSummaryStatMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 06/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.dnd;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class DndDndSummaryStatMod extends RrcStandardMod {

    private String sessionId;
    private String username;
    private String country;
    private String fsc;
    private String userPerm;
    
    private String dndType;
    private String demurage_detention;
    private String bl;
    private String vessel;
    private String voyage;
    
    
    public DndDndSummaryStatMod() {
        sessionId = "";
        username = "";
        country = "";
        fsc = "";
        bl="";
        vessel="";
        voyage="";
        userPerm = "";
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFsc() {
        return fsc;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getUserPerm() {
        return userPerm;
    }

    public void setUserPerm(String userPerm) {
        this.userPerm = userPerm;
    }
    
    public String getDndType() {
        return dndType;
    }

    public void setDndType(String dndType) {
        this.dndType = dndType;
    }
    
    public String getDemurage_Detention() {
        return demurage_detention;
    }

    public void setDemurage_Detention(String demurage_detention) {
        this.demurage_detention = demurage_detention;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }
}
