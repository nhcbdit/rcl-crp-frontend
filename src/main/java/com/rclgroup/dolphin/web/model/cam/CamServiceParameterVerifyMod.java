package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamServiceParameterVerifyMod extends RrcStandardMod {
    
    private String parameterVerifyId;
    private String parameterVerifyCode;
    private String parameterVerifyName;
    private String parameterVerifyType;
    private String sqlStatement;
    private String staticVerify;
    private String staticVerifyRegExp;
    private String errorMsg;

    private String recordChangeDateStr;
    
    public CamServiceParameterVerifyMod() {
    
        parameterVerifyId   ="";
        parameterVerifyCode ="";
        parameterVerifyName ="";
        parameterVerifyType ="";
        sqlStatement        ="";
        staticVerify        ="";
        staticVerifyRegExp  ="";
        recordChangeDateStr ="";
        errorMsg            ="";
    }


    public void setParameterVerifyId(String parameterVerifyId) {
        this.parameterVerifyId = parameterVerifyId;
    }

    public String getParameterVerifyId() {
        return parameterVerifyId;
    }

    public void setParameterVerifyCode(String parameterVerifyCode) {
        this.parameterVerifyCode = parameterVerifyCode;
    }

    public String getParameterVerifyCode() {
        return parameterVerifyCode;
    }

    public void setParameterVerifyName(String parameterVerifyName) {
        this.parameterVerifyName = parameterVerifyName;
    }

    public String getParameterVerifyName() {
        return parameterVerifyName;
    }

    public void setSqlStatement(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }

    public String getSqlStatement() {
        return sqlStatement;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setRecordChangeDateStr(String recordChangeDateStr) {
        this.recordChangeDateStr = recordChangeDateStr;
    }

    public String getRecordChangeDateStr() {
        return recordChangeDateStr;
    }


    public void setParameterVerifyType(String parameterVerifyType) {
        this.parameterVerifyType = parameterVerifyType;
    }

    public String getParameterVerifyType() {
        return parameterVerifyType;
    }

    public void setStaticVerify(String staticVerify) {
        this.staticVerify = staticVerify;
    }

    public String getStaticVerify() {
        return staticVerify;
    }


    public void setStaticVerifyRegExp(String staticVerifyRegExp) {
        this.staticVerifyRegExp = staticVerifyRegExp;
    }

    public String getStaticVerifyRegExp() {
        return staticVerifyRegExp;
    }
}
