/*-----------------------------------------------------------------------------------------------------------  
CtfPortGroupHeaderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 25/03/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.ctf;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Date;


public class CtfPortGroupHeaderMod extends RrcStandardMod {
    static final String SOC_COC_DEFAULT = "S";

    private String portGroupCode; // PORT_GRP_CDE 
    private String description; // DESCRIPTION
    private String socCoc; // SOC_COC
    private String countryCode; // COUNTRY_CDE
    private String updateTime; // UPDATE_TIME
    private String line; // LINE
    private String trade; // TRADE
    private String agent; // AGENT
    private String recordAddDate; // RECORD_ADD_DATETIME
    private String recordChangeDate; // RECORD_CHANGE_DATETIME

    public CtfPortGroupHeaderMod(String portGroupCode,
                                 String description,
                                 String socCoc,
                                 String countryCode,
                                 String updateTime,
                                 String line,
                                 String trade,
                                 String agent,
                                 String recordStatus,
                                 String recordAddUser,
                                 String recordAddDate,
                                 String recordChangeUser,
                                 String recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.portGroupCode = portGroupCode;
        this.description = description;
        this.socCoc = socCoc;
        this.countryCode = countryCode;
        this.updateTime = updateTime;
        this.line = line;
        this.trade = trade;
        this.agent = agent;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public CtfPortGroupHeaderMod() {
        this("","",SOC_COC_DEFAULT,"","","","","",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Date(0).toString(),"",new Date(0).toString());
    }
    
    public CtfPortGroupHeaderMod(CtfPortGroupHeaderMod mod){
       this(mod.getPortGroupCode(),
            mod.getDescription(),
            mod.getSocCoc(),
            mod.getCountryCode(),
            mod.getUpdateTime(),
            mod.getLine(),
            mod.getTrade(),
            mod.getAgent(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate());
    }
    
    public boolean equals(Object object){
        if(object instanceof CtfPortGroupHeaderMod){
            CtfPortGroupHeaderMod mod = (CtfPortGroupHeaderMod)object;
            if(!this.portGroupCode.equals(mod.getPortGroupCode())){
                return false;
            }else if(!this.description.equals(mod.getDescription())){
                return false;
            }else if(!this.socCoc.equals(mod.getSocCoc())){
                return false;
            }else if(!this.countryCode.equals(mod.getCountryCode())){
                return false;
            }else if(!this.updateTime.equals(mod.getUpdateTime())){
                return false;
            }else if(!this.line.equals(mod.getLine())){
                return false;
            }else if(!this.trade.equals(mod.getTrade())){
                return false;
            }else if(!this.agent.equals(mod.getAgent())){
                return false;
            }else if(!this.recordStatus.equals(mod.getRecordStatus())){
                return false;
            }
        }else{
            return false;
        }
        return true;
    }


    public void setPortGroupCode(String portGroupCode) {
        this.portGroupCode = portGroupCode;
    }

    public String getPortGroupCode() {
        return portGroupCode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setSocCoc(String socCoc) {
        this.socCoc = socCoc;
    }

    public String getSocCoc() {
        return socCoc;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getTrade() {
        return trade;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setRecordAddDate(String recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public String getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(String recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public String getRecordChangeDate() {
        return recordChangeDate;
    }
}

