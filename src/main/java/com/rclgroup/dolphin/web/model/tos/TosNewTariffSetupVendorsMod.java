/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupVendorsMod.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Panadda P. 29/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupVendorsMod extends RrcStandardMod{
    private String vesselOpr;
    private String oprType;
    private String oprTypeDesc;
    private String vendorCode;
    private String vendorDesc;
    private String payByFCS;
    private String tosRateSeq; 
    private String effDate;
    private String expDate;
    private String rateRef;
    private String reference;
    private boolean isDelete;
    private int rowStatus;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewTariffSetupVendorsMod() {
        super();
        vesselOpr = "";
        oprType = "";
        oprTypeDesc = "";
        vendorCode = "";
        vendorDesc = "";
        payByFCS = "";
        tosRateSeq = "";
        effDate = "";
        expDate = "";
        rateRef = "";
        reference = "";
        isDelete = false;
        rowStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setVesselOpr(String vesselOpr) {
        this.vesselOpr = vesselOpr;
    }

    public String getVesselOpr() {
        return vesselOpr;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorDesc(String vendorDesc) {
        this.vendorDesc = vendorDesc;
    }

    public String getVendorDesc() {
        return vendorDesc;
    }

    public void setPayByFCS(String payByFCS) {
        this.payByFCS = payByFCS;
    }

    public String getPayByFCS() {
        return payByFCS;
    }

    public void setTosRateSeq(String tosRateSeq) {
        this.tosRateSeq = tosRateSeq;
    }

    public String getTosRateSeq() {
        return tosRateSeq;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setRateRef(String rateRef) {
        this.rateRef = rateRef;
    }

    public String getRateRef() {
        return rateRef;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprTypeDesc(String oprTypeDesc) {
        this.oprTypeDesc = oprTypeDesc;
    }

    public String getOprTypeDesc() {
        return oprTypeDesc;
    }

    public void setRowStatus(int rowStatus) {
        this.rowStatus = rowStatus;
    }

    public int getRowStatus() {
        return rowStatus;
    }
}
