/*-----------------------------------------------------------------------------------------------------------  
CamRseServiceParameterHelpMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 25/08/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamRseServiceParameterHelpMod extends RrcStandardMod {
    
    private String helpId;
    private String parameterType;
    private String lookupName;
    private String lookupType;
    private String lookupReturn;
    
    private String functionName;
    private String codeJavaScript;
    
    public CamRseServiceParameterHelpMod() {
        super();
    }

    public String getCodeJavaScript() {
        return codeJavaScript;
    }

    public void setCodeJavaScript(String codeJavaScript) {
        this.codeJavaScript = codeJavaScript;
    }

    public String getHelpId() {
        return helpId;
    }

    public void setHelpId(String helpId) {
        this.helpId = helpId;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getLookupName() {
        return lookupName;
    }

    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupReturn() {
        return lookupReturn;
    }

    public void setLookupReturn(String lookupReturn) {
        this.lookupReturn = lookupReturn;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
}


