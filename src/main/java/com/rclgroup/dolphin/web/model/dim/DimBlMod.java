/*--
--------------------------------------------------------
DexBlMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sukit    
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
## 12/01/09  KIT    BUG.174   B/L Lookup problems
## 24/01/09  KIT    BUG.174(Re-Opend)   B/L Lookup problems
03 03/06/09  POR    MODIFIED  ADD HOUSE B/L
04 29062009  WAC    MODIFIED  ADDED BL ID,BL TYPE,SERVICE,VER,POT
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.util.RutDate;

import java.sql.Date;

public class DimBlMod extends RrcStandardMod {

    private String booking;
    private String bl;
    private String hbl; // --##03
    private String opCode;
    private Date issueDate;
    private String cocSoc;
    private String outStatus;
    private String inStatus;
    private String vessel;
    private String voyage;
    private String pol;
    private String pod;
    private String manifestDate;
    private String issueDateFormat; // --##01
//--##04    
    private String blId; 
    private String blType;
    private String service;
    private String ver;
    private String pot;
//--##04

    public DimBlMod() {
        booking = "";
        bl = "";
        hbl = "";
        opCode = "";
        cocSoc = "";
        outStatus = "";
        inStatus = "";
        vessel = "";
        voyage = "";
        pol = "";
        pod = "";
        manifestDate = "";  
        blId="";
        blType="";
        service="";
        ver="";
        pot="";

    }

    public void setBooking(String booking) {
        this.booking = booking;
    }

    public String getBooking() {
        return booking;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setOpCode(String opCode) {
        this.opCode = opCode;
    }

    public String getOpCode() {
        return opCode;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
        // --##BEGIN 01
        if(this.issueDate != null){
            this.issueDateFormat = this.issueDate.toString();
        }else{
            this.issueDateFormat = "-";
        }
        // --##END 01
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setOutStatus(String outStatus) {
        this.outStatus = outStatus;
    }

    public String getOutStatus() {
        return outStatus;
    }

    public void setInStatus(String inStatus) {
        this.inStatus = inStatus;
    }

    public String getInStatus() {
        return inStatus;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setManifestDate(String manifestDate) {
        this.manifestDate = manifestDate;
    }

    public String getManifestDate() {
        return manifestDate;
    }
  // --##BEGIN 01
    public String getIssueDateFormat() {
        if(this.issueDateFormat.length() == 10){
        String [] issueDateArray = this.issueDateFormat.split("/");
        if(issueDateArray != null && issueDateArray.length == 3){
            this.issueDateFormat = issueDateArray[2]+"-"+issueDateArray[1]+"-"+issueDateArray[0];
        }
            this.issueDateFormat = RutDate.getDefaultDateStringFromJdbcDateString(this.issueDateFormat);  
        }
        return issueDateFormat;
    }
  // --##END 01

   // --##BEGIN 03
    public void setHbl(String hbl) {
        this.hbl = hbl;
    }

    public String getHbl() {
        return hbl;
    }
    // --##END 03
    
    //--Begin ##04
     public void setBlId(String blId) {
         this.blId = blId;
     }

     public String getBlId() {
         return blId;
     }

     public void setBlType(String blType) {
         this.blType = blType;
     }

     public String getBlType() {
         return blType;
     }

     public void setBlVersion(String ver) {
         this.ver = ver;
     }

     public String getBlVersion() {
         return ver;
     }
    public void setPot(String pot) {
        this.pot = pot;
    }

    public String getPot() {
        return pot;
    }
    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }
    
//--END ##04
}
