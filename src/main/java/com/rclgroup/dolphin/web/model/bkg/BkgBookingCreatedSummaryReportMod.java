/*-----------------------------------------------------------------------------------------------------------  
BkgBookingCreatedSummaryReportMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 04/08/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class BkgBookingCreatedSummaryReportMod extends RrcStandardMod {
    
    private String sessionId;
    private String username;
    private String reportType;
    private String fsc;
    private String dateFrom;
    private String dateTo;
    private String specificUser;
    
    public BkgBookingCreatedSummaryReportMod() {
        sessionId = "";
        username = "";
        reportType = "";
        fsc = "";
        dateFrom = "";
        dateTo = "";
        specificUser = "";
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getFsc() {
        return fsc;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getSpecificUser() {
        return specificUser;
    }

    public void setSpecificUser(String specificUser) {
        this.specificUser = specificUser;
    }
}
