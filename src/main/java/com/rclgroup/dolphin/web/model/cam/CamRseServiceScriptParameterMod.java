/*
 * -----------------------------------------------------------------------------
 * CamRseServiceScriptParameterMod.java
 * -----------------------------------------------------------------------------
 * Copyright RCL Public Co., Ltd. 2007 
 * -----------------------------------------------------------------------------
 * Author Dhruv Parekh 31/08/2012
 * ------- Change Log ----------------------------------------------------------
 * ##   DD/MM/YY    -User-      -TaskRef-      -Short Description  
 * -----------------------------------------------------------------------------
 */

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseServiceScriptParameterMod extends RrcStandardMod {
    
    private String parameterId;
    private String serviceId;
    private String serviceCode;
    private String seqNo;
    private String labelName;
    private String parameterName;
    private String inputName;
    private String inputType;
    private String inputGroupCode;
    private String inputDefaultValue;
    private String inputVerifyCode;
    private String parameterType;
    private String lookupName;
    private String lookupType;
    private String lookupReturn;
    private String mandatoryFlag;
    private String grpMandatoryCode;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    //private String grpMandatoryFlg;
    private String periodParameterCode;
    
    private int noOfDay;
    
    private boolean isDisable;
    
    public CamRseServiceScriptParameterMod() {
        super();
    }

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getInputGroupCode() {
        return inputGroupCode;
    }

    public void setInputGroupCode(String inputGroupCode) {
        this.inputGroupCode = inputGroupCode;
    }

    public String getInputDefaultValue() {
        return inputDefaultValue;
    }

    public void setInputDefaultValue(String inputDefaultValue) {
        this.inputDefaultValue = inputDefaultValue;
    }

    public String getInputVerifyCode() {
        return inputVerifyCode;
    }

    public void setInputVerifyCode(String inputVerifyCode) {
        this.inputVerifyCode = inputVerifyCode;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getLookupName() {
        return lookupName;
    }

    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupReturn() {
        return lookupReturn;
    }

    public void setLookupReturn(String lookupReturn) {
        this.lookupReturn = lookupReturn;
    }

    public String getMandatoryFlag() {
        return mandatoryFlag;
    }

    public void setMandatoryFlag(String mandatoryFlag) {
        this.mandatoryFlag = mandatoryFlag;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
    
  /*  public void setGrpMandatoryFlg(String grpMandatoryFlg) {
        this.grpMandatoryFlg = grpMandatoryFlg;
    }

    public String getGrpMandatoryFlg() {
        return grpMandatoryFlg;
    }
   */ 
    public void setGrpMandatoryCode(String grpMandatoryCode) {
        this.grpMandatoryCode = grpMandatoryCode;
    }

    public String getGrpMandatoryCode() {
        return grpMandatoryCode;
    }

    public void setIsDisable(boolean isDisable) {
        this.isDisable = isDisable;
    }

    public boolean isIsDisable() {
        return isDisable;
    }

    public void setPeriodParameterCode(String periodParameterCode) {
        this.periodParameterCode = periodParameterCode;
    }

    public String getPeriodParameterCode() {
        return periodParameterCode;
    }

    public void setNoOfDay(int noOfDay) {
        this.noOfDay = noOfDay;
    }

    public int getNoOfDay() {
        return noOfDay;
    }
}


