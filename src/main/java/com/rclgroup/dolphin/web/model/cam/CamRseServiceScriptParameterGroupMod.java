/*
 * -----------------------------------------------------------------------------
 * CamRseServiceScriptParameterGroupMod.java
 * -----------------------------------------------------------------------------
 * Copyright RCL Public Co., Ltd. 2007 
 * -----------------------------------------------------------------------------
 * Author Dhruv Parekh 31/08/2012
 * ------- Change Log ----------------------------------------------------------
 * ##   DD/MM/YY    -User-      -TaskRef-      -Short Description  
 * -----------------------------------------------------------------------------
 */

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseServiceScriptParameterGroupMod extends RrcStandardMod {
    
    private String parameterGroupId;
    private String parameterGroupCode;
    private String parameterGroupName;
    private String elementName;
    private String elementValue;
    private String seqNo;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;

    public CamRseServiceScriptParameterGroupMod() {
        super();
    }

    public String getParameterGroupId() {
        return parameterGroupId;
    }

    public void setParameterGroupId(String parameterGroupId) {
        this.parameterGroupId = parameterGroupId;
    }

    public String getParameterGroupCode() {
        return parameterGroupCode;
    }

    public void setParameterGroupCode(String parameterGroupCode) {
        this.parameterGroupCode = parameterGroupCode;
    }

    public String getParameterGroupName() {
        return parameterGroupName;
    }

    public void setParameterGroupName(String parameterGroupName) {
        this.parameterGroupName = parameterGroupName;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementValue() {
        return elementValue;
    }

    public void setElementValue(String elementValue) {
        this.elementValue = elementValue;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}


