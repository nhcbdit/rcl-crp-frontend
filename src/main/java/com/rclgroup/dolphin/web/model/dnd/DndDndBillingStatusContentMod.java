/*-----------------------------------------------------------------------------------------------------------  
DndDndBillingStatusContentMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 02/08/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 27/12/12 NIP    PD_CR_20121220  New Option "All Pending Billing � Details"
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.dnd;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class DndDndBillingStatusContentMod extends RrcStandardMod {

    private String sessionId;
    private String username;
    private String reportType;
    private String dateFrom;
    private String dateTo;
    private String country;
    private String fsc;
    private String userPerm;
    private String searchBy;//##01
    private String vessel;//##01
    private String voyage;//##01
    private String dndType;//##01
    private String demurageDetention;//##01
    private String bl;//##02
    
    public DndDndBillingStatusContentMod() {
        sessionId = "";
        username = "";
        reportType = "";
        dateFrom = "";
        dateTo = "";
        country = "";
        fsc = "";
        userPerm = "";
        dndType = ""; //##264
        demurageDetention = ""; //##264
        bl=""; // ##02
        
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFsc() {
        return fsc;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getUserPerm() {
        return userPerm;
    }

    public void setUserPerm(String userPerm) {
        this.userPerm = userPerm;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDndType(String dndType) {
        this.dndType = dndType;
    }

    public String getDndType() {
        return dndType;
    }

    public void setDemurageDetention(String demurageDetention) {
        this.demurageDetention = demurageDetention;
    }

    public String getDemurageDetention() {
        return demurageDetention;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }
}
