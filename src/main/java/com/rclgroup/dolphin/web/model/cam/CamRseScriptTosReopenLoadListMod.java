/*-----------------------------------------------------------------------------------------------------------  
CamRseScriptTosReopenLoadListMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 14/03/11
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import java.sql.Timestamp;


public class CamRseScriptTosReopenLoadListMod extends CamRseScriptMod {
    
    private String vessel;
    private String voyage;
    private String pol;
    private String polTerminal;
    
    public CamRseScriptTosReopenLoadListMod() {
        super();
    }
    
    public CamRseScriptTosReopenLoadListMod(String vessel, String voyage, String pol, String polTerminal) {
        super();
        this.vessel = vessel;
        this.voyage = voyage;
        this.pol = pol;
        this.polTerminal = polTerminal;
    }
    
    public CamRseScriptTosReopenLoadListMod(String vessel, String voyage, String pol, String polTerminal, String flagExecute, String returnResult, Timestamp recordChangeDate) {
        super(flagExecute, returnResult, recordChangeDate);
        this.vessel = vessel;
        this.voyage = voyage;
        this.pol = pol;
        this.polTerminal = polTerminal;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPolTerminal() {
        return polTerminal;
    }

    public void setPolTerminal(String polTerminal) {
        this.polTerminal = polTerminal;
    }

}

