/*-----------------------------------------------------------------------------------------------------------  
BsaBsaModelMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 10/03/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class BsaBsaModelMod extends RrcStandardMod {
    static final String SIMULATION_FLAG_DEFAULT = "S"; 

    private String bsaModelId; // BSA_MODEL_ID 
    private String modelName; // MODEL_NAME
    private String simulationFlag; // SIMULATION_FLAG
    private String validFrom; // VALID_FROM
    private String validTo; // VALID_TO
    private String averageMtTeuWeight; // AVG_MT_TEU_WEIGHT
    private String imbalanceWarningPercentage; // IMBALANCE_WARNING_PER
    private String calculatedDuration;
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;

    public BsaBsaModelMod(String bsaModelId,
                          String modelName,
                          String simulationFlag,
                          String validFrom,
                          String validTo,
                          String averageMtTeuWeight,
                          String imbalanceWarningPercentage,
                          String calculatedDuration,
                          String recordStatus,
                          String recordAddUser,
                          Timestamp recordAddDate,
                          String recordChangeUser,
                          Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.bsaModelId = bsaModelId;
        this.modelName = modelName;
        this.simulationFlag = simulationFlag;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.averageMtTeuWeight = averageMtTeuWeight;
        this.imbalanceWarningPercentage = imbalanceWarningPercentage;
        this.calculatedDuration = calculatedDuration;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public BsaBsaModelMod() {
        this("0","",SIMULATION_FLAG_DEFAULT,"","","0.0","0.0","0",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0));
    }
    
    public BsaBsaModelMod(BsaBsaModelMod mod){
       this(mod.getBsaModelId(),
            mod.getModelName(),
            mod.getSimulationFlag(),
            mod.getValidFrom(),
            mod.getValidTo(),
            mod.getAverageMtTeuWeight(),
            mod.getImbalanceWarningPercentage(),
            mod.getCalculatedDuration(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate());
    }
    
    public boolean equals(Object object){
        if(object instanceof BsaBsaModelMod){
            BsaBsaModelMod mod = (BsaBsaModelMod)object;
            if(!this.bsaModelId.equals(mod.getBsaModelId())){
                return false;
            }else if(!this.modelName.equals(mod.getModelName())){
                return false;
            }else if(!this.simulationFlag.equals(mod.getSimulationFlag())){
                return false;
            }else if(!this.validFrom.equals(mod.getValidFrom())){
                return false;
            }else if(!this.validTo.equals(mod.getValidTo())){
                return false;
            }else if(!this.averageMtTeuWeight.equals(mod.getAverageMtTeuWeight())){
                return false;
            }else if(!this.imbalanceWarningPercentage.equals(mod.getImbalanceWarningPercentage())){
                return false;
            }else if(!this.calculatedDuration.equals(mod.getCalculatedDuration())){
                return false;
            }else if(!this.recordStatus.equals(mod.getRecordStatus())){
                return false;
            }
        }else{
            return false;
        }
        return true;
    } 

    public void setBsaModelId(String bsaModelId) {
        this.bsaModelId = bsaModelId;
    }

    public String getBsaModelId() {
        return bsaModelId;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setSimulationFlag(String simulationFlag) {
        this.simulationFlag = simulationFlag;
    }

    public String getSimulationFlag() {
        return simulationFlag;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setAverageMtTeuWeight(String averageMtTeuWeight) {
        this.averageMtTeuWeight = averageMtTeuWeight;
    }

    public String getAverageMtTeuWeight() {
        return averageMtTeuWeight;
    }

    public void setImbalanceWarningPercentage(String imbalanceWarningPercentage) {
        this.imbalanceWarningPercentage = imbalanceWarningPercentage;
    }

    public String getImbalanceWarningPercentage() {
        return imbalanceWarningPercentage;
    }

    public void setCalculatedDuration(String calculatedDuration) {
        this.calculatedDuration = calculatedDuration;
    }

    public String getCalculatedDuration() {
        return calculatedDuration;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}

