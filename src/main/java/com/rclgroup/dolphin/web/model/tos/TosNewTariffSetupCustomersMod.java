/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupCustomersMod.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Panadda P. 29/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupCustomersMod extends RrcStandardMod{
    
    private String vesselOpr;
    private String customerCd;
    private String customerDesc;
    private String oprType;
    private String oprTypeDesc;
    private String effDate;
    private String expDate;
    private String seqNo;
    private String recoveryRateRef;
    private String tosRateSeqNo;
    private int rowStatus;
    private boolean isDelete;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewTariffSetupCustomersMod() {
        super();
        vesselOpr = "";
        customerCd = "";
        customerDesc = "";
        oprType = "";
        oprTypeDesc = "";
        effDate = "";
        expDate = "";
        seqNo = "";
        recoveryRateRef = "";
        tosRateSeqNo = "";
        isDelete = false;
        rowStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setVesselOpr(String vesselOpr) {
        this.vesselOpr = vesselOpr;
    }

    public String getVesselOpr() {
        return vesselOpr;
    }

    public void setCustomerCd(String customerCd) {
        this.customerCd = customerCd;
    }

    public String getCustomerCd() {
        return customerCd;
    }

    public void setCustomerDesc(String customerDesc) {
        this.customerDesc = customerDesc;
    }

    public String getCustomerDesc() {
        return customerDesc;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprTypeDesc(String oprTypeDesc) {
        this.oprTypeDesc = oprTypeDesc;
    }

    public String getOprTypeDesc() {
        return oprTypeDesc;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setRecoveryRateRef(String recoveryRateRef) {
        this.recoveryRateRef = recoveryRateRef;
    }

    public String getRecoveryRateRef() {
        return recoveryRateRef;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setTosRateSeqNo(String tosRateSeqNo) {
        this.tosRateSeqNo = tosRateSeqNo;
    }

    public String getTosRateSeqNo() {
        return tosRateSeqNo;
    }

    public void setRowStatus(int rowStatus) {
        this.rowStatus = rowStatus;
    }

    public int getRowStatus() {
        return rowStatus;
    }
}
