package com.rclgroup.dolphin.web.model.cam;

public class CamStaticVerifyParamMod {

    public CamStaticVerifyParamMod() {
    }
    
    private String staticVerifyCode;
    private String staticVerifyName;
  

    public void setStaticVerifyCode(String staticVerifyCode) {
        this.staticVerifyCode = staticVerifyCode;
    }

    public String getStaticVerifyCode() {
        return staticVerifyCode;
    }

    public void setStaticVerifyName(String staticVerifyName) {
        this.staticVerifyName = staticVerifyName;
    }

    public String getStaticVerifyName() {
        return staticVerifyName;
    }

  
}
