package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosServiceMod extends RrcStandardMod {
    
    private String service;
    private String serviceDesc;
    private String port;
    private String terminal;    
    private String status;
    
    public TosServiceMod() {
        super();
        service = "";
        serviceDesc = "";
        port = "";
        terminal = "";
        status = "";
    }
    
    public TosServiceMod(String service, String serviceDesc, String port, String terminal, String status) {
        this.service = service;
        this.serviceDesc = serviceDesc;
        this.port = port;
        this.terminal = terminal;    
        this.status = status;
    }


    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
