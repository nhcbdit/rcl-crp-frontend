/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupSearchMod.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Dhruv Parekh 25/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupSearchMod extends RrcStandardMod {
    private String port;
    private String terminal;  
    private String port_desc;
    private String terminal_desc;
    private String currency;
    
    public TosNewTariffSetupSearchMod() {
        super();
        port = "";
        terminal = ""; 
        port_desc = "";
        terminal_desc = "";
        currency = "";
    }
    
    public TosNewTariffSetupSearchMod(String port, String terminal, String port_desc, String terminal_desc, String currency){
        this.port = port;
        this.terminal = terminal;
        this.port_desc = port_desc;
        this.terminal_desc = terminal_desc;
        this.currency = currency;
    }
    

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setPort_desc(String port_desc) {
        this.port_desc = port_desc;
    }

    public String getPort_desc() {
        return port_desc;
    }

    public void setTerminal_desc(String terminal_desc) {
        this.terminal_desc = terminal_desc;
    }

    public String getTerminal_desc() {
        return terminal_desc;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }
}
