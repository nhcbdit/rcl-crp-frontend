/*-----------------------------------------------------------------------------------------------------------  
VsaVsaPortPairTsMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaVsaPortPairTsMod extends RrcStandardMod {
    private String vsaModelId; // BSA_MODEL_ID 
    private String tsIndic; // TS_INDIC
    private String vsaService; // SERVICE
    private String vsaVariant; // V_CODE
    private String vsaPort; // PORT_CODE
    private String vsaGroup; // GROUP_CODE
    private String vsaPortGroupFlag; // GROUP_FLAG    
    private String vsaSubGroupFlag; //SUB_GROUP_FLAG
    private String status;// RECORD_STATUS
    
    static final String SEARCH_TS_INDIC = "TS_INDIC";
    static final String SEARCH_SERVICE = "SERVICE";
    static final String SEARCH_VARIANT = "V_CODE";
    static final String SEARCH_PORT = "PORT_CODE";
    static final String SEARCH_GROUP = "GROUP_CODE";
    static final String SEARCH_STATUS = "RECODR_STATUS";

    public BsaVsaPortPairTsMod(String vsaModelId,
                          String tsIndic,
                          String vsaService,
                          String vsaVariant,
                          String vsaPort,
                          String vsaGroup,
                          String vsaPortGroupFlag,
                          String vsaSubGroupFlag,
                          String status) {        
        this.vsaModelId = vsaModelId;
        this.tsIndic = tsIndic;
        this.vsaService = vsaService;
        this.vsaVariant = vsaVariant;
        this.vsaPort = vsaPort;
        this.vsaGroup = vsaGroup;
        this.vsaPortGroupFlag = vsaPortGroupFlag;
        this.vsaSubGroupFlag = vsaSubGroupFlag;
        this.status = status;        
    }
    
    public BsaVsaPortPairTsMod() {
        this("","","","","","","","","");
    }
    
    public BsaVsaPortPairTsMod(BsaVsaPortPairTsMod mod){
       this(mod.getVsaModelId(),
            mod.getTsIndic(),
            mod.getVsaService(),
            mod.getVsaVariant(),
            mod.getVsaPort(),
            mod.getVsaGroup(),
            mod.getVsaPortGroupFlag(),
            mod.getVsaSubGroupFlag(),
            mod.getStatus());
    }
    
    public boolean equals(Object object){
        if(object instanceof BsaVsaPortPairTsMod){
            BsaVsaPortPairTsMod mod = (BsaVsaPortPairTsMod)object;
            if(!this.vsaModelId.equals(mod.getVsaModelId())){
                return false;
            }else if(!this.tsIndic.equals(mod.getTsIndic())){
                return false;
            }else if(!this.vsaService.equals(mod.getVsaService())){
                return false;
            }else if(!this.vsaVariant.equals(mod.getVsaVariant())){
                return false;
            }else if(!this.vsaPort.equals(mod.getVsaPort())){
                return false;
            }else if(!this.vsaGroup.equals(mod.getVsaGroup())){
                return false;
            }else if(!this.vsaPortGroupFlag.equals(mod.getVsaPortGroupFlag())){
                return false;        
            }else if(!this.vsaSubGroupFlag.equals(mod.getVsaSubGroupFlag())){
                return false;     
            }else if(!this.status.equals(mod.getStatus())){
                return false;   
            }
        }else{
            return false;
        }
        return true;
    } 
    
    public void setVsaModelId(String vsaModelId) {
        this.vsaModelId = vsaModelId;
    }

    public String getVsaModelId() {
        return vsaModelId;
    }

    public void setTsIndic(String tsIndic) {
        this.tsIndic = tsIndic;
    }

    public String getTsIndic() {
        return tsIndic;
    }

    public void setVsaService(String vsaService) {
        this.vsaService = vsaService;
    }

    public String getVsaService() {
        return vsaService;
    }

    public void setVsaVariant(String vsaVariant) {
        this.vsaVariant = vsaVariant;
    }

    public String getVsaVariant() {
        return vsaVariant;
    }

    public void setVsaPort(String vsaPort) {
        this.vsaPort = vsaPort;
    }

    public String getVsaPort() {
        return vsaPort;
    }

    public void setVsaGroup(String vsaGroup) {
        this.vsaGroup = vsaGroup;
    }

    public String getVsaGroup() {
        return vsaGroup;
    }

    public void setVsaPortGroupFlag(String vsaPortGroupFlag) {
        this.vsaPortGroupFlag = vsaPortGroupFlag;
    }

    public String getVsaPortGroupFlag() {
        return vsaPortGroupFlag;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setVsaSubGroupFlag(String vsaSubGroupFlag) {
        this.vsaSubGroupFlag = vsaSubGroupFlag;
    }

    public String getVsaSubGroupFlag() {
        return vsaSubGroupFlag;
    }
}
