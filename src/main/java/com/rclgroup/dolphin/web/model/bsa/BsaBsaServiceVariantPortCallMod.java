package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BsaBsaServiceVariantPortCallMod  extends RrcStandardMod{
        private String import_id;
        private String model_name;
        private String fk_service;
        private String fk_proforma_ref_no;
        private String variant_code;
        private String bsa_vessel_type;
        private String dn_port;
        private String fk_port_seq_no_from;
        private String fk_direction_from;
        private String fk_supported_port_group_id;
        private String dn_port_group_code;
        private String fk_port_seq_no_to;
        private String fk_direction_to;
        private String dn_load_discharge_flag;
        private String dn_transhipment_flag;
        private String from_to;
        private String dn_transhipment_port;
        private String dn_wayport_trunk_indicator;
        private String port_call_level;
        private String port_call_tolerant;
        private String slot_teu;
        private String slot_tons;
        private String slot_reefer_plugs;
        private String avg_coc_teu_weight;
        private String avg_soc_teu_weight;
        private String min_teu;
        private String port_call_record_status;
        private String errorMessageOfRow;
        
        public void setModel_name(String model_name) {
            this.model_name = model_name;
        }

        public String getModel_name() {
            return model_name;
        }

        public void setFk_service(String fk_service) {
            this.fk_service = fk_service;
        }

        public String getFk_service() {
            return fk_service;
        }

        public void setFk_proforma_ref_no(String fk_proforma_ref_no) {
            this.fk_proforma_ref_no = fk_proforma_ref_no;
        }

        public String getFk_proforma_ref_no() {
            return fk_proforma_ref_no;
        }

        public void setVariant_code(String variant_code) {
            this.variant_code = variant_code;
        }

        public String getVariant_code() {
            return variant_code;
        }

        public void setBsa_vessel_type(String bsa_vessel_type) {
            this.bsa_vessel_type = bsa_vessel_type;
        }

        public String getBsa_vessel_type() {
            return bsa_vessel_type;
        }

        public void setDn_port(String dn_port) {
            this.dn_port = dn_port;
        }

        public String getDn_port() {
            return dn_port;
        }

        public void setFk_port_seq_no_from(String fk_port_seq_no_from) {
            this.fk_port_seq_no_from = fk_port_seq_no_from;
        }

        public String getFk_port_seq_no_from() {
            return fk_port_seq_no_from;
        }

        public void setFk_direction_from(String fk_direction_from) {
            this.fk_direction_from = fk_direction_from;
        }

        public String getFk_direction_from() {
            return fk_direction_from;
        }

        public void setFk_supported_port_group_id(String fk_supported_port_group_id) {
            this.fk_supported_port_group_id = fk_supported_port_group_id;
        }

        public String getFk_supported_port_group_id() {
            return fk_supported_port_group_id;
        }

        public void setDn_port_group_code(String dn_port_group_code) {
            this.dn_port_group_code = dn_port_group_code;
        }

        public String getDn_port_group_code() {
            return dn_port_group_code;
        }

        public void setFk_port_seq_no_to(String fk_port_seq_no_to) {
            this.fk_port_seq_no_to = fk_port_seq_no_to;
        }

        public String getFk_port_seq_no_to() {
            return fk_port_seq_no_to;
        }

        public void setFk_direction_to(String fk_direction_to) {
            this.fk_direction_to = fk_direction_to;
        }

        public String getFk_direction_to() {
            return fk_direction_to;
        }

        public void setDn_load_discharge_flag(String dn_load_discharge_flag) {
            this.dn_load_discharge_flag = dn_load_discharge_flag;
        }

        public String getDn_load_discharge_flag() {
            return dn_load_discharge_flag;
        }

        public void setDn_transhipment_flag(String dn_transhipment_flag) {
            this.dn_transhipment_flag = dn_transhipment_flag;
        }

        public String getDn_transhipment_flag() {
            return dn_transhipment_flag;
        }

        public void setDn_wayport_trunk_indicator(String dn_wayport_trunk_indicator) {
            this.dn_wayport_trunk_indicator = dn_wayport_trunk_indicator;
        }

        public String getDn_wayport_trunk_indicator() {
            return dn_wayport_trunk_indicator;
        }

        public void setPort_call_level(String port_call_level) {
            this.port_call_level = port_call_level;
        }

        public String getPort_call_level() {
            return port_call_level;
        }

        public void setPort_call_tolerant(String port_call_tolerant) {
            this.port_call_tolerant = port_call_tolerant;
        }

        public String getPort_call_tolerant() {
            return port_call_tolerant;
        }

        public void setSlot_teu(String slot_teu) {
            this.slot_teu = slot_teu;
        }

        public String getSlot_teu() {
            return slot_teu;
        }

        public void setSlot_tons(String slot_tons) {
            this.slot_tons = slot_tons;
        }

        public String getSlot_tons() {
            return slot_tons;
        }

        public void setSlot_reefer_plugs(String slot_reefer_plugs) {
            this.slot_reefer_plugs = slot_reefer_plugs;
        }

        public String getSlot_reefer_plugs() {
            return slot_reefer_plugs;
        }

        public void setAvg_coc_teu_weight(String avg_coc_teu_weight) {
            this.avg_coc_teu_weight = avg_coc_teu_weight;
        }

        public String getAvg_coc_teu_weight() {
            return avg_coc_teu_weight;
        }

        public void setAvg_soc_teu_weight(String avg_soc_teu_weight) {
            this.avg_soc_teu_weight = avg_soc_teu_weight;
        }

        public String getAvg_soc_teu_weight() {
            return avg_soc_teu_weight;
        }

        public void setMin_teu(String min_teu) {
            this.min_teu = min_teu;
        }

        public String getMin_teu() {
            return min_teu;
        }

        public void setPort_call_record_status(String port_call_record_status) {
            this.port_call_record_status = port_call_record_status;
        }

        public String getPort_call_record_status() {
            return port_call_record_status;
        }

        public void setFrom_to(String from_to) {
            this.from_to = from_to;
        }

        public String getFrom_to() {
            return from_to;
        }

        public void setDn_transhipment_port(String dn_transhipment_port) {
            this.dn_transhipment_port = dn_transhipment_port;
        }

        public String getDn_transhipment_port() {
            return dn_transhipment_port;
        }

    public void setImport_id(String import_id) {
        this.import_id = import_id;
    }

    public String getImport_id() {
        return import_id;
    }

    public void setErrorMessageOfRow(String errorMessageOfRow) {
        this.errorMessageOfRow = errorMessageOfRow;
    }

    public String getErrorMessageOfRow() {
        return errorMessageOfRow;
    }
}
