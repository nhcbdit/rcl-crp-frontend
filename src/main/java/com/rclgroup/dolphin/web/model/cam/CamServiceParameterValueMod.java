package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamServiceParameterValueMod extends RrcStandardMod {
    
    private String parameterValueId;
    private String parameterValueCode;
    private String parameterValueName;
    private String staticSqlFlag;
    private String sqlStatement;
    private String staticUserParam;
    private String detail;

    private String recordChangeDateStr;
    
    public CamServiceParameterValueMod() {
    
        parameterValueId   ="";
        parameterValueCode ="";
        parameterValueName ="";
        staticSqlFlag       ="";
        sqlStatement        ="";
        staticUserParam     ="";
        recordChangeDateStr ="";
       
    }


    public void setParameterValueId(String parameterValueId) {
        this.parameterValueId = parameterValueId;
    }

    public String getParameterValueId() {
        return parameterValueId;
    }

    public void setParameterValueCode(String parameterValueCode) {
        this.parameterValueCode = parameterValueCode;
    }

    public String getParameterValueCode() {
        return parameterValueCode;
    }

    public void setParameterValueName(String parameterValueName) {
        this.parameterValueName = parameterValueName;
    }

    public String getParameterValueName() {
        return parameterValueName;
    }

    public void setStaticSqlFlag(String staticSqlFlag) {
        this.staticSqlFlag = staticSqlFlag;
    }

    public String getStaticSqlFlag() {
        return staticSqlFlag;
    }

    public void setSqlStatement(String sqlStatement) {
        this.sqlStatement = sqlStatement;
    }

    public String getSqlStatement() {
        return sqlStatement;
    }

    public void setStaticUserParam(String staticUserParam) {
        this.staticUserParam = staticUserParam;
    }

    public String getStaticUserParam() {
        return staticUserParam;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setRecordChangeDateStr(String recordChangeDateStr) {
        this.recordChangeDateStr = recordChangeDateStr;
    }

    public String getRecordChangeDateStr() {
        return recordChangeDateStr;
    }
}
