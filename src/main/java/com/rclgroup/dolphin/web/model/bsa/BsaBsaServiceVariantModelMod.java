package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BsaBsaServiceVariantModelMod  extends RrcStandardMod{
    private String import_id;
    private String model_name;
    private String simulation_flag;
    private String valid_from;
    private String valid_to;
    private String avg_mt_teu_weight;
    private String imbalance_warning_perc;
    private String model_calc_duration;
    private String model_record_status;
    private String errorMessageOfRow;
    
    
    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setSimulation_flag(String simulation_flag) {
        this.simulation_flag = simulation_flag;
    }

    public String getSimulation_flag() {
        return simulation_flag;
    }

    public void setValid_from(String valid_from) {
        this.valid_from = valid_from;
    }

    public String getValid_from() {
        return valid_from;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public String getValid_to() {
        return valid_to;
    }

    public void setAvg_mt_teu_weight(String avg_mt_teu_weight) {
        this.avg_mt_teu_weight = avg_mt_teu_weight;
    }

    public String getAvg_mt_teu_weight() {
        return avg_mt_teu_weight;
    }

    public void setImbalance_warning_perc(String imbalance_warning_perc) {
        this.imbalance_warning_perc = imbalance_warning_perc;
    }

    public String getImbalance_warning_perc() {
        return imbalance_warning_perc;
    }

    public void setModel_calc_duration(String model_calc_duration) {
        this.model_calc_duration = model_calc_duration;
    }

    public String getModel_calc_duration() {
        return model_calc_duration;
    }

    public void setModel_record_status(String model_record_status) {
        this.model_record_status = model_record_status;
    }

    public String getModel_record_status() {
        return model_record_status;
    }

    public void setImport_id(String import_id) {
        this.import_id = import_id;
    }

    public String getImport_id() {
        return import_id;
    }

    public void setErrorMessageOfRow(String errorMessageOfRow) {
        this.errorMessageOfRow = errorMessageOfRow;
    }

    public String getErrorMessageOfRow() {
        return errorMessageOfRow;
    }
}
