/*-----------------------------------------------------------------------------------------------------------  
FarFunlocMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2010
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 01/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class FarFunlocMod extends RrcStandardMod {   
    private String dataMapId;           //PK_DATA_MAP_ID
    private String billToParty;         //BILL_TO_PARTY
    private String billToPartyName;     //BILL_TO_PARTY_DESC    
    private String blNo;                //BL_NO
    private String invNo;               //INVOICE_NO_LIKE
    private String funloc;                //FUN_LOC
    private String markDesc;            //MARKS_DES_TEXT
    private String status;             //STATUS
    private String addUser;            //RECORD_ADD_USER
    private Timestamp addDate;          //RECORD_ADD_DATE
    private String changeUser;            //RECORD_CHANGE_USER
    private Timestamp changeDate;          //RECORD_CHANGE_DATE
    private int numIns;
    
    
    public FarFunlocMod() {     
        this.dataMapId = "";
        this.billToParty = "";
        this.billToPartyName = "";
        this.blNo = "";
        this.invNo = "";
        this.funloc = "";
        this.markDesc = "";
        this.status = "";        
        this.addUser = "";        
        this.changeUser = "";
        this.addDate = new Timestamp(0);
        this.changeDate = new Timestamp(0);
        this.numIns = 0;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setBillToPartyName(String billToPartyName) {
        this.billToPartyName = billToPartyName;
    }

    public String getBillToPartyName() {
        return billToPartyName;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setFunloc(String funloc) {
        this.funloc = funloc;
    }

    public String getFunloc() {
        return funloc;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setMarkDesc(String markDesc) {
        this.markDesc = markDesc;
    }

    public String getMarkDesc() {
        return markDesc;
    }

    public void setDataMapId(String dataMapId) {
        this.dataMapId = dataMapId;
    }

    public String getDataMapId() {
        return dataMapId;
    }

    public void setAddUser(String addUser) {
        this.addUser = addUser;
    }

    public String getAddUser() {
        return addUser;
    }

    public void setAddDate(Timestamp addDate) {
        this.addDate = addDate;
    }

    public Timestamp getAddDate() {
        return addDate;
    }

    public void setChangeUser(String changeUser) {
        this.changeUser = changeUser;
    }

    public String getChangeUser() {
        return changeUser;
    }

    public void setChangeDate(Timestamp changeDate) {
        this.changeDate = changeDate;
    }

    public Timestamp getChangeDate() {
        return changeDate;
    }

    public void setNumIns(int numIns) {
        this.numIns = numIns;
    }

    public int getNumIns() {
        return numIns;
    }
}
