package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosMidStreamMod extends RrcStandardMod{

    private String typeCode;
    private String typeDesc;
    public TosMidStreamMod() {
        super();
        typeCode = "";
        typeDesc = "";
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getTypeDesc() {
        return typeDesc;
    }
}
