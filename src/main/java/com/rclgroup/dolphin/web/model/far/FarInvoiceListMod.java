/*-----------------------------------------------------------------------------------------------------------  
FarInvoiceListMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2010
-------------------------------------------------------------------------------------------------------------
Author Leena Babu 21/07/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 17/10/13  LEE    PD_CR_20130830 :Allow Control FSC to print Invoice List under 
                      their Control 
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class FarInvoiceListMod extends RrcStandardMod{
     
    private String dateFrm;
    private String dateTo;   
    private String sessionId;
    private String usrId;
    
    private String line;
    private String trade;
    private String agent;
    private String fsc;
    
    private String customerCode; 
    private String collFsc;
    private String reportFormat;
    private String showAllFsc; //##01
    private boolean isControlFsc;//##01
    
    public FarInvoiceListMod() {
        dateFrm = "";
        dateTo = "";
        sessionId = "";
        usrId = "";
        line = "";
        trade = "";
        agent = "";
        fsc = "";        
        customerCode = "";
        collFsc = "";
        reportFormat = "";
        showAllFsc = ""; //##01
        isControlFsc = false; //##01
        }

    public String getDateFrm() {
        return dateFrm;
    }

    public void setDateFrm(String dateFrm) {
        this.dateFrm = dateFrm;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsrId() {
        return usrId;
    }

    public void setUsrId(String usrId) {
        this.usrId = usrId;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getFsc() {
        return fsc;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCollFsc() {
        return collFsc;
    }

    public void setCollFsc(String collFsc) {
        this.collFsc = collFsc;
    }

    public String getReportFormat() {
        return reportFormat;
    }

    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }
    
    //##01 begin
    public void setShowAllFsc(String showAllFsc) {
        this.showAllFsc = showAllFsc;
    }

    public String getShowAllFsc() {
        return showAllFsc;
    }

    public void setIsControlFsc(boolean isControlFsc) {
        this.isControlFsc = isControlFsc;
    }

    public boolean isIsControlFsc() {
        return isControlFsc;
    }
    //##01 end
}
