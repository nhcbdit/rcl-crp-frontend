/*-----------------------------------------------------------------------------------------------------------  
FarInvoicBillStatementMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 17/06/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.util.RutDate;

public class FarInvoicBillStatementMod {

    private String fsc;
    private String billToParty;
    private String billToPartyName;
    private String billNo;
    private int version;
    private String billDate;
    private String description;
    private String billStatus;
    
    public FarInvoicBillStatementMod() {
        fsc = "";
        billToParty = "";
        billToPartyName = "";
        billNo = "";
        version = 0;
        billDate = "";
        description = "";
        billStatus = "";
    }
    
    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setBillToPartyName(String billToPartyName) {
        this.billToPartyName = billToPartyName;
    }

    public String getBillToPartyName() {
        return billToPartyName;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getVersion() {
        return version;
    } 
}
