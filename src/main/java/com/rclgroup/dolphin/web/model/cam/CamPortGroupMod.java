/*------------------------------------------------------
CamPortGroupMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 02/09/2008 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamPortGroupMod extends RrcStandardMod {
     private String portGroupCode;
     private String portGroupName;
     private String countryCode;
     private String portGroupStatus;

     public CamPortGroupMod() {
         portGroupCode = "";
         portGroupName = "";
         countryCode = "";
         portGroupStatus = "";
     }   

     public String getPortGroupCode() {
         return portGroupCode;
     }

     public void setPortGroupCode(String portGroupCode) {
         this.portGroupCode = portGroupCode;
     }

     public String getPortGroupName() {
         return portGroupName;
     }

     public void setPortGroupName(String portGroupName) {
         this.portGroupName = portGroupName;
     }

     public String getPortGroupStatus() {
         return portGroupStatus;
     }

     public void setPortGroupStatus(String portGroupStatus) {
         this.portGroupStatus = portGroupStatus;
     }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
