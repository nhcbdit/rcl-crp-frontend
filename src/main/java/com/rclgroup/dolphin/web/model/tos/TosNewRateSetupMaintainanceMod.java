package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewRateSetupMaintainanceMod extends RrcStandardMod {
    private String line;
    private String port;
    private String terminal;
    private String service;
    private String opr_code;
    private String vendor_code;
    private String vendor_seq;
    private String activity_code;
    private String parent_activity_code;
    private String description;
    private String ocean_coast;
    private String charge_code;
    private String pod_ocean_coast;
    private String activity_rate;
    private String rate_basis;
    private String col1_rate;
    private String col2_rate;
    private String col3_rate;
    private String col4_rate;
    private String col5_rate;
    private String col6_rate;
    private String col7_rate;
    private String col8_rate;
    private String col9_rate;
    private String col10_rate;
    private String col11_rate;
    private String col12_rate;
    private String col13_rate;
    private String col14_rate;
    private String col15_rate;
    private String col16_rate;
    private String col17_rate;
    private String col18_rate;
    private String col19_rate;
    private String col20_rate;
    private String record_status;
    private String tos_rate_seqno;
    private String include_yn;
    private String catg_based_yn;
    private String auto_yn;
    private String coc_soc;
    private String ship_term;
    private String crane_type;
    private String vessel_type;
    private String operation_type;
    private String tos_rate_seq_dtl;
    private String load_disch_tran_flag; 
    private String auto_activity; 
    private String destination_type; 
    private String destination_code; 
    private String mot; 
    private String weight_min; 
    private String weight_max; 
    private String weekday_weekend; 
    private String holiday; 
    private String day_night; 
    private String reason_code; 
    private String dom_inter;
    private String ship_type;
    
    private boolean isDelete;
    private int dataStatus;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewRateSetupMaintainanceMod() {
        super();

        line = "";
        port = "";
        terminal = "";
        service = "";
        opr_code = "";
        vendor_code = "";
        vendor_seq = "";
        activity_code = "";
        parent_activity_code = "";
        description = "";
        ocean_coast = "";
        charge_code = "";
        pod_ocean_coast = "";
        activity_rate = "";
        rate_basis = "";
        col1_rate = "";
        col2_rate = "";
        col3_rate = "";
        col4_rate = "";
        col5_rate = "";
        col6_rate = "";
        col7_rate = "";
        col8_rate = "";
        col9_rate = "";
        col10_rate = "";
        col11_rate = "";
        col12_rate = "";
        col13_rate = "";
        col14_rate = "";
        col15_rate = "";
        col16_rate = "";
        col17_rate = "";
        col18_rate = "";
        col19_rate = "";
        col20_rate = "";
        record_status = "";
        tos_rate_seqno = "";
        include_yn = "";
        catg_based_yn = "Y";
        auto_yn = "N";
        coc_soc = "";
        ship_term = "";
        crane_type = "";
        vessel_type = "";
        operation_type = "";
        tos_rate_seq_dtl = "";
        isDelete = false;
        dataStatus = 1;        
        INSERT = 1;
        UPDATE = 0;
        load_disch_tran_flag = ""; 
        auto_activity = "N"; 
        destination_type = ""; 
        destination_code = ""; 
        mot = ""; 
        weight_min = ""; 
        weight_max = ""; 
        weekday_weekend = ""; 
        holiday = "N"; 
        day_night = ""; 
        reason_code = "";
        dom_inter = "";
        ship_type = "";
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setOpr_code(String opr_code) {
        this.opr_code = opr_code;
    }

    public String getOpr_code() {
        return opr_code;
    }

    public void setVendor_code(String vendor_code) {
        this.vendor_code = vendor_code;
    }

    public String getVendor_code() {
        return vendor_code;
    }

    public void setVendor_seq(String vendor_seq) {
        this.vendor_seq = vendor_seq;
    }

    public String getVendor_seq() {
        return vendor_seq;
    }

    public void setActivity_code(String activity_code) {
        this.activity_code = activity_code;
    }

    public String getActivity_code() {
        return activity_code;
    }

    public void setParent_activity_code(String parent_activity_code) {
        this.parent_activity_code = parent_activity_code;
    }

    public String getParent_activity_code() {
        return parent_activity_code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setOcean_coast(String ocean_coast) {
        this.ocean_coast = ocean_coast;
    }

    public String getOcean_coast() {
        return ocean_coast;
    }

    public void setCharge_code(String charge_code) {
        this.charge_code = charge_code;
    }

    public String getCharge_code() {
        return charge_code;
    }

    public void setPod_ocean_coast(String pod_ocean_coast) {
        this.pod_ocean_coast = pod_ocean_coast;
    }

    public String getPod_ocean_coast() {
        return pod_ocean_coast;
    }

    public void setActivity_rate(String activity_rate) {
        this.activity_rate = activity_rate;
    }

    public String getActivity_rate() {
        return activity_rate;
    }

    public void setRate_basis(String rate_basis) {
        this.rate_basis = rate_basis;
    }

    public String getRate_basis() {
        return rate_basis;
    }

    public void setCol1_rate(String col1_rate) {
        this.col1_rate = col1_rate;
    }

    public String getCol1_rate() {
        return col1_rate;
    }

    public void setCol2_rate(String col2_rate) {
        this.col2_rate = col2_rate;
    }

    public String getCol2_rate() {
        return col2_rate;
    }

    public void setCol3_rate(String col3_rate) {
        this.col3_rate = col3_rate;
    }

    public String getCol3_rate() {
        return col3_rate;
    }

    public void setCol4_rate(String col4_rate) {
        this.col4_rate = col4_rate;
    }

    public String getCol4_rate() {
        return col4_rate;
    }

    public void setCol5_rate(String col5_rate) {
        this.col5_rate = col5_rate;
    }

    public String getCol5_rate() {
        return col5_rate;
    }

    public void setCol6_rate(String col6_rate) {
        this.col6_rate = col6_rate;
    }

    public String getCol6_rate() {
        return col6_rate;
    }

    public void setCol7_rate(String col7_rate) {
        this.col7_rate = col7_rate;
    }

    public String getCol7_rate() {
        return col7_rate;
    }

    public void setCol8_rate(String col8_rate) {
        this.col8_rate = col8_rate;
    }

    public String getCol8_rate() {
        return col8_rate;
    }

    public void setCol9_rate(String col9_rate) {
        this.col9_rate = col9_rate;
    }

    public String getCol9_rate() {
        return col9_rate;
    }

    public void setCol10_rate(String col10_rate) {
        this.col10_rate = col10_rate;
    }

    public String getCol10_rate() {
        return col10_rate;
    }

    public void setCol11_rate(String col11_rate) {
        this.col11_rate = col11_rate;
    }

    public String getCol11_rate() {
        return col11_rate;
    }

    public void setCol12_rate(String col12_rate) {
        this.col12_rate = col12_rate;
    }

    public String getCol12_rate() {
        return col12_rate;
    }

    public void setCol13_rate(String col13_rate) {
        this.col13_rate = col13_rate;
    }

    public String getCol13_rate() {
        return col13_rate;
    }

    public void setCol14_rate(String col14_rate) {
        this.col14_rate = col14_rate;
    }

    public String getCol14_rate() {
        return col14_rate;
    }

    public void setCol15_rate(String col15_rate) {
        this.col15_rate = col15_rate;
    }

    public String getCol15_rate() {
        return col15_rate;
    }

    public void setCol16_rate(String col16_rate) {
        this.col16_rate = col16_rate;
    }

    public String getCol16_rate() {
        return col16_rate;
    }

    public void setCol17_rate(String col17_rate) {
        this.col17_rate = col17_rate;
    }

    public String getCol17_rate() {
        return col17_rate;
    }

    public void setCol18_rate(String col18_rate) {
        this.col18_rate = col18_rate;
    }

    public String getCol18_rate() {
        return col18_rate;
    }

    public void setCol19_rate(String col19_rate) {
        this.col19_rate = col19_rate;
    }

    public String getCol19_rate() {
        return col19_rate;
    }

    public void setCol20_rate(String col20_rate) {
        this.col20_rate = col20_rate;
    }

    public String getCol20_rate() {
        return col20_rate;
    }

    public void setRecord_status(String record_status) {
        this.record_status = record_status;
    }

    public String getRecord_status() {
        return record_status;
    }

    public void setTos_rate_seqno(String tos_rate_seqno) {
        this.tos_rate_seqno = tos_rate_seqno;
    }

    public String getTos_rate_seqno() {
        return tos_rate_seqno;
    }

    public void setInclude_yn(String include_yn) {
        this.include_yn = include_yn;
    }

    public String getInclude_yn() {
        return include_yn;
    }

    public void setCatg_based_yn(String catg_based_yn) {
        this.catg_based_yn = catg_based_yn;
    }

    public String getCatg_based_yn() {
        return catg_based_yn;
    }

    public void setAuto_yn(String auto_yn) {
        this.auto_yn = auto_yn;
    }

    public String getAuto_yn() {
        return auto_yn;
    }

    public void setCoc_soc(String coc_soc) {
        this.coc_soc = coc_soc;
    }

    public String getCoc_soc() {
        return coc_soc;
    }

    public void setShip_term(String ship_term) {
        this.ship_term = ship_term;
    }

    public String getShip_term() {
        return ship_term;
    }

    public void setCrane_type(String crane_type) {
        this.crane_type = crane_type;
    }

    public String getCrane_type() {
        return crane_type;
    }

    public void setVessel_type(String vessel_type) {
        this.vessel_type = vessel_type;
    }

    public String getVessel_type() {
        return vessel_type;
    }

    public void setOperation_type(String operation_type) {
        this.operation_type = operation_type;
    }

    public String getOperation_type() {
        return operation_type;
    }

    public void setTos_rate_seq_dtl(String tos_rate_seq_dtl) {
        this.tos_rate_seq_dtl = tos_rate_seq_dtl;
    }

    public String getTos_rate_seq_dtl() {
        return tos_rate_seq_dtl;
    }

    public String getRate(int column) {
        if (column == 1)
            return getCol1_rate();
        else if (column == 2)
            return getCol2_rate();
        else if (column == 3)
            return getCol3_rate();
        else if (column == 4)
            return getCol4_rate();
        else if (column == 5)
            return getCol5_rate();
        else if (column == 6)
            return getCol6_rate();
        else if (column == 7)
            return getCol7_rate();
        else if (column == 8)
            return getCol8_rate();
        else if (column == 9)
            return getCol9_rate();
        else if (column == 10)
            return getCol10_rate();
        else if (column == 11)
            return getCol11_rate();
        else if (column == 12)
            return getCol12_rate();
        else if (column == 13)
            return getCol13_rate();
        else if (column == 14)
            return getCol14_rate();
        else if (column == 15)
            return getCol15_rate();
        else if (column == 16)
            return getCol16_rate();
        else if (column == 17)
            return getCol17_rate();
        else if (column == 18)
            return getCol18_rate();
        else if (column == 19)
            return getCol19_rate();
        else if (column == 20)
            return getCol20_rate();
        else
            return "0";
    }
    
    public void setRate (int column, String rate) {
        if (column == 1)
            setCol1_rate(rate);
        else if (column == 2)
            setCol2_rate(rate);
        else if (column == 3)
            setCol3_rate(rate);
        else if (column == 4)
            setCol4_rate(rate);
        else if (column == 5)
            setCol5_rate(rate);
        else if (column == 6)
            setCol6_rate(rate);
        else if (column == 7)
            setCol7_rate(rate);
        else if (column == 8)
            setCol8_rate(rate);
        else if (column == 9)
            setCol9_rate(rate);
        else if (column == 10)
            setCol10_rate(rate);
        else if (column == 11)
            setCol11_rate(rate);
        else if (column == 12)
            setCol12_rate(rate);
        else if (column == 13)
            setCol13_rate(rate);
        else if (column == 14)
            setCol14_rate(rate);
        else if (column == 15)
            setCol15_rate(rate);
        else if (column == 16)
            setCol16_rate(rate);
        else if (column == 17)
            setCol17_rate(rate);
        else if (column == 18)
            setCol18_rate(rate);
        else if (column == 19)
            setCol19_rate(rate);
        else if (column == 20)
            setCol20_rate(rate);
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDataStatus(int dataStatus) {
        this.dataStatus = dataStatus;
    }

    public int getDataStatus() {
        return dataStatus;
    }

    public void setLoad_disch_tran_flag(String load_disch_tran_flag) {
        this.load_disch_tran_flag = load_disch_tran_flag;
    }

    public String getLoad_disch_tran_flag() {
        return load_disch_tran_flag;
    }

    public void setAuto_activity(String auto_activity) {
        this.auto_activity = auto_activity;
    }

    public String getAuto_activity() {
        return auto_activity;
    }

    public void setDestination_type(String destination_type) {
        this.destination_type = destination_type;
    }

    public String getDestination_type() {
        return destination_type;
    }

    public void setDestination_code(String destination_code) {
        this.destination_code = destination_code;
    }

    public String getDestination_code() {
        return destination_code;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public String getMot() {
        return mot;
    }

    public void setWeight_min(String weight_min) {
        this.weight_min = weight_min;
    }

    public String getWeight_min() {
        return weight_min;
    }

    public void setWeight_max(String weight_max) {
        this.weight_max = weight_max;
    }

    public String getWeight_max() {
        return weight_max;
    }

    public void setWeekday_weekend(String weekday_weekend) {
        this.weekday_weekend = weekday_weekend;
    }

    public String getWeekday_weekend() {
        return weekday_weekend;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setDay_night(String day_night) {
        this.day_night = day_night;
    }

    public String getDay_night() {
        return day_night;
    }

    public void setReason_code(String reason_code) {
        this.reason_code = reason_code;
    }

    public String getReason_code() {
        return reason_code;
    }

    public void setDom_inter(String dom_inter) {
        this.dom_inter = dom_inter;
    }

    public String getDom_inter() {
        return dom_inter;
    }

    public void setShip_type(String ship_type) {
        this.ship_type = ship_type;
    }

    public String getShip_type() {
        return ship_type;
    }
}
