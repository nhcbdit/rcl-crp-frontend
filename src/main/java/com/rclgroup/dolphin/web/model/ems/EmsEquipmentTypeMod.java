/*------------------------------------------------------
EmsEquipmentTypeMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 10/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
01 18/10/07 MNW              Added listForHelpScreen()
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class EmsEquipmentTypeMod extends RrcStandardMod {
    private String categoryCode;
    private String equipmentType;
    private String equipmentTypeName;
    private String equipmentTypeStatus;

    public EmsEquipmentTypeMod() {
        categoryCode = "";
        equipmentType = "";
        equipmentTypeName = "";
        equipmentTypeStatus = "";
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getEquipmentTypeName() {
        return equipmentTypeName;
    }

    public void setEquipmentTypeName(String equipmentTypeName) {
        this.equipmentTypeName = equipmentTypeName;
    }

    public String getEquipmentTypeStatus() {
        return equipmentTypeStatus;
    }

    public void setEquipmentTypeStatus(String equipmentTypeStatus) {
        this.equipmentTypeStatus = equipmentTypeStatus;
    }
}