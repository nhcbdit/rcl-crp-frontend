package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamRseLookupMasterMod extends RrcStandardMod {


    private String lookupId;
    private String lookupCode;
    private String lookupName;
    private String lookupDescr;
    private String lookupType;
    private String lookupReturns;

    private String recordChangeDateStr;
    
    public CamRseLookupMasterMod() {
    
        lookupId   ="";
        lookupCode ="";
        lookupName ="";
        lookupDescr       ="";
        lookupType        ="";
        lookupReturns     ="";
        recordChangeDateStr ="";
    }


    public void setLookupId(String lookupId) {
        this.lookupId = lookupId;
    }

    public String getLookupId() {
        return lookupId;
    }

    public void setLookupCode(String lookupCode) {
        this.lookupCode = lookupCode;
    }

    public String getLookupCode() {
        return lookupCode;
    }

    public void setLookupName(String lookupName) {
        this.lookupName = lookupName;
    }

    public String getLookupName() {
        return lookupName;
    }

    public void setLookupDescr(String lookupDescr) {
        this.lookupDescr = lookupDescr;
    }

    public String getLookupDescr() {
        return lookupDescr;
    }

    public void setLookupType(String lookupType) {
        this.lookupType = lookupType;
    }

    public String getLookupType() {
        return lookupType;
    }

    public void setLookupReturns(String lookupReturns) {
        this.lookupReturns = lookupReturns;
    }

    public String getLookupReturns() {
        return lookupReturns;
    }

    public void setRecordChangeDateStr(String recordChangeDateStr) {
        this.recordChangeDateStr = recordChangeDateStr;
    }

    public String getRecordChangeDateStr() {
        return recordChangeDateStr;
    }
}
