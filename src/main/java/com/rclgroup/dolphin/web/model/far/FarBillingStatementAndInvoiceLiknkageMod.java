package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class FarBillingStatementAndInvoiceLiknkageMod extends RrcStandardMod {
    
    private String invLinkNum;
    private String invLinkParty;
    private String invLinkPartyName;
    private String invLinkDesc;
    private String invLinkStatus;
    private String invLinkFsc;
    private int invLinkBillVersion;
    public FarBillingStatementAndInvoiceLiknkageMod() {
        invLinkNum = "";
        invLinkParty = "";
        invLinkPartyName = ""; 
        invLinkDesc = "";
        invLinkStatus = "";
        invLinkFsc = "";    
        invLinkBillVersion = 0;
    }


    public void setInvLinkNum(String invLinkNum) {
        this.invLinkNum = invLinkNum;
    }

    public String getInvLinkNum() {
        return invLinkNum;
    }

    public void setInvLinkParty(String invLinkParty) {
        this.invLinkParty = invLinkParty;
    }

    public String getInvLinkParty() {
        return invLinkParty;
    }

    public void setInvLinkPartyName(String invLinkPartyName) {
        this.invLinkPartyName = invLinkPartyName;
    }

    public String getInvLinkPartyName() {
        return invLinkPartyName;
    }

    public void setInvLinkDesc(String invLinkDesc) {
        this.invLinkDesc = invLinkDesc;
    }

    public String getInvLinkDesc() {
        return invLinkDesc;
    }

    public void setInvLinkStatus(String invLinkStatus) {
        this.invLinkStatus = invLinkStatus;
    }

    public String getInvLinkStatus() {
        return invLinkStatus;
    }

    public void setInvLinkFsc(String invLinkFsc) {
        this.invLinkFsc = invLinkFsc;
    }

    public String getInvLinkFsc() {
        return invLinkFsc;
    }

    public void setInvLinkBillVersion(int invLinkBillVersion) {
        this.invLinkBillVersion = invLinkBillVersion;
    }

    public int getInvLinkBillVersion() {
        return invLinkBillVersion;
    }
}
