/*--------------------------------------------------------
DexInvoiceCancelledBlValidMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Nipun Sutes 26/04/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dex;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DexInvoiceCancelledBlValidMod extends RrcStandardMod {
    private String sessionId;
    private String userId;
    private String service;
    private String vessel;
    private String voyage;
    private String port;
    private String pod;
    private String fromDate;
    private String toDate;
    private String blStatus;
    
    public DexInvoiceCancelledBlValidMod() {
        sessionId = "";
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }
    
    public String getFromDateFormat(){
        // for format dd/mm/yyyy
        String stringDate = this.fromDate;
        if(stringDate!=null && !stringDate.equals("")){
            String arrayDate[] = stringDate.split("/");
            stringDate = arrayDate[2]+arrayDate[1]+arrayDate[0];
        }
        
        return stringDate;
    }
    
    public String getToDateFormat(){
        // for format dd/mm/yyyy
        String stringDate = this.toDate;
        if(stringDate!=null && !stringDate.equals("")){
            String arrayDate[] = stringDate.split("/");
            stringDate = arrayDate[2]+arrayDate[1]+arrayDate[0];
        }
        
        return stringDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setBlStatus(String blStatus) {
        this.blStatus = blStatus;
    }

    public String getBlStatus() {
        return blStatus;
    }
}
