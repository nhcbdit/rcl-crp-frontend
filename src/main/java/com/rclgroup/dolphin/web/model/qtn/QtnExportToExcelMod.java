/*------------------------------------------------------
QtnExportToExcelMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2008
--------------------------------------------------------
 Author Kitti Pongsirisakun 01/12/2008 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
02 15/01/09  KIT    BUG.163   Export data should be tested with Import data 
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.util.Vector;


public class QtnExportToExcelMod extends RrcStandardMod{
    private String quotationNo;
    private String refQuotation;
    private String partyCode;
    private String partyName;
    private String status;
    private String effDate;
    private String expDate;
    private String followDate;
    private String tel;
    private String email;
    private String remarks;
    private String por;
    private String pol;
    private String ts;
    private String pod;
    private String del;
    private String shipType;
    private String term;
    private String lineOfBus;
    private String service;
    private String polSts;
    private int size;
    private String type;
    private String podSts;
    private int ldnQty;
    private int mtQty;
    private String rateType;
    private String portClass;
    private String commodityCode;
    private String commodityrRateType;
    private double freight;
    private String freightPlaceOfSettle;
    private String freigthCur;
    
    private int polDem;
    private int polDet;
    private int podDem;
    private int podDet;
    private int corridorSeq;
    private Vector surchargeVec;
    
    
    public QtnExportToExcelMod() {
        quotationNo = "";
        refQuotation = "";
        partyCode = "";
        status = "";
        effDate = "";
        expDate = "";
        followDate = "";
        tel = "";
        email = "";
        remarks = "";
        por = "";
        pol = "";
        ts = "";
        pod = "";
        del = "";
        shipType = "";
        term = "";
        lineOfBus = "";
        service = "";
        polSts = "";
        size = 0;
        type = "";
        podSts = "";
        ldnQty = 0;
        mtQty = 0;
        rateType = "";
        portClass = "";
        commodityCode = "";
        commodityrRateType = "";
        freight = 0;
        freightPlaceOfSettle = "";
        freigthCur = "";
        polDem = 0;
        polDet = 0;
        podDem = 0;
        podDet = 0;
        corridorSeq = 0;
    }


    public void setQuotationNo(String quotationNo) {
        this.quotationNo = quotationNo;
    }

    public String getQuotationNo() {
        return quotationNo;
    }

    public void setRefQuotation(String refQuotation) {
        this.refQuotation = refQuotation;
    }

    public String getRefQuotation() {
        return refQuotation;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    public String getPartyCode() {
        return partyCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTel() {
        return tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPor(String por) {
        this.por = por;
    }

    public String getPor() {
        return por;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getTs() {
        return ts;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setDel(String del) {
        this.del = del;
    }

    public String getDel() {
        return del;
    }

    public void setShipType(String shipType) {
        this.shipType = shipType;
    }

    public String getShipType() {
        return shipType;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public void setLineOfBus(String lineOfBus) {
        this.lineOfBus = lineOfBus;
    }

    public String getLineOfBus() {
        return lineOfBus;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setPolSts(String polSts) {
        this.polSts = polSts;
    }

    public String getPolSts() {
        return polSts;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setPodSts(String podSts) {
        this.podSts = podSts;
    }

    public String getPodSts() {
        return podSts;
    }

    public void setLdnQty(int ldnQty) {
        this.ldnQty = ldnQty;
    }

    public int getLdnQty() {
        return ldnQty;
    }

    public void setMtQty(int mtQty) {
        this.mtQty = mtQty;
    }

    public int getMtQty() {
        return mtQty;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getRateType() {
        return rateType;
    }

    public void setPortClass(String portClass) {
        this.portClass = portClass;
    }

    public String getPortClass() {
        return portClass;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityrRateType(String commodityrRateType) {
        this.commodityrRateType = commodityrRateType;
    }

    public String getCommodityrRateType() {
        return commodityrRateType;
    }

    public void setFreight(double freight) {
        this.freight = freight;
    }

    public double getFreight() {
        return freight;
    }

    public void setFreightPlaceOfSettle(String freightPlaceOfSettle) {
        this.freightPlaceOfSettle = freightPlaceOfSettle;
    }

    public String getFreightPlaceOfSettle() {
        return freightPlaceOfSettle;
    }

    public void setFreigthCur(String freigthCur) {
        this.freigthCur = freigthCur;
    }

    public String getFreigthCur() {
        return freigthCur;
    }

    public void setPolDem(int polDem) {
        this.polDem = polDem;
    }

    public int getPolDem() {
        return polDem;
    }

    public void setPolDet(int polDet) {
        this.polDet = polDet;
    }

    public int getPolDet() {
        return polDet;
    }

    public void setPodDem(int podDem) {
        this.podDem = podDem;
    }

    public int getPodDem() {
        return podDem;
    }

    public void setPodDet(int podDet) {
        this.podDet = podDet;
    }

    public int getPodDet() {
        return podDet;
    }

    public void setSurchargeVec(Vector surchargeVec) {
        this.surchargeVec = surchargeVec;
    }

    public Vector getSurchargeVec() {
        return surchargeVec;
    }

    public void setCorridorSeq(int corridorSeq) {
        this.corridorSeq = corridorSeq;
    }

    public int getCorridorSeq() {
        return corridorSeq;
    }
}

