/*
 * -----------------------------------------------------------------------------
 * CamRseScheduleScriptMod.java
 * -----------------------------------------------------------------------------
 * Copyright RCL Public Co., Ltd. 2007 
 * -----------------------------------------------------------------------------
 * Author Dhruv Parekh 31/08/2012
 * ------- Change Log ----------------------------------------------------------
 * ##   DD/MM/YY    -User-      -TaskRef-      -Short Description  
 * -----------------------------------------------------------------------------
 */

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseScheduleScriptMod extends RrcStandardMod {
    
    private String scheduleId;
    private String scheduleCode;
    private String scheduleDesc;
    private String serviceId;
    private String fileFormat;
    private String scheduleType;
    private String choice;
    private String frequency;
    private String runTime;
    private String runDate;
    private String runDay;
    private String runMonth;
    private String orderly;
    private String monday;
    private String tuesday;
    private String wednesday;
    private String thursday;
    private String friday;
    private String saturday;
    private String sunday;
    private String directoryProcess;
    private String directoryHistory;
    private String startDate;
    private String endDateChoice;
    private String endDateOccurrence;
    private String endDate;
    private String recipientEmail;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    
    private String recordAddFsc;
    
    public CamRseScheduleScriptMod() {
        super();
        
        scheduleId = "";
        scheduleCode = "";
        scheduleDesc = "";
        serviceId = "";
        fileFormat = "";
        scheduleType = RcmConstant.SCHEDULE_TYPE_DEFAULT;
        choice = RcmConstant.SCHEDULE_CHOICE_1;
        frequency = "";
        runTime = "";
        runDate = "";
        runDay = RcmConstant.SCHEDULE_RUNDAY_DEFAULT;
        runMonth = RcmConstant.SCHEDULE_RUNMONTH_DEFAULT;
        orderly = RcmConstant.SCHEDULE_ORDERLY_DEFAULT;
        monday = RcmConstant.FLAG_NO;
        tuesday = RcmConstant.FLAG_NO;
        wednesday = RcmConstant.FLAG_NO;
        thursday = RcmConstant.FLAG_NO;
        friday = RcmConstant.FLAG_NO;
        saturday = RcmConstant.FLAG_NO;
        sunday = RcmConstant.FLAG_NO;
        directoryProcess = "";
        directoryHistory = "";
        startDate = "";
        endDateChoice = RcmConstant.SCHEDULE_ENDDATE_CHOICE_1;
        endDateOccurrence = "";
        endDate = "";
        recipientEmail = "";
        recordAddDate = null;
        recordChangeDate = null;
        recordAddFsc ="";
    }
    
    public CamRseScheduleScriptMod(CamRseScheduleScriptMod copy) {
        super();
        if (copy != null) {
            this.scheduleId = copy.getScheduleId();
            this.scheduleCode = copy.getScheduleCode();
            this.scheduleDesc = copy.getScheduleDesc();
            this.serviceId = copy.getServiceId();
            this.fileFormat = copy.getFileFormat();
            this.scheduleType = copy.getScheduleType();
            this.choice = copy.getChoice();
            this.frequency = copy.getFrequency();
            this.runTime = copy.getRunTime();
            this.runDate = copy.getRunDate();
            this.runDay = copy.getRunDay();
            this.runMonth = copy.getRunMonth();
            this.orderly = copy.getOrderly();
            this.monday = copy.getMonday();
            this.tuesday = copy.getTuesday();
            this.wednesday = copy.getWednesday();
            this.thursday = copy.getThursday();
            this.friday = copy.getFriday();
            this.saturday = copy.getSaturday();
            this.sunday = copy.getSunday();
            this.directoryProcess = copy.getDirectoryProcess();
            this.directoryHistory = copy.getDirectoryHistory();
            this.startDate = copy.getStartDate();
            this.endDateChoice = copy.getEndDateChoice();
            this.endDateOccurrence = copy.getEndDateOccurrence();
            this.endDate = copy.getEndDate();
            this.recipientEmail = copy.getRecipientEmail();
            this.recordAddDate = copy.getRecordAddDate(); 
            this.recordChangeDate = copy.getRecordChangeDate();
            this.recordStatus = copy.getRecordStatus();
            this.recordAddUser = copy.getRecordAddUser();
            this.recordChangeUser = copy.getRecordChangeUser();
        }
    }
    
    public boolean equals(Object object) {
        if (object instanceof CamRseScheduleMod) {
            CamRseScheduleScriptMod mod = (CamRseScheduleScriptMod) object;
            if (!this.scheduleId.equals(mod.getScheduleId())) {
                return false;
            } else if (!this.scheduleCode.equals(mod.getScheduleCode())) {
                return false;
            } else if (!this.scheduleDesc.equals(mod.getScheduleDesc())) {
                return false;
            } else if (!this.serviceId.equals(mod.getServiceId())) {
                return false;
            } else if (!this.fileFormat.equals(mod.getFileFormat())) {
                return false;
            } else if (!this.scheduleType.equals(mod.getScheduleType())) {
                return false;
            } else if (!this.choice.equals(mod.getChoice())) {
                return false;
            } else if (!this.frequency.equals(mod.getFrequency())) {
                return false;
            } else if (!this.runTime.equals(mod.getRunTime())) {
                return false;
            } else if (!this.runDate.equals(mod.getRunDate())) {
                return false;
            } else if (!this.runDay.equals(mod.getRunDay())) {
                return false;
            } else if (!this.runMonth.equals(mod.getRunMonth())) {
                return false;
            } else if (!this.orderly.equals(mod.getOrderly())) {
                return false;
            } else if (!this.monday.equals(mod.getMonday())) {
                return false;
            } else if (!this.tuesday.equals(mod.getTuesday())) {
                return false;
            } else if (!this.wednesday.equals(mod.getWednesday())) {
                return false;
            } else if (!this.thursday.equals(mod.getThursday())) {
                return false;
            } else if (!this.friday.equals(mod.getFriday())) {
                return false;
            } else if (this.saturday.equals(mod.getSaturday())) {
                return false;
            } else if (!this.sunday.equals(mod.getSunday())) {
                return false;                
            } else if (!this.directoryProcess.equals(mod.getDirectoryProcess())) {
                return false;
            } else if (!this.directoryHistory.equals(mod.getDirectoryHistory())) {
                return false;
            } else if (!this.startDate.equals(mod.getStartDate())) {
                return false;
            } else if (!this.endDateChoice.equals(mod.getEndDateChoice())) {
                return false;
            } else if (!this.endDateOccurrence.equals(mod.getEndDateOccurrence())) {
                return false;
            } else if (!this.endDate.equals(mod.getEndDate())) {
                return false;
            } else if (!this.recipientEmail.equals(mod.getRecipientEmail())) {
                return false;
            } else if (!this.recordStatus.equals(mod.getRecordStatus())) {
                return false;
            } else if (this.recordAddDate != null && !this.recordAddDate.equals(mod.getRecordAddDate())) {
                return false;
            } else if (this.recordChangeDate != null && !this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
    
    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }
    
    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleCode() {
        return scheduleCode;
    }

    public void setScheduleCode(String scheduleCode) {
        this.scheduleCode = scheduleCode;
    }

    public String getScheduleDesc() {
        return scheduleDesc;
    }

    public void setScheduleDesc(String scheduleDesc) {
        this.scheduleDesc = scheduleDesc;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getRunDate() {
        return runDate;
    }

    public void setRunDate(String runDate) {
        this.runDate = runDate;
    }

    public String getRunDay() {
        return runDay;
    }

    public void setRunDay(String runDay) {
        this.runDay = runDay;
    }

    public String getRunMonth() {
        return runMonth;
    }

    public void setRunMonth(String runMonth) {
        this.runMonth = runMonth;
    }

    public String getOrderly() {
        return orderly;
    }

    public void setOrderly(String orderly) {
        this.orderly = orderly;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getDirectoryProcess() {
        return directoryProcess;
    }

    public void setDirectoryProcess(String directoryProcess) {
        this.directoryProcess = directoryProcess;
    }

    public String getDirectoryHistory() {
        return directoryHistory;
    }

    public void setDirectoryHistory(String directoryHistory) {
        this.directoryHistory = directoryHistory;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDateChoice() {
        return endDateChoice;
    }

    public void setEndDateChoice(String endDateChoice) {
        this.endDateChoice = endDateChoice;
    }

    public String getEndDateOccurrence() {
        return endDateOccurrence;
    }

    public void setEndDateOccurrence(String endDateOccurrence) {
        this.endDateOccurrence = endDateOccurrence;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public void setRecordAddFsc(String recordAddFsc) {
        this.recordAddFsc = recordAddFsc;
    }

    public String getRecordAddFsc() {
        return recordAddFsc;
    }
}
