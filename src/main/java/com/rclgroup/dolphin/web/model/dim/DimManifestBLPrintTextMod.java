package com.rclgroup.dolphin.web.model.dim;

public class DimManifestBLPrintTextMod {
     private String vesSel;
     private String voyage;
     private String shipPer;
     private String conSigNee;
     private String notify;
     private String  bl;
     private String  por;
     private String  pol;
     private String  pod;
     private String  contNo;
     private String  sizeType;
     private String  sealNo;
     private String  packQty;   
     private String  description;
     private String  master;
     private int   GrWtTareWtinKG;
     private int  GrWtKg;
     private int  GrWtLb;
     private int  CBM;
     private int  CFT;
     private String PSN;
     private String PSNDesc;
     private String IMCO;
     private String UNNO;
     private String packingGrp;
     private String FND;
     private String marinePollutant;
     private String severeMarinePollutant;
     private String MPAGrp;
     private String temp;


    public void setVesSel(String vesSel) {
        this.vesSel = vesSel;
    }

    public String getVesSel() {
        return vesSel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setShipPer(String shipPer) {
        this.shipPer = shipPer;
    }

    public String getShipPer() {
        return shipPer;
    }

    public void setConSigNee(String conSigNee) {
        this.conSigNee = conSigNee;
    }

    public String getConSigNee() {
        return conSigNee;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public String getNotify() {
        return notify;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setPor(String por) {
        this.por = por;
    }

    public String getPor() {
        return por;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setSizeType(String sizeType) {
        this.sizeType = sizeType;
    }

    public String getSizeType() {
        return sizeType;
    }

    public void setSealNo(String sealNo) {
        this.sealNo = sealNo;
    }

    public String getSealNo() {
        return sealNo;
    }

    public void setPackQty(String packQty) {
        this.packQty = packQty;
    }

    public String getPackQty() {
        return packQty;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getMaster() {
        return master;
    }

    public void setGrWtTareWtinKG(int grWtTareWtinKG) {
        this.GrWtTareWtinKG = grWtTareWtinKG;
    }

    public int getGrWtTareWtinKG() {
        return GrWtTareWtinKG;
    }

    public void setGrWtKg(int grWtKg) {
        this.GrWtKg = grWtKg;
    }

    public int getGrWtKg() {
        return GrWtKg;
    }

    public void setGrWtLb(int grWtLb) {
        this.GrWtLb = grWtLb;
    }

    public int getGrWtLb() {
        return GrWtLb;
    }

    public void setCBM(int cBM) {
        this.CBM = cBM;
    }

    public int getCBM() {
        return CBM;
    }

    public void setCFT(int cFT) {
        this.CFT = cFT;
    }

    public int getCFT() {
        return CFT;
    }

    public void setPSN(String pSN) {
        this.PSN = pSN;
    }

    public String getPSN() {
        return PSN;
    }

    public void setPSNDesc(String pSNDesc) {
        this.PSNDesc = pSNDesc;
    }

    public String getPSNDesc() {
        return PSNDesc;
    }

    public void setIMCO(String iMCO) {
        this.IMCO = iMCO;
    }

    public String getIMCO() {
        return IMCO;
    }

    public void setUNNO(String uNNO) {
        this.UNNO = uNNO;
    }

    public String getUNNO() {
        return UNNO;
    }

    public void setPackingGrp(String packingGrp) {
        this.packingGrp = packingGrp;
    }

    public String getPackingGrp() {
        return packingGrp;
    }

    public void setFND(String fND) {
        this.FND = fND;
    }

    public String getFND() {
        return FND;
    }

    public void setMarinePollutant(String marinePollutant) {
        this.marinePollutant = marinePollutant;
    }

    public String getMarinePollutant() {
        return marinePollutant;
    }

    public void setSevereMarinePollutant(String severeMarinePollutant) {
        this.severeMarinePollutant = severeMarinePollutant;
    }

    public String getSevereMarinePollutant() {
        return severeMarinePollutant;
    }

    public void setMPAGrp(String mPAGrp) {
        this.MPAGrp = mPAGrp;
    }

    public String getMPAGrp() {
        return MPAGrp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTemp() {
        return temp;
    }
}
