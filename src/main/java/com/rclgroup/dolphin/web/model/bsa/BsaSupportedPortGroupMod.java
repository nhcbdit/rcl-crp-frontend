/*-----------------------------------------------------------------------------------------------------------  
BsaSupportedPortGroupMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 20/03/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class BsaSupportedPortGroupMod extends RrcStandardMod {
    public static final String PORT_GROUP_SOC = "S"; 

    private String bsaSupportedPortGroupId; // BSA_SUPPORTED_PORT_GROUP_ID
    private String bsaModelId; // BSA_MODEL_ID 
    private String portGroupCode; // PORT_GROUP_CODE
    private String portGroupName; // PORT_GROUP_NAME
    private String portGroupSocCoc; // PORT_GRP_SOC_COC
    private String service; // SERVICE
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;

    public BsaSupportedPortGroupMod cloneObject(Object object) {
        BsaSupportedPortGroupMod mod = null;
        if (object != null && object instanceof BsaSupportedPortGroupMod) {
            BsaSupportedPortGroupMod bean = (BsaSupportedPortGroupMod) object;
            mod = new BsaSupportedPortGroupMod();
            mod.setBsaSupportedPortGroupId(bean.getBsaSupportedPortGroupId());
            mod.setBsaModelId(bean.getBsaModelId());
            mod.setPortGroupCode(bean.getPortGroupCode());
            mod.setPortGroupName(bean.getPortGroupName());
            mod.setPortGroupSocCoc(bean.getPortGroupSocCoc());
            mod.setService(bean.getService());
            mod.setRecordAddUser(bean.getRecordAddUser());
            mod.setRecordAddDate(bean.getRecordAddDate());
            mod.setRecordChangeUser(bean.getRecordChangeUser());
            mod.setRecordChangeDate(bean.getRecordChangeDate());
            mod.setRecordStatus(bean.getRecordStatus());
        }
        return mod;
    }

    public BsaSupportedPortGroupMod(String bsaSupportedPortGroupId,
                          String bsaModelId,
                          String portGroupCode,
                          String portGroupName,
                          String portGroupSocCoc,
                          String service,
                          String recordStatus,
                          String recordAddUser,
                          Timestamp recordAddDate,
                          String recordChangeUser,
                          Timestamp recordChangeDate) {
        super(recordStatus,recordAddUser,recordChangeUser);
        this.bsaSupportedPortGroupId = bsaSupportedPortGroupId;
        this.bsaModelId = bsaModelId;
        this.portGroupCode = portGroupCode;
        this.portGroupName = portGroupName;
        this.portGroupSocCoc = portGroupSocCoc;
        this.service = service;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
    }
    
    public BsaSupportedPortGroupMod() {
        this("","","","",PORT_GROUP_SOC,"",RrcStandardMod.RECORD_STATUS_ACTIVE,"",new Timestamp(0),"",new Timestamp(0));
    }
    
    public BsaSupportedPortGroupMod(BsaSupportedPortGroupMod mod){
       this(mod.getBsaSupportedPortGroupId(),
            mod.getBsaModelId(),
            mod.getPortGroupCode(),
            mod.getPortGroupName(),
            mod.getPortGroupSocCoc(),
            mod.getService(),
            mod.getRecordStatus(),
            mod.getRecordAddUser(),
            mod.getRecordAddDate(),
            mod.getRecordChangeUser(),
            mod.getRecordChangeDate());
    }
    
    public boolean equals(Object object){
        if(object instanceof BsaSupportedPortGroupMod){
            BsaSupportedPortGroupMod mod = (BsaSupportedPortGroupMod)object;
            if(!this.bsaSupportedPortGroupId.equals(mod.getBsaSupportedPortGroupId())){
                return false;
            }else if(!this.bsaModelId.equals(mod.getBsaModelId())){
                return false;
            }else if(!this.portGroupCode.equals(mod.getPortGroupCode())){
                return false;
            }else if(!this.portGroupSocCoc.equals(mod.getPortGroupSocCoc())){
                return false;
            }else if(!this.service.equals(mod.getService())){
                return false;
            }else if(!this.recordStatus.equals(mod.getRecordStatus())){
                return false;
            }
        }else{
            return false;
        }
        return true;
    }

    public void setBsaSupportedPortGroupId(String bsaSupportedPortGroupId) {
        this.bsaSupportedPortGroupId = bsaSupportedPortGroupId;
    }

    public String getBsaSupportedPortGroupId() {
        return bsaSupportedPortGroupId;
    }

    public void setBsaModelId(String bsaModelId) {
        this.bsaModelId = bsaModelId;
    }

    public String getBsaModelId() {
        return bsaModelId;
    }

    public void setPortGroupCode(String portGroupCode) {
        this.portGroupCode = portGroupCode;
    }

    public String getPortGroupCode() {
        return portGroupCode;
    }
    
    public void setPortGroupName(String portGroupName) {
        this.portGroupName = portGroupName;
    }

    public String getPortGroupName() {
        return portGroupName;
    }

    public void setPortGroupSocCoc(String portGroupSocCoc) {
        this.portGroupSocCoc = portGroupSocCoc;
    }

    public String getPortGroupSocCoc() {
        return portGroupSocCoc;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }
}

