/*-----------------------------------------------------------------------------------------------------------  
EmsAreaMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Manop Wanngam 05/11/07 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 29/04/08  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.model.ems;

 import com.rclgroup.dolphin.web.common.RrcStandardMod;


 public class EmsAreaMod extends RrcStandardMod {
     private String areaCode;
     private String areaName;
     private String areaStatus;
     private String regionCode;

     public EmsAreaMod(String areaCode,String areaName,String areaStatus,String regionCode) {
         this.areaCode = areaCode;
         this.areaName = areaName;
         this.areaStatus = areaStatus;
         this.regionCode = regionCode;
     }
     
     public EmsAreaMod(){
        this("","","","");
     }
     
     public EmsAreaMod(EmsAreaMod mod){
        this(mod.getAreaCode(),mod.getAreaName(),mod.getAreaStatus(),mod.getRegionCode());
     }
     
     public String getAreaCode() {
         return areaCode;
     }

     public void setAreaCode(String areaCode) {
         this.areaCode = areaCode;
     }

     public String getAreaName() {
         return areaName;
     }

     public void setAreaName(String areaName) {
         this.areaName = areaName;
     }

     public String getAreaStatus() {
         return areaStatus;
     }

     public void setAreaStatus(String areaStatus) {
         this.areaStatus = areaStatus;
     }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }
}
