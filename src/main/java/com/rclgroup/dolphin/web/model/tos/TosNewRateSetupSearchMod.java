package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewRateSetupSearchMod extends RrcStandardMod {
    private String port;
    private String terminal;    
    private String status;
    private String operationType;
    private String rateReference;
    private String desc;
    private String effDate;
    private String expDate;
    private String rateStatus;
    private String currency;
    private String rateSeqNo;
    private String oprCode;
    private String stsCode;
    
    public TosNewRateSetupSearchMod() {
        super();
        port = "";
        terminal = ""; 
        status = "";
        operationType = "";
        rateReference = "";
        desc = "";
        effDate = "";
        expDate = "";
        rateStatus = "";
        currency = "";
        rateSeqNo = ""; 
        oprCode = "";
        stsCode = "S";
    }
    
    public TosNewRateSetupSearchMod(String port,
                                    String terminal,    
                                    String status,
                                    String operationType,
                                    String rateReference,
                                    String desc,
                                    String effDate,
                                    String expDate,
                                    String rateStatus,
                                    String currency,
                                    String rateSeqNo,
                                    String oprCode,
                                    String stsCode) {
        
        this.port = port;
        this.terminal = terminal;
        this.status = status;
        this.operationType = operationType;
        this.rateReference = rateReference;
        this.desc = desc;
        this.effDate = effDate;
        this.expDate = expDate;
        this.rateStatus = rateStatus;
        this.currency = currency;
        this.rateSeqNo = rateSeqNo;
        this.oprCode = oprCode;
        this.stsCode = stsCode;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setRateReference(String rateReference) {
        this.rateReference = rateReference;
    }

    public String getRateReference() {
        return rateReference;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setRateStatus(String rateStatus) {
        this.rateStatus = rateStatus;
    }

    public String getRateStatus() {
        return rateStatus;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setRateSeqNo(String rateSeqNo) {
        this.rateSeqNo = rateSeqNo;
    }

    public String getRateSeqNo() {
        return rateSeqNo;
    }

    public void setOprCode(String oprCode) {
        this.oprCode = oprCode;
    }

    public String getOprCode() {
        return oprCode;
    }

    public void setStsCode(String stsCode) {
        this.stsCode = stsCode;
    }

    public String getStsCode() {
        return stsCode;
    }
}
