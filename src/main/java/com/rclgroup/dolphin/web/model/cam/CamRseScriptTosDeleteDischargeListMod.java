/*-----------------------------------------------------------------------------------------------------------  
CamRseScriptTosDeleteDischargeListMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 14/03/11
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import java.sql.Timestamp;


public class CamRseScriptTosDeleteDischargeListMod extends CamRseScriptMod {
    
    private String vessel;
    private String voyage;
    private String pod;
    private String podTerminal;
    
    public CamRseScriptTosDeleteDischargeListMod() {
        super();
    }
    
    public CamRseScriptTosDeleteDischargeListMod(String vessel, String voyage, String pod, String podTerminal, String flagExecute, String returnResult, Timestamp recordChangeDate) {
        super(flagExecute, returnResult, recordChangeDate);
        this.vessel = vessel;
        this.voyage = voyage;
        this.pod = pod;
        this.podTerminal = podTerminal;
    }
    
    public CamRseScriptTosDeleteDischargeListMod(String vessel, String voyage, String pod, String podTerminal) {
        super();
        this.vessel = vessel;
        this.voyage = voyage;
        this.pod = pod;
        this.podTerminal = podTerminal;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

}

