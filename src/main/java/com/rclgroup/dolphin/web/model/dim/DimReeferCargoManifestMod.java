package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimReeferCargoManifestMod  extends RrcStandardMod{
    private String sessionId;
    private String username;
    private String recordAddUser;
    private String fromDate;
    private String toDate;
    private String service;
    private String vessle;
    private String voyage;
    private String line;
    private String region;
    private String agent;
    private String fsc;
    private String cocSoc;
    private String pol;
    private String pod;
    private String bl;
    private String polTer;
    private String podTer;
    private String direction;
    
    public DimReeferCargoManifestMod() {
        sessionId = "";
        username = "";
        fromDate = "";
        toDate = "";
        recordAddUser = "";
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessle(String vessle) {
        this.vessle = vessle;
    }

    public String getVessle() {
        return vessle;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setPolTer(String polTer) {
        this.polTer = polTer;
    }

    public String getPolTer() {
        return polTer;
    }

    public void setPodTer(String podTer) {
        this.podTer = podTer;
    }

    public String getPodTer() {
        return podTer;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
