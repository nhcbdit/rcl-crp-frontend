package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class FarReportTemplateMod extends RrcStandardMod {
    private String templateName;
    private String custInvCode;
    private String reportName;
    private String invNo;
    private String templateCode;
    
    public FarReportTemplateMod() {
        templateName = "";
        custInvCode = "";
        reportName = "";
        invNo = "";
        templateCode="";
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }
    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getTemplateCode() {
        return templateCode;
    }
    public void setCustInvCode(String custInvCode) {
        this.custInvCode = custInvCode;
    }

    public String getCustInvCode() {
        return custInvCode;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getInvNo() {
        return invNo;
    }
}
