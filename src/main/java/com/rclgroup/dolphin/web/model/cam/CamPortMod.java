/*------------------------------------------------------
CamPortMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 10/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
01 18/10/07 MNW              Added listForHelpScreen()
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamPortMod extends RrcStandardMod {
    private String countryCode;
    private String portCode;
    private String portName;
    private String portState;
    private String portStatus;
    private String portType;
    private String portZone;
    private String portArea;
    private String portRegion;
    private String terminalCode;
    private String terminalName;


    public CamPortMod() {
        countryCode = "";
        portCode = "";
        portName = "";
        portState = "";
        portStatus = "";
        portType = "";
        portZone = "";
        portArea = "";
        portRegion = "";
    } 
    
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getPortState() {
        return portState;
    }

    public void setPortState(String portState) {
        this.portState = portState;
    }

    public String getPortStatus() {
        return portStatus;
    }

    public void setPortStatus(String portStatus) {
        this.portStatus = portStatus;
    }

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    public String getPortZone() {
        return portZone;
    }

    public void setPortZone(String portZone) {
        this.portZone = portZone;
    }

    public void setPortArea(String portArea) {
        this.portArea = portArea;
    }

    public String getPortArea() {
        return portArea;
    }

    public void setPortRegion(String portRegion) {
        this.portRegion = portRegion;
    }

    public String getPortRegion() {
        return portRegion;
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    public String getTerminalCode() {
        return terminalCode;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public String getTerminalName() {
        return terminalName;
    }

}
