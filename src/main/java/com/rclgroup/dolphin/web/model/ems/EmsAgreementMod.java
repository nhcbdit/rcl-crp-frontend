/*-----------------------------------------------------------------------------------------------------------  
EmsAgreementMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Manop Wanngam 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.model.ems;

 import com.rclgroup.dolphin.web.common.RrcStandardMod;


 public class EmsAgreementMod extends RrcStandardMod {
     private String AgreementCode;
     private String SubAgreement;
    private String LessorCode;
     private String AgreementStatus;

     public EmsAgreementMod() {
         AgreementCode = "";
         SubAgreement = "";
         LessorCode = "";
         AgreementStatus = "";
     }
     
     public String getAgreementCode() {
         return AgreementCode;
     }

     public void setAgreementCode(String AgreementCode) {
         this.AgreementCode = AgreementCode;
     }

     public String getSubAgreement() {
         return SubAgreement;
     }

     public void setSubAgreement(String SubAgreement) {
         this.SubAgreement = SubAgreement;
     }
     
    public void setLessorCode(String lessorCode) {
        this.LessorCode = lessorCode;
    }

    public String getLessorCode() {
        return LessorCode;
    }

     public String getAgreementStatus() {
         return AgreementStatus;
     }

     public void setAgreementStatus(String AgreementStatus) {
         this.AgreementStatus = AgreementStatus;
     }
}
