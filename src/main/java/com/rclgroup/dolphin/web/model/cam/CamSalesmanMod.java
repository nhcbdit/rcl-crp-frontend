package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CamSalesmanMod extends RrcStandardMod {
    private String salesmanCode;
    private String countryCode;
    private String fscCode;
    private boolean isAManager;
    private String regionCode;
    
    public CamSalesmanMod() {
        salesmanCode = "";
        countryCode = "";
        fscCode = "";
        isAManager = true;
        regionCode = "";
    }

    public String getSalesmanCode() {
        return salesmanCode;
    }

    public void setSalesmanCode(String salesmanCode) {
        this.salesmanCode = salesmanCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFscCode() {
        return fscCode;
    }

    public void setFscCode(String fscCode) {
        this.fscCode = fscCode;
    }

    public boolean isAManager() {
        return isAManager;
    }

    public void setIsAManager(boolean isAManager) {
        this.isAManager = isAManager;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
}
