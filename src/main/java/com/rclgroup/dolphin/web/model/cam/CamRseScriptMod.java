/*-----------------------------------------------------------------------------------------------------------  
CamRseScriptMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 14/03/11
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseScriptMod extends RrcStandardMod {
    
    private String serviceFile;
    private String flagExecute;
    private String returnResult;
    private Timestamp recordChangeDate;
    
    public CamRseScriptMod() {
        super();
        
        this.serviceFile = "";
        this.flagExecute = RcmConstant.FLAG_NO;
        this.returnResult = "";
        this.recordChangeDate = null;
    }
    
    public CamRseScriptMod(String flagExecute, String returnResult, Timestamp recordChangeDate) {
        this.serviceFile = "";
        this.flagExecute = flagExecute;
        this.returnResult = returnResult;
        this.recordChangeDate = recordChangeDate;
    }

    public String getServiceFile() {
        return serviceFile;
    }

    public void setServiceFile(String serviceFile) {
        this.serviceFile = serviceFile;
    }

    public String getFlagExecute() {
        return flagExecute;
    }

    public void setFlagExecute(String flagExecute) {
        this.flagExecute = flagExecute;
    }

    public String getReturnResult() {
        return returnResult;
    }

    public void setReturnResult(String returnResult) {
        this.returnResult = returnResult;
    }
    
    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}

