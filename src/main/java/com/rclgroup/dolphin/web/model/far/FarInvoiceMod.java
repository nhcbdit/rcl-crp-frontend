package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.util.Vector;

public class FarInvoiceMod extends RrcStandardMod {
    private String fsc;
    private String invNo;
    private String billToParty;
    private String status;   
    private String billToPartyName;
    private String invDate;
    private String remarks;

    public FarInvoiceMod() {
        fsc = "";
        invNo = "";
        billToParty = "";
        status = "";       
        billToPartyName="";
        invDate="";
        remarks="";
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setInvNo(String invNo) {
        this.invNo = invNo;
    }

    public String getInvNo() {
        return invNo;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    
    public void setBillToPartyName(String billToPartyName) {
        this.billToPartyName = billToPartyName;
    }

    public String getBillToPartyName() {
        return billToPartyName;
    }
    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }

    public String getInvDate() {
        return invDate;
    }
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }
        
        
}
