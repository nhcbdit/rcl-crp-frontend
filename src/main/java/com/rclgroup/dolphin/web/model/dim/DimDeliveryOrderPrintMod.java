/*-----------------------------------------------------------------------------------------------------------  
DimDeliveryOrderPrintMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 25/09/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class DimDeliveryOrderPrintMod extends RrcStandardMod {
    
    private String service;
    private String vessel;
    private String voyage;
    private String direction;
    private String pol;
    private String pod;
    private String podTerminal;
    private String blId;
    private String blNo;
    private String hblNo;
    private String deliveryOrderNo;
    private String deliveryOrderDate;
    private String remarks;
    
    public DimDeliveryOrderPrintMod() {
        service = "";
        vessel = "";
        voyage = "";
        direction = "";
        pol = "";
        pod = "";
        podTerminal = "";
        blId = "";
        blNo = "";
        hblNo = "";
        deliveryOrderNo = "";
        deliveryOrderDate = "";
        remarks = "";
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPod() {
        return pod;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }
    
    public String getPodTerminal() {
        return podTerminal;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getBlId() {
        return blId;
    }

    public void setBlId(String blId) {
        this.blId = blId;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getHblNo() {
        return hblNo;
    }

    public void setHblNo(String hblNo) {
        this.hblNo = hblNo;
    }

    public String getDeliveryOrderNo() {
        return deliveryOrderNo;
    }

    public void setDeliveryOrderNo(String deliveryOrderNo) {
        this.deliveryOrderNo = deliveryOrderNo;
    }

    public String getDeliveryOrderDate() {
        return deliveryOrderDate;
    }

    public void setDeliveryOrderDate(String deliveryOrderDate) {
        this.deliveryOrderDate = deliveryOrderDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
