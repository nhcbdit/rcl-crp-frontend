package com.rclgroup.dolphin.web.model.bsa;

import java.util.ArrayList;
import java.util.List;

public class BsaBsaServiceVariantExportMod {

    private List<BsaBsaServiceVariantModelMod> modelList = new ArrayList<BsaBsaServiceVariantModelMod>(); 
    private List<BsaBsaServiceVariantServiceMod> serviceList = new ArrayList<BsaBsaServiceVariantServiceMod>(); 
    private List<BsaBsaServiceVariantPortCallMod> portCallList = new ArrayList<BsaBsaServiceVariantPortCallMod>(); 
    private List<BsaBsaServiceVariantPortPairMod> portPairList = new ArrayList<BsaBsaServiceVariantPortPairMod>(); 
    private String exportMode;

    public void setModelList(List<BsaBsaServiceVariantModelMod> modelList) {
        this.modelList = modelList;
    }

    public List<BsaBsaServiceVariantModelMod> getModelList() {
        return modelList;
    }

    public void setServiceList(List<BsaBsaServiceVariantServiceMod> serviceList) {
        this.serviceList = serviceList;
    }

    public List<BsaBsaServiceVariantServiceMod> getServiceList() {
        return serviceList;
    }

    public void setPortCallList(List<BsaBsaServiceVariantPortCallMod> portCallList) {
        this.portCallList = portCallList;
    }

    public List<BsaBsaServiceVariantPortCallMod> getPortCallList() {
        return portCallList;
    }

    public void setPortPairList(List<BsaBsaServiceVariantPortPairMod> portPairList) {
        this.portPairList = portPairList;
    }

    public List<BsaBsaServiceVariantPortPairMod> getPortPairList() {
        return portPairList;
    }

    public void setExportMode(String exportMode) {
        this.exportMode = exportMode;
    }

    public String getExportMode() {
        return exportMode;
    }
}
