/*-----------------------------------------------------------------------------------------------------------  
EmsTerminalMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 10/06/2008   
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class EmsTerminalMod extends RrcStandardMod {
    private String terminalCode;
    private String terminalName;
    private String pointCode;
    private String flag;
    private String zone;
    private String controlZone;
    private String area;
    private String region;
    private String terminalStatus;

    public EmsTerminalMod() {
        terminalCode = "";
        terminalName = "";
        pointCode = "";
        flag = "";
        zone = "";
        controlZone = "";
        area = "";
        region = "";
        terminalStatus = "";
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    public String getTerminalCode() {
        return terminalCode;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getZone() {
        return zone;
    }

    public void setControlZone(String controlZone) {
        this.controlZone = controlZone;
    }

    public String getControlZone() {
        return controlZone;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setTerminalStatus(String terminalStatus) {
        this.terminalStatus = terminalStatus;
    }

    public String getTerminalStatus() {
        return terminalStatus;
    }
}
