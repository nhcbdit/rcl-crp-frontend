/*--------------------------------------------------------
FarInvoicePhilipMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Kitti Pongsirisakun 26/10/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
 01  10/11/09       KIT      -355-         Few incorrect data on Philips Template
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class FarInvoicePhilipMod extends RrcStandardMod {
     private String blNo;                                                // BL NO
     private String por;                                                  // Place of Receipt
     private String pol;                                                  // POL
     private String pod;                                                // POD
     private String del;                                                 // Final Destination
     private String vesVoy;                                         // Vessel / Voyage
     private String etd;                                                 // Estimate Time Departure
     private String amount20;                                    // No. of 20ft
     private String amount40;                                    // No. of 40ft     
     private String amountHC;                                   // No. of 40 HC
     private String shiptmentTerm;                           // Shipment Term
     private String freightType;                                  // Freight Type
     private String charge;                                          // Charge
     private String Unit;                                               // Unit
     private String cur;                                                // Currency
     private String unitCost20ft;                                // 20ft Unit Cost
     private String unitCost40ft;                                // 40ft Unit Cost  
     private String unitCost40HC;                            // 40HC Unit Cost
     private String subTotal;                                      // Sub Toatal
     private String exchangeRate;                           // Exchange Rate
     private String subTotaldollar;                           // Sub Total 
     private String gstApplicable;                             // GST/VAT Applicable (Y/N)
     private String gstFivePercent;                           // GST/VAT 5%
     private String totAmountdollar;                        // Total Amount S$
     private String billNo;                                           // Bill Number
     private String billDate;                                        // Bill Date
     private String shipper;                                         // Shipper
     private String payeeName;                                // Payee Name
     private String paymentCur;                                // Payment Currency
     private String imex;                                              // Import or Export ##01
     private String invoiceNo;                                     // InvoiceNO
      private String shipperDesc;                               // shipperName

    public FarInvoicePhilipMod() {
         blNo = "";
          por = "";                                                
          pol = "";                                                        
          pod = "";                                                       
          del = "";                                                      
          vesVoy = "";                                        
          etd = "";                                                      
          amount20 = "";                                        
          amount40 = "";                                         
          amountHC = "";                                      
          shiptmentTerm = "";                               
          freightType = "";                                      
          charge = "";                                             
          Unit = "";                                                     
          cur = "";                                                   
          unitCost20ft = "";                                    
          unitCost40ft = "";                                  
          unitCost40HC = "";                                
          subTotal = "";                                            
          exchangeRate = "";                               
          subTotaldollar = "";                               
          gstApplicable = "";                                
          gstFivePercent = "";                          
          totAmountdollar = "";                                      
          billNo = "";                                               
          billDate = "";                                         
          shipper = "";                                            
          payeeName = "";                              
          paymentCur = "";                  
          imex = "";  //##01
           invoiceNo = "";
        shipperDesc = "";
    }


    public void setPor(String por) {
        this.por = por;
    }

    public String getPor() {
        return por;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setDel(String del) {
        this.del = del;
    }

    public String getDel() {
        return del;
    }

    public void setVesVoy(String vesVoy) {
        this.vesVoy = vesVoy;
    }

    public String getVesVoy() {
        return vesVoy;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEtd() {
        return etd;
    }

    public void setAmount20(String amount20) {
        this.amount20 = amount20;
    }

    public String getAmount20() {
        return amount20;
    }

    public void setAmount40(String amount40) {
        this.amount40 = amount40;
    }

    public String getAmount40() {
        return amount40;
    }

    public void setAmountHC(String amountHC) {
        this.amountHC = amountHC;
    }

    public String getAmountHC() {
        return amountHC;
    }

    public void setShiptmentTerm(String shiptmentTerm) {
        this.shiptmentTerm = shiptmentTerm;
    }

    public String getShiptmentTerm() {
        return shiptmentTerm;
    }

    public void setFreightType(String freightType) {
        this.freightType = freightType;
    }

    public String getFreightType() {
        return freightType;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getCharge() {
        return charge;
    }

    public void setUnit(String unit) {
        this.Unit = unit;
    }

    public String getUnit() {
        return Unit;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getCur() {
        return cur;
    }

    public void setUnitCost20ft(String unitCost20ft) {
        this.unitCost20ft = unitCost20ft;
    }

    public String getUnitCost20ft() {
        return unitCost20ft;
    }

    public void setUnitCost40ft(String unitCost40ft) {
        this.unitCost40ft = unitCost40ft;
    }

    public String getUnitCost40ft() {
        return unitCost40ft;
    }

    public void setUnitCost40HC(String unitCost40HC) {
        this.unitCost40HC = unitCost40HC;
    }

    public String getUnitCost40HC() {
        return unitCost40HC;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setSubTotaldollar(String subTotaldollar) {
        this.subTotaldollar = subTotaldollar;
    }

    public String getSubTotaldollar() {
        return subTotaldollar;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstFivePercent(String gstFivePercent) {
        this.gstFivePercent = gstFivePercent;
    }

    public String getGstFivePercent() {
        return gstFivePercent;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public String getShipper() {
        return shipper;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPaymentCur(String paymentCur) {
        this.paymentCur = paymentCur;
    }

    public String getPaymentCur() {
        return paymentCur;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setTotAmountdollar(String totAmountdollar) {
        this.totAmountdollar = totAmountdollar;
    }

    public String getTotAmountdollar() {
        return totAmountdollar;
    }
//## BEGIN 01
    public void setImex(String imex) {
        this.imex = imex;
    }

    public String getImex() {
        return imex;
    }
    //## END 01

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setShipperDesc(String shipperDesc) {
        this.shipperDesc = shipperDesc;
    }

    public String getShipperDesc() {
        return shipperDesc;
    }
}
