package com.rclgroup.dolphin.web.model.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class BkgRejectedAndCancelledBookingReportMod extends RrcStandardMod {
    public BkgRejectedAndCancelledBookingReportMod() {
    }
    
    private String userId;
    private String sessionId;
    private String reportBy;
    private String etdFrom;
    private String etdTo;
    private String service;
    private String vessel;
    private String voyage;
    private String direction;
    private String bookingStatus;
    private String cocSoc;
    private String pol;
    private String pod;
    private String polTerminal;
    private String podTerminal;
    private String bookingNo;
    
    private String line;
    private String region;
    private String agent;
    
    private String portCall;

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setReportBy(String reportBy) {
        this.reportBy = reportBy;
    }

    public String getReportBy() {
        return reportBy;
    }

    public void setEtdFrom(String etdFrom) {
        this.etdFrom = etdFrom;
    }

    public String getEtdFrom() {
        return etdFrom;
    }

    public void setEtdTo(String etdTo) {
        this.etdTo = etdTo;
    }

    public String getEtdTo() {
        return etdTo;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPolTerminal(String polTerminal) {
        this.polTerminal = polTerminal;
    }

    public String getPolTerminal() {
        return polTerminal;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getPodTerminal() {
        return podTerminal;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getAgent() {
        return agent;
    }

    public void setPortCall(String portCall) {
        this.portCall = portCall;
    }

    public String getPortCall() {
        return portCall;
    }
}
