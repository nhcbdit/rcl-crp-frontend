/*--
--------------------------------------------------------
DexBillOfLandingPrintMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
Author Sukit    
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
## 05/01/11         KIT        N/A         Created
 
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dex;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DexBillOfLandingPrintMod extends RrcStandardMod {
 
    public final String COC = "C";
    public final String SOC = "S";
    public final String COC_FULL = "COC";
    public final String SOC_FULL = "SOC";
    
    private String sessionId;
    private String blNo;
    private String shipperName;
    private String pla_of_issue;
    private String office;
    private String blcdtec;
    private String cocsoc;
    private String blstatus;
    private String printed;
    private String userName;
    private String blType;
 

    public DexBillOfLandingPrintMod() {
        sessionId = "";
        blNo = "";
        shipperName = "";
        pla_of_issue = "";
        office = "" ;
        blcdtec = "";
        cocsoc = "";
        blstatus = "";
        printed = "";
        userName="";
        blType ="";
    }


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setPla_of_issue(String pla_of_issue) {
        this.pla_of_issue = pla_of_issue;
    }

    public String getPla_of_issue() {
        return pla_of_issue;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getOffice() {
        return office;
    }

    public void setBlcdtec(String blcdtec) {
        this.blcdtec = blcdtec;
    }

    public String getBlcdtec() {
        return blcdtec;
    }

    public void setCocsoc(String cocsoc) {
        this.cocsoc = cocsoc;
        if (this.cocsoc.equals(COC)) {
            this.cocsoc = COC_FULL;
        }else{
            this.cocsoc = SOC_FULL;
        }
    }

    public String getCocsoc() {
        return cocsoc;
    }

    public void setBlstatus(String blstatus) {
        this.blstatus = blstatus;
    }

    public String getBlstatus() {
        return blstatus;
    }

    public void setPrinted(String printed) {
        this.printed = printed;
        if (this.printed .equalsIgnoreCase("A")){
            this.printed = "Printed";
        }else{
            this.printed  = "-";
        }
    }

    public String getPrinted() {
        return printed;
    }

    public void setBlType(String blType) {
        this.blType = blType;
    }

    public String getBlType() {
        return blType;
    }
}
