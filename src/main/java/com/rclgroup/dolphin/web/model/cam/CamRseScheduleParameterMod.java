/*-----------------------------------------------------------------------------------------------------------  
CamRseScheduleParameterMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 11/02/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseScheduleParameterMod extends RrcStandardMod {
    
    private String scheduleParamId;
    private String scheduleId;
    private String serviceId;
    private String parameterId;
    private String parameterName;
    private String parameterValue;
    private String specialDateFlag;
    private String specialDay;
    private String specialMonth;
    private String specialYear;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    
    public CamRseScheduleParameterMod() {
        super();
        
        scheduleParamId = "";
        scheduleId = "";
        serviceId = "";
        parameterId = "";
        parameterName = "";
        parameterValue = "";
        specialDateFlag = RcmConstant.FLAG_NO;
        specialDay = "";
        specialMonth = "";
        specialYear = "";
        recordAddDate = null;
        recordChangeDate = null;
    }
    
    public CamRseScheduleParameterMod(CamRseScheduleParameterMod copy) {
        super();
        if (copy != null) {
            this.scheduleParamId = copy.getScheduleParamId();
            this.scheduleId = copy.getScheduleId();
            this.serviceId = copy.getServiceId();
            this.parameterId = copy.getParameterId();
            this.parameterName = copy.getParameterName();
            this.parameterValue = copy.getParameterValue();
            this.specialDateFlag = copy.getSpecialDateFlag();
            this.specialDay = copy.getSpecialDay();
            this.specialMonth = copy.getSpecialMonth();
            this.specialYear = copy.getSpecialYear();
            this.recordAddDate = copy.getRecordAddDate(); 
            this.recordChangeDate = copy.getRecordChangeDate();
            this.recordStatus = copy.getRecordStatus();
            this.recordAddUser = copy.getRecordAddUser();
            this.recordChangeUser = copy.getRecordChangeUser();
        }
    }
    
    public CamRseScheduleParameterMod cloneObject(Object object) {
        CamRseScheduleParameterMod mod = null;
        if (object != null && object instanceof CamRseScheduleParameterMod) {
            CamRseScheduleParameterMod bean = (CamRseScheduleParameterMod) object;
            mod = new CamRseScheduleParameterMod(bean);
        }
        return mod;
    }
    
    public boolean equals(Object object) {
        if (object instanceof CamRseScheduleParameterMod) {
            CamRseScheduleParameterMod mod = (CamRseScheduleParameterMod) object;
            if (!this.scheduleParamId.equals(mod.getScheduleParamId())) {
                return false;
            } else if (!this.scheduleId.equals(mod.getScheduleId())) {
                return false;
            } else if (!this.serviceId.equals(mod.getServiceId())) {
                return false;
            } else if (!this.parameterId.equals(mod.getParameterId())) {
                return false;
            } else if (!this.parameterName.equals(mod.getParameterName())) {
                return false;
            } else if (!this.parameterValue.equals(mod.getParameterValue())) {
                return false;
            } else if (!this.specialDateFlag.equals(mod.getSpecialDateFlag())) {
                return false;
            } else if (!this.specialDay.equals(mod.getSpecialDay())) {
                return false;
            } else if (!this.specialMonth.equals(mod.getSpecialMonth())) {
                return false;
            } else if (!this.specialYear.equals(mod.getSpecialYear())) {
                return false;
            } else if (!this.recordStatus.equals(mod.getRecordStatus())) {
                return false;
            } else if (this.recordAddDate != null && !this.recordAddDate.equals(mod.getRecordAddDate())) {
                return false;
            } else if (this.recordChangeDate != null && !this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public String getScheduleParamId() {
        return scheduleParamId;
    }

    public void setScheduleParamId(String scheduleParamId) {
        this.scheduleParamId = scheduleParamId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getParameterId() {
        return parameterId;
    }

    public void setParameterId(String parameterId) {
        this.parameterId = parameterId;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public String getSpecialDateFlag() {
        return specialDateFlag;
    }

    public void setSpecialDateFlag(String specialDateFlag) {
        this.specialDateFlag = specialDateFlag;
    }

    public String getSpecialDay() {
        return specialDay;
    }

    public void setSpecialDay(String specialDay) {
        this.specialDay = specialDay;
    }

    public String getSpecialMonth() {
        return specialMonth;
    }

    public void setSpecialMonth(String specialMonth) {
        this.specialMonth = specialMonth;
    }

    public String getSpecialYear() {
        return specialYear;
    }

    public void setSpecialYear(String specialYear) {
        this.specialYear = specialYear;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}


