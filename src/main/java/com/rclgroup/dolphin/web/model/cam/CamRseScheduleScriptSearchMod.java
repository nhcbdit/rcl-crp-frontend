/*
 * -----------------------------------------------------------------------------
 * CamRseScheduleScriptSearchMod.java
 * -----------------------------------------------------------------------------
 * Copyright RCL Public Co., Ltd. 2007 
 * -----------------------------------------------------------------------------
 * Author Dhruv Parekh 31/08/2012
 * ------- Change Log ----------------------------------------------------------
 * ##   DD/MM/YY    -User-      -TaskRef-      -Short Description  
 * -----------------------------------------------------------------------------
 */

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class CamRseScheduleScriptSearchMod extends RrcStandardMod {
    
    private String scheduleId;
    private String scheduleCode;
    private String scheduleDesc;
    private String serviceId;
    private String serviceCode;
    private String serviceName;
    private String serviceType;
    private String moduleCode;
    private String subtype;
    private String schedStatus;
    private String schedStartDate;
    private String schedNextRun;
    private String scheduleType;
    private String directoryProcess;
    private String repeatInterval;
    private String enabled;
    private String runCount;
    private String failCount;
    private String lastRunDate;
    private String lastRunDuration;
    
    public CamRseScheduleScriptSearchMod() {
        super();
    }
    
    public void clone(CamRseScheduleScriptSearchMod copy) {
        if (copy != null) {
            this.scheduleId = copy.getScheduleId();
            this.scheduleCode = copy.getScheduleCode();
            this.scheduleDesc = copy.getScheduleDesc();
            this.serviceId = copy.getServiceId();
            this.serviceCode = copy.getServiceCode();
            this.serviceName = copy.getServiceName();
            this.serviceType = copy.getServiceType();
            this.moduleCode = copy.getModuleCode();
            this.subtype = copy.getSubtype();
            this.schedStatus = copy.getSchedStatus();
            this.schedStartDate = copy.getSchedStartDate();
            this.schedNextRun = copy.getSchedNextRun();
            this.scheduleType = copy.getScheduleType();
            this.directoryProcess = copy.getDirectoryProcess();
            this.repeatInterval = copy.getRepeatInterval();
            this.enabled = copy.getEnabled();
            this.runCount = copy.getRunCount();
            this.failCount = copy.getFailCount();
            this.lastRunDate = copy.getLastRunDate();
            this.lastRunDuration = copy.getLastRunDuration();
            this.recordStatus = copy.getRecordStatus();
            this.recordAddUser = copy.getRecordAddUser();
            this.recordChangeUser = copy.getRecordChangeUser();
        }
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleCode() {
        return scheduleCode;
    }

    public void setScheduleCode(String scheduleCode) {
        this.scheduleCode = scheduleCode;
    }

    public String getScheduleDesc() {
        return scheduleDesc;
    }

    public void setScheduleDesc(String scheduleDesc) {
        this.scheduleDesc = scheduleDesc;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getSchedStatus() {
        return schedStatus;
    }

    public void setSchedStatus(String schedStatus) {
        this.schedStatus = schedStatus;
    }

    public String getSchedStartDate() {
        return schedStartDate;
    }

    public void setSchedStartDate(String schedStartDate) {
        this.schedStartDate = schedStartDate;
    }

    public String getSchedNextRun() {
        return schedNextRun;
    }

    public void setSchedNextRun(String schedNextRun) {
        this.schedNextRun = schedNextRun;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getDirectoryProcess() {
        return directoryProcess;
    }

    public void setDirectoryProcess(String directoryProcess) {
        this.directoryProcess = directoryProcess;
    }

    public String getRepeatInterval() {
        return repeatInterval;
    }

    public void setRepeatInterval(String repeatInterval) {
        this.repeatInterval = repeatInterval;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getRunCount() {
        return runCount;
    }

    public void setRunCount(String runCount) {
        this.runCount = runCount;
    }

    public String getFailCount() {
        return failCount;
    }

    public void setFailCount(String failCount) {
        this.failCount = failCount;
    }

    public String getLastRunDate() {
        return lastRunDate;
    }

    public void setLastRunDate(String lastRunDate) {
        this.lastRunDate = lastRunDate;
    }

    public String getLastRunDuration() {
        return lastRunDuration;
    }

    public void setLastRunDuration(String lastRunDuration) {
        this.lastRunDuration = lastRunDuration;
    }
}


