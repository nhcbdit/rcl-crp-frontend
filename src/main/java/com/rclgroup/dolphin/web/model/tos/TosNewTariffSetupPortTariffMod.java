/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupPortTariffMod.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Panadda P. 28/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosNewTariffSetupPortTariffMod extends RrcStandardMod{

    private String operationCd;
    private String operationDesc;
    private String effDate;
    private String expDate;
    private String socProforma;
    private boolean isDelete;
    private String rateRef;
    private String tosRateSeq;
    private String seqNo;    
    private int rowStatus;
    
    public int INSERT;
    public int UPDATE;
    
    public TosNewTariffSetupPortTariffMod() {
        super();
        operationCd = "";
        operationDesc = "";
        effDate = "";
        expDate = "";
        socProforma = "";
        isDelete = false;
        rateRef = "";
        tosRateSeq = "";
        seqNo = "";
        rowStatus = 1;
        INSERT = 1;
        UPDATE = 0;
    }

    public void setOperationCd(String operationCd) {
        this.operationCd = operationCd;
    }

    public String getOperationCd() {
        return operationCd;
    }

    public void setOperationDesc(String operationDesc) {
        this.operationDesc = operationDesc;
    }

    public String getOperationDesc() {
        return operationDesc;
    }



    public void setSocProforma(String socProforma) {
        this.socProforma = socProforma;
    }

    public String getSocProforma() {
        return socProforma;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }


    public void setEffDate(String effDate) {
        this.effDate = effDate;
    }

    public String getEffDate() {
        return effDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setRateRef(String rateRef) {
        this.rateRef = rateRef;
    }

    public String getRateRef() {
        return rateRef;
    }

    public void setTosRateSeq(String tosRateSeq) {
        this.tosRateSeq = tosRateSeq;
    }

    public String getTosRateSeq() {
        return tosRateSeq;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setRowStatus(int rowStatus) {
        this.rowStatus = rowStatus;
    }

    public int getRowStatus() {
        return rowStatus;
    }
}
