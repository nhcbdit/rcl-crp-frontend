/*-----------------------------------------------------------------------------------------------------------  
CamRseScheduleAlarmMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 11/02/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;


public class CamRseScheduleAlarmMod extends RrcStandardMod {
    
    private String scheduleAlarmId;
    private String scheduleId;
    private String eventCode;
    private String eventDescription;
    private String emailAddress;
    private Timestamp recordAddDate; 
    private Timestamp recordChangeDate;
    
    public CamRseScheduleAlarmMod() {
        super();

        scheduleAlarmId = "";
        scheduleId = "";
        eventCode = "";
        eventDescription = "";
        emailAddress = "";
        recordAddDate = null;
        recordChangeDate = null;
    }

    public CamRseScheduleAlarmMod(CamRseScheduleAlarmMod copy) {
        super();
        if (copy != null) {
            this.scheduleAlarmId = copy.getScheduleAlarmId();
            this.scheduleId = copy.getScheduleId();
            this.eventCode = copy.getEventCode();
            this.eventDescription = copy.getEventDescription();
            this.emailAddress = copy.getEmailAddress();
            this.recordAddDate = copy.getRecordAddDate();
            this.recordChangeDate = copy.getRecordChangeDate();
            this.recordStatus = copy.getRecordStatus();
            this.recordAddUser = copy.getRecordAddUser();
            this.recordChangeUser = copy.getRecordChangeUser();
        }
    }

    public CamRseScheduleAlarmMod cloneObject(Object object) {
        CamRseScheduleAlarmMod mod = null;
        if (object != null && object instanceof CamRseScheduleAlarmMod) {
            CamRseScheduleAlarmMod bean = (CamRseScheduleAlarmMod) object;
            mod = new CamRseScheduleAlarmMod(bean);
        }
        return mod;
    }

    public boolean equals(Object object) {
        if (object instanceof CamRseScheduleAlarmMod) {
            CamRseScheduleAlarmMod mod = (CamRseScheduleAlarmMod) object;
            if (!this.scheduleAlarmId.equals(mod.getScheduleAlarmId())) {
                return false;
            } else if (!this.scheduleId.equals(mod.getScheduleId())) {
                return false;
            } else if (!this.eventCode.equals(mod.getEventCode())) {
                return false;
            } else if (!this.emailAddress.equals(mod.getEmailAddress())) {
                return false;
            } else if (!this.recordStatus.equals(mod.getRecordStatus())) {
                return false;
            } else if (this.recordAddDate != null && !this.recordAddDate.equals(mod.getRecordAddDate())) {
                return false;
            } else if (this.recordChangeDate != null && !this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public String getScheduleAlarmId() {
        return scheduleAlarmId;
    }

    public void setScheduleAlarmId(String scheduleAlarmId) {
        this.scheduleAlarmId = scheduleAlarmId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }
}


