/*-----------------------------------------------------------------------------------------------------------  
EmsAreaMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Manop Wanngam 05/11/07 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 29/04/08  SPD                      Change to new framework
-----------------------------------------------------------------------------------------------------------*/


 package com.rclgroup.dolphin.web.model.ems;

 import com.rclgroup.dolphin.web.common.RrcStandardMod;

 public class EmsControlZoneMod extends RrcStandardMod {
     private String controlZoneCode;
     private String controlZoneName;
    private String regionCode;
     private String areaCode;
     private String controlZoneStatus;

     public EmsControlZoneMod() {
         controlZoneCode = "";
         controlZoneName = "";
         regionCode = "";
         areaCode = "";
         controlZoneStatus = "";
     }
     
     public String getControlZoneCode() {
         return controlZoneCode;
     }

     public void setControlZoneCode(String controlZoneCode) {
         this.controlZoneCode = controlZoneCode;
     }

     public String getControlZoneName() {
         return controlZoneName;
     }

     public void setControlZoneName(String controlZoneName) {
         this.controlZoneName = controlZoneName;
     }

     public String getControlZoneStatus() {
         return controlZoneStatus;
     }

     public void setControlZoneStatus(String controlZoneStatus) {
         this.controlZoneStatus = controlZoneStatus;
     }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }
}
