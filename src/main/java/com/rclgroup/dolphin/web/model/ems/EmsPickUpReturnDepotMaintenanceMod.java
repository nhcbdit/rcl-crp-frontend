/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotMaintenanceMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 15/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import java.sql.Timestamp;
import java.util.List;

public class EmsPickUpReturnDepotMaintenanceMod extends RrcStandardMod {
    private String multiDepotHdrId;
    private String mporr;
    private String depotCode;
    private String pointCode;
    private String fsc;    
    private String bl;
    private String bkg;
    private String consignee;
    private String supplier;
    private String shipper;
    private String contractParty;
    private String service;
    private String vessel;
    private String voyage;
    private String status;    
    private Timestamp recordAddDate;    
    private Timestamp recordChangeDate;
    private String porrFlag;
    private String activityFlagOri; //I = insert, R = Read from DB, D = delete
    private String activityFlag; //I = insert, R = Read from DB, D = delete
    private String deleteFlag;
    
    public static String ACTIVITY_INS = "I";
    public static String ACTIVITY_READ = "R";
    public static String ACTIVITY_DEL = "D";
    
    private List emsPickUpReturnDepotMaintenanceDetailList;
    
    public EmsPickUpReturnDepotMaintenanceMod(String multiDepotHdrId,   String mporr,
                    String fsc,         String bl,
                    String bkg,         String consignee,
                    String supplier,
                    String shipper,     String contractParty,
                    String service,     String vessel,
                    String voyage,      String status,
                    String depotCode,   String pointCode,
                    Timestamp recordAddDate, Timestamp recordChangeDate,
                    String activityFlag,String activityFlagOri,
                    String deleteFlag,  String porrFlag) {
        super();
        this.multiDepotHdrId = multiDepotHdrId;
        this.mporr = mporr;
        this.fsc = fsc;
        this.bl = bl;
        this.bkg = bkg;
        this.consignee = consignee;
        this.supplier = supplier;
        this.shipper = shipper;
        this.contractParty = contractParty;
        this.service = service;
        this.vessel = vessel;
        this.voyage = voyage;
        this.status = status;
        this.depotCode = depotCode;
        this.pointCode = pointCode;   
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
        this.activityFlag = activityFlag;
        this.activityFlagOri = activityFlagOri;
        this.deleteFlag = deleteFlag;
        this.porrFlag = porrFlag;
    }
    
    public EmsPickUpReturnDepotMaintenanceMod(EmsPickUpReturnDepotMaintenanceMod mod){
         this(mod.multiDepotHdrId,mod.mporr, mod.fsc,mod.bl,mod.bkg, mod.consignee,mod.supplier, mod.shipper, mod.contractParty, mod.service, mod.vessel, mod.voyage, mod.status, mod.depotCode, mod.pointCode, mod.recordAddDate,mod.recordChangeDate,mod.activityFlag,mod.activityFlagOri,mod.deleteFlag,mod.porrFlag);
    }
    
    public EmsPickUpReturnDepotMaintenanceMod() {
        this("","","","","","","","","","","","","","","",new Timestamp(0),new Timestamp(0),ACTIVITY_INS,ACTIVITY_INS,"","P");
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setBl(String bl) {
        this.bl = bl;
    }

    public String getBl() {
        return bl;
    }

    public void setBkg(String bkg) {
        this.bkg = bkg;
    }

    public String getBkg() {
        return bkg;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public String getShipper() {
        return shipper;
    }

    public void setContractParty(String contractParty) {
        this.contractParty = contractParty;
    }

    public String getContractParty() {
        return contractParty;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setEmsPickUpReturnDepotMaintenanceDetailList(List emsPickUpReturnDepotMaintenanceDetailList) {
        this.emsPickUpReturnDepotMaintenanceDetailList = emsPickUpReturnDepotMaintenanceDetailList;
    }

    public List getEmsPickUpReturnDepotMaintenanceDetailList() {
        return emsPickUpReturnDepotMaintenanceDetailList;
    }

    public void setDepotCode(String depotCode) {
        this.depotCode = depotCode;
    }

    public String getDepotCode() {
        return depotCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setMultiDepotHdrId(String multiDepotHdrId) {
        this.multiDepotHdrId = multiDepotHdrId;
    }

    public String getMultiDepotHdrId() {
        return multiDepotHdrId;
    }

    public void setMporr(String mporr) {
        this.mporr = mporr;
    }

    public String getMporr() {
        return mporr;
    }

    public void setPorrFlag(String porrFlag) {
        this.porrFlag = porrFlag;
    }

    public String getPorrFlag() {
        return porrFlag;
    }

    public void setActivityFlagOri(String activityFlagOri) {
        this.activityFlagOri = activityFlagOri;
    }

    public String getActivityFlagOri() {
        return activityFlagOri;
    }

    public void setActivityFlag(String activityFlag) {
        this.activityFlag = activityFlag;
    }

    public String getActivityFlag() {
        return activityFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getSupplier() {
        return supplier;
    }
}
