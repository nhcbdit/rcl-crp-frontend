/*-----------------------------------------------------------------------------------------------------------  
CtfGrcTrackingHeaderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 05/11/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.ctf;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.util.ArrayList;
import java.util.List;

public class CtfGrcTrackingHeaderMod extends RrcStandardMod{

    public static String CS_DEFAULT_FLAG = "";
    public static String CS_COC_FLAG = "C";
    public static String CS_SOC_FLAG = "S";
    
    public static String RATE_CHG_FROM_DEFAULT = "";
    public static String RATE_CHG_FROM_GRC = "GRC";  // GLOBAL RATE CHANGE
    public static String RATE_CHG_FROM_QUO = "QUO"; //SUPERSEDING QUOTATION
    public static String RATE_CHG_FROM_NPR = "NSR"; //NEW PORT SURCHARGE RATES
    
    public static String RATE_CHG_TYPE_DEFAULT = "";
    public static String RATE_CHG_TYPE_MDF_F = "F"; // MODIFY GUIDELINE-FINAL
    public static String RATE_CHG_TYPE_MDF_FR = "FR"; // MODIFY GUIDELINE-FINAL/RR
    public static String RATE_CHG_TYPE_MDF_SURCHARGE = "MS"; // MODIFY SURCHARGE
    public static String RATE_CHG_TYPE_EXP_SURCHARGE = "EXPS"; // EXPIRE SURCHARGE
    public static String RATE_CHG_TYPE_EXT_SURCHARGE = "EXTS"; //EXTEND SURCHARGE
    public static String RATE_CHG_TYPE_NP_SURCHARGE = "PSR"; //NEW PORT SURCHARGE RATES
    public static String RATE_CHG_TYPE_QUO = "QUO"; // SUPERSEDING QUOTATION
    
    public static String STATUS_DEFAULT = "";
    public static String STATUS_NEW  = "N";
    public static String STATUS_PENDING = "P";
    public static String STATUS_CLOSED = "C";
    
    public static String ASC = "A";
    public static String DESC = "D";
    
    private String cocSoc;
    private String cocSocName;
    private String rateChgFrom;
    private String rateChgFromName;
    private String rateChgType;
    private String rateChgTypeName;
    private String grcRefNo;
    private String grcChildRefNo;
    private int grcDate;
    private String status;
    private String statusName;
    
    public CtfGrcTrackingHeaderMod() {
        this.cocSoc = CS_DEFAULT_FLAG ;
        this.rateChgFrom = RATE_CHG_FROM_DEFAULT;
        this.rateChgType = RATE_CHG_TYPE_DEFAULT;
        this.grcRefNo = "";
        this.grcChildRefNo = "";
        this.grcDate = 0;
        this.status = STATUS_DEFAULT;
        this.cocSocName  = "";
        this.rateChgFromName = "";
        this.rateChgTypeName = "";
        this.statusName = "";
    }


    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setRateChgFrom(String rateChgFrom) {
        this.rateChgFrom = rateChgFrom;
    }

    public String getRateChgFrom() {
        return rateChgFrom;
    }

    public void setRateChgType(String rateChgType) {
        this.rateChgType = rateChgType;
    }

    public String getRateChgType() {
        return rateChgType;
    }

    public void setGrcRefNo(String grcRefNo) {
        this.grcRefNo = grcRefNo;
    }

    public String getGrcRefNo() {
        return grcRefNo;
    }

    public void setGrcChildRefNo(String grcChildRefNo) {
        this.grcChildRefNo = grcChildRefNo;
    }

    public String getGrcChildRefNo() {
        return grcChildRefNo;
    }

    public void setGrcDate(int grcDate) {
        this.grcDate = grcDate;
    }

    public int getGrcDate() {
        return grcDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setCocSocName(String cocSocName) {
        this.cocSocName = cocSocName;
    }

    public String getCocSocName() {
        return cocSocName;
    }

    public void setRateChgFromName(String rateChgFromName) {
        this.rateChgFromName = rateChgFromName;
    }

    public String getRateChgFromName() {
        return rateChgFromName;
    }

    public void setRateChgTypeName(String rateChgTypeName) {
        this.rateChgTypeName = rateChgTypeName;
    }

    public String getRateChgTypeName() {
        return rateChgTypeName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusName() {
        return statusName;
    }
}
