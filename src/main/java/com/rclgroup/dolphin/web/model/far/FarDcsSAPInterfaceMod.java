package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class FarDcsSAPInterfaceMod  extends RrcStandardMod{

private String number;
private String fsc;
private String charge;
private String taxCode;
private String taxRate;
private String sapTaxCode;
    public FarDcsSAPInterfaceMod() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getFsc() {
        return fsc;
    }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public String getSapTaxCode() {
        return sapTaxCode;
    }

    public void setSapTaxCode(String sapTaxCode) {
        this.sapTaxCode = sapTaxCode;
    }
}
