/*--------------------------------------------------------
DimInwardDischargeDetailMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Kitti Pongsirisakun 23/11/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class DimInwardDischargeDetailMod extends RrcStandardMod{
    private String blNo;
    private String paymentPort;
    private String size;
    private String type;
    private String amt;
    private String containerSts;
    public DimInwardDischargeDetailMod() {
    
    blNo = "";
    paymentPort = "";
    size = "";
    type = "";
    containerSts = "";
    amt = "";
    
    }

    public void setBlNo(String blNo) {
        this.blNo = blNo;
    }

    public String getBlNo() {
        return blNo;
    }

    public void setPaymentPort(String paymentPort) {
        this.paymentPort = paymentPort;
    }

    public String getPaymentPort() {
        return paymentPort;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setContainerSts(String containerSts) {
        this.containerSts = containerSts;
    }

    public String getContainerSts() {
        return containerSts;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getAmt() {
        return amt;
    }
}
