package com.rclgroup.dolphin.web.model.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class TosApprovalInternalPSSearchMod extends RrcStandardMod {
    private String port;
    private String terminal;    
    private String service;
    private String vessel;
    private String voyage;
    private String pcsq;
    private String etaDate;
    private String moduleType;
    private String items;
    
    public TosApprovalInternalPSSearchMod() {
        super();
        port = "";
        terminal = ""; 
        service = "";
        vessel = "";
        voyage = "";
        pcsq = "";
        etaDate = "";
        moduleType = "";
        items = "";
    }
    
    public TosApprovalInternalPSSearchMod(String port,
                                    String terminal,    
                                    String service,
                                    String vessel,
                                    String voyage,
                                    String pcsq,
                                    String etaDate,
                                    String moduleType,
                                    String items) {
        
        this.port = port;
        this.terminal = terminal;
        this.service = service;
        this.vessel = vessel;
        this.voyage = voyage;
        this.pcsq = pcsq;
        this.etaDate = etaDate;
        this.moduleType = moduleType;
        this.items = items;
    }


    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setPcsq(String pcsq) {
        this.pcsq = pcsq;
    }

    public String getPcsq() {
        return pcsq;
    }

    public void setEtaDate(String etaDate) {
        this.etaDate = etaDate;
    }

    public String getEtaDate() {
        return etaDate;
    }

    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    public String getModuleType() {
        return moduleType;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getItems() {
        return items;
    }
}
