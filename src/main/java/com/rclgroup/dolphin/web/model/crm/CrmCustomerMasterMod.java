/*--------------------------------------------------------
CrmCustomerMasterMod.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Wuttitorn Wuttijiaranai 04/08/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.model.crm;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

public class CrmCustomerMasterMod extends RrcStandardMod {

    private String line;
    private String trade;
    private String agent;
    private String customerCode;
    private String customerName;
    private String countryCode;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    private String city;
    private String stateCode;
    private String zipCode;
    private String webAddress;
    private String phoneNo;
    private String fax;
    private String email;
    private String portPointCode;
    private String equipmentPosition;
    private String fscCode;
    private String commPreference;
    private String salesMan;
    private String bkgFscCode;
    private String consolidatedTo;
    private String customerType;
    private String billToPartyCustomer;
    private String statementFlag;
    private String contractReqFlag;
    private String cocSoc;
    private String registerNoForVatInvoice;
    private String returnDepot;
    private String hearAboutUs;
    private String onlineUserFlag;
    private String onlineUserId;
    private String onlinePassword;
    private String vatExcempted;
    private String invoicePrintedBy;
    private String newCustomerFlag;
    private String operatorCode;
    private String agentCode;
    private int exportLayoutDays;
    private int importlayoutDays;
    private int transhipmentlayoutDays;
    private String source;
    private String custcd;
    private String cucrst;
    private String vendorCode;
    private String customerCategory;
    private String ttcLubFlag;
    private String commPrefSend;
    private String customerFilteredName;
    private String industryCode;
    private String industryRemarks;
    private String sapCustomerCode;
    private String sapCustomerFlag;
    private String haulageLoc;
    private String preferredCarrier;
    private String templateBooking;
    private String transportType;
    private String proofOfDelivery;
    private String mobileNo;
    private String parentCompany;
    private String preAudit;
    private String federalTaxId;
    private String taxIdNumber;
    private String sicCode;
    private String lastName;
    private String firstName;
    private String secondSalesman;
    private String collectFsc;
    private String middleInitial;
    private String amsFiling;
    private String cargoRolloverAuth;
    private String bkgPerCargoUnit;
    private String customerAlias;
    private String directInvoice;
    private String intraMappingCode;
    private String masterBooking;
    private String applySpecialExchangeRateFlag;
    private String customerBillingCurr;
    private String updateTime;
    private int dateOfSuspension;
    private String suspendedBy;
    private String taxIdType;
    private String businessPartnerType;
    private String resident;
    private String invoiceTemplateCode;
    private String residentId;
    private String overrideCurrency;

    public CrmCustomerMasterMod() {
        super();
        line = "";
        trade = "";
        agent = "";
        customerCode = "";
        customerName = "";
        countryCode = "";
        addressLine1 = "";
        addressLine2 = "";
        addressLine3 = "";
        addressLine4 = "";
        city = "";
        stateCode = "";
        zipCode = "";
        webAddress = "";
        phoneNo = "";
        fax = "";
        email = "";
        portPointCode = "";
        equipmentPosition = "";
        fscCode = "";
        commPreference = "";
        salesMan = "";
        bkgFscCode = "";
        consolidatedTo = "";
        customerType = "";
        billToPartyCustomer = "";
        statementFlag = "";
        contractReqFlag = "";
        cocSoc = "";
        registerNoForVatInvoice = "";
        returnDepot = "";
        hearAboutUs = "";
        onlineUserFlag = "";
        onlineUserId = "";
        onlinePassword = "";
        vatExcempted = "";
        invoicePrintedBy = "";
        newCustomerFlag = "";
        operatorCode = "";
        agentCode = "";
        exportLayoutDays = 0;
        importlayoutDays = 0;
        transhipmentlayoutDays = 0;
        source = "";
        custcd = "";
        cucrst = "";
        vendorCode = "";
        customerCategory = "";
        ttcLubFlag = "";
        commPrefSend = "";
        customerFilteredName = "";
        industryCode = "";
        industryRemarks = "";
        sapCustomerCode = "";
        sapCustomerFlag = "";
        haulageLoc = "";
        preferredCarrier = "";
        templateBooking = "";
        transportType = "";
        proofOfDelivery = "";
        mobileNo = "";
        parentCompany = "";
        preAudit = "";
        federalTaxId = "";
        taxIdNumber = "";
        sicCode = "";
        lastName = "";
        firstName = "";
        secondSalesman = "";
        collectFsc = "";
        middleInitial = "";
        amsFiling = "";
        cargoRolloverAuth = "";
        bkgPerCargoUnit = "";
        customerAlias = "";
        directInvoice = "";
        intraMappingCode = "";
        masterBooking = "";
        applySpecialExchangeRateFlag = "";
        customerBillingCurr = "";
        updateTime = "";
        dateOfSuspension = 0;
        suspendedBy = "";
        taxIdType = "";
        businessPartnerType = "";
        resident = "";
        invoiceTemplateCode = "";
        residentId = "";
        overrideCurrency = "";
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPortPointCode() {
        return portPointCode;
    }

    public void setPortPointCode(String portPointCode) {
        this.portPointCode = portPointCode;
    }

    public String getEquipmentPosition() {
        return equipmentPosition;
    }

    public void setEquipmentPosition(String equipmentPosition) {
        this.equipmentPosition = equipmentPosition;
    }

    public String getFscCode() {
        return fscCode;
    }

    public void setFscCode(String fscCode) {
        this.fscCode = fscCode;
    }

    public String getCommPreference() {
        return commPreference;
    }

    public void setCommPreference(String commPreference) {
        this.commPreference = commPreference;
    }

    public String getSalesMan() {
        return salesMan;
    }

    public void setSalesMan(String salesMan) {
        this.salesMan = salesMan;
    }

    public String getBkgFscCode() {
        return bkgFscCode;
    }

    public void setBkgFscCode(String bkgFscCode) {
        this.bkgFscCode = bkgFscCode;
    }

    public String getConsolidatedTo() {
        return consolidatedTo;
    }

    public void setConsolidatedTo(String consolidatedTo) {
        this.consolidatedTo = consolidatedTo;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getBillToPartyCustomer() {
        return billToPartyCustomer;
    }

    public void setBillToPartyCustomer(String billToPartyCustomer) {
        this.billToPartyCustomer = billToPartyCustomer;
    }

    public String getStatementFlag() {
        return statementFlag;
    }

    public void setStatementFlag(String statementFlag) {
        this.statementFlag = statementFlag;
    }

    public String getContractReqFlag() {
        return contractReqFlag;
    }

    public void setContractReqFlag(String contractReqFlag) {
        this.contractReqFlag = contractReqFlag;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getRegisterNoForVatInvoice() {
        return registerNoForVatInvoice;
    }

    public void setRegisterNoForVatInvoice(String registerNoForVatInvoice) {
        this.registerNoForVatInvoice = registerNoForVatInvoice;
    }

    public String getReturnDepot() {
        return returnDepot;
    }

    public void setReturnDepot(String returnDepot) {
        this.returnDepot = returnDepot;
    }

    public String getHearAboutUs() {
        return hearAboutUs;
    }

    public void setHearAboutUs(String hearAboutUs) {
        this.hearAboutUs = hearAboutUs;
    }

    public String getOnlineUserFlag() {
        return onlineUserFlag;
    }

    public void setOnlineUserFlag(String onlineUserFlag) {
        this.onlineUserFlag = onlineUserFlag;
    }

    public String getOnlineUserId() {
        return onlineUserId;
    }

    public void setOnlineUserId(String onlineUserId) {
        this.onlineUserId = onlineUserId;
    }

    public String getOnlinePassword() {
        return onlinePassword;
    }

    public void setOnlinePassword(String onlinePassword) {
        this.onlinePassword = onlinePassword;
    }

    public String getVatExcempted() {
        return vatExcempted;
    }

    public void setVatExcempted(String vatExcempted) {
        this.vatExcempted = vatExcempted;
    }

    public String getInvoicePrintedBy() {
        return invoicePrintedBy;
    }

    public void setInvoicePrintedBy(String invoicePrintedBy) {
        this.invoicePrintedBy = invoicePrintedBy;
    }

    public String getNewCustomerFlag() {
        return newCustomerFlag;
    }

    public void setNewCustomerFlag(String newCustomerFlag) {
        this.newCustomerFlag = newCustomerFlag;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public int getExportLayoutDays() {
        return exportLayoutDays;
    }

    public void setExportLayoutDays(int exportLayoutDays) {
        this.exportLayoutDays = exportLayoutDays;
    }

    public int getImportlayoutDays() {
        return importlayoutDays;
    }

    public void setImportlayoutDays(int importlayoutDays) {
        this.importlayoutDays = importlayoutDays;
    }

    public int getTranshipmentlayoutDays() {
        return transhipmentlayoutDays;
    }

    public void setTranshipmentlayoutDays(int transhipmentlayoutDays) {
        this.transhipmentlayoutDays = transhipmentlayoutDays;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCustcd() {
        return custcd;
    }

    public void setCustcd(String custcd) {
        this.custcd = custcd;
    }

    public String getCucrst() {
        return cucrst;
    }

    public void setCucrst(String cucrst) {
        this.cucrst = cucrst;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public String getTtcLubFlag() {
        return ttcLubFlag;
    }

    public void setTtcLubFlag(String ttcLubFlag) {
        this.ttcLubFlag = ttcLubFlag;
    }

    public String getCommPrefSend() {
        return commPrefSend;
    }

    public void setCommPrefSend(String commPrefSend) {
        this.commPrefSend = commPrefSend;
    }

    public String getCustomerFilteredName() {
        return customerFilteredName;
    }

    public void setCustomerFilteredName(String customerFilteredName) {
        this.customerFilteredName = customerFilteredName;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getIndustryRemarks() {
        return industryRemarks;
    }

    public void setIndustryRemarks(String industryRemarks) {
        this.industryRemarks = industryRemarks;
    }

    public String getSapCustomerCode() {
        return sapCustomerCode;
    }

    public void setSapCustomerCode(String sapCustomerCode) {
        this.sapCustomerCode = sapCustomerCode;
    }

    public String getSapCustomerFlag() {
        return sapCustomerFlag;
    }

    public void setSapCustomerFlag(String sapCustomerFlag) {
        this.sapCustomerFlag = sapCustomerFlag;
    }

    public String getHaulageLoc() {
        return haulageLoc;
    }

    public void setHaulageLoc(String haulageLoc) {
        this.haulageLoc = haulageLoc;
    }

    public String getPreferredCarrier() {
        return preferredCarrier;
    }

    public void setPreferredCarrier(String preferredCarrier) {
        this.preferredCarrier = preferredCarrier;
    }

    public String getTemplateBooking() {
        return templateBooking;
    }

    public void setTemplateBooking(String templateBooking) {
        this.templateBooking = templateBooking;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getProofOfDelivery() {
        return proofOfDelivery;
    }

    public void setProofOfDelivery(String proofOfDelivery) {
        this.proofOfDelivery = proofOfDelivery;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getParentCompany() {
        return parentCompany;
    }

    public void setParentCompany(String parentCompany) {
        this.parentCompany = parentCompany;
    }

    public String getPreAudit() {
        return preAudit;
    }

    public void setPreAudit(String preAudit) {
        this.preAudit = preAudit;
    }

    public String getFederalTaxId() {
        return federalTaxId;
    }

    public void setFederalTaxId(String federalTaxId) {
        this.federalTaxId = federalTaxId;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public String getSicCode() {
        return sicCode;
    }

    public void setSicCode(String sicCode) {
        this.sicCode = sicCode;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondSalesman() {
        return secondSalesman;
    }

    public void setSecondSalesman(String secondSalesman) {
        this.secondSalesman = secondSalesman;
    }

    public String getCollectFsc() {
        return collectFsc;
    }

    public void setCollectFsc(String collectFsc) {
        this.collectFsc = collectFsc;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getAmsFiling() {
        return amsFiling;
    }

    public void setAmsFiling(String amsFiling) {
        this.amsFiling = amsFiling;
    }

    public String getCargoRolloverAuth() {
        return cargoRolloverAuth;
    }

    public void setCargoRolloverAuth(String cargoRolloverAuth) {
        this.cargoRolloverAuth = cargoRolloverAuth;
    }

    public String getBkgPerCargoUnit() {
        return bkgPerCargoUnit;
    }

    public void setBkgPerCargoUnit(String bkgPerCargoUnit) {
        this.bkgPerCargoUnit = bkgPerCargoUnit;
    }

    public String getCustomerAlias() {
        return customerAlias;
    }

    public void setCustomerAlias(String customerAlias) {
        this.customerAlias = customerAlias;
    }

    public String getDirectInvoice() {
        return directInvoice;
    }

    public void setDirectInvoice(String directInvoice) {
        this.directInvoice = directInvoice;
    }

    public String getIntraMappingCode() {
        return intraMappingCode;
    }

    public void setIntraMappingCode(String intraMappingCode) {
        this.intraMappingCode = intraMappingCode;
    }

    public String getMasterBooking() {
        return masterBooking;
    }

    public void setMasterBooking(String masterBooking) {
        this.masterBooking = masterBooking;
    }

    public String getApplySpecialExchangeRateFlag() {
        return applySpecialExchangeRateFlag;
    }

    public void setApplySpecialExchangeRateFlag(String applySpecialExchangeRateFlag) {
        this.applySpecialExchangeRateFlag = applySpecialExchangeRateFlag;
    }

    public String getCustomerBillingCurr() {
        return customerBillingCurr;
    }

    public void setCustomerBillingCurr(String customerBillingCurr) {
        this.customerBillingCurr = customerBillingCurr;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getDateOfSuspension() {
        return dateOfSuspension;
    }

    public void setDateOfSuspension(int dateOfSuspension) {
        this.dateOfSuspension = dateOfSuspension;
    }

    public String getSuspendedBy() {
        return suspendedBy;
    }

    public void setSuspendedBy(String suspendedBy) {
        this.suspendedBy = suspendedBy;
    }

    public String getTaxIdType() {
        return taxIdType;
    }

    public void setTaxIdType(String taxIdType) {
        this.taxIdType = taxIdType;
    }

    public String getBusinessPartnerType() {
        return businessPartnerType;
    }

    public void setBusinessPartnerType(String businessPartnerType) {
        this.businessPartnerType = businessPartnerType;
    }

    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
    }

    public String getInvoiceTemplateCode() {
        return invoiceTemplateCode;
    }

    public void setInvoiceTemplateCode(String invoiceTemplateCode) {
        this.invoiceTemplateCode = invoiceTemplateCode;
    }

    public String getResidentId() {
        return residentId;
    }

    public void setResidentId(String residentId) {
        this.residentId = residentId;
    }

    public String getOverrideCurrency() {
        return overrideCurrency;
    }

    public void setOverrideCurrency(String overrideCurrency) {
        this.overrideCurrency = overrideCurrency;
    }
}
