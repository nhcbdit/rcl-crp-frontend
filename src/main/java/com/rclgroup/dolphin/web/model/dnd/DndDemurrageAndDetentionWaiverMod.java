package com.rclgroup.dolphin.web.model.dnd;

public class DndDemurrageAndDetentionWaiverMod {

       public static final String FIND_SORT_DEFAULT = "";
       public static final String FIND_SORT_REFERENCE ="R";
       public static final String FIND_SORT_INVOICE_NO = "I";
       public static final String FIND_SORT_PRE_POST_INVOICE = "PI";
       public static final String FIND_SORT_BL = "BL";
       public static final String FIND_SORT_INV_AMT = "IA";
       public static final String FIND_SORT_CURR = "C";
       public static final String FIND_SORT_BILL_TO_PARTY = "B";
       public static final String FIND_SORT_DISCOUNT_AMT ="DA";
       public static final String FIND_SORT_FSC = "F";
       
       public static final String STATUS_DEFUALT = "";
       public static final String STATUS_OPEN = "O";
       public static final String STATUS_CANCEL = "C";
       public static final String STATUS_SEND_FOR_REVIEW = "R";
       public static final String STATUS_SEND_FOR_APPROVAL = "S";
       public static final String STATUS_APPROVED = "A";
       
       public static final String SORT_IN_ASC = "A";
       public static final String SORT_IN_DESC = "D";
       
       private String reference;
       private String invoiceNo;
       private String prePostInvoice;
       private String blNo;
       private int dndInvAmt;
       private String invCurr;
       private String billToParty;
       private int discountedAmt;
       private String fsc;
       private String waiverStatus;
       
       public DndDemurrageAndDetentionWaiverMod() {
           this.reference = "";
           this.invoiceNo = "";
           this.prePostInvoice = "";
           this.blNo = "";
           this.dndInvAmt = 0;
           this.invCurr = "";
           this.billToParty = "";
           this.discountedAmt = 0;
           this.fsc = "";
           this.waiverStatus = "";
       }
       
       public void setReference(String reference) {
           this.reference = reference;
       }

       public String getReference() {
           return reference;
       }

       public void setInvoiceNo(String invoiceNo) {
           this.invoiceNo = invoiceNo;
       }

       public String getInvoiceNo() {
           return invoiceNo;
       }

       public void setPrePostInvoice(String prePostInvoice) {
           this.prePostInvoice = prePostInvoice;
       }

       public String getPrePostInvoice() {
           return prePostInvoice;
       }

       public void setBlNo(String blNo) {
           this.blNo = blNo;
       }

       public String getBlNo() {
           return blNo;
       }

       public void setDndInvAmt(int dndInvAmt) {
           this.dndInvAmt = dndInvAmt;
       }

       public int getDndInvAmt() {
           return dndInvAmt;
       }

       public void setInvCurr(String invCurr) {
           this.invCurr = invCurr;
       }

       public String getInvCurr() {
           return invCurr;
       }

       public void setBillToParty(String billToParty) {
           this.billToParty = billToParty;
       }

       public String getBillToParty() {
           return billToParty;
       }

       public void setDiscountedAmt(int discountedAmt) {
           this.discountedAmt = discountedAmt;
       }

       public int getDiscountedAmt() {
           return discountedAmt;
       }

       public void setFsc(String fsc) {
           this.fsc = fsc;
       }

       public String getFsc() {
           return fsc;
       }

       public void setWaiverStatus(String waiverStatus) {
           this.waiverStatus = waiverStatus;
       }

       public String getWaiverStatus() {
           return waiverStatus;
       }

}
