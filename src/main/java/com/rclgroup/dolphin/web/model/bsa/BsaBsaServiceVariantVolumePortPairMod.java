package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaBsaServiceVariantVolumePortPairMod extends RrcStandardMod {
    private String bsaModelId;
    private String bsaServiceVariantId;
    private String bsaRouteId;          // PK_BSA_ROUTE_ID
    private String polPortCallId;                 // FK_POL_BSA_PORT_CALL_ID
    private String polPortCall;                 // FK_POL_BSA_PORT_CALL_ID
    private String polGroup;            
    private String polTsFlag;
    private String podPortCallId;                 // FK_POD_BSA_PORT_CALL_ID
    private String podPortCall;                 // FK_POL_BSA_PORT_CALL_ID
    private String podGroup; 
    private String podTsFlag;
    private String polSeqNo;
    private String podSeqNo;
    private String polCallLv;
    private String podCallLv;
    private String cocTeuFull;          // COC_TEU_LADEN
    private String cocFullWeight;       // COC_TON_LADEN
    private String coc20Gp;             // COC_20GP
    private String coc40Gp;             // COC_40GP
    private String coc45Hc;             // COC_45HC
    private String cocReefer;           // COC_REEFER_PLUGS
    private String cocTeuMt;            // COC_TEU_MT
    private String cocMt20;             // COC_20MT
    private String cocMt40;             // COC_40MT
    private String socTeuFull;          // SOC_TEU_LADEN
    private String socFullWeight;       // SOC_TON_LADEN
    private String socTeuMt;            // SOC_TEU_MT
    private String onBoardTeu;          // 
    private String onBoardWeight;       // 
    private String tsRecord;
    private String tsIndic;             // 
    private String tsService;           //    
    private String tsVariant;           // 
    private String tsPort;              // 
    private String tsGroup;             // 
    private String tsPortLevel;             // 
    private String addDeleteFlag;     
    private String robFlag;     
    private String polGroupFlag;     
    private String podGroupFlag;     
    private String tolelant;
    private Timestamp recordAddDate;
    private Timestamp recordChangeDate;
//    private int exceedVolume;
    private String tsSource;
    private String isDeleted;
    private String portCallSlotTeu;
    private String portCallSlotTons;
    private String portCallSlotReefer;
    private String portCallAvgCocTeu;
    private String portCallAvgSocTeu;
    private String portCallMinTeu;
    
    private String proformaRefNo;
    
    
    public BsaBsaServiceVariantVolumePortPairMod(String bsaModelId,
                    String bsaServiceVariantId,  String bsaRouteId,
                    String polPortCallId,        String polPortCall,
                    String polSeqNo,             String podSeqNo,
                    String polGroup,             String podPortCallId,
                    String podPortCall,          String podGroup,
                    String polTsFlag,            String podTsFlag,
                    String polCallLv,            String podCallLv,
                    String cocTeuFull,           String cocFullWeight,
                    String coc20Gp,              String coc40Gp,
                    String coc45Hc,              String cocReefer,
                    String cocTeuMt,             String cocMt20,
                    String cocMt40,              String socTeuFull,
                    String socFullWeight,        String socTeuMt,
                    String onBoardTeu,           String onBoardWeight,
                    String tsRecord,             String robFlag,
                    String tsIndic,              String tsService,
                    String tsVariant,            String tsPort,
                    String tsGroup,              String tsPortLevel,
                    String addDeleteFlag,        Timestamp recordAddDate,
                    Timestamp recordChangeDate,  String isDeleted,
                    String polGroupFlag,         String podGroupFlag,
                    String tsSource,
                    String portCallSlotTeu,      String portCallSlotTons,
                    String portCallSlotReefer,   String portCallAvgCocTeu,
                    String portCallAvgSocTeu,    String portCallMinTeu,
                    String tolelant,             String proformaRefNo) {
        super();
        this.bsaModelId = bsaModelId;
        this.bsaServiceVariantId = bsaServiceVariantId;
        this.bsaRouteId = bsaRouteId;
        this.polPortCallId = polPortCallId;
        this.polPortCall = polPortCall;
        this.polSeqNo = polSeqNo;
        this.podSeqNo = podSeqNo;
        this.polGroup = polGroup;
        this.podPortCallId = podPortCallId;
        this.podPortCall = podPortCall;
        this.podGroup = podGroup;
        this.polTsFlag = polTsFlag;
        this.podTsFlag = podTsFlag;
        this.polCallLv = polCallLv;
        this.podCallLv = podCallLv;
        this.cocTeuFull = cocTeuFull;
        this.cocFullWeight = cocFullWeight;
        this.coc20Gp = coc20Gp;
        this.coc40Gp = coc40Gp;
        this.coc45Hc = coc45Hc;
        this.cocReefer = cocReefer;
        this.cocTeuMt = cocTeuMt;
        this.cocMt20 = cocMt20;
        this.cocMt40 = cocMt40;
        this.socTeuFull = socTeuFull;
        this.socFullWeight = socFullWeight;
        this.socTeuMt = socTeuMt;
        this.onBoardTeu = onBoardTeu;
        this.onBoardWeight = onBoardWeight;
        this.tsRecord = tsRecord;
        this.tsIndic = tsIndic;
        this.tsService = tsService;
        this.tsVariant = tsVariant;
        this.tsPort = tsPort;
        this.tsGroup = tsGroup;
        this.tsPortLevel = tsPortLevel;
        this.addDeleteFlag= addDeleteFlag;
        this.recordAddDate = recordAddDate;
        this.recordChangeDate = recordChangeDate;
        this.isDeleted = isDeleted;        
        this.robFlag = robFlag;
        this.polGroupFlag = polGroupFlag;
        this.podGroupFlag = podGroupFlag;
        this.tolelant = tolelant;
//        this.exceedVolume = exceedVolume;
        this.tsSource = tsSource;
//Data of Port Call for calculate
        this.portCallSlotTeu = portCallSlotTeu;
        this.portCallSlotTons= portCallSlotTons;
        this.portCallSlotReefer = portCallSlotReefer;
        this.portCallAvgCocTeu = portCallAvgCocTeu;
        this.portCallAvgSocTeu = portCallAvgSocTeu;        
        this.portCallMinTeu = portCallMinTeu;
        
        this.proformaRefNo = proformaRefNo;
    }
    
    public BsaBsaServiceVariantVolumePortPairMod(BsaBsaServiceVariantVolumePortPairMod mod){
         this(mod.bsaModelId,mod.bsaServiceVariantId, mod.bsaRouteId, mod.polPortCallId, mod.polPortCall, mod.polSeqNo, mod.podSeqNo, mod.polGroup, mod.podPortCallId, mod.podPortCall, mod.podGroup,
              mod.polTsFlag, mod.podTsFlag, mod.polCallLv, mod.podCallLv, mod.cocTeuFull, mod.cocFullWeight, mod.coc20Gp, mod.coc40Gp, mod.coc45Hc, mod.cocReefer, mod.cocTeuMt, mod.cocMt20,
              mod.cocMt40, mod.socTeuFull, mod.socFullWeight, mod.socTeuMt, mod.onBoardTeu, mod.onBoardWeight, mod.tsRecord, mod.robFlag, mod.tsIndic, mod.tsService, mod.tsVariant, mod.tsPort,
              mod.tsGroup, mod.tsPortLevel, mod.addDeleteFlag, mod.recordAddDate, mod.recordChangeDate, mod.isDeleted, mod.polGroupFlag, mod.podGroupFlag,  mod.tsSource,
              mod.portCallSlotTeu,mod.portCallSlotTons,mod.portCallSlotReefer,mod.portCallAvgCocTeu,mod.portCallAvgSocTeu,mod.portCallMinTeu,mod.tolelant,mod.proformaRefNo);        
    }
    
    public BsaBsaServiceVariantVolumePortPairMod() {
        this("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",new Timestamp(0),new Timestamp(0),"","","","T","","","","","","","","");
    }

    public boolean equals(Object object) {    
        if (object instanceof BsaBsaServiceVariantVolumePortPairMod) {
            BsaBsaServiceVariantVolumePortPairMod mod = (BsaBsaServiceVariantVolumePortPairMod)object;
           
            if (!this.bsaModelId.equals(mod.getBsaModelId())) {
                System.out.println("condition 0 not equal");
                return false;
            } else if (!this.bsaServiceVariantId.equals(mod.getBsaServiceVariantId())) {
                System.out.println("condition 1 not equal");
                return false;
            } else if (!this.bsaRouteId.equals(mod.getBsaRouteId())) {
                System.out.println("condition 2 not equal");
                return false;
            } else if (!this.polPortCallId.equals(mod.getPolPortCallId())) {
                System.out.println("condition 3 not equal");
                return false;
            } else if (!this.polPortCall.equals(mod.getPolPortCall())) {
                System.out.println("condition 4 not equal");
                return false;
            } else if (!this.podGroup.equals(mod.getPodGroup())) {
                System.out.println("condition 5 not equal");
                return false;
            } else if (!this.polTsFlag.equals(mod.getPolTsFlag())) {
                System.out.println("condition 6 not equal");
                return false;
            } else if (!this.podTsFlag.equals(mod.getPodTsFlag())) {
                System.out.println("condition 7 not equal");
                return false;
            } else if (!this.polCallLv.equals(mod.getPolCallLv())) {
                System.out.println("condition 8 not equal");
                return false;
            } else if (!this.podCallLv.equals(mod.getPodCallLv())) {
                System.out.println("condition 9 not equal");
                return false;
            } else if (!this.cocTeuFull.equals(mod.getCocTeuFull())) {
                System.out.println("condition 10 not equal "+this.cocTeuFull+"and "+mod.getCocTeuFull());
                return false;
            } else if (!this.cocFullWeight.equals(mod.getCocFullWeight())) {
                System.out.println("condition 11 not equal");
                return false;
            } else if (!this.coc20Gp.equals(mod.getCoc20Gp())) {
                System.out.println("condition 12 not equal");
                return false;
            } else if (!this.coc40Gp.equals(mod.getCoc40Gp())) {
                System.out.println("condition 13 not equal");
                return false;
            } else if (!this.coc45Hc.equals(mod.getCoc45Hc())) {
                System.out.println("condition 14 not equal");
                return false;
            } else if (!this.cocReefer.equals(mod.getCocReefer())) {
                System.out.println("condition 15 not equal");
                return false;
            } else if (!this.cocTeuMt.equals(mod.getCocTeuMt())) {
                System.out.println("condition 16 not equal");
                return false;
            } else if (!this.cocMt20.equals(mod.getCocMt20())) {
                System.out.println("condition 17 not equal");
                return false;
            } else if (!this.cocMt40.equals(mod.getCocMt40())) {
                System.out.println("condition 18 not equal");
                return false;            
            } else if (!this.socTeuFull.equals(mod.getSocTeuFull())) {
                System.out.println("condition 19 not equal");
                return false;
            } else if (!this.socFullWeight.equals(mod.getSocFullWeight())) {
                System.out.println("condition 20 not equal");
                return false;
            } else if (!this.socTeuMt.equals(mod.getSocTeuMt())) {
                System.out.println("condition 21 not equal");
                return false;
//            } else if (this.onBoardTeu.equals(mod.getOnBoardTeu())) {
//                System.out.println("condition 22 not equal");
//                return false;
//            } else if (this.onBoardWeight.equals(mod.getOnBoardWeight())) {
//                System.out.println("condition 22 not equal");
//                return false;
            } else if (!this.tsRecord.equals(mod.getTsRecord())) {
                System.out.println("condition 23 not equal");
                return false;
            } else if (!this.tsIndic.equals(mod.getTsIndic())) {
                System.out.println("condition 24 not equal");
                return false;
            } else if (!this.tsService.equals(mod.getTsService())) {
                System.out.println("condition 25 not equal");
                return false;
            } else if (!this.tsVariant.equals(mod.getTsVariant())) {
                System.out.println("condition 26 not equal");
                return false;
            } else if (!this.tsPort.equals(mod.getTsPort())) {
                System.out.println("condition 27 not equal");
                return false;
            } else if (!this.tsGroup.equals(mod.getTsGroup())) {
                System.out.println("condition 28 not equal");
                return false;
            /*} else if (!this.addDeleteFlag.equals(mod.getAddDeleteFlag())) {
                return false;*/
            } else if (!this.recordAddDate.equals(mod.getRecordAddDate())) {
                System.out.println("condition 29 not equal");
                return false;
            } else if (!this.recordChangeDate.equals(mod.getRecordChangeDate())) {
                System.out.println("condition 30 not equal");
                return false;
            } else if (!this.isDeleted.equals(mod.getIsDeleted())) {
                System.out.println("condition 31 not equal");
                return false;
            } else if (!this.robFlag.equals(mod.getRobFlag())) {
                System.out.println("condition 32 not equal");
                return false;
            } else if (!this.polGroupFlag.equals(mod.getPolGroupFlag())) {
                System.out.println("condition 33 not equal");
                return false;              
            } else if (!this.podGroupFlag.equals(mod.getPodGroupFlag())) {
                System.out.println("condition 34 not equal");
                return false;                
//            } else if (this.exceedVolume == mod.getExceedVolume()) {
//                System.out.println("condition 35 not equal");
//                return false;
            } else if (!this.tsSource.equals(mod.getTsSource())) {
                System.out.println("condition 36 not equal");
                return false;
            }
           
        } else {
            return false;
        }        
        return true;
    }    
    
    public void setBsaRouteId(String bsaRouteId) {
        this.bsaRouteId = bsaRouteId;
    }

    public String getBsaRouteId() {
        return bsaRouteId;
    }

    public void setPolPortCallId(String polPortCallId) {
        this.polPortCallId = polPortCallId;
    }

    public String getPolPortCallId() {
        return polPortCallId;
    }

    public void setPolPortCall(String polPortCall) {
        this.polPortCall = polPortCall;
    }

    public String getPolPortCall() {
        return polPortCall;
    }

    public void setPodPortCallId(String podPortCallId) {
        this.podPortCallId = podPortCallId;
    }

    public String getPodPortCallId() {
        return podPortCallId;
    }

    public void setPodPortCall(String podPortCall) {
        this.podPortCall = podPortCall;
    }

    public String getPodPortCall() {
        return podPortCall;
    }

    public void setCocTeuFull(String cocTeuFull) {
        this.cocTeuFull = cocTeuFull;
    }

    public String getCocTeuFull() {
        return cocTeuFull;
    }

    public void setCocFullWeight(String cocFullWeight) {
        this.cocFullWeight = cocFullWeight;
    }

    public String getCocFullWeight() {
        return cocFullWeight;
    }

    public void setCoc20Gp(String coc20Gp) {
        this.coc20Gp = coc20Gp;
    }

    public String getCoc20Gp() {
        return coc20Gp;
    }

    public void setCoc40Gp(String coc40Gp) {
        this.coc40Gp = coc40Gp;
    }

    public String getCoc40Gp() {
        return coc40Gp;
    }

    public void setCoc45Hc(String coc45Hc) {
        this.coc45Hc = coc45Hc;
    }

    public String getCoc45Hc() {
        return coc45Hc;
    }

    public void setCocReefer(String cocReefer) {
        this.cocReefer = cocReefer;
    }

    public String getCocReefer() {
        return cocReefer;
    }

    public void setCocTeuMt(String cocTeuMt) {
        this.cocTeuMt = cocTeuMt;
    }

    public String getCocTeuMt() {
        return cocTeuMt;
    }

    public void setCocMt20(String cocMt20) {
        this.cocMt20 = cocMt20;
    }

    public String getCocMt20() {
        return cocMt20;
    }

    public void setCocMt40(String cocMt40) {
        this.cocMt40 = cocMt40;
    }

    public String getCocMt40() {
        return cocMt40;
    }

    public void setSocTeuFull(String socTeuFull) {
        this.socTeuFull = socTeuFull;
    }

    public String getSocTeuFull() {
        return socTeuFull;
    }

    public void setSocFullWeight(String socFullWeight) {
        this.socFullWeight = socFullWeight;
    }

    public String getSocFullWeight() {
        return socFullWeight;
    }

    public void setSocTeuMt(String socTeuMt) {
        this.socTeuMt = socTeuMt;
    }

    public String getSocTeuMt() {
        return socTeuMt;
    }

    public void setOnBoardTeu(String onBoardTeu) {
        this.onBoardTeu = onBoardTeu;
    }

    public String getOnBoardTeu() {
        return onBoardTeu;
    }

    public void setOnBoardWeight(String onBoardWeight) {
        this.onBoardWeight = onBoardWeight;
    }

    public String getOnBoardWeight() {
        return onBoardWeight;
    }

    public void setTsIndic(String tsIndic) {
        this.tsIndic = tsIndic;
    }

    public String getTsIndic() {
        return tsIndic;
    }

    public void setTsService(String tsService) {
        this.tsService = tsService;
    }

    public String getTsService() {
        return tsService;
    }

    public void setTsVariant(String tsVariant) {
        this.tsVariant = tsVariant;
    }

    public String getTsVariant() {
        return tsVariant;
    }

    public void setTsPort(String tsPort) {
        this.tsPort = tsPort;
    }

    public String getTsPort() {
        return tsPort;
    }

    public void setTsGroup(String tsGroup) {
        this.tsGroup = tsGroup;
    }

    public String getTsGroup() {
        return tsGroup;
    }

    public void setAddDeleteFlag(String addDeleteFlag) {
        this.addDeleteFlag = addDeleteFlag;
    }

    public String getAddDeleteFlag() {
        return addDeleteFlag;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordChangeDate(Timestamp recordChangeDate) {
        this.recordChangeDate = recordChangeDate;
    }

    public Timestamp getRecordChangeDate() {
        return recordChangeDate;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setBsaServiceVariantId(String bsaServiceVariantId) {
        this.bsaServiceVariantId = bsaServiceVariantId;
    }

    public String getBsaServiceVariantId() {
        return bsaServiceVariantId;
    }

    public void setRobFlag(String robFlag) {
        this.robFlag = robFlag;
    }

    public String getRobFlag() {
        return robFlag;
    }

    public String getPolGroup() {
        return polGroup;
    }

    public String getPodGroup() {
        return podGroup;
    }

    public void setTsRecord(String tsRecord) {
        this.tsRecord = tsRecord;
    }

    public String getTsRecord() {
        return tsRecord;
    }

    public void setPolGroup(String polGroup) {
        this.polGroup = polGroup;
    }

    public void setPodGroup(String podGroup) {
        this.podGroup = podGroup;
    }

    public void setPolTsFlag(String polTsFlag) {
        this.polTsFlag = polTsFlag;
    }

    public String getPolTsFlag() {
        return polTsFlag;
    }

    public void setPodTsFlag(String podTsFlag) {
        this.podTsFlag = podTsFlag;
    }

    public String getPodTsFlag() {
        return podTsFlag;
    }

    public void setPolGroupFlag(String polGroupFlag) {
        this.polGroupFlag = polGroupFlag;
    }

    public String getPolGroupFlag() {
        return polGroupFlag;
    }

    public void setPodGroupFlag(String podGroupFlag) {
        this.podGroupFlag = podGroupFlag;
    }

    public String getPodGroupFlag() {
        return podGroupFlag;
    }

    public void setPolSeqNo(String polSeqNo) {
        this.polSeqNo = polSeqNo;
    }

    public String getPolSeqNo() {
        return polSeqNo;
    }

    public void setPodSeqNo(String podSeqNo) {
        this.podSeqNo = podSeqNo; 
    }

    public String getPodSeqNo() {
        return podSeqNo;
    }

    public void setPolCallLv(String polCallLv) {
        this.polCallLv = polCallLv;
    }

    public String getPolCallLv() {
        return polCallLv;
    }

    public void setPodCallLv(String podCallLv) {
        this.podCallLv = podCallLv;
    }

    public String getPodCallLv() {
        return podCallLv;
    }

//    public int getExceedVolume() {
//        return exceedVolume;
//    }

    public void setTsSource(String tsSource) {
        this.tsSource = tsSource;
    }

    public String getTsSource() {
        return tsSource;
    }

    public void setBsaModelId(String bsaModelId) {
        this.bsaModelId = bsaModelId;
    }

    public String getBsaModelId() {
        return bsaModelId;
    }

//    public void setExceedVolume(int exceedVolume) {
//        this.exceedVolume = exceedVolume;
//    }

    public void setPortCallSlotTeu(String portCallSlotTeu) {
        this.portCallSlotTeu = portCallSlotTeu;
    }

    public String getPortCallSlotTeu() {
        return portCallSlotTeu;
    }

    public void setPortCallSlotTons(String portCallSlotTons) {
        this.portCallSlotTons = portCallSlotTons;
    }

    public String getPortCallSlotTons() {
        return portCallSlotTons;
    }

    public void setPortCallSlotReefer(String portCallSlotReefer) {
        this.portCallSlotReefer = portCallSlotReefer;
    }

    public String getPortCallSlotReefer() {
        return portCallSlotReefer;
    }

    public void setPortCallAvgCocTeu(String portCallAvgCocTeu) {
        this.portCallAvgCocTeu = portCallAvgCocTeu;
    }

    public String getPortCallAvgCocTeu() {
        return portCallAvgCocTeu;
    }

    public void setPortCallAvgSocTeu(String portCallAvgSocTeu) {
        this.portCallAvgSocTeu = portCallAvgSocTeu;
    }

    public String getPortCallAvgSocTeu() {
        return portCallAvgSocTeu;
    }

    public void setPortCallMinTeu(String portCallMinTeu) {
        this.portCallMinTeu = portCallMinTeu;
    }

    public String getPortCallMinTeu() {
        return portCallMinTeu;
    }

    public void setTsPortLevel(String tsPortLevel) {
        this.tsPortLevel = tsPortLevel;
    }

    public String getTsPortLevel() {
        return tsPortLevel;
    }

    public void setTolelant(String tolelant) {
        this.tolelant = tolelant;
    }

    public String getTolelant() {
        return tolelant;
    }

    public void setProformaRefNo(String proformaRefNo) {
        this.proformaRefNo = proformaRefNo;
    }

    public String getProformaRefNo() {
        return proformaRefNo;
    }
}
