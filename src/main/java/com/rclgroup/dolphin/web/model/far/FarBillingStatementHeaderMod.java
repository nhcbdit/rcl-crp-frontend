/*-----------------------------------------------------------------------------------------------------------  
FarBillingStatementHeaderMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 30/07/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.model.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;


public class FarBillingStatementHeaderMod extends RrcStandardMod {
    
    private String billNo;
    private int billVersion;
    private int batchNumber;
    private String templateCode;   
    private String ibObXt;
    private String collFsc;
    private String billToParty;
    private String poNumber;
    private String vendorId;
    private String billDate;
    private String billFromDate;
    private String billToDate;
    private String billAttn;
    private String description;
    private String billStatus;
    private String recordAddDate;

    public FarBillingStatementHeaderMod() {
        super();
        billNo = "";
        billVersion = 0;
        batchNumber = 0;
        templateCode = "";
        ibObXt = "";
        collFsc = "";
        billToParty = "";
        poNumber = "";
        vendorId = "";
        billDate = "";
        billFromDate = "";
        billToDate = "";
        billAttn = "";
        description = "";
        billStatus = "";
        recordAddDate = "";
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public int getBillVersion() {
        return billVersion;
    }

    public void setBillVersion(int billVersion) {
        this.billVersion = billVersion;
    }

    public int getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(int batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getIbObXt() {
        return ibObXt;
    }

    public void setIbObXt(String ibObXt) {
        this.ibObXt = ibObXt;
    }

    public String getCollFsc() {
        return collFsc;
    }

    public void setCollFsc(String collFsc) {
        this.collFsc = collFsc;
    }

    public String getBillToParty() {
        return billToParty;
    }

    public void setBillToParty(String billToParty) {
        this.billToParty = billToParty;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillFromDate() {
        return billFromDate;
    }

    public void setBillFromDate(String billFromDate) {
        this.billFromDate = billFromDate;
    }

    public String getBillToDate() {
        return billToDate;
    }

    public void setBillToDate(String billToDate) {
        this.billToDate = billToDate;
    }

    public String getBillAttn() {
        return billAttn;
    }

    public void setBillAttn(String billAttn) {
        this.billAttn = billAttn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getRecordAddDate() {
        return recordAddDate;
    }

    public void setRecordAddDate(String recordAddDate) {
        this.recordAddDate = recordAddDate;
    }
    
}
