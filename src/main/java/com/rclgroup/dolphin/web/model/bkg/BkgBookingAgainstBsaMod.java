/*-----------------------------------------------------------------------------------------------------------  
BkgBookingAgainstBsaMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 19/03/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.model.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.dao.bkg.BkgBookingAgainstBsaDao;
import com.rclgroup.dolphin.web.dao.bkg.BkgBookingAgainstBsaJdbcDao;

import java.sql.Timestamp;

public class BkgBookingAgainstBsaMod extends RrcStandardMod{

    private String sessionId;
    private Timestamp recordAddDate;
    private String permissionUser;
    private String fromDate;
    private String toDate;
    private String service;
    private String vessel;
    private String voyage;
    private String direction;
    private String cocSoc;
    private String portcall;
    private String portcallTerminal;
    private String pol;
    private String polTerminal;
    private String localTs;
    private String pod;
    private String podTerminal;
    
    public BkgBookingAgainstBsaMod() {
        this("","",new Timestamp(0),"","","","","","","","","","","","","","","");
    }
    
    public BkgBookingAgainstBsaMod(String sessionId,String recordAddUser,Timestamp recordAddDate
                                   ,String permissionUser,String fromDate,String toDate,String service
                                   ,String vessel, String voyage,String direction,String cocSoc,String portCall,String portCallTerminal,String pol,String polTerminal,String localTs,String pod,String podTerminal){
        this.sessionId = sessionId;
        this.recordAddUser = recordAddUser;
        this.recordAddDate = recordAddDate;
        this.permissionUser = permissionUser;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.service = service;
        this.vessel = vessel;
        this.voyage = voyage;
        this.direction = direction;
        this.cocSoc = cocSoc;
        this.portcall = portCall;
        this.portcallTerminal = portCallTerminal;
        this.pol = pol;
        this.polTerminal = polTerminal;
        this.localTs = localTs;
        this.pod = this.podTerminal;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setRecordAddDate(Timestamp recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public Timestamp getRecordAddDate() {
        return recordAddDate;
    }

    public void setPermissionUser(String permissionUser) {
        this.permissionUser = permissionUser;
    }

    public String getPermissionUser() {
        return permissionUser;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public void setVessel(String vessel) {
        this.vessel = vessel;
    }

    public String getVessel() {
        return vessel;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public String getVoyage() {
        return voyage;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void setCocSoc(String cocSoc) {
        this.cocSoc = cocSoc;
    }

    public String getCocSoc() {
        return cocSoc;
    }

    public void setPortcall(String portcall) {
        this.portcall = portcall;
    }

    public String getPortcall() {
        return portcall;
    }

    public void setPortcallTerminal(String portcallTerminal) {
        this.portcallTerminal = portcallTerminal;
    }

    public String getPortcallTerminal() {
        return portcallTerminal;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getPol() {
        return pol;
    }

    public void setPolTerminal(String polTerminal) {
        this.polTerminal = polTerminal;
    }

    public String getPolTerminal() {
        return polTerminal;
    }

    public void setLocalTs(String localTs) {
        this.localTs = localTs;
    }

    public String getLocalTs() {
        return localTs;
    }

    public void setPod(String pod) {
        this.pod = pod;
    }

    public String getPod() {
        return pod;
    }

    public void setPodTerminal(String podTerminal) {
        this.podTerminal = podTerminal;
    }

    public String getPodTerminal() {
        return podTerminal;
    }
}
