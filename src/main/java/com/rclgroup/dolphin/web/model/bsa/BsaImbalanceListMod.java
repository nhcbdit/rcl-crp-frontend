/*-----------------------------------------------------------------------------------------------------------  
BsaBsaModelMod.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 11/09/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.model.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.sql.Timestamp;

public class BsaImbalanceListMod extends RrcStandardMod{

    private String imbSessionId;
    private String imbBsaModelId;
    private String imbViewPort;
    private String imbViewPortGrp;
    private String imbDirectSer;
    private String imbDirectVar;
    private String imbDirectPort;
    private String imbDirectPortGrp;
    private String imbTSSer;
    private String imbTSVar;
    private String imbTSPort;
    private String imbTSPortGrp;
    private String imbImLad;
    private String imbImMT;
    private String imbExLad;
    private String imbExMT;
    private String imbAvgImLad;
    private String imbAvgImMT;
    private String imbAvgExLad;
    private String imbAvgExMT;
    private String imbAvgCal;
    private String imbBalance;
    private String imbVarCode;
    private String imbSerCode;
    private String imbWarnLevel;
    private String imbFrequency;
    private String imbDuration;
    private String imbDirectPortPOL;
    private String imbDirectPortPOD;
    private String imbTSDir;
    private String imbCOCTeuLad;
    private String imbCOCTeuMT;
    private String imbAvgTeuLad;
    private String imbAvgTeuMT;
    private String flag;


    private String recordAddDate;
//    private Timestamp recordChangeDate;
    public BsaImbalanceListMod( String imbSessionId,
                             String imbBsaModelId,
                             String imbViewPort,
                             String imbViewPortGrp,
                             String imbDirectSer,
                             String imbDirectVar,
                             String imbDirectPort,
                             String imbDirectPortGrp,
                             String imbTSSer,
                             String imbTSVar,
                             String imbTSPort,
                             String imbTSPortGrp,
                             String imbImLad,
                             String imbImMT,
                             String imbExLad,
                             String imbExMT,
                             String imbAvgImLad,
                             String imbAvgImMT,
                             String imbAvgExLad,
                             String imbAvgExMT,
                             String imbAvgCal,
                             String imbBalance,
                             String imbVarCode,
                             String imbSerCode,
                             String imbWarnLevel,
                             String imbFrequency,
                             String imbDuration,
                             String imbDirectPortPOL,
                             String imbDirectPortPOD,
                             String imbTSDir,
                             String imbCOCTeuLad,
                             String imbCOCTeuMT,
                             String imbAvgTeuLad,
                             String imbAvgTeuMT,
                             String flag,
                             String recordAddDate,
                             String recordStatus,
                             String recordAddUser,
                             String recordChangeUser){
                             super(recordStatus,recordAddUser,recordChangeUser);
                             this.imbSessionId = imbSessionId;
                             this.imbBsaModelId = imbBsaModelId;
                            this.imbViewPort = imbViewPort;
                            this.imbViewPortGrp = imbViewPortGrp;
                            this.imbDirectSer = imbDirectSer;
                            this.imbDirectVar = imbDirectVar;
                            this.imbDirectPort = imbDirectPort;
                            this.imbDirectPortGrp = imbDirectPortGrp;
                            this.imbTSSer = imbTSSer;
                            this.imbTSVar = imbTSVar;
                            this.imbTSPort = imbTSPort;
                            this.imbTSPortGrp = imbTSPortGrp;
                            this.imbImLad = imbImLad;
                            this.imbImMT = imbImMT;
                            this.imbExLad = imbExLad;
                            this.imbExMT = imbExMT;
                            this.imbAvgImLad = imbAvgImLad;
                            this.imbAvgImMT = imbAvgImMT;
                            this.imbAvgExLad = imbAvgExLad;
                            this.imbAvgExMT = imbAvgExMT;
                            this.imbAvgCal = imbAvgCal;
                            this.imbBalance = imbBalance;
                            this.imbVarCode = imbVarCode;
                            this.imbSerCode = imbSerCode;
                            this.imbWarnLevel = imbWarnLevel;
                            this.imbFrequency = imbFrequency;
                            this.imbDuration = imbDuration;
                            this.imbDirectPortPOL = imbDirectPortPOL;
                            this.imbDirectPortPOD = imbDirectPortPOD;
                            this.imbTSDir = imbTSDir;
                            this.imbCOCTeuLad = imbCOCTeuLad;
                            this.imbCOCTeuMT = imbCOCTeuMT;
                            this.imbAvgTeuLad = imbAvgTeuLad;
                            this.imbAvgTeuMT = imbAvgTeuMT;
                            this.flag = flag;
                            this.recordAddDate = recordAddDate;
    }

    public BsaImbalanceListMod() {
        this("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",null,"","","");
    }
    
    public BsaImbalanceListMod(BsaImbalanceListMod mod) {
        this(   
                 mod.getImbSessionId(),
                 mod.getImbBsaModelId(),
                 mod.getImbViewPort(),
                 mod.getImbViewPortGrp(),
                 mod.getImbDirectSer(),
                 mod.getImbDirectVar(),
                 mod.getImbDirectPort(),
                 mod.getImbDirectPortGrp(),
                 mod.getImbTSSer(),
                 mod.getImbTSVar(),
                 mod.getImbTSPort(),
                 mod.getImbTSPortGrp(),
                 mod.getImbImLad(),
                 mod.getImbImMT(),
                 mod.getImbExLad(),
                 mod.getImbExMT(),
                 mod.getImbAvgImLad(),
                 mod.getImbAvgImMT(),
                 mod.getImbAvgExLad(),
                 mod.getImbAvgExMT(),
                 mod.getImbAvgCal(),
                 mod.getImbBalance(),
                 mod.getImbVarCode(),
                 mod.getImbSerCode(),
                 mod.getImbWarnLevel(),
                 mod.getImbFrequency(),
                 mod.getImbDuration(),
                 mod.getImbDirectPortPOL(),
                 mod.getImbDirectPortPOD(),
                 mod.getImbTSDir(),
                 mod.getImbCOCTeuLad(),
                 mod.getImbCOCTeuMT(),
                 mod.getImbAvgTeuLad(),
                 mod.getImbAvgTeuMT(),
                 mod.getFlag(),
                 mod.getRecordAddDate(),
                 mod.getRecordStatus(),
                 mod.getRecordAddUser(),
                 mod.getRecordChangeUser());
    }
    public void setImbViewPort(String imbViewPort) {
        this.imbViewPort = imbViewPort;
    }

    public String getImbViewPort() {
        return imbViewPort;
    }

    public void setImbViewPortGrp(String imbViewPortGrp) {
        this.imbViewPortGrp = imbViewPortGrp;
    }

    public String getImbViewPortGrp() {
        return imbViewPortGrp;
    }

    public void setImbDirectSer(String imbDirectSer) {
        this.imbDirectSer = imbDirectSer;
    }

    public String getImbDirectSer() {
        return imbDirectSer;
    }

    public void setImbDirectVar(String imbDirectVar) {
        this.imbDirectVar = imbDirectVar;
    }

    public String getImbDirectVar() {
        return imbDirectVar;
    }

    public void setImbTSSer(String imbTSSer) {
        this.imbTSSer = imbTSSer;
    }

    public String getImbTSSer() {
        return imbTSSer;
    }

    public void setImbTSVar(String imbTSVar) {
        this.imbTSVar = imbTSVar;
    }

    public String getImbTSVar() {
        return imbTSVar;
    }

    public void setImbTSPort(String imbTSPort) {
        this.imbTSPort = imbTSPort;
    }

    public String getImbTSPort() {
        return imbTSPort;
    }

    public void setImbTSPortGrp(String imbTSPortGrp) {
        this.imbTSPortGrp = imbTSPortGrp;
    }

    public String getImbTSPortGrp() {
        return imbTSPortGrp;
    }

    public void setImbAvgTeuLad(String imbAvgTeuLad) {
        this.imbAvgTeuLad = imbAvgTeuLad;
    }

    public String getImbAvgTeuLad() {
        return imbAvgTeuLad;
    }

    public void setImbAvgTeuMT(String imbAvgTeuMT) {
        this.imbAvgTeuMT = imbAvgTeuMT;
    }

    public String getImbAvgTeuMT() {
        return imbAvgTeuMT;
    }

    public void setImbAvgCal(String imbAvgCal) {
        this.imbAvgCal = imbAvgCal;
    }

    public String getImbAvgCal() {
        return imbAvgCal;
    }

    public void setImbBalance(String imbBalance) {
        this.imbBalance = imbBalance;
    }

    public String getImbBalance() {
        return imbBalance;
    }

//    public void setRecordAddDate(Timestamp recordAddDate) {
//        this.recordAddDate = recordAddDate;
//    }
//
//    public Timestamp getRecordAddDate() {
//        return recordAddDate;
//    }
//
//    public void setRecordChangeDate(Timestamp recordChangeDate) {
//        this.recordChangeDate = recordChangeDate;
//    }
//
//    public Timestamp getRecordChangeDate() {
//        return recordChangeDate;
//    }

    public void setImbBsaModelId(String imbBsaModelId) {
        this.imbBsaModelId = imbBsaModelId;
    }

    public String getImbBsaModelId() {
        return imbBsaModelId;
    }

    public void setImbVarCode(String imbVarCode) {
        this.imbVarCode = imbVarCode;
    }

    public String getImbVarCode() {
        return imbVarCode;
    }

    public void setImbSerCode(String imbSerCode) {
        this.imbSerCode = imbSerCode;
    }

    public String getImbSerCode() {
        return imbSerCode;
    }

    public void setImbDirectPortPOL(String imbDirectPortPOL) {
        this.imbDirectPortPOL = imbDirectPortPOL;
    }

    public String getImbDirectPortPOL() {
        return imbDirectPortPOL;
    }

    public void setImbDirectPortPOD(String imbDirectPortPOD) {
        this.imbDirectPortPOD = imbDirectPortPOD;
    }

    public String getImbDirectPortPOD() {
        return imbDirectPortPOD;
    }

    public void setImbCOCTeuLad(String imbCOCTeuLad) {
        this.imbCOCTeuLad = imbCOCTeuLad;
    }

    public String getImbCOCTeuLad() {
        return imbCOCTeuLad;
    }

    public void setImbCOCTeuMT(String imbCOCTeuMT) {
        this.imbCOCTeuMT = imbCOCTeuMT;
    }

    public String getImbCOCTeuMT() {
        return imbCOCTeuMT;
    }

    public void setImbTSDir(String imbTSDir) {
        this.imbTSDir = imbTSDir;
    }

    public String getImbTSDir() {
        return imbTSDir;
    }

    public void setImbFrequency(String imbFrequency) {
        this.imbFrequency = imbFrequency;
    }

    public String getImbFrequency() {
        return imbFrequency;
    }

    public void setImbDuration(String imbDuration) {
        this.imbDuration = imbDuration;
    }

    public String getImbDuration() {
        return imbDuration;
    }

    public void setImbWarnLevel(String imbWarnLevel) {
        this.imbWarnLevel = imbWarnLevel;
    }

    public String getImbWarnLevel() {
        return imbWarnLevel;
    }

    public void setImbDirectPort(String imbDirectPort) {
        this.imbDirectPort = imbDirectPort;
    }

    public String getImbDirectPort() {
        return imbDirectPort;
    }

    public void setImbDirectPortGrp(String imbDirectPortGrp) {
        this.imbDirectPortGrp = imbDirectPortGrp;
    }

    public String getImbDirectPortGrp() {
        return imbDirectPortGrp;
    }

    public void setImbImLad(String imbImLad) {
        this.imbImLad = imbImLad;
    }

    public String getImbImLad() {
        return imbImLad;
    }

    public void setImbImMT(String imbImMT) {
        this.imbImMT = imbImMT;
    }

    public String getImbImMT() {
        return imbImMT;
    }

    public void setImbExLad(String imbExLad) {
        this.imbExLad = imbExLad;
    }

    public String getImbExLad() {
        return imbExLad;
    }

    public void setImbExMT(String imbExMT) {
        this.imbExMT = imbExMT;
    }

    public String getImbExMT() {
        return imbExMT;
    }

    public void setImbAvgImLad(String imbAvgImLad) {
        this.imbAvgImLad = imbAvgImLad;
    }

    public String getImbAvgImLad() {
        return imbAvgImLad;
    }

    public void setImbAvgImMT(String imbAvgImMT) {
        this.imbAvgImMT = imbAvgImMT;
    }

    public String getImbAvgImMT() {
        return imbAvgImMT;
    }

    public void setImbAvgExLad(String imbAvgExLad) {
        this.imbAvgExLad = imbAvgExLad;
    }

    public String getImbAvgExLad() {
        return imbAvgExLad;
    }

    public void setImbAvgExMT(String imbAvgExMT) {
        this.imbAvgExMT = imbAvgExMT;
    }

    public String getImbAvgExMT() {
        return imbAvgExMT;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setRecordAddDate(String recordAddDate) {
        this.recordAddDate = recordAddDate;
    }

    public String getRecordAddDate() {
        return recordAddDate;
    }

    public void setImbSessionId(String imbSessionId) {
        this.imbSessionId = imbSessionId;
    }

    public String getImbSessionId() {
        return imbSessionId;
    }

}
