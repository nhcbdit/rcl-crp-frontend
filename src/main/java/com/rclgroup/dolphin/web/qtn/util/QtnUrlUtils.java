package com.rclgroup.dolphin.web.qtn.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import com.rclgroup.dolphin.web.util.RutString;

public class QtnUrlUtils {
	public static final String rclServletPackedParamKey = "params";
	public static final String rclServletParamDelimiter = "\\$\\$\\$";
	public static final String rclServletEqDelimiter = "~~";
	
	public static String processTargetUrl(String targetUrl, HttpServletRequest request) {
		String packedParams = RutString.nullToStr(request.getParameter(rclServletPackedParamKey));
        if(packedParams != null && !packedParams.isEmpty()) {
        	try {
        		String[] paramPairs = packedParams.split(rclServletParamDelimiter);
        		packedParams = "";
        		for(String pair : paramPairs) {
        			String[] splitPair = pair.split(rclServletEqDelimiter);
        			if(splitPair.length > 1) {
        				if(packedParams.length() > 0) {
        					packedParams += "&";
        				}
        				packedParams += splitPair[0] + "=" + jsEncodeURI(splitPair[1]); 
        			}
        		}
			} catch (Exception e) {
				e.printStackTrace();
			}
        	targetUrl += (targetUrl.contains("?") ? "&" : "?") + packedParams;
        }
        System.out.println("targetUrl :" +targetUrl);
        return targetUrl;
	}
	
	private static String[] charTable = new String[] { ";", ",", "\\/", "\\?", "\\:", 
			"@", "&", "\\=", "\\+", "\\$", "#", "\\!", "~", "'", "\\(", "\\)" };
	private static String[] encodeTable = new String[] { "%3B", "%2C", "%2F", "%3F", "%3A",
			"%40", "%26", "%3D", "%2B", "%24", "%23", "%21", "%7E", "%27", "%28", "%29" };
	public static String jsEncodeURI(String url) {
		try {
			url = URLEncoder.encode(url, "UTF-8");
			//Then reverse some encoding back to match encodeURI in js
			for(int i=0; i<encodeTable.length; i++) {
				url = url.replaceAll(encodeTable[i], charTable[i]);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return url;
	}
}
