package com.rclgroup.dolphin.web.qtn.util;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import com.rclgroup.dolphin.web.auth.BrowserData;
import com.rclgroup.dolphin.web.auth.IAuth;
import com.rclgroup.dolphin.web.auth.LoginUser;
import com.rclgroup.dolphin.web.auth.TempAuth;
import com.rclgroup.dolphin.web.auth.dao.IAuthDao;
import com.rclgroup.dolphin.web.common.RrcApplicationContextWS;
import com.rclgroup.dolphin.web.dao.rcm.RcmUserDao;
import com.rclgroup.dolphin.web.model.esm.EsmMenuTreeMod;
import com.rclgroup.dolphin.web.model.rcm.RcmUserMod;

public class QtnTokenAuthUtils {
	
	private static IAuth authInstance;
	
	public static boolean auth(RcmUserMod userMod, List<EsmMenuTreeMod> authorities, HttpSession session) {
		if(authInstance == null) {
			int sessionTimeout = 30;
			if(session != null && session.getMaxInactiveInterval() > 0) {
				sessionTimeout = session.getMaxInactiveInterval()/60;
			}
			authInstance = new TempAuth(
					(IAuthDao) RrcApplicationContextWS.getBean("authDao"), 
					(RcmUserDao) RrcApplicationContextWS.getBean("rcmUserDao"),
					sessionTimeout, 30*60);
		}
		session.removeAttribute("userData");
		LoginUser user = authInstance.createInternalUser(userMod, authorities);
		if(user != null) {
			session.setAttribute("user", user);
			//session.setAttribute("userData", user.toBrowserData());
			
			BrowserData data = new BrowserData();
			data.setUserId(userMod.getPrsnLogId());
			data.setUserToken(user.getUserToken());
			data.setLine(userMod.getFscLvl1());
			data.setTrade(userMod.getFscLvl2());
			data.setAgent(userMod.getFscLvl3());
			data.setFscCode(userMod.getFscCode());
			session.setAttribute("userData", data);
			
			
			session.setAttribute("userDesc", user.getUserMod().getDescr());
			session.setAttribute("publicToken", UUID.randomUUID());


			System.out.println("[RcmLoginSvc][userData] : "+data.toString());
			System.out.println("[RcmLoginSvc][userDesc] : "+user.getUserMod().getDescr());
			
			return true;
		}else {
			return false;
		}
	}
}
