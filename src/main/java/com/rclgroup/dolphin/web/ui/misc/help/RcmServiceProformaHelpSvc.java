/*-----------------------------------------------------------------------------------------------------------  
RcmServiceProformaHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.vss.VssServiceProformaDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmServiceProformaHelpSvc extends RrcStandardHelpSvc {
    public RcmServiceProformaHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmServiceProformaHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmServiceProformaHelpUim";
        String helpScnName = "/RcmServiceProformaHelpScn.jsp?service=ui.misc.help.RcmServiceProformaHelpSvc";
        
        RrcStandardHelpUim uim = (RcmServiceProformaHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmServiceProformaHelpUim newServiceProformaHelpUim = new RcmServiceProformaHelpUim();
        
        // get Dao
        newServiceProformaHelpUim.setVssServiceProformaDao((VssServiceProformaDao)getBean("vssServiceProformaDao"));
        RrcStandardHelpUim newUim = newServiceProformaHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmServiceProformaHelpSvc][execute]: Finished");
    }
}

