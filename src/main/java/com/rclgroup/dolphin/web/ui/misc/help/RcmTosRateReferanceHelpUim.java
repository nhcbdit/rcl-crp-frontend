/*-----------------------------------------------------------------------------------------------------------  
RcmTosRateReferanceHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 05/06/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;

import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.tos.TosRateHeaderDao;
import com.rclgroup.dolphin.web.dao.tos.TosServiceDao;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;

import com.rclgroup.dolphin.web.util.RutString;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class RcmTosRateReferanceHelpUim extends RcmStandardHelpOptimizeUim{
    
    private TosRateHeaderDao tosRateHeaderDao;
    private String port;
    private String terminal;
    
    public RcmTosRateReferanceHelpUim(){
        super();
        
        this.setSortBy("TOS_RATE_REF");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public RcmTosRateReferanceHelpUim(String port, String terminal){
        super();
        
        this.setSortBy("TOS_RATE_REF");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
        this.port = port;
        this.terminal = terminal;
    }
        
    public void setPort(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminal() {
        return terminal;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Rate Reference";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        
        columnNameShow.put("OPERATION_TYPE", new RcmColumnNameShowMod("Operation Type|10|left|STRING"));
        columnNameShow.put("DESCR", new RcmColumnNameShowMod("Operation Description|25|left|STRING"));
        columnNameShow.put("TOS_RATE_REF", new RcmColumnNameShowMod("Rate Reference|25|left|STRING"));
        
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"OPERATION_TYPE", "DESCR", "TOS_RATE_REF"};
        } 
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sb.append("SELECT OPERATION_TYPE, ");
            sb.append(" DESCR, ");
            sb.append(" TOS_RATE_REF ");
            sb.append(" FROM ( ");
            sb.append("SELECT OPERATION_TYPE, ");
            sb.append("       (SELECT DESCR FROM TOS_OPR_CODE_MASTER OPR_MST WHERE OPR_MST.OPR_CODE = TOS_RATE_HEADER.OPERATION_TYPE) DESCR, ");
            sb.append("       TOS_RATE_REF ");
            sb.append(" FROM TOS_RATE_HEADER  WHERE PORT = '"+port+"' ");
            sb.append(" AND TERMINAL = '"+terminal+"' ");
            sb.append(" ) ");
            sb.append(" [and :columnName :conditionWild :columnFind] ");
        }
        return sb.toString();
    }
}
