package com.rclgroup.dolphin.web.ui.crp;

import java.util.List;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rclgroup.dolphin.web.auth.IAuth;
import com.rclgroup.dolphin.web.auth.LoginUser;
import com.rclgroup.dolphin.web.auth.TempAuth;
import com.rclgroup.dolphin.web.auth.dao.IAuthDao;
import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcApplicationContextWS;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;
import com.rclgroup.dolphin.web.dao.rcm.RcmUserDao;
import com.rclgroup.dolphin.web.model.esm.EsmMenuTreeMod;
import com.rclgroup.dolphin.web.model.rcm.RcmUserMod;

public class CrpScheduleMonitoringSvc extends RrcStandardSvc{
	public static final String TARGET_PAGE = "/CrpScheduleMonitoringScn.jsp";

	private static IAuth authInstance;
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context)
			throws Exception {
		String strTarget = RcmConstant.CRP_PAGE_URL + TARGET_PAGE;
		System.out.println("CAll CrpScheduleMonitoringSvc.");
		
		request.setAttribute("target", strTarget);
	}

	public static boolean auth(RcmUserMod userMod, List<EsmMenuTreeMod> authorities, HttpSession session) {
		if(authInstance == null) {
			int sessionTimeout = 30;
			if(session != null && session.getMaxInactiveInterval() > 0) {
				sessionTimeout = session.getMaxInactiveInterval()/60;
			}
			authInstance = new TempAuth(
					(IAuthDao) RrcApplicationContextWS.getBean("authDao"), 
					(RcmUserDao) RrcApplicationContextWS.getBean("rcmUserDao"),
					sessionTimeout, 30*60);
		}
		session.removeAttribute("userData");
		
		LoginUser user = authInstance.createInternalUser(userMod, authorities);
		if(user != null) {
			session.setAttribute("user", user);
			session.setAttribute("userData", user.toBrowserData());
			session.setAttribute("userDesc", user.getUserMod().getDescr());
			session.setAttribute("publicToken", UUID.randomUUID());


			System.out.println("[RcmLoginSvc][userData] : "+user.toBrowserData().toString());
			System.out.println("[RcmLoginSvc][userDesc] : "+user.getUserMod().getDescr());
			
			return true;
		}else {
			return false;
		}
	}
}
