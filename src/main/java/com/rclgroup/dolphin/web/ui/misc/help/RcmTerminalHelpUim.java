 /*-----------------------------------------------------------------------------------------------------------  
 RcmTerminalHelpUim.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 10/06/2008
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


public class RcmTerminalHelpUim extends RcmStandardHelpOptimizeUim{
    private String usrPermission;
    private String fscCode;
    private String regionCode;
    private String areaCode;
    private String controlZone;
    private String controlZoneCode;
    private String zoneCode;
    private String point;
    
    public RcmTerminalHelpUim() {
        super();
        
        // default sort
        this.setSortBy("TERMINAL_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
        String fscCode = RutString.nullToStr(request.getParameter("hidFsc")).toUpperCase().trim();
        String regionCode = RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase().trim();
        String areaCode = RutString.nullToStr(request.getParameter("txtArea")).toUpperCase().trim();
        String controlZone = RutString.nullToStr(request.getParameter("hidControlZone")).toUpperCase().trim();
        String controlZoneCode = RutString.nullToStr(request.getParameter("txtControlZone")).toUpperCase().trim();
        String zoneCode = RutString.nullToStr(request.getParameter("txtZone")).toUpperCase().trim();
        String point = RutString.nullToStr(request.getParameter("txtPoint")).toUpperCase().trim();
        
        this.usrPermission = usrPermission;
        this.fscCode = fscCode;
        this.regionCode = regionCode;
        this.areaCode = areaCode;
        this.controlZone = controlZone;
        this.controlZoneCode = controlZoneCode;
        this.zoneCode = zoneCode;
        this.point = point;
    }
    
    public String findTitleNameShowByType(String type) {
        String titleName = null;
        
        if (RcmConstant.GET_GENERAL.equals(type) || "GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            titleName = "Terminal/Depot";
        }else if(RcmConstant.GET_TERMINAL_FILTERED.equals(type)){
            titleName = "Port/Terminal";
        }
        
        return titleName;
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type) || "GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            columnNameShow.put("TERMINAL_CODE", new RcmColumnNameShowMod("Terminal Code|15|left|STRING"));
            columnNameShow.put("TERMINAL_NAME", new RcmColumnNameShowMod("Terminal Name|25|left|STRING"));
            columnNameShow.put("POINT_CODE", new RcmColumnNameShowMod("Point Code|10|left|STRING"));
            columnNameShow.put("FLAG", new RcmColumnNameShowMod("Flag|7|left|STRING"));
            columnNameShow.put("ZONE", new RcmColumnNameShowMod("Zone|7|left|STRING"));
            columnNameShow.put("CONTROL_ZONE", new RcmColumnNameShowMod("Control Zone|15|left|STRING"));
            columnNameShow.put("AREA", new RcmColumnNameShowMod("Area|7|left|STRING"));
            columnNameShow.put("REGION", new RcmColumnNameShowMod("Region|10|left|STRING"));
            columnNameShow.put("STATUS", new RcmColumnNameShowMod("Status|15|left|STRING"));
        } else if(RcmConstant.GET_TERMINAL_FILTERED.equals(type)){
            columnNameShow.put("TERMINAL_CODE", new RcmColumnNameShowMod("Terminal Code|15|left|STRING"));
            columnNameShow.put("TERMINAL_NAME", new RcmColumnNameShowMod("Terminal Name|25|left|STRING"));
            columnNameShow.put("POINT_CODE", new RcmColumnNameShowMod("Port Code|10|left|STRING"));
            columnNameShow.put("STATUS", new RcmColumnNameShowMod("Status|15|left|STRING"));
        }
        
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type) || "GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            arrReturnValue = new String[] {"TERMINAL_CODE", "POINT_CODE", "ZONE", "CONTROL_ZONE", "AREA", "REGION"};
        } else if(RcmConstant.GET_TERMINAL_FILTERED.equals(type)){
            arrReturnValue = new String[] {"TERMINAL_CODE", "POINT_CODE"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sb.append("select distinct TERMINAL_CODE ");
            sb.append(" ,TERMINAL_NAME ");
            sb.append(" ,POINT_CODE ");   
            sb.append(" ,FLAG ");
            sb.append(" ,ZONE ");
            sb.append(" ,CONTROL_ZONE ");            
            sb.append(" ,AREA ");
            sb.append(" ,REGION ");
            sb.append(" ,STATUS "); 
            sb.append("from VR_EMS_TERMINAL ");
            sb.append("where STATUS = 'Active' ");
            
            StringBuffer sbWhere = new StringBuffer("");
            if (usrPermission != null && !"".equals(usrPermission)) {
                sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_TERMINAL('"+usrPermission+"', TERMINAL_CODE) = '"+RcmConstant.FLAG_YES+"' ");
            }
            if (regionCode != null && !"".equals(regionCode)) {
                sbWhere.append(" and REGION = '"+regionCode+"' ");
            }
            if (areaCode != null && !"".equals(areaCode)) {
                sbWhere.append(" and AREA = '"+areaCode+"' ");
            }
            if (controlZoneCode != null && !"".equals(controlZoneCode)) {
                sbWhere.append(" and CONTROL_ZONE = '"+controlZoneCode+"' ");
            }
            if (zoneCode != null && !"".equals(zoneCode)) {
                sbWhere.append(" and ZONE = '"+zoneCode+"' ");
            }
            if (point != null && !"".equals(point)) {
                sbWhere.append(" and POINT_CODE = '"+point+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append(" and "+sbWhere.substring(4));
            }
            
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        
        } else if ("GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            sb.append("select distinct TERMINAL_CODE ");
            sb.append(" ,TERMINAL_NAME ");
            sb.append(" ,POINT_CODE ");   
            sb.append(" ,FLAG ");
            sb.append(" ,ZONE ");
            sb.append(" ,CONTROL_ZONE ");            
            sb.append(" ,AREA ");
            sb.append(" ,REGION ");
            sb.append(" ,STATUS "); 
            sb.append("from VR_EMS_TERMINAL ");
            sb.append("where STATUS = 'Active' ");
            
            StringBuffer sbWhere = new StringBuffer("");
            if (point != null && !"".equals(point)) {
                sbWhere.append(" and POINT_CODE = '"+point+"' ");
            }
            
            // Edit by NUTTHA1 (Jan 14, 2013) (PD_CR_20110810-01) 
            // No need to filter FSC in case of FSC is R or BLANK
            // Cannot change in set up, so validate here
            /*if (fscCode != null && !"".equals(fscCode)) {
                sbWhere.append(" and FSC = '"+fscCode+"' ");
            }*/
             if (fscCode != null && !"".equals(fscCode) && !"R".equals(fscCode)) {
                 sbWhere.append(" and FSC = '"+fscCode+"' ");
             }
            
            if (controlZone != null && !"".equals(controlZone)) {
                sbWhere.append(" and CONTROL_ZONE = '"+controlZone+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append(" and "+sbWhere.substring(4));
            }
            
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        } else if (RcmConstant.GET_TERMINAL_FILTERED.equals(type)) {
            sb.append("select distinct TERMINAL_CODE ");
            sb.append(" ,TERMINAL_NAME ");
            sb.append(" ,POINT_CODE ");           
            sb.append(" ,STATUS "); 
            sb.append("from VR_EMS_TERMINAL ");
            sb.append("where STATUS = 'Active' ");
            sb.append(" and FLAG = 'Terminal' ");
            
            StringBuffer sbWhere = new StringBuffer("");
            if (usrPermission != null && !"".equals(usrPermission)) {
                sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_TERMINAL('"+usrPermission+"', TERMINAL_CODE) = '"+RcmConstant.FLAG_YES+"' ");
            }           
            if (point != null && !"".equals(point)) {
                sbWhere.append(" and POINT_CODE = '"+point+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append(" and "+sbWhere.substring(4));
            }
            
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        
        }
        
        System.out.println(sb.toString());
        
        return sb.toString();
    }
    //end: implement function
    
    public String getUsrPermission() {
        return usrPermission;
    }

    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }

    public String getFscCode() {
        return fscCode;
    }

    public void setFscCode(String fscCode) {
        this.fscCode = fscCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getControlZone() {
        return controlZone;
    }

    public void setControlZone(String controlZone) {
        this.controlZone = controlZone;
    }

    public String getControlZoneCode() {
        return controlZoneCode;
    }

    public void setControlZoneCode(String controlZoneCode) {
        this.controlZoneCode = controlZoneCode;
    }

    public String getZoneCode() {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
}
