/*-----------------------------------------------------------------------------------------------------------  
RcmSlotAgreementHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Surachai Yindeeram 04/02/2015
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY     -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.model.cam.CamFscMod;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RcmSlotServiceGroupHelpUim extends RcmStandardHelpOptimizeUim {
    private String depotFscLvl1;
    private String depotFscLvl2;
    private String depotFscLvl3;
    
    public RcmSlotServiceGroupHelpUim() {
        super();
        // default sort
        this.setSortBy("VAL");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        HttpSession session = request.getSession(false);
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        if(camFscDao != null){
            CamFscMod camFscMod = camFscDao.findByKeyfscCode(fscCodeOfUser);
            depotFscLvl1 = camFscMod.getLineCode();
            depotFscLvl2 = camFscMod.getRegionCode();
            depotFscLvl3 = camFscMod.getAgentCode();
            isControlFsc = camFscDao.isControlFsc(userBean.getFscLvl1(), userBean.getFscLvl2(), userBean.getFscLvl3(), userBean.getFscCode());
        }
    }
     
    public String findTitleNameShowByType(String type) {
        if(RcmConstant.GET_GENERAL.equals(type)){
            return "Slot Service Group";
        }
        return "Slot Service Group";
    }
     
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]

        if (RcmConstant.GET_GENERAL.equals(type)){
            columnNameShow.put("VAL", new RcmColumnNameShowMod("Service Group Code|10|left|STRING"));
            columnNameShow.put("DESCRIPTION", new RcmColumnNameShowMod("Description|20|left|STRING"));
            columnNameShow.put("DIVISION_CODE", new RcmColumnNameShowMod("Division Code|20|left|STRING"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"VAL", "DESCRIPTION","DIVISION_CODE"};
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sb.append(" SELECT VAL, DESCRIPTION,DIVISION_CODE FROM (SELECT VALUE VAL, DESCRIPTION,DIVISION_CODE FROM  " +
                       " VASAPPS.VR_SLM_LKP_SRVC_GRP WHERE STATUS = 'A') WHERE 1=1  ");
           
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append(" [order by :sortBy :sortIn] ");
        }
        System.out.print("===============### "+sb.toString()+" ###==================");
        return sb.toString();
    }
    //end: implement function


    public CamFscDao getCamFscDao() {
        return camFscDao;
    }

   
}
