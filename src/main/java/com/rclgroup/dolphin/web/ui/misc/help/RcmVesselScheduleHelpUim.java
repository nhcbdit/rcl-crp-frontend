/*-----------------------------------------------------------------------------------------------------------  
RcmSvcVslVoyDirHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 14/10/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description
01 09/09/09 WUT                       Convert to optimization help screen
02 19/11/09 POR                       Added type GET_SRV_VSS_VOY_INVOY
03 21/07/11 API                       Remove format date
04 08/12/11 SON                       Change VR_DEX_SVC_VSL_VOY_DIR_PORT to VR_DEX_SVC_VSL_VOY_DIR_PORT_02
05 19/07/12 PAN                       Added type GET_VESSEL_FROM_VSA
06 25/02/14 EAY         @06           Added type GET_GENERAL_WITH_VESSEL_VOYAGE
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;


public class RcmVesselScheduleHelpUim extends RcmStandardHelpOptimizeUim {

    private String usrPermission;

    public RcmVesselScheduleHelpUim() 
    {
        super();
        
        // default sort
        this.setSortBy("SERVICE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
         
        this.usrPermission = usrPermission;
    }
     
    public String findTitleNameShowByType(String type) {
        return "Service/Vessel/Voyage/Direction";
    }
     
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_DISCHARGE.equals(type)) {
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|10|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|10|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|10|left|STRING"));
            columnNameShow.put("VESSEL_NAME", new RcmColumnNameShowMod("Vessel Name|25|left|STRING"));
        } else if (RcmConstant.GET_GENERAL_WITH_COMPLETED.equals(type) || RcmConstant.GET_SRV_VSS_VOY_PORT.equals(type)) {
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|10|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|10|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|10|left|STRING"));
            columnNameShow.put("VESSEL_NAME", new RcmColumnNameShowMod("Vessel Name|20|left|STRING"));
            columnNameShow.put("SEQ", new RcmColumnNameShowMod("Seq|5|left|STRING"));
            columnNameShow.put("PORT", new RcmColumnNameShowMod("Port|10|left|STRING"));
            columnNameShow.put("TERMINAL", new RcmColumnNameShowMod("Terminal|10|left|STRING"));
        } else if (RcmConstant.GET_SRV_VSS_VOY_POL.equals(type)) {
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|7|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|7|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|12|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getDirectionDesc"));
            columnNameShow.put("PORT", new RcmColumnNameShowMod("POL|10|left|STRING"));
            columnNameShow.put("PORT_TERMINAL", new RcmColumnNameShowMod("POL Terminal|10|left|STRING"));
            columnNameShow.put("ETA", new RcmColumnNameShowMod("ETA Date|11|left|STRING"));
            columnNameShow.put("ETD", new RcmColumnNameShowMod("ETD Date|11|left|STRING"));
            columnNameShow.put("ATA", new RcmColumnNameShowMod("ATA Date|11|left|STRING"));
            columnNameShow.put("ATD", new RcmColumnNameShowMod("ATD Date|11|left|STRING"));
        } else if (RcmConstant.GET_SRV_VSS_VOY_POD.equals(type)) {
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|7|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|7|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|12|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getDirectionDesc"));
            columnNameShow.put("PORT", new RcmColumnNameShowMod("POD|10|left|STRING"));
            columnNameShow.put("PORT_TERMINAL", new RcmColumnNameShowMod("POD Terminal|10|left|STRING"));
            columnNameShow.put("ETA", new RcmColumnNameShowMod("ETA Date|11|left|STRING"));
            columnNameShow.put("ETD", new RcmColumnNameShowMod("ETD Date|11|left|STRING"));
            columnNameShow.put("ATA", new RcmColumnNameShowMod("ATA Date|11|left|STRING"));
            columnNameShow.put("ATD", new RcmColumnNameShowMod("ATD Date|11|left|STRING"));
        } else if (RcmConstant.GET_SRV_VSS_VOY_INVOY.equals(type) ){
            columnNameShow.put("PORT", new RcmColumnNameShowMod("Port|10|left|STRING"));
            columnNameShow.put("IN_VOYAGE", new RcmColumnNameShowMod("In Voyage#|11|left|STRING"));
            columnNameShow.put("ETA", new RcmColumnNameShowMod("ETA Date|11|left|STRING"));
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|10|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|10|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|10|left|STRING"));
        }else if(RcmConstant.GET_VESSEL_FROM_VSA.equals(type)){
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|10|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|10|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|10|left|STRING"));
            columnNameShow.put("VESSEL_NAME", new RcmColumnNameShowMod("Vessel Name|25|left|STRING"));
        }
        //@06 : Begin
        else if (RcmConstant.GET_GENERAL_WITH_VESSEL_VOYAGE.equals(type)) 
        {
            columnNameShow.put("FK_SERVICE", new RcmColumnNameShowMod("Service|10|left|STRING"));
            columnNameShow.put("FK_VESSEL", new RcmColumnNameShowMod("Vessel|10|left|STRING"));
            columnNameShow.put("FK_VOYAGE", new RcmColumnNameShowMod("Voyage|15|left|STRING"));
            columnNameShow.put("FK_DIR", new RcmColumnNameShowMod("Direction|10|left|STRING"));
            columnNameShow.put("FK_PCSQ", new RcmColumnNameShowMod("Loadlist Seq|15|left|STRING"));
            columnNameShow.put("DN_PORT", new RcmColumnNameShowMod("PORT|10|left|STRING"));
            columnNameShow.put("DN_TERMINAL", new RcmColumnNameShowMod("Terminal|10|left|STRING"));
            columnNameShow.put("ACT_VOYAGE", new RcmColumnNameShowMod("Invoy|10|left|STRING"));          
        }
        //@06 : End
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_DISCHARGE.equals(type)) {
            arrReturnValue = new String[] {"SERVICE", "VESSEL", "VOYAGE", "DIRECTION"};
        } else if (RcmConstant.GET_GENERAL_WITH_COMPLETED.equals(type) || RcmConstant.GET_SRV_VSS_VOY_PORT.equals(type)) {
            arrReturnValue = new String[] {"SERVICE", "VESSEL", "VOYAGE", "DIRECTION", "SEQ", "PORT", "TERMINAL"};
        } else if (RcmConstant.GET_SRV_VSS_VOY_POL.equals(type) || RcmConstant.GET_SRV_VSS_VOY_POD.equals(type)) {
            arrReturnValue = new String[] {"SERVICE", "VESSEL", "VOYAGE", "DIRECTION", "PORT", "PORT_TERMINAL", "ETA", "ETD", "ATA", "ATD"};
        } else if (RcmConstant.GET_SRV_VSS_VOY_INVOY.equals(type)){
            arrReturnValue = new String[] {"SERVICE", "VESSEL", "VOYAGE", "DIRECTION", "IN_VOYAGE", "ETA"};
        } else if(RcmConstant.GET_VESSEL_FROM_VSA.equals(type)){
            arrReturnValue = new String[] {"SERVICE", "VESSEL", "VOYAGE", "DIRECTION"};
        }
        //@06 : Begin
        else if (RcmConstant.GET_GENERAL_WITH_VESSEL_VOYAGE.equals(type)) 
        {
            arrReturnValue = new String[] {"FK_SERVICE","FK_VESSEL","FK_VOYAGE","FK_DIR","FK_PCSQ","DN_PORT","DN_TERMINAL","ACT_VOYAGE","ACT_PCSQ"};
        }
        //@06 : End
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sb.append("select distinct SERVICE ");
            sb.append(" ,VESSEL ");
            sb.append(" ,VOYAGE ");
            sb.append(" ,DIRECTION "); 
            sb.append(" ,VESSEL_NAME ");
            sb.append("from VR_DEX_SVC_VSL_VOY_DIR ");
            sb.append("[where :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        
        } else if (RcmConstant.GET_DISCHARGE.equals(type)) {
            sb.append("select distinct SERVICE ");
            sb.append(" ,VESSEL ");
            sb.append(" ,VOYAGE ");
            sb.append(" ,DIRECTION "); 
            sb.append(" ,VESSEL_NAME ");
            sb.append("from VR_DEX_SVC_VSL_VOY_DIR_DISCH ");
            sb.append("[where :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        
        } else if (RcmConstant.GET_GENERAL_WITH_COMPLETED.equals(type) || RcmConstant.GET_SRV_VSS_VOY_PORT.equals(type)) {
            sb.append("select distinct SERVICE ");
            sb.append(" ,VESSEL ");
            sb.append(" ,VOYAGE ");
            sb.append(" ,DIRECTION "); 
            sb.append(" ,VESSEL_NAME ");
            sb.append(" ,to_char(SEQ) as SEQ ");
            sb.append(" ,PORT ");
            sb.append(" ,TERMINAL ");
            sb.append("from VR_DEX_SVC_VSL_VOY_DIR_PORT_02 ");
            if (RcmConstant.GET_GENERAL_WITH_COMPLETED.equals(type)) {
                sb.append("where trim(STATUS) = '"+RcmConstant.FLAG_YES+"' ");
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");    
            }
            sb.append("[order by :sortBy :sortIn] ");
            
        } else if (RcmConstant.GET_SRV_VSS_VOY_POL.equals(type) || RcmConstant.GET_SRV_VSS_VOY_POD.equals(type)) {
            sb.append("select distinct SERVICE ");
            sb.append(" ,VESSEL ");
            sb.append(" ,VOYAGE ");
            sb.append(" ,DIRECTION "); 
            sb.append(" ,PORT ");
            sb.append(" ,PORT_TERMINAL ");
            /*  //BEGIN #03
            sb.append(" ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(ETA) as ETA ");
            sb.append(" ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(ETD) as ETD ");
            sb.append(" ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(ATA) as ATA ");
            sb.append(" ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(ATD) as ATD ");
            */ // END #03
            sb.append(" , ETA ");
            sb.append(" , ETD ");
            sb.append(" , ATA ");
            sb.append(" , ATD ");
            
            sb.append("from VR_DEX_SVC_VSL_VOY_DIR_POL ");
            
            StringBuffer sbWhere = new StringBuffer("");
            if (usrPermission != null && !"".equals(usrPermission)) {
                sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+usrPermission+"', PORT) = '"+RcmConstant.FLAG_YES+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere.substring(4));
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        } else if (RcmConstant.GET_SRV_VSS_VOY_INVOY.equals(type)) {
            sb.append("select distinct PORT ");
            sb.append(" ,IN_VOYAGE ");
           // sb.append(" ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(ETA) as ETA "); #03
            sb.append(" ,ETA ");
            sb.append(" ,SERVICE ");
            sb.append(" ,VESSEL "); 
            sb.append(" ,VOYAGE ");
            sb.append(" ,DIRECTION "); 
            sb.append("from VR_DEX_SVC_VSL_VOY_INVOY_DIR ");
            sb.append("[where :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        }else if(RcmConstant.GET_VESSEL_FROM_VSA.equals(type)){
        //select distinct vvh.fk_service,vvh.fk_vessel, vvh.fk_voyage, vvh.fk_direction, itp060.vslgnm from vsa_voyage_header vvh 
        // left join itp060 on vvh.fk_vessel = itp060.vsvess
            sb.append("select distinct vvh.fk_service as SERVICE");
            sb.append(" ,vvh.fk_vessel AS VESSEL ");
            sb.append(" ,vvh.fk_voyage as VOYAGE");
            sb.append(" ,vvh.fk_direction as DIRECTION"); 
            sb.append(" ,itp060.vslgnm as  VESSEL_NAME");
            sb.append(" from vsa_voyage_header vvh ");
            sb.append("   left join itp060 on vvh.fk_vessel = itp060.vsvess ");
            sb.append("[where :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        }
        //@06 : Begin
        else if (RcmConstant.GET_GENERAL_WITH_VESSEL_VOYAGE.equals(type)) 
        {
             this.setSortBy("FK_SERVICE");
             sb.append(" SELECT FK_SERVICE ");
             sb.append(" , FK_VESSEL ");
             sb.append(" , FK_VOYAGE ");
             sb.append(" , FK_DIR "); 
             sb.append(" , FK_PCSQ ");
             sb.append(" , DN_PORT ");
             sb.append(" , DN_TERMINAL ");
             sb.append(" , ACT_VOYAGE ");
             sb.append(" , ACT_PCSQ ");
             sb.append(" FROM VASAPPS.TOS_VESSEL_VOYAGE  ");
             sb.append("[where :columnName :conditionWild :columnFind] ");    
             sb.append("[order by :sortBy :sortIn] ");
        }
        //@06 : End
        return sb.toString();
    }
    //end: implement function
    
    public String getUsrPermission() {
        return usrPermission;
    }
     
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
