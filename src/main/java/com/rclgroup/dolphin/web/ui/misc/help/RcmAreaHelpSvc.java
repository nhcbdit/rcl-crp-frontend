/*-----------------------------------------------------------------------------------------------------------  
RcmAreaHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsAreaDao;
import com.rclgroup.dolphin.web.util.RutString;
 import javax.servlet.ServletContext;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 import javax.servlet.http.HttpSession;

 public class RcmAreaHelpSvc extends RrcStandardHelpSvc {
     public RcmAreaHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmAreaHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmAreaHelpUim";
         String helpScnName = "/RcmAreaHelpScn.jsp?service=ui.misc.help.RcmAreaHelpSvc";
         
         RrcStandardHelpUim uim = (RcmAreaHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmAreaHelpUim newAreaHelpUim = new RcmAreaHelpUim();
         
         newAreaHelpUim.setRegionCode(RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase());
         
         newAreaHelpUim.setEmsAreaDao((EmsAreaDao)getBean("emsAreaDao"));
         RrcStandardHelpUim newUim = newAreaHelpUim;
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmAreaHelpSvc][execute]: Finished");
     }
 }
