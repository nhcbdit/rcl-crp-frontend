/*-----------------------------------------------------------------------------------------------------------  
RcmGeographicalRegionHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;

import com.rclgroup.dolphin.web.dao.ems.EmsGeographicalRegionDao;

import javax.servlet.ServletContext;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 import javax.servlet.http.HttpSession;

 public class RcmGeographicalRegionHelpSvc extends RrcStandardHelpSvc {
     public RcmGeographicalRegionHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmGeographicalRegionHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmGeographicalRegionHelpUim";
         String helpScnName = "/RcmGeographicalRegionHelpScn.jsp?service=ui.misc.help.RcmGeographicalRegionHelpSvc";
         
         RrcStandardHelpUim uim = (RcmGeographicalRegionHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmGeographicalRegionHelpUim newRcmGeographicalRegionHelpUim = new RcmGeographicalRegionHelpUim();
         
         newRcmGeographicalRegionHelpUim.setEmsGeographicalRegionDao((EmsGeographicalRegionDao)getBean("emsGeographicalRegionDao"));
         RrcStandardHelpUim newUim = newRcmGeographicalRegionHelpUim;
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmGeographicalRegionHelpSvc][execute]: Finished");
     }
 }
