 /*-----------------------------------------------------------------------------------------------------------  
 RcmActivityHelpUim.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 10/06/2008
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;
 import com.rclgroup.dolphin.web.dao.ems.EmsActivityDao;

 import java.util.List;

 public class RcmActivityHelpUim extends RrcStandardHelpUim{

     private EmsActivityDao emsActivityDao;
     
     public void setEmsActivityDao(EmsActivityDao emsActivityDao) {
         this.emsActivityDao = emsActivityDao;
     }

     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsActivityDao.listForHelpScreen(find,search,wild);
        }
         return list;
     }
 }
