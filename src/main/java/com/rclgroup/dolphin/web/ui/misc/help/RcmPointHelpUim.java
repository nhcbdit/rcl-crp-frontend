 /*------------------------------------------------------
 RcmPointHelpUim.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
Author Sopon Dee-udomvongsa 18/07/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;
 import com.rclgroup.dolphin.web.dao.cam.CamPointDao;

 import java.util.List;

 public class RcmPointHelpUim extends RrcStandardHelpUim{
     private String fsc;
     private String controlZone;
     
    private String regionCode;
    private String areaCode;
    private String controlZoneCode;
    private String zoneCode;
    
     private CamPointDao camPointDao;
     
     public void setCamPointDao(CamPointDao camPointDao) {
         this.camPointDao = camPointDao;
     }
     
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             regionCode = this.getRegionCode();
             areaCode = this.getAreaCode();             
             controlZoneCode = this.getControlZoneCode();
             zoneCode = this.getZoneCode();
             list = camPointDao.listForHelpScreen(find, search, wild, regionCode, areaCode, controlZoneCode, zoneCode);
         }else if(type.equals("GET_WITH_FSC")) {
             fsc = this.getFsc();
             list = camPointDao.listForHelpScreenWithFsc(find, search, wild, fsc);
         }else if(type.equals("GET_WITH_CONTROL_ZONE")) {
             controlZone = this.getControlZone();
             list = camPointDao.listForHelpScreenWithControlZone(find, search, wild, controlZone);
         }
         return list;
     }

    public void setFsc(String fsc) {
        this.fsc = fsc;
    }

    public String getFsc() {
        return fsc;
    }

    public void setControlZone(String controlZone) {
        this.controlZone = controlZone;
    }

    public String getControlZone() {
        return controlZone;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setControlZoneCode(String controlZoneCode) {
        this.controlZoneCode = controlZoneCode;
    }

    public String getControlZoneCode() {
        return controlZoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.zoneCode = zoneCode;
    }

    public String getZoneCode() {
        return zoneCode;
    }
}
