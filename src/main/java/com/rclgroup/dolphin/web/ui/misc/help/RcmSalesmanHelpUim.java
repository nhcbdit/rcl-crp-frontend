/*-----------------------------------------------------------------------------------------------------------  
RcmSalesmanHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2009
-------------------------------------------------------------------------------------------------------------
Author Leena Babu 18/08/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 07/10/11  LEE                        Added code for the lookup of salesman under a given manager
02 13/12/11  LEE                        Added code for the lookup of salesmanager
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class RcmSalesmanHelpUim extends RcmStandardHelpOptimizeUim {
    
    public static String SLMN_FOR_MANAGER = "SLMN_FOR_MANAGER";
    public static String SLMN_MANAGER = "SLMN_MANAGER";
    private String salesmanCode; 
    
    public RcmSalesmanHelpUim() {
        super();
        
        // default sort
        this.setSortBy("SALESMAN_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
 
    
    public String findTitleNameShowByType(String type) {
        return "Salesman";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type) || 
            RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type) || 
            SLMN_FOR_MANAGER.equals(type) ||
            SLMN_MANAGER.equals(type)) {
            columnNameShow.put("SALESMAN_CODE", new RcmColumnNameShowMod("Salesman Code|25|left|STRING"));
            columnNameShow.put("SALESMAN_NAME", new RcmColumnNameShowMod("Salesman Name|40|left|STRING"));
            columnNameShow.put("MANAGER", new RcmColumnNameShowMod("Manager|15|left|STRING"));
            columnNameShow.put("COUNTRY", new RcmColumnNameShowMod("Country|40|left|STRING"));
            columnNameShow.put("CURRENCY", new RcmColumnNameShowMod("Currency|12|left|STRING"));
            columnNameShow.put("FSC", new RcmColumnNameShowMod("FSC|40|left|STRING"));
            columnNameShow.put("SALESMAN_STATUS", new RcmColumnNameShowMod("Status|12|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getStatusDesc"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type) || 
            RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type) || 
            SLMN_FOR_MANAGER.equals(type) ||
            SLMN_MANAGER.equals(type)) {
            arrReturnValue = new String[] { "SALESMAN_CODE" };
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            sb.append("select SALESMAN_CODE ");
            sb.append(" ,SALESMAN_NAME ");
            sb.append(" ,MANAGER ");
            sb.append(" ,COUNTRY ");
            sb.append(" ,CURRENCY ");
            sb.append(" ,FSC ");
            sb.append(" ,SALESMAN_STATUS ");
            sb.append("from VR_CAM_SALESMAN ");
        
           
            StringBuffer sbWhere = new StringBuffer("");
            if (RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
                sbWhere.append(" and SALESMAN_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
            }
                        
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere.substring(4));
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        }else if(SLMN_FOR_MANAGER.equals(type)){
            sb.append("select SALESMAN_CODE ");
            sb.append(" ,SALESMAN_NAME ");
            sb.append(" ,MANAGER ");
            sb.append(" ,COUNTRY ");
            sb.append(" ,CURRENCY ");
            sb.append(" ,FSC ");
            sb.append(" ,SALESMAN_STATUS ");
            sb.append("from VR_CAM_SALESMAN ");
            
            
            StringBuffer sbWhere = new StringBuffer("");
           
            sbWhere.append(" and SALESMAN_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
            sbWhere.append(" and MANAGER = '" + getSalesmanCode() + "' ");
                        
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere.substring(4));
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        }else if(SLMN_MANAGER.equals(type)){
            sb.append("select SALESMAN_CODE ");
            sb.append(" ,SALESMAN_NAME ");
            sb.append(" ,MANAGER ");
            sb.append(" ,COUNTRY ");
            sb.append(" ,CURRENCY ");
            sb.append(" ,FSC ");
            sb.append(" ,SALESMAN_STATUS ");
            sb.append("from VR_CAM_SALESMAN ");
            
            
            StringBuffer sbWhere = new StringBuffer("");           
            sbWhere.append(" and SALESMAN_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
            sbWhere.append(" and SALESMAN_CODE IN (SELECT DISTINCT MANAGER FROM VR_CAM_SALESMAN ) ");
                        
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere.substring(4));
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        }
        return sb.toString();
    }
    
    //end: implement function


    public String getSalesmanCode() {
        return salesmanCode;
    }

    public void setSalesmanCode(String salesmanCode) {
        this.salesmanCode = salesmanCode;
    }
    public void manageRequestParameter(HttpServletRequest request) {
       String salesmanCode = RutString.nullToStr(request.getParameter("salesmanCode"));
       this.salesmanCode = salesmanCode;       
    }
}
