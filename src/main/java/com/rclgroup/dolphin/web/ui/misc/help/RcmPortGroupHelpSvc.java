 /*------------------------------------------------------
 RcmPortGroupHelpSvc.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Sopon Dee-udomvongsa 02/09/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.cam.CamPortGroupDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmPortGroupHelpSvc extends RrcStandardHelpSvc {
     public RcmPortGroupHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmPortGroupHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmPortGroupHelpUim";
         String helpScnName = "/RcmPortGroupHelpScn.jsp?service=ui.misc.help.RcmPortGroupHelpSvc";
         
         RrcStandardHelpUim uim = (RcmPortGroupHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmPortGroupHelpUim newPortGroupHelpUim = new RcmPortGroupHelpUim();
         
         newPortGroupHelpUim.setCamPortGroupDao((CamPortGroupDao)getBean("camPortGroupDao"));
         RrcStandardHelpUim newUim = newPortGroupHelpUim;
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmPortGroupHelpSvc][execute]: Finished");
     }
 }