/*-----------------------------------------------------------------------------------------------------------  
RcmEquipmentHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Atul Pandey 03/12/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.model.cam.CamFscMod;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;

import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RcmStorageDepotHelpUim extends RcmStandardHelpOptimizeUim {
    private String depotFscLvl1;
    private String depotFscLvl2;
    private String depotFscLvl3;
    
    public RcmStorageDepotHelpUim() {
        super();
        // default sort
        this.setSortBy("DEPOT_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        HttpSession session = request.getSession(false);
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        if(camFscDao != null){
            CamFscMod camFscMod = camFscDao.findByKeyfscCode(fscCodeOfUser);
            depotFscLvl1 = camFscMod.getLineCode();
            depotFscLvl2 = camFscMod.getRegionCode();
            depotFscLvl3 = camFscMod.getAgentCode();
            isControlFsc = camFscDao.isControlFsc(userBean.getFscLvl1(), userBean.getFscLvl2(), userBean.getFscLvl3(), userBean.getFscCode());
        }
    }
     
    public String findTitleNameShowByType(String type) {
        if(RcmConstant.GET_STR_DEPOT.equals(type)){
            return "DEPOT";
        }else  if(RcmConstant.GET_STR_TERMINAL.equals(type)){
            return "TERMINAL";
        }
        return "DEPOT";
    }
     
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_STR_DEPOT.equals(type)) {
            columnNameShow.put("DEPOT_CODE", new RcmColumnNameShowMod("Depot Code|10|left|STRING"));
            columnNameShow.put("POINT_OR_PORT_CODE", new RcmColumnNameShowMod("Port/Point|10|left|STRING"));
            columnNameShow.put("AGREEMENT_NO", new RcmColumnNameShowMod("Agreement No|10|left|STRING"));
            columnNameShow.put("AGREEMENT_FROM_DATE", new RcmColumnNameShowMod("From Date|10|left|STRING"));
            columnNameShow.put("AGREEMENT_EXPIRY_DATE", new RcmColumnNameShowMod("Expiry Date|10|left|STRING"));
            columnNameShow.put("LAST_BILLED_DATE", new RcmColumnNameShowMod("Last Bill Date|10|left|STRING"));
            columnNameShow.put("CALCULATION_TYPE", new RcmColumnNameShowMod("Calc Type|10|left|STRING"));
            
            columnNameShow.put("TEU_CALC_BASIS", new RcmColumnNameShowMod("Slab Calc Basis|10|left|STRING"));
            columnNameShow.put("VENDOR_CODE", new RcmColumnNameShowMod("Vendor|10|left|STRING"));
            columnNameShow.put("VENDOR_CURRENCY", new RcmColumnNameShowMod("Currency|10|left|STRING"));
        }
        else if (RcmConstant.GET_STR_TERMINAL.equals(type)){
            columnNameShow.put("DEPOT_CODE", new RcmColumnNameShowMod("Terminal Code|10|left|STRING"));
            columnNameShow.put("POINT_OR_PORT_CODE", new RcmColumnNameShowMod("Port/Point|10|left|STRING"));
            columnNameShow.put("AGREEMENT_NO", new RcmColumnNameShowMod("Agreement No|20|left|STRING"));
            columnNameShow.put("AGREEMENT_FROM_DATE", new RcmColumnNameShowMod("From Date|10|left|STRING"));
            columnNameShow.put("AGREEMENT_EXPIRY_DATE", new RcmColumnNameShowMod("Expiry Date|10|left|STRING"));
            columnNameShow.put("LAST_BILLED_DATE", new RcmColumnNameShowMod("Last Bill Date|10|left|STRING"));
            columnNameShow.put("CALCULATION_TYPE", new RcmColumnNameShowMod("Calc Type|15|left|STRING"));
            
            columnNameShow.put("TEU_CALC_BASIS", new RcmColumnNameShowMod("Slab Calc Basis|20|left|STRING"));
            columnNameShow.put("CALCULATED_BY", new RcmColumnNameShowMod("Calculated By|10|left|STRING"));
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|8|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|8|left|STRING"));
            columnNameShow.put("VENDOR_CODE", new RcmColumnNameShowMod("Vendor|10|left|STRING"));
            columnNameShow.put("VENDOR_CURRENCY", new RcmColumnNameShowMod("Currency|10|left|STRING"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_STR_DEPOT.equals(type)) {
            arrReturnValue = new String[] {"DEPOT_CODE","POINT_OR_PORT_CODE", "AGREEMENT_NO","LAST_BILLED_DATE","CALCULATION_TYPE", "CALCULATION_TYPE_CODE", "TEU_CALC_BASIS","VENDOR_CODE","VENDOR_CURRENCY","DEPOT_FSC"};
        }
        else if (RcmConstant.GET_STR_TERMINAL.equals(type)){
            arrReturnValue = new String[] {"DEPOT_CODE","POINT_OR_PORT_CODE", "AGREEMENT_NO","LAST_BILLED_DATE","CALCULATION_TYPE", "CALCULATION_TYPE_CODE", "TEU_CALC_BASIS","VENDOR_CODE","VENDOR_CURRENCY", "SERVICE", "VESSEL", "CALCULATED_BY_CODE","DEPOT_FSC"};
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_STR_DEPOT.equals(type)) {
            sb.append(" SELECT DEPOT_CODE , POINT_OR_PORT_CODE , AGREEMENT_NO , " +
                    "AGREEMENT_FROM_DATE, AGREEMENT_EXPIRY_DATE, " +
                    "LAST_BILLED_DATE , CALCULATION_TYPE , CALCULATION_TYPE_CODE, " +
                    "TEU_CALC_BASIS , VENDOR_CODE , VENDOR_CURRENCY , DEPOT_FSC" +
                    " from vasapps.VR_STR_STORAGE_DEPOT " +
                    "where terminal_depot_flag = 'D' AND depot_type <> 'I' ");
           
            if(depotFscLvl3 != null && !depotFscLvl3.equals("***") && (isControlFsc)){
                sb.append(" and depot_fsc_crflv3 = '" +depotFscLvl3 + "' ");
            }else if(!depotFscLvl3.equals("***") && (!isControlFsc)){
                sb.append(" and depot_fsc = '" +fscCodeOfUser+ "' ");
            }
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append(" [order by :sortBy :sortIn] ");
        }else if (RcmConstant.GET_STR_TERMINAL.equals(type)) {
            sb.append(" SELECT DEPOT_CODE ,SERVICE, VESSEL, CALCULATED_BY, " +
            "CALCULATED_BY_CODE, POINT_OR_PORT_CODE, AGREEMENT_NO,  " +
            "AGREEMENT_FROM_DATE, AGREEMENT_EXPIRY_DATE, LAST_BILLED_DATE , " +
            "CALCULATION_TYPE ,  CALCULATION_TYPE_CODE, TEU_CALC_BASIS , VENDOR_CODE  , " +
            "VENDOR_CURRENCY , DEPOT_FSC" +
            " from vasapps.VR_STR_STORAGE_DEPOT " +
            " where ( terminal_depot_flag = 'T' or depot_type = 'I')  ");
           
            if(depotFscLvl3 != null && !depotFscLvl3.equals("***") && (isControlFsc)){
                sb.append(" and depot_fsc_crflv3 = '" +depotFscLvl3 + "' ");
            }else if(!depotFscLvl3.equals("***") && (!isControlFsc)){
                sb.append(" and depot_fsc = '" +fscCodeOfUser+ "' ");
            }
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append(" [order by :sortBy :sortIn] ");
        }
        System.out.print("===============### "+sb.toString()+" ###==================");
        return sb.toString();
    }
    //end: implement function


    public CamFscDao getCamFscDao() {
        return camFscDao;
    }

   
}
