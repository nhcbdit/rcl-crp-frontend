/*
---------------------------------------------------------------------------------------------------------
PilotInquirySVC.java
---------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
---------------------------------------------------------------------------------------------------------
Author Nuttapol Thanasrisatit 07/05/2013
- Change Log---------------------------------------------------------------------------------------------
## 		DD/MM/YY 		-User- 		-TaskRef-       -ShortDescription
01 		06/09/18 		Nuttapol               		Create this screen
---------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.ui.pilot;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;

import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.dao.rcm.RcmConstantDao;

import com.rclgroup.dolphin.web.util.RutString;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//
public class PilotInquirySvc  extends RrcStandardSvc{

    public PilotInquirySvc() {
        super();
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
    
        try {
        	
        	HttpSession session = request.getSession(false); 
            
        	PilotInquiryUim uim = (PilotInquiryUim) session.getAttribute("PilotInquiryUim");
            
            String strPageAction = RutString.nullToStr(request.getParameter("pageAction"));
            
            if (uim == null || strPageAction.equalsIgnoreCase("new")) {
                
                session.removeAttribute("PilotInquirySvc");
                
                uim = new PilotInquiryUim();
                uim.setRcmConstantDao((RcmConstantDao)getBean("rcmConstantDao"));
                uim.setCamFscDao((CamFscDao)getBean("camFscDao"));
                
                // Manage the attributes of User: line, region, agent, fsc
                RcmUserBean userBean = (RcmUserBean) session.getAttribute("userBean");
                this.manageUserBean(uim, userBean);
                 
                String strPermissionUser = this.getPermissionUserCode(uim);             
                uim.setStrPermissionUser(strPermissionUser);
                
                String strTarget = RcmConstant.PILOT_PAGE_URL + "/pilotInquiryScn.jsp";
     
                request.setAttribute("target", strTarget);
    
            } 
        }  catch (Exception exc) {
            exc.printStackTrace();
        }  
    }
}
