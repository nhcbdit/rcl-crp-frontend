/*-----------------------------------------------------------------------------------------------------------  
RcmAgreementHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsAgreementDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmAgreementHelpSvc extends RrcStandardHelpSvc {
     public RcmAgreementHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmAgreementHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmAgreementHelpUim";
         String helpScnName = "/RcmAgreementHelpScn.jsp?service=ui.misc.help.RcmAgreementHelpSvc";
         
         RrcStandardHelpUim uim = (RcmAgreementHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmAgreementHelpUim newAgreementHelpUim = new RcmAgreementHelpUim();
         
         newAgreementHelpUim.setEmsAgreementDao((EmsAgreementDao)getBean("emsAgreementDao"));
         RrcStandardHelpUim newUim = newAgreementHelpUim;
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmAgreementHelpSvc][execute]: Finished");
     }
 }
