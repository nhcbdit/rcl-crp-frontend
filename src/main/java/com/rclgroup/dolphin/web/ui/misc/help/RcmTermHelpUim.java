 /*------------------------------------------------------
 RcmTermHelpUim.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Sopon Dee-udomvongsa 02/09/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.qtn.QtnTermDao;

import java.util.List;


public class RcmTermHelpUim extends RrcStandardHelpUim{
 
     private QtnTermDao qtnTermDao;
     
     public void setQtnTermDao(QtnTermDao qtnTermDao) {
         this.qtnTermDao = qtnTermDao;
     }
 
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = qtnTermDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = qtnTermDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }
         return list;
     }
 }
