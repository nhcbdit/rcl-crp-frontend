/*-----------------------------------------------------------------------------------------------------------  
RcmAgreementHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ems.EmsAgreementDao;

import java.util.List;


public class RcmAgreementHelpUim extends RrcStandardHelpUim{
     private EmsAgreementDao emsAgreementDao;
     
     public RcmAgreementHelpUim(){
     } 
     
    public void setEmsAgreementDao(EmsAgreementDao emsAgreementDao) {
        this.emsAgreementDao = emsAgreementDao;
    }
    
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsAgreementDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = emsAgreementDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }
         return list;
     }
}
