/*-----------------------------------------------------------------------------------------------------------  
RcmAgentHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 09/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;

import com.rclgroup.dolphin.web.dao.cam.CamAgentDao;

import com.rclgroup.dolphin.web.util.RutString;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmAgentHelpSvc extends RrcStandardHelpSvc {
    public RcmAgentHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmAgentHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmAgentHelpUim";
        String helpScnName = "/RcmAgentHelpScn.jsp?service=ui.misc.help.RcmAgentHelpSvc";
        
        RrcStandardHelpUim uim = (RcmAgentHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmAgentHelpUim newAgentHelpUim = new RcmAgentHelpUim();
        
        newAgentHelpUim.setRegionCode(RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase());
        newAgentHelpUim.setCountryCode(RutString.nullToStr(request.getParameter("txtCountry")).toUpperCase());
        
        newAgentHelpUim.setCamAgentDao((CamAgentDao)getBean("camAgentDao"));
        RrcStandardHelpUim newUim = newAgentHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmAgentHelpSvc][execute]: Finished");
    }
}
