/*-----------------------------------------------------------------------------------------------------------  
RcmProformaRefNoHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 29/04/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


public class RcmProformaRefNoHelpUim extends RcmStandardHelpOptimizeUim {

    private String usrPermission;

    public RcmProformaRefNoHelpUim() {
        super();
        
        // default sort
        this.setSortBy("PROFORMA_REF_NO");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
         
        this.usrPermission = usrPermission;
    }
     
    public String findTitleNameShowByType(String type) {
        return "Proforma/SHS Job Order Ref No";
    }
     
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type)) {
            columnNameShow.put("PROFORMA_REF_NO", new RcmColumnNameShowMod("Proforma Ref No|11|left|STRING"));
            columnNameShow.put("SERVICE", new RcmColumnNameShowMod("Service|7|left|STRING"));
            columnNameShow.put("VESSEL", new RcmColumnNameShowMod("Vessel|7|left|STRING"));
            columnNameShow.put("VOYAGE", new RcmColumnNameShowMod("Voyage|10|left|STRING"));
            columnNameShow.put("DIRECTION", new RcmColumnNameShowMod("Direction|12|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getDirectionDesc"));
            columnNameShow.put("PORT", new RcmColumnNameShowMod("Port|10|left|STRING"));
            columnNameShow.put("TERMINAL", new RcmColumnNameShowMod("Terminal|10|left|STRING"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"PROFORMA_REF_NO"};
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sb.append("select distinct PROFORMA_REF_NO ");
            sb.append(" ,SERVICE ");
            sb.append(" ,VESSEL ");
            sb.append(" ,VOYAGE ");
            sb.append(" ,DIRECTION "); 
            sb.append(" ,PORT ");
            sb.append(" ,TERMINAL ");
            sb.append("from VR_TOS_SHS_PROFORMA_REF_NO ");
            sb.append("[where :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        }
        
        return sb.toString();
    }
    //end: implement function
    
    public String getUsrPermission() {
        return usrPermission;
    }
     
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
