package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ijs.IjsJobOrderDao;


import java.util.List;

public class RcmJobOrderHelpUim extends RrcStandardHelpUim{

    private IjsJobOrderDao ijsJobOrderDao;
    
    public void setIjsJobOrderDao(IjsJobOrderDao ijsJobOrderDao) {
        this.ijsJobOrderDao = ijsJobOrderDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){        
            list = ijsJobOrderDao.listForHelpScreen(find, search, wild);
        }
        return list;
    }
   
}
