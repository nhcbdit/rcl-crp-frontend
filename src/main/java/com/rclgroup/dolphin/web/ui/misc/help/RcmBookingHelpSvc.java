package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.bkg.BkgBookingDao;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmBookingHelpSvc extends RrcStandardHelpSvc{
    public RcmBookingHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmBookingHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "RcmBookingHelpUim";
        String helpScnName = "/RcmBookingHelpScn.jsp?service=ui.misc.help.RcmBookingHelpSvc";
        
        RrcStandardHelpUim uim = (RcmBookingHelpUim)session.getAttribute(helpUiModInstanceName);

        RcmBookingHelpUim newBookingHelpUim = new RcmBookingHelpUim();        
        newBookingHelpUim.setBkgBookingDao((BkgBookingDao)getBean("bkgBookingDao"));
        
        
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        String line = userBean.getFscLvl1();
        String region = userBean.getFscLvl2();
        String agent = userBean.getFscLvl3();
        String fsc = userBean.getFscCode();
        
    //        String fscName = userBean.getFscName();
        //            String line = "E";
        //            String region = "*";
        //            String agent = "***";
        //            String fsc = "DMM";

        //            String line = "R";
        //            String region = "*";
        //            String agent = "***";
        //            String fsc = "***";

        //
        //            CONTROL_FSC="N";
        //            String line = "R";
        //            String region = "N";
        //            String agent = "TYO";
        //            String fsc = "NGO";
        //
        //            CONTROL_FSC="Y"
        //            String line = "R";
        //            String region = "S";
        //            String agent = "CEB";
        //            String fsc = "CEB";
        newBookingHelpUim.setLineCodeOfUser(line);
        newBookingHelpUim.setCamFscDao((CamFscDao)getBean("camFscDao"));
        if(region.equals("*")){ //check whether user is in line level
            newBookingHelpUim.setLevel(RrcStandardUim.LEVEL_LINE,line,"","","");
            newBookingHelpUim.setRegionCodeOfUser("");
            newBookingHelpUim.setAgentCodeOfUser("");
            newBookingHelpUim.setFscCodeOfUser("");
        }else if(agent.equals("***")){ //check whether user is in region level
            newBookingHelpUim.setLevel(RrcStandardUim.LEVEL_REGION,line,region,"","");
            newBookingHelpUim.setRegionCodeOfUser(region);
            newBookingHelpUim.setAgentCodeOfUser("");
            newBookingHelpUim.setFscCodeOfUser("");
        }else{ //check whether user is in agent level
            newBookingHelpUim.setLevel(RrcStandardUim.LEVEL_AGENT,line,region,agent,"");
            newBookingHelpUim.setRegionCodeOfUser(region);
            newBookingHelpUim.setAgentCodeOfUser(agent);
            newBookingHelpUim.setFscCodeOfUser(fsc);
        }
    //        uim.setFscName(fscName);
        newBookingHelpUim.setIsControlFsc(line,region,agent,fsc); 
        
        RrcStandardHelpUim newUim = newBookingHelpUim;
    
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmBookingHelpSvc][execute]: Finished");
    }
}
