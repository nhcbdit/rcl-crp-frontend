package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.dex.DexBlDao;

import java.util.List;

public class RcmBlHelpUim extends RrcStandardHelpUim{
    public RcmBlHelpUim() {
    }
    private DexBlDao dexBlDao;
    
    public void setDexBlDao(DexBlDao dexBlDao) {
        this.dexBlDao = dexBlDao;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        String line = this.getLineCodeOfUser();
        String trade = this.getRegionCodeOfUser();
        String agent = this.getAgentCodeOfUser();
        String fsc = this.getFscCodeOfUser();
        //RrcStandardUim.GET_GENERAL
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS)){
            System.out.println("[RcmBlHelpUim][Type]: GET_WITH_USER_LEVEL_ACTIVE_STATUS");
            list = dexBlDao.listForHelpScreenForBankGuarantee(find,search,wild,line,trade,agent,fsc);
       }else if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_BL_FOR_DG_MANIFEST)){
            System.out.println("[RcmBlHelpUim][Type]: GET_BL_FOR_DG_MANIFEST");
            list = dexBlDao.listForHelpScreenForDangerousCargoManifest(find,search,wild,line,trade,agent,fsc);       
       }else if (type.equalsIgnoreCase(RrcStandardUim.GET_REEFER_GENERAL)){
            System.out.println("[RcmBlHelpUim][Type]: GET_REEFER_GENERAL");
            list = dexBlDao.listForHelpScreenForReeferManifest(find,search,wild,line,trade,agent,fsc);
       }else if (type.equalsIgnoreCase(RrcStandardUim.GET_REEFER_IMP_GENERAL)){
             System.out.println("[RcmBlHelpUim][Type]: GET_REEFER_IMP_GENERAL");
             list = dexBlDao.listForHelpScreenForReeferManifestImport(find,search,wild,line,trade,agent,fsc);
        }else if (type.equalsIgnoreCase(RrcStandardUim.GET_HOUSEBL_GENERAL) || type.equalsIgnoreCase(RrcStandardUim.GET_BL_GENERAL)){
             System.out.println("[RcmBlHelpUim][Type]: GET_BL_GENERAL");
             int size = dexBlDao.countListForNewHelpScreenByFsc(find,search,wild,line,trade,agent,fsc);
             setNoOfResultRecords(size);//All records size            
             list = dexBlDao.listForNewHelpScreenByFsc(rutPage.calculateRowAt(),rutPage.calculateRowTo(),find,search,wild,line,trade,agent,fsc);  
        }else if(type.equalsIgnoreCase(RrcStandardUim.GET_WITH_FSC)){
            System.out.println("[RcmBlHelpUim][Type]: GET_BL_WITH_FSC");
            list = dexBlDao.listForHelpScreenWithFsc(find,search,wild,line,trade,agent,fsc);
        }else if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_HELP_V02_GENERAL)){
            int size = dexBlDao.countListForNewHelpScreen(find,search,wild);
            setNoOfResultRecords(size);//All records size            
            list = dexBlDao.listForNewHelpScreen(rutPage.calculateRowAt(),rutPage.calculateRowTo(),find,search,wild);                         
        }else if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_HELP_V02_WITH_FSC)){
            int size = dexBlDao.countListForNewHelpScreenByFsc(find,search,wild,line,trade,agent,fsc);
            setNoOfResultRecords(size);//All records size            
            list = dexBlDao.listForNewHelpScreenByFsc(rutPage.calculateRowAt(),rutPage.calculateRowTo(),find,search,wild,line,trade,agent,fsc);                         
        }
        return list;
        
        
    }
}
