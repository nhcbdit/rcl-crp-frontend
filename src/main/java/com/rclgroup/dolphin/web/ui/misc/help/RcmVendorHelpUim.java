package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.vms.VmsVendorDao;

import java.util.List;

public class RcmVendorHelpUim extends RrcStandardHelpUim{

    private VmsVendorDao vmsVendorDao;
    
    public void setVmsVendorDao(VmsVendorDao vmsVendorDao) {
        this.vmsVendorDao = vmsVendorDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){        
            list = vmsVendorDao.listForHelpScreen(find, search, wild);
        }
        return list;
    }
    
}
