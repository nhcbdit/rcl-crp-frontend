package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.dao.dex.DexBlDao;

import com.rclgroup.dolphin.web.dao.far.FarInvoiceDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmInvoiceHelpSvc extends RrcStandardHelpSvc{
    public RcmInvoiceHelpSvc() {
    }
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmInvoiceHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmInvoiceHelpUim";
        String helpScnName = "/RcmInvoiceHelpScn.jsp?service=ui.misc.help.RcmInvoiceHelpSvc";
        
        RrcStandardHelpUim uim = (RcmInvoiceHelpUim)session.getAttribute(helpUiModInstanceName);

        RcmInvoiceHelpUim newInvoiceHelpUim = new RcmInvoiceHelpUim();        
        newInvoiceHelpUim.setFarInvoiceDao((FarInvoiceDao)getBean("farInvoiceDao"));
        
        
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        String line = userBean.getFscLvl1();
        String region = userBean.getFscLvl2();
        String agent = userBean.getFscLvl3();
        String fsc = userBean.getFscCode();
        
//        String fscName = userBean.getFscName();
        //            String line = "E";
        //            String region = "*";
        //            String agent = "***";
        //            String fsc = "DMM";

        //            String line = "R";
        //            String region = "*";
        //            String agent = "***";
        //            String fsc = "***";

        //
        //            CONTROL_FSC="N";
        //            String line = "R";
        //            String region = "N";
        //            String agent = "TYO";
        //            String fsc = "NGO";
        //
        //            CONTROL_FSC="Y"
        //            String line = "R";
        //            String region = "S";
        //            String agent = "CEB";
        //            String fsc = "CEB";
        newInvoiceHelpUim.setLineCodeOfUser(line);
        newInvoiceHelpUim.setCamFscDao((CamFscDao)getBean("camFscDao"));
        newInvoiceHelpUim.setIsControlFsc(line,region,agent,fsc);        
        if(region.equals("*")){ //check whether user is in line level
            newInvoiceHelpUim.setLevel(RrcStandardUim.LEVEL_LINE,line,"","","");
            newInvoiceHelpUim.setRegionCodeOfUser("");
            newInvoiceHelpUim.setAgentCodeOfUser("");
            newInvoiceHelpUim.setFscCodeOfUser("");
        }else if(agent.equals("***")){ //check whether user is in region level
            newInvoiceHelpUim.setLevel(RrcStandardUim.LEVEL_REGION,line,region,"","");
            newInvoiceHelpUim.setRegionCodeOfUser(region);
            newInvoiceHelpUim.setAgentCodeOfUser("");
            newInvoiceHelpUim.setFscCodeOfUser("");
        }else if(newInvoiceHelpUim.isControlFsc()){ //check whether user is in agent level
            newInvoiceHelpUim.setLevel(RrcStandardUim.LEVEL_AGENT,line,region,agent,"");
            //uim.setRegionCodeOfUser(region);
            //uim.setAgentCodeOfUser(agent);
            //uim.setFscCodeOfUser(fsc);
        }else{
            uim.setLevel(RrcStandardUim.LEVEL_AGENT,line,region,agent,fsc);
        }
        
//        uim.setFscName(fscName);
//        newInvoiceHelpUim.setIsControlFsc(line,region,agent,fsc); 
        
        RrcStandardHelpUim newUim = newInvoiceHelpUim;
    
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmInvoiceHelpSvc][execute]: Finished");
    }
    
}
