/*-----------------------------------------------------------------------------------------------------------  
RcmLessorHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;

import com.rclgroup.dolphin.web.dao.ems.EmsLessorDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmLessorHelpSvc extends RrcStandardHelpSvc {
    public RcmLessorHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmLessorHelpSvc][execute]: Started");
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmLessorHelpUim";
        String helpScnName = "/RcmLessorHelpScn.jsp?service=ui.misc.help.RcmLessorHelpSvc";
        
        RrcStandardHelpUim uim = (RcmLessorHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmLessorHelpUim newLessorHelpUim = new RcmLessorHelpUim();
        
        newLessorHelpUim.setEmsLessorDao((EmsLessorDao)getBean("emsLessorDao"));
        RrcStandardHelpUim newUim = newLessorHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmLessorHelpSvc][execute]: Finished");
    }
}
