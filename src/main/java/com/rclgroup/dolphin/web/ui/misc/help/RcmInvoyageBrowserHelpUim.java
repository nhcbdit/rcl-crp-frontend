/*-----------------------------------------------------------------------------------------------------------  
RcmInvoyageBrowserHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 14/10/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.dao.rcm.RcmAttributeSessionDao;
import com.rclgroup.dolphin.web.model.rcm.RcmAttributeSessionMod;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class RcmInvoyageBrowserHelpUim extends RcmStandardHelpOptimizeUim {
    public static final String MODULE_CODE = "RCM_INVOY";
    
    private String usrPermission;
    private String invoyagePort;
    private String invoyageVessel;
    private String invoyageVoyage;
    private String invoyageEta;
    private String sessionId;
    private String username;
    
    private String optSelect;
    private HashMap hChkValue;
    
    private RcmAttributeSessionDao rcmAttributeSessionDao;
    
    public RcmInvoyageBrowserHelpUim() {
        super();
        
        // default sort
        this.setSortBy("VVARDT");
        this.setSortIn(RcmConstant.SORT_DESCENDING);
    }
    
    //begin: implement function
    public void manageDao(RrcStandardHelpOptimizeSvc bean) {
        //To do get dao object
        if (bean != null) {
            this.setRcmAttributeSessionDao((RcmAttributeSessionDao) bean.getBean("rcmAttributeSessionDao"));
        }
    }
    
    public void doBeforeOptional(RrcStandardHelpOptimizeSvc bean, HttpServletRequest request, HttpSession session) {
        //To do before [optional]
        if (bean != null && session != null) {
            // clear data
            this.setHChkValue(new HashMap());
            this.setOptSelect("");
            
            // set username
            RcmUserBean userBean = (RcmUserBean) session.getAttribute("userBean");
            this.setUsername(userBean.getPrsnLogId()); 
            
            // clear table RCM_ATTRIBUTE_SESSION [by session id]
            RcmAttributeSessionMod rcmAttributeSessionMod = new RcmAttributeSessionMod();
            rcmAttributeSessionMod.setSessionId(sessionId);
            rcmAttributeSessionMod.setModuleCode(MODULE_CODE);
            
            rcmAttributeSessionDao.clearAttributeSession(rcmAttributeSessionMod);
        }
    }
    
    public void managePageActionOptional(String pageAction, RrcStandardHelpOptimizeSvc bean, HttpServletRequest request, HttpSession session) {
        //To da page action [optional]
        if (bean != null) {
            if (pageAction.equalsIgnoreCase("selectInvoyage")) { //if 2
                this.setOptSelect("");
                String[] arValue = request.getParameterValues("chkValue");
                String[] arValueUncheck = request.getParameterValues("chkValueUncheck");
                
                HashMap hBean = RutString.mergeHashMap(hChkValue, arValue, arValueUncheck);
                if (hBean != null && hBean.size() > 0) { //if 1
                    
                    // insert table RCM_ATTRIBUTE_SESSION [by session id]
                    String arBean[] = (String[]) hBean.keySet().toArray(new String[0]);
                    String tmp[] = null;
                    String keyCode = null;
                    RcmAttributeSessionMod rcmAttributeSessionMod = null;
                    for (int i=0;i<arBean.length;i++) {
                        tmp = arBean[i].split("\\|");
                        keyCode = (!RutString.isEmptyString(tmp[7])) ? tmp[7] : tmp[8];
                        
                        rcmAttributeSessionMod = new RcmAttributeSessionMod();
                        rcmAttributeSessionMod.setSessionId(sessionId);
                        rcmAttributeSessionMod.setModuleCode(MODULE_CODE);
                        rcmAttributeSessionMod.setRecordAddUser(username);
                        rcmAttributeSessionMod.setRecordChangeUser(username);
                        rcmAttributeSessionMod.setKeyValue(keyCode);
                        rcmAttributeSessionMod.setValue01(tmp[0]); // Service
                        rcmAttributeSessionMod.setValue02(tmp[1]); // Vessel
                        rcmAttributeSessionMod.setValue03(tmp[2]); // Voyage
                        rcmAttributeSessionMod.setValue04(tmp[3]); // Direction
                        rcmAttributeSessionMod.setValue05(tmp[4]); // Port
                        rcmAttributeSessionMod.setValue06(tmp[5]); // Terminal
                        rcmAttributeSessionMod.setValue07(tmp[6]); // Port Seq
                        rcmAttributeSessionMod.setValue08(tmp[7]); // Exp. InVoyage
                        rcmAttributeSessionMod.setValue09(tmp[8]); // InVoyage
                        rcmAttributeSessionMod.setValue10("");
                        
                        rcmAttributeSessionDao.insertAttributeSession(rcmAttributeSessionMod);
                    }
                    
                    // set message
                    session.setAttribute("msg", bean.getErrorMessage("RCM.SAVE_SUCCESSFULLY"));
                    
                } //end if 1
                
            } else if (pageAction.equalsIgnoreCase("checkAllInvoyage")) {
                String optSelect = RutString.nullToStr(request.getParameter("optSelect")).toUpperCase().trim();
                this.setOptSelect(optSelect);
                
                List arBean = this.queryDataByParameter();
                if (arBean != null && arBean.size() > 0) {
                    HashMap mod = null;
                    String keyCode = null;
                    RcmAttributeSessionMod rcmAttributeSessionMod = null;
                    for (int i=0;i<arBean.size();i++) {
                        mod = (HashMap) arBean.get(i);
                        keyCode = (!RutString.isEmptyString((String) mod.get("INVOYAGENO"))) ? (String) mod.get("INVOYAGENO") : (String) mod.get("IN_VOYAGE");
                        
                        rcmAttributeSessionMod = new RcmAttributeSessionMod();
                        rcmAttributeSessionMod.setSessionId(sessionId);
                        rcmAttributeSessionMod.setModuleCode(MODULE_CODE);
                        rcmAttributeSessionMod.setRecordAddUser(username);
                        rcmAttributeSessionMod.setRecordChangeUser(username);
                        rcmAttributeSessionMod.setKeyValue(keyCode);
                        rcmAttributeSessionMod.setValue01((String) mod.get("VVSRVC")); // Service
                        rcmAttributeSessionMod.setValue02((String) mod.get("VVVESS")); // Vessel
                        rcmAttributeSessionMod.setValue03((String) mod.get("VVVOYN")); // Voyage
                        rcmAttributeSessionMod.setValue04((String) mod.get("VVPDIR")); // Direction
                        rcmAttributeSessionMod.setValue05((String) mod.get("VVPCAL")); // Port
                        rcmAttributeSessionMod.setValue06((String) mod.get("VVTRM1")); // Terminal
                        rcmAttributeSessionMod.setValue07((String) mod.get("VVPCSQ")); // Port Seq
                        rcmAttributeSessionMod.setValue08((String) mod.get("INVOYAGENO")); // Exp. InVoyage
                        rcmAttributeSessionMod.setValue09((String) mod.get("IN_VOYAGE")); // InVoyage
                        rcmAttributeSessionMod.setValue10("");
                        
                        rcmAttributeSessionDao.insertAttributeSession(rcmAttributeSessionMod);                        
                    }
                    
                    // set message
                    session.setAttribute("msg", bean.getErrorMessage("RCM.SAVE_SUCCESSFULLY"));
                }
            
            } //end if 2
        }
    }
    
    public void manageLinkPageOptional(RrcStandardHelpOptimizeSvc bean, HttpServletRequest request) {
        //To da link page [optional]
        if (bean != null) {
            String[] arValue = request.getParameterValues("chkValue");
            String[] arValueUncheck = request.getParameterValues("chkValueUncheck");
            
            hChkValue = RutString.mergeHashMap(hChkValue, arValue, arValueUncheck);
        }
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
        String invoyagePort = RutString.nullToStr(request.getParameter("invoyagePort")).toUpperCase().trim();
        String invoyageVessel = RutString.nullToStr(request.getParameter("invoyageVessel")).toUpperCase().trim();
        String invoyageVoyage = RutString.nullToStr(request.getParameter("invoyageVoyage")).toUpperCase().trim();
        String invoyageEta = RutString.nullToStr(request.getParameter("invoyageEta")).toUpperCase().trim();
        String sessionId = RutString.nullToStr(request.getParameter("sessionId")).toUpperCase().trim();
        
        //prepare date
        invoyageEta = RutDate.dateToStr(invoyageEta);
        
        this.usrPermission = usrPermission;
        this.invoyagePort = invoyagePort;
        this.invoyageVessel = invoyageVessel;
        this.invoyageVoyage = invoyageVoyage;
        this.invoyageEta = invoyageEta;
        this.sessionId = sessionId;
    }
    
    public String findTitleNameShowByType(String type) {
        return "InVoyage Browser";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        columnNameShow.put("VVPCAL", new RcmColumnNameShowMod("Port|8|left|STRING"));
        columnNameShow.put("VVTRM1", new RcmColumnNameShowMod("Terminal|8|left|STRING"));
        columnNameShow.put("IN_VOYAGE", new RcmColumnNameShowMod("In Voyage|8|left|STRING"));
        columnNameShow.put("INVOYAGENO", new RcmColumnNameShowMod("Exception <br>In Voyage|10|left|STRING"));
        columnNameShow.put("VVPCSQ", new RcmColumnNameShowMod("Port Seq|9|left|STRING"));
        columnNameShow.put("VVSRVC", new RcmColumnNameShowMod("Service|7|left|STRING"));
        columnNameShow.put("VVVESS", new RcmColumnNameShowMod("Vessel|7|left|STRING"));
        columnNameShow.put("VVVOYN", new RcmColumnNameShowMod("Voyage|8|left|STRING"));
        columnNameShow.put("VVPDIR", new RcmColumnNameShowMod("Direction|10|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getDirectionDesc"));
        columnNameShow.put("VVARDT", new RcmColumnNameShowMod("ETA|10|left|STRING"));
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"VVSRVC", "VVVESS", "VVVOYN", "VVPDIR", "VVPCAL", "VVTRM1", "VVPCSQ", "INVOYAGENO", "IN_VOYAGE"}; // Service|Vessel|Voyage|Direction|Port|Terminal|Port Seq|Exp. InVoyage|InVoyage
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (!RutString.isEmptyString(invoyagePort) && !RutString.isEmptyString(invoyageVoyage)) {
            if (RcmConstant.GET_GENERAL.equals(type)) {
                sb.append("select i63.VVPCAL ");
                sb.append("    ,i63.VVTRM1 ");
                sb.append("    ,i63.IN_VOYAGE ");
                sb.append("    ,i63.INVOYAGENO ");
                sb.append("    ,i63.VVPCSQ ");
                sb.append("    ,i63.VVSRVC ");
                sb.append("    ,i63.VVVESS ");
                sb.append("    ,i63.VVVOYN ");
                sb.append("    ,i63.VVPDIR ");
                sb.append("    ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(i63.VVARDT) as VVARDT ");
                sb.append("    ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(i63.VVDPDT) as VVDPDT ");
                sb.append("    ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(i63.VVAPDT) as VVAPDT ");
                sb.append("    ,PCR_RUT_UTILITY.FR_RCM_GET_DATE_FORMAT_SHOW(i63.VVSLDT) as VVSLDT ");
                sb.append("    ,i63.PREV_NEXT_VVVOYN ");
                sb.append("from ITP063 i63 ");
                sb.append("where i63.VVVERS = 99 ");
                sb.append("    and i63.VVLDDS <> 'D' ");
                sb.append("    and i63.OMMISSION_FLAG is null ");
                sb.append("    and exists ( select 1 from ITP063 where VVVERS = 99 and VVLDDS <> 'D' and VVPCAL = '"+invoyagePort+"' and nvl(INVOYAGENO, IN_VOYAGE) = '"+invoyageVoyage+"' and VVVESS = i63.VVVESS ) ");
                sb.append("    and i63.VVARDT < ( select min(VVARDT) from ITP063 where VVVERS = 99 and VVLDDS <> 'D' and VVVESS = i63.VVVESS and VVPCAL = '"+invoyagePort+"' and nvl(INVOYAGENO, IN_VOYAGE) = '"+invoyageVoyage+"' and VVARDT is not null ) ");
                sb.append("    and i63.VVARDT > (  ");
                sb.append("        select max(VVARDT) ");
                sb.append("        from ITP063 ");
                sb.append("        where VVVERS = 99 ");
                sb.append("            and VVLDDS <> 'D' ");
                sb.append("            and VVVESS = i63.VVVESS ");
                sb.append("            and VVPCAL = '"+invoyagePort+"' ");
                sb.append("            and VVARDT < ( select min(VVARDT) from ITP063 where VVVERS = 99 and VVLDDS <> 'D' and VVVESS = i63.VVVESS and VVPCAL = '"+invoyagePort+"' and nvl(INVOYAGENO, IN_VOYAGE) = '"+invoyageVoyage+"' and VVARDT is not null ) ) ");
                
                StringBuffer sbWhere = new StringBuffer("");
                if (invoyageVessel != null && !"".equals(invoyageVessel)) {
                    sbWhere.append(" and i63.VVVESS = '"+invoyageVessel+"' ");
                }
                if (invoyageEta != null && !"".equals(invoyageEta)) {
                    sbWhere.append(" and i63.VVARDT <= "+invoyageEta+" ");
                }
                /*if (usrPermission != null && !"".equals(usrPermission)) {
                    sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+usrPermission+"', i63.VVPCAL) = '"+RcmConstant.FLAG_YES+"' ");
                }*/
                
                if (sbWhere != null && sbWhere.length() > 0) {
                    sb.append(" and "+sbWhere.substring(4));
                    sb.append(" [and :columnName :conditionWild :columnFind] ");
                } else {
                    sb.append(" [and :columnName :conditionWild :columnFind] ");
                }
                sb.append("[order by :sortBy :sortIn] ");
            }    
        }
        return sb.toString();
    }
    //end: implement function
    
    public boolean isExistValue(HashMap hBean, String keyCode) {
        boolean isExist = false;
        if (hBean != null && hBean.size() > 0) {
            isExist = hBean.containsKey(keyCode);
        }
        return isExist;
    }

    public void setRcmAttributeSessionDao(RcmAttributeSessionDao rcmAttributeSessionDao) {
        this.rcmAttributeSessionDao = rcmAttributeSessionDao;
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
    
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
    
    public String getInvoyagePort() {
        return invoyagePort;
    }

    public void setInvoyagePort(String invoyagePort) {
        this.invoyagePort = invoyagePort;
    }
    
    public String getInvoyageVessel() {
        return invoyageVessel;
    }

    public void setInvoyageVessel(String invoyageVessel) {
        this.invoyageVessel = invoyageVessel;
    }

    public String getInvoyageVoyage() {
        return invoyageVoyage;
    }

    public void setInvoyageVoyage(String invoyageVoyage) {
        this.invoyageVoyage = invoyageVoyage;
    }
    
    public String getInvoyageEta() {
        return invoyageEta;
    }

    public void setInvoyageEta(String invoyageEta) {
        this.invoyageEta = invoyageEta;
    }
    
    public HashMap getHChkValue() {
        return hChkValue;
    }

    public void setHChkValue(HashMap hChkValue) {
        this.hChkValue = hChkValue;
    }
    
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getOptSelect() {
        return optSelect;
    }

    public void setOptSelect(String optSelect) {
        this.optSelect = optSelect;
    }
    
}
