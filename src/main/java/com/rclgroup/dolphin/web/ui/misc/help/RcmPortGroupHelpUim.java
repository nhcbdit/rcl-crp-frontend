 /*------------------------------------------------------
 RcmPortGroupHelpUim.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Sopon Dee-udomvongsa 02/09/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
    25/03/09 PIE              Add type: GET_SUPPORTED_PORT_GROUP
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.cam.CamPortGroupDao;

import java.util.List;


public class RcmPortGroupHelpUim extends RrcStandardHelpUim{
    
     private CamPortGroupDao camPortGroupDao;
     
     public void setCamPortGroupDao(CamPortGroupDao camPortGroupDao) {
         this.camPortGroupDao = camPortGroupDao;
     }
 
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = camPortGroupDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = camPortGroupDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }else if(type.equals(RrcStandardUim.GET_SUPPORTED_PORT_GROUP)) {
             final String ACTIVE_STATUS = "A";
             list = camPortGroupDao.listSupportedPortGroupForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }else if(type.equals(RrcStandardUim.GET_SUPPORTED_PORT_COUNTRY_CODE)) {
             final String ACTIVE_STATUS = "A";
             list = camPortGroupDao.listSupportedPortGroupForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }
         return list;
     }
 }
