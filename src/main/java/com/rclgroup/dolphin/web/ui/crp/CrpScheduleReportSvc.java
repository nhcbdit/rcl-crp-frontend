package com.rclgroup.dolphin.web.ui.crp;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rclgroup.dolphin.web.auth.IAuth;
import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;

public class CrpScheduleReportSvc extends RrcStandardSvc{
	public static final String TARGET_PAGE = "/CrpScheduleReportScn.jsp";

	private static IAuth authInstance;

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context)
			throws Exception {
		String strTarget = RcmConstant.CRP_PAGE_URL + TARGET_PAGE;
		System.out.println("CAll CrpScheduleReportSvc.");
		
		request.setAttribute("target", strTarget);
	}
}
