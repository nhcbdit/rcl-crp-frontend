/*-----------------------------------------------------------------------------------------------------------  
RcmControlZoneHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ems.EmsControlZoneDao;
import com.rclgroup.dolphin.web.model.ems.EmsControlZoneMod;

 import java.util.List;

 public class RcmControlZoneHelpUim extends RrcStandardHelpUim{
     private String regionCode;
     private String areaCode;
     private EmsControlZoneDao emsControlZoneDao;
     
     public RcmControlZoneHelpUim(){
         regionCode = "";
         areaCode = "";
     }
     
     public void setEmsControlZoneDao(EmsControlZoneDao emsControlZoneDao) {
         this.emsControlZoneDao = emsControlZoneDao;
     }
     
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsControlZoneDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             regionCode = this.getRegionCode();
             areaCode = this.getAreaCode();
             list = emsControlZoneDao.listForHelpScreen(find, search, wild, regionCode, areaCode, ACTIVE_STATUS);
         }
         return list;
     }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return areaCode;
    }
}
