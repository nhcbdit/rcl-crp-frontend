package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.cam.CamOperatorDao;

import java.util.List;

public class RcmOperatorHelpUim extends RrcStandardHelpUim{
    private CamOperatorDao camOperatorDao;
    public RcmOperatorHelpUim() {
    }
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        String status = "A";
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
            list = camOperatorDao.listForHelpScreen(find,search,wild,status);
        }
        return list;
    }

    public void setCamOperatorDao(CamOperatorDao camOperatorDao) {
        this.camOperatorDao = camOperatorDao;
    }
}
