/*-----------------------------------------------------------------------------------------------------------  
RcmBsaModelHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 23/04/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


public class RcmBsaModelHelpUim extends RcmStandardHelpOptimizeUim {
    
    public RcmBsaModelHelpUim() {
        super();
        
        // default sort
        this.setSortBy("MODEL_NAME");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
    }
    
    public String findTitleNameShowByType(String type) {
        return "BSA Model";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        columnNameShow.put("MODEL_NAME", new RcmColumnNameShowMod("Model Name|20|left|STRING"));
        columnNameShow.put("SIMULATION_FLAG", new RcmColumnNameShowMod("Simulation Flag|20|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getSimulationDesc"));
        columnNameShow.put("RECORD_STATUS", new RcmColumnNameShowMod("Status|15|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getStatusDesc"));
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            arrReturnValue = new String[] {"BSA_MODEL_ID", "MODEL_NAME"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            sb.append("select distinct BSA_MODEL_ID ");
            sb.append(" ,MODEL_NAME ");
            sb.append(" ,SIMULATION_FLAG ");   
            sb.append(" ,VALID_FROM ");
            sb.append(" ,VALID_TO ");
            sb.append(" ,RECORD_STATUS ");            
            sb.append("from VR_BSA_BASE_ALLOCATION_MODEL ");
            sb.append("where RECORD_STATUS = 'A' ");
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        }
        return sb.toString();
    }
    //end: implement function

}
