 /*------------------------------------------------------
 RcmQtnExportToExcelHelpUim.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Kitti Pongsirisakun 01/12/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.qtn.QtnPortSurchargeDao;

import com.rclgroup.dolphin.web.dao.qtn.QtnExportToExcelDao;

import java.util.List;


public class RcmQtnExportToExcelHelpUim extends RrcStandardHelpUim{
 
     private QtnExportToExcelDao qtnExportToExcelDao;
     
     public void setQtnExportToExcelDao(QtnExportToExcelDao qtnExportToExcelDao) {
         this.qtnExportToExcelDao = qtnExportToExcelDao;
     }
    
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         String line = this.getLineCodeOfUser();
         String trade = this.getRegionCodeOfUser();
         String agent = this.getAgentCodeOfUser();
         String fsc = this.getFscCodeOfUser();
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = qtnExportToExcelDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = qtnExportToExcelDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS,line,trade,agent,fsc);
         }else if(type.equals(RrcStandardUim.GET_HELP_V02__WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             int size = qtnExportToExcelDao.countListForNewHelpScreenByFsc(find,search,wild, ACTIVE_STATUS,line,trade,agent,fsc);
             setNoOfResultRecords(size);//All records size     
             list = qtnExportToExcelDao.listForNewHelpScreenByFsc(rutPage.calculateRowAt(),rutPage.calculateRowTo(),find,search,wild,ACTIVE_STATUS,line,trade,agent,fsc);      
             
         }
         return list;
     }
 }
