/*-----------------------------------------------------------------------------------------------------------  
RcmStrProformaHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Leena Babu 03/05/2013
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.model.cam.CamFscMod;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RcmStrProformaHelpUim extends RcmStandardHelpOptimizeUim {
    private String depotFscLvl1;
    private String depotFscLvl2;
    private String depotFscLvl3;
    
    public RcmStrProformaHelpUim() {
        super();
        // default sort
        this.setSortBy("PROFORMA_NO");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        HttpSession session = request.getSession(false);
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        if(camFscDao != null){
            CamFscMod camFscMod = camFscDao.findByKeyfscCode(fscCodeOfUser);
            depotFscLvl1 = camFscMod.getLineCode();
            depotFscLvl2 = camFscMod.getRegionCode();
            depotFscLvl3 = camFscMod.getAgentCode();
            isControlFsc = camFscDao.isControlFsc(userBean.getFscLvl1(), userBean.getFscLvl2(), userBean.getFscLvl3(), userBean.getFscCode());
        }
    }
     
    public String findTitleNameShowByType(String type) {
        if(RcmConstant.GET_STR_DEPOT.equals(type) || RcmConstant.GET_STR_TERMINAL.equals(type)){
            return "STR PROFORMA";
        }
        return "STR PROFORMA";
    }
     
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_STR_DEPOT.equals(type)) {
            columnNameShow.put("PROFORMA_NO", new RcmColumnNameShowMod("Proforma No|15|left|STRING"));
            columnNameShow.put("DEPOT_CODE", new RcmColumnNameShowMod("Depot|10|left|STRING"));
            columnNameShow.put("AGREEMENT_NO", new RcmColumnNameShowMod("Agreement No|10|left|STRING"));
            columnNameShow.put("CALCULATE_BY", new RcmColumnNameShowMod("Calculate By|10|left|STRING"));
            columnNameShow.put("EMPTY_LADEN", new RcmColumnNameShowMod("Empty/Laden|10|left|STRING"));
        }else if (RcmConstant.GET_STR_TERMINAL.equals(type)){
            columnNameShow.put("PROFORMA_NO", new RcmColumnNameShowMod("Proforma No|15|left|STRING"));
            columnNameShow.put("DEPOT_CODE", new RcmColumnNameShowMod("Terminal|10|left|STRING"));
            columnNameShow.put("AGREEMENT_NO", new RcmColumnNameShowMod("Agreement No|10|left|STRING"));
            columnNameShow.put("CALCULATE_BY", new RcmColumnNameShowMod("Calculate By|10|left|STRING"));
            columnNameShow.put("EMPTY_LADEN", new RcmColumnNameShowMod("Empty/Laden|10|left|STRING"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_STR_DEPOT.equals(type) || RcmConstant.GET_STR_TERMINAL.equals(type)) {
            arrReturnValue = new String[] {"PROFORMA_NO", "DEPOT_CODE", "AGREEMENT_NO", "CALCULATE_BY", "EMPTY_LADEN" };
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_STR_DEPOT.equals(type)) {
            sb.append(" select PROFORMA_NO, DEPOT_CODE , AGREEMENT_NO, CALCULATE_BY, EMPTY_LADEN " +
                       "from vasapps.VR_STR_PROFORMA " +
                       "where terminal_depot_flag = 'D' AND depot_type <> 'I' ");
           
            if(depotFscLvl3 != null && !depotFscLvl3.equals("***") && (isControlFsc)){
                sb.append(" and depot_fsc_crflv3 = '" +depotFscLvl3 + "' ");
            }else if(!depotFscLvl3.equals("***") && (!isControlFsc)){
                sb.append(" and depot_fsc = '" +fscCodeOfUser+ "' ");
            }
            
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append(" [order by :sortBy :sortIn] ");
        }else if (RcmConstant.GET_STR_TERMINAL.equals(type)) {
            sb.append(" select PROFORMA_NO, DEPOT_CODE , AGREEMENT_NO, CALCULATE_BY, EMPTY_LADEN " +
                       "from vasapps.VR_STR_PROFORMA " +
                       "where (terminal_depot_flag = 'T' or (terminal_depot_flag = 'D' AND depot_type = 'I') ) ");
           
            if(depotFscLvl3 != null && !depotFscLvl3.equals("***") && (isControlFsc)){
                sb.append(" and depot_fsc_crflv3 = '" +depotFscLvl3 + "' ");
            }else if(!depotFscLvl3.equals("***") && (!isControlFsc)){
                sb.append(" and depot_fsc = '" +fscCodeOfUser+ "' ");
            }
            
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append(" [order by :sortBy :sortIn] ");
        }
        System.out.print("===============### "+sb.toString()+" ###==================");
        return sb.toString();
    }
    //end: implement function


    public CamFscDao getCamFscDao() {
        return camFscDao;
    }

   
}
