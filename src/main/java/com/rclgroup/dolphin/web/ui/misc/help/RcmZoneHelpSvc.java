/*-----------------------------------------------------------------------------------------------------------  
RcmZoneHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsZoneDao;
import com.rclgroup.dolphin.web.util.RutString;
 import javax.servlet.ServletContext;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 import javax.servlet.http.HttpSession;

 public class RcmZoneHelpSvc extends RrcStandardHelpSvc {
     public RcmZoneHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmZoneHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmZoneHelpUim";
         String helpScnName = "/RcmZoneHelpScn.jsp?service=ui.misc.help.RcmZoneHelpSvc";
         
         RrcStandardHelpUim uim = (RcmZoneHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmZoneHelpUim newZoneHelpUim = new RcmZoneHelpUim();
         
         newZoneHelpUim.setRegionCode(RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase());
         newZoneHelpUim.setAreaCode(RutString.nullToStr(request.getParameter("txtArea")).toUpperCase());
         newZoneHelpUim.setControlZoneCode(RutString.nullToStr(request.getParameter("txtControlZone")).toUpperCase());
         
         newZoneHelpUim.setEmsZoneDao((EmsZoneDao)getBean("emsZoneDao"));
         RrcStandardHelpUim newUim = newZoneHelpUim;
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmZoneHelpSvc][execute]: Finished");
     }
 }
