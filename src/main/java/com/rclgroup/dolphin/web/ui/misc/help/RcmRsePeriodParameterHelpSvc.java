package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmRsePeriodParameterHelpSvc  extends RrcStandardHelpOptimizeSvc {
    public RcmRsePeriodParameterHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmRseGroupMandatoryHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeUim uim = (RcmRsePeriodParameterHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmRsePeriodParameterHelpUim.class);
        if (uim == null) {
            uim = new RcmRsePeriodParameterHelpUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmRseGroupMandatoryHelpSvc][execute]: Finished");
    }
}
