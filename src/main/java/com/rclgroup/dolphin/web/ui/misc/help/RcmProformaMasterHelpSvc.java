/*-----------------------------------------------------------------------------------------------------------  
RcmProformaMasterHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 02/06/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.vss.VssProformaMasterDao;

import com.rclgroup.dolphin.web.util.RutString;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmProformaMasterHelpSvc extends RrcStandardHelpSvc {
    public RcmProformaMasterHelpSvc() {
        super();
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmProformaMasterHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmProformaMasterHelpUim";
        String helpScnName = "/RcmProformaMasterHelpScn.jsp?service=ui.misc.help.RcmProformaMasterHelpSvc";
        
        RrcStandardHelpUim uim = (RcmProformaMasterHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmProformaMasterHelpUim newProformaMasterHelpUim = new RcmProformaMasterHelpUim();
        
         //get data from screen: serviceCode
        String serviceCode = RutString.getParameterToStringUpper(request, "serviceCode");
        
        //set value to new uim
        newProformaMasterHelpUim.setServiceCode(serviceCode);
        
        newProformaMasterHelpUim.setVssProformaMasterDao((VssProformaMasterDao)getBean("vssProformaMasterDao"));
        RrcStandardHelpUim newUim = newProformaMasterHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmProformaMasterHelpSvc][execute]: Finished");
    }
}

