/*------------------------------------------------------
RcmTermHelpSvc.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 02/09/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.qtn.QtnTermDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmTermHelpSvc extends RrcStandardHelpSvc {
    public RcmTermHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmTermHelpSvc][execute]: Started");
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmTermHelpUim";
        String helpScnName = "/RcmTermHelpScn.jsp?service=ui.misc.help.RcmTermHelpSvc";
        
        RrcStandardHelpUim uim = (RcmTermHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmTermHelpUim newTermHelpUim = new RcmTermHelpUim();
        
        newTermHelpUim.setQtnTermDao((QtnTermDao)getBean("qtnTermDao"));
        RrcStandardHelpUim newUim = newTermHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmTermHelpSvc][execute]: Finished");
    }
}