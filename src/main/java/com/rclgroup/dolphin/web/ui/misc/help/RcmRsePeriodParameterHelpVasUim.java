package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeVasappsUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class RcmRsePeriodParameterHelpVasUim extends RcmStandardHelpOptimizeVasappsUim {
    private String usrPermission;
    public RcmRsePeriodParameterHelpVasUim() {
        super();
        
        // default sort
        this.setSortBy("PERIOD_PARAMETER_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
    //get request parameter
    String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
     
    this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
    return "Period Parameter";
    }
    
    public HashMap findColumnNameShowByType(String type) {
    HashMap columnNameShow = new HashMap();
    
    columnNameShow.put("PERIOD_PARAMETER_CODE", new RcmColumnNameShowMod("Period Parameter Code|40|left|STRING"));  
    columnNameShow.put("NO_OF_DAY", new RcmColumnNameShowMod("No of Day|20|left|STRING"));  
    
    return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
    String[] arrReturnValue = null;
    
    arrReturnValue = new String[] {"PERIOD_PARAMETER_CODE"};
    return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
    StringBuffer sb = new StringBuffer("");
    sb.append(" SELECT PERIOD_PARAMETER_CODE, ");
    sb.append(" NO_OF_DAY ");
    sb.append(" FROM CAM_PERIOD_PARAMETER ");
    sb.append(" [where :columnName :conditionWild :columnFind] ");
    sb.append("[order by :sortBy :sortIn] ");
    
    return sb.toString();
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
    
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}

