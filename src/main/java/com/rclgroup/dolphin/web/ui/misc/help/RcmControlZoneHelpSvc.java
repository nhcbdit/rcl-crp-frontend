/*-----------------------------------------------------------------------------------------------------------  
RcmControlZoneHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsControlZoneDao;
import com.rclgroup.dolphin.web.util.RutString;

 import javax.servlet.ServletContext;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 import javax.servlet.http.HttpSession;

 public class RcmControlZoneHelpSvc extends RrcStandardHelpSvc {
     public RcmControlZoneHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmControlZoneHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmControlZoneHelpUim";
         String helpScnName = "/RcmControlZoneHelpScn.jsp?service=ui.misc.help.RcmControlZoneHelpSvc";
         
         RrcStandardHelpUim uim = (RcmControlZoneHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmControlZoneHelpUim newControlZoneHelpUim = new RcmControlZoneHelpUim();
         
         newControlZoneHelpUim.setRegionCode(RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase());
         newControlZoneHelpUim.setAreaCode(RutString.nullToStr(request.getParameter("txtArea")).toUpperCase());
         
         newControlZoneHelpUim.setEmsControlZoneDao((EmsControlZoneDao)getBean("emsControlZoneDao"));
         RrcStandardHelpUim newUim = newControlZoneHelpUim;
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmControlZoneHelpSvc][execute]: Finished");
     }
 }
