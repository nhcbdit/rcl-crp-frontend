package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.bsa.BsaBsaPortPairTsDao;

import java.util.List;

public class RcmBsaPortPairTsHelpUim2 extends RrcStandardHelpUim{
    public RcmBsaPortPairTsHelpUim2() {
        tsIndic = "";
        tsPolPort = "";
        tsPodPort = "";
        serviceVariantId = -1;
        rowAt = "0";
        modelId = 0;
        //add for new bsa logic//
        ownService ="";
        ownProfRefNo ="";
        modelName ="";
    }
    private BsaBsaPortPairTsDao bsaPortPairTsDao;
    private String tsIndic;
    private String tsPolPort;
    private String tsPodPort;
    private String polTsFlag;
    private String podTsFlag;
    private String tsIndicName;
    private String rowAt;
    private int serviceVariantId;
    private int modelId;
    
    //new logic for searching tranship//
    private String ownService;
    private String ownProfRefNo;
    private String modelName;

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_WITH_ACTIVE_STATUS)){
           list = bsaPortPairTsDao.listForHelpScreenForVolumnPortPair(find,search,wild,"A",this.tsIndic,this.ownService,this.modelId,this.ownProfRefNo,this.tsPolPort,this.tsPodPort);
       }
        return list;        
    }

    public void setBsaPortPairTsDao(BsaBsaPortPairTsDao bsaPortPairTsDao) {
        this.bsaPortPairTsDao = bsaPortPairTsDao;
    }

    public void setTsIndic(String tsIndic) {
        this.tsIndic = tsIndic;
    }

    public String getTsIndic() {
        return tsIndic;
    }

    public void setServiceVariantId(int serviceVariantId) {
        this.serviceVariantId = serviceVariantId;
    }

    public int getServiceVariantId() {
        return serviceVariantId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setTsPolPort(String tsPolPort) {
        this.tsPolPort = tsPolPort;
    }

    public String getTsPolPort() {
        return tsPolPort;
    }
    public void setTsPodPort(String tsPodPort) {
        this.tsPodPort = tsPodPort;
    }

    public String getTsPodPort() {
        return tsPodPort;
    }

    public void setTsIndicName(String tsIndicName) {
        this.tsIndicName = tsIndicName;
    }

    public String getTsIndicName() {
        return tsIndicName;
    }

    public void setPolTsFlag(String polTsFlag) {
        this.polTsFlag = polTsFlag;
    }

    public String getPolTsFlag() {
        return polTsFlag;
    }

    public void setPodTsFlag(String podTsFlag) {
        this.podTsFlag = podTsFlag;
    }

    public String getPodTsFlag() {
        return podTsFlag;
    }

    public void setRowAt(String rowAt) {
        this.rowAt = rowAt;
    }

    public String getRowAt() {
        return rowAt;
    }
    public void setOwnService(String ownService) {
        this.ownService = ownService;
    }

    public String getOwnService() {
        return ownService;
    }

    public void setOwnProfRefNo(String ownProfRefNo) {
        this.ownProfRefNo = ownProfRefNo;
    }

    public String getOwnProfRefNo() {
        return ownProfRefNo;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelName() {
        return modelName;
    }
}
