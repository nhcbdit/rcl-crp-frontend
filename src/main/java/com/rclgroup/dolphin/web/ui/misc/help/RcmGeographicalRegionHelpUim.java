/*-----------------------------------------------------------------------------------------------------------  
RcmGeographicalRegionHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.ems.EmsGeographicalRegionDao;


 import java.util.List;

 public class RcmGeographicalRegionHelpUim extends RrcStandardHelpUim{
    
    private EmsGeographicalRegionDao emsGeographicalRegionDao;
     
    public RcmGeographicalRegionHelpUim(){
    }
    
     public void setEmsGeographicalRegionDao(EmsGeographicalRegionDao emsGeographicalRegionDao) {
         this.emsGeographicalRegionDao = emsGeographicalRegionDao;
     }
     
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsGeographicalRegionDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = emsGeographicalRegionDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }
         return list;
     }
 }
