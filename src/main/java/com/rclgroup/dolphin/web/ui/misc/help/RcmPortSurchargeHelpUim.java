 /*------------------------------------------------------
 RcmPortSurchargeHelpUim.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Sopon Dee-udomvongsa 02/09/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.qtn.QtnPortSurchargeDao;

import java.util.List;


public class RcmPortSurchargeHelpUim extends RrcStandardHelpUim{
 
     private QtnPortSurchargeDao qtnPortSurchargeDao;
     
     public void setQtnPortSurchargeDao(QtnPortSurchargeDao qtnPortSurchargeDao) {
         this.qtnPortSurchargeDao = qtnPortSurchargeDao;
     }
    
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = qtnPortSurchargeDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = qtnPortSurchargeDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }
         return list;
     }
 }
