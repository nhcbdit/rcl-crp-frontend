/*-----------------------------------------------------------------------------------------------------------  
RcmSlotOperatorHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Surachai Yindeeram 19/01/2015
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY     -User-     -TaskRef-      -Short Description
01 26/01/15     SURYUN1                     Fixed issues can't to find by criteria because the query not clear.
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.model.cam.CamFscMod;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RcmSlotOperatorHelpUim extends RcmStandardHelpOptimizeUim {
    private String depotFscLvl1;
    private String depotFscLvl2;
    private String depotFscLvl3;
    
    public RcmSlotOperatorHelpUim() {
        super();
        // default sort
        this.setSortBy("VAL");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        HttpSession session = request.getSession(false);
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        if(camFscDao != null){
            CamFscMod camFscMod = camFscDao.findByKeyfscCode(fscCodeOfUser);
            depotFscLvl1 = camFscMod.getLineCode();
            depotFscLvl2 = camFscMod.getRegionCode();
            depotFscLvl3 = camFscMod.getAgentCode();
            isControlFsc = camFscDao.isControlFsc(userBean.getFscLvl1(), userBean.getFscLvl2(), userBean.getFscLvl3(), userBean.getFscCode());
        }
    }
     
    public String findTitleNameShowByType(String type) {
        if(RcmConstant.GET_GENERAL.equals(type)){
            return "Slot Operator Code";
        }
        return "Slot Operator Code";
    }
     
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]

        if (RcmConstant.GET_GENERAL.equals(type)){
            columnNameShow.put("VAL", new RcmColumnNameShowMod("Slot Operator Code|10|left|STRING"));
            columnNameShow.put("DESCRIPTION", new RcmColumnNameShowMod("Description|20|left|STRING"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"VAL", "DESCRIPTION"};
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sb.append(" SELECT VAL, DESCRIPTION FROM (SELECT VALUE VAL, DESCRIPTION FROM  " +
                       " VASAPPS.VR_SLM_LKP_OPER_CODE WHERE STATUS = 'A') WHERE 1=1  ");
           
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append(" [order by :sortBy :sortIn] ");
        }
        System.out.print("===============### "+sb.toString()+" ###==================");
        return sb.toString();
    }
    //end: implement function


    public CamFscDao getCamFscDao() {
        return camFscDao;
    }

   
}
