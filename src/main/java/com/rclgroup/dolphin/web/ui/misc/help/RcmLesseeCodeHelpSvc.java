 /*-----------------------------------------------------------------------------------------------------------  
 RcmTerminalHelpSvc.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Natcha Phuukarat 24/04/14
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmLesseeCodeHelpSvc extends RrcStandardHelpOptimizeSvc {
     public RcmLesseeCodeHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmLesseeHelpSvc][execute]: Started");
         HttpSession session = request.getSession(false);
         
         RcmStandardHelpOptimizeUim uim = (RcmLesseeCodeHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmLesseeCodeHelpUim.class);
         if (uim == null) {
             uim = new RcmLesseeCodeHelpUim();
         }
         
         this.doExecute(request, uim);
         System.out.println("[RcmLesseeHelpSvc][execute]: Finished");
     }
}
