 /*------------------------------------------------------
 RcmPointHelpSvc.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sopon Dee-udomvongsa 18/07/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.cam.CamPointDao;

import com.rclgroup.dolphin.web.util.RutString;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmPointHelpSvc extends RrcStandardHelpSvc {
     public RcmPointHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmPointHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmPointHelpUim";
         String helpScnName = "/RcmPointHelpScn.jsp?service=ui.misc.help.RcmPointHelpSvc";
         
         RrcStandardHelpUim uim = (RcmPointHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmPointHelpUim newPointHelpUim = new RcmPointHelpUim();
         
         newPointHelpUim.setControlZone(RutString.nullToStr(request.getParameter("hidControlZone")).toUpperCase());
         newPointHelpUim.setFsc(RutString.nullToStr(request.getParameter("hidFsc")).toUpperCase());
         
         newPointHelpUim.setRegionCode(RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase());
         newPointHelpUim.setAreaCode(RutString.nullToStr(request.getParameter("txtArea")).toUpperCase());
         newPointHelpUim.setControlZoneCode(RutString.nullToStr(request.getParameter("txtControlZone")).toUpperCase());
         newPointHelpUim.setZoneCode(RutString.nullToStr(request.getParameter("txtZone")).toUpperCase());
         
         newPointHelpUim.setCamPointDao((CamPointDao)getBean("camPointDao"));
         RrcStandardHelpUim newUim = newPointHelpUim;       
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmPointHelpSvc][execute]: Finished");
     }
 }
