/*-----------------------------------------------------------------------------------------------------------  
RcmPortHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 05/11/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


public class RcmDepotTerminalHelpUim extends RcmStandardHelpOptimizeUim {
    public RcmDepotTerminalHelpUim() {
        super();
        
     // default sort
        this.setSortBy("DEPOT_TERMINAL");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
    	
    }
    
    public String findTitleNameShowByType(String type) {
        return "Depot/Terminal";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type)) {
            columnNameShow.put("DEPOT_TERMINAL", new RcmColumnNameShowMod("Depot/Terminal Code|14|left|STRING"));
            columnNameShow.put("DEPOT_TERMINAL_NAME", new RcmColumnNameShowMod("Depot/Terminal Name|17|left|STRING"));
            columnNameShow.put("FSC", new RcmColumnNameShowMod("FSC|14|left|STRING"));
            columnNameShow.put("Depot_or_Terminal", new RcmColumnNameShowMod("Depot/Terminal|17|left|STRING"));
        }
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"DEPOT_TERMINAL"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
    	System.out.println("[RcmDepotTerminalHelpUim][Begin][findSqlStatementByType] typeXXXXXX:"+type);
        StringBuffer sb 		= new StringBuffer("");
        StringBuffer sbWhere 	= new StringBuffer("");
        
        try{
	        if (RcmConstant.GET_GENERAL.equals(type)) {
	            sb.append("select ");
	            sb.append(" I130.TQTERM as DEPOT_TERMINAL ");
	            sb.append(" ,I130.TQTRNM as DEPOT_TERMINAL_NAME ");
	            sb.append(" ,I130.TQOFFC as FSC ");
	            sb.append(" ,decode(i130.terminal_depot_flag,'D','Depot','T','Terminal','') as Depot_or_Terminal ");
	            sb.append("from ITP130 i130,DEPOT_CONTRACT_MASTER DCM ");
	            sb.append("where i130.TQTERM = DCM.DEPOT_CODE ");
	            sb.append("AND DCM.storage_handelling_contract is null ");
	            sb.append("and DCM.RECORD_STATUS = 'A' ");
	            sb.append("AND i130.TQRCST = 'A' ");
	            
	           
	            sb.append(" [and :columnName :conditionWild :columnFind] ");
	            sb.append("[order by :sortBy :sortIn] ");            
	        }
        }catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("[RcmDepotTerminalHelpUim][End][findSqlStatementByType] typeXXXXXX:"+type);
		}
		return sb.toString();
    }
    //end: implement function

}
