package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ijs.IjsJobOrderDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmJobOrderHelpSvc extends RrcStandardHelpSvc {
    public RcmJobOrderHelpSvc() {
    }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmJobOrderHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmJobOrderHelpUim";
         String helpScnName = "/RcmJobOrderHelpScn.jsp?service=ui.misc.help.RcmJobOrderHelpSvc";
         
         RrcStandardHelpUim uim = (RcmJobOrderHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmJobOrderHelpUim newJobOrderHelpUim = new RcmJobOrderHelpUim();         
        
         newJobOrderHelpUim.setIjsJobOrderDao((IjsJobOrderDao)getBean("ijsJobOrderDao"));
         RrcStandardHelpUim newUim = newJobOrderHelpUim;       
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmJobOrderHelpSvc][execute]: Finished");
     }
    
}
