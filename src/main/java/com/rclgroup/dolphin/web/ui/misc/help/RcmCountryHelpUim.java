/*-----------------------------------------------------------------------------------------------------------  
RcmCountryHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 15/10/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class RcmCountryHelpUim extends RcmStandardHelpOptimizeUim {
    
    private String usrPermission;
    
    public RcmCountryHelpUim() {
        super();
        
        // default sort
        this.setSortBy("COUNTRY_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        // get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
        
        this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Country";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            columnNameShow.put("COUNTRY_CODE", new RcmColumnNameShowMod("Country Code|15|left|STRING"));
            columnNameShow.put("COUNTRY_NAME", new RcmColumnNameShowMod("Country Name|40|left|STRING"));
            columnNameShow.put("COUNTRY_STATUS", new RcmColumnNameShowMod("Status|12|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getStatusDesc"));
        }
        return columnNameShow;
    }
     
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            arrReturnValue = new String[] {"COUNTRY_CODE"};
        }
        return arrReturnValue;
    }
     
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            sb.append("select COUNTRY_CODE ");
            sb.append(" ,COUNTRY_NAME ");
            sb.append(" ,COUNTRY_STATUS ");
            sb.append("from VR_CAM_COUNTRY ");
            
            StringBuffer sbWhere = new StringBuffer("");
            if (RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
                sbWhere.append(" and COUNTRY_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
            }
            if (usrPermission != null && !"".equals(usrPermission)) {
                sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_COUNTRY('"+usrPermission+"', COUNTRY_CODE) = '"+RcmConstant.FLAG_YES+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere.substring(4));
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        }
        return sb.toString();
    }
    //end: implement function
    
    public String getUsrPermission() {
        return usrPermission;
    }
    
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
