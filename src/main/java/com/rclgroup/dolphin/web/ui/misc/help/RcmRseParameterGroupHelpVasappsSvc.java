package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeVasappsSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeVasappsUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmRseParameterGroupHelpVasappsSvc extends RrcStandardHelpOptimizeVasappsSvc {
    public RcmRseParameterGroupHelpVasappsSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmRseParameterGroupHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeVasappsUim uim = (RcmRseParameterGroupHelpVasappsUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeVasappsSvc.INSTANCE_NAME_UIM_DEFAULT, RcmRseParameterGroupHelpVasappsUim.class);
        if (uim == null) {
            uim = new RcmRseParameterGroupHelpVasappsUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmRseParameterGroupHelpVasappsSvc][execute]: Finished");
    }
}
