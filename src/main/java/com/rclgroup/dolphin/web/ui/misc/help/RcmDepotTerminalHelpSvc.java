/*-----------------------------------------------------------------------------------------------------------  
RcmDepotTerminalHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 05/11/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;


public class RcmDepotTerminalHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmDepotTerminalHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmDepotTerminalHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeUim uim = (RcmDepotTerminalHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmDepotTerminalHelpUim.class);
        System.out.println("[RcmDepotTerminalHelpSvc][execute]: uim:" + uim);
        if (uim == null) {
            uim = new RcmDepotTerminalHelpUim();
        }
        
        this.doExecute(request, uim);
        System.out.println("[RcmDepotTerminalHelpSvc][execute]: Finished");
    }
}
