 /*------------------------------------------------------
 RcmQtnExportToExcelHelpSvc.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2008
 --------------------------------------------------------
  Author Kitti Pongsirisakun 01/12/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.dao.qtn.QtnPortSurchargeDao;

import com.rclgroup.dolphin.web.dao.qtn.QtnExportToExcelDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmQtnExportToExcelHelpSvc extends RrcStandardHelpSvc {
     public RcmQtnExportToExcelHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmQtnExportToExcelHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmQtnExportToExcelHelpUim";
         String helpScnName = "/RcmQuotationHelpScn.jsp?service=ui.misc.help.RcmQtnExportToExcelHelpSvc";
         
         RrcStandardHelpUim uim = (RcmQtnExportToExcelHelpUim)session.getAttribute(helpUiModInstanceName);
       
         RcmQtnExportToExcelHelpUim newQuotationHelpUim = new RcmQtnExportToExcelHelpUim();
         newQuotationHelpUim.setQtnExportToExcelDao((QtnExportToExcelDao)getBean("qtnExportToExcelDao"));
       
         RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
         
         String line = userBean.getFscLvl1();
         String region = userBean.getFscLvl2();
         String agent = userBean.getFscLvl3();
         String fsc = userBean.getFscCode();
         
         newQuotationHelpUim.setLineCodeOfUser(line);
         newQuotationHelpUim.setCamFscDao((CamFscDao)getBean("camFscDao"));
         if(region.equals("*")){ //check whether user is in line level
             newQuotationHelpUim.setLevel(RrcStandardUim.LEVEL_LINE,line,"","","");
             newQuotationHelpUim.setRegionCodeOfUser("");
             newQuotationHelpUim.setAgentCodeOfUser("");
             newQuotationHelpUim.setFscCodeOfUser("");
         }else if(agent.equals("***")){ //check whether user is in region level
             newQuotationHelpUim.setLevel(RrcStandardUim.LEVEL_REGION,line,region,"","");
             newQuotationHelpUim.setRegionCodeOfUser(region);
             newQuotationHelpUim.setAgentCodeOfUser("");
             newQuotationHelpUim.setFscCodeOfUser("");
         }else{ //check whether user is in agent level
             newQuotationHelpUim.setLevel(RrcStandardUim.LEVEL_AGENT,line,region,agent,"");
             newQuotationHelpUim.setRegionCodeOfUser(region);
             newQuotationHelpUim.setAgentCodeOfUser(agent);
             newQuotationHelpUim.setFscCodeOfUser(fsc);
         }
         newQuotationHelpUim.setIsControlFsc(line,region,agent,fsc); 
         
         RrcStandardHelpUim newUim = newQuotationHelpUim;
         
         helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
         System.out.println("[RcmQtnExportToExcelHelpSvc][execute]: Finished");
     }
 }
