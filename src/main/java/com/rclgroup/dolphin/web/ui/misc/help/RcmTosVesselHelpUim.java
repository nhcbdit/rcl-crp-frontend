package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class RcmTosVesselHelpUim extends RcmStandardHelpOptimizeUim {
    private String usrPermission;
    
    public RcmTosVesselHelpUim() {
        super();
        
        this.setSortBy("OPERATOR_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
        // get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();        
        this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Vessel Operator";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type)) {
            columnNameShow.put("OPERATOR_CODE", new RcmColumnNameShowMod("Operator Code|15|left|STRING"));            
            columnNameShow.put("RECORD_STATUS", new RcmColumnNameShowMod("Status|15|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getStatusDesc"));
        }
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"OPERATOR_CODE"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sql = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type)) {
            sql.append("SELECT DISTINCT OPERATOR_CODE ");
            sql.append("      ,RECORD_STATUS ");
            sql.append("FROM VR_CAM_VESSEL_MASTER ");
            sql.append(" WHERE RECORD_STATUS = 'A' ");
            sql.append(" [and :columnName :conditionWild :columnFind] ");
            sql.append("[order by :sortBy :sortIn] ");
        }
        return sql.toString();
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
    
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
