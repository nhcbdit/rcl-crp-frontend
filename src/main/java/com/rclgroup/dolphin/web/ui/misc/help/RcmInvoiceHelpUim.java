package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.far.FarInvoiceDao;

import java.util.List;

public class RcmInvoiceHelpUim extends RrcStandardHelpUim{
    public RcmInvoiceHelpUim() {
    }
    private FarInvoiceDao farInvoiceDao;
    
    public void setFarInvoiceDao(FarInvoiceDao farInvoiceDao) {
        this.farInvoiceDao = farInvoiceDao;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        String line = this.getLineCodeOfUser();
        String trade = this.getRegionCodeOfUser();
        String agent = this.getAgentCodeOfUser();
        String fsc = this.getFscCodeOfUser();        
        
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS)){
            list = farInvoiceDao.listForHelpScreenByFsc(find,search,wild,line,trade,agent,fsc);       
        }else if (type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
            list = farInvoiceDao.listForHelpScreen(find,search,wild);
        }else if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_HELP_V02_WITH_USER_LEVEL_ACTIVE_STATUS)){
            int size = farInvoiceDao.countListForNewHelpScreenByFsc(find,search,wild,line,trade,agent,fsc);
            setNoOfResultRecords(size);//All records size            
            list = farInvoiceDao.listForNewHelpScreenByFsc(rutPage.calculateRowAt(),rutPage.calculateRowTo(),find,search,wild,line,trade,agent,fsc);                         
        }
        return list;
                
    }
    
}
