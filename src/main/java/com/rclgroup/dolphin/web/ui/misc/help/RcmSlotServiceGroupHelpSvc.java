/*-----------------------------------------------------------------------------------------------------------  
RcmSlotAgreementHelpSvc 
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Surachai Yindeeram 04/02/2015
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmSlotServiceGroupHelpSvc extends RrcStandardHelpOptimizeSvc {
   
    public RcmSlotServiceGroupHelpSvc () {
        super();
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmSlotServiceGroupHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        RcmStandardHelpOptimizeUim uim = (RcmSlotServiceGroupHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmSlotServiceGroupHelpUim.class);
        if (uim == null) {
            uim = new RcmSlotServiceGroupHelpUim();          
        }
        uim.setCamFscDao((CamFscDao)getBean("camFscDao"));
        uim.setFscCodeOfUser(userBean.getFscCode()); 
        this.doExecute(request, uim);
        System.out.println("[RcmSlotServiceGroupHelpSvc][execute]: Finished");
    }
}
