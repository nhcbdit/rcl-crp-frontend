package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class RcmRseParameterGroupHelpUim extends RcmStandardHelpOptimizeUim {

    private String usrPermission;        
    private String userId;    
    private HttpSession session;
    
    public RcmRseParameterGroupHelpUim() {
        super();
        
        // default sort
        this.setSortBy("PARAMETER_GROUP_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();  //##01
        session = request.getSession(); //##01
        userId = ((RcmUserBean)session.getAttribute("userBean")).getPrsnLogId(); //##01        
        
        System.out.println("[RcmRseParameterGroupHelpUim][getPrsnLogIdOfUser]: "+userId); //##01
        this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Group Code";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type)|| RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            columnNameShow.put("PARAMETER_GROUP_CODE", new RcmColumnNameShowMod("Group Code|15|left|STRING"));
            columnNameShow.put("PARAMETER_GROUP_NAME", new RcmColumnNameShowMod("Group Name|25|left|STRING"));
            columnNameShow.put("ELEMENT_NAME", new RcmColumnNameShowMod("Element Name|10|left|STRING"));
            columnNameShow.put("ELEMENT_VALUE", new RcmColumnNameShowMod("Element Value|8|left|STRING"));
            columnNameShow.put("RECORD_STATUS", new RcmColumnNameShowMod("Status|10|left|FUNCTION|com.rclgroup.dolphin.web.util.RutCodeDescription|getStatusDesc"));
        }
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)|| RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            arrReturnValue = new String[] { "PARAMETER_GROUP_CODE","ELEMENT_VALUE" };
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            sb.append("SELECT PARAMETER_GROUP_CODE ");
            sb.append(" ,PARAMETER_GROUP_NAME");
            sb.append(" ,ELEMENT_NAME");
            sb.append(" ,ELEMENT_VALUE");
            sb.append(" ,RECORD_STATUS");
            sb.append(" FROM CAM_SERVICE_PARAMETER_GROUP ");           
            
            StringBuffer sbWhere = new StringBuffer("");
            if (RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
                sbWhere.append(" and RECORD_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere);
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        }
        return sb.toString();
    }
}
