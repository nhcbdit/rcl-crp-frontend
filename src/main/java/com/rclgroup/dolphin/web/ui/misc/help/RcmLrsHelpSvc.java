package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.vss.VssLrsDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmLrsHelpSvc extends RrcStandardHelpSvc {
    public RcmLrsHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmLrsHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmLrsHelpUim";
        String helpScnName = "/RcmLrsHelpScn.jsp?service=ui.misc.help.RcmLrsHelpSvc";
        
        RrcStandardHelpUim uim = (RcmLrsHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmLrsHelpUim newLrsHelpUim = new RcmLrsHelpUim();
        
        newLrsHelpUim.setVssLrsDao((VssLrsDao)getBean("vssLrsDao"));
        RrcStandardHelpUim newUim = newLrsHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmLrsHelpSvc][execute]: Finished");
    }
    
}
