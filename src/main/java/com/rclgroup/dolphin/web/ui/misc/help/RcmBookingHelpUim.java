package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.bkg.BkgBookingDao;


import java.util.List;

public class RcmBookingHelpUim extends RrcStandardHelpUim{
    private BkgBookingDao bkgBookingDao;
    
    public RcmBookingHelpUim() {
    }
    
    public void setBkgBookingDao(BkgBookingDao bkgBookingDao) {
        this.bkgBookingDao = bkgBookingDao;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        String line = this.getLineCodeOfUser();
        String trade = this.getRegionCodeOfUser();
        String agent = this.getAgentCodeOfUser();
        String fsc = this.getFscCodeOfUser();
        
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
            list = bkgBookingDao.listForHelpScreen(find,search,wild);
        }else if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_WITH_FSC)){
            list = bkgBookingDao.listForHelpScreenByFsc(find,search,wild,line,trade,agent,fsc);              
        }else if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_HELP_V02_WITH_FSC)){
            int size = bkgBookingDao.countListForNewHelpScreenByFsc(find,search,wild,line,trade,agent,fsc);
            setNoOfResultRecords(size);//All records size            
            list = bkgBookingDao.listForNewHelpScreenByFsc(rutPage.calculateRowAt(),rutPage.calculateRowTo(),find,search,wild,line,trade,agent,fsc);              
        }
        return list;
        
        
    }
}
