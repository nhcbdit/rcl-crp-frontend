package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;


import com.rclgroup.dolphin.web.dao.tos.TosServiceDao;

import java.util.List;

public class RcmTosServiceHelpUim extends RrcStandardHelpUim{

    private TosServiceDao tosServiceDao;
    
    public void setTosServiceDao(TosServiceDao tosServiceDao) {
        this.tosServiceDao = tosServiceDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){        
            list = tosServiceDao.listForHelpScreen(find, search, wild);
        }
        return list;
    }
    
}
