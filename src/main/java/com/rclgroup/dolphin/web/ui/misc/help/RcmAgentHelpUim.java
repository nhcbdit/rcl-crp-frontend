/*-----------------------------------------------------------------------------------------------------------  
RcmAgentHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 09/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 12/11/07 PIE                       Add type parameter for getListForHelpScreen
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.cam.CamAgentDao;
import java.util.List;

public class RcmAgentHelpUim extends RrcStandardHelpUim{
    private String regionCode;
    private String countryCode;
    private CamAgentDao camAgentDao;
    
    public RcmAgentHelpUim(){
        regionCode = "";
        countryCode = "";
    }
    
    public void setCamAgentDao(CamAgentDao camAgentDao) {
        this.camAgentDao = camAgentDao;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }
    
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL_WITH_REGION_COUNTRY)){
            list = camAgentDao.listForHelpScreen(find,search,wild,getRegionCode(),getCountryCode());
        }else if(type.equals(RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS)) {
            final String ACTIVE_STATUS = "A";
            String regionCode = this.getRegionCodeOfUser();
            if(!this.getRegionCode().equals("")){
                regionCode = this.getRegionCode();
            }
            list = camAgentDao.listForHelpScreenWithUserLevel(find,search,wild,this.getLineCodeOfUser(),regionCode,ACTIVE_STATUS);
        }
        return list;
    }
}
