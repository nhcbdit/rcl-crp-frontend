/*-----------------------------------------------------------------------------------------------------------  
RcmInvoyageBrowserHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 14/10/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmInvoyageBrowserHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmInvoyageBrowserHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmInvoyageBrowserHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
        
        RcmStandardHelpOptimizeUim uim = (RcmInvoyageBrowserHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmInvoyageBrowserHelpUim.class);
        if (uim == null) {
            uim = new RcmInvoyageBrowserHelpUim();
        }
        
        // set target page
        String target = RcmConstant.HELP_PAGE_URL + "/RcmInvoyageBrowserHelpScn.jsp";
        uim.setPageTarget(target);
        
        this.doExecute(request, uim);
        System.out.println("[RcmInvoyageBrowserHelpSvc][execute]: Finished");
    }
}
