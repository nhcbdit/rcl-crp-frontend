/*-----------------------------------------------------------------------------------------------------------  
RcmActivityHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 5/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsActivityDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmActivityHelpSvc extends RrcStandardHelpSvc {
    public RcmActivityHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmActivityHelpSvc][execute]: Started");
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmActivityHelpUim";
        String helpScnName = "/RcmActivityHelpScn.jsp?service=ui.misc.help.RcmActivityHelpSvc";
        
        RrcStandardHelpUim uim = (RcmActivityHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmActivityHelpUim newActivityHelpUim = new RcmActivityHelpUim();
        
        newActivityHelpUim.setEmsActivityDao((EmsActivityDao)getBean("emsActivityDao"));
        RrcStandardHelpUim newUim = newActivityHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmActivityHelpSvc][execute]: Finished");
    }
}
