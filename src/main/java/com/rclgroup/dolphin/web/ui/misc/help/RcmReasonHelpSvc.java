package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ijs.IjsReasonDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmReasonHelpSvc extends RrcStandardHelpSvc {
    public RcmReasonHelpSvc() {
    }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmReasonHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmReasonHelpUim";
         String helpScnName = "/RcmReasonHelpScn.jsp?service=ui.misc.help.RcmReasonHelpSvc";
         
         RrcStandardHelpUim uim = (RcmReasonHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmReasonHelpUim newReasonHelpUim = new RcmReasonHelpUim();         
        
         newReasonHelpUim.setIjsReasonDao((IjsReasonDao)getBean("ijsReasonDao"));
         RrcStandardHelpUim newUim = newReasonHelpUim;       
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmReasonHelpSvc][execute]: Finished");
     }
    
}
