 /*-----------------------------------------------------------------------------------------------------------  
 RcmBillNoHelpSvc.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Kitti Pongsirisakun 17/06/2009
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsActivityDao;

import com.rclgroup.dolphin.web.dao.far.FarBillingStatementAndInvoiceLinkageDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmBillNoHelpSvc extends RrcStandardHelpSvc {
    public RcmBillNoHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmBillNoHelpSvc][execute]: Started");
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmBillNoHelpUim";
        String helpScnName = "/RcmBillNoHelpScn.jsp?service=ui.misc.help.RcmBillNoHelpSvc";
        
        RrcStandardHelpUim uim = (RcmBillNoHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmBillNoHelpUim newActivityHelpUim = new RcmBillNoHelpUim();
        
        newActivityHelpUim.setFarBillingStatementAndInvoiceLinkageDao((FarBillingStatementAndInvoiceLinkageDao)getBean("farInvLinkDao"));
        RrcStandardHelpUim newUim = newActivityHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmBillNoHelpSvc][execute]: Finished");
    }
}
