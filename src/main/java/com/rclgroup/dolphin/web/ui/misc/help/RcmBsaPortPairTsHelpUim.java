package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.bsa.BsaBsaPortPairTsDao;
import com.rclgroup.dolphin.web.dao.dex.DexBlDao;

import java.util.List;

public class RcmBsaPortPairTsHelpUim extends RrcStandardHelpUim{
    public RcmBsaPortPairTsHelpUim() {
        tsIndic = "";
        tsPolPort = "";
        tsPodPort = "";
        serviceVariantId = -1;
        rowAt = "0";
        modelId = 0;
    }
    private BsaBsaPortPairTsDao bsaPortPairTsDao;
    private String tsIndic;
    private String tsPolPort;
    private String tsPodPort;
    private String polTsFlag;
    private String podTsFlag;
    private String tsIndicName;
    private String rowAt;
    private int serviceVariantId;
    private int modelId;

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_WITH_ACTIVE_STATUS)){
            list = bsaPortPairTsDao.listForHelpScreenWithStatus(find,search,wild,"A",this.tsIndic,this.serviceVariantId,this.modelId,this.tsPolPort,this.tsPodPort,this.polTsFlag,this.podTsFlag);
       }
        return list;        
    }

    public void setBsaPortPairTsDao(BsaBsaPortPairTsDao bsaPortPairTsDao) {
        this.bsaPortPairTsDao = bsaPortPairTsDao;
    }

    public void setTsIndic(String tsIndic) {
        this.tsIndic = tsIndic;
    }

    public String getTsIndic() {
        return tsIndic;
    }

    public void setServiceVariantId(int serviceVariantId) {
        this.serviceVariantId = serviceVariantId;
    }

    public int getServiceVariantId() {
        return serviceVariantId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setTsPolPort(String tsPolPort) {
        this.tsPolPort = tsPolPort;
    }

    public String getTsPolPort() {
        return tsPolPort;
    }
    public void setTsPodPort(String tsPodPort) {
        this.tsPodPort = tsPodPort;
    }

    public String getTsPodPort() {
        return tsPodPort;
    }

    public void setTsIndicName(String tsIndicName) {
        this.tsIndicName = tsIndicName;
    }

    public String getTsIndicName() {
        return tsIndicName;
    }

    public void setPolTsFlag(String polTsFlag) {
        this.polTsFlag = polTsFlag;
    }

    public String getPolTsFlag() {
        return polTsFlag;
    }

    public void setPodTsFlag(String podTsFlag) {
        this.podTsFlag = podTsFlag;
    }

    public String getPodTsFlag() {
        return podTsFlag;
    }

    public void setRowAt(String rowAt) {
        this.rowAt = rowAt;
    }

    public String getRowAt() {
        return rowAt;
    }
}
