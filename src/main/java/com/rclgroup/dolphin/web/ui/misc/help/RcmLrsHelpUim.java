package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.tos.TosServiceDao;

import com.rclgroup.dolphin.web.dao.vss.VssLrsDao;

import java.util.List;

public class RcmLrsHelpUim extends RrcStandardHelpUim{

    private VssLrsDao vssLrsDao;
    
    public void setVssLrsDao(VssLrsDao vssLrsDao) {
        this.vssLrsDao = vssLrsDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){        
            list = vssLrsDao.listForHelpScreen(find, search, wild);
        }
        return list;
    }    
}
