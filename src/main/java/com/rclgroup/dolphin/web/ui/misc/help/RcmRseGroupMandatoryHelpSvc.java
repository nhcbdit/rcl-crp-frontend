package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmRseGroupMandatoryHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmRseGroupMandatoryHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmRseGroupMandatoryHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeUim uim = (RcmRseGroupMandatoryHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmRseGroupMandatoryHelpUim.class);
        if (uim == null) {
            uim = new RcmRseGroupMandatoryHelpUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmRseGroupMandatoryHelpSvc][execute]: Finished");
    }
}
