/*
---------------------------------------------------------------------------------------------------------
PilotInquiryUim.java
---------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
---------------------------------------------------------------------------------------------------------
Author Nuttapol Thanasrisatit 07/05/2013
- Change Log---------------------------------------------------------------------------------------------
## 		DD/MM/YY 		-User- 		-TaskRef-       -ShortDescription
01 		06/09/18 		Nuttapol               		Create this screen
---------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.ui.pilot;

import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.dnd.DndMonitoringReportDao;
import com.rclgroup.dolphin.web.model.dnd.DndMonitoringReportMod;

import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;
import java.util.List;

/**
 */
public class PilotInquiryUim  extends RrcStandardUim {

	private String strPermissionUser;
    
    public PilotInquiryUim() {    
     
    }
    
    public String getStrPermissionUser() {
        return strPermissionUser;
    }

    public void setStrPermissionUser(String strPermissionUser) {
        this.strPermissionUser = strPermissionUser;
    }

}
