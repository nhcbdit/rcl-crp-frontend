package com.rclgroup.dolphin.web.ui.crp;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.dao.dnd.DndMonitoringReportDao;
import com.rclgroup.dolphin.web.dao.rcm.RcmConstantDao;
import com.rclgroup.dolphin.web.ui.dnd.DndMonitoringReportUim;
import com.rclgroup.dolphin.web.util.RutString;

public class CrpScheduleMaintenanceSvc extends RrcStandardSvc{
	public static final String TARGET_PAGE = "/CrpScheduleMaintenanceScn.jsp";
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context)
			throws Exception {
		String strTarget = RcmConstant.CRP_PAGE_URL + TARGET_PAGE;
		System.out.println("CAll CrpScheduleMaintenanceSvc.");
		
		CrpScheduleMaintenanceUim uim = initialSession(request);
		
		String voyageId = request.getParameter("voyageId");
		uim.setCurrentPort(voyageId);
		
		String strPageAction = RutString.nullToStr(request.getParameter("pageAction"));
		
		if( strPageAction.equals("historyRevisionPopup") ) {
			String revision = request.getParameter("revision");
			
			request.setAttribute("target", RcmConstant.CRP_PAGE_URL + "/popup/historyRevision.jsp");
		} else {
			request.setAttribute("target", strTarget);
		}
		
		
	}
	
	private CrpScheduleMaintenanceUim initialSession(HttpServletRequest request) {
		final String sessionkey = "crpScheduleMaintenanceUim";
		
		HttpSession session = request.getSession(false); 
		String strPageAction = RutString.nullToStr(request.getParameter("pageAction"));
		
		CrpScheduleMaintenanceUim uim = (CrpScheduleMaintenanceUim) session.getAttribute(sessionkey);
		
		if (uim == null || strPageAction.equalsIgnoreCase("new")) {
            
            session.removeAttribute(sessionkey);
            
            uim = new CrpScheduleMaintenanceUim();
            uim.setRcmConstantDao((RcmConstantDao)getBean("rcmConstantDao"));
            uim.setCamFscDao((CamFscDao)getBean("camFscDao"));
            
            // Manage the attributes of User: line, region, agent, fsc
            RcmUserBean userBean = (RcmUserBean) session.getAttribute("userBean");
            this.manageUserBean(uim, userBean);
             

            session.setAttribute(sessionkey, uim);
        }
		
		return uim;
	}

}
