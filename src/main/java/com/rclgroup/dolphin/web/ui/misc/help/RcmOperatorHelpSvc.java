package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.cam.CamOperatorDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmOperatorHelpSvc extends RrcStandardHelpSvc{
    public RcmOperatorHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmOperatorHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmOperatorHelpUim";
        String helpScnName = "/RcmOperatorHelpScn.jsp?service=ui.misc.help.RcmOperatorHelpSvc";
        
        RrcStandardHelpUim uim = (RcmOperatorHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmOperatorHelpUim newOperatorHelpUim = new RcmOperatorHelpUim();
        
        newOperatorHelpUim.setCamOperatorDao((CamOperatorDao)getBean("camOperatorDao"));
        RrcStandardHelpUim newUim = newOperatorHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmOperatorHelpSvc][execute]: Finished");
    }
}
