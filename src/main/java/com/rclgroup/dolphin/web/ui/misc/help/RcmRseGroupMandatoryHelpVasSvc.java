package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeVasappsSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeVasappsUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmRseGroupMandatoryHelpVasSvc extends RrcStandardHelpOptimizeVasappsSvc {
    public RcmRseGroupMandatoryHelpVasSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmRseGroupMandatoryHelpVasSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeVasappsUim uim = (RcmRseGroupMandatoryHelpVasUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeVasappsSvc.INSTANCE_NAME_UIM_DEFAULT, RcmRseGroupMandatoryHelpVasUim.class);
        if (uim == null) {
            uim = new RcmRseGroupMandatoryHelpVasUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmRseGroupMandatoryHelpVasSvc][execute]: Finished");
    }
}
