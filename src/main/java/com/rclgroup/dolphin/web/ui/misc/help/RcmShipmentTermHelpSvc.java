package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmShipmentTermHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmShipmentTermHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmCurrencyHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
        
        RcmStandardHelpOptimizeUim uim = (RcmShipmentTermHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmShipmentTermHelpUim.class);
        
        if (uim == null) {
            uim = new RcmShipmentTermHelpUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmCurrencyHelpSvc][execute]: Finished");
    }
}
