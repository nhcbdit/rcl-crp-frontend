 /*-----------------------------------------------------------------------------------------------------------  
 RcmBillNoHelpUim.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Kitti Pongsirisakun 17/06/2009
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;

import com.rclgroup.dolphin.web.dao.far.FarBillingStatementAndInvoiceLinkageDao;

import java.util.List;

 public class RcmBillNoHelpUim extends RrcStandardHelpUim{

     private FarBillingStatementAndInvoiceLinkageDao farBillInvLinkDao;
     
     public void setFarBillingStatementAndInvoiceLinkageDao(FarBillingStatementAndInvoiceLinkageDao farBillInvLinkDao) {
         this.farBillInvLinkDao = farBillInvLinkDao;
     }

     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = farBillInvLinkDao.listForHelpScreen(find,search,wild);
        }
         return list;
     }
 }
