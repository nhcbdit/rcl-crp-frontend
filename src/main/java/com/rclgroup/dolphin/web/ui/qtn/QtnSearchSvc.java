package com.rclgroup.dolphin.web.ui.qtn;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;

public class QtnSearchSvc extends RrcStandardSvc{

    public QtnSearchSvc() {
        super();
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        
        try {
        	String strTarget = RcmConstant.QTN_PAGE_URL + "/include/screen/QtnSearchScn.jsp";
        	
            request.setAttribute("target", strTarget);
        }  catch (Exception exc) {
            exc.printStackTrace();
        }  
    }

}
