package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeVasappsUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RcmRseGroupMandatoryHelpVasUim extends RcmStandardHelpOptimizeVasappsUim {
           private String usrPermission;
        public RcmRseGroupMandatoryHelpVasUim() {
            super();
            
            // default sort
            this.setSortBy("GROUP_MANDATORY_CODE");
            this.setSortIn(RcmConstant.SORT_ASCENDING);
        }
        
        public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
         
        this.usrPermission = usrPermission;
        }
        
        public String findTitleNameShowByType(String type) {
        return "Group Mandatory";
        }
        
        public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        
        columnNameShow.put("GROUP_MANDATORY_CODE", new RcmColumnNameShowMod("Group Mandatory Code|50|left|STRING"));  
        
        return columnNameShow;
        }
        
        public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        
        arrReturnValue = new String[] {"GROUP_MANDATORY_CODE"};
        return arrReturnValue;
        }
        
        public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        sb.append(" SELECT DISTINCT GROUP_MANDATORY_CODE ");
        sb.append(" FROM CAM_GROUP_MANDATORY ");
        sb.append(" where 1=1 ");
        sb.append(" [and :columnName :conditionWild :columnFind] ");
        
        if (RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            sb.append(" AND RECORD_STATUS='A' ");
        }
        
       // sb.append(" GROUP BY GROUP_MANDATORY_CODE ");
        sb.append("[order by :sortBy :sortIn] ");
        
        return sb.toString();
        }
        
        public String getUsrPermission() {
            return usrPermission;
        }
        
        public void setUsrPermission(String usrPermission) {
            this.usrPermission = usrPermission;
        }
}
