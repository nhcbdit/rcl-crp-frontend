/*-----------------------------------------------------------------------------------------------------------  
RcmTosReasonHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Dhruv Parekh 17/05/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmTosReasonHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmTosReasonHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmTosReasonHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
        
        RcmStandardHelpOptimizeUim uim = (RcmTosReasonHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmTosReasonHelpUim.class);
        
        if (uim == null) {
            uim = new RcmTosReasonHelpUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmTosReasonHelpSvc][execute]: Finished");
    }
}
