 /*------------------------------------------------------
 RcmPortSurchargeHelpSvc.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Sopon Dee-udomvongsa 02/09/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.qtn.QtnPortSurchargeDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmPortSurchargeHelpSvc extends RrcStandardHelpSvc {
     public RcmPortSurchargeHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmPortSurchargeHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmPortSurchargeHelpUim";
         String helpScnName = "/RcmPortSurchargeHelpScn.jsp?service=ui.misc.help.RcmPortSurchargeHelpSvc";
         
         RrcStandardHelpUim uim = (RcmPortSurchargeHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmPortSurchargeHelpUim newPortSurchargeHelpUim = new RcmPortSurchargeHelpUim();
         
         newPortSurchargeHelpUim.setQtnPortSurchargeDao((QtnPortSurchargeDao)getBean("qtnPortSurchargeDao"));
         RrcStandardHelpUim newUim = newPortSurchargeHelpUim;
         
         helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
         System.out.println("[RcmPortSurchargeHelpSvc][execute]: Finished");
     }
 }