/*-----------------------------------------------------------------------------------------------------------  
RcmTosReasonHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Dhruv Parekh 17/05/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class RcmTosReasonHelpUim extends RcmStandardHelpOptimizeUim {
    private String usrPermission;
    
    public RcmTosReasonHelpUim() {
        super();
        
        this.setSortBy("REASON_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
        // get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
        
        this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Reason";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            columnNameShow.put("REASON_CODE", new RcmColumnNameShowMod("Reason Code|15|left|STRING"));
            columnNameShow.put("REASON_DESC", new RcmColumnNameShowMod("Description|40|left|STRING"));
            columnNameShow.put("RECORD_STATUS", new RcmColumnNameShowMod("Status|12|left|STRING"));
        }
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            arrReturnValue = new String[] {"REASON_CODE"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
         
        if (RcmConstant.GET_GENERAL.equals(type) || RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
            sb.append("SELECT ");
            sb.append(" REASON_CODE, REASON_DESC, RECORD_STATUS ");          
            sb.append("FROM VSS_REASON\n ");
            
            StringBuffer sbWhere = new StringBuffer("");
            if (RcmConstant.GET_WITH_ACTIVE_STATUS.equals(type)) {
                sbWhere.append(" and RECORD_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
            }
                        
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append("where "+sbWhere.substring(4));
                sb.append(" [and :columnName :conditionWild :columnFind] ");
            } else {
                sb.append("[where :columnName :conditionWild :columnFind] ");
            }
            
            sb.append("[order by :sortBy :sortIn] ");
        }
        return sb.toString();
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
    
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
