package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RcmUserBean;
import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.cam.CamAgentDao;
import com.rclgroup.dolphin.web.dao.cam.CamFscDao;
import com.rclgroup.dolphin.web.dao.dex.DexBlDao;
import com.rclgroup.dolphin.web.dao.ems.EmsControlZoneDao;
import com.rclgroup.dolphin.web.dao.ems.EmsGeographicalRegionDao;
import com.rclgroup.dolphin.web.dao.qtn.QtnPortSurchargeTypeDao;
import com.rclgroup.dolphin.web.dao.rcm.RcmConstantDao;
import com.rclgroup.dolphin.web.dao.sys.SysUserGroupDao;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmBlHelpSvc extends RrcStandardHelpSvc{
    public RcmBlHelpSvc() {
    }
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmBlHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmBlHelpUim";
        String helpScnName = "/RcmBlHelpScn.jsp?service=ui.misc.help.RcmBlHelpSvc";
        
        RrcStandardHelpUim uim = (RcmBlHelpUim)session.getAttribute(helpUiModInstanceName);

        RcmBlHelpUim newBlHelpUim = new RcmBlHelpUim();        
        newBlHelpUim.setDexBlDao((DexBlDao)getBean("dexBlDao"));
        
        
        RcmUserBean userBean = (RcmUserBean)session.getAttribute("userBean");
        String line = userBean.getFscLvl1();
        String region = userBean.getFscLvl2();
        String agent = userBean.getFscLvl3();
        String fsc = userBean.getFscCode();
        
//        String fscName = userBean.getFscName();
        //            String line = "E";
        //            String region = "*";
        //            String agent = "***";
        //            String fsc = "DMM";

        //            String line = "R";
        //            String region = "*";
        //            String agent = "***";
        //            String fsc = "***";

        //
        //            CONTROL_FSC="N";
        //            String line = "R";
        //            String region = "N";
        //            String agent = "TYO";
        //            String fsc = "NGO";
        //
        //            CONTROL_FSC="Y"
        //            String line = "R";
        //            String region = "S";
        //            String agent = "CEB";
        //            String fsc = "CEB";
        newBlHelpUim.setLineCodeOfUser(line);
        newBlHelpUim.setCamFscDao((CamFscDao)getBean("camFscDao"));
        if(region.equals("*")){ //check whether user is in line level
            newBlHelpUim.setLevel(RrcStandardUim.LEVEL_LINE,line,"","","");
            newBlHelpUim.setRegionCodeOfUser("");
            newBlHelpUim.setAgentCodeOfUser("");
            newBlHelpUim.setFscCodeOfUser("");
        }else if(agent.equals("***")){ //check whether user is in region level
            newBlHelpUim.setLevel(RrcStandardUim.LEVEL_REGION,line,region,"","");
            newBlHelpUim.setRegionCodeOfUser(region);
            newBlHelpUim.setAgentCodeOfUser("");
            newBlHelpUim.setFscCodeOfUser("");
        }else{ //check whether user is in agent level
            newBlHelpUim.setLevel(RrcStandardUim.LEVEL_AGENT,line,region,agent,"");
            newBlHelpUim.setRegionCodeOfUser(region);
            newBlHelpUim.setAgentCodeOfUser(agent);
            newBlHelpUim.setFscCodeOfUser(fsc);
        }
//        uim.setFscName(fscName);
        newBlHelpUim.setIsControlFsc(line,region,agent,fsc); 
        
        RrcStandardHelpUim newUim = newBlHelpUim;
    
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmBlHelpSvc][execute]: Finished");
    }
}
