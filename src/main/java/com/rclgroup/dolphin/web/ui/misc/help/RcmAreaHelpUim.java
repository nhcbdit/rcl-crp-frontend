/*-----------------------------------------------------------------------------------------------------------  
RcmAreaHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ems.EmsAreaDao;

import java.util.List;

 public class RcmAreaHelpUim extends RrcStandardHelpUim{
     private String regionCode;
     private EmsAreaDao emsAreaDao;
     
     public RcmAreaHelpUim(){
         regionCode = "";
     } 
     
    public void setEmsAreaDao(EmsAreaDao emsAreaDao) {
        this.emsAreaDao = emsAreaDao;
    }
    
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsAreaDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             regionCode = this.getRegionCode();
             list = emsAreaDao.listForHelpScreen(find, search, wild, regionCode, ACTIVE_STATUS);
         }
         return list;
     }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }
}
