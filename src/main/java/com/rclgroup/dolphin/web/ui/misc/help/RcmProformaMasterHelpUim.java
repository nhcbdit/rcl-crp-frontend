/*-----------------------------------------------------------------------------------------------------------  
RcmProformaMasterHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 02/06/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.vss.VssProformaMasterDao;
import com.rclgroup.dolphin.web.dao.vss.VssServiceProformaDao;

import java.util.List;


public class RcmProformaMasterHelpUim extends RrcStandardHelpUim{
    private VssProformaMasterDao vssProformaMasterDao;
    
    private String serviceCode;
         
    public RcmProformaMasterHelpUim() {
        super();
        serviceCode = "";
    }
     
    public void setVssProformaMasterDao(VssProformaMasterDao vssProformaMasterDao) {
        this.vssProformaMasterDao = vssProformaMasterDao;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if (type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)) {
            list = vssProformaMasterDao.listForHelpScreen(find, search, wild, RcmConstant.RECORD_STATUS_ACTIVE, this.serviceCode);
        }
        return list;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
}

