package com.rclgroup.dolphin.web.ui.crp;

import com.rclgroup.dolphin.web.common.RrcStandardUim;

public class CrpScheduleMaintenanceUim extends RrcStandardUim {
	 private String currentPort;

	public String getCurrentPort() {
		return currentPort;
	}

	public void setCurrentPort(String currentPort) {
		this.currentPort = currentPort;
	}
	 
	 
}
