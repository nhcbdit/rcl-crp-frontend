/*-----------------------------------------------------------------------------------------------------------  
RcmTosOperationHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Dhruv Parekh 15/05/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;

import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmTosOperationHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmTosOperationHelpSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmTosOperationHelp][execute]: Started");
        HttpSession session = request.getSession(false);
        
        RcmStandardHelpOptimizeUim uim = (RcmTosOperationHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmTosOperationHelpUim.class);
        
        if (uim == null) {
            uim = new RcmTosOperationHelpUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmTosOperationHelpSvc][execute]: Finished");
    }

}
