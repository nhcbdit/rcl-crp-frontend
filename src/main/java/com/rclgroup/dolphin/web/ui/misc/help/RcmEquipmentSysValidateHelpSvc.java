/*-----------------------------------------------------------------------------------------------------------  
RcmEquipmentSysValidateHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/04/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmEquipmentSysValidateHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmEquipmentSysValidateHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmEquipmentSysValidateHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
        
        RcmStandardHelpOptimizeUim uim = (RcmEquipmentSysValidateHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmEquipmentSysValidateHelpUim.class);
        if (uim == null) {
            uim = new RcmEquipmentSysValidateHelpUim();
        }
        
        this.doExecute(request, uim);
        System.out.println("[RcmEquipmentSysValidateHelpSvc][execute]: Finished");
    }
}
