/*-----------------------------------------------------------------------------------------------------------  
RcmVesselHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;

import com.rclgroup.dolphin.web.dao.cam.CamVesselDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmVesselHelpSvc extends RrcStandardHelpSvc {
    public RcmVesselHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmVesselHelpSvc][execute]: Started");        
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmVesselHelpUim";
        String helpScnName = "/RcmVesselHelpScn.jsp?service=ui.misc.help.RcmVesselHelpSvc";
        
        RrcStandardHelpUim uim = (RcmVesselHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmVesselHelpUim newVesselHelpUim = new RcmVesselHelpUim();
        
        newVesselHelpUim.setCamVesselDao((CamVesselDao)getBean("camVesselDao"));
        RrcStandardHelpUim newUim = newVesselHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmVesselHelpSvc][execute]: Finished");
    }
}


