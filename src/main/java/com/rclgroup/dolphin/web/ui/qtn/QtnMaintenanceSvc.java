package com.rclgroup.dolphin.web.ui.qtn;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;
import com.rclgroup.dolphin.web.util.RutString;

public class QtnMaintenanceSvc extends RrcStandardSvc{

    public QtnMaintenanceSvc() {
        super();
    }
	
public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        
        try {
        	String rclp = RutString.nullToStr(request.getParameter("rclp"));
        	
        	System.out.println("rclp :: "+rclp);
        	
        	String strTarget = RcmConstant.QTN_PAGE_URL + RcmConstant.QTN_INCLUDE_PAGE_URL + QtnConstant.QTN_PAGE_QUOTATION_NEW;
            
            request.setAttribute("target", strTarget);
        }  catch (Exception exc) {
            exc.printStackTrace();
        }  
        
       
        
    }
}
