/*-----------------------------------------------------------------------------------------------------------  
RcmServiceProformaHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.vss.VssServiceProformaDao;

import java.util.List;


public class RcmServiceProformaHelpUim extends RrcStandardHelpUim{
    private VssServiceProformaDao vssServiceProformaDao;
     
    public RcmServiceProformaHelpUim(){
    }
     
    public void setVssServiceProformaDao(VssServiceProformaDao vssServiceProformaDao) {
        this.vssServiceProformaDao = vssServiceProformaDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if (type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)) {
            list = vssServiceProformaDao.listForHelpScreen(find, search, wild, RcmConstant.RECORD_STATUS_ACTIVE);
        }
        return list;
    }
}

