/*-----------------------------------------------------------------------------------------------------------  
RcmEquipmentSysValidateHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/04/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


public class RcmEquipmentSysValidateHelpUim extends RcmStandardHelpOptimizeUim {
    private String regionCode;
    private String errorType;
    
    public RcmEquipmentSysValidateHelpUim() {
        super();
        
        // default sort
        this.setSortBy("ERROR_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String regionCode = RutString.nullToStr(request.getParameter("regionCode"));
        String errorType = RutString.nullToStr(request.getParameter("errorType"));
        
        this.regionCode = regionCode;
        this.errorType = errorType;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Equipment System Validate";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        if (RcmConstant.GET_GENERAL.equals(type)) {
            if (!"*".equals(regionCode)) {
                columnNameShow.put("REGION_CODE", new RcmColumnNameShowMod("Region Code|10|left|STRING"));
            }
            columnNameShow.put("ERROR_CODE", new RcmColumnNameShowMod("Error Code|10|left|STRING"));
            columnNameShow.put("ERROR_DESCRIPTION", new RcmColumnNameShowMod("Error Description|40|left|STRING"));
            columnNameShow.put("ERROR_TYPE", new RcmColumnNameShowMod("Error Type|10|left|STRING"));
            columnNameShow.put("STATUS", new RcmColumnNameShowMod("Status|7|left|STRING"));
        }
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"ERROR_CODE", "ERROR_DESCRIPTION"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        
        if (RcmConstant.GET_GENERAL.equals(type)) {
            if ("*".equals(regionCode)) {
                sb.append("select distinct '*' as REGION_CODE ");
            } else {
                sb.append("select REGION_CODE ");    
            }
            sb.append("    ,ERROR_CODE ");
            sb.append("    ,ERROR_DESCRIPTION ");
            sb.append("    ,ERROR_TYPE ");
            sb.append("    ,STATUS ");
            sb.append("from VR_EMS_EQ_SYS_VALIDATION ");
            sb.append("where STATUS = 'A' ");
            
            if (regionCode != null && !"".equals(regionCode) && !"*".equals(regionCode)) {
                sb.append(" and REGION_CODE = '" + regionCode + "' ");
            }
            if (errorType != null && !"".equals(errorType)) {
                sb.append(" and ERROR_TYPE = '" + errorType + "' ");
            }
            
            sb.append(" [and :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");
        }
        return sb.toString();
    }
    //end: implement function

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
}
