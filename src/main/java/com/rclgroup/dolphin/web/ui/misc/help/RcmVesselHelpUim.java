/*-----------------------------------------------------------------------------------------------------------  
RcmVesselHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.cam.CamVesselDao;

import java.util.List;

public class RcmVesselHelpUim extends RrcStandardHelpUim{
    private CamVesselDao camVesselDao;
     
    public RcmVesselHelpUim(){
    }
     
    public void setCamVesselDao(CamVesselDao camVesselDao) {
        this.camVesselDao = camVesselDao;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
            final String ACTIVE_STATUS = "A";
            if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                
                list = camVesselDao.listForHelpScreen(find,search,wild,ACTIVE_STATUS);
            }else if(type.equalsIgnoreCase("GET_VESSEL_OPR")){
                
                list = camVesselDao.listForHelpScreenVesselOpr(find,search,wild,ACTIVE_STATUS);
            }
        return list;
    }
}
