package com.rclgroup.dolphin.web.ui.crp;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardSvc;

public class CrpGlobalSetupSvc extends RrcStandardSvc{

	public static final String CRP_PAGE_QUOTATION_NEW = "/CrpGlobalSetupScn.jsp";
	
	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context)
			throws Exception {
		String strTarget = RcmConstant.CRP_PAGE_URL + CRP_PAGE_QUOTATION_NEW;
		
		request.setAttribute("target", strTarget);
	}

}
