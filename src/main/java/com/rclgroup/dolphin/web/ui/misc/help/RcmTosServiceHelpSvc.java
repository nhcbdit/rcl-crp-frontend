package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.tos.TosServiceDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmTosServiceHelpSvc extends RrcStandardHelpSvc {
    public RcmTosServiceHelpSvc() {
    }
     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmTosServiceHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmTosServiceHelpUim";
         String helpScnName = "/RcmTosServiceHelpScn.jsp?service=ui.misc.help.RcmTosServiceHelpSvc";
         
         RrcStandardHelpUim uim = (RcmTosServiceHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmTosServiceHelpUim newTosServiceHelpUim = new RcmTosServiceHelpUim();         
        
         newTosServiceHelpUim.setTosServiceDao((TosServiceDao)getBean("tosServiceDao"));
         RrcStandardHelpUim newUim = newTosServiceHelpUim;       
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmTosServiceHelpSvc][execute]: Finished");
     }
    
}
