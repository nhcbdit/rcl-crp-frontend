/*-----------------------------------------------------------------------------------------------------------  
RcmZoneHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ems.EmsZoneDao;

import java.util.List;

 public class RcmZoneHelpUim extends RrcStandardHelpUim{
     private String regionCode;
     private String areaCode;
     private String controlZoneCode;
     private EmsZoneDao emsZoneDao;
     
     public RcmZoneHelpUim(){
     }
     public void setEmsZoneDao(EmsZoneDao emsZoneDao) {
         this.emsZoneDao = emsZoneDao;
     }
     
     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsZoneDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             regionCode = this.getRegionCode();
             areaCode = this.getAreaCode();             
             controlZoneCode = this.getControlZoneCode();
             list = emsZoneDao.listForHelpScreen(find, search, wild, regionCode, areaCode, controlZoneCode, ACTIVE_STATUS);
         }
         return list;
     }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setControlZoneCode(String controlZoneCode) {
        this.controlZoneCode = controlZoneCode;
    }

    public String getControlZoneCode() {
        return controlZoneCode;
    }
}
