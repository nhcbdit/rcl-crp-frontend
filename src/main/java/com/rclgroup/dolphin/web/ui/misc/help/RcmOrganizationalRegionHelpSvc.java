/*-----------------------------------------------------------------------------------------------------------  
RcmOrganizationalRegionHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;

import com.rclgroup.dolphin.web.dao.cam.CamOrganizationalRegionDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmOrganizationalRegionHelpSvc extends RrcStandardHelpSvc {
    public RcmOrganizationalRegionHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmOrganizationalRegionHelpSvc][execute]: Started");
        HttpSession session=request.getSession(false);
        String helpUiModInstanceName = "rcmOrganizationalRegionHelpUim";
        String helpScnName = "/RcmOrganizationalRegionHelpScn.jsp?service=ui.misc.help.RcmOrganizationalRegionHelpSvc";
        
        RrcStandardHelpUim uim = (RcmOrganizationalRegionHelpUim)session.getAttribute(helpUiModInstanceName);
        RcmOrganizationalRegionHelpUim newOrganizationalRegionHelpUim = new RcmOrganizationalRegionHelpUim();
        
        newOrganizationalRegionHelpUim.setCamOrganizationalRegionDao((CamOrganizationalRegionDao)getBean("camOrganizationalRegionDao"));
        RrcStandardHelpUim newUim = newOrganizationalRegionHelpUim;
        
        helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
        System.out.println("[RcmOrganizationalRegionHelpSvc][execute]: Finished");
    }
}
