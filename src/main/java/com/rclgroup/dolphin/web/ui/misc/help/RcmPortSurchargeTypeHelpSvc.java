 /*------------------------------------------------------
 RcmPortSurchargeTypeHelpSvc.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
  Author Sopon Dee-udomvongsa 02/09/2008
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.qtn.QtnPortSurchargeTypeDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmPortSurchargeTypeHelpSvc extends RrcStandardHelpSvc {
     public RcmPortSurchargeTypeHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmPortSurchargeTypeHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmPortSurchargeTypeHelpUim";
         String helpScnName = "/RcmPortSurchargeTypeHelpScn.jsp?service=ui.misc.help.RcmPortSurchargeTypeHelpSvc";
         
         RrcStandardHelpUim uim = (RcmPortSurchargeTypeHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmPortSurchargeTypeHelpUim newPortSurchargeTypeHelpUim = new RcmPortSurchargeTypeHelpUim();
         
         newPortSurchargeTypeHelpUim.setQtnPortSurchargeTypeDao((QtnPortSurchargeTypeDao)getBean("qtnPortSurchargeTypeDao"));
         RrcStandardHelpUim newUim = newPortSurchargeTypeHelpUim;
         
         helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
         System.out.println("[RcmPortSurchargeTypeHelpSvc][execute]: Finished");
     }
 }