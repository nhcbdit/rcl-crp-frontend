 /*-----------------------------------------------------------------------------------------------------------  
 RcmLessorHelpUim.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 03/07/2008
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.ui.misc.help;

 import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
 import com.rclgroup.dolphin.web.common.RrcStandardUim;
 import com.rclgroup.dolphin.web.dao.ems.EmsLessorDao;

 import java.util.List;

 public class RcmLessorHelpUim extends RrcStandardHelpUim{

     private EmsLessorDao emsLessorDao;
     
     public void setEmsLessorDao(EmsLessorDao emsLessorDao) {
         this.emsLessorDao = emsLessorDao;
     }

     public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsLessorDao.listForHelpScreen(find,search,wild);
        }
         return list;
     }
 }
