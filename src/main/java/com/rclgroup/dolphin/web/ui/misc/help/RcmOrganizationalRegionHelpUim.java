/*-----------------------------------------------------------------------------------------------------------  
RcmOrganizationalRegionHelpUim.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 05/11/07
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.cam.CamOrganizationalRegionDao;

import java.util.List;

public class RcmOrganizationalRegionHelpUim extends RrcStandardHelpUim{

    private CamOrganizationalRegionDao camOrganizationalRegionDao;
    
    public void setCamOrganizationalRegionDao(CamOrganizationalRegionDao camOrganizationalRegionDao) {
        this.camOrganizationalRegionDao = camOrganizationalRegionDao;
    }

    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
            list = camOrganizationalRegionDao.listForHelpScreen(find,search,wild);
        }else if(type.equals(RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS)) {
            final String ACTIVE_STATUS = "A";//Active
            list = camOrganizationalRegionDao.listForHelpScreenWithUserLevel(find,search,wild,this.getLineCodeOfUser(),ACTIVE_STATUS);
        }
        return list;
    }
}
