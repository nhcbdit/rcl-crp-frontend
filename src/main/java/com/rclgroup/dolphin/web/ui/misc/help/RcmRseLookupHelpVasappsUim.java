package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeVasappsUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class RcmRseLookupHelpVasappsUim extends RcmStandardHelpOptimizeVasappsUim {
    private String usrPermission;
    
    public RcmRseLookupHelpVasappsUim() {
        super();
        
        // default sort
        this.setSortBy("LOOKUP_NAME");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
         
        this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
        return "RSE Lookup Name";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        
        columnNameShow.put("LOOKUP_CODE", new RcmColumnNameShowMod("Lookup Code|20|left|STRING"));
        columnNameShow.put("LOOKUP_NAME", new RcmColumnNameShowMod("Lookup Name|20|left|STRING"));
        columnNameShow.put("LOOKUP_DESCR", new RcmColumnNameShowMod("Description|30|left|STRING"));
        columnNameShow.put("LOOKUP_TYPE", new RcmColumnNameShowMod("Lookup Type|15|left|STRING"));
        columnNameShow.put("LOOKUP_RETUNS", new RcmColumnNameShowMod("Lookup Returns|75|left|STRING"));
        
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        
        arrReturnValue = new String[] {"LOOKUP_NAME", "LOOKUP_TYPE"};
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        sb.append(" select LOOKUP_CODE");
        sb.append(" ,LOOKUP_NAME ");
        sb.append(" ,LOOKUP_DESCR");
        sb.append(" ,LOOKUP_TYPE");
        sb.append(" ,LOOKUP_RETUNS");
        sb.append(" from CAM_RSE_LOOKUP_MASTER ");
        sb.append("[where :columnName :conditionWild :columnFind] ");
        sb.append("[order by :sortBy :sortIn] ");
        
        return sb.toString();
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
     
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
