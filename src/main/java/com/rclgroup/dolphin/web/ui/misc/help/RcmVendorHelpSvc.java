package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.vms.VmsVendorDao;
import com.rclgroup.dolphin.web.util.RutString;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmVendorHelpSvc extends RrcStandardHelpSvc {
    public RcmVendorHelpSvc() {
    }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmVendorHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmVendorHelpUim";
         String helpScnName = "/RcmVendorHelpScn.jsp?service=ui.misc.help.RcmVendorHelpSvc";
         
         RrcStandardHelpUim uim = (RcmVendorHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmVendorHelpUim newVendorHelpUim = new RcmVendorHelpUim();         
        
         newVendorHelpUim.setVmsVendorDao((VmsVendorDao)getBean("vmsVendorDao"));
         RrcStandardHelpUim newUim = newVendorHelpUim;       
         
         helpProcess(request, helpUiModInstanceName, helpScnName, uim, newUim);
         System.out.println("[RcmVendorHelpSvc][execute]: Finished");
     }
    
}
