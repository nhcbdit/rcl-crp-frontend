 /*------------------------------------------------------
 RcmEquipmentTypeHelpUim.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Manop Wanngam 15/10/07
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ems.EmsEquipmentTypeDao;

import java.util.List;


public class RcmEquipmentTypeHelpUim extends RrcStandardHelpUim{
    private EmsEquipmentTypeDao emsEquipmentTypeDao;
     
    public void setEmsEquipmentTypeDao(EmsEquipmentTypeDao emsEquipmentTypeDao) {
        this.emsEquipmentTypeDao = emsEquipmentTypeDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
         List list = null;
         if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
             list = emsEquipmentTypeDao.listForHelpScreen(find, search, wild);
         }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
             final String ACTIVE_STATUS = "A";
             list = emsEquipmentTypeDao.listForHelpScreen(find, search, wild, ACTIVE_STATUS);
         }
         return list;
     }

}
