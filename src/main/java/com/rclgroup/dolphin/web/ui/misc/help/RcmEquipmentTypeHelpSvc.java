 /*------------------------------------------------------
 RcmEquipmentTypeHelpSvc.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Manop Wanngam 15/10/07
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.dao.ems.EmsEquipmentTypeDao;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmEquipmentTypeHelpSvc extends RrcStandardHelpSvc {
     public RcmEquipmentTypeHelpSvc() {
     }

     public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
         System.out.println("[RcmEquipmentTypeHelpSvc][execute]: Started");
         HttpSession session=request.getSession(false);
         String helpUiModInstanceName = "rcmEquipmentTypeHelpUim";
         String helpScnName = "/RcmEquipmentTypeHelpScn.jsp?service=ui.misc.help.RcmEquipmentTypeHelpSvc";
         
         RrcStandardHelpUim uim = (RcmEquipmentTypeHelpUim)session.getAttribute(helpUiModInstanceName);
         RcmEquipmentTypeHelpUim newEquipmentTypeHelpUim = new RcmEquipmentTypeHelpUim();
         
         newEquipmentTypeHelpUim.setEmsEquipmentTypeDao((EmsEquipmentTypeDao)getBean("emsEquipmentTypeDao"));
         RrcStandardHelpUim newUim = newEquipmentTypeHelpUim;
         
         helpProcess(request,helpUiModInstanceName,helpScnName,uim,newUim);
         System.out.println("[RcmEquipmentTypeHelpSvc][execute]: Finished");
     }
 }