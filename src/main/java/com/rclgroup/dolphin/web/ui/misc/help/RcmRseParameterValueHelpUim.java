package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class RcmRseParameterValueHelpUim extends RcmStandardHelpOptimizeUim {
    private String usrPermission;
    public RcmRseParameterValueHelpUim() {
        super();
        
        // default sort
        this.setSortBy("PARAMETER_VALUE_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
    //get request parameter
    String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
     
    this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
    return "Parameter Value";
    }
    
    public HashMap findColumnNameShowByType(String type) {
    HashMap columnNameShow = new HashMap();
        
        columnNameShow.put("PARAMETER_VALUE_CODE", new RcmColumnNameShowMod("Parameter Value Code|20|left|STRING"));  
        columnNameShow.put("PARAMETER_VALUE_NAME", new RcmColumnNameShowMod("Parameter Value Name|20|left|STRING"));  
        columnNameShow.put("STATIC_SQL_FLAG", new RcmColumnNameShowMod("Parameter Value Type|20|left|STRING"));
        columnNameShow.put("STATIC_USER_PARAMETER", new RcmColumnNameShowMod("Static User Paramer|20|left|STRING"));  
        columnNameShow.put("SQL_STATEMENT", new RcmColumnNameShowMod("SQL Statement|20|left|STRING"));  
    
    
    return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
    String[] arrReturnValue = null;
    
    arrReturnValue = new String[] {"PARAMETER_VALUE_CODE"};
    return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
    
        StringBuffer sb = new StringBuffer("");
    
   
    sb.append(" SELECT ':'||PARAMETER_VALUE_CODE PARAMETER_VALUE_CODE, PARAMETER_VALUE_NAME , STATIC_SQL_FLAG , STATIC_USER_PARAMETER , SUBSTR(SQL_STATEMENT,0,100) SQL_STATEMENT ");
    sb.append(" FROM CAM_SERVICE_PARAMETER_VALUE ");
    sb.append(" where RECORD_STATUS ='A' ");
    sb.append(" [and UPPER(:columnName) :conditionWild :columnFind] ");
    sb.append(" [order by :sortBy :sortIn] ");
    
    

   /*  sb.append(" SELECT PARAMETER_VALUE_CODE ");
     sb.append(" ,STATIC_SQL_FLAG           ");
     sb.append(" ,CASE STATIC_SQL_FLAG ");
     sb.append("  WHEN 'SQL' THEN  SUBSTR(SQL_STATEMENT,0,100) ");
     sb.append("  WHEN 'STT' THEN  STATIC_USER_PARAMETER ");
     sb.append("   END DETAIL ");
     sb.append("  FROM CAM_SERVICE_PARAMETER_VALUE ");
     sb.append(" WHERE RECORD_STATUS ='A' ");
     sb.append(" [and UPPER(:columnName) :conditionWild :columnFind] ");
     sb.append(" [order by :sortBy :sortIn] "); */
    return sb.toString();
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
    
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}

