/*-----------------------------------------------------------------------------------------------------------  
RcmSalesmanHelpSvc.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2009
-------------------------------------------------------------------------------------------------------------
Author Leena Babu 18/08/11
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class RcmSalesmanHelpSvc extends RrcStandardHelpOptimizeSvc {
    public RcmSalesmanHelpSvc() {
    }

    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmSalesmanHelpSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeUim uim = (RcmSalesmanHelpUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeSvc.INSTANCE_NAME_UIM_DEFAULT, RcmSalesmanHelpUim.class);
        if (uim == null) {
            uim = new RcmSalesmanHelpUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmSalesmanHelpSvc][execute]: Finished");
    }
}
