package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

public class RcmRseVerifyNameHelpUim extends RcmStandardHelpOptimizeUim {
    private String usrPermission;
    
    public RcmRseVerifyNameHelpUim() {
        super();
        
        // default sort
        this.setSortBy("PARAMETER_VERIFY_NAME");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
         
        this.usrPermission = usrPermission;
    }
    
    public String findTitleNameShowByType(String type) {
        return "Parameter Verify Name";
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        
        columnNameShow.put("PARAMETER_VERIFY_CODE", new RcmColumnNameShowMod("Verify Code|20|left|STRING"));
        columnNameShow.put("PARAMETER_VERIFY_NAME", new RcmColumnNameShowMod("Verify Name|20|left|STRING"));
        columnNameShow.put("PARAMETER_VERIFY_TYPE", new RcmColumnNameShowMod("Verify Type|30|left|STRING"));
        columnNameShow.put("RECORD_STATUS", new RcmColumnNameShowMod("Status|15|left|STRING"));        
        
        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        
        arrReturnValue = new String[] {"PARAMETER_VERIFY_CODE", "PK_CAM_PARAMETER_VERIFY_ID"};
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        sb.append(" select PK_CAM_PARAMETER_VERIFY_ID");
        sb.append(" ,PARAMETER_VERIFY_CODE ");
        sb.append(" ,PARAMETER_VERIFY_NAME");
        sb.append(" ,PARAMETER_VERIFY_TYPE");
        sb.append(" ,RECORD_STATUS");
        sb.append(" from CAM_SERVICE_PARAMETER_VERIFY ");
        sb.append("[where :columnName :conditionWild :columnFind] ");
        sb.append("[order by :sortBy :sortIn] ");
        
        return sb.toString();
    }
    
    public String getUsrPermission() {
        return usrPermission;
    }
     
    public void setUsrPermission(String usrPermission) {
        this.usrPermission = usrPermission;
    }
}
