  /*-----------------------------------------------------------------------------------------------------------  
  RcmLessorHelpUim.java
  ------------------------------------------------------------------------------------------------------------- 
  Copyright RCL Public Co., Ltd. 2007 
  -------------------------------------------------------------------------------------------------------------
  Author Natcha Phuukarat 24/04/14
  - Change Log ------------------------------------------------------------------------------------------------  
  ## DD/MM/YY -User-     -TaskRef-      -Short Description  
  -----------------------------------------------------------------------------------------------------------*/


package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.model.rcm.RcmColumnNameShowMod;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;


public class RcmLesseeCodeHelpUim extends RcmStandardHelpOptimizeUim{
    /*private String usrPermission;
    private String fscCode;
    private String regionCode;
    private String areaCode;
    private String controlZone;
    private String controlZoneCode;
    private String zoneCode;
    private String point;*/
    
    private String LessorCode;
    private String LessorName;
    private String ShippingLine;
    private String VendorCode;
    private String CustomerCode;
    private String LessorStatus;
    
    public RcmLesseeCodeHelpUim() {
        super();
        
        // default sort
        this.setSortBy("LESSOR_CODE");
        this.setSortIn(RcmConstant.SORT_ASCENDING);
    }
    
    //begin: implement function
    public void manageRequestParameter(HttpServletRequest request) {
        //get request parameter
        /*String usrPermission = RutString.nullToStr(request.getParameter("usrPerm")).toUpperCase().trim();
        String fscCode = RutString.nullToStr(request.getParameter("hidFsc")).toUpperCase().trim();
        String regionCode = RutString.nullToStr(request.getParameter("txtRegion")).toUpperCase().trim();
        String areaCode = RutString.nullToStr(request.getParameter("txtArea")).toUpperCase().trim();
        String controlZone = RutString.nullToStr(request.getParameter("hidControlZone")).toUpperCase().trim();
        String controlZoneCode = RutString.nullToStr(request.getParameter("txtControlZone")).toUpperCase().trim();
        String zoneCode = RutString.nullToStr(request.getParameter("txtZone")).toUpperCase().trim();
        String point = RutString.nullToStr(request.getParameter("txtPoint")).toUpperCase().trim();*/
	String LessorCode = RutString.nullToStr(request.getParameter("LESSOR_CODE")).toUpperCase().trim();
        String LessorName = RutString.nullToStr(request.getParameter("LESSOR_NAME")).toUpperCase().trim();
        String ShippingLine = RutString.nullToStr(request.getParameter("SHIPPING_LINE")).toUpperCase().trim();
        String VendorCode = RutString.nullToStr(request.getParameter("VENDOR_CODE")).toUpperCase().trim();
        String CustomerCode = RutString.nullToStr(request.getParameter("CUSTOMER_CODE")).toUpperCase().trim();
        String LessorStatus = RutString.nullToStr(request.getParameter("STATUS")).toUpperCase().trim();
        
        /*this.usrPermission = usrPermission;
        this.fscCode = fscCode;
        this.regionCode = regionCode;
        this.areaCode = areaCode;
        this.controlZone = controlZone;
        this.controlZoneCode = controlZoneCode;
        this.zoneCode = zoneCode;
        this.point = point;*/       
        this.LessorCode = LessorCode;
        this.LessorName = LessorName;
        this.ShippingLine = ShippingLine;
        this.VendorCode = VendorCode;
        this.CustomerCode = CustomerCode;
        this.LessorStatus = LessorStatus;
    }
    
    public String findTitleNameShowByType(String type) {
        String titleName = null;
        
        /*if (RcmConstant.GET_GENERAL.equals(type) || "GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            titleName = "Terminal/Depot";
        }else if(RcmConstant.GET_TERMINAL_FILTERED.equals(type)){
            titleName = "Port/Terminal";
        }*/

        if (RcmConstant.GET_GENERAL.equals(type)) {
            titleName = "Lessor Code";
        }
        return titleName;
    }
    
    public HashMap findColumnNameShowByType(String type) {
        HashMap columnNameShow = new HashMap();
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        /*if (RcmConstant.GET_GENERAL.equals(type) || "GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            columnNameShow.put("TERMINAL_CODE", new RcmColumnNameShowMod("Terminal Code|15|left|STRING"));
            columnNameShow.put("TERMINAL_NAME", new RcmColumnNameShowMod("Terminal Name|25|left|STRING"));
            columnNameShow.put("POINT_CODE", new RcmColumnNameShowMod("Point Code|10|left|STRING"));
            columnNameShow.put("FLAG", new RcmColumnNameShowMod("Flag|7|left|STRING"));
            columnNameShow.put("ZONE", new RcmColumnNameShowMod("Zone|7|left|STRING"));
            columnNameShow.put("CONTROL_ZONE", new RcmColumnNameShowMod("Control Zone|15|left|STRING"));
            columnNameShow.put("AREA", new RcmColumnNameShowMod("Area|7|left|STRING"));
            columnNameShow.put("REGION", new RcmColumnNameShowMod("Region|10|left|STRING"));
            columnNameShow.put("STATUS", new RcmColumnNameShowMod("Status|15|left|STRING"));
        } else if(RcmConstant.GET_TERMINAL_FILTERED.equals(type)){
            columnNameShow.put("TERMINAL_CODE", new RcmColumnNameShowMod("Terminal Code|15|left|STRING"));
            columnNameShow.put("TERMINAL_NAME", new RcmColumnNameShowMod("Terminal Name|25|left|STRING"));
            columnNameShow.put("POINT_CODE", new RcmColumnNameShowMod("Port Code|10|left|STRING"));
            columnNameShow.put("STATUS", new RcmColumnNameShowMod("Status|15|left|STRING"));
        }*/
        if (RcmConstant.GET_GENERAL.equals(type)) {
            columnNameShow.put("LESSOR_CODE", new RcmColumnNameShowMod("Lessor Code|5|left|STRING"));
            columnNameShow.put("LESSOR_NAME", new RcmColumnNameShowMod("Lessor Name|30|left|STRING"));
            columnNameShow.put("SHIPPING_LINE", new RcmColumnNameShowMod("Shipping Line|5|left|STRING"));
            columnNameShow.put("VENDOR_CODE", new RcmColumnNameShowMod("Vendor Code|10|left|STRING"));
            columnNameShow.put("CUSTOMER_CODE", new RcmColumnNameShowMod("Customer Code|10|left|STRING"));
            columnNameShow.put("STATUS", new RcmColumnNameShowMod("Status|10|left|STRING"));
        }

        return columnNameShow;
    }
    
    public String[] findReturnValueByType(String type) {
        String[] arrReturnValue = null;
        // columenNameShow = Column Text | Size | Align | Data Type [| Data Format Type | Data Format]
        /*if (RcmConstant.GET_GENERAL.equals(type) || "GET_GENERAL_WITH_POINT".equals(type) || "GET_WITH_FSC".equals(type) || "GET_WITH_CONTROL_ZONE".equals(type)) {
            arrReturnValue = new String[] {"TERMINAL_CODE", "POINT_CODE", "ZONE", "CONTROL_ZONE", "AREA", "REGION"};
        } else if(RcmConstant.GET_TERMINAL_FILTERED.equals(type)){
            arrReturnValue = new String[] {"TERMINAL_CODE", "POINT_CODE"};
        }*/
		if (RcmConstant.GET_GENERAL.equals(type)) {
            arrReturnValue = new String[] {"LESSOR_CODE", "LESSOR_NAME", "SHIPPING_LINE", "VENDOR_CODE", "CUSTOMER_CODE", "STATUS"};
        }
        return arrReturnValue;
    }
    
    public String findSqlStatementByType(String type) {
        StringBuffer sb = new StringBuffer("");
        if (RcmConstant.GET_GENERAL.equals(type)) {
            /*sb.append("select distinct TERMINAL_CODE ");
            sb.append(" ,TERMINAL_NAME ");
            sb.append(" ,POINT_CODE ");   
            sb.append(" ,FLAG ");
            sb.append(" ,ZONE ");
            sb.append(" ,CONTROL_ZONE ");            
            sb.append(" ,AREA ");
            sb.append(" ,REGION ");
            sb.append(" ,STATUS "); 
            sb.append("from VR_EMS_TERMINAL ");
            sb.append("where STATUS = 'Active' ");*/
            sb.append("SELECT LESSOR_CODE ");
            sb.append("      ,LESSOR_NAME ");
            sb.append("      ,SHIPPING_LINE ");
            sb.append("      ,VENDOR_CODE ");
            sb.append("      ,CUSTOMER_CODE ");
            sb.append("      ,STATUS "); 
            sb.append("FROM VR_EMS_LESSOR ");
            
            
            /*StringBuffer sbWhere = new StringBuffer("");
            if (usrPermission != null && !"".equals(usrPermission)) {
                sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_TERMINAL('"+usrPermission+"', TERMINAL_CODE) = '"+RcmConstant.FLAG_YES+"' ");
            }
            if (regionCode != null && !"".equals(regionCode)) {
                sbWhere.append(" and REGION = '"+regionCode+"' ");
            }
            if (areaCode != null && !"".equals(areaCode)) {
                sbWhere.append(" and AREA = '"+areaCode+"' ");
            }
            if (controlZoneCode != null && !"".equals(controlZoneCode)) {
                sbWhere.append(" and CONTROL_ZONE = '"+controlZoneCode+"' ");
            }
            if (zoneCode != null && !"".equals(zoneCode)) {
                sbWhere.append(" and ZONE = '"+zoneCode+"' ");
            }
            if (point != null && !"".equals(point)) {
                sbWhere.append(" and POINT_CODE = '"+point+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                sb.append(" and "+sbWhere.substring(4));
            }*/
            
            sb.append("[and :columnName :conditionWild :columnFind] ");
            sb.append("[order by :sortBy :sortIn] ");

            System.out.println("[RcmLessorHelp][listForHelpScreen]: SQL = " + sb.toString());
            System.out.println("[RcmLessorHelp][listForHelpScreen]: Finished");
        
        }
        
        System.out.println(sb.toString());
        
        return sb.toString();
    }
    //end: implement function
    
    public String getLessorCode() {
        return LessorCode;
    }

    public void setLessorCode(String LessorCode) {
        this.LessorCode = LessorCode;
    }

    public String getLessorName() {
        return LessorName;
    }

    public void setLessorName(String LessorName) {
        this.LessorName = LessorName;
    }

    public String getShippingLine() {
        return ShippingLine;
    }

    public void setShippingLine(String ShippingLine) {
        this.ShippingLine = ShippingLine;
    }

    public String getVendorCode() {
        return VendorCode;
    }

    public void setVendorCode(String VendorCode) {
        this.VendorCode = VendorCode;
    }

    public String getCustomerCode() {
        return CustomerCode;
    }

    public void setCustomerCode(String CustomerCode) {
        this.CustomerCode = CustomerCode;
    }

    public String getLessorStatus() {
        return LessorStatus;
    }

    public void setLessorStatus(String LessorStatus) {
        this.LessorStatus = LessorStatus;
    }
}
