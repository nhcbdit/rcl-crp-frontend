package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeSvc;
import com.rclgroup.dolphin.web.common.RrcStandardHelpOptimizeVasappsSvc;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeUim;
import com.rclgroup.dolphin.web.ui.misc.RcmStandardHelpOptimizeVasappsUim;
import com.rclgroup.dolphin.web.util.RutRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RcmRseLookupHelpVasappsSvc extends RrcStandardHelpOptimizeVasappsSvc {
    public RcmRseLookupHelpVasappsSvc() {
    }
    
    public void execute(HttpServletRequest request, HttpServletResponse response, ServletContext context) throws Exception {
        System.out.println("[RcmRseLookupHelpVasappsSvc][execute]: Started");
        HttpSession session = request.getSession(false);
         
        RcmStandardHelpOptimizeVasappsUim uim = (RcmRseLookupHelpVasappsUim) RutRequest.getSessionObject(session, RrcStandardHelpOptimizeVasappsSvc.INSTANCE_NAME_UIM_DEFAULT, RcmRseLookupHelpVasappsUim.class);
        if (uim == null) {
            uim = new RcmRseLookupHelpVasappsUim();
        }
         
        this.doExecute(request, uim);
        System.out.println("[RcmRseLookupHelpVasappsSvc][execute]: Finished");
    }
}
