package com.rclgroup.dolphin.web.ui.misc.help;

import com.rclgroup.dolphin.web.common.RrcStandardHelpUim;
import com.rclgroup.dolphin.web.common.RrcStandardUim;
import com.rclgroup.dolphin.web.dao.ijs.IjsReasonDao;

import java.util.List;

public class RcmReasonHelpUim extends RrcStandardHelpUim{

    private IjsReasonDao ijsReasonDao;
    
    public void setIjsReasonDao(IjsReasonDao ijsReasonDao) {
        this.ijsReasonDao = ijsReasonDao;
    }
    
    public List getListForHelpScreen(String find, String search, String wild, String type) {
        List list = null;
        if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){        
            list = ijsReasonDao.listForHelpScreen(find, search, wild);
        }
        return list;
    }
    
}
