 /*-----------------------------------------------------------------------------------------------------------  
 DimBLStatusReportJdbcDao.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Kitti Pongsirisakun  17/02/10
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description  
 -----------------------------------------------------------------------------------------------------------*/
 package com.rclgroup.dolphin.web.dao.dim;

 import com.rclgroup.dolphin.web.common.RrcStandardDao;
 import com.rclgroup.dolphin.web.util.RutDatabase;
 import java.sql.Types;
 import java.util.HashMap;
 import java.util.Map;
 import org.springframework.dao.DataAccessException;
 import org.springframework.jdbc.core.JdbcTemplate;
 import org.springframework.jdbc.core.SqlOutParameter;
 import org.springframework.jdbc.core.SqlParameter;
 import org.springframework.jdbc.object.StoredProcedure;
 import org.springframework.jdbc.core.SqlInOutParameter;

 public class DimBLStatusReportJdbcDao extends RrcStandardDao implements DimBLStatusReportDao{
     private InsertStoreProcedure insertStoreProcedure;
     protected void initDao() throws Exception {
         super.initDao();
         insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
     }
     public DimBLStatusReportJdbcDao() {
     }
     protected class InsertStoreProcedure extends StoredProcedure {
     private static final String STORED_PROCEDURE_NAME = "PCR_DIM_STATUSBL_AND_INV.PR_MAIN_DIM_STATUSBL_AND_INV";
     protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);
            System.out.println("InsertStoreProcedure declare parameter begin");
             declareParameter(new SqlParameter("p_rpt", Types.VARCHAR));
             declareParameter(new SqlParameter("p_ser", Types.VARCHAR));
             declareParameter(new  SqlParameter("p_ves", Types.VARCHAR));
             declareParameter(new SqlParameter("p_voy", Types.VARCHAR));
             declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
             declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
             declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
             declareParameter(new SqlParameter("p_ef", Types.VARCHAR));
             declareParameter(new  SqlParameter("p_et", Types.VARCHAR));
             declareParameter(new SqlParameter("p_if", Types.VARCHAR));
             declareParameter(new SqlParameter("p_it", Types.VARCHAR));
             declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
             declareParameter(new SqlParameter("p_cc", Types.VARCHAR));
             declareParameter(new SqlParameter("p_invf", Types.VARCHAR));
             declareParameter(new  SqlParameter("p_invt", Types.VARCHAR));
           //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
             declareParameter(new SqlInOutParameter("p_session", Types.VARCHAR));    
             declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));      
             declareParameter(new SqlParameter("p_cocsoc", Types.VARCHAR));      
             compile();
             System.out.println("InsertStoreProcedure declare parameter end");
         }
         protected String[] insert(String blBy  , String ser  , String ves  , String voy  , String dir  , String pol  , String pod  , String etatedFrom
                                                          , String etatedTo  , String blNo  , String instructFrom  , String instructTo  , String invNoFrom  , String invNoTo
                                                          , String collectFsc,String sessionID,String cocsoc) {
             return insertPro(blBy ,  ser   ,  ves  ,  voy   ,  dir  ,  pol  ,  pod  ,  etatedFrom ,  etatedTo  ,  blNo   ,  instructFrom  ,  instructTo  ,  invNoFrom  ,  invNoTo , collectFsc,sessionID,cocsoc);
         }
         protected String[] insertPro(String blBy  , String ser   , String ves  , String voy   , String dir  , String pol    , String pod
                                                           , String etatedFrom  , String etatedTo  , String blNo   , String instructFrom  , String instructTo
                                                           , String invNoFrom  , String invNoTo  , String collectFsc ,String sessionID,String cocsoc) {
                 System.out.println("InsertStoreProcedure assign value to  parameter begin");
                 String sessionId = "";
                 String sysdate = "";
                 String arrStr[] = {"",""};
                 Map inParameters = new HashMap();
                 inParameters.put("p_rpt",RutDatabase.stringToDb(blBy));
                 inParameters.put("p_ser",RutDatabase.stringToDb(ser));
                 inParameters.put("p_ves",RutDatabase.stringToDb(ves));
                 inParameters.put("p_voy",RutDatabase.stringToDb(voy));
                 inParameters.put("p_dir",RutDatabase.stringToDb(dir));
                 inParameters.put("p_pol",RutDatabase.stringToDb(pol));
                 inParameters.put("p_pod",RutDatabase.stringToDb(pod));
                 inParameters.put("p_ef",RutDatabase.stringToDb(etatedFrom));
                 inParameters.put("p_et",RutDatabase.stringToDb(etatedTo));
                 inParameters.put("p_if",RutDatabase.stringToDb(instructFrom));
                 inParameters.put("p_it",RutDatabase.stringToDb(instructTo));
                 inParameters.put("p_bl", RutDatabase.stringToDb(blNo));
                 inParameters.put("p_cc", RutDatabase.stringToDb(collectFsc));
                 inParameters.put("p_invf", RutDatabase.stringToDb(invNoFrom));
                 inParameters.put("p_invt", RutDatabase.stringToDb(invNoTo));
                 inParameters.put("p_session",RutDatabase.stringToDb(sessionID));
                 inParameters.put("p_cocsoc",RutDatabase.stringToDb(cocsoc));
                 
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_rpt = "+inParameters.get("p_rpt"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_ser = "+inParameters.get("p_ser"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_ves = "+inParameters.get("p_ves"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_voy = "+inParameters.get("p_voy"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_dir = "+inParameters.get("p_dir"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_pol = "+inParameters.get("p_pol"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_pod = "+inParameters.get("p_pod"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_ef = "+inParameters.get("p_ef"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_et = "+inParameters.get("p_et"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_if = "+inParameters.get("p_if"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_it = "+inParameters.get("p_it"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_bl = "+inParameters.get("p_bl"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_cc = "+inParameters.get("p_cc"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_invf = "+inParameters.get("p_invf"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_invt = "+inParameters.get("p_invt"));
                 System.out.println("[DimBLStatusReportJdbcDao][InsertStoreProcedure][insert]: p_session = "+inParameters.get("p_session"));
               /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
                 Map outParameters = execute(inParameters);
                 if (outParameters.size() > 0) {
                    sessionId = RutDatabase.dbToString(outParameters, "p_session");
                     sysdate = RutDatabase.dbToString(outParameters, "p_sysdate");
                     arrStr[0] = sessionId;
                     arrStr[1] = sysdate;
                    System.out.println("  sessionId >>>>>>>>>>>>> "+sessionId);
                 }
                 return arrStr;
             }
           
         } 
       public String[]  getSessionId(String blBy  , String ser  , String ves   , String voy   , String dir  , String pol  , String pod
                                                          , String etatedFrom  , String etatedTo   , String blNo  , String instructFrom  , String instructTo
                                                        , String invNoFrom  , String invNoTo  , String collectFsc,String sessionID,String cocsoc)throws DataAccessException {
                                                            
       return insertStoreProcedure.insert( blBy  ,  ser ,  ves  ,  voy  ,  dir  ,  pol  ,  pod ,  etatedFrom
                                                                             ,  etatedTo  ,  blNo  ,  instructFrom  ,  instructTo   ,  invNoFrom
                                                                             ,  invNoTo  , collectFsc,sessionID,cocsoc);
        }
   }

