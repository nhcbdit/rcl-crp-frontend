/*-----------------------------------------------------------------------------------------------------------  
CamAgentDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 24/04/08  
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
01 24/04/08  SPD                       Change to new framework
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamAgentDao {

    /**
     * check valid of agent code
     * @param agentCode
     * @param status
     * @return valid of agent code
     * @throws DataAccessException
     */
    public boolean isValid(String agentCode,String status) throws DataAccessException;

    /**
     * list agent records for help screen
     * @param find
     * @param search
     * @param wild
     * @param regionCode
     * @param countryCode
     * @return list of agent
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild,String regionCode,String countryCode) throws DataAccessException;

    /**
     * list agent records with user level for help screen
     * @param find
     * @param search
     * @param wild
     * @param lineCode
     * @param regionCode
     * @param status
     * @return list of agent with user level
     * @throws DataAccessException
     */
    public List listForHelpScreenWithUserLevel(String find,String search,String wild,String lineCode,String regionCode,String status) throws DataAccessException;

}

