/*-----------------------------------------------------------------------------------------------------------  
EmsZoneJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 29/04/2008 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 23/08/13  NIP                       Add function for Area and Zone
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsZoneMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsZoneJdbcDao extends RrcStandardDao implements EmsZoneDao  {
    public EmsZoneJdbcDao() {
    }
     
    protected void initDao() throws Exception {
        super.initDao();
    }
     
    public boolean isValid(String zone) throws DataAccessException {
        System.out.println("[EmsZoneJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ZONE_CODE ");
        sql.append("FROM VR_EMS_ZONE ");
        sql.append("WHERE ZONE_CODE = :zone ");
        HashMap map = new HashMap();
        map.put("zone",zone);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }       
        System.out.println("[EmsZoneJdbcDao][isValid]: Finished");
        return isValid;
    }
     
    public boolean isValid(String zone, String status) throws DataAccessException {
        System.out.println("[EmsZoneJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ZONE_CODE ");
        sql.append("FROM VR_EMS_ZONE ");
        sql.append("WHERE ZONE_CODE = :zone ");
        sql.append("AND ZONE_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("zone",zone);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }      
        System.out.println("[EmsZoneJdbcDao][isValid]: With status: Finished");
        return isValid;
     }          
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[EmsZoneJdbcDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT ZONE_CODE ");
         sql.append("      ,ZONE_DESCRIPTION ");
         sql.append("      ,CONTROL_ZONE_CODE ");             
         sql.append("      ,ZONE_STATUS "); 
         sql.append("FROM VR_EMS_ZONE ");
         sql.append(sqlCriteria);
         System.out.println("[EmsZoneJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
         System.out.println("[EmsZoneJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowModMapper()); 
     }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "WHERE ZONE_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "WHERE ZONE_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("Z")){
                 sqlCriteria = "WHERE CONTROL_ZONE_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "WHERE ZONE_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }   
     
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String controlZoneCode, String status) throws DataAccessException {
         System.out.println("[EmsZoneJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT DISTINCT ZONE_CODE ");
         sql.append("      ,ZONE_DESCRIPTION ");
         sql.append("      ,REGION_CODE ");             
         sql.append("      ,AREA_CODE ");                          
         sql.append("      ,CONTROL_ZONE_CODE ");             
         sql.append("      ,ZONE_STATUS "); 
         sql.append("FROM VR_EMS_ALL_GEO ");
         sql.append("WHERE ZONE_STATUS = :status ");
         if((regionCode!=null)&&(!regionCode.trim().equals(""))){
             sql.append("  AND REGION_CODE = :regionCode "); 
         }
         if((areaCode!=null)&&(!areaCode.trim().equals(""))){
             sql.append("  AND AREA_CODE = :areaCode "); 
         }  
         if((controlZoneCode!=null)&&(!controlZoneCode.trim().equals(""))){
             sql.append("  AND CONTROL_ZONE_CODE = :controlZoneCode "); 
         }             
         sql.append(sqlCriteria);
         System.out.println("[EmsZoneJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         HashMap map = new HashMap();
         map.put("regionCode",regionCode);
         map.put("areaCode",areaCode);
         map.put("controlZoneCode",controlZoneCode);
         map.put("status",status);
         System.out.println("[EmsZoneJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());  
         
    }
     
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND ZONE_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "AND ZONE_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("Z")){
                sqlCriteria = "AND CONTROL_ZONE_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND ZONE_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }      
     
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    }

    private EmsZoneMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        EmsZoneMod zone = new EmsZoneMod();
        zone.setZoneCode(RutString.nullToStr(rs.getString("ZONE_CODE")));
        zone.setZoneName(RutString.nullToStr(rs.getString("ZONE_DESCRIPTION")));
        zone.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));
        zone.setAreaCode(RutString.nullToStr(rs.getString("AREA_CODE")));
        zone.setControlZoneCode(RutString.nullToStr(rs.getString("CONTROL_ZONE_CODE")));
        zone.setZoneStatus(RutString.nullToStr(rs.getString("ZONE_STATUS")));

        return zone;
    }
    
    public String getZoneView(String line,String region,String agent,String fsc) throws DataAccessException {//##01
        String result = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct ZONE_CODE ");
        sql.append("from rclapps.VR_SYS_GEO ");
        sql.append("WHERE 1=1 ");
        
        HashMap map = new HashMap();
        
        if(line != null && !line.equals("")){
           map.put("line",line.toUpperCase());
           sql.append("and LV1 = :line ");
        } 
        if(region != null && !region.equals("")){
           map.put("region",region.toUpperCase());
           sql.append("and LV2 = :region ");
        } 
        if(agent != null && !agent.equals("")){
           map.put("agent",agent.toUpperCase());
           sql.append("and LV3 = :agent ");
        }  
        
        if(fsc != null && !fsc.equals("")){
           map.put("fsc",fsc.toUpperCase());
           sql.append("and FSC_CODE = :fsc ");
        }  
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            result=RutString.nullToStr(rs.getString("ZONE_CODE"));
        } 
        return result;
    }  
}
