package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface DimInvoiceCancelledBlValidDao {
    /**
     * generate a DIM Invoice cancelled BL valid:
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateDimInvoiceCancelledBlValid(RrcStandardMod mod) throws DataAccessException;
}
