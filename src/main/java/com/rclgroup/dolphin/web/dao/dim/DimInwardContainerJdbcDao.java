package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dim.DimInwardContainerMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

public class DimInwardContainerJdbcDao extends RrcStandardDao implements DimInwardContainerDao{
    public DimInwardContainerJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    public List getListInwardContainer(String service,String vessel,String voyage,String direction,String pol,String pod,String podT,String finalDes ,String lineCode,String regionCode,String agenCode,String fscCode) throws DataAccessException{
        System.out.println("[DimInwardContainerJdbcDao][getListInwardContainer]: Started");
        StringBuffer sql = new StringBuffer();  
        String sqlStmt = checkPortCodeForInward(lineCode,regionCode,agenCode,fscCode);
        sql.append("SELECT V_LINE ");
        sql.append(" ,V_TRAD ");
        sql.append(" ,V_AGNT ");
        sql.append(" ,V_SER ");
        sql.append(" ,V_VES ");
        sql.append(" ,V_VOY ");
        sql.append(" ,V_DIR ");
        sql.append(" ,V_PCSQ ");
        sql.append(" ,V_POD ");
        sql.append(" ,V_POD_TER ");
        sql.append(" ,V_STS ");
        sql.append(" ,V_BOOK ");  
        sql.append(" ,V_FUMT ");   
        sql.append(" ,SOC_COC ");
        sql.append(" ,V_SHIP_AGENT ");
        sql.append(" ,V_DATE ");
        sql.append(" ,V_DATE2 ");
        sql.append(" ,V_CONTAINER ");
        sql.append(" ,V_WEIGHT ");
        sql.append(" ,V_WEG_UNT ");
        sql.append(" ,V_EQSIZE_TYP ");
        sql.append(" ,V_CALLSIGN_VES ");
        sql.append(" ,V_LAST_PORT ");
        sql.append(" ,V_NEXT_PORT ");
        sql.append(" ,V_BL ");
        sql.append(" ,V_STOW ");
        sql.append(" ,V_LOAD_PORT ");
        sql.append(" ,V_ORI_PORT ");
        sql.append(" ,V_DIS_PORT ");
        sql.append(" ,V_TS ");
        sql.append(" ,V_FINAL_PORT ");
        sql.append(" ,V_PLA_RCP ");
        sql.append(" ,V_PLA_DEV ");
        sql.append(" ,V_CONSIG_OPER ");
        sql.append(" ,V_STEEL ");
        sql.append(" ,V_STATUS ");
        sql.append(" ,V_RF_UN ");
        sql.append(" ,V_IMDG ");
        sql.append(" ,V_OVER_HI ");
        sql.append(" ,V_OVER_LN ");
        sql.append(" ,V_OVER_WL ");
        sql.append(" ,V_OVER_WR ");
        sql.append(" ,V_ID_BLANK ");
        sql.append(" ,V_SEC_VES_CALL ");
        sql.append(" ,V_SEC_VOY_NO ");
        sql.append(" ,V_CONSGN ");
        sql.append(" ,V_TYP_CONT ");
        sql.append(" ,V_CONT_ACT ");
        sql.append(" ,V_MOVEMENT ");
        sql.append(" ,V_TXT_RMK ");
        sql.append(" ,V_PRO_INC ");
        sql.append(" ,V_PORT_DOC ");
        sql.append(" ,V_EDI_ID ");
        sql.append(" ,V_DATE_TRANSMIT ");
        sql.append(" ,V_TIME_TRANSMIT ");
           
        sql.append("FROM VR_DIM_INWARD ");
        sql.append("WHERE 1 = 1 ");
        if((service!=null)&&(!service.trim().equals(""))){
            sql.append("  AND V_SER = :service "); 
        }
        if((vessel!=null)&&(!vessel.trim().equals(""))){
            sql.append("  AND V_VES = :vessel "); 
        }
        if((voyage!=null)&&(!voyage.trim().equals(""))){
            sql.append("  AND V_VOY = :voyage "); 
        }  
        if((direction!=null)&&(!direction.trim().equals(""))){
            sql.append("  AND V_DIR = :direction "); 
        }
        
        if((pol!=null)&&(!pol.trim().equals(""))){
            sql.append("  AND V_LOAD_PORT = :pol ");
        }
        
        if((pod!=null)&&(!pod.trim().equals(""))){
            sql.append("  AND V_POD = :pod ");
        }
        
        if((podT!=null)&&(!podT.trim().equals(""))){
            sql.append("  AND V_POD_TER = :podT ");
        }
        
        if((finalDes!=null)&&(!finalDes.trim().equals(""))){
            sql.append("  AND V_FINAL_PORT = :finalDes ");
        }
        
        if((pod!=null)&&(!pod.trim().equals(""))){
            sql.append("  AND V_POD IN " + sqlStmt);
        }
           
        System.out.println("[DimInwardContainerJdbcDao][listFscForInwardContainer]: SQL: " + sql.toString());
        HashMap map = new HashMap();
        map.put("service",service);
        map.put("vessel",vessel);
        map.put("voyage",voyage);
        map.put("direction",direction);
        map.put("pol",pol);
        map.put("pod",pod);
        map.put("podT",podT);
        map.put("finalDes",finalDes);
        System.out.println("[DimInwardContainerJdbcDao][listFscForInwardContainer]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sql.toString(),
                map,
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimInwardContainerMod dimInwardMod = new DimInwardContainerMod();
                           dimInwardMod.setShipAgentCode(RutString.nullToStr(rs.getString("V_SHIP_AGENT")));
                           dimInwardMod.setVoyageNo(strPadding(rs.getString("V_VOY"),"R",10,"A"));
                           dimInwardMod.setArrivalDep1(rs.getString("V_DATE"));
                           dimInwardMod.setArrivalDep2(rs.getString("V_DATE2"));
                           dimInwardMod.setContainerNo(rs.getString("V_CONTAINER"));
                           dimInwardMod.setGrossWeg(rs.getString("V_WEIGHT"));
                           dimInwardMod.setUnitOfWef(rs.getString("V_WEG_UNT"));
                           dimInwardMod.setSizeTypeContainer(rs.getString("V_EQSIZE_TYP")); 
                           dimInwardMod.setCallSign(rs.getString("V_CALLSIGN_VES")); 
                           dimInwardMod.setLastPort(rs.getString("V_LAST_PORT")); 
                           dimInwardMod.setNextPort(rs.getString("V_NEXT_PORT")); 
                           dimInwardMod.setBlNo(rs.getString("V_BL"));
                           dimInwardMod.setStowagePos(rs.getString("V_STOW"));
                           dimInwardMod.setLoadingPort(rs.getString("V_LOAD_PORT"));
                           dimInwardMod.setOriginalLoadPort(rs.getString("V_ORI_PORT"));
                           dimInwardMod.setDischargePort(rs.getString("V_DIS_PORT"));
                           dimInwardMod.setTsDischargePort(rs.getString("V_TS"));
                           dimInwardMod.setFinalPort(rs.getString("V_FINAL_PORT"));
                           dimInwardMod.setPlaceOfRec(rs.getString("V_PLA_RCP"));
                           dimInwardMod.setPlaceOfDev(rs.getString("V_PLA_DEV"));
                           dimInwardMod.setConsigneeOper(rs.getString("V_CONSIG_OPER"));
                           dimInwardMod.setContainerMat(rs.getString("V_STEEL"));
                           dimInwardMod.setStatus(rs.getString("V_STATUS"));
                           dimInwardMod.setReeferTemp(rs.getString("V_RF_UN"));
                           dimInwardMod.setImoClass(rs.getString("V_IMDG"));
                           dimInwardMod.setOverHeight(rs.getString("V_OVER_HI"));
                           dimInwardMod.setOverLength(rs.getString("V_OVER_LN"));
                           dimInwardMod.setOverWidthR(rs.getString("V_OVER_WR"));
                           dimInwardMod.setOverWidthL(rs.getString("V_OVER_WL"));
                           dimInwardMod.setDirectDev(rs.getString("V_ID_BLANK"));
                           dimInwardMod.setSecondVesCallsign(rs.getString("V_SEC_VES_CALL"));
                           dimInwardMod.setSecondVoyNo(rs.getString("V_SEC_VOY_NO"));
                           dimInwardMod.setConsigneeName(rs.getString("V_CONSGN"));
                           dimInwardMod.setTypeOfContainer(rs.getString("V_TYP_CONT"));
                           dimInwardMod.setContainerAction(rs.getString("V_CONT_ACT"));
                           dimInwardMod.setMoveMent(rs.getString("V_MOVEMENT"));
                           dimInwardMod.setFreeTxtRmk(rs.getString("V_TXT_RMK"));
                           dimInwardMod.setProcessIndicator(rs.getString("V_PRO_INC"));
                           dimInwardMod.setPortOfLodOfDoc(rs.getString("V_PORT_DOC"));
                           dimInwardMod.setEdiUserId(rs.getString("V_EDI_ID"));
                           dimInwardMod.setDateTransmit(rs.getString("V_DATE_TRANSMIT"));
                           dimInwardMod.setTimeTransmit(rs.getString("V_TIME_TRANSMIT"));
                           
                           return dimInwardMod;
                       }
                   });
    
        
    }
    private String checkPortCodeForInward(String lineCode,String regionCode,String agentCode,String fscCode) throws DataAccessException{
        StringBuffer sqlStmt = new StringBuffer("");
        sqlStmt.append(" ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT WHERE 1 = 1 ");
        if(lineCode != null && !lineCode.equalsIgnoreCase("")){
            sqlStmt.append(" AND LINE_CODE = '"+lineCode+"'");
        }
        if(regionCode != null && !regionCode.equalsIgnoreCase("")){
            sqlStmt.append(" AND REGION_CODE = '"+regionCode+"'");
        }
        if(agentCode != null && !agentCode.equalsIgnoreCase("")){
            sqlStmt.append(" AND AGENT_CODE = '"+agentCode+"'");
        }
        if(fscCode != null && !fscCode.equalsIgnoreCase("")){
            sqlStmt.append(" AND FSC_CODE = '"+fscCode+"'");
        }
        sqlStmt.append(" GROUP BY PORT_CODE ) ");
        return sqlStmt.toString();
    }
    /**
     * Return the String was after left padding 
     * @param strPad    : String for padding
     * @param position  : L pad left , R pad right
     * @param amount    : number of padding
     * @param character : A use pad by space , N use pad by zero
     * @return String
     */
    public String strPadding(String strPad,String position,int amount ,String character){
       String space = "                     ";
       String zero  = "00000000";
       int lengthStr = 0;
       
        if(strPad == null){
            strPad = "";
        }
        
        lengthStr = strPad.length();
        
        if(lengthStr < amount){
        int subLast = amount-lengthStr;
        // padding left
            if(position.equalsIgnoreCase("L")){
                if(character.equalsIgnoreCase("A")){
                    strPad = space.substring(0,subLast)+strPad;
                }else{
                    strPad = zero.substring(0,subLast)+strPad;
                }
        // padding right
            }else{
                if(character.equalsIgnoreCase("A")){
                    strPad = strPad+space.substring(0,subLast);
                }else{
                    strPad = strPad+zero.substring(0,subLast);
                }
            }
    }
      return strPad;
  }
}
