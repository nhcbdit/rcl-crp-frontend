/*-----------------------------------------------------------------------------------------------------------  
BsaVsaVoyageHeaderJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda.P 15/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaServiceVariantMod;
import com.rclgroup.dolphin.web.model.bsa.BsaImbalanceListMod;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaVoyageHeaderMod;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class BsaVsaVoyageHeaderJdbcDao extends RrcStandardDao implements BsaVsaVoyageHeaderDao{
    private DeleteStoreProcedure deleteStoreProcedure;
    private RutDate rutDt = new RutDate();
    private SelectStoreProcedure selectStoreProcedure;
    private List lst = new ArrayList();
    /* Holds Current Page No incase of pagination */
    public int intCurrPage = 0;
    public BsaVsaVoyageHeaderJdbcDao(){
    }

    protected void initDao() throws Exception {
        super.initDao();
        selectStoreProcedure = new SelectStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    
    /** getVSAList: Gets Result of VSA Search
     * @param Map
     * @return Map
     * @throws BusinessException
     * @throws DataAccessException
     */  
    
    public List listForSearchScreen(String modelId, String proforma, String service, String vessel, String voyage, String direction, String effectiveDate, String expiredDate) throws DataAccessException{
        
        List list = new ArrayList();
        return list;
    }
    protected class SelectStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA.PRR_VSA_GET_LIST";
        protected  SelectStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);            
            
            declareParameter(new SqlOutParameter("P_O_V_VSA_REC", OracleTypes.CURSOR,
                new RowMapper(){                                   
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    BsaVsaVoyageHeaderMod vsaVoyageHeaderMod = new BsaVsaVoyageHeaderMod();
                    vsaVoyageHeaderMod.setModelName(RutString.nullToStr(rs.getString("MODEL_NAME")));
                    vsaVoyageHeaderMod.setService(RutString.nullToStr(rs.getString("SERVICE")));
                    vsaVoyageHeaderMod.setVariant(RutString.nullToStr(rs.getString("VARIANT_CODE")));
                    vsaVoyageHeaderMod.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                    vsaVoyageHeaderMod.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));                    
                    vsaVoyageHeaderMod.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                    vsaVoyageHeaderMod.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                    vsaVoyageHeaderMod.setEffectiveDate(rs.getString("START_DATETIME"));
                    vsaVoyageHeaderMod.setExpiryDate(rs.getString("END_DATETIME"));
                    vsaVoyageHeaderMod.setVssProformaId(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                    vsaVoyageHeaderMod.setVoyageHeaderId(RutString.nullToStr(rs.getString("PK_VOYAGE_HEADER_ID")));
                    vsaVoyageHeaderMod.setMinDate(rs.getString("MIN_DATE"));
                    vsaVoyageHeaderMod.setMaxDate(rs.getString("MAX_DATE"));                   
                    vsaVoyageHeaderMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                    vsaVoyageHeaderMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                    lst.add(vsaVoyageHeaderMod);
                    return  vsaVoyageHeaderMod;
                }
            }));
            declareParameter(new SqlParameter("P_I_V_ARRIVAL_FROM", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_ARRIVAL_TO", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_MODEL_NAME", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_PROFORMA", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_SERVICE", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_VESSEL", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_VOYAGE", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_DIRECTION", Types.VARCHAR));
            declareParameter(new SqlParameter("P_I_V_SORT_EXP", Types.VARCHAR));
            
                    
            compile();
            System.out.println("BsaVsaVoyageHeaderJdbcDao: SelectStoreProcedure declare parameter end");
        }
        protected List select(String modelId, String proforma, String service, String vessel, String voyage, String direction, String effectiveDate, String expiryDate, String sortBy, String sortByIn) {
            return  selecta(modelId, proforma, service, vessel, voyage, direction, effectiveDate, expiryDate, sortBy, sortByIn);
        }
        
        protected List selecta(String modelId, String proforma, String service, String vessel, String voyage, String direction, String effectiveDate, String expiryDate, String sortBy, String sortByIn) {
            Map inParameters = new HashMap();
            lst.clear();
            System.out.println("BsaVsaVoyageHeaderJdbcDao SelectStoreProcedure assign value to  parameter begin");
            inParameters.put("P_I_V_ARRIVAL_FROM", RutDate.dateToStr(effectiveDate));
            inParameters.put("P_I_V_ARRIVAL_TO", RutDate.dateToStr(expiryDate));
            inParameters.put("P_I_V_MODEL_NAME", modelId);
            inParameters.put("P_I_V_PROFORMA", proforma);
            inParameters.put("P_I_V_SERVICE", service);
            inParameters.put("P_I_V_VESSEL",vessel);
            inParameters.put("P_I_V_VOYAGE",voyage);
            inParameters.put("P_I_V_DIRECTION",direction);
            inParameters.put("P_I_V_SORT_EXP", sortBy+" "+sortByIn);              
                
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
                return lst;
            }else{
                return null;
            }
           
        }
    }
    
    public List  getCursor (String modelId, String proforma, String service, String vessel, String voyage, String direction, String effectiveDate, String expiryDate, String sortBy, String sortByIn)  throws DataAccessException{
    
     return selectStoreProcedure.select(modelId, proforma, service, vessel, voyage, direction, effectiveDate, expiryDate, sortBy, sortByIn);
     
    }
   

    
    public boolean isValidForService(String service) {
        System.out.println("[BsaVsaVoyageHeaderJdbcDao][isValidForService]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        if ((service!=null)&&(!service.trim().equals(""))) {
            sql.append("where SERVICE = :service ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("service",service);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[BsaVsaVoyageHeaderJdbcDao][isValidForService]: Finished");
        return isValid;
    }

    public boolean isValidForVessel(String vessel) {
        System.out.println("[BsaVsaVoyageHeaderJdbcDao][isValidForVessel]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VESSEL ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        if ((vessel!=null)&&(!vessel.trim().equals(""))) {
            sql.append("where VESSEL = :vessel ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("vessel",vessel);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[BsaVsaVoyageHeaderJdbcDao][isValidForVessel]: Finished");
        return isValid;
    }

    public boolean isValidForVoyage(String voyage) {
        System.out.println("[BsaVsaVoyageHeaderJdbcDao][isValidForVoyage]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VOYAGE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        if ((voyage!=null)&&(!voyage.trim().equals(""))) {
            sql.append("where VOYAGE = :voyage ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("voyage",voyage);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[BsaVsaVoyageHeaderJdbcDao][isValidForVoyage]: Finished");
        return isValid;
    }

    public BsaVsaVoyageHeaderMod findByKeyVoyageHeaderId(String voyageHeaderId) throws DataAccessException{
        
        BsaVsaVoyageHeaderMod bsaVsaVoyageHeaderMod = null;
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();

        sql.append("SELECT VVH.PK_VOYAGE_HEADER_ID ");
        sql.append("      ,SV.VARIANT_CODE  ");
        sql.append("      ,VVH.FK_BSA_MODEL_ID ");
        sql.append("      ,VVH.FK_VSS_PROFORMA_ID  ");
        sql.append("      ,VVH.FK_VSS_VESSEL_ASSIGNMENT_ID  ");
        sql.append("      ,VVH.DN_SERVICE_GROUP_CODE  ");
        sql.append("      ,VVH.FK_SERVICE  ");
        sql.append("      ,VVH.FK_VESSEL  ");
        sql.append("      ,VVH.FK_VOYAGE  ");
        sql.append("      ,VVH.FK_DIRECTION  ");
        sql.append("      ,VVH.TOLERANT  ");
        sql.append("      ,BSA_MD.MODEL_NAME AS MODEL_NAME  ");
        sql.append("      ,VSS.VSLGNM AS VESSEL_NAME  ");
        sql.append("FROM VSA_VOYAGE_HEADER VVH ");
        
        sql.append("LEFT JOIN BSA_BASE_ALLOCATION_MODEL BSA_MD ON VVH.FK_BSA_MODEL_ID = BSA_MD.PK_BSA_MODEL_ID ");
        sql.append("LEFT JOIN ITP060 VSS ON VVH.FK_VESSEL = VSS.VSVESS ");
        sql.append("LEFT JOIN BSA_SERVICE_VARIANT SV ON VVH.FK_BSA_SERVICE_VARIANT_ID = SV.PK_BSA_SERVICE_VARIANT_ID ");
        sql.append("WHERE VVH.PK_VOYAGE_HEADER_ID = :voyageHeaderId ");//
        sql.append("ORDER BY PK_VOYAGE_HEADER_ID ");
        
        System.out.println("[BsaVsaVoyageHeaderJdbcDao] :[findByKeyVoyageHeaderId] sql >>  "+sql.toString());
        System.out.println("[BsaVsaVoyageHeaderJdbcDao] :[findByKeyVoyageHeaderId] voyageHeaderId >>  "+voyageHeaderId);
        try{
            bsaVsaVoyageHeaderMod = (BsaVsaVoyageHeaderMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("voyageHeaderId", voyageHeaderId),
                   new RowMapper(){
                      public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            BsaVsaVoyageHeaderMod bsaVsaVoyageHeaderMod = new BsaVsaVoyageHeaderMod();
                            bsaVsaVoyageHeaderMod.setVoyageHeaderId(String.valueOf(rs.getInt("PK_VOYAGE_HEADER_ID")));
                            bsaVsaVoyageHeaderMod.setVariant(RutString.nullToStr(rs.getString("VARIANT_CODE")));
                            bsaVsaVoyageHeaderMod.setBsaModelId(String.valueOf(rs.getInt("FK_BSA_MODEL_ID")));
                            bsaVsaVoyageHeaderMod.setModelName(RutString.nullToStr(rs.getString("MODEL_NAME")));
                            bsaVsaVoyageHeaderMod.setVssProformaId(String.valueOf(rs.getInt("FK_VSS_PROFORMA_ID")));
                            bsaVsaVoyageHeaderMod.setVssVesselAssigmentId(String.valueOf(rs.getInt("FK_VSS_VESSEL_ASSIGNMENT_ID")));
                            bsaVsaVoyageHeaderMod.setServiceGroupCode(RutString.nullToStr(rs.getString("DN_SERVICE_GROUP_CODE")));
                            bsaVsaVoyageHeaderMod.setService(RutString.nullToStr(rs.getString("FK_SERVICE")));
                            bsaVsaVoyageHeaderMod.setVessel(RutString.nullToStr(rs.getString("FK_VESSEL")));
                            bsaVsaVoyageHeaderMod.setVoyage(RutString.nullToStr(rs.getString("FK_VOYAGE")));
                            bsaVsaVoyageHeaderMod.setTolelant(String.valueOf(rs.getInt("TOLERANT")));
                            bsaVsaVoyageHeaderMod.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
                            bsaVsaVoyageHeaderMod.setDirection(RutString.nullToStr(rs.getString("FK_DIRECTION")));
                           return bsaVsaVoyageHeaderMod;
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            bsaVsaVoyageHeaderMod = null;
        }
        return bsaVsaVoyageHeaderMod;
    }
    
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException {
        BsaVsaVoyageHeaderMod mod = (BsaVsaVoyageHeaderMod) masterMod;
        
        StringBuffer errorMsgBuffer = new StringBuffer();
        try {
            if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)) {
                //insert(masterMod);
            } else if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)) {
                //update(masterMod);
            }
        } catch(CustomDataAccessException e) {
            errorMsgBuffer.append(e.getMessages()+"&");
        } catch(DataAccessException e) {
            e.printStackTrace();
        }
        
        //begin: delete record of bsa service variant
        if (isDeletes != null && !isDeletes.isEmpty()) { //if 1
            BsaVsaVoyageHeaderMod bean = null;
            ArrayList listDelete = new ArrayList(isDeletes.values());
            for (int i=0;i<listDelete.size();i++) { //for 1
                bean = (BsaVsaVoyageHeaderMod) listDelete.get(i);
                this.delete(bean);
                
            } //end for 1
        } //end if 1
        //end: delete record of bsa service variant
    }
    
     protected class DeleteStoreProcedure extends StoredProcedure {
          private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA.PRR_DEL_VSA_DATA";
    
          protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
              super(jdbcTemplate, STORED_PROCEDURE_NAME);
              declareParameter(new SqlParameter("p_vsa_voyage_header_id", Types.INTEGER));
              declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
              declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
              compile();
          }
          
          protected boolean delete(final RrcStandardMod inputMod) {
              boolean isSuccess = false;
              if (inputMod instanceof BsaVsaVoyageHeaderMod) {
                  BsaVsaVoyageHeaderMod aInputMod = (BsaVsaVoyageHeaderMod) inputMod;
                  
                  Map inParameters = new HashMap(3);
                  inParameters.put("p_vsa_voyage_header_id", new Integer((RutString.nullToStr(aInputMod.getVoyageHeaderId())).equals("")?"0":RutString.nullToStr(aInputMod.getVoyageHeaderId())));                      
                  inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                  inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                  
                  System.out.println("[BsaVsaVoyageHeaderJdbcDao][DeleteStoreProcedure][delete]: p_vsa_voyage_header_id = "+inParameters.get("p_vsa_voyage_header_id"));
                  System.out.println("[BsaVsaVoyageHeaderJdbcDao][DeleteStoreProcedure][delete]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                  System.out.println("[BsaVsaVoyageHeaderJdbcDao][DeleteStoreProcedure][delete]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                  
                  execute(inParameters);
                  isSuccess = true;
              }
              return isSuccess;
          }
      }
}
