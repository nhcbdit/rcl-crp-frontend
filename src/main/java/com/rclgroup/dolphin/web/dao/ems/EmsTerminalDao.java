/*-----------------------------------------------------------------------------------------------------------  
EmsTerminalDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 10/06/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.ems;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface EmsTerminalDao {

    /**
     * check valid of terminal code
     * @param terminalCode
     * @return valid of terminal code
     * @throws DataAccessException
     */
    public boolean isValid(String terminalCode) throws DataAccessException;

    /**
     * list terminal records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of terminal
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String controlZoneCode, String zoneCode, String point) throws DataAccessException;
    
    public List listForHelpScreenWithPoint(String find, String search, String wild, String point) throws DataAccessException;
    
    public List listForHelpScreenWithFsc(String find, String search, String wild, String fsc, String point) throws DataAccessException;
    
    public List listForHelpScreenWithControlZone(String find, String search, String wild, String controlZone, String point) throws DataAccessException;
}
