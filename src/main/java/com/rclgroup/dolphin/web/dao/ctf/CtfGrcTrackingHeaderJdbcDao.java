/*-----------------------------------------------------------------------------------------------------------  
CtfGrcTrackingHeaderJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 05/11/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.ctf;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.model.ctf.CtfGrcTrackingHeaderMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class CtfGrcTrackingHeaderJdbcDao extends RrcStandardDao implements CtfGrcTrackingHeaderDao {
    public CtfGrcTrackingHeaderJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String grcTrackingNo) {
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][isValid]: Started ");  
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT GRC_TRACKING_REF_NO FROM VR_CTF_GRC_TRACKING_LIST ");
        sql.append(" WHERE 1=1 ");
        if(grcTrackingNo != null && !grcTrackingNo.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_REF_NO) = '"+ grcTrackingNo + "' ");
        }
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][isValid]: SQL: "+sql.toString());  
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),new HashMap());
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }   
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][isValid]: Finished ");  
        return isValid;
    }

    public List listForGrcTrackingReport(int rowAt,int rowTo,String cocSoc,String rateChgFrm,String rateChgType,String grcNo,String fromDate,String toDate,String status,String sortBy,String sortIn) {
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][listForHelpScreen]: Started");  
        String sqlColumn = "GRC_TRACKING_REF_NO , GRC_TRACKING_CHILD_REF_NO , SOC_COC_FLAG , SOC_COC_FLAG_NAME , RATE_CHG_FROM , RATE_CHG_FROM_NAME , RATE_CHG_TYPE , RATE_CHG_TYPE_NAME , GRC_TRACKING_DATE , RECORD_STATUS , RECORD_STATUS_NAME ";
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT "+sqlColumn);
        sql.append(" FROM VR_CTF_GRC_TRACKING_LIST ");
        sql.append(" WHERE 1=1 ");
        
        if(cocSoc != null && !cocSoc.trim().equals("")){
            sql.append(" AND UPPER(SOC_COC_FLAG) = '" +cocSoc + "' ");
        }
        if(rateChgFrm != null && !rateChgFrm.trim().equals("")){
            sql.append(" AND UPPER(RATE_CHG_FROM) = '"+rateChgFrm + "' ");
        }
        if(rateChgType != null && !rateChgType.trim().equals("")){
            sql.append(" AND UPPER(RATE_CHG_TYPE) = '"+rateChgType + "' ");
        }
        if(grcNo != null && !grcNo.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_REF_NO) = '"+grcNo + "' ");
        }
        if(fromDate != null && !fromDate.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_DATE) >= "+ fromDate);
        }
        if(toDate != null && !toDate.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_DATE) <= "+toDate);
        }
        if(status != null && !status.trim().equals("")){
            sql.append(" AND UPPER(RECORD_STATUS) = '"+ status + "' ");
        }
        if(sortBy != null && !sortBy.trim().equals("")){
            sql.append(" ORDER BY ");
            if(sortBy.equalsIgnoreCase("CS")){
                sql.append(" SOC_COC_FLAG ");
            }
            if(sortBy.equalsIgnoreCase("F")){
                sql.append(" RATE_CHG_FROM ");
            }
            if(sortBy.equalsIgnoreCase("T")){
                sql.append(" RATE_CHG_TYPE ");
            }
            if(sortBy.equalsIgnoreCase("G")){
                sql.append(" GRC_TRACKING_REF_NO ");
            }
            if(sortBy.equalsIgnoreCase("D")){
                sql.append(" GRC_TRACKING_DATE ");
            }
            if(sortBy.equalsIgnoreCase("S")){
                sql.append(" RECORD_STATUS ");
            }
            if(sortBy.equalsIgnoreCase("C")){
                sql.append(" GRC_TRACKING_CHILD_REF_NO ");
            }
        }else{
            sql.append(" ORDER BY ");
            sql.append("        GRC_TRACKING_REF_NO ");
            sql.append("        ,GRC_TRACKING_CHILD_REF_NO");
            sql.append("        ,SOC_COC_FLAG ");
            sql.append("        ,RATE_CHG_FROM ");
            sql.append("        ,RATE_CHG_TYPE ");
            sql.append("        ,GRC_TRACKING_DATE ");
            sql.append("        ,RECORD_STATUS ");
        }
        if(sortIn != null && !sortIn.trim().equals("")){
            if(sortIn.equalsIgnoreCase("A")){
                sql.append(" ASC");
            }else{
                sql.append(" DESC");
            }
        }
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][listForHelpScreen]: SQL: "+sql.toString());
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][listForHelpScreen]: Finished ");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                CtfGrcTrackingHeaderMod mod = new CtfGrcTrackingHeaderMod();
                mod.setCocSoc(RutString.nullToStr(rs.getString("SOC_COC_FLAG")));
                mod.setRateChgFrom(RutString.nullToStr(rs.getString("RATE_CHG_FROM")));
                mod.setRateChgType(RutString.nullToStr(rs.getString("RATE_CHG_TYPE")));
                mod.setCocSocName(RutString.nullToStr(rs.getString("SOC_COC_FLAG_NAME")));
                mod.setRateChgFromName(RutString.nullToStr(rs.getString("RATE_CHG_FROM_NAME")));
                mod.setRateChgTypeName(RutString.nullToStr(rs.getString("RATE_CHG_TYPE_NAME")));
                mod.setGrcRefNo(RutString.nullToStr(rs.getString("GRC_TRACKING_REF_NO")));
                mod.setGrcChildRefNo(RutString.nullToStr(rs.getString("GRC_TRACKING_CHILD_REF_NO")));
                mod.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                mod.setStatusName(RutString.nullToStr(rs.getString("RECORD_STATUS_NAME")));
                mod.setGrcDate(rs.getInt("GRC_TRACKING_DATE"));
                return mod;
             }
         });
    }

    public int countListForGrcTrackingReport(String cocSoc,String rateChgFrm,String rateChgType,String grcNo,String fromDate,String toDate,String status,String sortBy,String sortIn) {
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][countListForGrcTrackingReport]: Started");  
        String sqlColumn = " GRC_TRACKING_REF_NO , GRC_TRACKING_CHILD_REF_NO , SOC_COC_FLAG , SOC_COC_FLAG_NAME , RATE_CHG_FROM , RATE_CHG_FROM_NAME , RATE_CHG_TYPE , RATE_CHG_TYPE_NAME , GRC_TRACKING_DATE , RECORD_STATUS , RECORD_STATUS_NAME ";
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT "+sqlColumn);
        sql.append(" FROM VR_CTF_GRC_TRACKING_LIST ");
        sql.append(" WHERE 1=1 ");
        
        if(cocSoc != null && !cocSoc.trim().equals("")){
            sql.append(" AND UPPER(SOC_COC_FLAG) = '" +cocSoc + "' ");
        }
        if(rateChgFrm != null && !rateChgFrm.trim().equals("")){
            sql.append(" AND UPPER(RATE_CHG_FROM) = '"+rateChgFrm + "' ");
        }
        if(rateChgType != null && !rateChgType.trim().equals("")){
            sql.append(" AND UPPER(RATE_CHG_TYPE) = '"+rateChgType + "' ");
        }
        if(grcNo != null && !grcNo.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_REF_NO) = '"+grcNo + "' ");
        }
        if(fromDate != null && !fromDate.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_DATE) >= "+ fromDate);
        }
        if(toDate != null && !toDate.trim().equals("")){
            sql.append(" AND UPPER(GRC_TRACKING_DATE) <= "+toDate);
        }
        if(status != null && !status.trim().equals("")){
            sql.append(" AND UPPER(RECORD_STATUS) = '"+ status + "' ");
        }
        sql = getNumberOfAllData(sql);
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][countListForGrcTrackingReport]: SQL: "+sql.toString());
        System.out.println("[CtfGrcTrackingHeaderJdbcDao][countListForGrcTrackingReport]: Finished ");

		
		Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),new HashMap(), Integer.class);
		return result;
        
        
    }


}
