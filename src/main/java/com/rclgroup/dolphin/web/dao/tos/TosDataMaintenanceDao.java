/*-----------------------------------------------------------------------------------------------------------  
TosDataMaintenanceDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 24/02/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface TosDataMaintenanceDao extends RriStandardDao {

    /**
     * reopen Load List record
     * @param mod
     * @return whether insertion is successful
     * @throws DataAccessException
     */ 
    public boolean reopenLoadList(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * delete Discharge List record
     * @param mod
     * @return whether insertion is successful
     * @throws DataAccessException
     */ 
    public boolean deleteDischargeList(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * @param portType
     * @param vessel
     * @param voyage
     * @param port
     * @param terminal
     * @return List
     * @throws DataAccessException
     */
    public List findTosBKP009(String portType, String vessel, String voyage, String port, String terminal) throws DataAccessException;
    
    /**
     * @param portType
     * @param vessel
     * @param voyage
     * @param port
     * @param terminal
     * @return List
     * @throws DataAccessException
     */
    public List findTosOnboardList(String portType, String vessel, String voyage, String port, String terminal) throws DataAccessException;
    
    /**
     * @param portType
     * @param vessel
     * @param voyage
     * @param port
     * @param terminal
     * @return List
     * @throws DataAccessException
     */
    public List findTosDischList(String portType, String vessel, String voyage, String port, String terminal) throws DataAccessException;
    
}


