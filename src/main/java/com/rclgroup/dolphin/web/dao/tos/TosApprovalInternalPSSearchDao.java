/*-----------------------------------------------------------------------------------------------------------  
TosApprovalInternalPSSearchDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Tanaphl Lhimmanee 26/04/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;
import java.util.List;
import org.springframework.dao.DataAccessException;

public interface TosApprovalInternalPSSearchDao extends RriStandardDao{

    /**
     * @param port
     * @param terminal
     * @param service
     * @param vessel
     * @param voyage
     * @param adtFrom
     * @param adtTo
     * @param tosProRef
     * @param cocSoc
     * @param sort
     * @return List
     * @throws DataAccessException
     */
    public List getApprovalSearchList(String port,String terminal,String service,String vessel,String voyage,String adtFrom, String adtTo, String tosProRef, String cocSoc, String sort) throws DataAccessException;
    
}
