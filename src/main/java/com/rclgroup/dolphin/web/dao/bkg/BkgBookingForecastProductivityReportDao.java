/*--------------------------------------------------------
BkgBookingForecastProductivityReportDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Leena Babu 25/07/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.bkg;

public interface BkgBookingForecastProductivityReportDao {

    /**
     * Generates the data for booking forecast productivity report for the 
     *  given search criteria and stores in the temporary table in database
     *  
     * @param userCode of the logged in user
     * @param sessionId of the session
     * @param startDate of the first week
     * @param endDate of the first week
     * @param socCoc booking type
     * @param equipmentSize 
     * @param equipmentType
     * @param dischargePort
     * 
     * @return true if successful else false
     * */
     public boolean  generateReportData(String userCode, String sessionId, 
                                       String startDate, String endDate, 
                                       String socCoc, String equipmentSize, 
                                       String equipmentType, 
                                       String dischargePort);
}
