package com.rclgroup.dolphin.web.dao.bkg;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface BkgBookingDao {
   
    /**
     * check valid of Booking#
     * @param bookingNo     
     * @return valid of bookingNo
     * @throws DataAccessException
     */
    public boolean isValid(String bookingNo) throws DataAccessException;
    
    /**
     * check valid of Booking#
     * @param bookingNo  
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return valid of bookingNo
     * @throws DataAccessException
     */
    public boolean isValidByFsc(String bookingNo, String line, String trade, String agent, String fsc) throws DataAccessException;
    
    /**
     * list Booking records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Booking
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
    /**
     * list Booking records for help screen
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Booking
     * @throws DataAccessException
     */
    public List listForHelpScreenByFsc(String find,String search,String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
    
    /**
     * list Invoice records for help screen
     * @param rowAt
     * @param rowTo
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Booking 
     * @throws DataAccessException
     */
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find,String search,String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
    /**
     * Count number of Invoice records for help screen
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Booking
     * @throws DataAccessException
     */
    public int countListForNewHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException;
   
}
