/*-----------------------------------------------------------------------------------------------------------  
VsaVsaPortPairTsJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 22/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaPortPairTsMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class BsaVsaPortPairTsJdbcDao extends RrcStandardDao implements BsaVsaPortPairTsDao {
    
    public BsaVsaPortPairTsJdbcDao() { 
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValidWithStatus(String find,String search,String status) {
        System.out.println("[VsaVsaPortPairTsJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select "+search+" ");
        sql.append("from VR_BSA_PORT_PAIR_TS ");
        sql.append(" where RECORD_STATUS = :status ");        
//        sql.append(" and rownum = 1 ");
        HashMap map = new HashMap();
        map.put("status",status);
        if ((find!=null)&&(!find.trim().equals(""))) {
            sql.append(" and "+search+" = :find ");            
            map.put(search,find);
        }
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[VsaVsaPortPairTsJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidWithStatus(String tsIndic,String service,String vCode,String portCode,String groupCode,String status) {
        System.out.println("[VsaVsaPortPairTsJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        String whereClause = "";
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE ");
        sql.append("from VR_BSA_PORT_PAIR_TS ");        
        HashMap map = new HashMap();
        if ((tsIndic!=null)&&(!tsIndic.trim().equals(""))) {
            whereClause = "AND TS_INDIC = :tsIndic ";            
            map.put("tsIndic",tsIndic);
        }        
        if ((service!=null)&&(!service.trim().equals(""))) {
            whereClause = "AND SERVICE = :service ";            
            map.put("service",service);
        }
        if ((vCode!=null)&&(!vCode.trim().equals(""))) {
            whereClause = "AND V_CODE = :vCode ";            
            map.put("vCode",vCode);
        }
        if ((portCode!=null)&&(!portCode.trim().equals(""))) {
            whereClause = "AND PORT_CODE = :portCode ";            
            map.put("portCode",portCode);
        }
        if ((groupCode!=null)&&(!groupCode.trim().equals(""))) {
            whereClause = "AND GROUP_CODE = :groupCode ";            
            map.put("groupCode",groupCode);
        }
        if ((status!=null)&&(!status.trim().equals(""))) {
            whereClause = "AND RECORD_STATUS = :status ";            
            map.put("status",status);
        }
        if (!whereClause.equals("")){
            sql.append(" where rownum = 1 "+whereClause);
        }
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[VsaVsaPortPairTsJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreenWithStatus(String find, String search, String wild, String status, String tsIndic, int serviceVariantId, int modelId, String tsPolPort,String tsPodPort, String polTsFlag, String podTsFlag) {
        System.out.println("[VsaVsaPortPairTsJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct TS_INDIC ");
        sql.append("    ,SERVICE ");
        sql.append("    ,V_CODE ");
        sql.append("    ,PORT_CODE ");
        sql.append("    ,GROUP_CODE "); 
        sql.append("    ,GROUP_FLAG ");
        sql.append("    ,NVL(SUB_GROUP_FLAG,'G') AS SUB_GROUP_FLAG ");
        sql.append("    ,RECORD_STATUS ");
        sql.append("from VR_BSA_PORT_PAIR_TS ");
        sql.append(" where RECORD_STATUS = :status "); 
        sql.append(" and SERVICE NOT IN  (SELECT SV.SERVICE FROM VR_BSA_SERVICE_VARIANT SV where BSA_SERVICE_VARIANT_ID = :sv_id1) ");
        HashMap map = new HashMap();        
        map.put("status",status);
        map.put("sv_id1",new Integer(serviceVariantId));
        /*
        if(tsIndic!=null && !tsIndic.equals("")){
            sql.append(" AND UPPER(TS_INDIC) = :ts_indic ");
            map.put("ts_indic",tsIndic.toUpperCase());
            
            if(tsIndic.toUpperCase().equals("FROM")){
                sql.append(" AND L_PORT = :ts_polport ");
                map.put("ts_polport",tsPolPort.toUpperCase());
            }else{
                sql.append(" AND D_PORT = :ts_podport ");
                map.put("ts_podport",tsPodPort.toUpperCase());
            }            
        }
        */
         if(polTsFlag.toUpperCase().equals("Y") && podTsFlag.toUpperCase().equals("Y")){
             sql.append(" AND (L_PORT = :ts_polport OR D_PORT = :ts_podport) ");
             map.put("ts_polport",tsPolPort.toUpperCase());
             map.put("ts_podport",tsPodPort.toUpperCase());
         }else if(polTsFlag.toUpperCase().equals("Y")){
             sql.append(" AND L_PORT = :ts_polport ");
             map.put("ts_polport",tsPolPort.toUpperCase());
         }else if(podTsFlag.toUpperCase().equals("Y")){
             sql.append(" AND D_PORT = :ts_podport ");
             map.put("ts_podport",tsPodPort.toUpperCase());
         }            
        if(serviceVariantId>0){
            sql.append(" AND SV_ID = :sv_id ");
            map.put("sv_id",new Integer(serviceVariantId));
        }
        if(modelId>0){
            sql.append(" AND MOD_ID = :mod_id ");
            map.put("mod_id",new Integer(modelId));
        }
        if(tsPolPort!=null && !tsPolPort.equals("")){
            sql.append(" AND PORT_CODE <> :ts_polport ");
            map.put("ts_polport",tsPolPort);
        }
        if(tsPodPort!=null && !tsPodPort.equals("")){
            sql.append(" AND PORT_CODE <> :ts_podport ");
            map.put("ts_podport",tsPodPort);
        }
        if(sqlCriteria!=null&&!sqlCriteria.equals("")){
            sql.append(sqlCriteria);
        }
        sql.append("order by TS_INDIC ");
        sql.append("    ,SERVICE ");
        sql.append("    ,V_CODE ");
        sql.append("    ,PORT_CODE ");
        sql.append("    ,GROUP_CODE ");
//        System.out.println("tspol = "+tsPolPort+" tspod = "+tsPodPort+"  tsindic = "+tsIndic+"   sv = "+serviceVariantId);
        System.out.println("[VsaVsaPortPairTsJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[VsaVsaPortPairTsJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaVsaPortPairTsMod bean = new BsaVsaPortPairTsMod();
                           bean.setTsIndic(RutString.nullToStr(rs.getString("TS_INDIC")));
                           bean.setVsaService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setVsaVariant(RutString.nullToStr(rs.getString("V_CODE")));
                           bean.setVsaPort(RutString.nullToStr(rs.getString("PORT_CODE")));
                           bean.setVsaGroup(RutString.nullToStr(rs.getString("GROUP_CODE")));                           
                           bean.setVsaPortGroupFlag(RutString.nullToStr(rs.getString("GROUP_FLAG")));  
                           bean.setVsaSubGroupFlag(RutString.nullToStr(rs.getString("SUB_GROUP_FLAG")));  
                           bean.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));  
                           return bean;
                       }
                   }
                );
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("T")) {
                sqlCriteria = "AND UPPER(TS_INDIC) " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND UPPER(SERVICE) " + sqlWild;
            } else if (search.equalsIgnoreCase("V")) {
                sqlCriteria = "AND UPPER(V_CODE) " + sqlWild;
            } else if (search.equalsIgnoreCase("P")) {
                sqlCriteria = "AND UPPER(PORT_CODE) " + sqlWild;
            } else if (search.equalsIgnoreCase("G")) {
                sqlCriteria = "AND UPPER(GROUP_CODE) " + sqlWild;
            }
        }
        return sqlCriteria;
    } 
    
}
