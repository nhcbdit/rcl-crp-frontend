package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.model.dim.DimBlMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface DimBlDao {
    /**
     * check valid of B/L#
     * @param bl
     * @return valid of bl
     * @throws DataAccessException
     */
    public boolean isValid(String bl) throws DataAccessException;
   
    /**
     * check valid of B/L#
     * @param bl
     * @return valid of bl of bg type
     * @throws DataAccessException
     */
    public boolean isValidForBankGuarantee(String bl) throws DataAccessException;
    
    /**
     * check valid of B/L#
     * @param bl
     * @return valid of bl of bg type
     * @throws DataAccessException
     */
    public boolean isValidForDangerousCargoManifest(String bl) throws DataAccessException;

    /**
     * list bl records for help screen
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of bl
     * @throws DataAccessException
     */
    public List listForHelpScreenWithFsc(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException;

    /**
     * list bl records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of bl
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

     /**
      * list user records for help screen
      * @param find
      * @param search
      * @param wild
      * @return list of bl only bg type
      * @throws DataAccessException
      */
     public List listForHelpScreenForBankGuarantee(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
     
    /**
     * list user records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of bl only bg type
     * @throws DataAccessException
     */
    public List listForHelpScreenForDangerousCargoManifest(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
    
     
    /**
     * find user by using key as user id
     * @param code
     * @return bl, name, fsc, organization type, department, email and status
     * @throws DataAccessException
     */
    public DimBlMod findByKeyCode(String code) throws DataAccessException;
    /**
     * list user records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of bl only bg type
     * @throws DataAccessException
     */
    public List listForHelpScreenForReeferManifest(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
    /**
     * list user records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of bl only bg type
     * @throws DataAccessException
     */
    public List listForHelpScreenForReeferManifestImport(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException;


    /**
     * check valid of B/L#
     * @param bl
     * @return valid of bl of bg type
     * @throws DataAccessException
     */
    public boolean isValidForCargoManifestList(String bl) throws DataAccessException;
    
    /**
     * list user records for help screen
     * @param line,trade,agent,fsc,cOrS,blNo,bkgNo,pol,pod,service,vessel,voy,direction,searchBy,sessionId
     * @return list of bl with booking# & equipment #
     * @throws DataAccessException
     */
    public List listForSearchScreenByBlEquipmentNo(String line, String trade, String agent, String fsc,String cOrS, String blNo,String equipmentNo,String pol,String pod,String service,String vessel,String voy,String direction,String invoyagePort,String sessionId) throws DataAccessException;
    /**
     * list bl records for help screen
     * @param rowAt
     * @param rowTo
     * @param find
     * @param search
     * @param wild
     * @return list of bl
     * @throws DataAccessException
     */
    public List listForNewHelpScreen(int rowAt,int rowTo,String find,String search,String wild) throws DataAccessException;
    
    /**
     * Count number of bl records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of bl
     * @throws DataAccessException
     */
    public int countListForNewHelpScreen(String find, String search, String wild) throws DataAccessException;
    /**
     * list Invoice records for help screen
     * @param rowAt
     * @param rowTo
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find,String search,String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
    
    /**
     * Count number of Invoice records for help screen
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Invoice
     * @throws DataAccessException
     */
    public int countListForNewHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException;
       
}
