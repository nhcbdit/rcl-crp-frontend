/*-----------------------------------------------------------------------------------------------------------  
FarBillingStatementAndInvoiceLinkageDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsiisakun 17/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.far;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface FarBillingStatementAndInvoiceLinkageDao {

    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
    /**
     * list Bill Statement records for help screen
     * @param find
     * @param search
     * @param wild
     * @throws DataAccessException
     */
     public boolean isValidBillNo(String billNo) throws DataAccessException;
     
     /**
      * list Booking records for help screen
      * @param billNo
      * @throws DataAccessException
      */
}
