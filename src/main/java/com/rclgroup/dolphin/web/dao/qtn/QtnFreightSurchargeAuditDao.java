/* -------------------------------------------
 * Change Log 
## DD/MM/YY     �User-      -TaskRef-                       -ShortDescription-
01 23/07/14    RAJA    PD_CR_20140311                       Ancillary Charge Summary Report
*/
package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface QtnFreightSurchargeAuditDao {
    /**
     * @param mod
     * @return
     * @throws DataAccessException
     */
    public boolean generateTempTable(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * @param mod
     * @return
     * @throws DataAccessException
     */
    public boolean generateSurchargeCodeTempTable(RrcStandardMod mod) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    public List searchQtnFreightSurchargeAuditData(RrcStandardMod mod) throws DataAccessException;
   
   // Start :01
   /**  
     * @param bl
     * @param commodity
     * @return
     * @throws DataAccessException
     */   
    public float getWeight(String bl,String commodity) throws DataAccessException;
    //End :01
}
