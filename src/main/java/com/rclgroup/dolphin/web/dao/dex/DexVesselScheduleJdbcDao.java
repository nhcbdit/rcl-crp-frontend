/*-----------------------------------------------------------------------------------------------------------  
DexSvcVslVoyDirJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 14/10/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-      -TaskRef-       -Short Description  
01 24/11/09  KIT                         Add method getVesselName
02 23/05/11  LEE        CR_20110331-02  Creation of Booking Confirmation Report
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dex.DexVesselScheduleMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DexVesselScheduleJdbcDao extends RrcStandardDao implements DexVesselScheduleDao {
    
    public DexVesselScheduleJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }

    public boolean isValidForService(String service) {
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        if ((service!=null)&&(!service.trim().equals(""))) {
            sql.append("where SERVICE = :service ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("service",service);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidDischargeForService(String service) {
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR_DISCH ");
        if ((service!=null)&&(!service.trim().equals(""))) {
            sql.append("where SERVICE = :service ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("service",service);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidForVessel(String vessel) {
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VESSEL ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        if ((vessel!=null)&&(!vessel.trim().equals(""))) {
            sql.append("where VESSEL = :vessel ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("vessel",vessel);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    public boolean isValidDischargeForVessel(String vessel) {
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VESSEL ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR_DISCH ");
        if ((vessel!=null)&&(!vessel.trim().equals(""))) {
            sql.append("where VESSEL = :vessel ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("vessel",vessel);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidForVoyage(String voyage) {
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VOYAGE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        if ((voyage!=null)&&(!voyage.trim().equals(""))) {
            sql.append("where VOYAGE = :voyage ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("voyage",voyage);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    public boolean isValidForInVoyage(String inVoyage) {
        System.out.println("[DexVesselScheduleJdbcDao][isValidForInVoyage]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select IN_VOYAGE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_INVOY_DIR ");
        if ((inVoyage!=null)&&(!inVoyage.trim().equals(""))) {
            sql.append("where IN_VOYAGE = :voyage ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("voyage",inVoyage);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValidForInVoyage]: Finished");
        return isValid;
    }
    
    public boolean isValidDischargeForVoyage(String voyage) {
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VOYAGE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR_DISCH ");
        if ((voyage!=null)&&(!voyage.trim().equals(""))) {
            sql.append("where VOYAGE = :voyage ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("voyage",voyage);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }

    public boolean isValidForServiceWithComplete(String service) {
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SERVICE ");
        sql.append("FROM VR_DEX_SVC_VSL_VOY_DIR_PORT ");
        if((service!=null)&&(!service.trim().equals(""))){
            sql.append("WHERE SERVICE = :service ");
            sql.append("    and TRIM(STATUS) = 'Y' ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("service",service);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: Finished");
        return isValid;
    }
    
    public boolean isValidForVesselWithComplete(String vessel) {
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VESSEL ");
        sql.append("FROM VR_DEX_SVC_VSL_VOY_DIR_PORT ");
        if((vessel!=null)&&(!vessel.trim().equals(""))){
            sql.append("WHERE VESSEL = :vessel ");
            sql.append("    and TRIM(STATUS) = 'Y' ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("vessel",vessel);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: Finished");
        return isValid;
    }
    
    public boolean isValidForVoyageWithComplete(String voyage) {
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VOYAGE ");
        sql.append("FROM VR_DEX_SVC_VSL_VOY_DIR_PORT ");
        if((voyage!=null)&&(!voyage.trim().equals(""))){
            sql.append("WHERE VOYAGE = :voyage ");
            sql.append("    and TRIM(STATUS) = 'Y' ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("voyage",voyage);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: Finished");
        return isValid;
    } 
    public boolean isValidForSeqWithComplete(String seq) {
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: With seq: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SEQ ");
        sql.append("FROM VR_DEX_SVC_VSL_VOY_DIR_PORT ");
        if((seq!=null)&&(!seq.trim().equals(""))){
            sql.append("WHERE TO_CHAR(SEQ) = :seq ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("seq",seq);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DexVesselScheduleJdbcDao][isValidComplete]: Finished");
        return isValid;
    } 
    
    
    public List listForHelpScreen(String find, String search, String wild) {
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct SERVICE ");
        sql.append("    ,VESSEL ");
        sql.append("    ,VESSEL_NAME ");
        sql.append("    ,VOYAGE ");
        sql.append("    ,DIRECTION "); 
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        sql.append(sqlCriteria);
        sql.append("order by SERVICE ");
        sql.append("    ,VESSEL ");
        sql.append("    ,VOYAGE ");
        sql.append("    ,DIRECTION ");
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DexVesselScheduleMod bean = new DexVesselScheduleMod();
                           bean.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                           bean.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
                           return bean;
                       }
                   }
                );
    }
    
    public List listForDischargeHelpScreen(String find, String search, String wild) {
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct SERVICE ");
        sql.append("    ,VESSEL ");
        sql.append("    ,VESSEL_NAME ");
        sql.append("    ,VOYAGE ");
        sql.append("    ,DIRECTION "); 
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR_DISCH ");
        sql.append(sqlCriteria);
        sql.append("order by SERVICE ");
        sql.append("    ,VESSEL ");
        sql.append("    ,VOYAGE ");
        sql.append("    ,DIRECTION ");
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DexVesselScheduleMod bean = new DexVesselScheduleMod();
                           bean.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                           bean.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
                           return bean;
                       }
                   }
                );
    }
    
    public List listForHelpScreenStatusComplete(String find, String search, String wild,String status) {
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreenStatusComplete]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SERVICE ");
        sql.append("      ,VESSEL ");
        sql.append("      ,VESSEL_NAME ");
        sql.append("      ,VOYAGE ");
        sql.append("      ,DIRECTION "); 
        sql.append("      ,TO_CHAR(SEQ) as SEQ ");
        sql.append("      ,PORT ");
        sql.append("      ,TERMINAL ");
        sql.append("FROM VR_DEX_SVC_VSL_VOY_DIR_PORT ");
        
        if(RutString.nullToStr(sqlCriteria).equals("")){
            if(status!=null && !status.equals("")){
                sql.append("WHERE TRIM(STATUS) = '"+status+"' "); 
            }
        }else{
            sql.append(sqlCriteria);
            if(status!=null && !status.equals("")){
                sql.append("AND TRIM(STATUS) = '"+status+"' ");
            }
        }
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreenStatusComplete]: SQL: " + sql.toString());
        System.out.println("[DexVesselScheduleJdbcDao][listForHelpScreenStatusComplete]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            DexVesselScheduleMod svcVslVoyDir = new DexVesselScheduleMod();
                            svcVslVoyDir.setService(RutString.nullToStr(rs.getString("SERVICE")));
                            svcVslVoyDir.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                            svcVslVoyDir.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                            svcVslVoyDir.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                            svcVslVoyDir.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
                            svcVslVoyDir.setSeq(RutString.nullToStr(rs.getString("SEQ")));
                            svcVslVoyDir.setPort(RutString.nullToStr(rs.getString("PORT")));
                            svcVslVoyDir.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
                            return svcVslVoyDir;
                        }
                    }
                );
    }
    
    public List listForHelpScreenStatusComplete(String find, String search, String wild) {
        List list = listForHelpScreenStatusComplete(find,search,wild,"Y");
        return list;
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "WHERE SERVICE " + sqlWild;
            } else if (search.equalsIgnoreCase("V")) {
                sqlCriteria = "WHERE VESSEL " + sqlWild;
            } else if (search.equalsIgnoreCase("VO")) {
                sqlCriteria = "WHERE VOYAGE " + sqlWild;
            } else if (search.equalsIgnoreCase("D")) {
                sqlCriteria = "WHERE DIRECTION " + sqlWild;
            } else if (search.equalsIgnoreCase("VN")) {
                sqlCriteria = "WHERE VESSEL_NAME " + sqlWild;
            }
        }
        return sqlCriteria;
    } 
    public List getEstimateTime(String service,String veesel,String voyage,String direct,String bl) throws DataAccessException{
        System.out.println("[DexVesselScheduleJdbcDao][getEstimateTime]: Started");        
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        List listEstTime = new ArrayList();
        sql.append("SELECT (subStr(ETA,7,2)||'/'||subStr(ETA,5,2)||'/'||subStr(ETA,1,4)) AS ETA ");
        sql.append("      ,(subStr(ETD,7,2)||'/'||subStr(ETD,5,2)||'/'||subStr(ETD,1,4)) AS ETD ");
        sql.append("FROM VR_DEX_REEFERCARGO_LIST ");    
        sql.append("WHERE 1 = 1 ");   
        
        if(bl != null && !bl.equals("")){
            sql.append(" AND BL_NO = :bl ");
            
            map.put("bl",bl);
        }else{
            sql.append(" AND SERVICE = :service ");
            sql.append(" AND VESSEL  = :veesel ");
            sql.append(" AND VOYAGE  = :voyage ");
            sql.append(" AND DIRECTION = :direct ");
            
            map.put("service",service);
            map.put("veesel",veesel);
            map.put("voyage",voyage);
            map.put("direct",direct);
        }
            sql.append(" GROUP BY ETA,ETD ");
        System.out.println("[DexVesselScheduleJdbcDao][getEstimateTime]: SQL: " + sql.toString());
        System.out.println("[DexVesselScheduleJdbcDao][getEstimateTime]: Finished");


        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        while (rs.next()){
            listEstTime.add(rs.getString("ETA"));
            listEstTime.add(rs.getString("ETD"));
        }
        return listEstTime;
    }
    //## BEGIN01
     public String getVesselName(String vesselCode) {
         System.out.println("[DexVesselScheduleJdbcDao][getVesselName]: Started");
         String veseselName = "";
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT VESSEL_NAME ");
         sql.append("FROM VR_DEX_SVC_VSL_VOY_DIR_PORT ");
         if((vesselCode!=null)&&(!vesselCode.trim().equals(""))){
             sql.append("WHERE VESSEL = :vesselCode AND ");
             sql.append(" ROWNUM = 1 ");
         }
         HashMap map = new HashMap();
         map.put("vesselCode",vesselCode);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             veseselName = RutString.nullToStr(rs.getString("VESSEL_NAME"));
         } else { 
             veseselName = "";
         }
         System.out.println("[DexVesselScheduleJdbcDao][getVesselName]: Finished");
         return veseselName;
     } 
    
    //## END01
    //##BEGIN02
    /**
     * Return true if the given service, vessel, voyage, direction combination
     * is valid
     * @param serice
     * @param vessel
     * @param voyage
     * @param direction
     * @return boolean
     * */
    public boolean isValidServVessVoyDirection(String service, String vessel, 
                                               String voyage, 
                                               String direction) {
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        sql.append("select SERVICE ");
        sql.append("from VR_DEX_SVC_VSL_VOY_DIR ");
        sql.append(" where 1 = 1 and rownum = 1 ");
        if ((service!=null)&&(!service.trim().equals(""))) {
            sql.append(" and SERVICE = :service ");
            map.put("service",service);
        }
        
        if ((voyage != null)&&(!voyage.trim().equals(""))) {
            sql.append(" and VOYAGE = :voyage ");
            map.put("voyage",voyage);
        }
        
        
        if ((vessel!=null)&&(!vessel.trim().equals(""))) {
            sql.append(" and VESSEL = :vessel ");
            map.put("vessel",vessel);
        }
        
        if ((direction!=null)&&(!direction.trim().equals(""))) {
            sql.append(" and DIRECTION = :direction ");
            map.put("direction",direction);
        }
               
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        return isValid;
    }
    //## END02
}
