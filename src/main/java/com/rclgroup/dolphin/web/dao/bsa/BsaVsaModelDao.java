/*-----------------------------------------------------------------------------------------------------------  
BsaVsaModelDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaModelMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;

import java.sql.Timestamp;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface BsaVsaModelDao extends RriStandardDao {

    /**
     * list vsa records for help screen
     * @param find
     * @param search
     * @param wild
     * @param sortBy
     * @param sortByIn
     * @param status
     * @return list of vsa
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild, String sortBy, String sortByIn, String status) throws DataAccessException;

    /**
     * list all BSA model records for search screen
     * @param searchMod all search criterias in one model
     * @return list of BSA model models(VsaVsaModelMod)
     * @throws DataAccessException
     */
    public List listForSearchScreen(RcmSearchMod searchMod,String validAt,String simulation) throws DataAccessException;

    /**
     * find BSA model by using key as BSA model ID 
     * @param vsaModelId BSA model ID 
     * @return BSA model model
     * @throws DataAccessException
     */
    public BsaVsaModelMod findByKeyVsaModelId(String vsaModelId) throws DataAccessException;

    /**
     * insert a BSA model record
     * @param mod a BSA model model as input and output
     * @return whether insertion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:                             
     *                             error message: BSA_BAM01_BSA_MODEL_ID_REQ
     *                             error message: BSA_BAM01_MODEL_NAME_REQ
     *                             error message: BSA_BAM01_MODEL_NAME_EXIST with one argument: model name from input value
     *                             error message: BSA_BAM01_SIM_FLAG_REQ
     *                             error message: BSA_BAM01_SIM_FLAG_NOT_IN_RANGE with one argument: simulation flag from input value
     *                             error message: BSA_BAM01_VALID_FROM_REQ
     *                             error message: BSA_BAM01_VALID_TO_REQ
     *                             error message: BSA_BAM01_VALID_DATE_NOT_IN_RANGE 
     *                             error message: BSA_BAM01_AVG_MT_TEU_WEIGHT_REQ
     *                             error message: BSA_BAM01_IMBALANCE_WARNING_PERC_REQ
     *                             error message: BSA_BAM01_CALC_DURATION_REQ
     *                             error message: BSA_BAM01_RECORD_STATUS_REQ
     *                             error message: BSA_BAM01_STATUS_NOT_IN_RANGE with one argument: record status from input value   
     *                             error message: BSA_BAM01_RECORD_ADD_USER_REQ
     *                             error message: BSA_BAM01_RECORD_ADD_DATE_REQ
     *                             error message: BSA_BAM01_RECORD_CHANGE_USER_REQ
     *                             error message: BSA_BAM01_RECORD_CHANGE_DATE_REQ
     *                             error message: BSA_BAM01_OVERLAP_VALIDITY    
     *                             error message: BSA_BAM01_VALUE_DUP with one argument: primary key from input value  
     *                             error message: ORA-XXXXX (un-exceptional oracle error)   
     */ 
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * update a BSA model record
     * @param mod a BSA model model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: BSA_BAM01_BSA_MODEL_ID_REQ
     *                             error message: BSA_BAM01_MODEL_NAME_REQ
     *                             error message: BSA_BAM01_MODEL_NAME_EXIST with one argument: model name from input value
     *                             error message: BSA_BAM01_SIM_FLAG_REQ
     *                             error message: BSA_BAM01_SIM_FLAG_NOT_IN_RANGE with one argument: simulation flag from input value
     *                             error message: BSA_BAM01_VALID_FROM_REQ
     *                             error message: BSA_BAM01_VALID_TO_REQ
     *                             error message: BSA_BAM01_VALID_DATE_NOT_IN_RANGE 
     *                             error message: BSA_BAM01_AVG_MT_TEU_WEIGHT_REQ
     *                             error message: BSA_BAM01_IMBALANCE_WARNING_PERC_REQ
     *                             error message: BSA_BAM01_CALC_DURATION_REQ
     *                             error message: BSA_BAM01_RECORD_STATUS_REQ
     *                             error message: BSA_BAM01_STATUS_NOT_IN_RANGE with one argument: record status from input value   
     *                             error message: BSA_BAM01_RECORD_CHANGE_USER_REQ
     *                             error message: BSA_BAM01_RECORD_CHANGE_DATE_REQ
     *                             error message: BSA_BAM01_OVERLAP_VALIDITY
     *                             error message: BSA_BAM01_UPDATE_CON
     *                             error message: BSA_BAM01_BSA_MODEL_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean update(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * delete a BSA model record
     * @param mod a BSA model model
     * @return whether deletion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: BSA_BAM01_RECORD_CHANGE_DATE_REQ 
     *                             error message: BSA_BAM01_DELETE_CON  
     *                             error message: BSA_BAM01_BSA_MODEL_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * save a BSA model record with its details in BSA supported port group
     * @param masterMod a BSA model
     * @param masterOperationFlag a operation flag for a BSA model
     * @param dtlModifiedMods detail modified models as a vector of BSA supported port groups
     */ 
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, List dtlModifiedMods, String copyVsaModelId, Timestamp copyRecordChangeDate) throws DataAccessException; // ##02

    /**
     * find BSA Model Id by using key as BSA Model Name
     * @param BSA Model Name
     * @return BSA Model ID
     * @throws DataAccessException
     */ 
    public String getModelId(String modelName) throws DataAccessException;
    
    // ##01 BEGIN 
    /**
     * find BSA Calculate Durateion by using key as BSA Model Id
     * @param BSA Model Id
     * @return BSA Calculate Duration
     * @throws DataAccessException
     */ 
    public String getCalDuration(String modelId) throws DataAccessException;
    // ##01 END
}


