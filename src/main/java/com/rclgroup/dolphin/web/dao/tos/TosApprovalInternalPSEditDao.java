package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import com.rclgroup.dolphin.web.model.tos.TosApprovalInternalPSEditMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TosApprovalInternalPSEditDao extends RriStandardDao{
    /**
     * @param port
     * @param terminal
     * @param service
     * @param vessel
     * @param voyage
     * @param pcsq
     * @param sort
     * @return List
     * @throws DataAccessException
     */
    public List<TosApprovalInternalPSEditMod> getProformaHeaderList(String port,String terminal,String service,String vessel,String voyage,String pcsq,String sort) throws DataAccessException;      
    
    /**
     * @param tosProRef
     * @param sort
     * @return List
     * @throws DataAccessException
     */
    public List<TosApprovalInternalPSEditMod> getProformaDetailList(String tosProRef,String sort) throws DataAccessException;
    
    /**
     * @param modList
     * @param user
     * @throws DataAccessException
     */
    public void updateProforma(List<TosApprovalInternalPSEditMod> modList,String user) throws DataAccessException;
}
