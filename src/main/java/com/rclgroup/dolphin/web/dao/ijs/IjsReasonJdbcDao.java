package com.rclgroup.dolphin.web.dao.ijs;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ijs.IjsReasonMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class IjsReasonJdbcDao extends RrcStandardDao implements IjsReasonDao{
    public IjsReasonJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String reasonCode) throws DataAccessException {
       System.out.println("[IjsReasonJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT REASON_CODE ");
       sql.append("FROM VR_IJS_REASON_MASTER ");
       sql.append("WHERE REASON_CODE = :reasonCode");        
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("reasonCode", reasonCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[IjsReasonJdbcDao][isValid]: Finished");
        return isValid;
    }
      
    public boolean isValid(String reasonCode, String status) throws DataAccessException {
        System.out.println("[IjsReasonJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT REASON_CODE ");
        sql.append("FROM VR_IJS_REASON_MASTER ");
        sql.append("WHERE REASON_CODE = :reasonCode");          
        sql.append("AND STATUS = :status ");
        
        HashMap map = new HashMap();
        map.put("reasonCode",reasonCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[IjsReasonJdbcDao][isValid]: Finished");
        return isValid;
    }   
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[IjsReasonJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT REASON_CODE ");
        sql.append("       ,REASON_CATEGORY_DESC ");
        sql.append("       ,DESCRIPTION ");            
        sql.append("       ,RECORD_STATUS_DESC ");
        sql.append("FROM VR_IJS_REASON_MASTER ");         
        sql.append(sqlCriteria);        
        System.out.println("[IjsReasonJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[IjsReasonJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("R")){
                sqlCriteria = "WHERE REASON_CODE " + sqlWild;            
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "WHERE DESCRIPTION " + sqlWild;                        
            }else if(search.equalsIgnoreCase("C")){
                sqlCriteria = "WHERE UPPER(REASON_CATEGORY_DESC) " + sqlWild;           
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE UPPER(RECORD_STATUS_DESC) " + sqlWild;
            }        
        }
        return sqlCriteria;
    }        
   
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    
    private IjsReasonMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        IjsReasonMod reason = new IjsReasonMod();
        reason.setReasonCode(RutString.nullToStr(rs.getString("REASON_CODE")));   
        reason.setCategory(RutString.nullToStr(rs.getString("REASON_CATEGORY_DESC")));        
        reason.setDesc(RutString.nullToStr(rs.getString("DESCRIPTION")));                
        reason.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS_DESC")));        
        return reason;
    } 
    
}
