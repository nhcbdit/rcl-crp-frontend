/*-----------------------------------------------------------------------------------------------------------  
TosDataMaintenanceJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 24/02/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.tos.TosDeleteDischargeListMod;
import com.rclgroup.dolphin.web.model.tos.TosReopenLoadListMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class TosDataMaintenanceJdbcDao extends RrcStandardDao implements TosDataMaintenanceDao {
    private ReopenLoadListStoreProcedure reopenLoadListProc;
    private DeleteDischargeListStoreProcedure deleteDischargeListStoreProc;
    
    public TosDataMaintenanceJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        reopenLoadListProc = new ReopenLoadListStoreProcedure(getJdbcTemplate());
        deleteDischargeListStoreProc = new DeleteDischargeListStoreProcedure(getJdbcTemplate());
    }
    
    public List findTosBKP009(String portType, String vessel, String voyage, String port, String terminal) throws DataAccessException {
        if (!RutString.isEmptyString(portType) && !RutString.isEmptyString(vessel) && !RutString.isEmptyString(voyage) && !RutString.isEmptyString(port) && !RutString.isEmptyString(terminal)) 
        {
            System.out.println("[TosDataMaintenanceJdbcDao][findTosBKP009]: Started");
            
            StringBuffer sql = new StringBuffer();
            sql.append("select STR_RECORD_DATA ");
            sql.append("from VR_TOS_DMTN_BKP009 vr ");
            sql.append("where vr.VESSEL = :vessel ");
            sql.append("    and vr.VOYAGE = :voyage ");
            
            if ("POL".equals(portType)) {
                sql.append("    and vr.POL = :port ");
                sql.append("    and vr.POL_TERMINAL = :terminal ");    
            }
            
            HashMap map = new HashMap();
            map.put("vessel", vessel);
            map.put("voyage", voyage);
            map.put("port", port);
            map.put("terminal", terminal);
            
            System.out.println("[TosDataMaintenanceJdbcDao][findTosBKP009]: sql = " + sql.toString());
            System.out.println("[TosDataMaintenanceJdbcDao][findTosBKP009]: Finished");
            
            return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return RutString.nullToStr(rs.getString("STR_RECORD_DATA"));
                        }
                    });

        } else {
            return new ArrayList();
        }
    }
    
    public List findTosOnboardList(String portType, String vessel, String voyage, String port, String terminal) throws DataAccessException {
        if (!RutString.isEmptyString(portType) && !RutString.isEmptyString(vessel) && !RutString.isEmptyString(voyage) && !RutString.isEmptyString(port) && !RutString.isEmptyString(terminal)) 
        {
            System.out.println("[TosDataMaintenanceJdbcDao][findTosOnboardList]: Started");
            
            StringBuffer sql = new StringBuffer();
            sql.append("select STR_RECORD_DATA ");
            sql.append("from VR_TOS_DMTN_ONBOARD_LIST vr ");
            sql.append("where vr.VESSEL = :vessel ");
            sql.append("    and vr.VOYAGE = :voyage ");
            
            if ("POL".equals(portType)) {
                sql.append("    and vr.POL = :port ");
                sql.append("    and vr.POL_TERMINAL = :terminal ");    
            } else if ("POD".equals(portType)) {
                sql.append("    and vr.POD = :port ");
                sql.append("    and vr.POD_TERMINAL = :terminal ");
            }
            
            HashMap map = new HashMap();
            map.put("vessel", vessel);
            map.put("voyage", voyage);
            map.put("port", port);
            map.put("terminal", terminal);
            
            System.out.println("[TosDataMaintenanceJdbcDao][findTosOnboardList]: sql = " + sql.toString());
            System.out.println("[TosDataMaintenanceJdbcDao][findTosOnboardList]: Finished");
            
            return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return RutString.nullToStr(rs.getString("STR_RECORD_DATA"));
                        }
                    });

        } else {
            return new ArrayList();
        }
    }
    
    public List findTosDischList(String portType, String vessel, String voyage, String port, String terminal) throws DataAccessException {
        if (!RutString.isEmptyString(portType) && !RutString.isEmptyString(vessel) && !RutString.isEmptyString(voyage) && !RutString.isEmptyString(port) && !RutString.isEmptyString(terminal)) 
        {
            System.out.println("[TosDataMaintenanceJdbcDao][findTosDischList]: Started");
            
            StringBuffer sql = new StringBuffer();
            sql.append("select STR_RECORD_DATA ");
            sql.append("from VR_TOS_DMTN_DISCHLIST_DTL vr ");
            sql.append("where vr.VESSEL = :vessel ");
            sql.append("    and vr.VOYAGE = :voyage ");
            
            if ("POL".equals(portType)) {
                sql.append("    and vr.POL = :port ");
                sql.append("    and vr.POL_TERMINAL = :terminal ");    
            } else if ("POD".equals(portType)) {
                sql.append("    and vr.POD = :port ");
                sql.append("    and vr.POD_TERMINAL = :terminal ");
            }
            
            HashMap map = new HashMap();
            map.put("vessel", vessel);
            map.put("voyage", voyage);
            map.put("port", port);
            map.put("terminal", terminal);
            
            System.out.println("[TosDataMaintenanceJdbcDao][findTosDischList]: sql = " + sql.toString());
            System.out.println("[TosDataMaintenanceJdbcDao][findTosDischList]: Finished");
            
            return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            return RutString.nullToStr(rs.getString("STR_RECORD_DATA"));
                        }
                    });

        } else {
            return new ArrayList();
        }
    }
    
    public boolean reopenLoadList(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return reopenLoadListProc.reopen(mod);
    }
    
    public boolean deleteDischargeList(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return deleteDischargeListStoreProc.delete(mod);
    }
    
    protected class ReopenLoadListStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_TOS_DATA_MAINTENANCE.PRR_REOPEN_LOAD_LIST";
        
        protected ReopenLoadListStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_flag_execute", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_return_result", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_return_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean reopen(RrcStandardMod mod) {
            return reopen(mod, mod);
        }
        
        protected boolean reopen(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if (inputMod instanceof TosReopenLoadListMod && outputMod instanceof TosReopenLoadListMod) {
                TosReopenLoadListMod aInputMod = (TosReopenLoadListMod) inputMod;
                TosReopenLoadListMod aOutputMod = (TosReopenLoadListMod) outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_vessel", RutString.nullToStr(aInputMod.getVessel()));
                inParameters.put("p_voyage", RutString.nullToStr(aInputMod.getVoyage()));
                inParameters.put("p_pol", RutString.nullToStr(aInputMod.getPol()));
                inParameters.put("p_pol_terminal", RutString.nullToStr(aInputMod.getPolTerminal()));
                inParameters.put("p_flag_execute", RutString.nullToStr(aInputMod.getFlagExecute()));
                inParameters.put("p_return_result", RutString.nullToStr(aInputMod.getReturnResult()));
                inParameters.put("p_return_change_user", RutString.nullToStr(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_pol = "+inParameters.get("p_pol"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_pol_terminal = "+inParameters.get("p_pol_terminal"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_flag_execute = "+inParameters.get("p_flag_execute"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_return_result = "+inParameters.get("p_return_result"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_return_change_user = "+inParameters.get("p_return_change_user"));
                System.out.println("[TosDataMaintenanceJdbcDao][ReopenLoadListStoreProcedure][reopen]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setReturnResult((RutString.nullToStr((String)outParameters.get("p_return_result"))));
                    aOutputMod.setRecordChangeDate((Timestamp)outParameters.get("p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
 
    protected class DeleteDischargeListStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_TOS_DATA_MAINTENANCE.PRR_DELETE_DISCHARGE_LIST";
        
        protected DeleteDischargeListStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_flag_execute", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_return_result", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_return_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean delete(RrcStandardMod mod) {
            return delete(mod, mod);
        }
        
        protected boolean delete(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if (inputMod instanceof TosDeleteDischargeListMod && outputMod instanceof TosDeleteDischargeListMod) {
                TosDeleteDischargeListMod aInputMod = (TosDeleteDischargeListMod) inputMod;
                TosDeleteDischargeListMod aOutputMod = (TosDeleteDischargeListMod) outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_vessel", RutString.nullToStr(aInputMod.getVessel()));
                inParameters.put("p_voyage", RutString.nullToStr(aInputMod.getVoyage()));
                inParameters.put("p_pod", RutString.nullToStr(aInputMod.getPod()));
                inParameters.put("p_pod_terminal", RutString.nullToStr(aInputMod.getPodTerminal()));
                inParameters.put("p_flag_execute", RutString.nullToStr(aInputMod.getFlagExecute()));
                inParameters.put("p_return_result", RutString.nullToStr(aInputMod.getReturnResult()));
                inParameters.put("p_return_change_user", RutString.nullToStr(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][delete]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][delete]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][delete]: p_pod = "+inParameters.get("p_pod"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][delete]: p_pod_terminal = "+inParameters.get("p_pod_terminal"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][delete]: p_flag_execute = "+inParameters.get("p_flag_execute"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][reopen]: p_return_result = "+inParameters.get("p_return_result"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][reopen]: p_return_change_user = "+inParameters.get("p_return_change_user"));
                System.out.println("[TosDataMaintenanceJdbcDao][DeleteDischargeListStoreProcedure][reopen]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setReturnResult((RutString.nullToStr((String)outParameters.get("p_return_result"))));
                    aOutputMod.setRecordChangeDate((Timestamp)outParameters.get("p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
       
}
