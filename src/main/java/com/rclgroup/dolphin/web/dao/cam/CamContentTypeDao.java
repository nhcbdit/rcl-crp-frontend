/*-----------------------------------------------------------------------------------------------------------  
CamContentTypeDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 03/04/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamContentTypeMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;

import java.sql.Date;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamContentTypeDao {

    /**
     * list all content type records for search screen
     * @param searchMod all search criterias in one model
     * @return list of content type models(CamContentTypeMod)
     * @throws DataAccessException
     */
    public List listForSearchScreen(RcmSearchMod searchMod) throws DataAccessException;

    /**
     * find content type by using key as content code 
     * @param code content code 
     * @return content type model
     * @throws DataAccessException
     */
    public CamContentTypeMod findByKeyCode(String code) throws DataAccessException;

    /**
     * insert a content type record
     * @param contentTypeMod a content type model
     * @param recordChangeDate record change date as input and output parameter
     * @return new content code
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: CONTENT_TYPE_CODE_REQ
     *                             error message: CONTENT_TYPE_CODE_DUP  with one argument: content code from input value   
     *                             error message: CONTENT_TYPE_TYPE_REQ
     *                             error message: CONTENT_TYPE_MODULE_NAME_REQ
     *                             error message: CONTENT_TYPE_STATUS_REQ
     *                             error message: CONTENT_TYPE_STATUS_NOT_IN_RANGE with one argument: record status from input value 
     *                             error message: CONTENT_TYPE_RAU_REQ 
     *                             error message: ORA-XXXXX (un-exceptional oracle error) 
     */    
    public String insert(CamContentTypeMod contentTypeMod, Date recordChangeDate) throws DataAccessException;
    
    /**
     * update a content type record
     * @param contentTypeMod a content type model
     * @return record change date
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: CONTENT_TYPE_CODE_REQ 
     *                             error message: CONTENT_TYPE_CODE_DUP with one argument: content code from input value 
     *                             error message: CONTENT_TYPE_CODE_NOT_FOUND with one argument: content code from input value  
     *                             error message: CONTENT_TYPE_TYPE_REQ 
     *                             error message: CONTENT_TYPE_MODULE_NAME_REQ 
     *                             error message: CONTENT_TYPE_STATUS_REQ 
     *                             error message: CONTENT_TYPE_STATUS_NOT_IN_RANGE with one argument: record status from input value  
     *                             error message: CONTENT_TYPE_RCU_REQ 
     *                             error message: CONTENT_TYPE_RCD_REQ 
     *                             error message: CONTENT_TYPE_RCD_CON 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)  
     */ 
    public Date update(CamContentTypeMod contentTypeMod) throws DataAccessException;
    
    public void delete(CamContentTypeMod contentTypeMod) throws DataAccessException;

    /**
     * get a list of all module names and module descriptions 
     * @return a list of content type's module name, description models(CamContentTypeModuleNameDescriptionMod) 
     * @throws DataAccessException
     */
    public List getContentTypeModuleNameDescriptionList() throws DataAccessException;
}


