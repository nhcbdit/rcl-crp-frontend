/*-----------------------------------------------------------------------------------------------------------  
DexSvcVslVoyDirDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 14/10/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
01 23/05/11  LEE        CR_20110331-02  Creation of Booking Confirmation Report 
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.dex;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface DexVesselScheduleDao {

    /**
     * @param service
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForService(String service) throws DataAccessException;
    
    /**
     * @param service
     * @return
     * @throws DataAccessException
     */
    public boolean isValidDischargeForService(String service) throws DataAccessException;

    /**
     * @param vessel
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVessel(String vessel) throws DataAccessException;
    
    /**
     * @param vessel
     * @return
     * @throws DataAccessException
     */
    public boolean isValidDischargeForVessel(String vessel) throws DataAccessException;

    /**
     * @param voyage
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVoyage(String voyage) throws DataAccessException;
    /**
     * @param inVoyage
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForInVoyage(String inVoyage) throws DataAccessException;
    
    /**
     * @param voyage
     * @return
     * @throws DataAccessException
     */
    public boolean isValidDischargeForVoyage(String voyage) throws DataAccessException;

    /**
     * @param service
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForServiceWithComplete(String service) throws DataAccessException;

    /**
     * @param vessel
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVesselWithComplete(String vessel) throws DataAccessException;

    /**
     * @param voyage
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVoyageWithComplete(String voyage) throws DataAccessException;
    
    /**
     * @param find
     * @param search
     * @param wild
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
    /**
     * @param find
     * @param search
     * @param wild
     * @return
     * @throws DataAccessException
     */
    public List listForDischargeHelpScreen(String find,String search,String wild) throws DataAccessException;
    
    /**
     * @param find
     * @param search
     * @param wild
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreenStatusComplete(String find,String search,String wild) throws DataAccessException;
    
    /**
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreenStatusComplete(String find,String search,String wild,String status) throws DataAccessException;
    
    /**
     * @param service
     * @param veesel
     * @param voyage
     * @param direct
     * @param bl
     * @return
     * @throws DataAccessException
     */
    public List getEstimateTime(String service,String veesel,String voyage,String direct,String bl) throws DataAccessException;
    
    /**
     * @param seq
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForSeqWithComplete(String seq) throws DataAccessException;

    /**
     * @param vesselCode
     * @return
     * @throws DataAccessException
     */
    public String  getVesselName(String vesselCode) throws DataAccessException;
    
    //## BEGIN01
    /**
     * @param service
     * @param vessel
     * @param voyage
     * @param direction
     * @return
     * @throws DataAccessException
     */
    public boolean isValidServVessVoyDirection(String service, String vessel, String voyage, String direction) throws DataAccessException;
    //## END01
}

