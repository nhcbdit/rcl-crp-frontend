/*-----------------------------------------------------------------------------------------------------------  
DimManifestJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 19/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 22/06/11 NIP                       BUG 505
02 07/06/12 SON                       Add billing status for show in listForTeminalStorage
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.dim.DimCargoManifestMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestMod;
import com.rclgroup.dolphin.web.model.dim.TosTerminalStorageMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DimManifestJdbcDao extends RrcStandardDao implements DimManifestDao {
    private InsertStoreProcedure insertStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    
    public DimManifestJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
    }
    
    public String makeCargoManifestSqlStatment(String serviceCode, String vesselCode, String voyageCode, String direction, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) 
    {
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct BL_NO ");
        sb.append("    ,BOOKING_NO ");
        sb.append("    ,BL_ID_NAME ");
        sb.append("    ,HBL_NO ");
        sb.append("    ,BL_CREATE ");
        sb.append("    ,IMP_STATUS ");
        sb.append("    ,EXP_STATUS ");
        sb.append("    ,POR ");
        sb.append("    ,POL_REAL AS POL ");
        sb.append("    ,POT ");
        sb.append("    ,POD ");
        sb.append("    ,TO_TERMINAL ");
        sb.append("    ,DEL ");
        sb.append("    ,FINALDEL ");
        sb.append("    ,SERVICE ");
        sb.append("    ,VESSEL ");
        sb.append("    ,VOYAGE ");
        sb.append("    ,DIRECTION ");
        sb.append("    ,CONSIGNEE_CODE ");
        sb.append("from VR_DIM_CARGO_RPT ");
           
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(serviceCode)) {
            sbWhere.append(" and SERVICE = '"+serviceCode+"' ");
        }
        if (!RutString.isEmptyString(vesselCode)) {
            sbWhere.append(" and VESSEL = '"+vesselCode+"' ");
        }
        if (!RutString.isEmptyString(voyageCode)) {
            sbWhere.append(" and VOYAGE = '"+voyageCode+"' ");
        }
        if (!RutString.isEmptyString(direction)) {
            sbWhere.append(" and DIRECTION = '"+direction+"' ");
        }
        
        // ##01 BEGIN
        if (!RutString.isEmptyString(pol)) {
            /*if(!RutString.isEmptyString(vesselCode)){// if criterial have vessel must use router of POL real.(VOYAGE_SEQ is min)
                sbWhere.append(" and (POL_REAL = '"+pol+"' ");
                sbWhere.append("      and VOYAGE_SEQ = 1"); 
                sbWhere.append("     )");
            }else// all router have start router is POL
                sbWhere.append(" and POL_REAL = '"+pol+"' ");*/
            sbWhere.append(" and POL_REAL = '"+pol+"' ");
        }
        // ## o1 END
        
        if (!RutString.isEmptyString(pot)) {
            sbWhere.append(" and POT = '"+pot+"' ");
        }
        
        // ##01 BEGIN
        if (!RutString.isEmptyString(pod)) {
            /*if(!RutString.isEmptyString(vesselCode)){// if criterial have vessel must use router of POD real.(VOYAGE_SEQ is max)
                sbWhere.append(" and (POD_REAL = '"+pod+"' ");
                sbWhere.append("      and VOYAGE_SEQ = (select max(VOYAGE_SEQ) from idp005 where syblno=BL_NO)");
                sbWhere.append("     )");
            }else// all router have end router is POD
                sbWhere.append(" and POD_REAL = '"+pod+"' ");*/
            sbWhere.append(" and POD_REAL = '"+pod+"' ");
        }
        // ##01 END
        
        if (!RutString.isEmptyString(del)) {
            sbWhere.append(" and FINALDEL = '"+del+"' ");
        }
        if (!RutString.isEmptyString(dischargeTerminal)) {
            sbWhere.append(" and TO_TERMINAL = '"+dischargeTerminal+"' ");
        }
        if (!RutString.isEmptyString(blNo)) {
            sbWhere.append(" and BL_NO = '"+blNo+"' ");
        }
        if (!RutString.isEmptyString(blId)) {
            sbWhere.append(" and BL_ID = '"+blId+"' ");
        }
        if (!RutString.isEmptyString(hbl)) {
            sbWhere.append(" and HBL_NO = '"+hbl+"' ");
        }
        if (!RutString.isEmptyString(blStatus)) {
            sbWhere.append(" and IMP_STATUS_ID = '"+blStatus+"' ");
        }
        if (!RutString.isEmptyString(consigneeCode)) {
            sbWhere.append(" and CONSIGNEE_CODE = '"+consigneeCode+"' ");
        }
        if (!RutString.isEmptyString(fsc)) {
            sbWhere.append(" and FSC = '"+fsc+"' ");
        }
        if (!RutString.isEmptyString(bookingNo)) {
            sbWhere.append(" and BOOKING_NO = '"+bookingNo+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
        }
        
        sb.append("order by BL_NO ");
        sb.append("    ,BL_ID_NAME ");
        sb.append("    ,HBL_NO ");
        sb.append("    ,IMP_STATUS ");
        sb.append("    ,EXP_STATUS ");
        
        return sb.toString();
    }
    
    public String makeCargoManifestSqlStatment(String invoyagePort, String sessionId, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc,String vesselCode) // ##01 add vesselCode
    {
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct BL_NO ");
        sb.append("    ,BOOKING_NO ");
        sb.append("    ,BL_ID_NAME ");
        sb.append("    ,HBL_NO ");
        sb.append("    ,BL_CREATE ");
        sb.append("    ,IMP_STATUS ");
        sb.append("    ,EXP_STATUS ");
        sb.append("    ,POR ");
        sb.append("    ,POL_REAL AS POL ");
        sb.append("    ,POT ");
        sb.append("    ,POD ");
        sb.append("    ,TO_TERMINAL ");
        sb.append("    ,DEL ");
        sb.append("    ,FINALDEL ");
        sb.append("    ,SERVICE ");
        sb.append("    ,VESSEL ");
        sb.append("    ,VOYAGE ");
        sb.append("    ,DIRECTION ");
        sb.append("    ,CONSIGNEE_CODE ");
        sb.append("from VR_DIM_CARGO_RPT vr ");
           
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(sessionId) && !RutString.isEmptyString(invoyagePort)) {
            sbWhere.append(" and exists ( select 1 ");
            sbWhere.append("    from VR_RCM_INVOYAGE_BROWSER ib ");
            sbWhere.append("    where vr.SERVICE = ib.SERVICE ");
            sbWhere.append("        and vr.VESSEL = ib.VESSEL ");
            sbWhere.append("        and vr.VOYAGE = ib.VOYAGE ");
            sbWhere.append("        and vr.POL = ib.PORT ");
            sbWhere.append("        and vr.POD = '"+invoyagePort+"' ");
            sbWhere.append("        and ib.SESSION_ID = '"+sessionId+"' ) ");
        }

        // ##01 BEGIN
        if (!RutString.isEmptyString(pol)) {
            /*if(!RutString.isEmptyString(vesselCode)){// if criterial have vessel must use router of POL real.(VOYAGE_SEQ is min)
                sbWhere.append(" and (POL_REAL = '"+pol+"' ");
                sbWhere.append("      and VOYAGE_SEQ = 1"); 
                sbWhere.append("     )");
            }else// all router have start router is POL
                sbWhere.append(" and POL_REAL = '"+pol+"' ");*/
            sbWhere.append(" and POL_REAL = '"+pol+"' ");
        }
        // ## o1 END
        
        if (!RutString.isEmptyString(pot)) {
            sbWhere.append(" and POT = '"+pot+"' ");
        }
        
        // ##01 BEGIN
        if (!RutString.isEmptyString(pod)) {
            /*if(!RutString.isEmptyString(vesselCode)){// if criterial have vessel must use router of POD real.(VOYAGE_SEQ is max)
                sbWhere.append(" and (POD_REAL = '"+pod+"' ");
                sbWhere.append("      and VOYAGE_SEQ = (select max(VOYAGE_SEQ) from idp005 where syblno=BL_NO)");
                sbWhere.append("     )");
            }else// all router have end router is POD
                sbWhere.append(" and POD_REAL = '"+pod+"' ");*/
            sbWhere.append(" and POD_REAL = '"+pod+"' ");
        }
        // ##01 END
        
        if (!RutString.isEmptyString(del)) {
            sbWhere.append(" and FINALDEL = '"+del+"' ");
        }
        if (!RutString.isEmptyString(dischargeTerminal)) {
            sbWhere.append(" and TO_TERMINAL = '"+dischargeTerminal+"' ");
        }
        if (!RutString.isEmptyString(blNo)) {
            sbWhere.append(" and BL_NO = '"+blNo+"' ");
        }
        if (!RutString.isEmptyString(blId)) {
            sbWhere.append(" and BL_ID = '"+blId+"' ");
        }
        if (!RutString.isEmptyString(hbl)) {
            sbWhere.append(" and HBL_NO = '"+hbl+"' ");
        }
        if (!RutString.isEmptyString(blStatus)) {
            sbWhere.append(" and IMP_STATUS_ID = '"+blStatus+"' ");
        }
        if (!RutString.isEmptyString(consigneeCode)) {
            sbWhere.append(" and CONSIGNEE_CODE = '"+consigneeCode+"' ");
        }
        if (!RutString.isEmptyString(fsc)) {
            sbWhere.append(" and FSC = '"+fsc+"' ");
        }
        if (!RutString.isEmptyString(bookingNo)) {
            sbWhere.append(" and BOOKING_NO = '"+bookingNo+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
        }
        
        sb.append("order by BL_NO ");
        sb.append("    ,BL_ID_NAME ");
        sb.append("    ,HBL_NO ");
        sb.append("    ,IMP_STATUS ");
        sb.append("    ,EXP_STATUS ");
        
        return sb.toString();
    }
    
    public List listForCargoManifest(String serviceCode, String vesselCode, String voyageCode, String direction, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) 
    {
        return this.listForCargoManifest(0, 0, serviceCode, vesselCode, voyageCode, direction, pol, pot, pod, dischargeTerminal, del, blNo, hbl, blStatus, blId, consigneeCode, bookingNo, fsc);
    }
    
    public List listForCargoManifest(int recFirst, int recLast, String serviceCode, String vesselCode, String voyageCode, String direction, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) throws DataAccessException  
    {
        System.out.println("[DimManifestJdbcDao][listForCargoManifest]: Started");
        
        String sqlStatement = this.makeCargoManifestSqlStatment(serviceCode, vesselCode, voyageCode, direction, pol, pot, pod, dischargeTerminal, del, blNo, hbl, blStatus, blId, consigneeCode, bookingNo, fsc);
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[DimManifestJdbcDao][listForCargoManifest]: sql = "+sqlStatement);
        System.out.println("[DimManifestJdbcDao][listForCargoManifest]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimManifestMod mf = new DimManifestMod();
                           mf.setBlNo(RutString.nullToStr(rs.getString("BL_NO")));
                           mf.setBooking(RutString.nullToStr(rs.getString("BOOKING_NO")));
                           mf.setBlId(RutString.nullToStr(rs.getString("BL_ID_NAME")));
                           mf.setHblNo(RutString.nullToStr(rs.getString("HBL_NO")));
                           mf.setBlCreationDate(rs.getInt("BL_CREATE"));
                           mf.setImportStatus(RutString.nullToStr(rs.getString("IMP_STATUS")));
                           mf.setExportStatus(RutString.nullToStr(rs.getString("EXP_STATUS")));
                           mf.setPorCode(RutString.nullToStr(rs.getString("POR")));
                           mf.setPolCode(RutString.nullToStr(rs.getString("POL")));
                           mf.setPotCode(RutString.nullToStr(rs.getString("POT")));
                           mf.setPodCode(RutString.nullToStr(rs.getString("POD")));
                           mf.setDelCode(RutString.nullToStr(rs.getString("DEL")));
                           mf.setFinalDelCode(RutString.nullToStr(rs.getString("FINALDEL")));
                           mf.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           mf.setVesselCode(RutString.nullToStr(rs.getString("VESSEL")));
                           mf.setVoyageCode(RutString.nullToStr(rs.getString("VOYAGE")));
                           mf.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                           mf.setConsigneeCode(RutString.nullToStr(rs.getString("CONSIGNEE_CODE")));
                           mf.setDischargeTerminal(RutString.nullToStr(rs.getString("TO_TERMINAL")));
                           return mf;
                       }
                   });
    }
    
    public List listForCargoManifestByInVoyageList(int recFirst, int recLast, String invoyagePort, String sessionId, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc,String vesselCode) throws DataAccessException // ##01 add vesselCode
    {
        System.out.println("[DimManifestJdbcDao][listForCargoManifestByInVoyageList]: Started");
        
        String sqlStatement = this.makeCargoManifestSqlStatment(invoyagePort, sessionId, pol, pot, pod, dischargeTerminal, del, blNo, hbl, blStatus, blId, consigneeCode, bookingNo, fsc, vesselCode);// ##01 add vesselCode
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[DimManifestJdbcDao][listForCargoManifestByInVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimManifestJdbcDao][listForCargoManifestByInVoyageList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimManifestMod mf = new DimManifestMod();
                           mf.setBlNo(RutString.nullToStr(rs.getString("BL_NO")));
                           mf.setBooking(RutString.nullToStr(rs.getString("BOOKING_NO")));
                           mf.setBlId(RutString.nullToStr(rs.getString("BL_ID_NAME")));
                           mf.setHblNo(RutString.nullToStr(rs.getString("HBL_NO")));
                           mf.setBlCreationDate(rs.getInt("BL_CREATE"));
                           mf.setImportStatus(RutString.nullToStr(rs.getString("IMP_STATUS")));
                           mf.setExportStatus(RutString.nullToStr(rs.getString("EXP_STATUS")));
                           mf.setPorCode(RutString.nullToStr(rs.getString("POR")));
                           mf.setPolCode(RutString.nullToStr(rs.getString("POL")));
                           mf.setPotCode(RutString.nullToStr(rs.getString("POT")));
                           mf.setPodCode(RutString.nullToStr(rs.getString("POD")));
                           mf.setDelCode(RutString.nullToStr(rs.getString("DEL")));
                           mf.setFinalDelCode(RutString.nullToStr(rs.getString("FINALDEL")));
                           mf.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           mf.setVesselCode(RutString.nullToStr(rs.getString("VESSEL")));
                           mf.setVoyageCode(RutString.nullToStr(rs.getString("VOYAGE")));
                           mf.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                           mf.setConsigneeCode(RutString.nullToStr(rs.getString("CONSIGNEE_CODE")));
                           mf.setDischargeTerminal(RutString.nullToStr(rs.getString("TO_TERMINAL")));
                           return mf;
                       }
                   });
    }
    
    
    public int countListForCargoManifest(String serviceCode, String vesselCode, String voyageCode, String direction, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) throws DataAccessException 
    {
        System.out.println("[DimManifestJdbcDao][countListForCargoManifest]: Started");
        
        String sqlStatement = this.makeCargoManifestSqlStatment(serviceCode, vesselCode, voyageCode, direction, pol, pot, pod, dischargeTerminal, del, blNo, hbl, blStatus, blId, consigneeCode, bookingNo, fsc);
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[DimManifestJdbcDao][countListForCargoManifest]: sql = "+sqlStatement);
        System.out.println("[DimManifestJdbcDao][countListForCargoManifest]: Finished");
        
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStatement, new HashMap(), Integer.class);
    }
    
    public int countListForCargoManifestByInVoyageList(String invoyagePort, String sessionId, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc,String vesselCode) throws DataAccessException // ##01 add vesselCode
    {
        System.out.println("[DimManifestJdbcDao][countListForCargoManifestByInVoyageList]: Started");
        
        String sqlStatement = this.makeCargoManifestSqlStatment(invoyagePort, sessionId, pol, pot, pod, dischargeTerminal, del, blNo, hbl, blStatus, blId, consigneeCode, bookingNo, fsc,vesselCode);// ##01 add vesselCode
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[DimManifestJdbcDao][countListForCargoManifestByInVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimManifestJdbcDao][countListForCargoManifestByInVoyageList]: Finished");
        
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStatement, new HashMap(), Integer.class);
    }

    public List listForTeminalStorage(String service,String vessel,String voyage,String direction,String port,String terminal,String fromDate,String toDate ) {
        System.out.println("[DimManifestJdbcDao][listForTerminalStorageReport]: Started");  
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT distinct vr.PORT ");
        sql.append("       ,vr.TERMINAL ");
        sql.append("       ,vr.VESSEL ");
        sql.append("       ,vr.VOYAGE  ");
        sql.append("       ,vr.SERVICE ");
        sql.append("       ,vr.ATA ");
        sql.append("       ,vr.ATD ");
        sql.append("       ,CASE WHEN B.BILLING_STATUS = 'X' THEN 'Open' ");
        sql.append("                WHEN B.BILLING_STATUS = 'P' THEN 'Pending' ");
        sql.append("                WHEN B.BILLING_STATUS = 'N' THEN 'Finalized' ");
        sql.append("                ELSE 'Not Found' END AS BILLING_STATUS  ");
        sql.append("       FROM VR_STR_TER_STORAGE vr LEFT JOIN STORAGE_BILLING B ON ");
        sql.append("              vr.SERVICE = B.SERVICE AND ");
        sql.append("              vr.VESSEL = B.VESSEL AND ");
        sql.append("              vr.VOYAGE = B.VOYAGE AND ");
        sql.append("              vr.TERMINAL = B.DEPOT_CODE  ");
        
        StringBuffer strWhere = new StringBuffer("");
        if(!RutString.isEmptyString(service)){
            strWhere.append(" AND vr.SERVICE = '" + service + "' ");
        }
        if(!RutString.isEmptyString(vessel)){
            strWhere.append(" AND vr.VESSEL = '" + vessel + "' ");
        }
        if(!RutString.isEmptyString(voyage)){
            strWhere.append(" AND vr.VOYAGE = '" + voyage + "' ");
        }
        if(!RutString.isEmptyString(direction)){
            strWhere.append(" AND vr.DIRECTION = '" + direction + "' ");
        }
        if(!RutString.isEmptyString(port)){
            strWhere.append(" AND vr.PORT = '" + port + "' ");
        }
        if(!RutString.isEmptyString(terminal)){
            strWhere.append(" AND vr.TERMINAL = '" + terminal + "' ");
        }
        if(!RutString.isEmptyString(fromDate)){
            strWhere.append(" AND vr.ATA >= " + fromDate + " ");
        }
        if(!RutString.isEmptyString(toDate)){
            strWhere.append(" AND vr.ATD <= " + toDate + " ");
        }
        
        if(strWhere != null && strWhere.length() >0){
            sql.append(" WHERE "+strWhere.substring(4) );
        }
        
        sql.append(" ORDER BY vr.VESSEL , vr.VOYAGE ,vr.ATA ");
        
        System.out.println("[DimManifestJdbcDao][listForCargoScreen]: SQL: "+sql.toString());
        System.out.println("[DimManifestJdbcDao][listForCargoScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                TosTerminalStorageMod tm = new TosTerminalStorageMod();
                tm.setPortCode(RutString.nullToStr(rs.getString("PORT")));
                tm.setTerminalCode(RutString.nullToStr(rs.getString("TERMINAL")));
                tm.setVesselCode(RutString.nullToStr(rs.getString("VESSEL")));
                tm.setVoyageCode(RutString.nullToStr(rs.getString("VOYAGE")));
                tm.setServiceCode(RutString.nullToStr(rs.getString("SERVICE")));
                tm.setAtaDate(rs.getInt("ATA"));
                tm.setAtdDate(rs.getInt("ATD"));
                tm.setBillingStatus(rs.getString("BILLING_STATUS"));
                return tm;
             }
         });
    }

    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        return insertStoreProcedure.insert(mod);
    }

    public boolean isValidWithSessionId(String sessionId) throws DataAccessException  {
        System.out.println("[DimManifesJdbcDao][isValidWithSessionId]: Started ");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append(" select DISTINCT SESSION_ID ");
        sql.append(" from DIM_CARGO_MANIFEST_RPT ");
        sql.append(" where SESSION_ID = '"+ sessionId +"' ");
        sql.append("      and rownum =1 ");
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),new HashMap());
        if(rs.next()){
            isValid = true;
        }else{
            isValid = false;
        }
        System.out.println("[DimManifestJdbcDao][isValidWithSessionId]: sql : "+sql.toString());
        System.out.println("[DimManifestJdbcDao][isValidWithSessionId]: isValid : "+isValid);
        System.out.println("[DimManifestJdbcDao][isValidWithSessionId]: Finished ");
        return isValid;
    }

    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        boolean isSuccess = false;
        if(mod instanceof DimCargoManifestMod){
            DimCargoManifestMod aInputMod = (DimCargoManifestMod)mod;
            if(isValidWithSessionId(aInputMod.getSessionId())){
                deleteStoreProcedure.delete(mod);
                isSuccess = true;
            }else{
                isSuccess = false;
            }
        }
        return isSuccess;
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DIM_CARGO_MANIFEST.PRR_INS_DIM_CARGO_MANIFEST";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate,STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_bl_no",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_add_user",Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id",Types.VARCHAR));
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod){
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod , RrcStandardMod outputMod){
            boolean isSuccess = false;
            DimCargoManifestMod aInputMod = (DimCargoManifestMod)inputMod;
            DimCargoManifestMod aOutputMod = (DimCargoManifestMod)outputMod;
            if((inputMod instanceof DimCargoManifestMod) && (outputMod instanceof DimCargoManifestMod)){
                
                Map inParameters = new HashMap(3);
                
                inParameters.put("p_bl_no",aInputMod.getBlNo());
                inParameters.put("p_record_add_user",aInputMod.getRecordAddUser());
                inParameters.put("p_session_id",aInputMod.getSessionId());
                Map outParameters = execute(inParameters);
//                if(outParameters.size() >0){
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DIM_CARGO_MANIFEST.PRR_DEL_DIM_CARGO_MANIFEST";
        
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_add_user",Types.VARCHAR));
            compile();
        }
        
        protected boolean delete(final RrcStandardMod inputMod){
            boolean isSuccess = false;
            if(inputMod instanceof DimCargoManifestMod){
                DimCargoManifestMod aInputMod = (DimCargoManifestMod)inputMod;
                    Map inParameters = new HashMap(2);
                    
                    System.out.println("[DimManifestJdbcDao][DeleteStoreProcedure][delete]: p_session_id: "+ aInputMod.getSessionId());
                    System.out.println("[DimManifestJdbcDao][DeleteStoreProcedure][delete]: p_record_add_user: "+ aInputMod.getRecordAddUser());
                    
                    inParameters.put("p_session_id",aInputMod.getSessionId());
                    inParameters.put("p_record_add_user",aInputMod.getRecordAddUser());
                    execute(inParameters);
                    isSuccess = true;
            }
            return isSuccess;
        }
    }
}
