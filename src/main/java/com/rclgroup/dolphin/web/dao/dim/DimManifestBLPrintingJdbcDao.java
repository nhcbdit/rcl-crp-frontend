/*------------------------------------------------------
DimManifestBLPrintingJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
Author Kitti Pongsirisakun 01/12/10
- Change Log -------------------------------------------
## DD/MM/YY     �User-      -TaskRef-        -ShortDescription-
00 04/08/10      KIT        N/A               Created
01 10/11/10      KIT        435               Wrong display data.
02 21/06/12      NIP        PD_CR_20120425-01 DEX_DIM_Add function to select some BL
03 06/10/16      Sarawut A.                   Create new method showHideGenExcelButton,generateExcelHeader,generateExcelDetail
04 10/08/17      Onsinee S.                   Create new method showHideGenTextButton,generateTextDetail
05 27/06/18 	 Sarawut					  Add search condition POL Terminal & coc/soc
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintContainerExcelMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintExcelMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintTextMod; //##04
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DimManifestBLPrintingJdbcDao extends RrcStandardDao implements DimManifestBLPrintingDao {
    private InsertStoreProcedure insertStoreProcedure;
    private InsertSelectBlStoreProcedure insertSelectBlStoreProcedure;//##02
    private ClearPrintedBlStoreProcedure clearPrintedBlStoreProcedure;//##02
    private ShowHideGenExcelButtonProcedure showHideGenExcelButtonProcedure;
    private ShowHideGenTextButtonProcedure showHideGenTextButtonProcedure; //##04
    private GenerateExcelHeaderProcedure generateExcelHeaderProcedure;
    private GenerateExcelDetailProcedure generateExcelDetailProcedure;
    private GenerateExcelContainerProcedure generateExcelContainerProcedure;
    private GenerateTextDetailProcedure generateTextDetailProcedure; // ##04
    public DimManifestBLPrintingJdbcDao() {
    }    
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        insertSelectBlStoreProcedure = new InsertSelectBlStoreProcedure(getJdbcTemplate());//##02
        clearPrintedBlStoreProcedure = new ClearPrintedBlStoreProcedure(getJdbcTemplate());//##02
        generateExcelHeaderProcedure = new GenerateExcelHeaderProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelHdrList());
        generateExcelDetailProcedure = new GenerateExcelDetailProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelDtlList());
        generateExcelContainerProcedure = new GenerateExcelContainerProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelContainerList());
        generateTextDetailProcedure = new GenerateTextDetailProcedure(this.getJdbcTemplate(),new RowMapperGenerateTextDtlList()); //##04
        showHideGenExcelButtonProcedure = new ShowHideGenExcelButtonProcedure(this.getJdbcTemplate());
        showHideGenTextButtonProcedure = new ShowHideGenTextButtonProcedure(this.getJdbcTemplate()); //##04
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_MAIN_MANIFEST";
    protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_ser", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ves", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voy", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            
            declareParameter(new SqlParameter("p_l_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_r_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_a_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc_code", Types.VARCHAR));
            
            declareParameter(new SqlParameter("p_option", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod_terminal", Types.VARCHAR));
//            declareParameter(new SqlParameter("P_COC_SOC", Types.VARCHAR));
            
          //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
          //  declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));     
            compile();
            System.out.println("InsertStoreProcedure declare parameter end");
        }
    protected String insert(String service,  String vessel, String voyage,    String direction, String pol,  String pod,  String bl,  
                                                String l_code,   String r_code,   String a_code, String fsc_code, 
                                                String option,    String userName,String session, String podTerminal, String cocSoc) {
        return insertPro(  service  ,  vessel,  voyage,   direction,  pol,   pod,   bl,   l_code, r_code, a_code, fsc_code, option, userName, session, podTerminal,cocSoc);
    }
    protected String insertPro(String service,  String vessel, String voyage,    String direction, String pol,  String pod,  String bl,  
                                                       String l_code,   String r_code,   String a_code, String fsc_code, 
                                                       String option,    String userName,String session, String podTerminal, String cocSoc) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            
            inParameters.put("p_ser",RutDatabase.stringToDb(service));
            inParameters.put("p_ves",RutDatabase.stringToDb(vessel));
            inParameters.put("p_voy",RutDatabase.stringToDb(voyage));
            inParameters.put("p_dir",RutDatabase.stringToDb(direction));
            inParameters.put("p_pol",RutDatabase.stringToDb(pol));
            inParameters.put("p_pod",RutDatabase.stringToDb(pod));
            inParameters.put("p_bl",RutDatabase.stringToDb(bl));
            
            inParameters.put("p_l_code",RutDatabase.stringToDb(l_code));
            inParameters.put("p_r_code",RutDatabase.stringToDb(r_code));
            inParameters.put("p_a_code",RutDatabase.stringToDb(a_code));
            inParameters.put("p_fsc_code",RutDatabase.stringToDb(fsc_code));
            
            inParameters.put("p_option",RutDatabase.stringToDb(option));
            inParameters.put("p_usr",RutDatabase.stringToDb(userName));             
            inParameters.put("p_sid",RutDatabase.stringToDb(session));  
            inParameters.put("p_pod_terminal",RutDatabase.stringToDb(podTerminal));
//            inParameters.put("P_COC_SOC",RutDatabase.stringToDb(cocSoc));
            
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_ser = "+inParameters.get("p_ser"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_ves = "+inParameters.get("p_ves"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_voy = "+inParameters.get("p_voy"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_dir = "+inParameters.get("p_dir"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_pol = "+inParameters.get("p_pol"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_pod = "+inParameters.get("p_pod"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_bl = "+inParameters.get("p_bl"));
            
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_l_code = "+inParameters.get("p_l_code"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_r_code = "+inParameters.get("p_r_code"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_a_code = "+inParameters.get("p_a_code"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_fsc_code = "+inParameters.get("p_fsc_code"));
            
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_option = "+inParameters.get("p_option"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_usr = "+inParameters.get("p_usr"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertStoreProcedure][insert]: p_sid = "+inParameters.get("p_sid"));
            
          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_sid");
               System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    
    /*************** ##02 **************/
    protected class InsertSelectBlStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_INSERT_SELECT_BL";
    protected  InsertSelectBlStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            System.out.println("InsertSelectBlStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session", Types.VARCHAR));
            declareParameter(new SqlParameter("p_selectBl", Types.VARCHAR));
              
            compile();
            System.out.println("InsertSelectBlStoreProcedure declare parameter end");
        }
    protected String insertSelectBl(String user  , String session, String selectBl) {
        return insertSelectBlPro(  user  ,  session,  selectBl);
    }
    protected String insertSelectBlPro(String user  , String session, String selectBl) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            inParameters.put("p_user",RutDatabase.stringToDb(user));
            inParameters.put("p_session",RutDatabase.stringToDb(session));
            inParameters.put("p_selectBl",RutDatabase.stringToDb(selectBl));          
           
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_user = "+inParameters.get("p_user"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_session = "+inParameters.get("p_session"));
            System.out.println("[DimManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_selectBl = "+inParameters.get("p_selectBl"));
            
          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_sid");
               System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    
    public String  insertSelectBl(DimManifestBLPrintMod bean) throws DataAccessException {
                                                         
            return insertSelectBlStoreProcedure.insertSelectBl( bean.getUserName()  ,  bean.getSessionId(),  bean.getSelectBl());
    }
    /*************** ##02 end **************/
    
     /*************** ##02 **************/
     protected class ClearPrintedBlStoreProcedure extends StoredProcedure {
     private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_CLEAR_BL_PRINTED";
     protected  ClearPrintedBlStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);
             System.out.println("ClearPrintedBlStoreProcedure declare parameter begin");
             declareParameter(new SqlParameter("p_user", Types.VARCHAR));
             declareParameter(new SqlParameter("p_session", Types.VARCHAR));
               
             compile();
             System.out.println("ClearPrintedBlStoreProcedure declare parameter end");
         }
     protected String clearPrintedBl(String user  , String session, String selectBl) {
         return clearPrintedBlStorePro(  user  ,  session,  selectBl);
     }
     protected String clearPrintedBlStorePro(String user  , String session, String selectBl) {
             System.out.println("ClearPrintedBlStoreProcedure assign value to  parameter begin");
             
             String sessionId = "";
             
             Map inParameters = new HashMap();
             inParameters.put("p_user",RutDatabase.stringToDb(user));
             inParameters.put("p_session",RutDatabase.stringToDb(session));       
            
             System.out.println("[DimManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_user = "+inParameters.get("p_user"));
             System.out.println("[DimManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_session = "+inParameters.get("p_session"));
             
           /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
             Map outParameters = execute(inParameters);
             if (outParameters.size() > 0) {
                sessionId = RutDatabase.dbToString(outParameters, "p_sid");
                System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
             }
             return sessionId;
         }          
     } 
     
     public String  clearPrintedBl(DimManifestBLPrintMod bean) throws DataAccessException {
                                                          
             return clearPrintedBlStoreProcedure.clearPrintedBl( bean.getUserName()  ,  bean.getSessionId(),  bean.getSelectBl());
     }
     /*************** ##02 end **************/
    
    public String  getSessionId(String service,  String vessel, String voyage,    String direction, String pol,  String pod,  String bl,  
                                                String l_code,   String r_code,   String a_code, String fsc_code, 
                                                String option,    String userName,String session, String podTerminal, String cocSoc)throws DataAccessException {
                                                         
    return insertStoreProcedure.insert(   service  ,  vessel,  voyage,   direction,  pol,   pod,   bl,   l_code, r_code, a_code, fsc_code, option, userName, session, podTerminal, cocSoc);
     }
    
    public Vector getListManifestPrint(String session, String userName) throws DataAccessException{
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]");
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]: With status: sesseion "+session);
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]: With status: userName "+userName);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
            
        Vector manifestPrintVec = new Vector();
        DimManifestBLPrintMod dimManifestBLPrintingMod = null;
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]: ");
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]: Finished");
        
        sql.append("SELECT DISTINCT  ");
        sql.append("   SESSIONID");
        sql.append(" , BLNO");
        sql.append(" ,  BLIDDES");
        sql.append(" , BLCDTEC");
        sql.append(" , BLSTATUSDESC");
        sql.append(" , POL ");
        sql.append(" ,  POLT ");
        sql.append(" , POT ");
        sql.append(" , POD ");
        sql.append("  ,PODT");
        
        /********** ##02 ***********/
        //sql.append(" , PRINTFLAGDESC ");
        sql.append(" ,(");
        sql.append("    select decode(count(*),0,'N','Y') from rclapps.DIM_PRI_MNF_PRINTED_BL_TMP pbl");
        sql.append("    where pbl.BLNO=mnf.BLNO");
        sql.append("    and pbl.SESSION_ID=mnf.SESSIONID");
        sql.append("    and pbl.MODULE_CODE='DIM'");
        sql.append("    and pbl.RECORD_ADD_USER=mnf.USERNAME");
        sql.append("  )PRINTFLAGDESC");
        /********** ##02 end ***********/
        
        sql.append(" , USERNAME ");
        sql.append(" , EXPORT_STS"); // ##01
        sql.append(" , IMPORT_STS"); // ##01
        sql.append(" FROM DIM_PRI_MNF mnf ");
        sql.append(" WHERE SESSIONID = :sesseion ");
        sql.append(" AND USERNAME = :userName ");
        sql.append(" ORDER BY BLNO,POL,POD,BLCDTEC ");
        map.put("sesseion",session);
        map.put("userName",userName);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        while (rs.next()){  
            dimManifestBLPrintingMod = new DimManifestBLPrintMod();
            dimManifestBLPrintingMod.setSessionId(RutString.nullToStr(rs.getString("SESSIONID")));
            dimManifestBLPrintingMod.setBlNo(RutString.nullToStr(rs.getString("BLNO")));
            dimManifestBLPrintingMod.setBlIdDesc(RutString.nullToStr(rs.getString("BLIDDES")));
            dimManifestBLPrintingMod.setBlCreateDateConvert(RutString.nullToStr(rs.getString("BLCDTEC")));
            dimManifestBLPrintingMod.setBlStatusDesc(RutString.nullToStr(rs.getString("BLSTATUSDESC")));
            dimManifestBLPrintingMod.setPol(RutString.nullToStr(rs.getString("POL")));
            dimManifestBLPrintingMod.setPolTerminal(RutString.nullToStr(rs.getString("POLT")));
            dimManifestBLPrintingMod.setPot(RutString.nullToStr(rs.getString("POT")));
            dimManifestBLPrintingMod.setPod(RutString.nullToStr(rs.getString("POD")));
            dimManifestBLPrintingMod.setPodTerminal(RutString.nullToStr(rs.getString("PODT")));
            dimManifestBLPrintingMod.setPrintFlagDesc(RutString.nullToStr(rs.getString("PRINTFLAGDESC")));
            dimManifestBLPrintingMod.setUserName(RutString.nullToStr(rs.getString("USERNAME")));
            dimManifestBLPrintingMod.setExportSts(RutString.nullToStr(rs.getString("EXPORT_STS")));
            dimManifestBLPrintingMod.setImportSts(RutString.nullToStr(rs.getString("IMPORT_STS")));
            manifestPrintVec.add(dimManifestBLPrintingMod);
        }
        return manifestPrintVec;
        
    }
    public String getPort(String lineCode,String regionCode,String agentCode,String fscCode) throws DataAccessException{
    
        String portName = "";
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        
        sql.append("SELECT   ");
        sql.append(" Pcr_Rut_Utility.FR_GET_POINT_CDE_BY_FSC(:lineCode,:regionCode,:agentCode,:fscCode) AS PORT  ");
        sql.append(" FROM DUAL ");
        
        map.put("lineCode",lineCode);
        map.put("regionCode",regionCode);
        map.put("agentCode",agentCode);
        map.put("fscCode",fscCode);
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        while (rs.next()){
            portName = rs.getString("PORT");
        }
       return portName;
    }
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException{
        return showHideGenExcelButtonProcedure.validateFsc(fsc);
    }
    public String showHideGenTextButton(String fsc) throws DataAccessException{
        return showHideGenTextButtonProcedure.validateFsc(fsc);
    } //##04
    public List<DimManifestBLPrintExcelMod> generateExcelHeader(String userCode,String sessionid,String selectBl) throws DataAccessException{
        return generateExcelHeaderProcedure.generate(userCode,sessionid,selectBl);
    }
    
    public List<DimManifestBLPrintExcelMod> generateExcelDetail(String userCode,String sessionid,String selectBl) throws DataAccessException{
        return generateExcelDetailProcedure.generate(userCode,sessionid,selectBl);
    }
    
    public List<DimManifestBLPrintContainerExcelMod> generateExcelContainer(String selectBl) throws DataAccessException{
        return generateExcelContainerProcedure.generate(selectBl);
    }
    public List<DimManifestBLPrintTextMod> generateTextDetail(String userCode,String sessionid,String selectBl) throws DataAccessException{
        return generateTextDetailProcedure.generate(userCode,sessionid,selectBl);
    } //##04
    protected class ShowHideGenExcelButtonProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_CHK_SHOW_GEN_EXCEL_BUTTON";
    protected  ShowHideGenExcelButtonProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            compile();

        }
    protected String validateFsc(String fsc) {         
        return validateFscPro(fsc);
    }
    protected String validateFscPro(String fsc) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            String result = "";
            
            try  {
                outMap = execute(inParameters);
                if (outMap.size() > 0) {
                   result = RutDatabase.dbToString(outMap, "P_O_V_RESULT");
                }
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return result;
        }          
    }
    // ##04  
    protected class ShowHideGenTextButtonProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_CHK_SHOW_GEN_TEXT_BUTTON";
    protected  ShowHideGenTextButtonProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            compile();

        }
    protected String validateFsc(String fsc) {         
        return validateFscPro(fsc);
    }
    protected String validateFscPro(String fsc) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            String result = "";
            
            try  {
                outMap = execute(inParameters);
                if (outMap.size() > 0) {
                   result = RutDatabase.dbToString(outMap, "P_O_V_RESULT");
                }
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return result;
        }          
    }
    protected class GenerateExcelHeaderProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_GET_EXCEL_HDR";
    protected  GenerateExcelHeaderProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_blno", Types.VARCHAR));
            compile();

        }
    protected List<DimManifestBLPrintExcelMod> generate(String userCode,String sessionid,String selectBl) {
                                                       
        return generateExcelHdrPro(userCode, sessionid,selectBl);
    }
    protected List<DimManifestBLPrintExcelMod> generateExcelHdrPro(String userCode, String sessionid,String selectBl) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_blno",RutDatabase.stringToDb(selectBl));
            List<DimManifestBLPrintExcelMod> returnList = new ArrayList<DimManifestBLPrintExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DimManifestBLPrintExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelHdrList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DimManifestBLPrintExcelMod bean = new DimManifestBLPrintExcelMod();

            bean.setVesselCallSign(rs.getString("LVES_NAME"));
            bean.setVoyageNo(rs.getString("LVES"));
            bean.setCarrierCode(rs.getString("LVOY"));
            bean.setPortOfDischargeCode(rs.getString("POD_CODE"));
            bean.setPortOfDischargeSuffix(rs.getString("POD_SUFFIX"));
            bean.setEstimateDateOfArrival(rs.getString("EST_DATE_ARRIVAL"));
            
            return bean;
        }
    }
    
    protected class GenerateExcelDetailProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_GET_EXCEL_DTL";
    protected  GenerateExcelDetailProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_blno", Types.VARCHAR));
            compile();

        }
    protected List<DimManifestBLPrintExcelMod> generate(String userCode,String sessionid,String selectBl) {
                                                       
        return generateExcelDtlPro(userCode, sessionid,selectBl);
    }
    protected List<DimManifestBLPrintExcelMod> generateExcelDtlPro(String userCode, String sessionid,String selectBl) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_blno",RutDatabase.stringToDb(selectBl));
            List<DimManifestBLPrintExcelMod> returnList = new ArrayList<DimManifestBLPrintExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DimManifestBLPrintExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelDtlList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DimManifestBLPrintExcelMod bean = new DimManifestBLPrintExcelMod();

            bean.setCyOperator(rs.getString("OPERATOR_CODE"));
            bean.setRetentionClassification(rs.getString("RETENTION_CLASSIFICATION"));
            bean.setBlNo(rs.getString("BLNO"));
            bean.setPortOfLoadingCode(rs.getString("POL"));
            bean.setFinalDestinationCode(rs.getString("POD"));
            bean.setFinalDestinationName(rs.getString("POD_NAME"));
            bean.setPlaceOfTransferCode(rs.getString("POT"));
            bean.setPlaceOfTransferName(rs.getString("POT_NAME"));
            bean.setConsignorName(rs.getString("CONSIGNOR_NAME"));
            bean.setConsignorAddress(rs.getString("CONSIGNOR_ADDRESS"));
            bean.setConsignorPostCode(rs.getString("CONSIGNOR_POST_CODE"));
            bean.setConsignorCountryCode(rs.getString("CONSIGNOR_COUNTRY_CODE"));
            bean.setConsignorTelephoneNo(rs.getString("CONSIGNOR_TELEPHONE"));
            bean.setConsigneeName(rs.getString("CONSIGNEE_NAME"));
            bean.setConsigneeAddress(rs.getString("CONSIGNEE_ADDRESS"));
            bean.setConsigneePostCode(rs.getString("CONSIGNEE_POST_CODE"));
            bean.setConsigneeCountryCode(rs.getString("CONSIGNEE_COUNTRY_CODE"));
            bean.setConsigneeTelephoneNo(rs.getString("CONSIGNEE_TELEPHONE"));
            bean.setNotifyPartyName1(rs.getString("NOTIFY1_NAME"));
            bean.setNotifyPartyAddress1(rs.getString("NOTIFY1_ADDRESS"));
            bean.setNotifyPartyPostCode1(rs.getString("NOTIFY1_ZIP"));
            bean.setNotifyPartyCountry1(rs.getString("NOTIFY1_COUNTRY_CODE"));
            bean.setNotifyPartyTelephone1(rs.getString("NOTIFY1_TEL"));
            bean.setNotifyPartyName2(rs.getString("NOTIFY2_NAME"));
            bean.setNotifyPartyAddress2(rs.getString("NOTIFY2_ADDRESS"));
            bean.setNotifyPartyPostCode2(rs.getString("NOTIFY2_ZIP"));
            bean.setNotifyPartyCountry2(rs.getString("NOTIFY2_COUNTRY_CODE"));
            bean.setNotifyPartyTelephone2(rs.getString("NOTIFY2_TEL"));
            bean.setFreightCharges(rs.getString("FREIGHT_CHARGE"));
            bean.setFreightCurrencyCode(rs.getString("FREIGHT_CURRENCY_CODE"));
            bean.setTransship(rs.getString("TRANSSHIP"));
            bean.setTransshipReason(rs.getString("TRANSSHIP_REASON"));
            bean.setTransshipDuration(rs.getString("TRANSSHIP_DURATION"));
            bean.setRemarks(rs.getString("REMARKS"));
            bean.setDescriptionOfGoods(rs.getString("DESCRIPTION_GOODS"));
            //bean.setSpecialCargoSign(rs.getString("SPECIAL_CARGO"));
            bean.setNumberOfPackage(rs.getString("PACKAGE_NUMBER"));
            bean.setUnitCodeOfPackage(rs.getString("PACKAGE_UNIT_CODE"));
            bean.setGrossWeight(rs.getString("GROSS_WEIGHT"));
            bean.setUnitCodeOfWeight(rs.getString("GROSS_WEIGHT_UNIT_CODE"));
            bean.setNetWeight(rs.getString("NET_WEIGHT"));
            bean.setUnitCodeOfNet(rs.getString("NET_WEIGHT_UNIT_CODE"));
            bean.setMeasurement(rs.getString("MEANSUREMENT"));
            bean.setUnitCodeOfMeasurement(rs.getString("MEANSUREMENT_UNIT_CODE"));
            bean.setMarksAndNos(rs.getString("MARKS_NOS"));
            //bean.setContainerInformation(rs.getString("CONTAINER_INFORMATION"));
            //bean.setEquipmentNo(rs.getString("EYEQNO"));
            //bean.setSealNo(rs.getString("EYCRSL"));
            //bean.setFullEmpty(rs.getString("EMPTY_FULL"));
            //bean.setEquipSize(rs.getString("EYEQSZ"));
            //bean.setEquipType(rs.getString("EYEQTP"));
            //bean.setTransferType(rs.getString("TRANSFER_TYPE"));
            //bean.setOwnershipType(rs.getString("OWNERSHIP_TYPE"));
            //bean.setVanningType(rs.getString("VANNING_TYPE"));
            
            return bean;
        }
    }
    
    protected class GenerateExcelContainerProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_GET_EXCEL_CONTAINER";
    protected  GenerateExcelContainerProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_blno", Types.VARCHAR));
            compile();

        }
    protected List<DimManifestBLPrintContainerExcelMod> generate(String selectBl) {
                                                       
        return generateExcelContainerPro(selectBl);
    }
    protected List<DimManifestBLPrintContainerExcelMod> generateExcelContainerPro(String selectBl) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_blno",RutDatabase.stringToDb(selectBl));
            List<DimManifestBLPrintContainerExcelMod> returnList = new ArrayList<DimManifestBLPrintContainerExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DimManifestBLPrintContainerExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelContainerList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DimManifestBLPrintContainerExcelMod bean = new DimManifestBLPrintContainerExcelMod();
            
            bean.setBlNo(rs.getString("BLNO"));
            bean.setSpecialCargo(rs.getString("SPECIAL_CARGO"));
            bean.setContainerInformation(rs.getString("CONTAINER_INFORMATION"));
            bean.setEquipmentNo(rs.getString("EYEQNO"));
            bean.setSealNo(rs.getString("EYCRSL"));
            bean.setFullEmpty(rs.getString("EMPTY_FULL"));
            bean.setEquipSize(rs.getString("EYEQSZ"));
            bean.setEquipType(rs.getString("EYEQTP"));
            bean.setTransferType(rs.getString("TRANSFER_TYPE"));
            bean.setOwnershipType(rs.getString("OWNERSHIP_TYPE"));
            bean.setVanningType(rs.getString("VANNING_TYPE"));
            
            return bean;
        }
    }
    
    // ##04
    protected class GenerateTextDetailProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_MANIFEST_PRINT.PR_GET_TEXT_DTL";
    protected  GenerateTextDetailProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_blno", Types.VARCHAR));
            compile();

        }
    protected List<DimManifestBLPrintTextMod> generate(String userCode,String sessionid,String selectBl) {
                                                       
        return generateTextDtlPro(userCode, sessionid,selectBl);
    }
    protected List<DimManifestBLPrintTextMod> generateTextDtlPro(String userCode, String sessionid,String selectBl) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_blno",RutDatabase.stringToDb(selectBl));
            
            System.out.println("p_usr >>>>>>>>"+RutDatabase.stringToDb(userCode));
            System.out.println("p_sid >>>>>>>"+RutDatabase.stringToDb(sessionid));
            System.out.println("p_blno >>>>>>>"+RutDatabase.stringToDb(selectBl));
            List<DimManifestBLPrintTextMod> returnList = new ArrayList<DimManifestBLPrintTextMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DimManifestBLPrintTextMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    private class RowMapperGenerateTextDtlList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DimManifestBLPrintTextMod bean = new DimManifestBLPrintTextMod();
            
           bean.setVesSel(rs.getString("Vessel"));
           bean.setVoyage(rs.getString("Voyage"));
           bean.setMaster(rs.getString("Master"));
           bean.setShipPer(rs.getString("Shipper"));
           bean.setConSigNee(rs.getString("Consignee"));
           bean.setNotify(rs.getString("Notify"));
           bean.setBl(rs.getString("BL"));
           bean.setPor(rs.getString("POR"));
           bean.setPol(rs.getString("POL"));
           bean.setPod(rs.getString("POD"));
           bean.setContNo(rs.getString("ContNo."));
           bean.setSizeType(rs.getString("SizeType"));
           bean.setSealNo(rs.getString("SealNo"));
           bean.setPackQty(rs.getString("PackQty"));
           bean.setDescription(rs.getString("Description"));
           bean.setGrWtTareWtinKG(rs.getInt("GrWt + TareWt in KG"));
           bean.setGrWtKg(rs.getInt("GrWtKg"));
           bean.setGrWtLb(rs.getInt("GrWtLb"));
           bean.setCBM(rs.getInt("CBM"));
           bean.setCFT(rs.getInt("CFT"));
           bean.setPSN(rs.getString("PSN"));
           bean.setPSNDesc(rs.getString("PSNDesc"));
           bean.setIMCO(rs.getString("IMCO"));
           bean.setUNNO(rs.getString("UNNO"));
           bean.setPackingGrp(rs.getString("PackingGrp"));
           bean.setMarinePollutant(rs.getString("MarinePollutant"));
           bean.setSevereMarinePollutant(rs.getString("SevereMarinePollutant"));
           bean.setMPAGrp(rs.getString("MPAGrp"));
           bean.setTemp(rs.getString("Temp"));
           bean.setFND(rs.getString("FND"));
            return bean;
        }
    }


}
