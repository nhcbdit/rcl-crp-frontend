package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.dex.DexCocSocFreightListMod;

import com.rclgroup.dolphin.web.model.dex.DexFreightDiscrepancyListMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class DexFreightDiscrepancyListJdbcDao extends RrcStandardDao implements DexFreightDiscrepancyListDao{
    private GenerateTempBLProcedure generateTempBLProcedure;

    public DexFreightDiscrepancyListJdbcDao() {
    }
    
    
    protected void initDao() throws Exception {
        super.initDao();
       generateTempBLProcedure = new GenerateTempBLProcedure(getJdbcTemplate());
    }
    
    public boolean generateTempBL(RrcStandardMod mod) throws DataAccessException {
        return generateTempBLProcedure.generate(mod);
    }
    
    protected class GenerateTempBLProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_DEX_FREIGHT_DISCREPANCY.PRR_GEN_DEX_121_TMP_STEP1";
        
        protected GenerateTempBLProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_inv_date_from", Types.VARCHAR));
            declareParameter(new SqlParameter("p_inv_date_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl_date_from", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl_date_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_line", Types.VARCHAR));
            declareParameter(new SqlParameter("p_region", Types.VARCHAR));
            declareParameter(new SqlParameter("p_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("P_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_case", Types.VARCHAR));
            declareParameter(new SqlParameter("p_qtn", Types.VARCHAR));
            declareParameter(new SqlParameter("p_inv", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            System.out.println("DexFreightDiscrepancyListJdbcDao.generate()");
            boolean isSuccess = false;
            if ((inputMod instanceof DexFreightDiscrepancyListMod) && (outputMod instanceof DexFreightDiscrepancyListMod)) {
                Map inParameters = new HashMap();
                DexFreightDiscrepancyListMod aInputMod = (DexFreightDiscrepancyListMod) inputMod;
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUserName());
                inParameters.put("p_inv_date_from", getDateFormat(aInputMod.getInvDateFrom()));
                inParameters.put("p_inv_date_to", getDateFormat(aInputMod.getInvDateTo()));
                inParameters.put("p_bl_date_from", getDateFormat(aInputMod.getBlDateFrom()));
                inParameters.put("p_bl_date_to", getDateFormat(aInputMod.getBlDateTo()));
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_line", aInputMod.getLine());
                inParameters.put("p_region", aInputMod.getRegion());
                inParameters.put("p_agent", aInputMod.getAgent());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_pol", aInputMod.getPol());
                inParameters.put("P_pod", aInputMod.getPod());
                inParameters.put("p_dir", aInputMod.getDir());
                inParameters.put("p_bl", aInputMod.getBl());
                inParameters.put("p_case", aInputMod.getCompareCase());
                inParameters.put("p_qtn", aInputMod.getQtn());
                inParameters.put("p_inv", aInputMod.getInv());
                
                System.out.println("p_session_id:"+inParameters.get("p_session_id"));
                System.out.println("p_username:"+inParameters.get("p_username"));
                System.out.println("p_inv_date_from:"+inParameters.get("p_inv_date_from"));
                System.out.println("p_inv_date_to:"+inParameters.get("p_inv_date_to"));
                System.out.println("p_bl_date_from:"+inParameters.get("p_bl_date_from"));
                System.out.println("p_bl_date_to:"+inParameters.get("p_bl_date_to"));
                System.out.println("p_service:"+inParameters.get("p_service"));
                System.out.println("p_vessel:"+inParameters.get("p_vessel"));
                System.out.println("p_voyage:"+inParameters.get("p_voyage"));
                System.out.println("p_line:"+inParameters.get("p_line"));
                System.out.println("p_region:"+inParameters.get("p_region"));
                System.out.println("p_agent:"+inParameters.get("p_agent"));
                System.out.println("p_fsc:"+inParameters.get("p_fsc"));
                System.out.println("p_pol:"+inParameters.get("p_pol"));
                System.out.println("p_pod:"+inParameters.get("p_pod"));
                System.out.println("p_bl:"+inParameters.get("p_bl"));
                System.out.println("p_dir:"+inParameters.get("p_dir"));
                System.out.println("p_case:"+inParameters.get("p_case"));
                System.out.println("p_qtn:"+inParameters.get("p_qtn"));
                System.out.println("p_inv:"+inParameters.get("p_inv"));
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
    // input format dd/mm/yyyy
    private String getDateFormat(String date){
        if(!"".equals(date)){
            String arrPeriod[] = date.split("/");
            return arrPeriod[2]+arrPeriod[1]+arrPeriod[0];
        }
        
        return null; 
    }
}
