/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotMaintenanceJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 15/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.ems.EmsPickUpReturnDepotAssignmentMod;
import com.rclgroup.dolphin.web.model.ems.EmsPickUpReturnDepotMaintenanceDetailMod;
import com.rclgroup.dolphin.web.model.ems.EmsPickUpReturnDepotMaintenanceMod;
import com.rclgroup.dolphin.web.model.far.FarFunlocMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class EmsPickUpReturnDepotMaintenanceJdbcDao extends RrcStandardDao implements EmsPickUpReturnDepotMaintenanceDao{
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;    
    private InsertHdrStoreProcedure insertHdrStoreProcedure;
    private UpdateHdrStoreProcedure updateHdrStoreProcedure;
    private DeleteHdrStoreProcedure deleteHdrStoreProcedure;
    private DeleteDtlByHdrStoreProcedure deleteDtlByHdrStoreProcedure;
    
    public EmsPickUpReturnDepotMaintenanceJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();        
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());        
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());    
        insertHdrStoreProcedure = new InsertHdrStoreProcedure(getJdbcTemplate());
        updateHdrStoreProcedure = new UpdateHdrStoreProcedure(getJdbcTemplate());        
        deleteHdrStoreProcedure = new DeleteHdrStoreProcedure(getJdbcTemplate());  
        deleteDtlByHdrStoreProcedure = new DeleteDtlByHdrStoreProcedure(getJdbcTemplate());  
    }                                             
    
    public List listDetailForStatement(String fk_ems_multi_depot_hdr_id,String status) {
        System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][listDepotForStatementByHeader]: Started");              
        String sqlColumn = "DEPOT_CODE, DEPOT_NAME, FSC, POINT_CODE, STATUS";
        StringBuffer sql = new StringBuffer();

        sql.append("SELECT PK_EMS_MULTI_DEPOT_DTL_ID ");
        sql.append(", FK_EMS_MULTI_DEPOT_HDR_ID ");
        sql.append(", EQ_SIZE ");
        sql.append(", EQ_TYPE ");        
        sql.append(", RECORD_STATUS ");
        sql.append(", RECORD_ADD_USER ");
        sql.append(", RECORD_ADD_DATE ");
        sql.append(", RECORD_CHANGE_USER ");
        sql.append(", RECORD_CHANGE_DATE ");
        sql.append("FROM EMS_MULTI_DEPOT_DTL ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");        
        if (!RutString.isEmptyString(fk_ems_multi_depot_hdr_id)) {
            strWhere.append(" and FK_EMS_MULTI_DEPOT_HDR_ID = '"+fk_ems_multi_depot_hdr_id+"' ");
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and STATUS = '"+status+"' ");
        }
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        // Query by record number range
//        sql.append(createSqlCriteriaSortBy(sortBy, sortByIn));
        
        System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][listDepotForStatementByHeader]: SQL new ="+sql.toString());
        System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][listDepotForStatementByHeader]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           EmsPickUpReturnDepotMaintenanceDetailMod ff = new EmsPickUpReturnDepotMaintenanceDetailMod();
                           ff.setMultiDepotDtlId(RutString.nullToStr(rs.getString("PK_EMS_MULTI_DEPOT_DTL_ID")));
                           ff.setFkMultiDepotHdrId(RutString.nullToStr(rs.getString("FK_EMS_MULTI_DEPOT_HDR_ID")));
                           ff.setEqSize(RutString.nullToStr(rs.getString("EQ_SIZE")));
                           ff.setEqType(RutString.nullToStr(rs.getString("EQ_TYPE")));
                           ff.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));                                                                                     
                           ff.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           ff.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           ff.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           ff.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           ff.setActivityFlag("R");
                           ff.setActivityFlagOri("R");
                           return ff;
                    }
                });
    }
    
    public boolean updateSizeType(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());                
//        mod.setRecordChangeUser("DEV_TEAM");
        return updateStoreProcedure.updateMultiDepotDetail(mod);
    }
    
    public boolean deleteSizeType(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId()); 
//        mod.setRecordChangeUser("DEV_TEAM");
        return deleteStoreProcedure.deleteMultiDepotDetail(mod);
    }

    public boolean insertSizeType(RrcStandardMod mod) throws DataAccessException{      
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
//         mod.setRecordAddUser("DEV_TEAM");
//         mod.setRecordChangeUser("DEV_TEAM");
        return insertStoreProcedure.insertMultiDepotDetail(mod);
    }
    public boolean updateHdr(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());        
        return updateHdrStoreProcedure.updateMultiDepotHeader(mod);
    }
    
    public boolean deleteHdr(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId()); 
//        mod.setRecordChangeUser("DEV_TEAM");
        return deleteHdrStoreProcedure.deleteMultiDepotHeader(mod);
    }
    public boolean deleteDtlByHdr(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId()); 
    //        mod.setRecordChangeUser("DEV_TEAM");
        return deleteDtlByHdrStoreProcedure.deleteMultiDepotDtlByHeader(mod);
    }

    public boolean insertHdr(RrcStandardMod mod) throws DataAccessException{      
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
//        mod.setRecordAddUser("DEV_TEAM");
//        mod.setRecordChangeUser("DEV_TEAM");
        return insertHdrStoreProcedure.insertMultiDepotHeader(mod);
    }
    
//    public int insertFunLoc(String billToParty, String inv_no,String funloc,String userId,int numIns) throws DataAccessException{        
//        return insertStoreProcedure.insertFunLoc(billToParty,inv_no,funloc,userId,numIns);
//    }
    protected class UpdateStoreProcedure extends StoredProcedure{
        private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_DTL.PRR_UPD_MULTI_DEPOT_DTL";
        
        protected  UpdateStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_data_map_id",Types.NUMERIC));
            declareParameter(new SqlParameter("p_bill_to_party",Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl",Types.VARCHAR));
            declareParameter(new SqlParameter("p_invoice_no_like",Types.VARCHAR));
            declareParameter(new SqlParameter("p_fun_loc",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_status",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_user",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date",Types.TIMESTAMP));
            compile();
        }
        
        protected boolean updateMultiDepotDetail(RrcStandardMod mod) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof EmsPickUpReturnDepotMaintenanceDetailMod)&&(outputMod instanceof EmsPickUpReturnDepotMaintenanceDetailMod)){
                EmsPickUpReturnDepotMaintenanceDetailMod aInputMod = (EmsPickUpReturnDepotMaintenanceDetailMod)inputMod;
                EmsPickUpReturnDepotMaintenanceDetailMod aOutputMod = (EmsPickUpReturnDepotMaintenanceDetailMod)outputMod;
                Map inParameters = new HashMap(7);
                inParameters.put("p_multi_depot_dtl_id",aInputMod.getMultiDepotDtlId());
                inParameters.put("p_fk_multi_depot_hdr_id",aInputMod.getFkMultiDepotHdrId());
                inParameters.put("p_eq_size",aInputMod.getEqSize());            
                inParameters.put("p_eq_type",aInputMod.getEqType());                
                inParameters.put("p_record_status",aInputMod.getStatus());            
                inParameters.put("p_record_change_user",aInputMod.getRecordChangeUser());            
                inParameters.put("p_record_change_date",aInputMod.getRecordChangeDate());  
                execute(inParameters);            
                isSuccess = true;
            }
            return isSuccess;
        }
    }
     
    protected class InsertStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_DTL.PRR_INS_MULTI_DEPOT_DTL";
         
            protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
                declareParameter(new SqlInOutParameter("p_ems_multi_depot_dtl_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_eq_size", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_eq_type", Types.VARCHAR));                                          
                declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));                        
                declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR)); 
                declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));                 
                declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR)); 
                declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));                 
                compile();
            }

            protected boolean insertMultiDepotDetail(RrcStandardMod mod) {
                return insert(mod,mod);
            }
         
            protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
                boolean isSuccess = false;
                 if((inputMod instanceof EmsPickUpReturnDepotMaintenanceDetailMod)&&(outputMod instanceof EmsPickUpReturnDepotMaintenanceDetailMod)){
                    EmsPickUpReturnDepotMaintenanceDetailMod aInputMod = (EmsPickUpReturnDepotMaintenanceDetailMod)inputMod;
                    EmsPickUpReturnDepotMaintenanceDetailMod aOutputMod = (EmsPickUpReturnDepotMaintenanceDetailMod)outputMod;
                    Map inParameters = new HashMap(9);
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_ems_multi_depot_dtl_id:"+aInputMod.getMultiDepotDtlId());                
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_ems_multi_depot_hdr_id:"+aInputMod.getFkMultiDepotHdrId());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_eq_size:"+aInputMod.getEqSize());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_eq_type:"+aInputMod.getEqType());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());                
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());                
                    if (aInputMod.getMultiDepotDtlId().equals("")){
                        System.out.println("null");           
                    }else{
                        System.out.println("ID = "+aInputMod.getMultiDepotDtlId());        
                    }
                     
                     inParameters.put("p_ems_multi_depot_dtl_id", (aInputMod.getMultiDepotDtlId().equals(""))?null:new Integer(aInputMod.getMultiDepotDtlId()));
//                    inParameters.put("p_ems_multi_depot_dtl_id", null);
                    inParameters.put("p_ems_multi_depot_hdr_id", (aInputMod.getFkMultiDepotHdrId().equals(""))?null:new Integer(aInputMod.getFkMultiDepotHdrId()));
//                    inParameters.put("p_ems_multi_depot_hdr_id", new Integer(19));
                    inParameters.put("p_eq_size", aInputMod.getEqSize());
                    inParameters.put("p_eq_type", aInputMod.getEqType());
                    inParameters.put("p_record_status", aInputMod.getRecordStatus());
                    inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                    inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate()); 
                    Map outParameters = execute(inParameters);
    
                    if (outParameters.size() > 0) {
//                        aInputMod.setNumIns(((Integer)outParameters.get("p_num_ins")).intValue());                    
                        isSuccess = true;                                            
                    }         
                 }
                return isSuccess;
            }
        }
     
    protected class DeleteStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_DTL.PRR_DEL_MULTI_DEPOT_DTL";
         
            protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                declareParameter(new SqlParameter("p_ems_multi_depot_dtl_id", Types.INTEGER));
                declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
                compile();
            }
         
            protected boolean deleteMultiDepotDetail(RrcStandardMod mod) {
                return delete(mod);
            }
            protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof EmsPickUpReturnDepotMaintenanceDetailMod){
                EmsPickUpReturnDepotMaintenanceDetailMod aInputMod = (EmsPickUpReturnDepotMaintenanceDetailMod)inputMod;
                Map inParameters = new HashMap(3);
    ////
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_pk_multi_depot_dtl_id:"+aInputMod.getMultiDepotDtlId());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////
                    inParameters.put("p_ems_multi_depot_dtl_id", new Integer((RutString.nullToStr(aInputMod.getMultiDepotDtlId()).equals("")?"0":RutString.nullToStr(aInputMod.getMultiDepotDtlId()))));
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                    execute(inParameters);
                    isSuccess = true;                
            }
            return isSuccess;
        }
    }
    
///////////////////////////////////////////////////
    protected class UpdateHdrStoreProcedure extends StoredProcedure{
     private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_HDR.PRR_UPD_MULTI_DEPOT_HDR";
     
     protected  UpdateHdrStoreProcedure(JdbcTemplate jdbcTemplate){
         super(jdbcTemplate, STORED_PROCEDURE_NAME);
         declareParameter(new SqlInOutParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
         declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_depot_code", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_point_code", Types.VARCHAR));                                          
         declareParameter(new SqlInOutParameter("p_mporr", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_vessel", Types.VARCHAR));    
         declareParameter(new SqlInOutParameter("p_voyage", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_contract_party", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_supplier", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_shipper", Types.VARCHAR));         
         declareParameter(new SqlInOutParameter("p_consignee", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_bl_no", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_booking_no", Types.VARCHAR));    
         declareParameter(new SqlInOutParameter("p_porr_flag", Types.VARCHAR));             
         declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));                                      
         declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR)); 
         declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP)); 
         compile();
     }
     
     protected boolean updateMultiDepotHeader(RrcStandardMod mod) {
         return update(mod,mod);
     }
     
     protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
         boolean isSuccess = false;
         if((inputMod instanceof EmsPickUpReturnDepotMaintenanceMod)&&(outputMod instanceof EmsPickUpReturnDepotMaintenanceMod)){
             EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
             EmsPickUpReturnDepotMaintenanceMod aOutputMod = (EmsPickUpReturnDepotMaintenanceMod)outputMod;
             Map inParameters = new HashMap(18);
             inParameters.put("p_ems_multi_depot_hdr_id", aInputMod.getMultiDepotHdrId());
             inParameters.put("p_fsc", aInputMod.getFsc());
             inParameters.put("p_depot_code", aInputMod.getDepotCode());
             inParameters.put("p_point_code", aInputMod.getPointCode());   
             inParameters.put("p_mporr",aInputMod.getMporr());
             inParameters.put("p_service",aInputMod.getService());
             inParameters.put("p_vessel",aInputMod.getVessel());            
             inParameters.put("p_voyage",aInputMod.getVoyage());                
             inParameters.put("p_contract_party", aInputMod.getContractParty());   
             inParameters.put("p_supplier", aInputMod.getSupplier());   
             inParameters.put("p_shipper", aInputMod.getShipper());   
             inParameters.put("p_consignee", aInputMod.getConsignee());   
             inParameters.put("p_bl_no", aInputMod.getBl());   
             inParameters.put("p_booking_no",aInputMod.getBkg());
             inParameters.put("p_porr_flag",aInputMod.getPorrFlag());                 
             inParameters.put("p_record_status",aInputMod.getStatus());                         
             inParameters.put("p_record_change_user",aInputMod.getRecordChangeUser());            
             inParameters.put("p_record_change_date",aInputMod.getRecordChangeDate());  
             execute(inParameters);            
             isSuccess = true;
         }
         return isSuccess;
     }
 }
  
    protected class InsertHdrStoreProcedure extends StoredProcedure {
         private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_HDR.PRR_INS_MULTI_DEPOT_HDR";
      
         protected InsertHdrStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
             declareParameter(new SqlInOutParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
             declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_depot_code", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_point_code", Types.VARCHAR));                                          
             declareParameter(new SqlInOutParameter("p_mporr", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_vessel", Types.VARCHAR));    
             declareParameter(new SqlInOutParameter("p_voyage", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_contract_party", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_supplier", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_shipper", Types.VARCHAR));             
             declareParameter(new SqlInOutParameter("p_consignee", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_bl_no", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_booking_no", Types.VARCHAR));    
             declareParameter(new SqlInOutParameter("p_porr_flag", Types.VARCHAR));             
             declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));                        
             declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR)); 
             declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));                 
             declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR)); 
             declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));                 
             compile();
         }

         protected boolean insertMultiDepotHeader(RrcStandardMod mod) {
             return insert(mod,mod);
         }
      
         protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
             boolean isSuccess = false;
              if((inputMod instanceof EmsPickUpReturnDepotMaintenanceMod)&&(outputMod instanceof EmsPickUpReturnDepotMaintenanceMod)){
                 EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
                 EmsPickUpReturnDepotMaintenanceMod aOutputMod = (EmsPickUpReturnDepotMaintenanceMod)outputMod;
                 Map inParameters = new HashMap(20);
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_ems_multi_depot_hdr_id:"+aInputMod.getMultiDepotHdrId());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_fsc:"+aInputMod.getFsc());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_depot_code:"+aInputMod.getDepotCode());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_point_code:"+aInputMod.getPointCode());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_mporr:"+aInputMod.getMporr());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_service:"+aInputMod.getService());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_vessel:"+aInputMod.getVessel());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_voyage:"+aInputMod.getVoyage());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_contract_party:"+aInputMod.getContractParty());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_shipper:"+aInputMod.getShipper());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_supplier:"+aInputMod.getSupplier());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_consignee:"+aInputMod.getConsignee());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_bl_no:"+aInputMod.getBl());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_booking_no:"+aInputMod.getBkg());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_porr_flag:"+aInputMod.getPorrFlag());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordChangeUser());                
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordChangeDate());                
                                         
                 inParameters.put("p_ems_multi_depot_hdr_id", (aInputMod.getMultiDepotHdrId().equals(""))?null:new Integer(aInputMod.getMultiDepotHdrId()));
                 inParameters.put("p_fsc", aInputMod.getFsc());
                 inParameters.put("p_depot_code", aInputMod.getDepotCode());
                 inParameters.put("p_point_code", aInputMod.getPointCode());   
                 inParameters.put("p_mporr",aInputMod.getMporr());
                 inParameters.put("p_service",aInputMod.getService());
                 inParameters.put("p_vessel",aInputMod.getVessel());            
                 inParameters.put("p_voyage",aInputMod.getVoyage());                
                 inParameters.put("p_contract_party", aInputMod.getContractParty());                       
                 inParameters.put("p_supplier", aInputMod.getSupplier());
                 inParameters.put("p_shipper", aInputMod.getShipper());
                 inParameters.put("p_consignee", aInputMod.getConsignee());   
                 inParameters.put("p_bl_no", aInputMod.getBl());   
                 inParameters.put("p_booking_no",aInputMod.getBkg());
                 inParameters.put("p_porr_flag",aInputMod.getPorrFlag());                 
                 inParameters.put("p_record_status",aInputMod.getRecordStatus());            
                 inParameters.put("p_record_add_user",aInputMod.getRecordAddUser());            
                 inParameters.put("p_record_add_date",aInputMod.getRecordAddDate());  
                 inParameters.put("p_record_change_user",aInputMod.getRecordChangeUser());            
                 inParameters.put("p_record_change_date",aInputMod.getRecordChangeDate());  
                 Map outParameters = execute(inParameters);
 
                 if (outParameters.size() > 0) {
                         aOutputMod.setMultiDepotHdrId(((Integer)outParameters.get("p_ems_multi_depot_hdr_id")).toString());                        
                     isSuccess = true;                                            
                 }         
              }
             return isSuccess;
         }
     }
  
    protected class DeleteHdrStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_HDR.PRR_DEL_MULTI_DEPOT_HDR";
         
            protected DeleteHdrStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                declareParameter(new SqlParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
                declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
                compile();
            }
         
            protected boolean deleteMultiDepotHeader(RrcStandardMod mod) {
                return delete(mod);
            }
            protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof EmsPickUpReturnDepotMaintenanceMod){
                EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
                Map inParameters = new HashMap(3);
    ////
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_pk_multi_depot_dtl_id:"+aInputMod.getMultiDepotHdrId());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////
                    inParameters.put("p_ems_multi_depot_hdr_id", new Integer((RutString.nullToStr(aInputMod.getMultiDepotHdrId()).equals("")?"0":RutString.nullToStr(aInputMod.getMultiDepotHdrId()))));
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                    execute(inParameters);
                    isSuccess = true;                
            }
            return isSuccess;
        }
    }
    
    protected class DeleteDtlByHdrStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_DTL.PRR_DEL_MULTI_DEPOT_DTL_BY_HDR";
     
        protected DeleteDtlByHdrStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean deleteMultiDepotDtlByHeader(RrcStandardMod mod) {
            return delete(mod);
        }
        protected boolean delete(final RrcStandardMod inputMod) {
        boolean isSuccess = false;
        if(inputMod instanceof EmsPickUpReturnDepotMaintenanceMod){
            EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
            Map inParameters = new HashMap(3);
////
                System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_pk_multi_depot_dtl_id:"+aInputMod.getMultiDepotHdrId());
                System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////
                inParameters.put("p_ems_multi_depot_hdr_id", new Integer((RutString.nullToStr(aInputMod.getMultiDepotHdrId()).equals("")?"0":RutString.nullToStr(aInputMod.getMultiDepotHdrId()))));
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;                
        }
        return isSuccess;
    }
  }      
}
