/*------------------------------------------------------
DexBillOfLandingPrintJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
Author Kitti Pongsirisakun 01/12/10
- Change Log -------------------------------------------
## DD/MM/YY     �User-       -TaskRef-  -ShortDescription-
01 04/08/10     KIT           N/A        Created
02 03/10/16     SarawutA.                Create new method showHideGenExcelButton,generateExcelHeader,generateExcelDetail
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dex.DexBillOfLandingPrintExcelMod;
import com.rclgroup.dolphin.web.model.dex.DexBillOfLandingPrintMod;
import com.rclgroup.dolphin.web.model.dex.DexManifestBLPrintingMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DexBillOfLandingPrintJdbcDao extends RrcStandardDao implements DexBillOfLandingPrintDao {
    private InsertStoreProcedure insertStoreProcedure;
    private GenerateExcelHeaderProcedure generateExcelHeaderProcedure;
    private GenerateExcelDetailProcedure generateExcelDetailProcedure;
    private ShowHideGenExcelButtonProcedure showHideGenExcelButtonProcedure;
    public DexBillOfLandingPrintJdbcDao() {
    }    
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        generateExcelHeaderProcedure = new GenerateExcelHeaderProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelHdrList());
        generateExcelDetailProcedure = new GenerateExcelDetailProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelDtlList());
        showHideGenExcelButtonProcedure = new ShowHideGenExcelButtonProcedure(this.getJdbcTemplate());
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_BL_PRINT.PR_BL_PRINT";
    protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_ser", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ves", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voy", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            declareParameter(new SqlParameter("p_off", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_cocsoc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_print", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_sts", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dte_frm", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dte_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_third", Types.VARCHAR));    
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));        
            declareParameter(new SqlParameter("p_l_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_r_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_a_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc_code", Types.VARCHAR));

            
          //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
          //  declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));     
            compile();
            System.out.println("InsertStoreProcedure declare parameter end");
        }
    protected String insert(      String service  ,           String vessel,            String voyage,     String direction, 
                                                       String offCreateBL ,   String bl,                    String cocsoc,      String print,
                                                       String pol_sts,             String dateFrom,      String dateTo,      String third,
                                                       String fsc,                     String userCode,     String sessionid,  String line,
                                                       String region,              String agent,             String fscCode) {
                                                       
        return insertPro(                service  ,                       vessel,                       voyage,                  direction, 
                                                       offCreateBL ,                bl,                               cocsoc,                   print,
                                                       pol_sts,                          dateFrom,                 dateTo,                   third,
                                                       fsc,                                  userCode,                sessionid,               line,
                                                      region,                            agent,                        fscCode);
    }
    protected String insertPro(String service  ,          String vessel,            String voyage,     String direction, 
                                                       String offCreateBL ,   String bl,                    String cocsoc,      String print,
                                                       String pol_sts,             String dateFrom,      String dateTo,      String third,
                                                       String fsc,                     String userCode,     String sessionid,  String line,
                                                       String region,              String agent,             String fscCode) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            inParameters.put("p_ser",RutDatabase.stringToDb(service));
            inParameters.put("p_ves",RutDatabase.stringToDb(vessel));
            inParameters.put("p_voy",RutDatabase.stringToDb(voyage));
            inParameters.put("p_dir",RutDatabase.stringToDb(direction));
            inParameters.put("p_off",RutDatabase.stringToDb(offCreateBL));
            inParameters.put("p_bl",RutDatabase.stringToDb(bl));
            inParameters.put("p_cocsoc",RutDatabase.stringToDb(cocsoc));
            inParameters.put("p_print",RutDatabase.stringToDb(print));
            inParameters.put("p_pol_sts",RutDatabase.stringToDb(pol_sts));
            inParameters.put("p_dte_frm",RutDatabase.stringToDb(dateFrom));
            inParameters.put("p_dte_to",RutDatabase.stringToDb(dateTo));             
            inParameters.put("p_third",RutDatabase.stringToDb(third));      
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_l_code",RutDatabase.stringToDb(line));
            inParameters.put("p_r_code",RutDatabase.stringToDb(region));
            inParameters.put("p_a_code",RutDatabase.stringToDb(agent));
            inParameters.put("p_fsc_code",RutDatabase.stringToDb(fscCode));      

            
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_ser = "+inParameters.get("p_ser"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_ves = "+inParameters.get("p_ves"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_voy = "+inParameters.get("p_voy"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_dir = "+inParameters.get("p_dir"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_off = "+inParameters.get("p_off"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_bl = "+inParameters.get("p_bl"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_cocsoc = "+inParameters.get("p_cocsoc"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_print = "+inParameters.get("p_print"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_pol_sts = "+inParameters.get("p_pol_sts"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_dte_frm = "+inParameters.get("p_dte_frm"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_dte_to = "+inParameters.get("p_dte_to"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_third = "+inParameters.get("p_third"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_fsc = "+inParameters.get("p_fsc"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_usr = "+inParameters.get("p_usr"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_sid = "+inParameters.get("p_sid"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_l_code = "+inParameters.get("p_l_code"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_r_code = "+inParameters.get("p_r_code"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_a_code = "+inParameters.get("p_a_code"));
            System.out.println("[DexBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_fsc_code = "+inParameters.get("p_fsc_code"));

          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_sid");
               System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    
    public String  getSessionId(String service  ,          String vessel,            String voyage,     String direction, 
                                                       String offCreateBL ,   String bl,                    String cocsoc,      String print,
                                                       String pol_sts,             String dateFrom,      String dateTo,      String third,
                                                       String fsc,                     String userCode,     String sessionid,  String line,
                                                       String region,              String agent,             String fscCode)throws DataAccessException {
                                                         
    return insertStoreProcedure.insert(    service  ,         vessel,         voyage,     direction, 
                                                                         offCreateBL ,  bl,                cocsoc,      print,
                                                                         pol_sts,            dateFrom,  dateTo,      third,
                                                                         fsc,                    userCode,  sessionid,  line,
                                                                         region,             agent,          fscCode);
     }
    
    public Vector getListBLPrint(String session, String userName) throws DataAccessException{
        System.out.println("[DexBillOfLandingPrintJdbcDao][getListBLPrint]");
        System.out.println("[DexBillOfLandingPrintJdbcDao][getListBLPrint]: With status: sesseion "+session);
        System.out.println("[DexBillOfLandingPrintJdbcDao][getListBLPrint]: With status: userName "+userName);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
            
        Vector manifestPrintVec = new Vector();
        DexBillOfLandingPrintMod dexBillOfLandingPrintMod = null;
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]: ");
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]: Finished");
        
        sql.append("SELECT DISTINCT  ");
        sql.append("   BLNO");
        sql.append(" , (SELECT CYNAME FROM DEX_BL_PRI_CUS WHERE CUSFLAG = 1 AND DEX_BL_PRI_CUS.BLNO = DEX_BL_PRI.BLNO AND ROWNUM = 1) SHIPPERNAME");
        sql.append(" ,  PLA_OF_ISSUE");
        sql.append(" , OFF_OR_FSC AS OFFICE");
        sql.append(" , BLCDTEC");
        sql.append(" , COCSOC ");
        sql.append(" ,  BLSTATUSDESC ");
        sql.append(" , DES_PRI ");
        sql.append(" , SESSIONID ");
        sql.append(" , USERNAME ");
        sql.append(" , BL_TYPE ");
        sql.append(" FROM DEX_BL_PRI ");
        sql.append(" WHERE SESSIONID = :sesseion ");
        sql.append(" AND USERNAME = :userName ");
        sql.append(" ORDER BY BLNO,BLCDTEC ");
        
        map.put("sesseion",session);
        map.put("userName",userName);
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        while (rs.next()){
            dexBillOfLandingPrintMod = new DexBillOfLandingPrintMod();
            dexBillOfLandingPrintMod.setSessionId(RutString.nullToStr(rs.getString("SESSIONID")));
            dexBillOfLandingPrintMod.setBlNo(RutString.nullToStr(rs.getString("BLNO")));
            dexBillOfLandingPrintMod.setShipperName(RutString.nullToStr(rs.getString("SHIPPERNAME")));
            dexBillOfLandingPrintMod.setPla_of_issue(RutString.nullToStr(rs.getString("PLA_OF_ISSUE")));
            dexBillOfLandingPrintMod.setOffice(RutString.nullToStr(rs.getString("OFFICE")));
            dexBillOfLandingPrintMod.setBlcdtec(RutString.nullToStr(rs.getString("BLCDTEC")));
            dexBillOfLandingPrintMod.setCocsoc(RutString.nullToStr(rs.getString("COCSOC")));
            dexBillOfLandingPrintMod.setBlstatus(RutString.nullToStr(rs.getString("BLSTATUSDESC")));
            dexBillOfLandingPrintMod.setPrinted(RutString.nullToStr(rs.getString("DES_PRI")));
            dexBillOfLandingPrintMod.setUserName(RutString.nullToStr(rs.getString("USERNAME")));
            dexBillOfLandingPrintMod.setBlType(RutString.nullToStr(rs.getString("BL_TYPE")));
            manifestPrintVec.add(dexBillOfLandingPrintMod);
        }
        return manifestPrintVec;
        
    }
    
    public List<DexBillOfLandingPrintExcelMod> generateExcelHeader(String userCode,String sessionid) throws DataAccessException{
        return generateExcelHeaderProcedure.generate(userCode,sessionid);
    }
    
    public List<DexBillOfLandingPrintExcelMod> generateExcelDetail(String userCode,String sessionid) throws DataAccessException{
        return generateExcelDetailProcedure.generate(userCode,sessionid);
    }
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException{
        return showHideGenExcelButtonProcedure.validateFsc(fsc);
    }
    
    protected class ShowHideGenExcelButtonProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_BL_PRINT.PR_CHK_SHOW_GEN_EXCEL_BUTTON";
    protected  ShowHideGenExcelButtonProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            compile();

        }
    protected String validateFsc(String fsc) {         
        return validateFscPro(fsc);
    }
    protected String validateFscPro(String fsc) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            String result = "";
            
            try  {
                outMap = execute(inParameters);
                if (outMap.size() > 0) {
                   result = RutDatabase.dbToString(outMap, "P_O_V_RESULT");
                }
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return result;
        }          
    }
    
    protected class GenerateExcelHeaderProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_BL_PRINT.PR_GET_EXCEL_HDR";
    protected  GenerateExcelHeaderProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));
            compile();

        }
    protected List<DexBillOfLandingPrintExcelMod> generate(String userCode,String sessionid) {
                                                       
        return generateExcelHdrPro(userCode, sessionid);
    }
    protected List<DexBillOfLandingPrintExcelMod> generateExcelHdrPro(String userCode, String sessionid) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            List<DexBillOfLandingPrintExcelMod> returnList = new ArrayList<DexBillOfLandingPrintExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DexBillOfLandingPrintExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelHdrList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DexBillOfLandingPrintExcelMod bean = new DexBillOfLandingPrintExcelMod();

            bean.setVesselCallSign(rs.getString("LVES_NAME"));
            bean.setVoyageNo(rs.getString("LVES"));
            bean.setCarrierCode(rs.getString("LVOY"));
            bean.setPortOfDischargeCode(rs.getString("POD_CODE"));
            bean.setPortOfDischargeSuffix(rs.getString("POD_SUFFIX"));
            bean.setEstimateDateOfArrival(rs.getString("EST_DATE_ARRIVAL"));
            
            return bean;
        }
    }
    
    protected class GenerateExcelDetailProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_BL_PRINT.PR_GET_EXCEL_DTL";
    protected  GenerateExcelDetailProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));
            compile();

        }
    protected List<DexBillOfLandingPrintExcelMod> generate(String userCode,String sessionid) {
                                                       
        return generateExcelDtlPro(userCode, sessionid);
    }
    protected List<DexBillOfLandingPrintExcelMod> generateExcelDtlPro(String userCode, String sessionid) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            List<DexBillOfLandingPrintExcelMod> returnList = new ArrayList<DexBillOfLandingPrintExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DexBillOfLandingPrintExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelDtlList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DexBillOfLandingPrintExcelMod bean = new DexBillOfLandingPrintExcelMod();

            bean.setCyOperator(rs.getString("OPERATOR_CODE"));
            bean.setRetentionClassification(rs.getString("RETENTION_CLASSIFICATION"));
            bean.setBlNo(rs.getString("BLNO"));
            bean.setPortOfLoadingCode(rs.getString("POL"));
            bean.setFinalDestinationCode(rs.getString("POD"));
            bean.setFinalDestinationName(rs.getString("POD_NAME"));
            bean.setPlaceOfTransferCode(rs.getString("POT"));
            bean.setPlaceOfTransferName(rs.getString("POT_NAME"));
            bean.setConsignorName(rs.getString("CONSIGNOR_NAME"));
            bean.setConsignorAddress(rs.getString("CONSIGNOR_ADDRESS"));
            bean.setConsignorPostCode(rs.getString("CONSIGNOR_POST_CODE"));
            bean.setConsignorCountryCode(rs.getString("CONSIGNOR_COUNTRY_CODE"));
            bean.setConsignorTelephoneNo(rs.getString("CONSIGNOR_TELEPHONE"));
            bean.setConsigneeName(rs.getString("CONSIGNEE_NAME"));
            bean.setConsigneeAddress(rs.getString("CONSIGNEE_ADDRESS"));
            bean.setConsigneePostCode(rs.getString("CONSIGNEE_POST_CODE"));
            bean.setConsigneeCountryCode(rs.getString("CONSIGNEE_COUNTRY_CODE"));
            bean.setConsigneeTelephoneNo(rs.getString("CONSIGNEE_TELEPHONE"));
            bean.setNotifyParty(rs.getString("NOTIFY_PARTY"));
            bean.setFreightCharges(rs.getString("FREIGHT_CHARGE"));
            bean.setFreightCurrencyCode(rs.getString("FREIGHT_CURRENCY_CODE"));
            bean.setTransship(rs.getString("TRANSSHIP"));
            bean.setTransshipReason(rs.getString("TRANSSHIP_REASON"));
            bean.setTransshipDuration(rs.getString("TRANSSHIP_DURATION"));
            bean.setRemarks(rs.getString("REMARKS"));
            bean.setDescriptionOfGoods(rs.getString("DESCRIPTION_GOODS"));
            bean.setSpecialCargoSign(rs.getString("SPECIAL_CARGO"));
            bean.setNumberOfPackage(rs.getString("PACKAGE_NUMBER"));
            bean.setUnitCodeOfPackage(rs.getString("PACKAGE_UNIT_CODE"));
            bean.setGrossWeight(rs.getString("GROSS_WEIGHT"));
            bean.setUnitCodeOfWeight(rs.getString("GROSS_WEIGHT_UNIT_CODE"));
            bean.setNetWeight(rs.getString("NET_WEIGHT"));
            bean.setUnitCodeOfNet(rs.getString("NET_WEIGHT_UNIT_CODE"));
            bean.setMeasurement(rs.getString("MEANSUREMENT"));
            bean.setUnitCodeOfMeasurement(rs.getString("MEANSUREMENT_UNIT_CODE"));
            bean.setMarksAndNos(rs.getString("MARKS_NOS"));
            bean.setContainerInformation(rs.getString("CONTAINER_INFORMATION"));
            
            return bean;
        }
    }
    
    
}
 
