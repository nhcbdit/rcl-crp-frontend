/*-----------------------------------------------------------------------------------------------------------  
DndDetentionAndDemurrageJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 22/02/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 18/03/13  Atu        dnd107 report  Add detdem and dnd type for dnd107 report
02 26/03/13 NIP                        modify logic same All Pending Billing(Details_DND Billing Extraction report)
03 28/1/13 RAJA  PD_CR_20130827_1     modify logic same All Cancel Repors(Enable Import and Export for DND Billing Status)
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.dnd;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.dnd.DndDemurrageAndDetentionMod;
import com.rclgroup.dolphin.web.model.dnd.DndDemurrageAndDetentionWaiverMod;
import com.rclgroup.dolphin.web.model.dnd.DndDndBillingStatusContentMod;
import com.rclgroup.dolphin.web.model.dnd.DndDndSummaryStatMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DndDemurrageAndDetentionJdbcDao extends RrcStandardDao implements DndDemurrageAndDetentionDao{
    private InsertStoreProcedure insertStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private GenerateBillingStatContentStoreProcedure generateBillingStatContentStoreProcedure;
    private GenerateInvoiceForAcosStoreProcedure generateInvoiceForAcosStoreProcedure;
    private GenerateSummaryStatStoreProcedure generateSummaryStatStoreProcedure;
    private GenerateSummaryPerdStoreProcedure generateSummaryPerdStoreProcedure;
    
    public DndDemurrageAndDetentionJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        generateBillingStatContentStoreProcedure = new GenerateBillingStatContentStoreProcedure(getJdbcTemplate());
        generateInvoiceForAcosStoreProcedure = new GenerateInvoiceForAcosStoreProcedure(getJdbcTemplate());
        generateSummaryStatStoreProcedure = new GenerateSummaryStatStoreProcedure(getJdbcTemplate());
        generateSummaryPerdStoreProcedure = new GenerateSummaryPerdStoreProcedure(getJdbcTemplate());
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean isValidWithSessionId(String sessionId) throws DataAccessException  {
        System.out.println("[DndDemurrageAndDetentionJdbcDao][isValidWithSessionId]: Started ");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append(" select DISTINCT SESSION_ID ");
        sql.append(" from DND_FREE_DAYS_RPT ");
        sql.append(" where SESSION_ID = '"+ sessionId +"' ");
        sql.append("      and rownum =1 ");
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),new HashMap());
        if(rs.next()){
            isValid = true;
        }else{
            isValid = false;
        }
        System.out.println("[DndDemurrageAndDetentionJdbcDao][isValidWithSessionId]: sql : "+sql.toString());
        System.out.println("[DndDemurrageAndDetentionJdbcDao][isValidWithSessionId]: isValid : "+isValid);
        System.out.println("[DndDemurrageAndDetentionJdbcDao][isValidWithSessionId]: Finished ");
        return isValid;
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        boolean isSuccess = false;
        if(mod instanceof DndDemurrageAndDetentionMod){
            DndDemurrageAndDetentionMod aInputMod = (DndDemurrageAndDetentionMod)mod;
            if(isValidWithSessionId(aInputMod.getSessionId())){
                deleteStoreProcedure.delete(mod);
                isSuccess = true;
            }else{
                isSuccess = false;
            }
        }
        return isSuccess;
    }
    
    public boolean generateBillingStatusContent(RrcStandardMod mod) throws DataAccessException {
        return generateBillingStatContentStoreProcedure.generate(mod);
    }
    
    public boolean generateInvoiceForAcos(RrcStandardMod mod) throws DataAccessException {
        return generateInvoiceForAcosStoreProcedure.generate(mod);
    }
    
    public boolean generateSummaryStatus(RrcStandardMod mod) throws DataAccessException {
        return generateSummaryStatStoreProcedure.generate(mod);
    }
    
    public boolean generateSummaryPeriod(RrcStandardMod mod) throws DataAccessException {
        return generateSummaryPerdStoreProcedure.generate(mod);
    }

    public List listForDndWaiverDiscountScreen(int rowAt, int rowTo, String findBy, String findIn, String sortBy, String sortIn, String status) {
        System.out.println("[DndDemurrageAndDetentionJdbcDao][listForDndWaiverDiscountScreen]: Started");
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT REFERENCE_NO ");
        sql.append("    ,INVOICE_NO ");
        sql.append("    ,POST_PRE_INVOICE ");
        sql.append("    ,BL_NO ");
        sql.append("    ,INVOICE_AMT ");
        sql.append("    ,INVOICE_CURR ");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,DISCOUNT_AMT ");
        sql.append("    ,FSC ");
        sql.append("    ,DISCOUNT_STATUS_NAME ");
        sql.append("    FROM VR_DND_WAIVER_INQUIRY ");
        sql.append("    WHERE 1=1 ");
        
        if(!RutString.isEmptyString(findBy)){
            if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_REFERENCE)){
                sql.append(" AND REFERENCE_NO ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INVOICE_NO)){
                sql.append(" AND INVOICE_NO ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_PRE_POST_INVOICE)){
                sql.append(" AND POST_PRE_INVOICE ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BL)){
                sql.append(" AND BL_NO ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INV_AMT)){
                sql.append(" AND INVOICE_AMT = "+ findBy+" ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_CURR)){
                sql.append(" AND INVOICE_CURR = '"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BILL_TO_PARTY)){
                sql.append(" AND BILL_TO_PARTY = '"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_DISCOUNT_AMT)){
                sql.append(" AND DISCOUNT_AMT ="+ findBy+" ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_FSC)){
                sql.append(" AND FSC = '"+ findBy+"' ");
            }
        }
        
        
        if(!RutString.isEmptyString(status)){
            sql.append(" AND DISCOUNT_STATUS = '"+status+"' ");
        }
        
        if(!RutString.isEmptyString(sortBy)){
                if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_REFERENCE)){
                    sql.append(" ORDER BY  REFERENCE_NO ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INVOICE_NO)){
                    sql.append(" ORDER BY INVOICE_NO ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_PRE_POST_INVOICE)){
                    sql.append(" ORDER BY POST_PRE_INVOICE ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BL)){
                    sql.append(" ORDER BY BL_NO ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INV_AMT)){
                    sql.append(" ORDER BY INVOICE_AMT ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_CURR)){
                    sql.append(" ORDER BY INVOICE_CURR ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BILL_TO_PARTY)){
                    sql.append(" ORDER BY BILL_TO_PARTY ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_DISCOUNT_AMT)){
                    sql.append(" ORDER BY DISCOUNT_AMT ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_FSC)){
                    sql.append(" ORDER BY FSC ");
                }
        }else{
            sql.append("    ORDER BY REFERENCE_NO ");
            sql.append("    ,INVOICE_NO ");
            sql.append("    ,POST_PRE_INVOICE ");
            sql.append("    ,BL_NO ");
            sql.append("    ,INVOICE_AMT ");
            sql.append("    ,INVOICE_CURR ");
            sql.append("    ,BILL_TO_PARTY ");
            sql.append("    ,DISCOUNT_AMT ");
            sql.append("    ,FSC ");
            sql.append("    ,DISCOUNT_STATUS_NAME "); 
        }
        
        if(!RutString.isEmptyString(sortIn)){
            if(sortIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.SORT_IN_DESC)){
                sql.append("  DESC");
            }else{
                sql.append("  ASC");
            }
        }
        System.out.println("[DndDemurrageAndDetentionJdbcDao][listForDndWaiverDiscoutnScreen]: SQL: "+sql.toString());
        System.out.println("[DndDemurrageAndDetentionJdbcDao][listForDndWaiverDiscountScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 DndDemurrageAndDetentionWaiverMod mod = new DndDemurrageAndDetentionWaiverMod();
                 mod.setReference(RutString.nullToStr(rs.getString("REFERENCE_NO")));
                 mod.setInvoiceNo(RutString.nullToStr(rs.getString("INVOICE_NO")));
                 mod.setPrePostInvoice(RutString.nullToStr(rs.getString("POST_PRE_INVOICE")));
                 mod.setBlNo(RutString.nullToStr(rs.getString("BL_NO")));
                 mod.setDndInvAmt(rs.getInt("INVOICE_AMT"));
                 mod.setInvCurr(RutString.nullToStr(rs.getString("INVOICE_CURR")));
                 mod.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                 mod.setDiscountedAmt(rs.getInt("DISCOUNT_AMT"));
                 mod.setFsc(RutString.nullToStr(rs.getString("FSC")));
                 mod.setWaiverStatus(RutString.nullToStr(rs.getString("DISCOUNT_STATUS_NAME")));
                 return mod;
             }
         });
    }

    public int countListForDndWaiverDiscountScreen(String findBy, String findIn, String sortBy, String sortIn, String status) {
        System.out.println("[DndDemurrageAndDetentionJdbcDao][countListforDndWaiverDiscountScreen]: Started");
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT REFERENCE_NO ");
        sql.append("    ,INVOICE_NO ");
        sql.append("    ,POST_PRE_INVOICE ");
        sql.append("    ,BL_NO ");
        sql.append("    ,INVOICE_AMT ");
        sql.append("    ,INVOICE_CURR ");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,DISCOUNT_AMT ");
        sql.append("    ,FSC ");
        sql.append("    ,DISCOUNT_STATUS_NAME ");
        sql.append("    FROM VR_DND_WAIVER_INQUIRY ");
        sql.append("    WHERE 1=1 ");
        
        if(!RutString.isEmptyString(findBy)){
            if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_REFERENCE)){
                sql.append(" AND REFERENCE_NO ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INVOICE_NO)){
                sql.append(" AND INVOICE_NO ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_PRE_POST_INVOICE)){
                sql.append(" AND POST_PRE_INVOICE ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BL)){
                sql.append(" AND BL_NO ='"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INV_AMT)){
                sql.append(" AND INVOICE_AMT = "+ findBy+" ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_CURR)){
                sql.append(" AND INVOICE_CURR = '"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BILL_TO_PARTY)){
                sql.append(" AND BILL_TO_PARTY = '"+ findBy+"' ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_DISCOUNT_AMT)){
                sql.append(" AND DISCOUNT_AMT ="+ findBy+" ");
            }else if(findIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_FSC)){
                sql.append(" AND FSC = '"+ findBy+"' ");
            }
        }
        
        
        if(!RutString.isEmptyString(status)){
            sql.append(" AND DISCOUNT_STATUS = '"+status+"' ");
        }
        
        if(!RutString.isEmptyString(sortBy)){
                if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_REFERENCE)){
                    sql.append(" ORDER BY  REFERENCE_NO ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INVOICE_NO)){
                    sql.append(" ORDER BY INVOICE_NO ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_PRE_POST_INVOICE)){
                    sql.append(" ORDER BY POST_PRE_INVOICE ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BL)){
                    sql.append(" ORDER BY BL_NO ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_INV_AMT)){
                    sql.append(" ORDER BY INVOICE_AMT ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_CURR)){
                    sql.append(" ORDER BY INVOICE_CURR ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_BILL_TO_PARTY)){
                    sql.append(" ORDER BY BILL_TO_PARTY ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_DISCOUNT_AMT)){
                    sql.append(" ORDER BY DISCOUNT_AMT ");
                }else if(sortBy.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.FIND_SORT_FSC)){
                    sql.append(" ORDER BY FSC ");
                }
        }else{
            sql.append("    ORDER BY REFERENCE_NO ");
            sql.append("    ,INVOICE_NO ");
            sql.append("    ,POST_PRE_INVOICE ");
            sql.append("    ,BL_NO ");
            sql.append("    ,INVOICE_AMT ");
            sql.append("    ,INVOICE_CURR ");
            sql.append("    ,BILL_TO_PARTY ");
            sql.append("    ,DISCOUNT_AMT ");
            sql.append("    ,FSC ");
            sql.append("    ,DISCOUNT_STATUS_NAME "); 
        }
        
        if(!RutString.isEmptyString(sortIn)){
            if(sortIn.equalsIgnoreCase(DndDemurrageAndDetentionWaiverMod.SORT_IN_DESC)){
                sql.append("  DESC");
            }else{
                sql.append("  ASC");
            }
        }
        
        sql = this.getNumberOfAllData(sql);
        System.out.println("[DndDemurrageAndDetetionJdbcDao][countListForDndWaiverDiscountScreen]: SQL: "+sql.toString());
        System.out.println("[DndDemurrageAndDetetionJdbcDao][countListForDndWaiverDiscountScreen]: numberOfAllData: "+this.getNumberOfAllData(sql));
        System.out.println("[DndDemurrageAndDetentionJdbcDao][countListForDndWaiverDiscountScreen]: Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(), new HashMap(), Integer.class);
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DND_RPT.PRR_INS_DND_FREE_DAYS";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate,STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_bl_no",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_add_user",Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id",Types.VARCHAR));
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod){
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod , RrcStandardMod outputMod){
            boolean isSuccess = false;
            DndDemurrageAndDetentionMod aInputMod = (DndDemurrageAndDetentionMod)inputMod;
            if((inputMod instanceof DndDemurrageAndDetentionMod) && (outputMod instanceof DndDemurrageAndDetentionMod)){
                
                Map inParameters = new HashMap(3);
                
                inParameters.put("p_bl_no",aInputMod.getBlNo());
                inParameters.put("p_record_add_user",aInputMod.getRecordAddUser());
                inParameters.put("p_session_id",aInputMod.getSessionId());
                Map outParameters = execute(inParameters);
//                if(outParameters.size() >0){
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DND_RPT.PRR_DEL_DND_FREE_DAYS";
        
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_add_user",Types.VARCHAR));
            compile();
        }
        
        protected boolean delete(final RrcStandardMod inputMod){
            boolean isSuccess = false;
            if(inputMod instanceof DndDemurrageAndDetentionMod){
                DndDemurrageAndDetentionMod aInputMod = (DndDemurrageAndDetentionMod)inputMod;
                    Map inParameters = new HashMap(2);
                    
                    System.out.println("[DndDemurrageAndDetentionJdbcDao][DeleteStoreProcedure][delete]: p_session_id: "+ aInputMod.getSessionId());
                    System.out.println("[DndDemurrageAndDetentionJdbcDao][DeleteStoreProcedure][delete]: p_record_add_user: "+ aInputMod.getRecordAddUser());
                    
                    inParameters.put("p_session_id",aInputMod.getSessionId());
                    inParameters.put("p_record_add_user",aInputMod.getRecordAddUser());
                    execute(inParameters);
                    isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class GenerateBillingStatContentStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DND_RPT.PRR_GEN_DND_107_CONTENT_PRT";
        
        protected GenerateBillingStatContentStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_rpt_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_fr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ctr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr_perm", Types.VARCHAR));
            // #01 dnd107 start
            declareParameter(new SqlParameter("p_det_dem", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dnd_type", Types.VARCHAR));
            //#01 dnd107 end
            //##02 start
             declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            //##02 end
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
                if ((inputMod instanceof DndDndBillingStatusContentMod) && (outputMod instanceof DndDndBillingStatusContentMod)) {
                Map inParameters = new HashMap();
                DndDndBillingStatusContentMod aInputMod = (DndDndBillingStatusContentMod) inputMod;
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_rpt_type", aInputMod.getReportType());
                inParameters.put("p_date_fr", aInputMod.getDateFrom());
                inParameters.put("p_date_to", aInputMod.getDateTo());
                inParameters.put("p_ctr", aInputMod.getCountry());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_usr_perm", aInputMod.getUserPerm());
                //#01 dnd107 start
                inParameters.put("p_det_dem", aInputMod.getDemurageDetention());
                inParameters.put("p_dnd_type", aInputMod.getDndType());
                //#01 dnd107 end
                //##02 start
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_bl", aInputMod.getBl());
                
                //##02 end
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_rpt_type = "+inParameters.get("p_rpt_type"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_date_fr = "+inParameters.get("p_date_fr"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_date_to = "+inParameters.get("p_date_to"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_ctr = "+inParameters.get("p_ctr"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_usr_perm = "+inParameters.get("p_usr_perm"));
                //#01 dnd107 start
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_det_dem = "+inParameters.get("p_det_dem"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_dnd_type = "+inParameters.get("p_dnd_type"));
                 //#01 dnd107 end
                 //##02 start
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_bl = "+inParameters.get("p_bl"));
                 //##02 end
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
    protected class GenerateInvoiceForAcosStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DND_RPT.PRR_GEN_DND_107_INV_ACOS_RPT";
        
        protected GenerateInvoiceForAcosStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_rpt_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_fr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ctr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr_perm", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dnd_type", Types.VARCHAR)); //##03
            declareParameter(new SqlParameter("p_det_dem", Types.VARCHAR));  //##03
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if ((inputMod instanceof DndDndBillingStatusContentMod) && (outputMod instanceof DndDndBillingStatusContentMod)) {
                Map inParameters = new HashMap();
                DndDndBillingStatusContentMod aInputMod = (DndDndBillingStatusContentMod) inputMod;
             
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_rpt_type", aInputMod.getReportType());
                inParameters.put("p_date_fr", aInputMod.getDateFrom());
                inParameters.put("p_date_to", aInputMod.getDateTo());
                inParameters.put("p_ctr", aInputMod.getCountry());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_usr_perm", aInputMod.getUserPerm());
                inParameters.put("p_dnd_type", aInputMod.getDndType()); //##03
                inParameters.put("p_det_dem", aInputMod.getDemurageDetention()); //##03
                inParameters.put("p_bl", aInputMod.getBl());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_rpt_type = "+inParameters.get("p_rpt_type"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_date_fr = "+inParameters.get("p_date_fr"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_date_to = "+inParameters.get("p_date_to"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_ctr = "+inParameters.get("p_ctr"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_usr_perm = "+inParameters.get("p_usr_perm"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_dnd_type = "+inParameters.get("p_dnd_type")); //##03
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateInvoiceForAcosStoreProcedure]: p_det_dem = "+inParameters.get("p_det_dem")); //##03 
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_bl = "+inParameters.get("p_bl"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateBillingStatContentStoreProcedure]: p_voyage = "+inParameters.get("p_voyage"));
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
    protected class GenerateSummaryStatStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DND_RPT.PRR_GEN_DND_107_SUM_STAT_RPT";
        
        protected GenerateSummaryStatStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ctr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            
            // Added by NUTTHA1 (Feb 20, 2013) Enable Import/Export for DND Summary By Period/Status
            declareParameter(new SqlParameter("p_dnd_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_demurage_detention", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr_perm", Types.VARCHAR));
            
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if ((inputMod instanceof DndDndSummaryStatMod) && (outputMod instanceof DndDndSummaryStatMod)) {
                Map inParameters = new HashMap();
                DndDndSummaryStatMod aInputMod = (DndDndSummaryStatMod) inputMod;                
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_ctr", aInputMod.getCountry());
                inParameters.put("p_fsc", aInputMod.getFsc());
                
                // Added by NUTTHA1 (Feb 20, 2013) Enable Import/Export for DND Summary By Period/Status
                inParameters.put("p_dnd_type", aInputMod.getDndType());
                inParameters.put("p_demurage_detention", aInputMod.getDemurage_Detention());                
                inParameters.put("p_bl", aInputMod.getBl());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_usr_perm", aInputMod.getUserPerm());
                
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_ctr = "+inParameters.get("p_ctr"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_bl = "+inParameters.get("p_bl"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_demurage_detention = "+inParameters.get("p_demurage_detention"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_usr_perm = "+inParameters.get("p_usr_perm"));
                
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
    protected class GenerateSummaryPerdStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DND_RPT.PRR_GEN_DND_107_SUM_PERD_RPT";
        
        protected GenerateSummaryPerdStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ctr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            
            // Added by NUTTHA1 (Feb 20, 2013) Enable Import/Export for DND Summary By Period/Status
            declareParameter(new SqlParameter("p_dnd_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_demurage_detention", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr_perm", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if ((inputMod instanceof DndDndSummaryStatMod) && (outputMod instanceof DndDndSummaryStatMod)) {
                Map inParameters = new HashMap();
                DndDndSummaryStatMod aInputMod = (DndDndSummaryStatMod) inputMod;
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_ctr", aInputMod.getCountry());
                inParameters.put("p_fsc", aInputMod.getFsc());
                
                // Added by NUTTHA1 (Feb 20, 2013) Enable Import/Export for DND Summary By Period/Status
                inParameters.put("p_dnd_type", aInputMod.getDndType());
                inParameters.put("p_demurage_detention", aInputMod.getDemurage_Detention());
                inParameters.put("p_bl", aInputMod.getBl());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_usr_perm", aInputMod.getUserPerm());
                
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryPerdStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryPerdStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryPerdStoreProcedure]: p_ctr = "+inParameters.get("p_ctr"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryPerdStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_demurage_detention = "+inParameters.get("p_demurage_detention"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryPerdStoreProcedure]: p_usr_perm = "+inParameters.get("p_usr_perm"));
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
    
}
