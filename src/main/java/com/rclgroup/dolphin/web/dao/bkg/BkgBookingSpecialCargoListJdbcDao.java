package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.bkg.BkgBookingSpecialCargoListMod;
import com.rclgroup.dolphin.web.model.bkg.BkgRejectedAndCancelledBookingReportMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BkgBookingSpecialCargoListJdbcDao extends RrcStandardDao implements BkgBookingSpecialCargoListDao{
    private BkgBookingSpecialCargoListStoreProcedure bkgBookingSpecialCargoListStoreProcedure;
    
    public BkgBookingSpecialCargoListJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        bkgBookingSpecialCargoListStoreProcedure = new BkgBookingSpecialCargoListStoreProcedure(getJdbcTemplate());
    }
    
    public boolean generateTempTable(RrcStandardMod mod) throws DataAccessException{
        return bkgBookingSpecialCargoListStoreProcedure.generate(mod);
    }

    protected class BkgBookingSpecialCargoListStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_BKG_SPECIAL_CARGO_LIST.PRR_GEN_BKG111_BOOKING";
        
        protected BkgBookingSpecialCargoListStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_search_by", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_from", Types.INTEGER));
            declareParameter(new SqlParameter("p_date_to", Types.INTEGER));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_direction", Types.VARCHAR));
            declareParameter(new SqlParameter("p_coc_soc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_booking_no", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof BkgBookingSpecialCargoListMod && outputMod instanceof BkgBookingSpecialCargoListMod) {
                BkgBookingSpecialCargoListMod aInputMod = (BkgBookingSpecialCargoListMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_user_id", aInputMod.getUserId());
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_search_by", aInputMod.getSearchBy());
                inParameters.put("p_date_from", aInputMod.getPeriodFrom());
                inParameters.put("p_date_to", aInputMod.getPeriodTo());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_direction", aInputMod.getDirection());
                inParameters.put("p_coc_soc", aInputMod.getCocSoc());
                inParameters.put("p_pol", aInputMod.getPol());
                inParameters.put("p_pod", aInputMod.getPod());
                inParameters.put("p_booking_no", aInputMod.getBookingNo());
                
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_user_id = "+inParameters.get("p_user_id"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_search_by = "+inParameters.get("p_search_by"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_date_from = "+inParameters.get("p_date_from"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_date_to = "+inParameters.get("p_date_to"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_service = "+inParameters.get("p_service"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_direction = "+inParameters.get("p_direction"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_coc_soc = "+inParameters.get("p_coc_soc"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_pol = "+inParameters.get("p_pol"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_pod = "+inParameters.get("p_pod"));
                System.out.println("[BkgBookingSpecialCargoListJdbcDao][generateTempTable]: p_booking_no = "+inParameters.get("p_booking_no"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
}
