package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamPeriodParameterMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamPeriodParameterDao {

        public List<CamPeriodParameterMod> getList(String periodParameterCode,String noOfDay,String status,String sortBy,String sortIn) throws DataAccessException;
        public List<CamPeriodParameterMod> getItem(String periodParameterCode) throws DataAccessException;
        public StringBuffer save(boolean isInsert, CamPeriodParameterMod updateData) throws Exception;
        public String[] deleteByIds(String[] idArray) throws Exception;
}
