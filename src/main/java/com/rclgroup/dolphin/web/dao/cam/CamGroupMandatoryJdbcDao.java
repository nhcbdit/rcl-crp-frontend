package com.rclgroup.dolphin.web.dao.cam;


import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamGroupMandatoryMod;


import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Timestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
/*------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sujittra 19/02/2013 
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/
public class CamGroupMandatoryJdbcDao extends RrcStandardDao implements CamGroupMandatoryDao{
    public CamGroupMandatoryJdbcDao() {
      
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List<CamGroupMandatoryMod> getListGroupCode(String groupMandatoryCode,String mandatorySet,String parameterName,String status,String sortBy,String sortIn) throws DataAccessException{
    
        CamGroupMandatoryHelperJdbcDao camGroupMandatoryHelperJdbcDao= new CamGroupMandatoryHelperJdbcDao();
        return camGroupMandatoryHelperJdbcDao.getListGroupCode(getNamedParameterJdbcTemplate(),groupMandatoryCode,mandatorySet,parameterName, status,sortBy,sortIn);
    }
    
    public List<CamGroupMandatoryMod> getList(String groupMandatoryCode) throws DataAccessException{
        CamGroupMandatoryHelperJdbcDao camGroupMandatoryHelperJdbcDao= new CamGroupMandatoryHelperJdbcDao();
        return camGroupMandatoryHelperJdbcDao.getList(getNamedParameterJdbcTemplate(),groupMandatoryCode);
    }
    
    public StringBuffer save(boolean isInsert,List<CamGroupMandatoryMod> newDataList, List<CamGroupMandatoryMod> updateList,String delIds) throws Exception {
    
        CamGroupMandatoryHelperJdbcDao camGroupMandatoryHelperJdbcDao= new CamGroupMandatoryHelperJdbcDao();
        return camGroupMandatoryHelperJdbcDao.save(getJdbcTemplate(),getNamedParameterJdbcTemplate(),isInsert,newDataList,  updateList,delIds);
      
    }
    
    
    public String[] deleteByGroupCodes(String[] groupCodeArray) throws Exception{
        CamGroupMandatoryHelperJdbcDao camGroupMandatoryHelperJdbcDao= new CamGroupMandatoryHelperJdbcDao();
        return camGroupMandatoryHelperJdbcDao.deleteByGroupCodes(getJdbcTemplate(),getNamedParameterJdbcTemplate(),groupCodeArray);
    }

  /*
   public void insertData(List<CamGroupMandatoryMod> newDataList) throws DataAccessException{
       StringBuffer insertSql =new StringBuffer(" INSERT INTO cam_group_mandatory ");
                    insertSql.append(" ( pk_cam_group_mandatory_id ");
                    insertSql.append(", group_mandatory_code");
                    insertSql.append(", mandatory_set");
                    insertSql.append(", parameter_name");
                    insertSql.append(", is_unselect_disable");
                    insertSql.append(", record_status");
                    insertSql.append(", record_add_user");
                    insertSql.append(", record_add_date");
                    insertSql.append(", record_change_user");
                    insertSql.append(", record_change_date");
                    insertSql.append(" ) ");
                    insertSql.append(" VALUES ( ?,?,?,?,?,?,?,SYSDATE,?,SYSDATE )");
                    
       CamGroupMandatoryMod newBean = null;
       if(newDataList !=null && newDataList.size()>0){
           for(int i = 0 ; i < newDataList.size(); i++){
               
               newBean = newDataList.get(i);
               newBean.setPkCamGroupMandatoryId(getPkId());
               
               final CamGroupMandatoryMod updateData=newBean;
               
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getPkCamGroupMandatoryId() : "+updateData.getPkCamGroupMandatoryId());
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getGroupMandatoryCode() : "+updateData.getGroupMandatoryCode());
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getMandatorySet() : "+updateData.getMandatorySet());
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getParameterName() : "+updateData.getParameterName());
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getIsUnselDisable() : "+updateData.getIsUnselDisable());
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getRecordStatus() : "+updateData.getRecordStatus());
               System.out.println ("[CamGroupMandatoryJdbcDao][insertData]: updateData.getRecordChangeUser() : "+updateData.getRecordChangeUser());
               
               getJdbcTemplate().update(insertSql.toString(),new PreparedStatementSetter() {
                                                                                     public void setValues(PreparedStatement ps) throws SQLException {
                                                                                     
                                                                                       ps.setLong(1,  updateData.getPkCamGroupMandatoryId());
                                                                                       ps.setString(2, updateData.getGroupMandatoryCode());
                                                                                       ps.setString(3, updateData.getMandatorySet());
                                                                                       ps.setString(4, updateData.getParameterName());
                                                                                       ps.setString(5, updateData.getIsUnselDisable());
                                                                                       ps.setString(6, updateData.getRecordStatus());
                                                                                       ps.setString(7, updateData.getRecordChangeUser());
                                                                                       ps.setString(8, updateData.getRecordChangeUser());
                                                                                     
                                                                                     }
               });
            
           }
       }
   }
   
   
    public void updateData(List<CamGroupMandatoryMod> updateList) throws DataAccessException{
        StringBuffer updateSql =new StringBuffer(" UPDATE cam_group_mandatory ");
                     updateSql.append(" SET ");
                     updateSql.append("  mandatory_set =? ");
                     updateSql.append(", parameter_name=? ");
                     updateSql.append(", is_unselect_disable=? ");
                     updateSql.append(", record_status=? ");
                     updateSql.append(", record_change_user=? ");
                     updateSql.append(", record_change_date=SYSDATE ");
                     updateSql.append(" WHERE pk_cam_group_mandatory_id =? ");
                     
        CamGroupMandatoryMod updataBean = null;
        if(updateList !=null && updateList.size()>0){
            for(int i = 0 ; i < updateList.size(); i++){
                
                updataBean = updateList.get(i);
                
                final CamGroupMandatoryMod updateData=updataBean;
                
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getPkCamGroupMandatoryId() : "+updateData.getPkCamGroupMandatoryId());
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getGroupMandatoryCode() : "+updateData.getGroupMandatoryCode());
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getMandatorySet() : "+updateData.getMandatorySet());
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getParameterName() : "+updateData.getParameterName());
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getIsUnselDisable() : "+updateData.getIsUnselDisable());
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getRecordStatus() : "+updateData.getRecordStatus());
                System.out.println ("[CamGroupMandatoryJdbcDao][updateData]: updateData.getRecordChangeUser() : "+updateData.getRecordChangeUser());
                
             
                getJdbcTemplate().update(updateSql.toString(),new PreparedStatementSetter() {
                                                                                      public void setValues(PreparedStatement ps) throws SQLException {
                                                                                      
                                                                                        ps.setString(1, updateData.getMandatorySet());
                                                                                        ps.setString(2, updateData.getParameterName());
                                                                                        ps.setString(3, updateData.getIsUnselDisable());
                                                                                        ps.setString(4, updateData.getRecordStatus());
                                                                                        ps.setString(5, updateData.getRecordChangeUser());
                                                                                        ps.setLong(6,  updateData.getPkCamGroupMandatoryId());
                                                                                      
                                                                                      }
                });
             
            }
        }
    }

    public void deleteManyData(final String idsForIn) throws DataAccessException{
            System.out.println ("[CamGroupMandatoryJdbcDao][deleteManyData]: idsForIn : "+idsForIn);
            StringBuffer deleteSql =new StringBuffer(" DELETE cam_group_mandatory ");
                     deleteSql.append(" WHERE pk_cam_group_mandatory_id in ( "+idsForIn+" ) ");
            
                this.getJdbcTemplate().update(deleteSql.toString());

    }
    
    public void deleteData(final String id) throws DataAccessException{
            System.out.println ("[CamGroupMandatoryJdbcDao][deleteManyData]: deleteData : "+id);
            StringBuffer deleteSql =new StringBuffer(" DELETE cam_group_mandatory ");
                     deleteSql.append(" WHERE GROUP_MANDATORY_CODE =? ");
            
                this.getJdbcTemplate().update(deleteSql.toString(),new PreparedStatementSetter() {
                                                                                      public void setValues(PreparedStatement ps) throws SQLException {
                                                                                      
                                                                                        ps.setString(1, id);
                                                                                       
                                                                                        }
                });
                
       
        
            

    
        
    }

   private long getPkId(){
        return getNamedParameterJdbcTemplate().queryForLong("SELECT SR_CAM_GMA01.nextVal FROM dual",new HashMap());
  }
  
  private boolean checkGroupMandatoryExistService(String groupMandatoryCode){
  
      int count = getNamedParameterJdbcTemplate().queryForInt("SELECT count(FK_GROUP_MANDATORY_CODE) FROM CAM_SERVICE WHERE FK_GROUP_MANDATORY_CODE ='"+groupMandatoryCode+"'",new HashMap());
      
      if(count>0){
          return true;
      }
      
      return false;
      
  }
  
    private boolean checkGroupMandatoryCodeExist(String groupMandatoryCode){
    
        int count = getNamedParameterJdbcTemplate().queryForInt("select count(GROUP_MANDATORY_CODE) from CAM_GROUP_MANDATORY WHERE GROUP_MANDATORY_CODE ='"+groupMandatoryCode+"'",new HashMap());
        
        if(count>0){
            return true;
        }
        
        return false;
        
    }

   */
}
