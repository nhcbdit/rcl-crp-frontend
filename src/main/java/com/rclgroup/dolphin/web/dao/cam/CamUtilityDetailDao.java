/*-----------------------------------------------------------------------------------------------------------  
CamUtilityDetailDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 11/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamUtilityDetailMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamUtilityDetailDao extends RriStandardDao {
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
    public boolean update(RrcStandardMod mod) throws DataAccessException;
    
    public CamUtilityDetailMod findUtilityDetailByKey(String utilityDtlId) throws DataAccessException;
    
    public List findUtilityDetailByHdrId(String utilityHdrId, String status) throws DataAccessException;

    //public List findUtilityDetailByCriteria(String moduleCode, String country, String status) throws DataAccessException;

}


