/*------------------------------------------------------
EmsEquipmentTypeDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 10/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
01 18/10/07 MNW              Added listForHelpScreen(), moveDbToModelForHelp()
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.ems;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface EmsEquipmentTypeDao {
    
    public boolean isValid(String equipmentType) throws DataAccessException;
    
    public boolean isValid(String equipmentType, String status) throws DataAccessException;
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;
    
}