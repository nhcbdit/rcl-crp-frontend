package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaModelMod;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaServiceVariantVolumePortPairMod;
import com.rclgroup.dolphin.web.model.bsa.BsaSupportedPortGroupMod;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.text.DecimalFormat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class BsaBsaServiceVariantVolumePortPairJdbcDao extends RrcStandardDao implements BsaBsaServiceVariantVolumePortPairDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;
//    private BsaBsaServiceVariantVolumePortPairJdbcDao bsaBsaServiceVariantVolumePortPairJdbcDao;
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());    
        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
    } 
    public BsaBsaServiceVariantVolumePortPairJdbcDao() {
    }
    
//    public BsaBsaServiceVariantVolumePortPairJdbcDao(BsaBsaServiceVariantVolumePortPairJdbcDao bsaBsaServiceVariantVolumePortPairJdbcDao) {
//        this.bsaBsaServiceVariantVolumePortPairJdbcDao = bsaBsaServiceVariantVolumePortPairJdbcDao;
//    }
    
    public List listForSearchScreen(String serviceVariantId) {
        Map columnMap = new HashMap();        
        
//        String sqlSearchCriteria = createSqlSearchCriteria(searchMod, columnMap, modelName);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT V.BSA_MODEL_ID ");
        sql.append(",   TMP.BSA_SERVICE_VARIANT_ID ");
        sql.append(",   TMP.PK_BSA_ROUTE_ID  ");
        sql.append(",   TMP.POL_PORT_ID     ");
        sql.append(",   TMP.POL_PORT_GROUP_FLAG ");
        sql.append(",   TMP.POL_PORT_CODE   ");
        sql.append(",   TMP.POL_PORT_SEQ_NO ");
        sql.append(",   TMP.POL_TRANSHIPMENT_FLAG ");
        sql.append(",   TMP.POL_CALL_LEVEL   ");
        sql.append(",   TMP.POD_PORT_ID      ");
        sql.append(",   TMP.POD_PORT_GROUP_FLAG ");
        sql.append(",   TMP.POD_PORT_CODE    ");
        sql.append(",   TMP.POD_PORT_SEQ_NO  ");
        sql.append(",   TMP.POD_TRANSHIPMENT_FLAG ");
        sql.append(",   TMP.POD_CALL_LEVEL   ");
        sql.append(",   TMP.TS_RECORD        ");
        sql.append(",   TMP.TS_INDICATOR     ");
        sql.append(",   TMP.TS_SERVICE       ");
        sql.append(",   TMP.TS_VARIANT       ");
        sql.append(",   TMP.TS_PORT_CODE     ");
        sql.append(",   TMP.TS_PORT_GROUP    ");
        sql.append(",   CASE WHEN (TS_RECORD = 'Y') THEN (SELECT DECODE(UPPER(TMP.TS_INDICATOR),'FROM',POL_CALL_LEVEL,POD_CALL_LEVEL) FROM VR_BSA_ALLOCATION_PORT_PAIRS ");
        sql.append("    WHERE PK_BSA_ROUTE_ID = (SELECT NVL(FK_PREV_BSA_ROUTE_ID,FK_NEXT_BSA_ROUTE_ID) AS TS_ROUTE_ID FROM BSA_ROUTE WHERE PK_BSA_ROUTE_ID = TMP.PK_BSA_ROUTE_ID)) ");
        sql.append("    ELSE 'P' END TS_PORT_LEVEL ");
        sql.append(",   TMP.COC_TEU_LADEN    ");
        sql.append(",   TMP.COC_TON_LADEN    ");
        sql.append(",   TMP.COC_20GP         ");
        sql.append(",   TMP.COC_40GP         ");
        sql.append(",   TMP.COC_45HC         ");
        sql.append(",   TMP.COC_REEFER_PLUGS ");
        sql.append(",   TMP.COC_TEU_MT       ");
        sql.append(",   TMP.COC_20MT         ");
        sql.append(",   TMP.COC_40MT         ");
        sql.append(",   TMP.SOC_TEU_LADEN    ");
        sql.append(",   TMP.SOC_TON_LADEN    ");
        sql.append(",   TMP.SOC_TEU_MT       ");
        sql.append(",   TMP.TOLERANT       ");
        sql.append(",   TMP.RECORD_STATUS       ");
        sql.append(",   TMP.RECORD_ADD_USER       ");
        sql.append(",   TMP.RECORD_ADD_DATE       ");
        sql.append(",   TMP.RECORD_CHANGE_USER       ");
        sql.append(",   TMP.RECORD_CHANGE_DATE           ");  
        sql.append(",   TMP.TS_PROFORMA_NO           "); // show ts proforma on bsa port pair screen 17/04/2012
        sql.append(",    (SELECT SOURCE_OF_TS_FIGURES FROM BSA_ROUTE WHERE PK_BSA_ROUTE_ID = TMP.PK_BSA_ROUTE_ID ) AS TS_SOURCE ");
        sql.append(",    (SELECT SLOT_TEU FROM VR_BSA_ALLOCATION_PORT_CALLS P WHERE P.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID AND P.PK_BSA_PORT_CALL_ID = TMP.POL_PORT_ID ) AS SLOT_TEU ");
        sql.append(",    (SELECT SLOT_TONS FROM VR_BSA_ALLOCATION_PORT_CALLS P WHERE P.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID AND P.PK_BSA_PORT_CALL_ID = TMP.POL_PORT_ID ) AS SLOT_TONS ");
        sql.append(",    (SELECT SLOT_REEFER_PLUGS FROM VR_BSA_ALLOCATION_PORT_CALLS P WHERE P.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID AND P.PK_BSA_PORT_CALL_ID = TMP.POL_PORT_ID ) AS SLOT_REEFER_PLUGS ");
        sql.append(",    (SELECT AVG_COC_TEU_WEIGHT FROM VR_BSA_ALLOCATION_PORT_CALLS P WHERE P.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID AND P.PK_BSA_PORT_CALL_ID = TMP.POL_PORT_ID ) AS AVG_COC_TEU_WEIGHT ");
        sql.append(",    (SELECT AVG_SOC_TEU_WEIGHT FROM VR_BSA_ALLOCATION_PORT_CALLS P WHERE P.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID AND P.PK_BSA_PORT_CALL_ID = TMP.POL_PORT_ID ) AS AVG_SOC_TEU_WEIGHT ");
        sql.append(",    (SELECT MIN_TEU FROM VR_BSA_ALLOCATION_PORT_CALLS P WHERE P.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID AND P.PK_BSA_PORT_CALL_ID = TMP.POL_PORT_ID ) AS MIN_TEU ");
        sql.append(" FROM  VR_BSA_ALLOCATION_PORT_PAIRS TMP JOIN VR_BSA_SERVICE_VARIANT V ");
        sql.append(" ON  V.BSA_SERVICE_VARIANT_ID =  TMP.BSA_SERVICE_VARIANT_ID ");
        sql.append(" WHERE TMP.BSA_SERVICE_VARIANT_ID = :serviceVariantId ");
        sql.append(" AND RCLAPPS.PCR_BSA_ROUTE.FR_CHECK_FUTURE_TS_ALLOCATION (tmp.PK_BSA_ROUTE_ID) = 'Y' ");

//        sql.append(sqlSearchCriteria);
        System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][listForSearchScreen][serviceVariantId]: " +serviceVariantId);
        System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][listForSearchScreen]: sql: [" + sql.toString() + "]");
        HashMap map = new HashMap();
        map.put("serviceVariantId",serviceVariantId);
        
        return getNamedParameterJdbcTemplate().query(sql.toString(), 
            map, 
            new RowMapper() {
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    BsaBsaServiceVariantVolumePortPairMod bean = new BsaBsaServiceVariantVolumePortPairMod();
                    bean.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                    bean.setBsaServiceVariantId(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                    bean.setBsaRouteId(RutString.nullToStr(rs.getString("PK_BSA_ROUTE_ID")));
                    bean.setPolPortCallId(RutString.nullToStr(rs.getString("POL_PORT_ID")));
                    bean.setPolGroupFlag(RutString.nullToStr(rs.getString("POL_PORT_GROUP_FLAG")));
                    if(bean.getPolGroupFlag().equals(RcmConstant.FLAG_YES)){
                        bean.setPolGroup(RutString.nullToStr(rs.getString("POL_PORT_CODE")));
                    }else{
                        bean.setPolPortCall(RutString.nullToStr(rs.getString("POL_PORT_CODE")));
                    }
                    bean.setPolSeqNo(RutString.nullToStr(rs.getString("POL_PORT_SEQ_NO")));
                    bean.setPolTsFlag(RutString.nullToStr(rs.getString("POL_TRANSHIPMENT_FLAG")));
                    bean.setPodPortCallId(RutString.nullToStr(rs.getString("POD_PORT_ID")));
                    bean.setPodGroupFlag(RutString.nullToStr(rs.getString("POD_PORT_GROUP_FLAG")));                    
                    if(bean.getPodGroupFlag().equals(RcmConstant.FLAG_YES)){
                        bean.setPodGroup(RutString.nullToStr(rs.getString("POD_PORT_CODE")));
                    }else{
                        bean.setPodPortCall(RutString.nullToStr(rs.getString("POD_PORT_CODE")));
                    }
                    bean.setPodSeqNo(RutString.nullToStr(rs.getString("POD_PORT_SEQ_NO")));
                    bean.setPodTsFlag(RutString.nullToStr(rs.getString("POD_TRANSHIPMENT_FLAG")));
                    bean.setPolCallLv(RutString.nullToStr(rs.getString("POL_CALL_LEVEL")));
                    bean.setPodCallLv(RutString.nullToStr(rs.getString("POD_CALL_LEVEL")));
                    bean.setTsRecord(RutString.nullToStr(rs.getString("TS_RECORD")));
                    bean.setTsIndic(RutString.nullToStr(rs.getString("TS_INDICATOR")));
                    bean.setTsService(RutString.nullToStr(rs.getString("TS_SERVICE")));
                    bean.setTsVariant(RutString.nullToStr(rs.getString("TS_VARIANT")));
                    bean.setTsPort(RutString.nullToStr(rs.getString("TS_PORT_CODE")));
                    bean.setTsGroup(RutString.nullToStr(rs.getString("TS_PORT_GROUP")));
                    bean.setTsPortLevel(RutString.nullToStr(rs.getString("TS_PORT_LEVEL")));
                    bean.setCocTeuFull(RutString.nullToStr(rs.getString("COC_TEU_LADEN")));
                    bean.setCocFullWeight(RutString.nullToStr(rs.getString("COC_TON_LADEN")));
                    bean.setCoc20Gp(RutString.nullToStr(rs.getString("COC_20GP")));
                    bean.setCoc40Gp(RutString.nullToStr(rs.getString("COC_40GP")));
                    bean.setCoc45Hc(RutString.nullToStr(rs.getString("COC_45HC")));
                    bean.setCocReefer(RutString.nullToStr(rs.getString("COC_REEFER_PLUGS")));
                    bean.setCocTeuMt(RutString.nullToStr(rs.getString("COC_TEU_MT")));
                    bean.setCocMt20(RutString.nullToStr(rs.getString("COC_20MT")));
                    bean.setCocMt40(RutString.nullToStr(rs.getString("COC_40MT")));
                    bean.setSocTeuFull(RutString.nullToStr(rs.getString("SOC_TEU_LADEN")));
                    bean.setSocFullWeight(RutString.nullToStr(rs.getString("SOC_TON_LADEN")));
                    bean.setSocTeuMt(RutString.nullToStr(rs.getString("SOC_TEU_MT")));   
                    bean.setTsSource(RutString.nullToStr(rs.getString("TS_SOURCE")));   
                    bean.setPortCallSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));
                    bean.setPortCallSlotTons(RutString.nullToStr(rs.getString("SLOT_TONS")));
                    bean.setPortCallSlotReefer(RutString.nullToStr(rs.getString("SLOT_REEFER_PLUGS")));
                    bean.setPortCallAvgCocTeu(RutString.nullToStr(rs.getString("AVG_COC_TEU_WEIGHT")));
                    bean.setPortCallAvgSocTeu(RutString.nullToStr(rs.getString("AVG_SOC_TEU_WEIGHT")));
                    bean.setPortCallMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));                    
                    bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                    bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                    bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                    bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                    bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                    if(RutString.nullToStr(rs.getString("TOLERANT")).equals("")||rs.getString("TOLERANT").equals("0")){
                        bean.setTolelant("0.00");
                    }else{
                        DecimalFormat dc=new DecimalFormat();
                        dc.applyPattern("###.00");
                        bean.setTolelant(dc.format(rs.getDouble("TOLERANT")));
                    }
                    // show ts proforma on bsa port pair screen 17/04/2012
                    bean.setProformaRefNo(RutString.nullToStr(rs.getString("TS_PROFORMA_NO")));
//                    System.out.println("##############################################");
                    return bean;
                }
            });
    }
    public void saveDetails(RrcStandardMod masterMod, String masterOperationFlag, List detailMods, Vector detailOperationFlags) throws DataAccessException{
//        BsaBsaServiceVariantMod mod = (BsaBsaServiceVariantMod)masterMod;
        //begin: save record change date for concurrency control
//        Timestamp masterRecordChangeDate = Timestamp.valueOf(mod.getRecordChangeDate().toString());

        Vector detailRecordChangeDates = new Vector();
        for(int i=0;i<detailMods.size();i++){
            BsaBsaServiceVariantVolumePortPairMod detailMod = (BsaBsaServiceVariantVolumePortPairMod)detailMods.get(i);
            detailRecordChangeDates.add(Timestamp.valueOf(detailMod.getRecordChangeDate().toString()));
        }
        //end: save record change date for concurrency control
        BsaBsaServiceVariantVolumePortPairMod detailMod;
        StringBuffer errorMsgBuffer = new StringBuffer();        
        int index = 0;        
        for(index=0;index<detailMods.size();index++){
            try{
//                String detailOperationFlag = (String)detailOperationFlags.get(index);
                detailMod = (BsaBsaServiceVariantVolumePortPairMod)detailMods.get(index);
//                detailMod.setBsaModelId(mod.getBsaModelId());
//                System.out.println("line at "+index);
                if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)){
//                    index++;
//                    System.out.println("insert");
                    insert(detailMod);
                }else if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)){
//                    index++;
//                    System.out.println("update");
                    update(detailMod);
                }else if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_DELETE)){
                    System.out.println("delete");
                    delete(detailMod); 
//                }else if(detailOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_MODIFY)){
//                    index++;
                }
            }catch(CustomDataAccessException e){                                
                errorMsgBuffer.append(e.getMessages()+"%"+index+"&");
                System.out.println(errorMsgBuffer.toString());
            }catch(DataAccessException e){
                e.printStackTrace();
            } 
        }
        

        if(!errorMsgBuffer.toString().equals("")){
            //begin: put record change date back
//            mod.setRecordChangeDate(masterRecordChangeDate);
            for(int i=0;i<detailMods.size();i++){
                detailMod = (BsaBsaServiceVariantVolumePortPairMod)detailMods.get(i);
                detailMod.setRecordChangeDate((Timestamp)detailRecordChangeDates.get(i));
            } 
            //end: put record change date back
             
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        checkConstraints(detailMods,detailRecordChangeDates);
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    
    public void checkConstraints(List detailMods,Vector detailRecordChangeDates){
//        BsaBsaModelMod mod = (BsaBsaModelMod)masterMod;
        BsaBsaServiceVariantVolumePortPairMod detailMod = new BsaBsaServiceVariantVolumePortPairMod();
        StringBuffer errorMsgBuffer = new StringBuffer();
        int index = 0;
        for(int i=0;i<detailMods.size();i++){
//                String detailOperationFlag = (String)detailOperationFlags.get(i);
                detailMod = (BsaBsaServiceVariantVolumePortPairMod)detailMods.get(i);
//                detailMod.setBsaModelId(mod.getBsaModelId());
                if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)||
                   detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)){
                    index++;
                }else if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_DELETE)){
                    index++;
                }else if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_MODIFY)){
                    index++;
                }
        }
        try{
            if(index > 0){
                checkConstraint(detailMod);
            }
        }catch(CustomDataAccessException e){
            errorMsgBuffer.append(e.getMessages());
            System.out.print("[BsaBsaServiceVariantVolumePortPairJdbcDao][Check constraint] :errorMsgBuffer= "+errorMsgBuffer.toString());
        }catch(DataAccessException e){
            e.printStackTrace();
            System.out.print("[BsaBsaServiceVariantVolumePortPairJdbcDao][Check constraint] :errorMsgBuffer= "+errorMsgBuffer.toString());
        } 
        
        if(!errorMsgBuffer.toString().equals("")){
            //begin: put record change date back
            System.out.println("size = "+detailMods.size()+"    vec size ="+detailRecordChangeDates.size());
//            mod.setRecordChangeDate(masterRecordChangeDate);
            for(int i=0;i<detailMods.size();i++){
                detailMod = (BsaBsaServiceVariantVolumePortPairMod)detailMods.get(i);
                detailMod.setRecordChangeDate((Timestamp)detailRecordChangeDates.get(i));
            } 
            
            //end: put record change date back
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
    }
    
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException{
        checkConstraintStoreProcedure.checkConstraint(mod);
    }

    protected class InsertStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_BSA_ROUTE.PRR_INS_BSA_ROUTE_TS";
         
            protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
                declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_pk_bsa_route_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_fk_pol_bsa_port_call_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_fk_pod_bsa_port_call_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_pol_bsa_port_call_code", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_pod_bsa_port_call_code", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_service", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_variant", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_indicator", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_port", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_group", Types.VARCHAR));                
                declareParameter(new SqlInOutParameter("p_coc_teu_laden", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_ton_laden", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_20gp", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_40gp", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_45hc", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_reefer_plugs", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_teu_mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_20mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_40mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_soc_teu_laden", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_soc_ton_laden", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_soc_teu_mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
                declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
                declareParameter(new SqlInOutParameter("p_tolerant", Types.NUMERIC));
                // send ts proforma 17/04/2012
                declareParameter(new SqlInOutParameter("P_TS_PROFORMA_NO", Types.VARCHAR));
                compile();
            }

            protected boolean insert(RrcStandardMod mod) {
                return insert(mod,mod);
            }
         
            protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
                boolean isSuccess = false; 
                if((inputMod instanceof BsaBsaServiceVariantVolumePortPairMod)&&(outputMod instanceof BsaBsaServiceVariantVolumePortPairMod)){
                    BsaBsaServiceVariantVolumePortPairMod aInputMod = (BsaBsaServiceVariantVolumePortPairMod)inputMod;
                    BsaBsaServiceVariantVolumePortPairMod aOutputMod = (BsaBsaServiceVariantVolumePortPairMod)outputMod;
                    Map inParameters = new HashMap(30);
    ////
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_model_id:"+aInputMod.getBsaModelId());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_pod_port_call_id:"+aInputMod.getPodPortCallId());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_pol_port_call_id:"+aInputMod.getPolPortCallId());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_pod_port_call_code:"+aInputMod.getPodPortCall());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_pol_port_call_code:"+aInputMod.getPolPortCall());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_route_id:"+aInputMod.getBsaRouteId());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_ts_service:"+aInputMod.getTsService().toUpperCase());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_ts_variant:"+aInputMod.getTsVariant().toUpperCase());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_ts_port:"+aInputMod.getTsPort().toUpperCase());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_ts_group:"+aInputMod.getTsGroup().toUpperCase());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_ts_indic:"+aInputMod.getTsIndic());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_bsa_ts_indic:"+aInputMod.getTolelant());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:P_TS_PROFORMA_NO:"+aInputMod.getProformaRefNo().toUpperCase());

//                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
//                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
//                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
//                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
//                    System.out.println("[BsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////

//                    inParameters.put("p_bsa_port_group_id", new Integer((RutString.nullToStr(aInputMod.getBsaSupportedPortGroupId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaSupportedPortGroupId())));
                    
                    inParameters.put("p_bsa_model_id", (aInputMod.getBsaModelId().equals(""))?null:new Integer(aInputMod.getBsaModelId()));
                    inParameters.put("p_pk_bsa_route_id", (aInputMod.getBsaRouteId().equals(""))?null:new Integer(aInputMod.getBsaRouteId()));
                    inParameters.put("p_fk_pol_bsa_port_call_id", (aInputMod.getPolPortCallId().equals(""))?null:new Integer(aInputMod.getPolPortCallId()));
                    inParameters.put("p_fk_pod_bsa_port_call_id", (aInputMod.getPodPortCallId().equals(""))?null:new Integer(aInputMod.getPodPortCallId()));
                    inParameters.put("p_pol_bsa_port_call_code", aInputMod.getPolPortCall());
                    inParameters.put("p_pod_bsa_port_call_code", aInputMod.getPodPortCall());
//                    inParameters.put("p_fk_prev_pol_bsa_port_call_id", aInputMod.get);
//                    inParameters.put("p_fk_prev_bsa_route_id", aInputMod.);
//                    inParameters.put("p_fk_next_pod_bsa_port_call_id", aInputMod.);
//                    inParameters.put("p_fk_next_bsa_route_id", aInputMod.);
                    inParameters.put("p_ts_service", aInputMod.getTsService().toUpperCase());
                    inParameters.put("p_ts_variant", aInputMod.getTsVariant().toUpperCase());
                    inParameters.put("p_ts_indicator", aInputMod.getTsIndic());
                    inParameters.put("p_ts_port", aInputMod.getTsPort().toUpperCase());
                    inParameters.put("p_ts_group", aInputMod.getTsGroup().toUpperCase());
                    inParameters.put("p_coc_teu_laden", (aInputMod.getCocTeuFull().equals("")?null:new Long(aInputMod.getCocTeuFull())));
                    inParameters.put("p_coc_ton_laden", (aInputMod.getCocFullWeight().equals("")?null:new Double(aInputMod.getCocFullWeight())));
                    inParameters.put("p_coc_20gp", (aInputMod.getCoc20Gp().equals("")?null:new Long(aInputMod.getCoc20Gp())));
                    inParameters.put("p_coc_40gp", (aInputMod.getCoc40Gp().equals("")?null:new Long(aInputMod.getCoc40Gp())));
                    inParameters.put("p_coc_45hc", (aInputMod.getCoc45Hc().equals("")?null:new Long(aInputMod.getCoc45Hc())));
                    inParameters.put("p_coc_reefer_plugs", (aInputMod.getCocReefer().equals("")?null:new Long(aInputMod.getCocReefer())));
                    inParameters.put("p_coc_teu_mt", (aInputMod.getCocTeuMt().equals("")?null:new Long(aInputMod.getCocTeuMt())));
                    inParameters.put("p_coc_20mt", (aInputMod.getCocMt20().equals("")?null:new Long(aInputMod.getCocMt20())));
                    inParameters.put("p_coc_40mt", (aInputMod.getCocMt40().equals("")?null:new Long(aInputMod.getCocMt40())));
                    inParameters.put("p_soc_teu_laden", (aInputMod.getSocTeuFull().equals("")?null:new Long(aInputMod.getSocTeuFull())));
                    inParameters.put("p_soc_ton_laden", (aInputMod.getSocFullWeight().equals("")?null:new Double(aInputMod.getSocFullWeight())));
                    inParameters.put("p_soc_teu_mt", (aInputMod.getSocTeuMt().equals("")?null:new Long(aInputMod.getSocTeuMt())));
//                    inParameters.put("p_source_of_ts_figures", aInputMod.getTsSource());
//                    inParameters.put("p_excess_voyage", new Integer(aInputMod.getExceedVolume()));
                    inParameters.put("p_record_status", aInputMod.getRecordStatus());
                    inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                    inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                    if(aInputMod.getTolelant()==null||aInputMod.getTolelant().equals("")){
                        inParameters.put("p_tolerant","0.00");
                    }else{
                         inParameters.put("p_tolerant",new Double(roundTwoDecimals(aInputMod.getTolelant())));
                    }
                    //inParameters.put("p_tolerant", aInputMod.getTolelant().equals("")?null:new Long(aInputMod.getTolelant()));
                    // send ts proforma 17/04/2012
                    inParameters.put("P_TS_PROFORMA_NO", aInputMod.getProformaRefNo().toUpperCase());
                    
                    Map outParameters = execute(inParameters);
                    if (outParameters.size() > 0) {
                        isSuccess = true;                        
                        aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                        aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                        aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                        aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                        aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                    }
                }
                return isSuccess;
            }
        }
     
    protected class UpdateStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_BSA_ROUTE.PRR_UPD_BSA_ROUTE_TS";
         
            protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);           
                declareParameter(new SqlInOutParameter("p_pk_bsa_route_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_fk_pol_bsa_port_call_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_fk_pod_bsa_port_call_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_ts_service", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_variant", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_indicator", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_port", Types.VARCHAR));               
                declareParameter(new SqlInOutParameter("p_ts_group", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_coc_teu_laden", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_ton_laden", Types.DECIMAL));
                declareParameter(new SqlInOutParameter("p_coc_20gp", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_40gp", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_45hc", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_reefer_plugs", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_teu_mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_20mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_coc_40mt", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_soc_teu_laden", Types.NUMERIC));
                declareParameter(new SqlInOutParameter("p_soc_ton_laden", Types.DECIMAL));
                declareParameter(new SqlInOutParameter("p_soc_teu_mt", Types.NUMERIC));
//                declareParameter(new SqlInOutParameter("p_source_of_ts_figures", Types.CHAR));                                                
                declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
                declareParameter(new SqlInOutParameter("p_tolerant", Types.NUMERIC));
                compile();
            }
         
            protected boolean update(RrcStandardMod mod) {
                return update(mod,mod);
            }
         
            protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
                boolean isSuccess = false;                
                if((inputMod instanceof BsaBsaServiceVariantVolumePortPairMod)&&(outputMod instanceof BsaBsaServiceVariantVolumePortPairMod)){
                    BsaBsaServiceVariantVolumePortPairMod aInputMod = (BsaBsaServiceVariantVolumePortPairMod)inputMod;
                    BsaBsaServiceVariantVolumePortPairMod aOutputMod = (BsaBsaServiceVariantVolumePortPairMod)outputMod;
                    Map inParameters = new HashMap(25);
    ////
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_pk_bsa_route_id:"+aInputMod.getBsaRouteId());
//                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_bsa_model_id:"+aInputMod.getBsaModelId());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_fk_pol_bsa_port_call_id:"+aInputMod.getPolPortCallId());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_fk_pod_bsa_port_call_id:"+aInputMod.getBsaModelId());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_ts_service:"+aInputMod.getTsService().toUpperCase());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_ts_variant:"+aInputMod.getTsVariant().toUpperCase());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_ts_indicator:"+aInputMod.getTsIndic());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_ts_port:"+aInputMod.getTsPort().toUpperCase());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_ts_group:"+aInputMod.getTsGroup().toUpperCase());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_teu_laden:"+aInputMod.getCocTeuFull());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_ton_laden:"+aInputMod.getCocFullWeight());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_20gp:"+aInputMod.getCoc20Gp());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_40gp:"+aInputMod.getCoc40Gp());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_45hc:"+aInputMod.getCoc45Hc());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_reefer_plugs:"+aInputMod.getCocReefer());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_teu_mt:"+aInputMod.getCocTeuMt());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_20mt:"+aInputMod.getCocMt20());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_40mt:"+aInputMod.getCocMt40());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_soc_teu_laden:"+aInputMod.getSocTeuFull());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_soc_ton_laden:"+aInputMod.getSocFullWeight());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_soc_teu_mt:"+aInputMod.getSocTeuMt());
                    
//                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_port_grp_soc_coc:"+aInputMod.getPortGroupSocCoc());
//                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_service:"+aInputMod.getService());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_source_of_ts_figures:"+aInputMod.getTsSource());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_record_change_date:"+aInputMod.getRecordChangeDate());
                    System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_toloelant:"+aInputMod.getTolelant());
    ////            
                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//                    RutString.debugObject(aInputMod, BsaBsaServiceVariantVolumePortPairMod.class);
                    
                    inParameters.put("p_pk_bsa_route_id", (aInputMod.getBsaRouteId().equals(""))?null:new Integer(aInputMod.getBsaRouteId()));
                    inParameters.put("p_fk_pol_bsa_port_call_id", (aInputMod.getPolPortCallId().equals(""))?null:new Integer(aInputMod.getPolPortCallId()));
                    inParameters.put("p_fk_pod_bsa_port_call_id", (aInputMod.getPodPortCallId().equals(""))?null:new Integer(aInputMod.getPodPortCallId()));
//                    inParameters.put("p_fk_prev_pol_bsa_port_call_id", aInputMod.get);
//                    inParameters.put("p_fk_prev_bsa_route_id", aInputMod.);
//                    inParameters.put("p_fk_next_pod_bsa_port_call_id", aInputMod.);
//                    inParameters.put("p_fk_next_bsa_route_id", aInputMod.);
                    inParameters.put("p_ts_service", aInputMod.getTsService().toUpperCase());
                    inParameters.put("p_ts_variant", aInputMod.getTsVariant().toUpperCase());
                    inParameters.put("p_ts_indicator", aInputMod.getTsIndic());
                    inParameters.put("p_ts_port", aInputMod.getTsPort().toUpperCase());
                    inParameters.put("p_ts_group", aInputMod.getTsGroup().toUpperCase());
                    inParameters.put("p_coc_teu_laden", (aInputMod.getCocTeuFull().equals("")?null:new Long(aInputMod.getCocTeuFull())));
                    inParameters.put("p_coc_ton_laden", (aInputMod.getCocFullWeight().equals("")?null:new Double(aInputMod.getCocFullWeight())));
                    inParameters.put("p_coc_20gp", (aInputMod.getCoc20Gp().equals("")?null:new Long(aInputMod.getCoc20Gp())));
                    inParameters.put("p_coc_40gp", (aInputMod.getCoc40Gp().equals("")?null:new Long(aInputMod.getCoc40Gp())));
                    inParameters.put("p_coc_45hc", (aInputMod.getCoc45Hc().equals("")?null:new Long(aInputMod.getCoc45Hc())));
                    inParameters.put("p_coc_reefer_plugs", (aInputMod.getCocReefer().equals("")?null:new Long(aInputMod.getCocReefer())));
                    inParameters.put("p_coc_teu_mt", (aInputMod.getCocTeuMt().equals("")?null:new Long(aInputMod.getCocTeuMt())));
                    inParameters.put("p_coc_20mt", (aInputMod.getCocMt20().equals("")?null:new Long(aInputMod.getCocMt20())));
                    inParameters.put("p_coc_40mt", (aInputMod.getCocMt40().equals("")?null:new Long(aInputMod.getCocMt40())));
                    inParameters.put("p_soc_teu_laden", (aInputMod.getSocTeuFull().equals("")?null:new Long(aInputMod.getSocTeuFull())));
                    inParameters.put("p_soc_ton_laden", (aInputMod.getSocFullWeight().equals("")?null:new Double(aInputMod.getSocFullWeight())));
                    inParameters.put("p_soc_teu_mt", (aInputMod.getSocTeuMt().equals("")?null:new Long(aInputMod.getSocTeuMt())));                                                            
//                    inParameters.put("p_source_of_ts_figures", new Character(aInputMod.getTsSource().charAt(0)));
//                    inParameters.put("p_excess_voyage", new Integer(aInputMod.getExceedVolume()));
                    inParameters.put("p_record_status", aInputMod.getRecordStatus());
//                    inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
//                    inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                    String test = aInputMod.getTolelant();
                    if(aInputMod.getTolelant()==null||aInputMod.getTolelant().equals("")){
                        aInputMod.setTolelant("0.00");            
                    }else{
                        aInputMod.setTolelant(String.valueOf(roundTwoDecimals(aInputMod.getTolelant())));
                    }
                    inParameters.put("p_tolerant",new Double(roundTwoDecimals(aInputMod.getTolelant())));
                    //inParameters.put("p_tolerant", aInputMod.getTolelant().equals("")?null:new Long(aInputMod.getTolelant()));
                    Map outParameters = execute(inParameters);
                    if (outParameters.size() > 0) {
                        isSuccess = true;                    
//                        aOutputMod.setBsaRouteId((RutString.nullToStr(((Integer)outParameters.get("p_pk_bsa_route_id")).toString())));
//                        aOutputMod.setPolPortCallId((RutString.nullToStr(((Integer)outParameters.get("p_fk_pol_bsa_port_call_id")).toString())));
//                        aOutputMod.setPodPortCallId((RutString.nullToStr(((Integer)outParameters.get("p_fk_pod_bsa_port_call_id")).toString())));
                        aOutputMod.setTsService((RutString.nullToStr((String)outParameters.get("p_ts_service"))));
                        aOutputMod.setTsVariant((RutString.nullToStr((String)outParameters.get("p_ts_variant"))));
                        aOutputMod.setTsIndic((RutString.nullToStr((String)outParameters.get("p_ts_indicator"))));
                        aOutputMod.setTsPort((RutString.nullToStr((String)outParameters.get("p_ts_port"))));
                        aOutputMod.setTsGroup((RutString.nullToStr((String)outParameters.get("p_ts_group"))));                        
//                        aOutputMod.setCocTeuFull((((Long)outParameters.get("p_coc_teu_laden")).longValue()));
//                        aOutputMod.setCocFullWeight((((Long)outParameters.get("p_coc_ton_laden")).longValue()));
//                        aOutputMod.setCoc20Gp((((Long)outParameters.get("p_coc_20gp")).longValue()));
//                        aOutputMod.setCoc40Gp((((Long)outParameters.get("p_coc_40gp")).longValue()));
//                        aOutputMod.setCoc45Hc((((Long)outParameters.get("p_coc_45hc")).longValue()));
//                        aOutputMod.setCocReefer((((Long)outParameters.get("p_coc_reefer_plugs")).longValue()));
//                        aOutputMod.setCocTeuMt((((Long)outParameters.get("p_coc_teu_mt")).longValue()));
//                        aOutputMod.setCocMt20((((Long)outParameters.get("p_coc_20mt")).longValue()));
//                        aOutputMod.setCocMt40((((Long)outParameters.get("p_coc_40mt")).longValue()));
//                        aOutputMod.setSocTeuFull((((Long)outParameters.get("p_soc_teu_laden")).longValue()));
//                        aOutputMod.setSocFullWeight((((Long)outParameters.get("p_soc_ton_laden")).longValue()));
//                        aOutputMod.setSocTeuMt((((Long)outParameters.get("p_soc_teu_mt")).longValue()));                        
//                        aOutputMod.setTsSource((RutString.nullToStr((String)outParameters.get("p_source_of_ts_figures"))));
//                        aOutputMod.setExceedVolume((((Integer)outParameters.get("p_excess_voyage")).intValue()));
//                        aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                        aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                        aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                        
                    } 
                }
                return isSuccess;
            }
        }
        
    protected class DeleteStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_BSA_ROUTE.PRR_DEL_BSA_ROUTE_TS";
         
            protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                declareParameter(new SqlParameter("p_pk_bsa_route_id", Types.INTEGER));
                declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
                compile();
            }
         
            protected boolean delete(final RrcStandardMod inputMod) {
                boolean isSuccess = false;
                if(inputMod instanceof BsaBsaServiceVariantVolumePortPairMod){
                    BsaBsaServiceVariantVolumePortPairMod aInputMod = (BsaBsaServiceVariantVolumePortPairMod)inputMod;
                    Map inParameters = new HashMap(3);
    ////
                    System.out.println("[BsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_bsa_port_group_id:"+aInputMod.getBsaRouteId());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[BsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////
                    inParameters.put("p_pk_bsa_route_id", new Integer((RutString.nullToStr(aInputMod.getBsaRouteId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaRouteId())));
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                    execute(inParameters);
                    isSuccess = true;
                }
                return isSuccess;
            }
        }
        
    protected class CheckConstraintStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_ROUTE.PRR_CHK_CONSTRAINT_RECORDS";
     
        protected CheckConstraintStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);            
            declareParameter(new SqlInOutParameter("p_bsa_sv_id", Types.INTEGER));            
            compile();
        }

        protected void checkConstraint(RrcStandardMod mod) {
            checkConstraint(mod,mod);
        }
        
        protected void checkConstraint(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            if((inputMod instanceof BsaBsaServiceVariantVolumePortPairMod)||(outputMod instanceof BsaBsaServiceVariantVolumePortPairMod)){
                BsaBsaServiceVariantVolumePortPairMod aInputMod = (BsaBsaServiceVariantVolumePortPairMod)inputMod;
                Map inParameters = new HashMap(1);
    ////
                System.out.println("[BsaBsaServiceVariantVolumePortPairJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_bsa_sv_id:"+aInputMod.getBsaServiceVariantId());
    ////
                inParameters.put("p_bsa_sv_id", aInputMod.getBsaServiceVariantId());
                execute(inParameters);
            }
        }
    }
         protected double roundTwoDecimals(String number) {
                double result = Double.parseDouble(number);
                DecimalFormat twoDForm = new DecimalFormat("#.##");
            return  Double.parseDouble(twoDForm.format(result).toString());
         }
}

