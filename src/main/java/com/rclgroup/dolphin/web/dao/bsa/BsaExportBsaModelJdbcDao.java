/*-----------------------------------------------------------------------------------------------------------  
EzlExcelUploadOnlineJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Nipun Sutes 28/11/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bsa.BsaExportBsaModelMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class BsaExportBsaModelJdbcDao extends RrcStandardDao implements BsaExportBsaModelDao {
//    private UpdateLoadListStoreProcedure updateLoadListStoreProcedure;
       
    public BsaExportBsaModelJdbcDao() {
        super();
    }
   
    protected void initDao() throws Exception {
        super.initDao();
//        updateLoadListStoreProcedure = new UpdateLoadListStoreProcedure(getJdbcTemplate());
    }
    
 public BsaExportBsaModelMod findByKeyBsaModelID(String bsaModelId) throws DataAccessException{
     BsaExportBsaModelMod bsaExportBsaModelMod = null;
     StringBuffer sql = new StringBuffer();
     sql.append("select VR.BSA_SERVICE_VARIANT_ID ");
     sql.append("      ,VR.BSA_MODEL_ID  ");
     sql.append("      ,VR.SERVICE ");
     sql.append("      ,BSV.VARIANT_CODE  ");
     sql.append("      ,VR.PROFORMA_REF_NO ");
     sql.append("      ,BSV.BSA_VESSEL_TYPE ");
     sql.append("      ,VR.NO_VESSELS ");
     sql.append("      ,VR.ROTATION_DURATION  ");
     sql.append("      ,BSV.USAGE_RULE ");
     sql.append("      ,BSV.CALC_FREQUENCY ");
     sql.append("      ,BSV.CALC_DURATION ");
     sql.append("      ,BSV.DEF_SLOT_TEU  ");
     sql.append("      ,BSV.DEF_SLOT_TONS ");
     sql.append("      ,BSV.DEF_SLOT_REEFER_PLUGS ");
     sql.append("      ,BSV.DEF_AVG_COC_TEU_WEIGHT ");
     sql.append("      ,BSV.DEF_AVG_SOC_TEU_WEIGHT ");
     sql.append("      ,BSV.DEF_MIN_TEU  ");
     sql.append("      ,VR.OVERBOOKING_PERC ");
     sql.append("      ,BSV.RECORD_STATUS ");
     sql.append("      ,BSV.RECORD_ADD_USER ");
     sql.append("      ,BSV.RECORD_ADD_DATE ");
     sql.append("      ,BSV.RECORD_CHANGE_USER ");
     sql.append("      ,BSV.RECORD_CHANGE_DATE ");
     sql.append("from BSA_SERVICE_VARIANT BSV,VR_BSA_SERVICE_VARIANT VR ");
     sql.append("WHERE VR.BSA_SERVICE_VARIANT_ID=BSV.PK_BSA_SERVICE_VARIANT_ID ");
     sql.append("      AND VR.BSA_MODEL_ID=BSV.FK_BSA_MODEL_ID");
     sql.append("      AND VR.BSA_MODEL_ID = :bsaModelId ");
     sql.append("ORDER BY BSA_SERVICE_VARIANT_ID ");
     try{
         bsaExportBsaModelMod = (BsaExportBsaModelMod)getNamedParameterJdbcTemplate().queryForObject(
                sql.toString(),
                Collections.singletonMap("bsaModelId", bsaModelId),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                         BsaExportBsaModelMod bsaExportBsaModelMod = new BsaExportBsaModelMod();
//                         bsaExportBsaModelMod.setBsaServiceVariantId(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
//                         bsaExportBsaModelMod.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
//                         bsaExportBsaModelMod.setService(RutString.nullToStr(rs.getString("SERVICE")));
//                         bsaExportBsaModelMod.setVariantCode(RutString.nullToStr(rs.getString("VARIANT_CODE")));
//                         bsaExportBsaModelMod.setProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
//                         bsaExportBsaModelMod.setBsaVesselType(RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE")));
//                         bsaExportBsaModelMod.setNoVessels(RutString.nullToStr(rs.getString("NO_VESSELS")));
//                         bsaExportBsaModelMod.setRotationDuration(RutString.nullToStr(rs.getString("ROTATION_DURATION")));
//                         bsaExportBsaModelMod.setUsageRule(RutString.nullToStr(rs.getString("USAGE_RULE")));
//                         bsaExportBsaModelMod.setCalcFrequency(RutString.nullToStr(rs.getString("CALC_FREQUENCY")));
//                         bsaExportBsaModelMod.setCalcDuration(RutString.nullToStr(rs.getString("CALC_DURATION")));
//                         bsaExportBsaModelMod.setDefSlotTeu(RutString.nullToStr(rs.getString("DEF_SLOT_TEU")));
//                         bsaExportBsaModelMod.setDefSlotTons(RutString.nullToStr(rs.getString("DEF_SLOT_TONS")));
//                         bsaExportBsaModelMod.setDefSlotReefer(RutString.nullToStr(rs.getString("DEF_SLOT_REEFER_PLUGS")));
//                         bsaExportBsaModelMod.setDefAvgCocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_COC_TEU_WEIGHT")));
//                         bsaExportBsaModelMod.setDefAvgSocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_SOC_TEU_WEIGHT")));
//                         bsaExportBsaModelMod.setDefMinTeu(RutString.nullToStr(rs.getString("DEF_MIN_TEU")));
//                         bsaExportBsaModelMod.setOverbookingPerc(RutString.nullToStr(rs.getString("OVERBOOKING_PERC")));
                         bsaExportBsaModelMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                         bsaExportBsaModelMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
//                         bsaExportBsaModelMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                         bsaExportBsaModelMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
//                         bsaExportBsaModelMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                         return bsaExportBsaModelMod;
                    }
                });
     }catch (EmptyResultDataAccessException e) {
         bsaExportBsaModelMod = null;
     }
     return bsaExportBsaModelMod;
 }

   
}
