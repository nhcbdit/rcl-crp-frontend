/*------------------------------------------------------
QtnPortSurchargeJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 10/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
01 18/10/07 MNW              Added listForHelpScreen(), moveDbToModelForHelp()
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.qtn.QtnPortSurchargeMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class QtnPortSurchargeJdbcDao extends RrcStandardDao implements QtnPortSurchargeDao {
    
    public QtnPortSurchargeJdbcDao() {
    }    
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String surchargeCode) throws DataAccessException {
        System.out.println("[QtnPortSurchargeJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_CODE ");
        sql.append("FROM VR_QTN_SURCHARGE ");
        sql.append("WHERE SURCHARGE_CODE = :surchargeCode ");
        HashMap map = new HashMap();
        map.put("surchargeCode",surchargeCode);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[QtnPortSurchargeJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValid(String surchargeCode, String status) throws DataAccessException {
        System.out.println("[QtnPortSurchargeJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_CODE ");
        sql.append("FROM VR_QTN_SURCHARGE ");
        sql.append("WHERE SURCHARGE_CODE = :surchargeCode ");
        sql.append("AND SURCHARGE_CODE_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("surchargeCode",surchargeCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[QtnPortSurchargeJdbcDao][isValid]: With status: Finished");
        return isValid;
    }    
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[QtnPortSurchargeJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_CODE ");
        sql.append("      ,SHORT_DESCRIPTION ");    
        sql.append("      ,LONG_DESCRIPTION ");  
        sql.append("      ,SURCHARGE_TYPE_CODE ");             
        sql.append("      ,SURCHARGE_CODE_STATUS "); 
        sql.append("FROM VR_QTN_SURCHARGE ");
        sql.append(sqlCriteria);
        System.out.println("[EmsAgreementJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[QtnPortSurchargeJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());   
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "WHERE SURCHARGE_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "WHERE LONG_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE SURCHARGE_CODE_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[QtnPortSurchargeJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_CODE ");
        sql.append("      ,SHORT_DESCRIPTION ");    
        sql.append("      ,LONG_DESCRIPTION ");  
        sql.append("      ,SURCHARGE_TYPE_CODE ");             
        sql.append("      ,SURCHARGE_CODE_STATUS "); 
        sql.append("FROM VR_QTN_SURCHARGE ");
        sql.append("WHERE SURCHARGE_CODE_STATUS = :status ");
        sql.append(sqlCriteria);
        System.out.println("[QtnPortSurchargeJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
        HashMap map = new HashMap();
        map.put("status",status);
        System.out.println("[QtnPortSurchargeJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());  
    }
    
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND SURCHARGE_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND LONG_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND SURCHARGE_CODE_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }   
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    }  
    
    private QtnPortSurchargeMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        QtnPortSurchargeMod portSurchargeCode = new QtnPortSurchargeMod();
        portSurchargeCode.setSurchargeCode(RutString.nullToStr(rs.getString("SURCHARGE_CODE")));   
        portSurchargeCode.setShortDescription(RutString.nullToStr(rs.getString("SHORT_DESCRIPTION")));     
        portSurchargeCode.setLongDescription(RutString.nullToStr(rs.getString("LONG_DESCRIPTION")));  
        portSurchargeCode.setSurchargeTypeCode(RutString.nullToStr(rs.getString("SURCHARGE_TYPE_CODE")));         
        portSurchargeCode.setSurchargeCodeStatus(RutString.nullToStr(rs.getString("SURCHARGE_CODE_STATUS")));

        return portSurchargeCode;
    }
}
