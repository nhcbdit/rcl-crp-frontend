/*-----------------------------------------------------------------------------------------------------------  
EmsGeographicalRegionJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 29/04/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/


package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsGeographicalRegionMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsGeographicalRegionJdbcDao extends RrcStandardDao implements EmsGeographicalRegionDao {
     
     public EmsGeographicalRegionJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String region) throws DataAccessException {
         System.out.println("[EmsGeographicalRegionJdbcDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT REGION_CODE ");
         sql.append("FROM VR_EMS_GEO_REGION ");
         sql.append("WHERE REGION_CODE = :region ");
         HashMap map = new HashMap();
         map.put("region",region);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }         
         System.out.println("[EmsGeographicalRegionJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValid(String region, String status) throws DataAccessException {
         System.out.println("[EmsGeographicalRegionJdbcDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT REGION_CODE ");
         sql.append("FROM VR_EMS_GEO_REGION ");
         sql.append("WHERE REGION_CODE = :region ");
         sql.append("AND REGION_STATUS = :status ");
         HashMap map = new HashMap();
         map.put("region",region);
         map.put("status",status);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }    
         System.out.println("[EmsGeographicalRegionJdbcDao][isValid]: With status: Finished");
         return isValid;
     }        
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[EmsGeographicalRegionJdbcDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT REGION_CODE ");
         sql.append("      ,REGION_DESCRIPTION ");
         sql.append("      ,REGION_STATUS ");
         sql.append("FROM VR_EMS_GEO_REGION ");
         sql.append(sqlCriteria);
         System.out.println("[EmsGeographicalRegionJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
         System.out.println("[EmsGeographicalRegionJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowModMapper()); 
    }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "WHERE REGION_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "WHERE REGION_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "WHERE REGION_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }     
     
     public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
         System.out.println("[EmsGeographicalRegionJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT REGION_CODE ");
         sql.append("      ,REGION_DESCRIPTION ");
         sql.append("      ,REGION_STATUS ");
         sql.append("FROM VR_EMS_GEO_REGION ");
         sql.append("WHERE REGION_STATUS = :status ");
         sql.append(sqlCriteria);
         System.out.println("[EmsGeographicalRegionJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         HashMap map = new HashMap();
         map.put("status",status);
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());     
     }
     
     private String createSqlCriteriaWithStatus(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND REGION_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "AND REGION_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND REGION_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }      
     
     protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModelForHelp(rs);
         }
     } 
     
     private EmsGeographicalRegionMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
         EmsGeographicalRegionMod region = new EmsGeographicalRegionMod();
         region.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));   
         region.setRegionName(RutString.nullToStr(rs.getString("REGION_DESCRIPTION")));            
         region.setRegionStatus(RutString.nullToStr(rs.getString("REGION_STATUS")));

         return region;
    }     
}
