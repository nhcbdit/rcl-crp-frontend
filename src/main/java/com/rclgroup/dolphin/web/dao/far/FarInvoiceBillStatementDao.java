/*--------------------------------------------------------
FarInvoiceBillStatementDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Porntip Aramrattana 17/06/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.far;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface FarInvoiceBillStatementDao {
    
    /**
     * number of list billing statement by header records for search screen
     * @param billDateFrom 
     * @param billDateTo
     * @param billNumFrom
     * @param billNumTo
     * @param description
     * @param line
     * @param region
     * @param agent
     * @param fsc
     * @param status
     * @return list of billing statement model(invoiceBillStatementMod)
     * @throws DataAccessException
     */
    public int countListForStatementByHeader(String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String description,String line,String region,String agent,String fsc,String status) throws DataAccessException;
    
    /**
     * list billing statement by header records for search screen
     * @param billDateFrom 
     * @param billDateTo
     * @param billNumFrom
     * @param billNumTo
     * @param description
     * @param line
     * @param region
     * @param agent
     * @param fsc
     * @param status
     * @return list of billing statement model(invoiceBillStatementMod)
     * @throws DataAccessException
     */
    public List listForStatementByHeader(String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String description,String line,String region,String agent,String fsc,String status) throws DataAccessException;
    
    /**
     * list billing statement by header records for search screen
     * @param rowAt
     * @param rowTo
     * @param billDateFrom 
     * @param billDateTo
     * @param billNumFrom
     * @param billNumTo
     * @param description
     * @param line
     * @param region
     * @param agent
     * @param fsc
     * @param status
     * @return list of billing statement model(invoiceBillStatementMod)
     * @throws DataAccessException
     */
    public List listForStatementByHeader(int rowAt, int rowTo, String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String description,String line,String region,String agent,String fsc,String status) throws DataAccessException;
    
    /**
     * list billing statement by header records for search screen
     * @param billDateFrom 
     * @param billDateTo
     * @param billNumFrom
     * @param billNumTo
     * @param billToParty
     * @param line
     * @param region
     * @param agent
     * @param fsc
     * @param status
     * @return list of billing statement model(invoiceBillStatementMod)
     * @throws DataAccessException
     */
    public List listForStatementByHeaderWithLinkage(String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String billToParty,String line,String region,String agent ,String fsc ,String status) throws DataAccessException;
    
    /**
     * number of list billing statement records for search screen (Details)
     * @param billToPartyName
     * @param dateType
     * @param fromDate
     * @param toDate
     * @param service
     * @param vessel
     * @param voyage
     * @param port
     * @param shipmentDirection
     * @param blNo
     * @param bkgNo
     * @param invoiceNo
     * @param invoiceStatus
     * @param sortBy
     * @param sortByIn
     * @param fsc
     * @param status
     * @return list of billing statement
     * @throws DataAccessException
     */
    public int countListForStatementByDetail(String billToPartyName, String dateType, String fromDate, String toDate, String service, String vessel, String voyage, String port, String shipmentDirection, String blNo, String bkgNo, String invoiceNo, String invoiceStatus, String sortBy, String sortByIn, String line, String region, String agent, String fsc, String status) throws DataAccessException;
    
    /**
     * list billing statement records for search screen (Details)
     * @param billToPartyName
     * @param dateType
     * @param fromDate
     * @param toDate
     * @param service
     * @param vessel
     * @param voyage
     * @param port
     * @param shipmentDirection
     * @param blNo
     * @param bkgNo
     * @param invoiceNo
     * @param invoiceStatus
     * @param sortBy
     * @param sortByIn
     * @param fsc
     * @param status
     * @return list of billing statement
     * @throws DataAccessException
     */
    public List listForStatementByDetail(String billToPartyName, String dateType, String fromDate, String toDate, String service, String vessel, String voyage, String port, String shipmentDirection, String blNo, String bkgNo, String invoiceNo, String invoiceStatus, String sortBy, String sortByIn, String line, String region, String agent, String fsc, String status) throws DataAccessException;
    
    /**
     * list billing statement records for search screen (Details)
     * @param rowAt
     * @param rowTo
     * @param billToPartyName
     * @param dateType
     * @param fromDate
     * @param toDate
     * @param service
     * @param vessel
     * @param voyage
     * @param port
     * @param shipmentDirection
     * @param blNo
     * @param bkgNo
     * @param invoiceNo
     * @param invoiceStatus
     * @param sortBy
     * @param sortByIn
     * @param fsc
     * @param status
     * @return list of billing statement
     * @throws DataAccessException
     */
    public List listForStatementByDetail(int rowAt, int rowTo, String billToPartyName, String dateType, String fromDate, String toDate, String service, String vessel, String voyage, String port, String shipmentDirection, String blNo, String bkgNo, String invoiceNo, String invoiceStatus, String sortBy, String sortByIn, String line, String region, String agent, String fsc, String status) throws DataAccessException;

    /**
     * update a billing statement record 
     * @param billNo
     * @param version
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: BILL_NUMBER_REQ
     *                              error message: BILL_VERSION_REQ
     *                              error message: BILL_NUMBER_NOT_FOUND
     *                              error message: VERSION_NOT_FOUND
     *                              error message: BILL_NNUMBER_NOT_FOUND OR VERSION_NOT_FOUND
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public void updateStatement (String billNo,String version) throws DataAccessException;
    
    /**
     * find the template code by bill no, bill version
     * @param billNo
     * @param billVersion
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: FAR_FAR01_TEMPLATE_NOT_FOUND
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public String findTemplateCodeByBill(String billNo, int billVersion) throws DataAccessException;
}
