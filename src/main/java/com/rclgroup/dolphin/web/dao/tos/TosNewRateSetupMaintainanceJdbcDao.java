package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.tos.TosMidStreamMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupActivityMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupCategoryMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupMaintainanceMod;
import com.rclgroup.dolphin.web.model.tos.TosOperationTypeMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class TosNewRateSetupMaintainanceJdbcDao extends RrcStandardDao implements TosNewRateSetupMaintainanceDao{
    private GetRateDetailListProcedure getRateDetailListProcedure;
    private GetCategoryDetailListProcedure getCategoryDetailListProcedure;
    private GetActivityListProcedure  getActivityListProcedure;
    
    public void initDao() throws Exception {
        super.initDao();        
        getRateDetailListProcedure = new GetRateDetailListProcedure(getJdbcTemplate(), new TosRateDtlListMapper());
        getCategoryDetailListProcedure = new GetCategoryDetailListProcedure(getJdbcTemplate(), new TosCatgDtlListMapper());
        getActivityListProcedure = new GetActivityListProcedure(getJdbcTemplate(), new TosActivityListMapper());
    }
    
    public TosNewRateSetupMaintainanceJdbcDao() {
    }
    
    public List getRateDtlList(String rateSeq, String port, String terminal, String operation) throws DataAccessException{
        StringBuffer sb = new StringBuffer();
        StringBuffer errorMsgBuffer = new StringBuffer();
        
//        sb.append("SELECT TOS_RATE_DTL.LINE,  ");
//        sb.append("TOS_RATE_DTL.PORT,  ");
//        sb.append("TOS_RATE_DTL.TERMINAL,  ");
//        sb.append("TOS_RATE_DTL.SERVICE,  ");
//        sb.append("TOS_RATE_DTL.OPR_CODE,  ");
//        sb.append("TOS_RATE_DTL.VENDOR_CODE,  ");
//        sb.append("TOS_RATE_DTL.VENDOR_SEQ,  ");
//        sb.append("TOS_RATE_DTL.ACTIVITY_CODE,  ");
//        sb.append("V_GENERAL_ACTIVITY.PARENT_ACTIVITY_CODE,  ");
//        sb.append("V_GENERAL_ACTIVITY.DESCRIPTION,  ");
//        sb.append("TOS_RATE_DTL.OCEAN_COAST,  ");
//        sb.append("V_GENERAL_ACTIVITY.CHARGE_CODE,  ");
//        sb.append("TOS_RATE_DTL.POD_OCEAN_COAST,  ");
//        sb.append("TOS_RATE_DTL.ACTIVITY_RATE,  ");
//        sb.append("V_GENERAL_ACTIVITY.RATE_BASIS,  ");
//        sb.append("TOS_RATE_DTL.COL1_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL2_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL3_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL4_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL5_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL6_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL7_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL8_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL9_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL10_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL11_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL12_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL13_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL14_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL15_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL16_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL17_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL18_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL19_RATE,  ");
//        sb.append("TOS_RATE_DTL.COL20_RATE,  ");
//        sb.append("TOS_RATE_DTL.RECORD_STATUS,  ");
//        sb.append("TOS_RATE_DTL.TOS_RATE_SEQNO, ");         
//        sb.append("V_GENERAL_ACTIVITY.INCLUDE_YN,  ");
//        sb.append("V_GENERAL_ACTIVITY.CATG_BASED_YN,  ");
//        sb.append("V_GENERAL_ACTIVITY.AUTO_YN, ");
//        sb.append("TOS_RATE_DTL.COC_SOC, ");
//        sb.append("TOS_RATE_DTL.SHIP_TERM, ");
//        sb.append("TOS_RATE_DTL.CRANE_TYPE,  ");
//        sb.append("TOS_RATE_DTL.VESSEL_TYPE,  "); 
//        sb.append("TOS_RATE_DTL.OPERATION_TYPE,  ");
//        sb.append("TOS_RATE_DTL.TOS_RATE_SEQ_DTL,  ");
//        sb.append("TOS_RATE_DTL.LOAD_DISCH_TRAN_FLAG, ");
//        sb.append("TOS_RATE_DTL.AUTO_ACTIVITY, ");
//        sb.append("TOS_RATE_DTL.DESTINATION_TYPE, ");
//        sb.append("TOS_RATE_DTL.DESTINATION_CODE, ");
//        sb.append("TOS_RATE_DTL.MOT, ");
//        sb.append("TOS_RATE_DTL.WEIGHT_MIN, ");
//        sb.append("TOS_RATE_DTL.WEIGHT_MAX, ");
//        sb.append("TOS_RATE_DTL.WEEKDAY_WEEKEND, ");
//        sb.append("TOS_RATE_DTL.HOLIDAY, ");
//        sb.append("TOS_RATE_DTL.DAY_NIGHT, ");
//        sb.append("TOS_RATE_DTL.REASON_CODE, ");
//        sb.append("TOS_RATE_DTL.DOM_INTER, ");
//        sb.append("TOS_RATE_DTL.SHIP_TYPE ");
//        sb.append("FROM SEALINER.TOS_RATE_DTL, SEALINER.V_GENERAL_ACTIVITY  ");
//        sb.append("WHERE TOS_RATE_DTL.PORT = V_GENERAL_ACTIVITY.PORT AND  ");
//        sb.append("  TOS_RATE_DTL.TERMINAL = V_GENERAL_ACTIVITY.TERMINAL AND  ");
//        sb.append("  TOS_RATE_DTL.OPR_CODE = V_GENERAL_ACTIVITY.OPR_CODE AND  ");
//        sb.append("  TOS_RATE_DTL.ACTIVITY_CODE = V_GENERAL_ACTIVITY.ACTIVITY_CODE AND  ");
//        sb.append("  TOS_RATE_DTL.PORT = '" + port + "' AND  ");
//        sb.append("  TOS_RATE_DTL.TERMINAL = '" + terminal + "' AND  ");
//        sb.append("  TOS_RATE_DTL.OPR_CODE = '" + operation + "' AND  ");
//        sb.append("  TOS_RATE_DTL.TOS_RATE_SEQNO = '" + rateSeq + "' AND  ");
//        sb.append("  (V_GENERAL_ACTIVITY.NON_VOY_FLAG <> 'Y' OR V_GENERAL_ACTIVITY.NON_VOY_FLAG is null)  ");
//        sb.append("ORDER BY V_GENERAL_ACTIVITY.EXPORT_IMPORT, V_GENERAL_ACTIVITY.ACTIVITY_CODE");
//        
//        System.out.println("[TosNewRateSetupMaintainanceJdbcDao][getRateDtlList] SQL:"+sb.toString());
//                
//        return getNamedParameterJdbcTemplate().query(
//                sb.toString(),
//                new HashMap(),
//                new RowMapper(){
//                    public Object mapRow(ResultSet rs, int row) throws SQLException {
//                        TosNewRateSetupMaintainanceMod bean = new TosNewRateSetupMaintainanceMod();
//                        
//                        bean.setLine(RutString.nullToStr(rs.getString("LINE")));  
//                        bean.setPort(RutString.nullToStr(rs.getString("PORT"))); 
//                        bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
//                        bean.setService(RutString.nullToStr(rs.getString("SERVICE")));  
//                        bean.setOpr_code(RutString.nullToStr(rs.getString("OPR_CODE")));  
//                        bean.setVendor_code(RutString.nullToStr(rs.getString("VENDOR_CODE")));  
//                        bean.setVendor_seq(RutString.nullToStr(rs.getString("VENDOR_SEQ")));  
//                        bean.setActivity_code(RutString.nullToStr(rs.getString("ACTIVITY_CODE")));
//                        bean.setParent_activity_code(RutString.nullToStr(rs.getString("PARENT_ACTIVITY_CODE")));
//                        bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));  
//                        bean.setOcean_coast(RutString.nullToStr(rs.getString("OCEAN_COAST")));  
//                        bean.setCharge_code(RutString.nullToStr(rs.getString("CHARGE_CODE")));  
//                        bean.setPod_ocean_coast(RutString.nullToStr(rs.getString("POD_OCEAN_COAST")));  
//                        if(RutString.nullToStr(rs.getString("ACTIVITY_RATE")).equals("")||RutString.nullToStr(rs.getString("ACTIVITY_RATE")).equals("0")){bean.setActivity_rate("0.00"); } else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setActivity_rate(dc.format((rs.getDouble("ACTIVITY_RATE"))));}                        
//                        bean.setRate_basis(RutString.nullToStr(rs.getString("RATE_BASIS")));  
//                        if(RutString.nullToStr(rs.getString("COL1_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol1_rate( dc.format(rs.getDouble("COL1_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL2_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol2_rate( dc.format(rs.getDouble("COL2_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL3_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol3_rate( dc.format(rs.getDouble("COL3_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL4_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol4_rate( dc.format(rs.getDouble("COL4_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL5_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol5_rate( dc.format(rs.getDouble("COL5_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL6_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol6_rate( dc.format(rs.getDouble("COL6_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL7_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol7_rate( dc.format(rs.getDouble("COL7_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL8_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol8_rate( dc.format(rs.getDouble("COL8_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL9_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol9_rate( dc.format(rs.getDouble("COL9_RATE") ));}
//                        if(RutString.nullToStr(rs.getString("COL10_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol10_rate(dc.format(rs.getDouble("COL10_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL11_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol11_rate(dc.format(rs.getDouble("COL11_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL12_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol12_rate(dc.format(rs.getDouble("COL12_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL13_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol13_rate(dc.format(rs.getDouble("COL13_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL14_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol14_rate(dc.format(rs.getDouble("COL14_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL15_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol15_rate(dc.format(rs.getDouble("COL15_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL16_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol16_rate(dc.format(rs.getDouble("COL16_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL17_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol17_rate(dc.format(rs.getDouble("COL17_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL18_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol18_rate(dc.format(rs.getDouble("COL18_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL19_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol19_rate(dc.format(rs.getDouble("COL19_RATE")));}
//                        if(RutString.nullToStr(rs.getString("COL20_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol20_rate(dc.format(rs.getDouble("COL20_RATE")));}                        
//                        bean.setRecord_status(RutString.nullToStr(rs.getString("RECORD_STATUS")));  
//                        bean.setTos_rate_seqno(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));          
//                        bean.setInclude_yn(RutString.nullToStr(rs.getString("INCLUDE_YN")));  
//                        bean.setCatg_based_yn(RutString.nullToStr(rs.getString("CATG_BASED_YN")));  
//                        bean.setAuto_yn(RutString.nullToStr(rs.getString("AUTO_YN"))); 
//                        bean.setCoc_soc(RutString.nullToStr(rs.getString("COC_SOC"))); 
//                        bean.setShip_term(RutString.nullToStr(rs.getString("SHIP_TERM")));
//                        bean.setCrane_type(RutString.nullToStr(rs.getString("CRANE_TYPE")));  
//                        bean.setVessel_type(RutString.nullToStr(rs.getString("VESSEL_TYPE")));   
//                        bean.setOperation_type(RutString.nullToStr(rs.getString("OPERATION_TYPE")));  
//                        bean.setTos_rate_seq_dtl(RutString.nullToStr(rs.getString("TOS_RATE_SEQ_DTL")));                        
//                        bean.setLoad_disch_tran_flag(RutString.nullToStr(rs.getString("Load_disch_tran_flag"))); 
//                        bean.setAuto_activity(RutString.nullToStr(rs.getString("Auto_activity"))); 
//                        bean.setDestination_type(RutString.nullToStr(rs.getString("Destination_type"))); 
//                        bean.setDestination_code(RutString.nullToStr(rs.getString("Destination_code"))); 
//                        bean.setMot(RutString.nullToStr(rs.getString("Mot"))); 
//                        if(RutString.nullToStr(rs.getString("Weight_min")).equals("") || RutString.nullToStr(rs.getString("Weight_min")).equals("0")){bean.setWeight_min("0.00");}else{DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setWeight_min(dc.format(rs.getDouble("Weight_min"))); }
//                        if(RutString.nullToStr(rs.getString("Weight_max")).equals("") || RutString.nullToStr(rs.getString("Weight_max")).equals("0")){bean.setWeight_max("0.00");}else{DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setWeight_max(dc.format(rs.getDouble("Weight_max"))); }                        
//                        bean.setWeekday_weekend(RutString.nullToStr(rs.getString("Weekday_weekend"))); 
//                        bean.setHoliday(RutString.nullToStr(rs.getString("Holiday"))); 
//                        bean.setDay_night(RutString.nullToStr(rs.getString("Day_night"))); 
//                        bean.setReason_code(RutString.nullToStr(rs.getString("Reason_code")));
//                        bean.setDom_inter(RutString.nullToStr(rs.getString("DOM_INTER")));
//                        bean.setShip_type(RutString.nullToStr(rs.getString("SHIP_TYPE")));
//                        bean.setDataStatus(bean.UPDATE);
//                        bean.setDelete(false);
//                        return bean;
//                    }
//                });        
    
        List resultList = new ArrayList();
        HashMap map = new HashMap();
        
        map.put("p_i_v_rate_seq",rateSeq);
        map.put("p_i_v_port", port);
        map.put("p_i_v_terminal", terminal);
        map.put("p_i_v_operation", operation);

        resultList = getRateDetailListProcedure.getRateList(map);
        
        return resultList;
    }
    
    public List getCategoryList(String port, String terminal, String operation) throws DataAccessException{
//        StringBuffer sb = new StringBuffer();
//        
//        sb.append("SELECT LINE, ");  
//        sb.append("PORT, ");  
//        sb.append("TERMINAL, ");
//        sb.append("OPR_CODE, ");  
//        sb.append("(CASE EMPTY_FULL ");
//        sb.append("   WHEN 'E' THEN 'EMPTY' ");
//        sb.append("   WHEN 'F' THEN 'FULL'  ");
//        sb.append(" END) ||' '|| ");
//        sb.append(" CAT_CODE ||' '|| ");
//        sb.append(" SIZE_FLAG ||' '|| ");
//        sb.append(" (CASE DECK_FLAG ");
//        sb.append("   WHEN 'C' THEN 'CNTR' ");
//        sb.append("   WHEN 'A' THEN 'AUTO' ");
//        sb.append("   WHEN 'R' THEN 'RORO' ");
//        sb.append(" END) "); 
//        sb.append("CATG_COL, ");                                         
//        sb.append("SEQ_NO, ");  
//        sb.append("DESCRIPTION, ");  
//        sb.append("RATE_BASIS, ");  
//        sb.append("CHARGE_CODE, ");  
//        sb.append("LINK_COLUMN, ");  
//        sb.append("TEU_FACTOR, ");  
//        sb.append("RECORD_STATUS ");  
//        sb.append("FROM SEALINER.V_GENERAL_CATEGORY ");
//        sb.append(" WHERE OPR_CODE = '"+operation+"' AND ");
//        sb.append("       PORT = '"+port+"' AND ");
//        sb.append("       TERMINAL = '"+terminal+"' ");
//        sb.append("ORDER BY SEQ_NO ");
//
//        System.out.println("[TosNewRateSetupMaintainanceJdbcDao][getCategoryList] SQL:"+sb.toString());
//        
//        return getNamedParameterJdbcTemplate().query(
//                sb.toString(),
//                new HashMap(),
//                new RowMapper(){
//                    public Object mapRow(ResultSet rs, int row) throws SQLException {
//                        TosNewRateSetupCategoryMod bean = new TosNewRateSetupCategoryMod();
//                        bean.setLine(RutString.nullToStr(rs.getString("LINE")));  
//                        bean.setPort(RutString.nullToStr(rs.getString("PORT")));  
//                        bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));  
//                        bean.setOpr_code(RutString.nullToStr(rs.getString("OPR_CODE"))); ; 
//                        bean.setCatg_col(RutString.nullToStr(rs.getString("CATG_COL")));
//                        bean.setSeq_no(RutString.nullToStr(rs.getString("SEQ_NO")));  
//                        bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));  
//                        bean.setRate_basis(RutString.nullToStr(rs.getString("RATE_BASIS")));  
//                        bean.setCharge_code(RutString.nullToStr(rs.getString("CHARGE_CODE")));  
//                        bean.setLink_column(RutString.nullToStr(rs.getString("LINK_COLUMN")));  
//                        bean.setTeu_factor(RutString.nullToStr(rs.getString("TEU_FACTOR")));  
//                        bean.setRecord_status(RutString.nullToStr(rs.getString("RECORD_STATUS")));
//                        return bean;
//                    }
//                });
        List resultList = new ArrayList();
        HashMap map = new HashMap();
        
        map.put("p_i_v_port", port);
        map.put("p_i_v_terminal", terminal);
        map.put("p_i_v_operation", operation);
        
        resultList = getCategoryDetailListProcedure.getCatgList(map);
        
        return resultList;
    }
    
    public List getActivityList(String port, String terminal, String operation) throws DataAccessException{
//        StringBuffer sb = new StringBuffer();
//        
//        sb.append("SELECT LINE, " );
//        sb.append("PORT, " );
//        sb.append("TERMINAL, " );
//        sb.append("OPR_CODE, " );
//        sb.append("ACTIVITY_CODE, " );
//        sb.append("SEQ_NO, " );
//        sb.append("SUB_SEQ_NO, " );
//        sb.append("PARENT_ACTIVITY_CODE, " );
//        sb.append("DESCRIPTION, " );
//        sb.append("RATE_BASIS, " );
//        sb.append("VOLUME_UNITS, " );
//        sb.append("VOLUME_MOVES, " );
//        sb.append("VOLUME_TONS, " );
//        sb.append("CHARGE_CODE, " );
//        sb.append("AUTO_YN, " );
//        sb.append("LOAD_DISCH_FLAG, " );
//        sb.append("INCLUDE_YN, " );
//        sb.append("RECORD_STATUS, " );
//        sb.append("DISCH_ACTIVITY, " );
//        sb.append("REEFER_YN, " );
//        sb.append("RECOVERY_YN, " );
//        sb.append("RECOVERY_CHG_CODE, " );
//        sb.append("CATG_BASED_YN " );
//        sb.append("FROM SEALINER.V_GENERAL_ACTIVITY ");
//        sb.append("WHERE (NON_VOY_FLAG <> 'Y' OR NON_VOY_FLAG is null) AND " );
//        sb.append("OPR_CODE = '" +operation + "' AND " );
//        sb.append("PORT = '" + port + "' AND " );
//        sb.append("TERMINAL = '" +terminal + "' " );
//        sb.append("ORDER BY EXPORT_IMPORT, ACTIVITY_CODE ");
//        System.out.println("[TosNewRateSetupMaintainanceJdbcDao][getActivityList] SQL:"+sb.toString());
//        return getNamedParameterJdbcTemplate().query(
//                sb.toString(),
//                new HashMap(),
//                new RowMapper(){
//                    public Object mapRow(ResultSet rs, int row) throws SQLException {
//                        TosNewRateSetupActivityMod bean = new TosNewRateSetupActivityMod();                        
//                        bean.setLine(RutString.nullToStr(rs.getString("line")));
//                        bean.setPort(RutString.nullToStr(rs.getString("port")));
//                        bean.setTerminal(RutString.nullToStr(rs.getString("terminal")));
//                        bean.setOpr_code(RutString.nullToStr(rs.getString("opr_code")));
//                        bean.setActivity_code(RutString.nullToStr(rs.getString("activity_code")));
//                        bean.setSeq_no(RutString.nullToStr(rs.getString("seq_no")));
//                        bean.setSub_seq_no(RutString.nullToStr(rs.getString("sub_seq_no")));
//                        bean.setParent_activity_code(RutString.nullToStr(rs.getString("parent_activity_code")));
//                        bean.setDescription(RutString.nullToStr(rs.getString("description")));
//                        bean.setRate_basis(RutString.nullToStr(rs.getString("rate_basis")));
//                        bean.setVolume_units(RutString.nullToStr(rs.getString("volume_units")));
//                        bean.setVolume_moves(RutString.nullToStr(rs.getString("volume_moves")));
//                        bean.setVolume_tons(RutString.nullToStr(rs.getString("volume_tons")));
//                        bean.setCharge_code(RutString.nullToStr(rs.getString("charge_code")));
//                        bean.setAuto_yn(RutString.nullToStr(rs.getString("auto_yn")));
//                        bean.setLoad_disch_flag(RutString.nullToStr(rs.getString("load_disch_flag")));
//                        bean.setInclude_yn(RutString.nullToStr(rs.getString("include_yn")));
//                        bean.setRecord_status(RutString.nullToStr(rs.getString("record_status")));
//                        bean.setDisch_activity(RutString.nullToStr(rs.getString("disch_activity")));
//                        bean.setReefer_yn(RutString.nullToStr(rs.getString("reefer_yn")));
//                        bean.setRecovery_yn(RutString.nullToStr(rs.getString("recovery_yn")));
//                        bean.setRecovery_chg_code(RutString.nullToStr(rs.getString("recovery_chg_code")));
//                        bean.setCatg_based_yn(RutString.nullToStr(rs.getString("catg_based_yn")));
//                        return bean;
//                    }
//                });    

         List resultList = new ArrayList();
         HashMap map = new HashMap();
         
         map.put("p_i_v_port", port);
         map.put("p_i_v_terminal", terminal);
         map.put("p_i_v_operation", operation);
         
         resultList = getActivityListProcedure.getActivityList(map);
         
         return resultList;
    }
    
    public int getRateSeqNo(String line, String port, String terminal, String rateRef, String oprType ) throws DataAccessException{
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        String sql = " SELECT COUNT(*) FROM SEALINER.TOS_RATE_HEADER "+
                     " WHERE LINE='"+line+"' " +
                     " AND PORT='"+port+"' " +
                     " AND TERMINAL='"+terminal+"' " +
                     " AND OPERATION_TYPE='"+oprType+"' " +
                     " AND TOS_RATE_REF='"+rateRef+"'";
                     
        int recCnt = 0;
        int rateSeqNo = 0;
        
        recCnt = (int) getNamedParameterJdbcTemplate().queryForObject(sql, new HashMap(), Integer.class);
        
        if(recCnt == 0){
            sql = "SELECT NVL(MAX(TOS_RATE_SEQNO),'0') + 1 FROM TOS_RATE_HEADER ";
            
            rateSeqNo = (int) getNamedParameterJdbcTemplate().queryForObject(sql, new HashMap(), Integer.class);    
        } 
        errorMsgBuffer = checkConstraint(port, terminal, oprType, rateRef);
        if(errorMsgBuffer.toString() != null && !errorMsgBuffer.toString().equals("")){
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        
        return rateSeqNo;
    }
    
    public int insertRateHdr(String line, String trade, String agent, String port, String terminal, String rateRef, String oprType, String effDate, String expDate, String currency, String description, String status, int rateSeqNo, String userId) throws DataAccessException{
        int insRec = 0;
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        String sql = " INSERT INTO TOS_RATE_HEADER (" +
			"LINE, " +
			"TRADE, " +
			"AGENT, " +
			"PORT, " +
			"TERMINAL, " +
			"TOS_RATE_SEQNO, " +
			"TOS_RATE_REF, " +
			"OPERATION_TYPE, " +
			"EFFECTIVE_DATE, " +
			"EXPIRY_DATE, " +
			"CURRENCY, "+
			"DESCR ," +
			"RATE_STATUS, " +
			"RECORD_ADD_USER , " +
			"RECORD_ADD_DATE )" +
			" VALUES " +
			"('" +line+"', " +
			" NVL('" + trade+ "','*'), " +
			" NVL('" + agent+ "','***'), " +
			"'" +port + "', " +
			"'" + terminal + "', " +
			"'" + rateSeqNo + "', " +
			"'" + rateRef + "', " +
			"'" + oprType + "', " +
			"TO_DATE('" +effDate+ "', 'DD/MM/YYYY'), " +
                        "TO_DATE('" +expDate+ "', 'DD/MM/YYYY'), " +
			"'" +currency + "', " +
			"'" +description + "', " +
			"'"+status+"' ,"+
			"'"+ userId+"'," 
			+ " SYSDATE ) ";
                       
        try{            
            insRec = getNamedParameterJdbcTemplate().update(sql, new HashMap());
        }catch(DataAccessException dae){
            dae.printStackTrace();
            errorMsgBuffer.append(dae.getMessage());
        }                
        
        if(errorMsgBuffer.toString() != null && !errorMsgBuffer.toString().equals("")){
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        
        return insRec;
    }
    
    public int updateRateHdr(String line, String port, String terminal, String rateRef, String oprType, String effDate, String expDate, String currency, String description, String status, String rateSeqNo, String userId) throws DataAccessException{
        int updRec = 0;
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        String sql = "UPDATE TOS_RATE_HEADER SET " +
                    " RECORD_CHANGE_USER= '"+userId+ "',  RECORD_CHANGE_DATE = SYSDATE , "+
                    " EFFECTIVE_DATE = TO_DATE('" +effDate+ "' ,'DD/MM/YYYY'), " +
                    " EXPIRY_DATE = TO_DATE('"+ expDate+ "' ,'DD/MM/YYYY'), " +
                    " TOS_RATE_REF = '"+rateRef+ "', " +
                    " PORT = '"+port+"' , " +
                    " TERMINAL = '"+terminal +"', " +
                    " OPERATION_TYPE = '"+ oprType+"' , " +
                    " RATE_STATUS = '"+ status+"', " +
                    " DESCR = '" +description+ "' "+
                    " WHERE LINE = '"+line+"' " +
                    "  AND PORT = '"+port+"' " +
                    "  AND TERMINAL = '"+terminal+"' " +
                    "  AND OPERATION_TYPE = '"+oprType+"'  " +
                    "  AND TOS_RATE_SEQNO = '"+rateSeqNo+"' " ;
                    
        try{
            updRec = getNamedParameterJdbcTemplate().update(sql, new HashMap());
        }catch(DataAccessException dae){
            errorMsgBuffer.append(dae.getMessage().toString());
            dae.printStackTrace();
        }
                
        return updRec;
    }
    
    public StringBuffer checkConstraint(String port, String terminal, String operation, String rateRef) throws DataAccessException{
        StringBuffer errorMsgBuffer = new StringBuffer();
        String sql = null;
        int recCnt = 0;
        
        sql = " SELECT COUNT(1) FROM TOS_RATE_HEADER "+
              "  WHERE PORT='"+port+"' "+
              "    AND TERMINAL='"+terminal+"' "+
              "    AND OPERATION_TYPE='"+operation+"' "+
              "    AND TOS_RATE_REF='"+rateRef+"' ";
        
        recCnt = (int) getNamedParameterJdbcTemplate().queryForObject(sql, new HashMap(), Integer.class);
        
        if(recCnt>0){
            errorMsgBuffer.append(" *Port("+port+"), Terminal("+terminal+"), Operation Type("+operation+") and Rate Reference("+rateRef+") is already exist");            
        }
        
        return errorMsgBuffer;
        
    }
    
    public StringBuffer checkShipConstraint(List rateList){
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        if(rateList.size() > 0){
            for (int i = 0; i < rateList.size(); i++)  {
                TosNewRateSetupMaintainanceMod rateMod = (TosNewRateSetupMaintainanceMod) rateList.get(i);
                
                String shipTerm = rateMod.getShip_term();
                
                if(!shipTerm.equals("**")){
                    String sqlShipment = "SELECT COUNT(1) FROM V_SHIP_TERM WHERE MMMODE = '" +shipTerm+"' ";
                    
                    int recCnt = (int) getNamedParameterJdbcTemplate().queryForObject(sqlShipment, new HashMap(), Integer.class);
                    
                    if (recCnt == 0){
                        errorMsgBuffer.append(" *Invalid Shipment Term at row "+(i+1) );
                    }
                }
            }            
        }
        return errorMsgBuffer;
    }
    
    public StringBuffer checkDestConstraint(List rateList){
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        if(rateList.size() > 0){
            for (int i = 0; i < rateList.size(); i++)  {
                TosNewRateSetupMaintainanceMod rateMod = (TosNewRateSetupMaintainanceMod) rateList.get(i);
                
                String destType = rateMod.getDestination_type();
                int recCnt = 0;
                
                if(!destType.equals("") || destType != null){
                    String destCode = rateMod.getDestination_code();
                    
                    String sqlDestination = "";
                    if(destType.equalsIgnoreCase("C")){
                        sqlDestination = "SELECT COUNT(1) FROM RCLAPPS.VR_CAM_COUNTRY WHERE COUNTRY_CODE = '" +destCode+"' ";
                        recCnt = (int) getNamedParameterJdbcTemplate().queryForObject(sqlDestination, new HashMap(), Integer.class);                
                        if (recCnt == 0){
                            errorMsgBuffer.append(" *Invalid Country Code at row "+(i+1) );
                        }
                    } else if(destType.equalsIgnoreCase("P")){
                        sqlDestination = "SELECT COUNT(1) FROM RCLAPPS.VR_CAM_PORT WHERE PORT_CODE = '" +destCode +"' ";
                        recCnt = (int) getNamedParameterJdbcTemplate().queryForObject(sqlDestination, new HashMap(), Integer.class);
                        if (recCnt == 0){
                            errorMsgBuffer.append(" *Invalid Port Code at row "+(i+1) );
                        }
                    }
                }
            }            
        }
        return errorMsgBuffer;
    }
    
    public StringBuffer verifyDuplicate(List rateList){   
        StringBuffer errorMsgBuffer = new StringBuffer();
        TosNewRateSetupMaintainanceMod rateBean = new TosNewRateSetupMaintainanceMod();
        TosNewRateSetupMaintainanceMod tempRateBean = new TosNewRateSetupMaintainanceMod();
        int repeatFlag= 0;
        int row = 0;
                        
        if(rateList.size() > 0){
            for (int i = 0; i < rateList.size(); i++)  {
                rateBean = (TosNewRateSetupMaintainanceMod) rateList.get(i);
                
                for (int j = i+1; j < rateList.size(); j++)  {
                    tempRateBean = (TosNewRateSetupMaintainanceMod) rateList.get(j);
                    
                    if(rateBean.getActivity_code().equalsIgnoreCase(tempRateBean.getActivity_code())){
                        //Match current row data with next row data
                        if( rateBean.getShip_term().equalsIgnoreCase(tempRateBean.getShip_term()) &&
                            rateBean.getCoc_soc().equalsIgnoreCase(tempRateBean.getCoc_soc()) &&
                            rateBean.getCrane_type().equalsIgnoreCase(tempRateBean.getCrane_type()) &&
                            rateBean.getDay_night().equalsIgnoreCase(tempRateBean.getDay_night()) &&
                            rateBean.getDestination_code().equalsIgnoreCase(tempRateBean.getDestination_code()) &&
                            rateBean.getDom_inter().equalsIgnoreCase(tempRateBean.getDom_inter()) &&
                            rateBean.getHoliday().equalsIgnoreCase(tempRateBean.getHoliday()) &&
                            rateBean.getLoad_disch_tran_flag().equalsIgnoreCase(tempRateBean.getLoad_disch_tran_flag()) &&
                            rateBean.getMot().equalsIgnoreCase(tempRateBean.getMot()) &&
                            rateBean.getOperation_type().equalsIgnoreCase(tempRateBean.getOperation_type()) &&
                            rateBean.getVessel_type().equalsIgnoreCase(tempRateBean.getVessel_type()) &&
                            rateBean.getReason_code().equalsIgnoreCase(tempRateBean.getReason_code()) &&
                            rateBean.getShip_type().equalsIgnoreCase(tempRateBean.getShip_type()) &&
                            rateBean.getWeekday_weekend().equalsIgnoreCase(tempRateBean.getWeekday_weekend()) &&
                            rateBean.getVendor_code().equalsIgnoreCase(tempRateBean.getVendor_code()) ){
                            
                            repeatFlag = 1;
                            row = j;
                            break;
                        }
                    }
                }
                
                if(repeatFlag == 1){                    
                    break;
                }                                    
            }
            
            if(repeatFlag == 1){
                rateBean = (TosNewRateSetupMaintainanceMod) rateList.get(row);
                String actvtCd = rateBean.getActivity_code();                
                errorMsgBuffer.append("Duplicate Activity Code ("+actvtCd+") found at row ("+(row+1)+")");
            }
        }
        
        return errorMsgBuffer;
    }
    
    public void saveRateDtl(String line, String trade, String agent, String port, String terminal, String operation, String rateSeq, String userId, String rateRef, List rateList) throws Exception{
        TosNewRateSetupMaintainanceMod rateBean = new TosNewRateSetupMaintainanceMod();
        String sql = null;
        int qryInt = 0;                       
        StringBuffer errorMsgBuffer = new StringBuffer();  
        
        for(int i=0; i<rateList.size(); i++){
            rateBean = (TosNewRateSetupMaintainanceMod) rateList.get(i);
            
            if(rateBean.getDataStatus() == rateBean.UPDATE){
                if(rateBean.isDelete()){
                    sql = "DELETE FROM TOS_RATE_DTL "+
                            "WHERE PORT='"+rateBean.getPort()+"' "+
                            " AND TERMINAL='"+rateBean.getTerminal()+"' "+
                            " AND OPR_CODE='"+rateBean.getOpr_code()+"' "+
                            " AND TOS_RATE_SEQNO="+rateBean.getTos_rate_seqno()+" "+
                            " AND ACTIVITY_CODE='"+rateBean.getActivity_code()+"' "+
                            " AND TOS_RATE_SEQ_DTL="+rateBean.getTos_rate_seq_dtl();
                    
                    System.out.println("Delete Query: "+sql);
                    
                    try{
                        qryInt= getNamedParameterJdbcTemplate().update(sql, new HashMap());
                    }catch(Exception ex){              
                        ex.printStackTrace();
                        errorMsgBuffer.append(ex.getMessage());
                    }
                } else if(!rateBean.isDelete()){
                    sql = "UPDATE TOS_RATE_DTL SET " +
                            "CHANGE_USER= '"+userId+ "', CHANGE_DATE = TO_CHAR(SYSDATE,'YYYYMMDD'), CHANGE_TIME = TO_CHAR(SYSDATE,'HH24MI') "+", " +
                            "ACTIVITY_RATE = " + setInt(rateBean.getActivity_rate()) + ", " +
                            "COL1_RATE = " + setInt(rateBean.getCol1_rate()) + ", " +
                            "COL2_RATE = " + setInt(rateBean.getCol2_rate()) + ", " +
                            "COL3_RATE = " + setInt(rateBean.getCol3_rate()) + ", " +
                            "COL4_RATE = " + setInt(rateBean.getCol4_rate()) + ", " +
                            "COL5_RATE = " + setInt(rateBean.getCol5_rate()) + ", " +
                            "col6_rate = " + setInt(rateBean.getCol6_rate()) + ", " +
                            "col7_rate = " + setInt(rateBean.getCol7_rate()) + ", " +
                            "COL8_RATE = " + setInt(rateBean.getCol8_rate()) + ", " +
                            "COL9_RATE = " + setInt(rateBean.getCol9_rate()) + ", " +
                            "COL10_RATE = " + setInt(rateBean.getCol10_rate()) + ", " +
                            "COL11_RATE = " + setInt(rateBean.getCol11_rate()) + ", " +
                            "COL12_RATE = " + setInt(rateBean.getCol12_rate()) + ", " +
                            "COL13_RATE = " + setInt(rateBean.getCol13_rate()) + ", " +
                            "COL14_RATE = " + setInt(rateBean.getCol14_rate()) + ", " +
                            "COL15_RATE = " + setInt(rateBean.getCol15_rate()) + ", " +
                            "COL16_RATE = " + setInt(rateBean.getCol16_rate()) + ", " +
                            "COL17_RATE = " + setInt(rateBean.getCol17_rate()) + ", " +
                            "col18_rate = " + setInt(rateBean.getCol18_rate()) + ", " +
                            "col19_rate = " + setInt(rateBean.getCol19_rate()) + ", " +
                            "COL20_RATE = " + setInt(rateBean.getCol20_rate()) + "," +
                            "COC_SOC='"+rateBean.getCoc_soc()+"' ," +
                            "CRANE_TYPE='"+rateBean.getCrane_type()+"'," +
                            "SHIP_TERM='"+rateBean.getShip_term()+"', " +
                            "VESSEL_TYPE='"+rateBean.getVessel_type()+"', " +
                            "OPERATION_TYPE='"+rateBean.getOperation_type()+"',  " +
                            "LOAD_DISCH_TRAN_FLAG='"+rateBean.getLoad_disch_tran_flag()+"' ," +
                            "AUTO_ACTIVITY ='"+rateBean.getAuto_activity()+"' ," +
                            "DESTINATION_TYPE='"+rateBean.getDestination_type()+"' ,"+
                            "DESTINATION_CODE='"+rateBean.getDestination_code()+"' ,"+
                            "MOT='"+rateBean.getMot()+"' ,"+
                            "WEIGHT_MIN="+rateBean.getWeight_min()+" ,"+
                            "WEIGHT_MAX="+rateBean.getWeight_max()+" ,"+
                            "WEEKDAY_WEEKEND='"+rateBean.getWeekday_weekend()+"' ,"+
                            "HOLIDAY='"+rateBean.getHoliday() +"' ,"+
                            "DAY_NIGHT='"+rateBean.getDay_night()+"' ,"+
                            "REASON_CODE='"+rateBean.getReason_code()+"' ,"+
                            "DOM_INTER='"+rateBean.getDom_inter()+"' , "+
                            "SHIP_TYPE='"+rateBean.getShip_type()+"' "+
                            "WHERE PORT = '" + rateBean.getPort() + "' AND " +
                            "TERMINAL = '" + rateBean.getTerminal() + "' AND " +                        
                            "OPR_CODE = '" + rateBean.getOpr_code() + "' AND " +                       
                            "TOS_RATE_SEQNO ='" + rateBean.getTos_rate_seqno() + "' AND " +
                            "ACTIVITY_CODE = '" + rateBean.getActivity_code() + "' AND "+
                            "TOS_RATE_SEQ_DTL='"+rateBean.getTos_rate_seq_dtl()+"'";
                    
                    System.out.println("Update Query: "+sql);
                    
                    try{
                        qryInt= getNamedParameterJdbcTemplate().update(sql, new HashMap());
                    }catch(Exception ex){
                        ex.printStackTrace();
                        errorMsgBuffer.append(ex.getMessage());
                    }
                }
            } else{
                if(rateBean.getDataStatus() == rateBean.INSERT && !rateBean.isDelete()){
                    String tos_rate_sub_seq="";
                    sql=" SELECT TOS_RATE_SEQNO FROM TOS_RATE_HEADER " +
                        " WHERE PORT='"+port+"' " +
                        "  AND TERMINAL='"+terminal+"' " +
                        "  AND OPERATION_TYPE='"+operation+"' " +
                        "  AND TOS_RATE_REF='"+rateRef+"'";
                    
                    int ratedtlseq = (int) getNamedParameterJdbcTemplate().queryForObject(sql, new HashMap(), Integer.class);
                    
                    sql="Select nvl(max(TOS_RATE_SEQ_DTL),0)+1 from tos_rate_dtl where TOS_RATE_SEQNO='"+ratedtlseq+"'";
                    
                    tos_rate_sub_seq = ""+getNamedParameterJdbcTemplate().queryForObject(sql, new HashMap(), Integer.class);
                    
                    rateBean.setTos_rate_seqno(""+ratedtlseq);
                    
                    sql =   "INSERT INTO TOS_RATE_DTL (" +
                            "LINE, " +
                            "TRADE, " +
                            "AGENT, " +
                            "PORT, " +
                            "TERMINAL, " +                            
                            "OPR_CODE, " +
                            "ACTIVITY_CODE, " +
                            "OCEAN_COAST, " +
                            "CHARGE_CODE, " +
                            "POD_OCEAN_COAST, " +
                            "ACTIVITY_RATE, " +
                            "RATE_BASIS, " +
                            "COL1_RATE, " +
                            "COL2_RATE, " +
                            "COL3_RATE, " +
                            "COL4_RATE, " +
                            "COL5_RATE, " +
                            "COL6_RATE, " +
                            "COL7_RATE, " +
                            "COL8_RATE, " +
                            "COL9_RATE, " +
                            "COL10_RATE, " +
                            "COL11_RATE, " +
                            "COL12_RATE, " +
                            "COL13_RATE, " +
                            "COL14_RATE, " +
                            "COL15_RATE, " +
                            "COL16_RATE, " +
                            "COL17_RATE, " +
                            "COL18_RATE, " +
                            "COL19_RATE, " +
                            "COL20_RATE, " +
                            "RECORD_STATUS, "+
                            "TOS_RATE_SEQNO, "+
                            "ADD_USER, "+
                            "ADD_DAT, " +
                            "ADD_TIME, " +
                            "COC_SOC, " +
                            "CRANE_TYPE, " +
                            "SHIP_TERM, " +
                            "VESSEL_TYPE, " +
                            "OPERATION_TYPE, " +
                            "TOS_RATE_SEQ_DTL, " +
                            "LOAD_DISCH_TRAN_FLAG, " +
                            "AUTO_ACTIVITY, " +
                            "DESTINATION_TYPE, " +
                            "DESTINATION_CODE, " +
                            "MOT, " +
                            "WEIGHT_MIN, " +
                            "WEIGHT_MAX, " +
                            "WEEKDAY_WEEKEND, " +
                            "HOLIDAY, " +
                            "DAY_NIGHT, " +
                            "REASON_CODE, " +
                            "DOM_INTER, "+
                            "SHIP_TYPE "+
                            ") " +
                            "VALUES (" +
                            "'"+line+"', " +
                            "'"+trade+"', " +
                            "'"+agent+"', " +
                            "'" + port + "', " +
                            "'" + terminal + "', " +                            
                            "'" + operation + "', " +                            
                            "'" + rateBean.getActivity_code() + "', " +
                            "'0', " +
                            "'" + rateBean.getCharge_code() + "', " +
                            "'0', " +
                            "" + setInt(rateBean.getActivity_rate()) + ", " +
                            "'" + rateBean.getRate_basis() + "', " +
                            "" + setInt(rateBean.getCol1_rate() )+ ", " +
                            "" + setInt(rateBean.getCol2_rate() )+ ", " +
                            "" + setInt(rateBean.getCol3_rate() )+ ", " +
                            "" + setInt(rateBean.getCol4_rate() )+ ", " +
                            "" + setInt(rateBean.getCol5_rate() )+ ", " +
                            "" + setInt(rateBean.getCol6_rate() )+ ", " +
                            "" + setInt(rateBean.getCol7_rate() )+ ", " +
                            "" + setInt(rateBean.getCol8_rate() )+ ", " +
                            "" + setInt(rateBean.getCol9_rate() )+ ", " +
                            "" + setInt(rateBean.getCol10_rate()) + ", " +
                            "" + setInt(rateBean.getCol11_rate()) + ", " +
                            "" + setInt(rateBean.getCol12_rate()) + ", " +
                            "" + setInt(rateBean.getCol13_rate()) + ", " +
                            "" + setInt(rateBean.getCol14_rate()) + ", " +
                            "" + setInt(rateBean.getCol15_rate()) + ", " +
                            "" + setInt(rateBean.getCol16_rate()) + ", " +
                            "" + setInt(rateBean.getCol17_rate()) + ", " +
                            "" + setInt(rateBean.getCol18_rate()) + ", " +
                            "" + setInt(rateBean.getCol19_rate()) + ", " +
                            "" + setInt(rateBean.getCol20_rate()) + ", " +
                            "'" + rateBean.getRecord_status() + "', " +
                            "'" + ratedtlseq +"', " +
                            "'" + userId +"', " +
                            "TO_CHAR(SYSDATE,'YYYYMMDD'), " +
                            "TO_CHAR(SYSDATE,'HH24MI'), " +
                            "'"+ rateBean.getCoc_soc() +"', " +
                            "'"+ rateBean.getCrane_type() +"', " +
                            "'"+ rateBean.getShip_term() +"', " +
                            "'"+ rateBean.getVessel_type() +"', " +
                            "'"+ rateBean.getOperation_type() +"', " +
                            "'"+ tos_rate_sub_seq +"', " +
                            "'"+rateBean.getLoad_disch_tran_flag()+"' ," +
                            "'"+rateBean.getAuto_activity()+"' ," +
                            "'"+rateBean.getDestination_type()+"' ,"+
                            "'"+rateBean.getDestination_code()+"' ,"+
                            "'"+rateBean.getMot()+"' ,"+
                            ""+setInt(rateBean.getWeight_min())+" ,"+
                            ""+setInt(rateBean.getWeight_max())+" ,"+
                            "'"+rateBean.getWeekday_weekend()+"' ,"+
                            "'"+rateBean.getHoliday() +"' ,"+
                            "'"+rateBean.getDay_night()+"' ,"+
                            "'"+rateBean.getReason_code()+"' ,"+
                            "'"+rateBean.getDom_inter()+"' ,"+
                            "'"+rateBean.getShip_type()+"' "+
                            ") ";
                    
                    System.out.println("Insert Query: "+sql);
                    
                    try{
                        qryInt = getNamedParameterJdbcTemplate().update(sql, new HashMap());
                    }catch(Exception dae){                       
                        errorMsgBuffer.append(dae.getMessage());
                    }
                }
            }            
        }
        
        errorMsgBuffer = checkShipConstraint(rateList);
        errorMsgBuffer = checkDestConstraint(rateList);
        errorMsgBuffer = verifyDuplicate(rateList);
        
        if(!errorMsgBuffer.toString().equals("")){
            throw new Exception(errorMsgBuffer.toString());
        }
    }

    protected class GetRateDetailListProcedure extends StoredProcedure{
        private static final String SQL_TOS_RATE_DTL = "PCR_TOS_RATE_SETUP.PRR_GET_RATE_DTL_LIST";
        
        protected GetRateDetailListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_RATE_DTL);
                            
            declareParameter(new SqlOutParameter("p_o_v_rate_list", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_rate_seq",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_operation",OracleTypes.VARCHAR,rowMapper));            
            
            compile();
        }
        
        protected List getRateList(Map mapParams){
            Map outMap = new HashMap();
            
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetRateDetailListProcedure] p_i_v_rate_seq: " + (String)mapParams.get("p_i_v_rate_seq"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetRateDetailListProcedure] p_i_v_port: "     + (String)mapParams.get("p_i_v_port"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetRateDetailListProcedure] p_i_v_terminal: " + (String)mapParams.get("p_i_v_terminal"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetRateDetailListProcedure] p_i_v_operation: "+ (String)mapParams.get("p_i_v_operation"));            
            
            List<TosNewRateSetupMaintainanceMod> returnList = new ArrayList<TosNewRateSetupMaintainanceMod>();
            
            try{
                outMap = execute(mapParams);
                returnList = (List<TosNewRateSetupMaintainanceMod>) outMap.get("p_o_v_rate_list");                
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
        }
    }
        
    private class GetCategoryDetailListProcedure extends StoredProcedure{
        private static final String SQL_TOS_CATG_DTL = "PCR_TOS_RATE_SETUP.PRR_GET_CATG_LIST";
        
        protected GetCategoryDetailListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_CATG_DTL);
            declareParameter(new SqlOutParameter("p_o_v_rate_list", OracleTypes.CURSOR, rowMapper));            
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_operation",OracleTypes.VARCHAR,rowMapper));            
            
            compile();
        }
        
        protected List getCatgList(Map mapParams){
            Map outMap = new HashMap();
                        
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetCategoryDetailListProcedure] p_i_v_port: "     + (String)mapParams.get("p_i_v_port"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetCategoryDetailListProcedure] p_i_v_terminal: " + (String)mapParams.get("p_i_v_terminal"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetCategoryDetailListProcedure] p_i_v_operation: "+ (String)mapParams.get("p_i_v_operation"));            
                        
            List<TosNewRateSetupCategoryMod> returnList = new ArrayList<TosNewRateSetupCategoryMod>();
            
            try{
                outMap = execute(mapParams);
                returnList = (List<TosNewRateSetupCategoryMod>) outMap.get("p_o_v_rate_list");                
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
        }
    }
    
    private class GetActivityListProcedure extends StoredProcedure{
        private static final String SQL_GET_ACTVT = "PCR_TOS_RATE_SETUP.PRR_GET_ACTVT_LIST";
        
        protected GetActivityListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_GET_ACTVT);
            declareParameter(new SqlOutParameter("p_o_v_activity_list", OracleTypes.CURSOR, rowMapper));            
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_operation",OracleTypes.VARCHAR,rowMapper));            
            
            compile();            
        }
        
        protected List getActivityList(Map inParam){
            Map outMap = new HashMap();
            
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetActivityListProcedure] p_i_v_port: "     + (String)inParam.get("p_i_v_port"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetActivityListProcedure] p_i_v_terminal: " + (String)inParam.get("p_i_v_terminal"));
            System.out.println("[TosNewRateSetupMainrainanceJdbcDao][GetActivityListProcedure] p_i_v_operation: "+ (String)inParam.get("p_i_v_operation"));   
            
            List<TosNewRateSetupActivityMod> resultList = new ArrayList<TosNewRateSetupActivityMod>();
            
            outMap = execute(inParam);
            resultList = (List<TosNewRateSetupActivityMod>) outMap.get("p_o_v_activity_list");
            
            return resultList;
        }
    }
    
    private class TosCatgDtlListMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            TosNewRateSetupCategoryMod bean = new TosNewRateSetupCategoryMod();
            bean.setLine(RutString.nullToStr(rs.getString("LINE")));  
            bean.setPort(RutString.nullToStr(rs.getString("PORT")));  
            bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));  
            bean.setOpr_code(RutString.nullToStr(rs.getString("OPR_CODE"))); ; 
            bean.setCatg_col(RutString.nullToStr(rs.getString("CATG_COL")));
            bean.setSeq_no(RutString.nullToStr(rs.getString("SEQ_NO")));  
            bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));  
            bean.setRate_basis(RutString.nullToStr(rs.getString("RATE_BASIS")));  
            bean.setCharge_code(RutString.nullToStr(rs.getString("CHARGE_CODE")));  
            bean.setLink_column(RutString.nullToStr(rs.getString("LINK_COLUMN")));  
            bean.setTeu_factor(RutString.nullToStr(rs.getString("TEU_FACTOR")));  
            bean.setRecord_status(RutString.nullToStr(rs.getString("RECORD_STATUS")));
            return bean;
        }
    }
    
    private class TosActivityListMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            TosNewRateSetupActivityMod bean = new TosNewRateSetupActivityMod();                        
            bean.setLine(RutString.nullToStr(rs.getString("line")));
            bean.setPort(RutString.nullToStr(rs.getString("port")));
            bean.setTerminal(RutString.nullToStr(rs.getString("terminal")));
            bean.setOpr_code(RutString.nullToStr(rs.getString("opr_code")));
            bean.setActivity_code(RutString.nullToStr(rs.getString("activity_code")));
            bean.setSeq_no(RutString.nullToStr(rs.getString("seq_no")));
            bean.setSub_seq_no(RutString.nullToStr(rs.getString("sub_seq_no")));
            bean.setParent_activity_code(RutString.nullToStr(rs.getString("parent_activity_code")));
            bean.setDescription(RutString.nullToStr(rs.getString("description")));
            bean.setRate_basis(RutString.nullToStr(rs.getString("rate_basis")));
            bean.setVolume_units(RutString.nullToStr(rs.getString("volume_units")));
            bean.setVolume_moves(RutString.nullToStr(rs.getString("volume_moves")));
            bean.setVolume_tons(RutString.nullToStr(rs.getString("volume_tons")));
            bean.setCharge_code(RutString.nullToStr(rs.getString("charge_code")));
            bean.setAuto_yn(RutString.nullToStr(rs.getString("auto_yn")));
            bean.setLoad_disch_flag(RutString.nullToStr(rs.getString("load_disch_flag")));
            bean.setInclude_yn(RutString.nullToStr(rs.getString("include_yn")));
            bean.setRecord_status(RutString.nullToStr(rs.getString("record_status")));
            bean.setDisch_activity(RutString.nullToStr(rs.getString("disch_activity")));
            bean.setReefer_yn(RutString.nullToStr(rs.getString("reefer_yn")));
            bean.setRecovery_yn(RutString.nullToStr(rs.getString("recovery_yn")));
            bean.setRecovery_chg_code(RutString.nullToStr(rs.getString("recovery_chg_code")));
            bean.setCatg_based_yn(RutString.nullToStr(rs.getString("catg_based_yn")));
            return bean;
        }
    }
    
    private class TosRateDtlListMapper implements RowMapper{
    public Object mapRow(ResultSet rs, int row) throws SQLException {
        TosNewRateSetupMaintainanceMod bean = new TosNewRateSetupMaintainanceMod();
        
        bean.setLine(RutString.nullToStr(rs.getString("LINE")));  
        bean.setPort(RutString.nullToStr(rs.getString("PORT"))); 
        bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
        bean.setService(RutString.nullToStr(rs.getString("SERVICE")));  
        bean.setOpr_code(RutString.nullToStr(rs.getString("OPR_CODE")));  
        bean.setVendor_code(RutString.nullToStr(rs.getString("VENDOR_CODE")));  
        bean.setVendor_seq(RutString.nullToStr(rs.getString("VENDOR_SEQ")));  
        bean.setActivity_code(RutString.nullToStr(rs.getString("ACTIVITY_CODE")));
        bean.setParent_activity_code(RutString.nullToStr(rs.getString("PARENT_ACTIVITY_CODE")));
        bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));  
        bean.setOcean_coast(RutString.nullToStr(rs.getString("OCEAN_COAST")));  
        bean.setCharge_code(RutString.nullToStr(rs.getString("CHARGE_CODE")));  
        bean.setPod_ocean_coast(RutString.nullToStr(rs.getString("POD_OCEAN_COAST")));  
        if(RutString.nullToStr(rs.getString("ACTIVITY_RATE")).equals("")||RutString.nullToStr(rs.getString("ACTIVITY_RATE")).equals("0")){bean.setActivity_rate("0.00"); } else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setActivity_rate(dc.format((rs.getDouble("ACTIVITY_RATE"))));}                        
        bean.setRate_basis(RutString.nullToStr(rs.getString("RATE_BASIS")));  
        if(RutString.nullToStr(rs.getString("COL1_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol1_rate( dc.format(rs.getDouble("COL1_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL2_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol2_rate( dc.format(rs.getDouble("COL2_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL3_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol3_rate( dc.format(rs.getDouble("COL3_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL4_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol4_rate( dc.format(rs.getDouble("COL4_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL5_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol5_rate( dc.format(rs.getDouble("COL5_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL6_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol6_rate( dc.format(rs.getDouble("COL6_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL7_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol7_rate( dc.format(rs.getDouble("COL7_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL8_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol8_rate( dc.format(rs.getDouble("COL8_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL9_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol9_rate( dc.format(rs.getDouble("COL9_RATE") ));}
        if(RutString.nullToStr(rs.getString("COL10_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol10_rate(dc.format(rs.getDouble("COL10_RATE")));}
        if(RutString.nullToStr(rs.getString("COL11_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol11_rate(dc.format(rs.getDouble("COL11_RATE")));}
        if(RutString.nullToStr(rs.getString("COL12_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol12_rate(dc.format(rs.getDouble("COL12_RATE")));}
        if(RutString.nullToStr(rs.getString("COL13_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol13_rate(dc.format(rs.getDouble("COL13_RATE")));}
        if(RutString.nullToStr(rs.getString("COL14_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol14_rate(dc.format(rs.getDouble("COL14_RATE")));}
        if(RutString.nullToStr(rs.getString("COL15_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol15_rate(dc.format(rs.getDouble("COL15_RATE")));}
        if(RutString.nullToStr(rs.getString("COL16_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol16_rate(dc.format(rs.getDouble("COL16_RATE")));}
        if(RutString.nullToStr(rs.getString("COL17_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol17_rate(dc.format(rs.getDouble("COL17_RATE")));}
        if(RutString.nullToStr(rs.getString("COL18_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol18_rate(dc.format(rs.getDouble("COL18_RATE")));}
        if(RutString.nullToStr(rs.getString("COL19_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol19_rate(dc.format(rs.getDouble("COL19_RATE")));}
        if(RutString.nullToStr(rs.getString("COL20_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol20_rate(dc.format(rs.getDouble("COL20_RATE")));}                        
        bean.setRecord_status(RutString.nullToStr(rs.getString("RECORD_STATUS")));  
        bean.setTos_rate_seqno(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));          
        bean.setInclude_yn(RutString.nullToStr(rs.getString("INCLUDE_YN")));  
        bean.setCatg_based_yn(RutString.nullToStr(rs.getString("CATG_BASED_YN")));  
        bean.setAuto_yn(RutString.nullToStr(rs.getString("AUTO_YN"))); 
        bean.setCoc_soc(RutString.nullToStr(rs.getString("COC_SOC"))); 
        bean.setShip_term(RutString.nullToStr(rs.getString("SHIP_TERM")));
        bean.setCrane_type(RutString.nullToStr(rs.getString("CRANE_TYPE")));  
        bean.setVessel_type(RutString.nullToStr(rs.getString("VESSEL_TYPE")));   
        bean.setOperation_type(RutString.nullToStr(rs.getString("OPERATION_TYPE")));  
        bean.setTos_rate_seq_dtl(RutString.nullToStr(rs.getString("TOS_RATE_SEQ_DTL")));                        
        bean.setLoad_disch_tran_flag(RutString.nullToStr(rs.getString("Load_disch_tran_flag"))); 
        bean.setAuto_activity(RutString.nullToStr(rs.getString("Auto_activity"))); 
        bean.setDestination_type(RutString.nullToStr(rs.getString("Destination_type"))); 
        bean.setDestination_code(RutString.nullToStr(rs.getString("Destination_code"))); 
        bean.setMot(RutString.nullToStr(rs.getString("Mot"))); 
        if(RutString.nullToStr(rs.getString("Weight_min")).equals("") || RutString.nullToStr(rs.getString("Weight_min")).equals("0")){bean.setWeight_min("0.00");}else{DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setWeight_min(dc.format(rs.getDouble("Weight_min"))); }
        if(RutString.nullToStr(rs.getString("Weight_max")).equals("") || RutString.nullToStr(rs.getString("Weight_max")).equals("0")){bean.setWeight_max("0.00");}else{DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setWeight_max(dc.format(rs.getDouble("Weight_max"))); }                        
        bean.setWeekday_weekend(RutString.nullToStr(rs.getString("Weekday_weekend"))); 
        bean.setHoliday(RutString.nullToStr(rs.getString("Holiday"))); 
        bean.setDay_night(RutString.nullToStr(rs.getString("Day_night"))); 
        bean.setReason_code(RutString.nullToStr(rs.getString("Reason_code")));
        bean.setDom_inter(RutString.nullToStr(rs.getString("DOM_INTER")));
        bean.setShip_type(RutString.nullToStr(rs.getString("SHIP_TYPE")));
        bean.setDataStatus(bean.UPDATE);
        bean.setDelete(false);
        return bean;
    }
}


    public String setInt(String str){
        if((str==null)||(str.trim().equals("null"))||(str.trim().equals(""))){
            return "null";
        }
        else{
            return str.trim();
        }
    }
    
    public List getMidStreamDropdownList() throws DataAccessException{
        
        System.out.println("[TosMidStreamJdbcDao][getMidStreamDropdownList]: Started");
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT CONFIG_VALUE AS OPR_TYPE_CODE, CONFIG_DESC AS OPR_TYPE_DESC ");
        sql.append(" FROM VASAPPS.VCM_CONFIG_MST ");
        sql.append(" WHERE CONFIG_TYP = 'TOS_OPR_TYPE' ");
        sql.append(" AND STATUS = 'A' ");
        sql.append(" AND DISPLAY_FLAG = 'Y' ");
        sql.append(" ORDER BY SORT_ORDER ");
        
        System.out.println("[TosMidStreamJdbcDao][getMidStreamDropdownList]:sql = " + sql.toString());
        HashMap map = new HashMap();
        
        return getNamedParameterJdbcTemplate().query(
            sql.toString(),
            new HashMap(),
            new RowMapper(){
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    TosMidStreamMod bean = new TosMidStreamMod();                    
                    bean.setTypeCode(RutString.nullToStr(rs.getString("OPR_TYPE_CODE")));
                    bean.setTypeDesc(RutString.nullToStr(rs.getString("OPR_TYPE_DESC")));
                    
                    return bean;
                }
            }
        );
        
    }
    

       
}
