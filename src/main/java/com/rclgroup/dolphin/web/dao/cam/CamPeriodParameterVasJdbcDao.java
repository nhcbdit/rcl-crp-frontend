package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamPeriodParameterMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public class CamPeriodParameterVasJdbcDao extends RrcStandardDao implements CamPeriodParameterVasDao{
    public CamPeriodParameterVasJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List<CamPeriodParameterMod> getList(String periodParameterCode,String noOfDay,String status,String sortBy,String sortIn) throws DataAccessException{
        CamPeriodParameterHelperJdbcDao camPeriodParameterHelperJdbcDao = new CamPeriodParameterHelperJdbcDao();
        return camPeriodParameterHelperJdbcDao.getList(this.getNamedParameterJdbcTemplate(),periodParameterCode,noOfDay,status,sortBy,sortIn);
    }
    
    public List<CamPeriodParameterMod> getItem(String periodParameterId) throws DataAccessException{
        CamPeriodParameterHelperJdbcDao camPeriodParameterHelperJdbcDao = new CamPeriodParameterHelperJdbcDao();
        return camPeriodParameterHelperJdbcDao.getItem(this.getNamedParameterJdbcTemplate(),periodParameterId);
    }
    
    public StringBuffer save(boolean isInsert, CamPeriodParameterMod updateData) throws Exception{
        CamPeriodParameterHelperJdbcDao camPeriodParameterHelperJdbcDao = new CamPeriodParameterHelperJdbcDao();
        return camPeriodParameterHelperJdbcDao.save(this.getJdbcTemplate(),this.getNamedParameterJdbcTemplate(), isInsert,updateData);
    }
    
    public String[] deleteByIds(String[] idArray) throws Exception{
        CamPeriodParameterHelperJdbcDao camPeriodParameterHelperJdbcDao = new CamPeriodParameterHelperJdbcDao();
        return camPeriodParameterHelperJdbcDao.deleteByIds(this.getJdbcTemplate(),this.getNamedParameterJdbcTemplate(),idArray);
    }
}
