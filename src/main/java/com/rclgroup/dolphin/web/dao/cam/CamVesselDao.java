/*-----------------------------------------------------------------------------------------------------------  
CamVesselDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamVesselMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamVesselDao extends RriStandardDao  {

    /**
     * list vessel records for help screen with status
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of vessels with status
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException;
    
    /**
     * @param vesselCode vessel code
     * @return vessel name of vessel code
     * @throws DataAccessException
     */
    public String getVesselName(String vesselCode) throws DataAccessException;

    /**
     * @param vesselCode vesselCode
     * @return vessel model of vessel code
     * @throws DataAccessException
     */
    public CamVesselMod findByKeyVesselCode(String vesselCode) throws DataAccessException;
    
    /**
     * list vessel records for help screen vessel operation with status
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of vessels with status
     * @throws DataAccessException
     */
    public List listForHelpScreenVesselOpr(String find,String search,String wild, String status) throws DataAccessException;
}


