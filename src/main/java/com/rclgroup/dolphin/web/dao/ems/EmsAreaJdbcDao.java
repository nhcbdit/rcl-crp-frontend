/*-----------------------------------------------------------------------------------------------------------  
EmsAreaJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 29/04/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
02 23/08/13  NIP                       Add function for Area and Zone
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsAreaMod;
import com.rclgroup.dolphin.web.util.RutString;
 import java.sql.ResultSet;
 import java.sql.SQLException;


import java.util.HashMap;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class EmsAreaJdbcDao extends RrcStandardDao implements EmsAreaDao {
     
     public EmsAreaJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String area) throws DataAccessException {
         System.out.println("[EmsAreaJdbcDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AREA_CODE ");
         sql.append("FROM VR_EMS_AREA ");
         sql.append("WHERE AREA_CODE = :area");
         HashMap map = new HashMap();
         map.put("area",area);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[EmsAreaJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValid(String area, String status) throws DataAccessException {
         System.out.println("[EmsAreaDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AREA_CODE ");
         sql.append("FROM VR_EMS_AREA ");
         sql.append("WHERE AREA_CODE = :area ");
         sql.append("AND AREA_STATUS = :status ");
         HashMap map = new HashMap();
         map.put("area",area);
         map.put("status",status);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[EmsAreaJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[EmsAreaDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AREA_CODE ");
         sql.append("      ,AREA_DESCRIPTION ");    
         sql.append("      ,AREA_STATUS "); 
         sql.append("FROM VR_EMS_AREA ");
         sql.append(sqlCriteria);
         System.out.println("[EmsAreaJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
         System.out.println("[EmsAreaJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowModMapper());         
     }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "WHERE AREA_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "WHERE AREA_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "WHERE AREA_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }      
     
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String status) throws DataAccessException {
         System.out.println("[EmsAreaJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT DISTINCT AREA_CODE ");
         sql.append("      ,AREA_DESCRIPTION ");
         sql.append("      ,REGION_CODE ");             
         sql.append("      ,AREA_STATUS ");              
         sql.append("FROM VR_EMS_ALL_GEO ");
         sql.append("WHERE AREA_STATUS = :status ");   
         if((regionCode!=null)&&(!regionCode.trim().equals(""))){
             sql.append("  AND REGION_CODE = :regionCode "); 
         }
         sql.append(sqlCriteria);
         System.out.println("[EmsAreaJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         HashMap map = new HashMap();
         map.put("regionCode",regionCode);
         map.put("status",status);
         System.out.println("[EmsAreaJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());  
     }      
     
     private String createSqlCriteriaWithStatus(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND AREA_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "AND AREA_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("R")){
                 sqlCriteria = "AND REGION_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND AREA_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }        
     
     protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModelForHelp(rs);
         }
     } 
     
     private EmsAreaMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
         EmsAreaMod area = new EmsAreaMod();
         area.setAreaCode(RutString.nullToStr(rs.getString("AREA_CODE")));   
         area.setAreaName(RutString.nullToStr(rs.getString("AREA_DESCRIPTION")));   
         area.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));   
         area.setAreaStatus(RutString.nullToStr(rs.getString("AREA_STATUS")));

         return area;
     }     
     
     public String getAreaView(String line,String region,String agent,String fsc) throws DataAccessException {//##01
         String result = null;
         StringBuffer sql = new StringBuffer();
         sql.append("select distinct AREA_CODE ");
         sql.append("from rclapps.VR_SYS_GEO ");
         sql.append("WHERE 1=1 ");
         
         HashMap map = new HashMap();
         
         if(line != null && !line.equals("")){
            map.put("line",line.toUpperCase());
            sql.append("and LV1 = :line ");
         } 
         if(region != null && !region.equals("")){
            map.put("region",region.toUpperCase());
            sql.append("and LV2 = :region ");
         } 
         if(agent != null && !agent.equals("")){
            map.put("agent",agent.toUpperCase());
            sql.append("and LV3 = :agent ");
         }  
         
         if(fsc != null && !fsc.equals("")){
            map.put("fsc",fsc.toUpperCase());
            sql.append("and FSC_CODE = :fsc ");
         }  
         
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             result=RutString.nullToStr(rs.getString("AREA_CODE"));
         } 
         return result;
     }  
 }
