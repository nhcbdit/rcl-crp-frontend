package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bkg.BkgBookingAvailabilityReportMod;
import com.rclgroup.dolphin.web.model.bkg.BkgBookingListMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BkgBookingAvailabilityReportJdbcDao  extends RrcStandardDao implements BkgBookingAvailabilityReportDao{
    
    private BkgBookingAvailabilityReportStoreProcedure bkgBookingAvailabilityReportStoreProcedure;
    public BkgBookingAvailabilityReportJdbcDao() {
         super();
    }
   
    protected void initDao() throws Exception {
        super.initDao();
        bkgBookingAvailabilityReportStoreProcedure = new BkgBookingAvailabilityReportStoreProcedure(getJdbcTemplate());
    }

    public boolean generateTempTable(RrcStandardMod mod) {
        return bkgBookingAvailabilityReportStoreProcedure.generate(mod);
    }

    protected class BkgBookingAvailabilityReportStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_BKG_BOOKING_AVAILABILITY.PR_GEN_BKG_AVAI_TMP";
        
        protected BkgBookingAvailabilityReportStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_direction", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_datefrom", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dateto", Types.VARCHAR));
            declareParameter(new SqlParameter("p_soccoc", Types.VARCHAR));
           
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof BkgBookingAvailabilityReportMod && outputMod instanceof BkgBookingAvailabilityReportMod) {
                BkgBookingAvailabilityReportMod aInputMod = (BkgBookingAvailabilityReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_user_id", aInputMod.getUserId());
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_direction", aInputMod.getDirection());
                inParameters.put("p_pol", aInputMod.getPol());
                inParameters.put("p_datefrom", aInputMod.getDateFrom());
                inParameters.put("p_dateto", aInputMod.getDateTo());
                inParameters.put("p_soccoc", aInputMod.getSocCoc());
                               
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_user_id = "+inParameters.get("p_user_id"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_service = "+inParameters.get("p_service"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_direction = "+inParameters.get("p_direction"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_pol = "+inParameters.get("p_pol"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_datefrom = "+inParameters.get("p_datefrom"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_dateto = "+inParameters.get("p_dateto"));
                System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_soccoc = "+inParameters.get("p_soccoc"));
                
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
}
