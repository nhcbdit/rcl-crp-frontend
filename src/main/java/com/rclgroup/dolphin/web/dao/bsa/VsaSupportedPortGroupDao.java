/*-----------------------------------------------------------------------------------------------------------  
VsaSupportedPortGroupDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 22/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface VsaSupportedPortGroupDao extends RriStandardDao {
    /**
     * find supported port group by using BSA model Id, and record status 
     * @param vsaModelId BSA model Id
     * @param recordStatus record status of returned records
     * @return List of BSA supported port group models
     * @throws DataAccessException
     */
    public List findByVsaModelIdRecordStatus(String vsaModelId,String recordStatus) throws DataAccessException;
    
    /**
     * insert a supported port group record
     * @param mod a supported port group model as input and output
     * @return whether insertion is successful
     * @throws DataAccessException exception which client has to catch all following error messages: 
     *                             error message: BSA_SPG01_BSA_PORT_GROUP_ID_REQ
     *                             error message: BSA_SPG01_BSA_MODEL_ID_REQ
     *                             error message: BSA_SPG01_PORT_GROUP_CODE_REQ
     *                             error message: BSA_SPG01_PORT_GROUP_CODE_NOT_FOUND with one argument: port group code from input value 
     *                             error message: BSA_SPG01_PORT_GRP_SOC_COC_REQ
     *                             error message: BSA_SPG01_PORT_GRP_SOC_COC_ONLY_SOC 
     *                             error message: BSA_SPG01_RECORD_STATUS_REQ
     *                             error message: BSA_SPG01_STATUS_NOT_IN_RANGE with one argument: record status from input value   
     *                             error message: BSA_SPG01_RECORD_ADD_USER_REQ
     *                             error message: BSA_SPG01_VALUE_DUP with one argument: primary key from input value  
     *                             error message: ORA-XXXXX (un-exceptional oracle error)   
     */
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * update a supported port group record
     * @param mod a supported port group model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages: 
     *                             error message: BSA_SPG01_BSA_PORT_GROUP_ID_REQ
     *                             error message: BSA_SPG01_BSA_MODEL_ID_REQ
     *                             error message: BSA_SPG01_PORT_GROUP_CODE_REQ
     *                             error message: BSA_SPG01_PORT_GROUP_CODE_NOT_FOUND with one argument: port group code from input value
     *                             error message: BSA_SPG01_PORT_GRP_SOC_COC_REQ
     *                             error message: BSA_SPG01_PORT_GRP_SOC_COC_ONLY_SOC 
     *                             error message: BSA_SPG01_RECORD_STATUS_REQ
     *                             error message: BSA_SPG01_STATUS_NOT_IN_RANGE with one argument: record status from input value    
     *                             error message: BSA_SPG01_RECORD_CHANGE_USER_REQ
     *                             error message: BSA_SPG01_RECORD_CHANGE_DATE_REQ
     *                             error message: BSA_SPG01_UPDATE_CON
     *                             error message: BSA_SPG01_BSA_PORT_GROUP_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean update(RrcStandardMod mod) throws DataAccessException;

    /**
     * delete a supported port group record
     * @param mod a supported port group model
     * @return whether deletion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: BSA_SPG01_RECORD_CHANGE_DATE_REQ 
     *                             error message: BSA_SPG01_DELETE_CON  
     *                             error message: BSA_SPG01_BSA_PORT_GROUP_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * check constraint on a supported port group record
     * @param mod a supported port group model
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: BSA_SPG01_PORT_GROUP_CONSTRAINT01
     *                             error message: BSA_SPG01_PORT_GROUP_CONSTRAINT01
     *                             error message: ORA-XXXXX (un-exceptional oracle error)     
     */
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException;
    
}



