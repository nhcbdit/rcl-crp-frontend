/*-----------------------------------------------------------------------------------------------------------  
CamVesselJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamContentTypeMod;
import com.rclgroup.dolphin.web.model.cam.CamVesselMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;


public class CamVesselJdbcDao extends RrcStandardDao implements CamVesselDao {
   
    public CamVesselJdbcDao() {
    }
   
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException {
        System.out.println("[CamVesselDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VESSEL_CODE ");
        sql.append("      ,VESSEL_NAME ");
        sql.append("      ,OPERATOR_CODE ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("FROM VR_CAM_VESSEL_MASTER ");
        sql.append("WHERE 1 = 1 ");
        sql.append("  AND RECORD_STATUS = :status ");
        sql.append(sqlCriteria);            
        sql.append("ORDER BY VESSEL_CODE ");
        System.out.println("[CamVesselDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[CamVesselDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   Collections.singletonMap("status", status),
                   new RowModMapper());
    }
    
    public CamVesselMod findByKeyVesselCode(String vesselCode) throws DataAccessException{
        System.out.println("[CamVesselDao][findByKeyVesselCode]: Started");
        CamVesselMod camVesselMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VESSEL_CODE ");
        sql.append("      ,VESSEL_NAME ");
        sql.append("      ,OPERATOR_CODE ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_CAM_VESSEL_MASTER ");
        sql.append("WHERE VESSEL_CODE = :vesselCode ");           
        System.out.println("[CamVesselDao][findByKeyVesselCode]: sql: [" + sql.toString()+"]");
        System.out.println("[CamVesselDao][findByKeyVesselCode]: vesselCode: " + vesselCode);
        System.out.println("[CamVesselDao][findByKeyVesselCode]: Finished");
        try{
            camVesselMod = (CamVesselMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("vesselCode", vesselCode),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamVesselMod mod = new CamVesselMod();
                           mod.setVesselCode(RutString.nullToStr(rs.getString("VESSEL_CODE")));
                           mod.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
                           mod.setOperatorCode(RutString.nullToStr(rs.getString("OPERATOR_CODE")));
                           mod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           mod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           mod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE")); 
                           mod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           mod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return mod; 
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            camVesselMod = null;
        }
        return camVesselMod;
    }
    
    public String getVesselName(String vesselCode) throws DataAccessException{
        String vesselName = null;
        CamVesselMod mod = findByKeyVesselCode(vesselCode);
        if(mod == null){
            vesselName = null;
        }else{
            vesselName = mod.getVesselName();
        }
        return vesselName;
    }

    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("C")) {
                sqlCriteria = "AND VESSEL_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("N")) {
                sqlCriteria = "AND VESSEL_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("O")) {
                sqlCriteria = "AND OPERATOR_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND RECORD_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private CamVesselMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        CamVesselMod mod = new CamVesselMod();
        mod.setVesselCode(RutString.nullToStr(rs.getString("VESSEL_CODE")));
        mod.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
        mod.setOperatorCode(RutString.nullToStr(rs.getString("OPERATOR_CODE")));
        mod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
        return mod;
    }
    
    public List listForHelpScreenVesselOpr(String find,String search,String wild, String status) throws DataAccessException {
        System.out.println("[CamVesselDao][listForHelpScreenVesselOpr]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT ");
        sql.append("      ,OPERATOR_CODE ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("FROM VR_CAM_VESSEL_MASTER ");
        sql.append("WHERE 1 = 1 ");
        sql.append("  AND RECORD_STATUS = :status ");
        sql.append(sqlCriteria);            
        sql.append("ORDER BY OPERATOR_CODE ");
        System.out.println("[CamVesselDao][listForHelpScreenVesselOpr]: SQL: " + sql.toString());
        System.out.println("[CamVesselDao][listForHelpScreenVesselOpr]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   Collections.singletonMap("status", status),
                   new RowModMapperVesselOpr());
    }
    
    
    protected class RowModMapperVesselOpr implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelpVesselOpr(rs);
        }
    } 
    
    private CamVesselMod moveDbToModelForHelpVesselOpr(ResultSet rs) throws SQLException {
        CamVesselMod mod = new CamVesselMod();
        mod.setOperatorCode(RutString.nullToStr(rs.getString("OPERATOR_CODE")));
        mod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
        return mod;
    }
}



