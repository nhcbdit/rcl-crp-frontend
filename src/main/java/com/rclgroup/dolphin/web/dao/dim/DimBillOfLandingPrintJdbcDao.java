/*------------------------------------------------------
DimBillOfLandingPrintJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
Author Kitti Pongsirisakun 11/012/10
- Change Log -------------------------------------------
## DD/MM/YY �User-        -TaskRef-  -ShortDescription-
01 04/08/10  KIT           N/A        Created
02 03/10/16  SarawutA.                Create new method showHideGenExcelButton,generateExcelHeader,generateExcelDetail
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dim.DimBillOfLandingPrintExcelMod;
import com.rclgroup.dolphin.web.model.dim.DimBillOfLandingPrintMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import oracle.jdbc.OracleTypes;
//import oracle.sql.ARRAY;
//import  oracle.sql.ArrayDescriptor ;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;

//import org.jboss.jca.adapters.jdbc.jdk8.WrappedConnectionJDK8;


public class DimBillOfLandingPrintJdbcDao extends RrcStandardDao implements DimBillOfLandingPrintDao {
    private InsertStoreProcedure insertStoreProcedure;
    private InsertStoreProcedureBlPriChk insertStoreProcedureBlPriChk;
    private GenerateExcelHeaderProcedure generateExcelHeaderProcedure;
    private GenerateExcelDetailProcedure generateExcelDetailProcedure;
    private ShowHideGenExcelButtonProcedure showHideGenExcelButtonProcedure;
    
    public DimBillOfLandingPrintJdbcDao() {
    }    
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        insertStoreProcedureBlPriChk = new InsertStoreProcedureBlPriChk(getJdbcTemplate());
        generateExcelHeaderProcedure = new GenerateExcelHeaderProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelHdrList());
        generateExcelDetailProcedure = new GenerateExcelDetailProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelDtlList());
        showHideGenExcelButtonProcedure = new ShowHideGenExcelButtonProcedure(this.getJdbcTemplate());
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_BL_PRINT.PR_BL_PRINT";
    protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_ser", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ves", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voy", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_cocsoc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_sts", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dte_frm", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dte_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_third", Types.VARCHAR));    
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));        
            declareParameter(new SqlParameter("p_l_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_r_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_a_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_option", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_inv", Types.VARCHAR));
            declareParameter(new SqlParameter("p_port_inv", Types.VARCHAR));
            declareParameter(new SqlParameter("p_chk_fwd", Types.VARCHAR));
            declareParameter(new SqlParameter("p_print", Types.VARCHAR));
            
          //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
          //  declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));     
            compile();
            System.out.println("InsertStoreProcedure declare parameter end");
        }
    protected String insert(      String service  ,           String vessel,            String voyage,     String direction, 
                                                       String bl,                    String cocsoc,      
                                                       String pol_sts,             String dateFrom,      String dateTo,      String third,
                                                       String fsc,                     String userCode,     String sessionid,  String line,
                                                       String region,              String agent,             String fscCode,     String p_option,
                                                       String p_session_inv,String p_port_inv,     String chkFwdBl, String print) {
                                                       
        return insertPro(                service  ,                       vessel,                       voyage,                  direction, 
                                                       bl,                                   cocsoc,                  
                                                       pol_sts,                          dateFrom,                 dateTo,                   third,
                                                       fsc,                                  userCode,                sessionid,               line,
                                                      region,                            agent,                        fscCode,                 p_option,
                                                      p_session_inv,              p_port_inv,                chkFwdBl, print);
    }
    protected String insertPro(String service  ,          String vessel,            String voyage,     String direction, 
                                                       String bl,                    String cocsoc,      
                                                       String pol_sts,             String dateFrom,      String dateTo,      String third,
                                                       String fsc,                     String userCode,     String sessionid,  String line,
                                                       String region,              String agent,             String fscCode,     String p_option,
                                                       String p_session_inv,String p_port_inv,     String chkFwdBl, String print) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            inParameters.put("p_ser",RutDatabase.stringToDb(service));
            inParameters.put("p_ves",RutDatabase.stringToDb(vessel));
            inParameters.put("p_voy",RutDatabase.stringToDb(voyage));
            inParameters.put("p_dir",RutDatabase.stringToDb(direction));
            inParameters.put("p_bl",RutDatabase.stringToDb(bl));
            inParameters.put("p_cocsoc",RutDatabase.stringToDb(cocsoc));
            inParameters.put("p_pol_sts",RutDatabase.stringToDb(pol_sts));
            inParameters.put("p_dte_frm",RutDatabase.stringToDb(dateFrom));
            inParameters.put("p_dte_to",RutDatabase.stringToDb(dateTo));             
            inParameters.put("p_third",RutDatabase.stringToDb(third));      
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_l_code",RutDatabase.stringToDb(line));
            inParameters.put("p_r_code",RutDatabase.stringToDb(region));
            inParameters.put("p_a_code",RutDatabase.stringToDb(agent));
            inParameters.put("p_fsc_code",RutDatabase.stringToDb(fscCode));      
            inParameters.put("p_option",RutDatabase.stringToDb(p_option));      
            inParameters.put("p_session_inv",RutDatabase.stringToDb(p_session_inv));      
            inParameters.put("p_port_inv",RutDatabase.stringToDb(p_port_inv));      
            inParameters.put("p_chk_fwd",RutDatabase.stringToDb(chkFwdBl)); 
            inParameters.put("p_print",RutDatabase.stringToDb(print)); 
            
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_ser = "+inParameters.get("p_ser"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_ves = "+inParameters.get("p_ves"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_voy = "+inParameters.get("p_voy"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_dir = "+inParameters.get("p_dir"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_bl = "+inParameters.get("p_bl"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_cocsoc = "+inParameters.get("p_cocsoc"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_pol_sts = "+inParameters.get("p_pol_sts"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_dte_frm = "+inParameters.get("p_dte_frm"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_dte_to = "+inParameters.get("p_dte_to"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_third = "+inParameters.get("p_third"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_fsc = "+inParameters.get("p_fsc"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_usr = "+inParameters.get("p_usr"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_sid = "+inParameters.get("p_sid"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_l_code = "+inParameters.get("p_l_code"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_r_code = "+inParameters.get("p_r_code"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_a_code = "+inParameters.get("p_a_code"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_fsc_code = "+inParameters.get("p_fsc_code"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_option = "+inParameters.get("p_option"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_session_inv = "+inParameters.get("p_session_inv"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_port_inv = "+inParameters.get("p_port_inv"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_chk_fwd = "+inParameters.get("p_chk_fwd"));
            System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_print = "+inParameters.get("p_print"));

          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_sid");
               System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    
    
    
    protected class InsertStoreProcedureBlPriChk extends StoredProcedure {
    
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_BL_PRINT.PR_BL_PRINT_CHECK";
    protected  InsertStoreProcedureBlPriChk(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("InsertStoreProcedureBlPriChk declare parameter begin");
            declareParameter(new SqlParameter("p_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_name", Types.VARCHAR));
//            declareParameter(new SqlParameter("p_BL_ARRAY", OracleTypes.ARRAY,"ARRAY_VARCHAR"));
            declareParameter(new SqlParameter("p_BL_ARRAY", Types.VARCHAR));
            declareParameter(new SqlParameter("p_del_old", Types.INTEGER));
     
          //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
          //  declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));     
            compile();
            System.out.println("InsertStoreProcedure declare parameter end");
        }
    protected String insert( String session, String userName,String[] blListl) {
                                                       
        return insertPro(  session  ,  userName,     blListl   );
    }
    protected String insertPro(String sessionIDPro, String userName,String[] blListl) {
            System.out.println("InsertStoreProcedureBlPriChk assign value to  parameter begin");
           
//            Map inParameters = new HashMap();
//            inParameters.put("p_id",RutDatabase.stringToDb(sessionIDPro));
//            inParameters.put("p_name",RutDatabase.stringToDb(userName));
//            Connection conn = null;
            try{
//                conn = getJdbcTemplate().getDataSource().getConnection();
//                Connection jbossConn = ((WrappedConnectionJDK8) conn).getUnderlyingConnection(); 
//                ArrayDescriptor intDesc = new ArrayDescriptor("ARRAY_VARCHAR", conn);
//                ARRAY roleIds = new ARRAY(intDesc, conn, blListl);
//                                
//                inParameters.put("p_BL_ARRAY", roleIds);
            	int del = 0;
            	
            	for(int i = 0; i < blListl.length; i++) {
            		Map inParameters = new HashMap();
                    inParameters.put("p_id",RutDatabase.stringToDb(sessionIDPro));
                    inParameters.put("p_name",RutDatabase.stringToDb(userName));
            		inParameters.put("p_BL_ARRAY", blListl[i]);
            		inParameters.put("p_del_old", del);
            		Map outParameters = execute(inParameters);
            		del = 1;
            	}
            	
//                System.out.println("No Error >>>>>>>>>>>>>>>>>>>>>>>>>>> ");
//                System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_id = "+inParameters.get("p_id"));
//                System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_name = "+inParameters.get("p_name"));
//                System.out.println("[DimBillOfLandingPrintJdbcDao][InsertStoreProcedure][insert]: p_BL_ARRAY = "+inParameters.get("p_BL_ARRAY"));
//                Map outParameters = execute(inParameters);
//                if (conn != null) conn.close();
//            }catch(SQLException e){
//                System.out.println("Error >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
//            } 
            }catch(Exception e){
            	System.out.println("Error >>>>>>>>>>>>>>>>>>>>>>>>> "+e.getMessage());
            }
            
            return sessionIDPro;
        }          
    } 
 
    
    public String  getSessionId(String service  ,          String vessel,            String voyage,     String direction, 
                                                       String bl,                    String cocsoc,   
                                                       String pol_sts,             String dateFrom,      String dateTo,      String third,
                                                       String fsc,                     String userCode,     String sessionid,  String line,
                                                       String region,              String agent,             String fscCode,     String p_option,
                                                       String p_session_inv, String p_port_inv,     String chkFwdBl, String print)throws DataAccessException {
                                                         
    return insertStoreProcedure.insert(    service  ,         vessel,         voyage,     direction, 
                                                                         bl,                cocsoc,    
                                                                         pol_sts,            dateFrom,  dateTo,      third,
                                                                         fsc,                    userCode,  sessionid,  line,
                                                                         region,             agent,          fscCode,    p_option,
                                                                         p_session_inv,p_port_inv , chkFwdBl, print);
     }
    
    
    public String  getListBLSelPrint(String session, String userName,String[] blList)throws DataAccessException {
                                                         
    return insertStoreProcedureBlPriChk.insert(  session , userName , blList);
     }
    
    public Vector getListBLPrint(String session, String userName,String columnName , String ascDesc) throws DataAccessException{
        System.out.println("[DimBillOfLandingPrintJdbcDao][getListBLPrint]");
        System.out.println("[DimBillOfLandingPrintJdbcDao][getListBLPrint]: With status: sesseion "+session);
        System.out.println("[DimBillOfLandingPrintJdbcDao][getListBLPrint]: With status: userName "+userName);
        System.out.println("[DimBillOfLandingPrintJdbcDao][getListBLPrint]: With status: columnName "+columnName);
        System.out.println("[DimBillOfLandingPrintJdbcDao][getListBLPrint]: With status: ascDesc "+ascDesc);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
            
        Vector manifestPrintVec = new Vector();
        DimBillOfLandingPrintMod dimBillOfLandingPrintMod = null;
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]: ");
        System.out.println("[DimManifestBLPrintingJdbcDao][getListManifestPrint]: Finished");
        
        sql.append("SELECT DISTINCT  ");
        sql.append("   BLNO");
        sql.append(" , (SELECT CYNAME FROM DIM_BL_PRI_CUS WHERE CUSFLAG = 2 AND DIM_BL_PRI_CUS.BLNO = DIM_BL_PRI.BLNO AND ROWNUM = 1) SHIPPERNAME");
        sql.append(" ,  PLA_OF_ISSUE");
        sql.append(" , OFF_OR_FSC AS OFFICE");
        sql.append(" , BLCDTEC");
        sql.append(" , COCSOC ");
        sql.append(" ,  BLSTATUSDESC ");
        sql.append(" , DES_PRI ");
        sql.append(" , BLID ");   
        sql.append(" , POD ");   
        sql.append(" , SESSIONID ");
        sql.append(" , USERNAME ");
        sql.append(" , BL_TYPE ");
        sql.append(" FROM DIM_BL_PRI ");
        sql.append(" WHERE SESSIONID = :sesseion ");
        sql.append(" AND USERNAME = :userName ");
        sql.append( createSorting(columnName,ascDesc));
        
        System.out.println(sql.toString());
        
        map.put("sesseion",session);
        map.put("userName",userName);
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        while (rs.next()){
            dimBillOfLandingPrintMod = new DimBillOfLandingPrintMod();
            dimBillOfLandingPrintMod.setSessionId(RutString.nullToStr(rs.getString("SESSIONID")));
            dimBillOfLandingPrintMod.setBlNo(RutString.nullToStr(rs.getString("BLNO")));
            dimBillOfLandingPrintMod.setShipperName(RutString.nullToStr(rs.getString("SHIPPERNAME")));
            dimBillOfLandingPrintMod.setPla_of_issue(RutString.nullToStr(rs.getString("PLA_OF_ISSUE")));
            dimBillOfLandingPrintMod.setOffice(RutString.nullToStr(rs.getString("OFFICE")));
            dimBillOfLandingPrintMod.setBlcdtec(RutString.nullToStr(rs.getString("BLCDTEC")));
            dimBillOfLandingPrintMod.setCocsoc(RutString.nullToStr(rs.getString("COCSOC")));
            dimBillOfLandingPrintMod.setBlstatus(RutString.nullToStr(rs.getString("BLSTATUSDESC")));
            dimBillOfLandingPrintMod.setPrinted(RutString.nullToStr(rs.getString("DES_PRI")));
            dimBillOfLandingPrintMod.setBlId(RutString.nullToStr(rs.getString("BLID")));
            dimBillOfLandingPrintMod.setPod(RutString.nullToStr(rs.getString("POD")));
            dimBillOfLandingPrintMod.setUserName(RutString.nullToStr(rs.getString("USERNAME")));
            dimBillOfLandingPrintMod.setBlType(RutString.nullToStr(rs.getString("BL_TYPE")));
            
            manifestPrintVec.add(dimBillOfLandingPrintMod);
        }
        return manifestPrintVec;
        
    }
    
    private String createSorting(String columnName , String ascDesc){
        
    String sortBy = "";    
        
        if (columnName == null || columnName.equals("")){
            sortBy = " ORDER BY BLNO,BLCDTEC ";
        }else {
            sortBy = "ORDER BY  "+columnName;
        }
        
        if (ascDesc == null ){
            sortBy = sortBy + "  ASC";
        }else{
            sortBy = sortBy + "   "+ascDesc;
        }
        System.out.println(sortBy);
    return sortBy;
    }
    
    public List<DimBillOfLandingPrintExcelMod> generateExcelHeader(String userCode,String sessionid) throws DataAccessException{
        return generateExcelHeaderProcedure.generate(userCode,sessionid);
    }
    
    public List<DimBillOfLandingPrintExcelMod> generateExcelDetail(String userCode,String sessionid) throws DataAccessException{
        return generateExcelDetailProcedure.generate(userCode,sessionid);
    }
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException{
        return showHideGenExcelButtonProcedure.validateFsc(fsc);
    }
    
    protected class ShowHideGenExcelButtonProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_BL_PRINT.PR_CHK_SHOW_GEN_EXCEL_BUTTON";
    protected  ShowHideGenExcelButtonProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           
            declareParameter(new SqlOutParameter("P_O_V_RESULT", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            compile();

        }
    protected String validateFsc(String fsc) {         
        return validateFscPro(fsc);
    }
    protected String validateFscPro(String fsc) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            String result = "";
            
            try  {
                outMap = execute(inParameters);
                if (outMap.size() > 0) {
                   result = RutDatabase.dbToString(outMap, "P_O_V_RESULT");
                }
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return result;
        }          
    }
    
    protected class GenerateExcelHeaderProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_BL_PRINT.PR_GET_EXCEL_HDR";
    protected  GenerateExcelHeaderProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));
            compile();

        }
    protected List<DimBillOfLandingPrintExcelMod> generate(String userCode,String sessionid) {
                                                       
        return generateExcelHdrPro(userCode, sessionid);
    }
    protected List<DimBillOfLandingPrintExcelMod> generateExcelHdrPro(String userCode, String sessionid) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            List<DimBillOfLandingPrintExcelMod> returnList = new ArrayList<DimBillOfLandingPrintExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DimBillOfLandingPrintExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelHdrList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DimBillOfLandingPrintExcelMod bean = new DimBillOfLandingPrintExcelMod();

            bean.setVesselCallSign(rs.getString("LVES_NAME"));
            bean.setVoyageNo(rs.getString("LVES"));
            bean.setCarrierCode(rs.getString("LVOY"));
            bean.setPortOfDischargeCode(rs.getString("POD_CODE"));
            bean.setPortOfDischargeSuffix(rs.getString("POD_SUFFIX"));
            bean.setEstimateDateOfArrival(rs.getString("EST_DATE_ARRIVAL"));
            
            return bean;
        }
    }
    
    protected class GenerateExcelDetailProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DIM_BL_PRINT.PR_GET_EXCEL_DTL";
    protected  GenerateExcelDetailProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));
            compile();

        }
    protected List<DimBillOfLandingPrintExcelMod> generate(String userCode,String sessionid) {
                                                       
        return generateExcelDtlPro(userCode, sessionid);
    }
    protected List<DimBillOfLandingPrintExcelMod> generateExcelDtlPro(String userCode, String sessionid) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            List<DimBillOfLandingPrintExcelMod> returnList = new ArrayList<DimBillOfLandingPrintExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DimBillOfLandingPrintExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelDtlList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DimBillOfLandingPrintExcelMod bean = new DimBillOfLandingPrintExcelMod();

            bean.setCyOperator(rs.getString("OPERATOR_CODE"));
            bean.setRetentionClassification(rs.getString("RETENTION_CLASSIFICATION"));
            bean.setBlNo(rs.getString("BLNO"));
            bean.setPortOfLoadingCode(rs.getString("POL"));
            bean.setFinalDestinationCode(rs.getString("POD"));
            bean.setFinalDestinationName(rs.getString("POD_NAME"));
            bean.setPlaceOfTransferCode(rs.getString("POT"));
            bean.setPlaceOfTransferName(rs.getString("POT_NAME"));
            bean.setConsignorName(rs.getString("CONSIGNOR_NAME"));
            bean.setConsignorAddress(rs.getString("CONSIGNOR_ADDRESS"));
            bean.setConsignorPostCode(rs.getString("CONSIGNOR_POST_CODE"));
            bean.setConsignorCountryCode(rs.getString("CONSIGNOR_COUNTRY_CODE"));
            bean.setConsignorTelephoneNo(rs.getString("CONSIGNOR_TELEPHONE"));
            bean.setConsigneeName(rs.getString("CONSIGNEE_NAME"));
            bean.setConsigneeAddress(rs.getString("CONSIGNEE_ADDRESS"));
            bean.setConsigneePostCode(rs.getString("CONSIGNEE_POST_CODE"));
            bean.setConsigneeCountryCode(rs.getString("CONSIGNEE_COUNTRY_CODE"));
            bean.setConsigneeTelephoneNo(rs.getString("CONSIGNEE_TELEPHONE"));
            bean.setNotifyParty(rs.getString("NOTIFY_PARTY"));
            bean.setFreightCharges(rs.getString("FREIGHT_CHARGE"));
            bean.setFreightCurrencyCode(rs.getString("FREIGHT_CURRENCY_CODE"));
            bean.setTransship(rs.getString("TRANSSHIP"));
            bean.setTransshipReason(rs.getString("TRANSSHIP_REASON"));
            bean.setTransshipDuration(rs.getString("TRANSSHIP_DURATION"));
            bean.setRemarks(rs.getString("REMARKS"));
            bean.setDescriptionOfGoods(rs.getString("DESCRIPTION_GOODS"));
            bean.setSpecialCargoSign(rs.getString("SPECIAL_CARGO"));
            bean.setNumberOfPackage(rs.getString("PACKAGE_NUMBER"));
            bean.setUnitCodeOfPackage(rs.getString("PACKAGE_UNIT_CODE"));
            bean.setGrossWeight(rs.getString("GROSS_WEIGHT"));
            bean.setUnitCodeOfWeight(rs.getString("GROSS_WEIGHT_UNIT_CODE"));
            bean.setNetWeight(rs.getString("NET_WEIGHT"));
            bean.setUnitCodeOfNet(rs.getString("NET_WEIGHT_UNIT_CODE"));
            bean.setMeasurement(rs.getString("MEANSUREMENT"));
            bean.setUnitCodeOfMeasurement(rs.getString("MEANSUREMENT_UNIT_CODE"));
            bean.setMarksAndNos(rs.getString("MARKS_NOS"));
            bean.setContainerInformation(rs.getString("CONTAINER_INFORMATION"));
            
            return bean;
        }
    }
    
}
 
