package com.rclgroup.dolphin.web.dao.ijs;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.ijs.IjsJobOrderDao;
import com.rclgroup.dolphin.web.model.ijs.IjsJobOrderMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class IjsJobOrderJdbcDao extends RrcStandardDao implements IjsJobOrderDao{
    public IjsJobOrderJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String joNo) throws DataAccessException {
       System.out.println("[IjsJobOrderJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT JO_NO ");
       sql.append("FROM VR_IJS_JOB_ORDER_MASTER ");
       sql.append("WHERE JO_NO = :joNo");        
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("joNo", joNo));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[IjsJobOrderJdbcDao][isValid]: Finished");
        return isValid;
    }
      
    public boolean isValid(String joNo, String status) throws DataAccessException {
        System.out.println("[IjsJobOrderJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT JO_NO ");
        sql.append("FROM VR_IJS_JOB_ORDER_MASTER ");
        sql.append("WHERE JO_NO = :joNo");        
        sql.append("AND STATUS = :status ");
        
        HashMap map = new HashMap();
        map.put("joNo",joNo);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[IjsJobOrderJdbcDao][isValid]: Finished");
        return isValid;
    }   
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[IjsJobOrderJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT JO_NO ");
        sql.append("       ,JO_VER ");
        sql.append("       ,JO_TYPE_DESC ");
        sql.append("       ,VENDOR_CODE "); 
        sql.append("       ,JO_STATUS_DESC ");        
        sql.append("       ,RECORD_STATUS_DESC ");
        sql.append("FROM VR_IJS_JOB_ORDER_MASTER ");         
        sql.append(sqlCriteria);        
        System.out.println("[IjsJobOrderJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[IjsJobOrderJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("JN")){
                sqlCriteria = "WHERE JO_NO " + sqlWild;
            }else if(search.equalsIgnoreCase("JV")){
                sqlCriteria = "WHERE JO_VER " + sqlWild;
            }else if(search.equalsIgnoreCase("JT")){
                sqlCriteria = "WHERE JO_TYPE_DESC " + sqlWild;            
            }else if(search.equalsIgnoreCase("V")){
                sqlCriteria = "WHERE VENDOR_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("JS")){
                sqlCriteria = "WHERE UPPER(JO_STATUS_DESC) " + sqlWild;           
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE UPPER(RECORD_STATUS_DESC) " + sqlWild;
            }        
        }
        return sqlCriteria;
    }        
   
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    
    private IjsJobOrderMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        IjsJobOrderMod vendor = new IjsJobOrderMod();
        vendor.setVendorCode(RutString.nullToStr(rs.getString("VENDOR_CODE")));   
        vendor.setJoNo(RutString.nullToStr(rs.getString("JO_NO")));        
        vendor.setJoVer(RutString.nullToStr(rs.getString("JO_VER")));        
        vendor.setJoType(RutString.nullToStr(rs.getString("JO_TYPE_DESC")));        
        vendor.setJoStatus(RutString.nullToStr(rs.getString("JO_STATUS_DESC")));         
        vendor.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS_DESC")));
        
        return vendor;
    }  
    
}
