/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotAssignmentDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 01/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface EmsPickUpReturnDepotAssignmentDao {
     
    /**
     * list billing statement by header records for search screen
     * @param rowAt
     * @param rowTo
     * @param find 
     * @param findIn 
     * @param status
     * @param sortBy
     * @param sortByIn
     * @return list of Pick up return depot model(pickUpReturnDepotAssignmentMod)
     * @throws DataAccessException
     */
    public List listDepotForStatement(int rowAt, int rowTo, String find,String findIn,String status,String sortBy, String sortByIn) throws DataAccessException;
    
    /**
     * number of list of B/L
     * @param find 
     * @param findIn
     * @param status
     * @return list of Pick up return depot model(pickUpReturnDepotAssignmentMod)
     * @throws DataAccessException
     */
    public int countListDepotForStatement(String find,String findIn,String status) throws DataAccessException;
     
}
