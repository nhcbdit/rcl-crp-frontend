/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotAssignmentJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 01/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.dao.ems.EmsPickUpReturnDepotAssignmentDao;
import com.rclgroup.dolphin.web.model.ems.EmsPickUpReturnDepotAssignmentMod;
import com.rclgroup.dolphin.web.model.ems.EmsPickUpReturnDepotMaintenanceMod;
import com.rclgroup.dolphin.web.model.far.FarFunlocMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class EmsPickUpReturnDepotAssignmentJdbcDao extends RrcStandardDao implements EmsPickUpReturnDepotAssignmentDao{
    
    public EmsPickUpReturnDepotAssignmentJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();                
    }                                         
    
    public List listDepotForStatement(int rowAt, int rowTo,String find,String in,String status,String sortBy, String sortByIn) {
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: Started");              
        String sqlColumn = "DEPOT_CODE, DEPOT_NAME, FSC, POINT_CODE, STATUS";
        StringBuffer sql = new StringBuffer();

        sql.append("select DEPOT_CODE");
        sql.append("    ,DEPOT_NAME ");
        sql.append("    ,FSC ");
        sql.append("    ,POINT_CODE ");
        sql.append("    ,STATUS ");        
        sql.append("from VR_EMS_DEPOT_MASTER ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");        
        if (!RutString.isEmptyString(find)) {
            if(in.equalsIgnoreCase("DC")){
                strWhere.append(" and DEPOT_CODE = '"+find+"' ");
            }else if(in.equalsIgnoreCase("DN")){
                strWhere.append(" and DEPOT_NAME like '%"+find+"%' ");
            }else if(in.equalsIgnoreCase("F")){
                strWhere.append(" and FSC = '"+find+"' ");
            }else if(in.equalsIgnoreCase("PC")){
                strWhere.append(" and POINT_CODE = '"+find+"' ");               
            }            
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and STATUS = '"+status+"' ");
        }
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        // Query by record number range
        sql.append(createSqlCriteriaSortBy(sortBy, sortByIn));
        
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: SQL ="+sql.toString());
        sql = this.addSqlForNewHelp(sql, rowAt, rowTo, sqlColumn);
        
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: SQL new ="+sql.toString());
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           EmsPickUpReturnDepotAssignmentMod ff = new EmsPickUpReturnDepotAssignmentMod();
                           ff.setDepotCode(RutString.nullToStr(rs.getString("DEPOT_CODE")));
                           ff.setDepotName(RutString.nullToStr(rs.getString("DEPOT_NAME")));
                           ff.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           ff.setPointCode(RutString.nullToStr(rs.getString("POINT_CODE")));
                           ff.setStatus(RutString.nullToStr(rs.getString("STATUS")));                           
                           return ff;
                    }
                });
    }
    
    public int countListDepotForStatement(String find,String in,String status) {
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][countListDepotForStatementByHeader]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("select DEPOT_CODE");
        sql.append("    ,DEPOT_NAME ");
        sql.append("    ,FSC ");
        sql.append("    ,POINT_CODE ");
        sql.append("    ,STATUS ");        
        sql.append("from VR_EMS_DEPOT_MASTER ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(find)) {
                if(in.equalsIgnoreCase("DC")){
                    strWhere.append(" and DEPOT_CODE = '"+find+"' ");
                }else if(in.equalsIgnoreCase("DN")){
                    strWhere.append(" and DEPOT_NAME like '%"+find+"%' ");
                }else if(in.equalsIgnoreCase("F")){
                    strWhere.append(" and FSC = '"+find+"' ");
                }else if(in.equalsIgnoreCase("PC")){
                    strWhere.append(" and POINT_CODE = '"+find+"' ");               
                }            
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and STATUS = '"+status+"' ");
        }
         
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
         
        // Find a number of all data
        sql = this.getNumberOfAllData(sql);
        
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][countListDepotForStatementByHeader]: SQL ="+sql.toString());
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][countListDepotForStatementByHeader]: Finished");
        Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(), new HashMap(), Integer.class);
        return result.intValue();
    }
    
    private String createSqlCriteriaSortBy (String sortBy, String sortByIn) {
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][createSqlCriteriaSortBy]: Started");
        String sqlCriteria = "";
        String sqlSortByIn = "";
        if (!RutString.isEmptyString(sortBy)) {
            if ("A".equals(sortByIn)) {
                sqlSortByIn = "ASC";
            } else if ("D".equals(sortByIn)) {
                sqlSortByIn = "DESC";
            }
            
            Map map = new HashMap();
            map.put("DC", "DEPOT_CODE");
            map.put("DN", "DEPOT_NAME");
            map.put("F", "FSC");
            map.put("PC", "POINT_CODE");            
            
            sqlCriteria = " order by "+ map.get(sortBy)+" "+sqlSortByIn;
        }
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][createSqlCriteriaSortBy]: sql = "+sqlCriteria);
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][createSqlCriteriaSortBy]: Finished");
        return sqlCriteria;
    }
    
}
