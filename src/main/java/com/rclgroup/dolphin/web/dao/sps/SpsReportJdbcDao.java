/*-----------------------------------------------------------------------------------------------------------  
SpsReportJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 11/03/11 API         BUG.469       Change prepare data for ETA date From, To  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.sps;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.sps.SpsForecastBookingMod;
import com.rclgroup.dolphin.web.util.RutDate;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class SpsReportJdbcDao extends RrcStandardDao implements SpsReportDao  {
    private GenerateNoBookingForForcastReportStoreProcedure generateNoBookingForForcastReportStoreProcedure;
    private DeleteNoBookingForForcastReportStoreProcedure   deleteNoBookingForForcastReportStoreProcedure;
    
    public SpsReportJdbcDao() {
        super();
    }

    protected void initDao() throws Exception {
        super.initDao();
        generateNoBookingForForcastReportStoreProcedure = new GenerateNoBookingForForcastReportStoreProcedure(getJdbcTemplate());
        deleteNoBookingForForcastReportStoreProcedure   = new DeleteNoBookingForForcastReportStoreProcedure(getJdbcTemplate());
        
    }
    
    /*  Start Function For Delete  */
    public boolean deleteAttributeSession(RrcStandardMod mod) {
        return deleteNoBookingForForcastReportStoreProcedure.generate(mod);
    }
     
    protected class DeleteNoBookingForForcastReportStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_SPS_RPT.PRR_DEL_SPS101_FORECAST_BKG";
        
        protected DeleteNoBookingForForcastReportStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            compile();
        }

        private boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof SpsForecastBookingMod && outputMod instanceof SpsForecastBookingMod) {
                SpsForecastBookingMod aInputMod = (SpsForecastBookingMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    /*  Start Function For Gererate  */
    public boolean generateSummaryStatus(RrcStandardMod mod) {
        return generateNoBookingForForcastReportStoreProcedure.generate(mod);
    }

    protected class GenerateNoBookingForForcastReportStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_SPS_RPT.PRR_GEN_SPS101_FORECAST_BKG";

        protected GenerateNoBookingForForcastReportStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_direction", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_eta_fr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_eta_to", Types.VARCHAR));
            compile();
        }

        private boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof SpsForecastBookingMod && outputMod instanceof SpsForecastBookingMod) {
                SpsForecastBookingMod aInputMod = (SpsForecastBookingMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUserName());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_direction", aInputMod.getDirection());
                inParameters.put("p_pol", aInputMod.getPol());
                
                //##01 BEGIN
                // prepare data
                String strDateFrom = RutDate.dateToStr(aInputMod.getFromDate());
                String strDateTo = RutDate.dateToStr(aInputMod.getToDate());                               
                inParameters.put("p_eta_fr", strDateFrom);
                inParameters.put("p_eta_to", strDateTo);
                //##01 END
                
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_username   = "+inParameters.get("p_username"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_service    = "+inParameters.get("p_service"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_vessel     = "+inParameters.get("p_vessel"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_voyage     = "+inParameters.get("p_voyage"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_direction  = "+inParameters.get("p_direction"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_pol        = "+inParameters.get("p_pol"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_eta_fr     = "+inParameters.get("p_eta_fr"));
                System.out.println("[SpsReportJdbcDao][GenerateNoBookingForForcastReportStoreProcedure]: p_eta_to     = "+inParameters.get("p_eta_to"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }

}
