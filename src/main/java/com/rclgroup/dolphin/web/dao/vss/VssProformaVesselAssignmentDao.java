/*-----------------------------------------------------------------------------------------------------------  
VssProformaVesselAssignmentDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 18/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.vss;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface VssProformaVesselAssignmentDao extends RriStandardDao {
    
    /**
     * find voyage proforma vessel assignment by using VSS proforma ID, and record status 
     * @param vssProformaId VSS proforma ID
     * @param recordStatus record status of returned records
     * @return List of VSS proforma vessel assignment models
     * @throws DataAccessException
     */
    public List findByVssProformaIdRecordStatus(String vssProformaId,String recordStatus) throws DataAccessException;
    
    /**
     * find bsa vessel type by using VSS proforma ID, and record status 
     * @param vssProformaId VSS proforma ID
     * @param recordStatus record status of returned records
     * @return List of BSA vessel type strings
     * @throws DataAccessException
     */
    public List findBsaVesselTypeByVssProformaId(String vssProformaId, String recordStatus) throws DataAccessException;

    /**
     * insert a proforma vessel assignment record
     * @param mod a VSS proforma vessel assignment model as input and output
     * @return whether insertion is successful
     * @throws DataAccessException exception which client has to catch all following error messages: 
     *                             error message: VSS_PVA01_VSS_VESSEL_ASSIGNMENT_ID_REQ
     *                             error message: VSS_PVA01_VSS_PROFORMA_ID_REQ
     *                             error message: VSS_PVA01_VESSEL_SEQ_NO_REQ
     *                             error message: VSS_PVA01_VESSEL_CODE_REQ
     *                             error message: VSS_PVA01_VESSEL_CODE_NOT_EXIST with one argument: vessel code from input value  
     *                             error message: VSS_PVA01_BSA_VESSEL_TYPE_REQ
     *                             error message: VSS_PVA01_VALID_FROM_REQ
     *                             error message: VSS_PVA01_VALID_TO_REQ
     *                             error message: VSS_PVA01_VALID_DATE_NOT_IN_RANGE 
     *                             error message: VSS_PVA01_RECORD_STATUS_REQ
     *                             error message: VSS_PVA01_STATUS_NOT_IN_RANGE with one argument: record status from input value   
     *                             error message: VSS_PVA01_RECORD_ADD_USER_REQ
     *                             error message: VSS_PVA01_VALUE_DUP with one argument: primary key from input value  
     *                             error message: ORA-XXXXX (un-exceptional oracle error) 
     */
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * update a proforma vessel assignment record
     * @param mod a VSS proforma vessel assignment model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages: 
     *                             error message: VSS_PVA01_VSS_VESSEL_ASSIGNMENT_ID_REQ
     *                             error message: VSS_PVA01_VSS_PROFORMA_ID_REQ
     *                             error message: VSS_PVA01_VESSEL_SEQ_NO_REQ
     *                             error message: VSS_PVA01_VESSEL_CODE_NOT_EXIST with one argument: vessel code from input value  
     *                             error message: VSS_PVA01_BSA_VESSEL_TYPE_REQ
     *                             error message: VSS_PVA01_VALID_FROM_REQ
     *                             error message: VSS_PVA01_VALID_TO_REQ
     *                             error message: VSS_PVA01_VALID_DATE_NOT_IN_RANGE 
     *                             error message: VSS_PVA01_RECORD_STATUS_REQ
     *                             error message: VSS_PVA01_STATUS_NOT_IN_RANGE with one argument: record status from input value    
     *                             error message: VSS_PVA01_RECORD_CHANGE_USER_REQ
     *                             error message: VSS_PVA01_RECORD_CHANGE_DATE_REQ
     *                             error message: VSS_PVA01_UPDATE_CON
     *                             error message: VSS_PVA01_PRFRM_VSSL_ASSGN_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean update(RrcStandardMod mod) throws DataAccessException;

    /**
     * delete a proforma vessel assignment record
     * @param mod a VSS proforma vessel assignment model
     * @return whether deletion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: VSS_PVA01_RECORD_CHANGE_DATE_REQ 
     *                             error message: VSS_PVA01_DELETE_CON  
     *                             error message: VSS_PVA01_PRFRM_VSSL_ASSGN_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * check constraint on a proforma vessel assignment record
     * @param mod a VSS proforma vessel assignment model
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: VSS_PVA01_VESSEL_CODE_OVERLAP_DATE with one argument: vessel code from input value
     *                             error message: VSS_PVA01_VESSEL_SEQ_NO_OVERLAP_DATE with one argument: vessel seq. no from input value  
     *                             error message: VSS_PVA01_VESSEL_SEQ_NO_NOT_SAME_TYPE with one argument: vessel seq. no from input value
     *                             error message: VSS_PVA01_VESSEL_RANGE_NOT_IN_PROFORMA_RANGE with two argument: vessel seq. no and vessel code from input value 
     *                             error message: VSS_PVA01_VESSEL_SEQ_NO_NOT_VALID with two argument: vessel seq. no and vessel code from input value
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException;
    
}



