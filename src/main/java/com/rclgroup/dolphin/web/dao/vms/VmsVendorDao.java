package com.rclgroup.dolphin.web.dao.vms;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface VmsVendorDao {
    /**
     * check valid of vendorCode#
     * @param vendorCode     
     * @return valid of vendorCode
     * @throws DataAccessException
     */
    public boolean isValid(String vendorCode) throws DataAccessException;
        
    /**
     * list Vendor records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Vendor
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
}
