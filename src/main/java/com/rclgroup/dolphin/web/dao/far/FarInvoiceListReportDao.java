/*-----------------------------------------------------------------------------------------------------------  
FarInvoiceListReportDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Leena Babu 21/07/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface FarInvoiceListReportDao {
    /**
     * insert a invoice report record to temporary table
     * @param mod a invoicelist model as input and output
     * @return whether insert is successful
     * @throws DataAccessException exceptin which client has to catch all  error messages:
     */
    public boolean insert (RrcStandardMod mod) throws DataAccessException;
}
