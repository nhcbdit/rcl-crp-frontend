package com.rclgroup.dolphin.web.dao.sps;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface SpsReportDao {

   
    /**
     * @param mod
     * @return whether delete is successful
     * @throws DataAccessException
     */ 
    public boolean deleteAttributeSession(RrcStandardMod mod) throws DataAccessException;
    
   
   
    /**
     * generate a SPS Forecast Booing: Summary by Status (USD) record
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateSummaryStatus(RrcStandardMod mod) throws DataAccessException;
    
    

}
