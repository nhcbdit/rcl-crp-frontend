/*------------------------------------------------------
QtnFreightSurchargeAuditJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Manop Wanngam 15/10/07  
- Change Log -------------------------------------------
## DD/MM/YY     �User-      -TaskRef-                       -ShortDescription-
01 12/12/13     RAJA        PD_CR_20131129                  Modify Freight Surcharge Audit Report
02 25/04/13     NUTTAPOL    Bug ID 1019                     Fix in case of more than 1 cntr size/type
03 06/05/14     RAJA        PD_CR_20140311                  Ancillary Charge Summary Report
04 25/05/15     SARAWUT     Ticket No 427773                Select more fileds idp_013.BSEFUL and idp_013.killed_slots
05 16/01/17     ONSINEE     Ticket No                       Add Column O/Frt(Usd) Generate Excel and Modify Script Column oFrt
06 15/03/18     NATTAK1                                     Single subsequence row on columns Laden / Void slot on BL:BLWSB18000280
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.bkg.BkgBookingMod;
import com.rclgroup.dolphin.web.model.qtn.QtnFreightSurchargeAuditReportMod;


import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.ResultSet;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import java.util.Collections;

public class QtnFreightSurchargeAuditJdbcDao extends RrcStandardDao implements QtnFreightSurchargeAuditDao{
    private QtnFreightSurchargeAuditInsertBlStoreProcedure qtnFreightSurchargeAuditInsertBlStoreProcedure;
    private QtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure qtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure;
    
    public QtnFreightSurchargeAuditJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        qtnFreightSurchargeAuditInsertBlStoreProcedure = new QtnFreightSurchargeAuditInsertBlStoreProcedure(getJdbcTemplate());
        qtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure = new QtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure(getJdbcTemplate());
    }
    
    public boolean generateTempTable(RrcStandardMod mod) throws DataAccessException{
        return qtnFreightSurchargeAuditInsertBlStoreProcedure.generate(mod);
    }
    public boolean generateSurchargeCodeTempTable(RrcStandardMod mod) throws DataAccessException{
        return qtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure.generate(mod);
    }
    
    protected class QtnFreightSurchargeAuditInsertBlStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_QTN_FRE_SUR_AUD.PRR_GEN_QTN_106_BL";
        
        protected QtnFreightSurchargeAuditInsertBlStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_from_date", Types.INTEGER));
            declareParameter(new SqlParameter("p_to_date", Types.INTEGER));
            declareParameter(new SqlParameter("p_coc_soc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_line", Types.VARCHAR));
            declareParameter(new SqlParameter("p_region", Types.VARCHAR));
            declareParameter(new SqlParameter("p_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_ter", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            // start : 03
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod_ter", Types.VARCHAR));
            declareParameter(new SqlParameter("p_eta_from_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_eta_to_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_search_by", Types.VARCHAR)); // For region user.
            // end : 03
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof QtnFreightSurchargeAuditReportMod && outputMod instanceof QtnFreightSurchargeAuditReportMod) {
                QtnFreightSurchargeAuditReportMod aInputMod = (QtnFreightSurchargeAuditReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUserId());
                inParameters.put("p_from_date", aInputMod.getEtdFrom());
                inParameters.put("p_to_date", aInputMod.getEtdTo());
                inParameters.put("p_coc_soc", aInputMod.getCocSoc());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_line", aInputMod.getLine());
                inParameters.put("p_region", aInputMod.getRegion());
                inParameters.put("p_agent", aInputMod.getAgent());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_pol", aInputMod.getPol());
                inParameters.put("p_pol_ter", aInputMod.getPolTerminal());
                inParameters.put("p_bl", null);
                inParameters.put("p_dir", aInputMod.getDirection());
                // start : 03
                inParameters.put("p_pod", aInputMod.getPod());
                inParameters.put("p_pod_ter", aInputMod.getPodTerminal());
                inParameters.put("p_eta_from_date", aInputMod.getEtaFrom());
                inParameters.put("p_eta_to_date", aInputMod.getEtaTo());
                inParameters.put("p_search_by", aInputMod.getReportBy()); // for region user.
               
                // end : 03
                
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_username = "+inParameters.get("p_usernane"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_from_date = "+inParameters.get("p_from_date"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_to_date = "+inParameters.get("p_to_date"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_coc_soc = "+inParameters.get("p_coc_soc"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_service = "+inParameters.get("p_service"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_line = "+inParameters.get("p_line"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_region = "+inParameters.get("p_region"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_agent = "+inParameters.get("p_agent"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_pol = "+inParameters.get("p_pol"));
                //System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_pol_terminal = "+inParameters.get("p_pol_ter2"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_bl = "+inParameters.get("p_bl"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_dir = "+inParameters.get("p_dir"));
                
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_pod = "+inParameters.get("p_pod"));
               // System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_pod_ter = "+inParameters.get("p_pod_ter"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_eta_from_date = "+inParameters.get("p_eta_from_date"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_eta_to_date = "+inParameters.get("p_eta_to_date"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateTempTable]: p_search_by = "+inParameters.get("p_search_by"));// for region user
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class QtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_QTN_FRE_SUR_AUD.PRR_GEN_QTN_106_SUR_CODE";
        
        protected QtnFreightSurchargeAuditInsertSurchargeCodeStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_surcharge_code", Types.VARCHAR));
            compile();
        }
    
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof QtnFreightSurchargeAuditReportMod && outputMod instanceof QtnFreightSurchargeAuditReportMod) {
                QtnFreightSurchargeAuditReportMod aInputMod = (QtnFreightSurchargeAuditReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUserId());
                inParameters.put("p_surcharge_code", aInputMod.getSurchargeCode());
                
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateSurchargeCodeTempTable]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateSurchargeCodeTempTable]: p_username = "+inParameters.get("p_usernane"));
                System.out.println("[QtnFreightSurchargeAuditJdbcDao][generateSurchargeCodeTempTable]: p_surcharge_code = "+inParameters.get("p_surcharge_code"));
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    // Start: 03 many things changed in this query 
    public List searchQtnFreightSurchargeAuditData(RrcStandardMod mod) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(mod);
        StringBuffer sql = new StringBuffer();
        sql.append("select result.*,dense_rank() over (PARTITION BY result.BL#,result.commodity ORDER BY rownum) rank from( ");
        sql.append("select ");
        sql.append("etd_date \n");
        sql.append(",eta_date \n");
        sql.append(",QUOTATION# \n");
        sql.append(",contract_party \n ");
        sql.append(",shipper_name \n ");
        sql.append(",cnee_name\n ");
        sql.append(",BL#\n ");
        sql.append(",svc\n ");
        sql.append(",vsl\n ");
        sql.append(",voy\n");
        sql.append(",por\n ");
        sql.append(",pol\n ");
        sql.append(",pod\n");
        sql.append(",del_\n");
        sql.append(",container_size\n");
        sql.append(",container_type\n");
        // sql.append(",weight\n");
        sql.append(",commodity_group\n");
        sql.append(",commodity_group_Desc\n");
        sql.append(",commodity\n");
        sql.append(",commodity_desc\n");
        sql.append(",case when container_size <> 0 then laden_empty  else '' end laden_empty\n");
        sql.append(",shipment_term\n");
        sql.append(",volumn_unit\n");
        sql.append(",volumn_teus\n");
        sql.append(",O_Frt\n");
        sql.append(",O_FrtUsd\n"); //##05
        sql.append(",currency\n");
        sql.append(",surcharge_code\n");
        sql.append(",rate\n");
        sql.append(",amount\n");
        sql.append(",amount_usd\n");
        sql.append(",GYITEM \n");
        sql.append(",LADEN \n");
        sql.append(",VOIDSLOT \n");
        //sql.append(",rank\n");
        
        sql.append("from(select ");
        sql.append("t_data.etd_date etd_date\n");
        sql.append(",t_data.eta_date eta_date\n");
        sql.append(",t_data.QUOTATION_NO QUOTATION#\n");
        sql.append(",t_data.cust_func_contract_party_name contract_party\n");
        sql.append(",t_data.cust_func_shipper_name shipper_name\n");
        sql.append(",t_data.cust_func_cnee_name cnee_name\n");
        sql.append(",t_data.blno BL#\n");
        sql.append(",t_data.cur_srvc svc\n");
        sql.append(",t_data.aymves vsl\n");
        sql.append(",t_data.aymvoy voy\n");
        sql.append(",t_data.AYORIG por\n");
        sql.append(",t_data.AYMPOL pol\n");
        sql.append(",t_data.AYMPOD pod\n");
        sql.append(",t_data.AYDEST del_\n");
        sql.append(",t_data.EQ_SIZE container_size\n");
        sql.append(",t_data.EQ_TYPE container_type\n");
        //sql.append(",to_char(t_data.weight,'FM9999999.90') weight \n"); // 03
       // sql.append(",case when t_data.rank=1 then t_data.weight  else 0 end weight\n"); // 03
        sql.append(",t_data.commodity_group\n");
        sql.append(",t_data.commodity_group_Desc\n");
        sql.append(",t_data.commodity_code commodity\n");
        sql.append(",t_data.commodity_desc commodity_desc\n");//##01
        //sql.append("--,t_data.commodity_group");
        sql.append(",case when t_data.EQ_SIZE <> 0 then t_data.MTY_FULL_FLAG  else '' end laden_empty"); // t_data.MTY_FULL_FLAG laden_empty
        sql.append(",t_data.AYMODE shipment_term\n");
        sql.append(",t_data.unit volumn_unit\n");
        sql.append(",RCLAPPS.PCR_QTN_FRE_SUR_AUD.FR_GEN_QTN_106_TEU(t_data.blno,t_data.EQ_SIZE,t_data.EQ_TYPE,t_data.unit)volumn_teus--teus\n");
        //sql.append(",'USD'||' '||nvl(t_data.O_Frt,'0') O_Frt\n"); //01
        sql.append(",nvl(t_data.O_Frt,'0') O_Frt\n");  //##5 
        sql.append(",'USD'||' '||nvl(t_data.O_FrtUsd,'0') O_FrtUsd\n"); //##5
        sql.append(",t_data.curr currency\n");
        sql.append(",t_data.surcharge_code surcharge_code\n");
        sql.append(",t_data.rate rate\n");
        sql.append(",to_char(t_data.amount,'FM9999999999999.90') amount \n"); //t_data.amount,'FM9999999.90'
        //sql.append(",to_char(t_data.amount_usd,'FM9999999.90') amount_usd \n"); //t_data.amount,'FM9999999.90'
        sql.append(",t_data.amount_usd amount_usd \n"); //t_data.amount,'FM9999999.90'
        sql.append(",t_data.GYITEM \n");
        sql.append(",t_data.LADEN \n");
        sql.append(",t_data.VOIDSLOT \n");
        //sql.append(",dense_rank() over (PARTITION BY t_data.blno,t_data.commodity_code ORDER BY rownum) rank\n");
        //sql.append(",rtrim (xmlagg (xmlelement (e,t_data.curr || ',')).extract ('//text()'), ',') currency\n");
        //sql.append(",rtrim (xmlagg (xmlelement (e,t_data.surcharge_code || ',')).extract ('//text()'), ',') surcharge_code\n");
        //sql.append(",rtrim (xmlagg (xmlelement (e,t_data.rate || ',')).extract ('//text()'), ',') rate\n");
        //sql.append(",count(*)count_surcharge_code\n");
        
        sql.append("from\n");
        sql.append("(\n");
        sql.append("    select ");
        sql.append("    idp_010.AYMSLD etd_date\n");
        sql.append("    ,idp_010.AYSARD eta_date\n");
        sql.append("    ,idp_010.QUOTATION_NO QUOTATION_NO\n");
        sql.append("    ,idp_010.cur_srvc cur_srvc\n");
        sql.append("    ,idp_010.aymves aymves\n");
        sql.append("    ,idp_010.aymvoy aymvoy\n");
        sql.append("    ,idp_010.AYORIG AYORIG\n");
        sql.append("    ,idp_010.AYMPOL AYMPOL\n");
        sql.append("    ,idp_010.AYMPOD AYMPOD\n");
        sql.append("    ,idp_010.AYDEST AYDEST\n");
        sql.append("    ,idp_010.AYMODE AYMODE\n");
        sql.append("    ,idp_070.GYBLNO blno\n");
        sql.append("    ,idp_070.gyeqsz EQ_SIZE\n");
        sql.append("    ,idp_070.gyeqtp EQ_TYPE\n");
        //sql.append("    ,idp_010.aymtwt weight\n"); //sql.append("    ,idp_010.aymtwt weight\n");
        //sql.append("    ,(idp_070.gyeqsz/20)*idp_070.GYFRUN EQ_TEU--teus*unit\n");
        sql.append("    ,RCLAPPS.PCR_QTN_FRE_SUR_AUD.FR_GEN_QTN_106_UNIT(idp_070.GYBLNO,idp_070.gyeqsz,idp_070.gyeqtp)unit\n");
        sql.append("    ,(idp_070.gyeqsz/20)*\n");
        sql.append("    (\n");
        sql.append("    select count(*)\n");
        sql.append("        from IDP055 i055--B/L Containers\n");
        sql.append("        where 1=1\n");
        sql.append("        and i055.EYEQSZ=idp_070.gyeqsz\n");
        sql.append("        and i055.EYEQTP=idp_070.gyeqtp\n");
        sql.append("       and i055.EYBLNO=idp_070.GYBLNO\n");
        sql.append("    )EQ_TEU--teus*unit\n");
        //sql.append("    ,(\n");
       // sql.append("        select eymtwt \n");
        //sql.append("        from IDP055 i055--B/L Containers\n");
        //sql.append("        where 1=1 and rownum=1\n");
       // sql.append("        and i055.EYEQSZ=idp_070.gyeqsz\n");
       // sql.append("        and i055.EYEQTP=idp_070.gyeqtp\n");
        //sql.append("       and i055.EYBLNO=idp_070.GYBLNO\n");
       // sql.append("    )weight--teus*unit\n");
        //sql.append("    /*,(select itp_080.FCDESC ");
        //sql.append("        from sealiner.ITP080 itp_080--Commodity Group Master ");
        //sql.append("        where itp_080.FCCODE=idp_050.BYCMCD-- Commodity Code");
        //sql.append("     )commodity_group*/");
       /* ##01 sql.append("    ,(\n");
        sql.append("        CASE  idp_070.GYRETP--Rate type\n");
        sql.append("          WHEN 'O0' THEN --OOG\n");
        sql.append("            (\n");
        sql.append("                -- base frt \n");
        sql.append("                (select nvl(idp_070_1.GYFRRT,0)--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where idp_070_1.GYRETP='O0'--OOG\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("                )+\n");
        sql.append("                --void slot\n");
        sql.append("                (select nvl(idp_070_1.GYFRRT,0)--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where idp_070_1.GYRETP='O7'--void slot\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("                )\n");
        sql.append("            )\n");
        sql.append("          ELSE \n");
        sql.append("            (\n");
        sql.append("                select sum(nvl(idp_070_1.GYFRRT,0))--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where 1=1\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        //sql.append("              RCLAPPS.PCR_QTN_FRE_SUR_AUD.FR_GET_OCEAN_FREIGHT_AMT(idp_070.GYBLNO,idp_070.gyeqsz,idp_070.gyeqtp)--type\n");
        sql.append("            )\n");
        sql.append("        END\n");
        sql.append("    ) O_Frt\n"); ##01 */ 
        // BEGIN ##05 
        sql.append("    ,(\n");
        sql.append("               ( SELECT distinct i7.GYCURR FROM  IDP070 i7 WHERE i7.gyfors='F'\n");
        sql.append("                     AND i7.GYBLNO=idp_070.GYBLNO AND i7.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     AND i7.gyeqtp=idp_070.gyeqtp ) || ' ' || CASE  idp_070.GYRETP--Rate type\n");
        sql.append("          WHEN 'O0' THEN --OOG\n");
        sql.append("            (\n");
        sql.append("                -- base frt \n");
        sql.append("                (select nvl(idp_070_1.GYFRRT,0)--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where idp_070_1.GYRETP='O0'--OOG\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("                )+\n");
        sql.append("                --void slot\n");
        sql.append("                (select nvl(idp_070_1.GYFRRT,0)--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where idp_070_1.GYRETP='O7'--void slot\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("                )\n");
        sql.append("            )\n");
        sql.append("          ELSE \n");
        sql.append("            (\n");
        sql.append("                select sum(nvl(idp_070_1.GYFRRT,0))--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where 1=1\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("            )\n");
        sql.append("        END\n");
        sql.append("    ) O_Frt\n"); 
        sql.append("    ,(\n");
        sql.append("         CASE  idp_070.GYRETP--Rate type\n");
        sql.append("          WHEN 'O0' THEN --OOG\n");
        sql.append("            (\n");
        sql.append("                -- base frt \n");
        sql.append("                (select nvl(idp_070_1.GYFRRT * idp_070_1.GYSCCN, 0)--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where idp_070_1.GYRETP='O0'--OOG\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("                )+\n");
        sql.append("                --void slot\n");
        sql.append("                (select nvl(idp_070_1.GYFRRT * idp_070_1.GYSCCN,0)--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where idp_070_1.GYRETP='O7'--void slot\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("                )\n");
        sql.append("            )\n");
        sql.append("          ELSE \n");
        sql.append("            (\n");
        sql.append("                select sum(nvl(idp_070_1.GYFRRT * idp_070_1.GYSCCN,0))--rate\n");
        sql.append("                     from idp070 idp_070_1--B/L Charges Details\n");
        sql.append("                     where 1=1\n");
        sql.append("                     and idp_070_1.gyfors='F'--freight\n");
        sql.append("                     and idp_070_1.GYBLNO=idp_070.GYBLNO\n");
        sql.append("                     and idp_070_1.gyeqsz=idp_070.gyeqsz--size\n");
        sql.append("                     and idp_070_1.gyeqtp=idp_070.gyeqtp--type\n");
        sql.append("            )\n");
        sql.append("        END\n");
        sql.append("    ) O_FrtUsd\n"); 
        // END ##05 
        sql.append("    ,idp_070.gyfrcd surcharge_code\n");
        sql.append("    ,idp_070.GYFRRT rate\n");
        sql.append("    ,decode(NVL (idp_070.gyscam, 0), 0,idp_070.GYFRRT*idp_070.GYFRUN,idp_070.gyscam) amount\n");//GYFRAM
        //sql.append("    ,idp_070.gyscam amount_usd \n");//GYFRAM
        sql.append("    ,case when nvl(idp_070.gyscam,0)=0 then '' else  'USD'||idp_070.gyscam end amount_usd  \n");//GYFRAM
        sql.append("    ,(\n");
        sql.append("        select idp_030.cyname \n");
        sql.append("        from rclapps.IDP030 idp_030--B/L Customer\n");
        sql.append("        where 1=1\n");
        sql.append("        and idp_030.CYBLNO=idp_010.ayblno\n");
        sql.append("        and idp_030.CYRCTP='C'\n");
        sql.append("        and rownum=1\n");
        sql.append("    )cust_func_cnee_name\n");
        sql.append("    ,(\n");
        sql.append("        select idp_030.cyname\n ");
        sql.append("        from rclapps.IDP030 idp_030--B/L Customer\n");
        sql.append("        where 1=1\n");
        sql.append("        and idp_030.CYBLNO=idp_010.ayblno\n");
        sql.append("        and idp_030.CYRCTP='S'\n");
        sql.append("        and rownum=1\n");
        sql.append("    )cust_func_shipper_name\n");
        sql.append("    ,(\n");
        sql.append("        select idp_030.cyname \n");
        sql.append("        from rclapps.IDP030 idp_030--B/L Customer\n");
        sql.append("        where 1=1\n");
        sql.append("        and idp_030.CYBLNO=idp_010.ayblno\n"); 
        sql.append("        and idp_030.CYRCTP='O'\n"); // and idp_030.CYRCTP='P'\n" to and idp_030.CYRCTP='O'\n"
        sql.append("        and rownum=1\n");
        sql.append("    )cust_func_contract_party_name\n");
        sql.append("    ,idp_070.GYCURR Curr\n");
        sql.append("    ,(\n");
        sql.append("    select comm_group.COMMODITY_GROUP_CODE commodity_group from ITP080 commom,commodity_group comm_group \n");
        sql.append("    where commom.COMMODITY_GROUP_CODE=comm_group.COMMODITY_GROUP_CODE  and commom.FCCODE=idp_050.BYCMCD \n");
        sql.append("    )commodity_group \n");
        sql.append("    ,(\n");
        sql.append("   select comm_group.DESCRIPTION commodity_Desc from ITP080 commom,commodity_group comm_group \n");
        sql.append("     where commom.COMMODITY_GROUP_CODE=comm_group.COMMODITY_GROUP_CODE and commom.FCCODE=idp_050.BYCMCD \n");
        sql.append("    )commodity_group_Desc \n");
        sql.append("    ,idp_050.BYCMCD commodity_code\n");
        sql.append("    ,idp_050.BYRMKS commodity_desc\n");//##01
        //#02 BEGIN
        //sql.append("     ,(select decode(idp_013.bseemt,0,'F','M')--bseemt=empty,bseful=full eq\n");
        sql.append("    ,(select decode(idp_070.gyeqsz,'0',' ',decode(sum(idp_013.bseemt),0,'F','M'))--bseemt=empty,bseful=full eq\n");
        //#02 END
        sql.append("          from sealiner.idp013 idp_013--B/L Shipment Summary\n");
        sql.append("          where \n");
        sql.append("          idp_013.BSEQSZ=idp_070.gyeqsz--size\n");
        sql.append("          and idp_013.BSEQTP=idp_070.gyeqtp--type\n");
        sql.append("          and idp_013.SPECIAL_HANDLING=idp_070.GYRETP--rate type\n");
        sql.append("          and idp_013.BSBLNO=idp_010.AYBLNO\n");
        sql.append("         )MTY_FULL_FLAG\n");
        //#04 BEGIN
        sql.append("  ,(  CASE idp_070.GYFORS WHEN 'F' THEN idp_070.GYFRUN   ELSE ( \n");  //#06
        sql.append("   select idp_013.BSEFUL\n");
        sql.append("          from sealiner.idp013 idp_013--B/L Shipment Summary\n");
        sql.append("          where \n");
        sql.append("          idp_013.BSEQSZ=idp_070.gyeqsz--size\n");
        sql.append("          and idp_013.BSEQTP=idp_070.gyeqtp--type\n");
        sql.append("          and idp_013.SPECIAL_HANDLING=idp_070.GYRETP--rate type\n");
		sql.append("          and idp_013.BSTEUS = idp_070.GYFRUN \n");  //#06 
        sql.append("          and idp_013.BSBLNO=idp_010.AYBLNO\n");
        sql.append("         ) END ) LADEN\n");
		sql.append("  ,(  CASE  idp_070.GYFORS WHEN 'F' THEN 0 ELSE ( \n "); //#06
        sql.append("    select idp_013.killed_slots\n");
        sql.append("          from sealiner.idp013 idp_013--B/L Shipment Summary\n");
        sql.append("          where \n");
        sql.append("          idp_013.BSEQSZ=idp_070.gyeqsz--size\n");
        sql.append("          and idp_013.BSEQTP=idp_070.gyeqtp--type\n");
        sql.append("          and idp_013.SPECIAL_HANDLING=idp_070.GYRETP--rate type\n");
		sql.append("          and idp_013.BSTEUS = idp_070.GYFRUN \n"); //#06
        sql.append("          and idp_013.BSBLNO=idp_010.AYBLNO\n");
        sql.append("         ) END ) VOIDSLOT\n");
        //#04 END
        sql.append("    ,idp_010.direction\n");
        sql.append("    ,idp_010.DIRECTION_NEXT\n");
        sql.append("    ,idp_070.GYITEM\n");
        //sql.append("    ,RANK() OVER (PARTITION BY idp_070.GYBLNO,idp_050.BYCMCD ORDER BY rownum) rank \n");
        sql.append("    from\n ");
        sql.append("    rclapps.QTN_QTN106_BL_TMP idp_010--BL master\n");
        //sql.append("    sealiner.idp010 idp_010--BL master\n");
        sql.append("    ,sealiner.idp070 idp_070--B/L Charges Details\n");
        sql.append("    ,sealiner.IDP050 idp_050--B/L Commodity\n");        
        sql.append("    where 1=1");
        sql.append("    and idp_070.GYBLNO=IDP_010.AYBLNO\n");
        sql.append("    and idp_050.BYBLNO=IDP_010.AYBLNO\n");
        //sql.append("    and idp_070.gyeqsz>0 -- EQ_SIZE > 0\n");
        sql.append("    and idp_070.gyfors in('S','F')--Surcharge\n"); // ##01 included 'F' also for this case.
        sql.append("    and idp_010.MODULE_CODE='QTN'\n");          
        //sql.append("    -----------------------------------------");
        //sql.append("    --and idp_010.ayblno in ('BKKCB14001898')");
        //sql.append("    --and idp_010.ayblno in ('BKKSB11019032')");
        //sql.append("    --and idp_010.ayblno in ('TPECB09000350')");
        //sql.append("    and idp_010.ayblno in ('SHACB10011054')");
        sql.append(sqlCriteria);
        //sql.append("    -------------------------------------------");
        sql.append("  order by idp_070.gyeqsz )t_data\n");        
        sql.append("where 1=1 )  \n"); //and t_data.blno='SHACB09005358'
        sql.append("group by  etd_date,eta_date,QUOTATION#,contract_party,shipper_name,cnee_name,BL#,svc,vsl,voy,por,pol,pod,del_,container_size,GYITEM,\n"); //and t_data.blno='SINCW14018289'
       // sql.append("container_type,commodity_group,commodity_group_Desc,commodity,commodity_desc,laden_empty,shipment_term,volumn_unit,volumn_teus,O_Frt,currency,surcharge_code,rate,amount,amount_usd,LADEN,VOIDSLOT  \n"); //and t_data.blno='SINCW14018289' , O_Frt_usd update 2017-01-16
        sql.append("container_type,commodity_group,commodity_group_Desc,commodity,commodity_desc,laden_empty,shipment_term,volumn_unit,volumn_teus,O_Frt,O_FrtUsd,currency,surcharge_code,rate,amount,amount_usd,LADEN,VOIDSLOT  \n"); 
        sql.append("order by etd_date,eta_date,BL#,container_size,container_type)result \n"); //and t_data.blno='SINCW14018289'
      // End :03  
     /* sql.append("group by ");
        sql.append("t_data.etd_date\n");
        sql.append(",t_data.QUOTATION_NO\n");
        sql.append(",t_data.cust_func_contract_party_name\n");
        sql.append(",t_data.cust_func_shipper_name\n");
        sql.append(",t_data.cust_func_cnee_name\n");
        sql.append(",t_data.blno\n");
        sql.append(",t_data.cur_srvc \n");
        sql.append(",t_data.aymves \n");
        sql.append(",t_data.aymvoy \n");
        sql.append(",t_data.AYORIG \n");
        sql.append(",t_data.AYMPOL \n");
        sql.append(",t_data.AYMPOD \n");
        sql.append(",t_data.AYDEST \n");
        sql.append(",t_data.EQ_SIZE \n");
        sql.append(",t_data.EQ_TYPE \n");
        sql.append(",t_data.commodity_code \n");
        sql.append(",t_data.commodity_desc \n");//##01
        //sql.append("--,t_data.commodity_group");
        sql.append(",t_data.MTY_FULL_FLAG \n");
        sql.append(",t_data.AYMODE \n");
        sql.append(",t_data.unit\n");
        sql.append(",RCLAPPS.PCR_QTN_FRE_SUR_AUD.FR_GEN_QTN_106_TEU(t_data.blno,t_data.EQ_SIZE,t_data.EQ_TYPE,t_data.unit)--teu\n");
        sql.append(",t_data.EQ_TEU\n");
        sql.append(",t_data.O_Frt\n");
        
        sql.append(",t_data.curr\n");
        sql.append(",t_data.surcharge_code\n");
        sql.append(",t_data.rate\n");
        sql.append(",t_data.amount\n");
        sql.append(",t_data.weight\n");
        sql.append("order by \n");
        sql.append("t_data.etd_date\n");
        sql.append(",t_data.blno\n");
        sql.append(",t_data.EQ_SIZE desc\n");
        sql.append(",t_data.EQ_TYPE\n");       
        sql.append("--,t_data.commodity_code");  */      
        //sql.append("--,t_data.commodity_group");
        //sql.append(",t_data.cust_func_cnee_name\n");
        //sql.append(",t_data.cust_func_shipper_name\n");
        //sql.append(",t_data.cust_func_contract_party_name\n");
        //sql.append("--,t_data.curr");
        //sql.append("--,t_data.surcharge_code");

        //sql = getNumberOfAllData(sql);
        System.out.println("searchQtnFreightSurchargeAuditData:"+sql);
        HashMap map = new HashMap();                
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            QtnFreightSurchargeAuditReportMod mod = new QtnFreightSurchargeAuditReportMod();
                            mod.setEtdDate(RutString.nullToStr(rs.getString("etd_date")));
                            mod.setEtaDate(RutString.nullToStr(rs.getString("eta_date")));
                            mod.setQuotationNum(RutString.nullToStr(rs.getString("QUOTATION#")));                      
                            mod.setContractParty(RutString.nullToStr(rs.getString("contract_party")));
                            mod.setShipperName(RutString.nullToStr(rs.getString("shipper_name")));  
                            mod.setCneeName(RutString.nullToStr(rs.getString("cnee_name")));  
                            mod.setBlNum(RutString.nullToStr(rs.getString("BL#")));  
                            mod.setSvc(RutString.nullToStr(rs.getString("svc")));  
                            mod.setVsl(RutString.nullToStr(rs.getString("vsl")));  
                            mod.setVoy(RutString.nullToStr(rs.getString("voy")));  
                            mod.setPor(RutString.nullToStr(rs.getString("por")));  
                            mod.setPol(RutString.nullToStr(rs.getString("pol")));  
                            mod.setPod(RutString.nullToStr(rs.getString("pod")));  
                            mod.setDel(RutString.nullToStr(rs.getString("del_")));  
                            mod.setContainerSize(RutString.nullToStr(rs.getString("container_size")));  
                            mod.setContainerType(RutString.nullToStr(rs.getString("container_type")));
                           // mod.setWeight(RutString.nullToStr(rs.getString("weight"))); // 03
                            mod.setCommodityGroupCode(RutString.nullToStr(rs.getString("commodity_group")));
                            mod.setCommodityGroupDesc(RutString.nullToStr(rs.getString("commodity_group_Desc")));
                            mod.setCommodity(RutString.nullToStr(rs.getString("commodity")));
                            mod.setCommodityDesc(RutString.nullToStr(rs.getString("commodity_desc"))); //##02
                            mod.setLadenEmpty(RutString.nullToStr(rs.getString("laden_empty")));
                            mod.setShipmentTerm(RutString.nullToStr(rs.getString("shipment_term")));  
                            mod.setVolumnUnit(RutString.nullToStr(rs.getString("volumn_unit")));  
                            mod.setVolumnTeus(RutString.nullToStr(rs.getString("volumn_teus")));  
                            mod.setOFrt(RutString.nullToStr(rs.getString("O_Frt")));  
                            mod.setOFrtUsd(RutString.nullToStr(rs.getString("O_FrtUsd"))); // ##05
                            mod.setCurrency(RutString.nullToStr(rs.getString("currency")));  
                            mod.setSurchargeCode(RutString.nullToStr(rs.getString("surcharge_code")));  
                            mod.setRate(RutString.nullToStr(rs.getString("rate"))); // 03
                            mod.setAmount(RutString.nullToStr(rs.getString("amount")));  // 03
                            mod.setAmount_usd(RutString.nullToStr(rs.getString("amount_usd")));  // 03
                            mod.setRank(RutString.nullToStr(rs.getString("rank")));  // 03
                            mod.setLaden(RutString.nullToStr(rs.getString("LADEN")));
                            mod.setVoidSlot(RutString.nullToStr(rs.getString("VOIDSLOT")));
                            //mod.setCountSurchargeCode(RutString.nullToStr(rs.getString("count_surcharge_code")));  
                            
                            return mod;
                        }
                    });
    }
    
    private String createSqlCriteria(RrcStandardMod mod) {
        String sqlCriteria = "";
        
        if(mod instanceof QtnFreightSurchargeAuditReportMod){
            QtnFreightSurchargeAuditReportMod getMod = (QtnFreightSurchargeAuditReportMod)mod;
            if(getMod.getSessionId()!=null && !getMod.getSessionId().equals(""))
                sqlCriteria+=" and idp_010.SESSION_ID='"+getMod.getSessionId()+"'";
            if(getMod.getUserId()!=null&&!getMod.getUserId().equals(""))
                sqlCriteria+=" and idp_010.RECORD_ADD_USER='"+getMod.getUserId()+"'";
                
            if(getMod.getEtdFrom()!=null && !getMod.getEtdFrom().equals(""))
                sqlCriteria+=" and idp_010.AYMSLD>="+getMod.getEtdFrom();
            if(getMod.getEtdTo()!=null && !getMod.getEtdTo().equals(""))
                sqlCriteria+=" and idp_010.AYMSLD<="+getMod.getEtdTo();
            if(getMod.getEtaFrom()!=null && !getMod.getEtaFrom().equals(""))
                sqlCriteria+=" and idp_010.aysard>="+getMod.getEtaFrom();
            if(getMod.getEtaTo()!=null && !getMod.getEtaTo().equals(""))
                sqlCriteria+=" and idp_010.aysard<="+getMod.getEtaTo();    
            if(getMod.getService()!=null&&!getMod.getService().equals(""))
                sqlCriteria+=" and (idp_010.cur_srvc='"+getMod.getService()+"'"+" or idp_010.AYSSRV='"+getMod.getService()+"'"+")";
            if(getMod.getVessel()!=null&&!getMod.getVessel().equals(""))
                sqlCriteria+=" and (idp_010.aymves='"+getMod.getVessel()+"'"+" or idp_010.AYSVES='"+getMod.getVessel()+"'"+")";
            if(getMod.getVoyage()!=null&&!getMod.getVoyage().equals(""))
                sqlCriteria+=" and (idp_010.aymvoy='"+getMod.getVoyage()+"'"+" or idp_010.AYSVOY='"+getMod.getVoyage()+"'"+")";    
            if(getMod.getDirection()!=null&&!getMod.getDirection().equals(""))
                sqlCriteria+=" and (idp_010.direction='"+getMod.getDirection()+"'"+" or idp_010.DIRECTION_NEXT='"+getMod.getDirection()+"'"+")";   
            if(getMod.getCocSoc()!=null&&!getMod.getCocSoc().equals(""))
                sqlCriteria+=" and idp_010.aysorc='"+getMod.getCocSoc()+"'";        
            if(getMod.getPol()!=null&&!getMod.getPol().equals(""))
                sqlCriteria+=" and idp_010.AYMPOL='"+getMod.getPol()+"'";        
            if(getMod.getPolTerminal()!=null&&!getMod.getPolTerminal().equals(""))
                sqlCriteria+=" and idp_010.FROM_TERMINAL='"+getMod.getPolTerminal()+"'";    
        }
        return sqlCriteria;
    }
    // Start:03 new method for getting weight
    public float getWeight(String bl,String commodity) throws DataAccessException{        
        float lstrWeight=0;
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        sql.append("select ");
        sql.append("TRUNC(sum(idp_050.bymtwt),2 ) weight from idp050 idp_050 \n"); //'FM9999999.90'
        sql.append("where idp_050.byblno =:lstrbl and  idp_050.bycmcd=:lstrCommodity \n");
        map.put("lstrbl",bl);
        map.put("lstrCommodity",commodity);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            lstrWeight = rs.getFloat("weight");
        }
      return lstrWeight  ;
    }
    // End:03 new method for getting weight    
}
