 /*-----------------------------------------------------------------------------------------------------------  
 EmsGeographicalRegionDao.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
  Author Sopon Dee-udomvongsa 29/04/08 
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description 
 01 29/04/08  SPD                       Change to new framework
 -----------------------------------------------------------------------------------------------------------*/  

 package com.rclgroup.dolphin.web.dao.ems;

 import java.util.List;
 import org.springframework.dao.DataAccessException;

 public interface EmsGeographicalRegionDao {

     /**
      * check valid of region
      * @param region
      * @return valid of region
      * @throws DataAccessException
      */
     public boolean isValid(String region) throws DataAccessException;

     /**
      * check valid of region with status
      * @param region
      * @param status
      * @return valid of region with status
      * @throws DataAccessException
      */
     public boolean isValid(String region, String status) throws DataAccessException;

     /**
      * list region records for help screen
      * @param find
      * @param search
      * @param wild
      * @return list of region
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

     /**
      * list region records for help screen with status
      * @param find
      * @param search
      * @param wild
      * @param status
      * @return list of region with status
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;

 }
