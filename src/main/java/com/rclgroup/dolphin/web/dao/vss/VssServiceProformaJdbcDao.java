/*-----------------------------------------------------------------------------------------------------------  
VssServiceProformaJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.vss;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.vss.VssServiceProformaMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class VssServiceProformaJdbcDao extends RrcStandardDao implements VssServiceProformaDao {
   
    public VssServiceProformaJdbcDao() {
    }
   
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException {
        System.out.println("[VssServiceProformaDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SERVICE_CODE ");
        sql.append("      ,SERVICE_DESCRIPTION ");
        sql.append("      ,SIMULATION_REFERENCE_NO ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("FROM VR_VSS_SERVICE_PROFORMA ");
        sql.append("WHERE RECORD_STATUS = :status ");
        sql.append(sqlCriteria);            
        sql.append("ORDER BY SERVICE_CODE ");
        sql.append("        ,SIMULATION_REFERENCE_NO ");
        System.out.println("[VssServiceProformaDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[VssServiceProformaDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    Collections.singletonMap("status", status),
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            VssServiceProformaMod bean = new VssServiceProformaMod();
                            bean.setServiceCode(RutString.nullToStr(rs.getString("SERVICE_CODE")));
                            bean.setServiceDescription(RutString.nullToStr(rs.getString("SERVICE_DESCRIPTION")));
                            bean.setSimulationReferenceNo(RutString.nullToStr(rs.getString("SIMULATION_REFERENCE_NO")));
                            bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            return bean;
                        }
                    });
    }    
    
    public boolean isValid(String performaNo,String status) throws DataAccessException {
        System.out.println("[VssServiceProformaDao][isValid]: Started");
        StringBuffer sql = new StringBuffer();
        boolean isValid = false;
        sql.append("SELECT SIMULATION_REFERENCE_NO ");
        sql.append("FROM VR_VSS_SERVICE_PROFORMA ");
        sql.append("WHERE RECORD_STATUS = :status "); 
        sql.append("  AND SIMULATION_REFERENCE_NO  = :performaNo ");
        System.out.println("[VssServiceProformaDao][isValid]: SQL: " + sql.toString());
        System.out.println("[VssServiceProformaDao][isValid]: Finished");
        HashMap map = new HashMap();
        map.put("status",status);
        map.put("performaNo",performaNo);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        return isValid;
    }    

    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("C")) {
                sqlCriteria = "AND SERVICE_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("D")) {
                sqlCriteria = "AND SERVICE_DESCRIPTION " + sqlWild;
            } else if (search.equalsIgnoreCase("P")) {
                sqlCriteria = "AND SIMULATION_REFERENCE_NO " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND RECORD_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }
    
}




