package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface BkgBookingSpecialCargoListDao {
    public boolean generateTempTable(RrcStandardMod mod) throws DataAccessException;
}
