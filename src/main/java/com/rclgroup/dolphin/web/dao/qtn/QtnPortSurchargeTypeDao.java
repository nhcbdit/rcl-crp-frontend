/*------------------------------------------------------
QtnPortSurchargeTypeDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
 Author Sopon Dee-udomvongsa 03/09/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface QtnPortSurchargeTypeDao {
    
    public boolean isValid(String surchargeType) throws DataAccessException;
    
    public boolean isValid(String surchargeType, String status) throws DataAccessException;
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;
    

}


