/*-----------------------------------------------------------------------------------------------------------  
EmsLessorDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.ems;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface EmsLessorDao {

    /**
     * check valid of Lessor code
     * @param LessorCode
     * @return valid of Lessor code
     * @throws DataAccessException
     */
    public boolean isValid(String LessorCode) throws DataAccessException;

    /**
     * list Lessor records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Lessor
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;
}
