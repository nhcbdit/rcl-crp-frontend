package com.rclgroup.dolphin.web.dao.vss;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface VssLrsDao {
    /**
     * check valid of LRS#
     * @param lrs     
     * @return valid of lrs
     * @throws DataAccessException
     */
    public boolean isLrsValid(String lrs) throws DataAccessException;
        
    /**
     * list Service records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Service
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
}
