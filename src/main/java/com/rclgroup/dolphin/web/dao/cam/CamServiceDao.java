/*-----------------------------------------------------------------------------------------------------------  
CamServiceDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamServiceDao {

    /**
     * list service records for help screen with status
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of services with status
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException;
    
    /**
     * list service records for help screen with status
     * @param service
     * @param status
     */
    public boolean isValid(String service,String status) throws DataAccessException;
}


