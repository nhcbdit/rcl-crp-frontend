package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.tos.TosServiceDao;
import com.rclgroup.dolphin.web.model.tos.TosServiceMod;
import com.rclgroup.dolphin.web.util.RutString;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class TosServiceJdbcDao extends RrcStandardDao implements TosServiceDao{
    public TosServiceJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isServiceValid(String service, String status) throws DataAccessException {
       System.out.println("[TosServiceJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT distinct service ");
       sql.append("FROM VR_TOS_SERVICE_MASTER ");
       sql.append("WHERE service = :service ");      
        sql.append("AND RECORD_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("service",service);
        map.put("status",status);
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[TosServiceJdbcDao][isValid]: Finished");
        return isValid;
    }
      
    public boolean isPortValid(String port, String status) throws DataAccessException {
       System.out.println("[TosServiceJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT distinct port ");
       sql.append("FROM VR_TOS_SERVICE_MASTER ");
       sql.append("WHERE port = :port ");      
        sql.append("AND RECORD_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("port",port);
        map.put("status",status);
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[TosServiceJdbcDao][isValid]: Finished");
        return isValid;
    }
    public boolean isTerminalValid(String terminal, String status) throws DataAccessException {
       System.out.println("[TosServiceJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT distinct terminal ");
       sql.append("FROM VR_TOS_SERVICE_MASTER ");
       sql.append("WHERE service = :terminal ");      
        sql.append("AND RECORD_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("terminal",terminal);
        map.put("status",status);
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[TosServiceJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[TosServiceJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SERVICE ");
        sql.append("       ,SERVICE_DESC ");
        sql.append("       ,PORT ");   
        sql.append("       ,TERMINAL "); 
        sql.append("       ,RECORD_STATUS_DESC ");
        sql.append("FROM VR_TOS_SERVICE_MASTER ");         
        sql.append(sqlCriteria);        
        System.out.println("[TosServiceJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[TosServiceJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE SERVICE " + sqlWild;            
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "WHERE UPPER(SERVICE_DESC) " + sqlWild;                        
            }else if(search.equalsIgnoreCase("P")){
                sqlCriteria = "WHERE PORT " + sqlWild;      
            }else if(search.equalsIgnoreCase("T")){
                sqlCriteria = "WHERE TERMINAL " + sqlWild; 
            }else if(search.equalsIgnoreCase("RS")){
                sqlCriteria = "WHERE UPPER(RECORD_STATUS_DESC) " + sqlWild;
            }        
        }
        return sqlCriteria;
    }

    public boolean isCalculatedByDate(String terminal) {
        System.out.println("[TosServiceJdbcDao][isCalculatedByDate]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT CALCULATED_BY ");
        sql.append("FROM DEPOT_CONTRACT_MASTER ");
        sql.append("WHERE TQTERM = :terminal ");      
         sql.append("AND (CALCULATED_BY = 'D' OR CALCULATED_BY IS NULL) ");
         HashMap map = new HashMap();
         map.put("terminal",terminal);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[TosServiceJdbcDao][isCalculatedByDate]: Finished");
         return isValid;
    }

    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    
    private TosServiceMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        TosServiceMod reason = new TosServiceMod();
        reason.setService(RutString.nullToStr(rs.getString("SERVICE")));   
        reason.setServiceDesc(RutString.nullToStr(rs.getString("SERVICE_DESC")));        
        reason.setPort(RutString.nullToStr(rs.getString("PORT")));                
        reason.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));                
        reason.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS_DESC")));        
        return reason;
    } 
    
    
}
