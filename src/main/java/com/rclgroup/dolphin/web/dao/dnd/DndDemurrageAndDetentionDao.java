/*-----------------------------------------------------------------------------------------------------------  
DndDetentionAndDemurrageDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 22/02/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.dnd;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface DndDemurrageAndDetentionDao {

    /**
     * @return list of DimCargoManifestMod
     * @throws DataAccessException
     */
    public boolean isValidWithSessionId(String sessionId) throws DataAccessException;
    
    /**
      * insert a DND FREE DAYS record
      * @param mod a DND FREE DAYS model
      * @return whether insertion is successful
      * @throws DataAccessException exception which client has to catch all following error messages:
      *                              error message: DND_FREE_DAYD_BL_NO_REQ
      *                              error message: DND_FREE_DAYS_RECORD_ADD_USER_REQ
      *                              error message: ORA-XXXXX (un_exceptional oracle error)
      */
     public boolean insert(RrcStandardMod mod) throws DataAccessException;
     
    /**
     * delete a DND FREE DAYS record
     * @param mod a DND FREE DAYS model
     * @return wheter deletion is successful
     * @throws DataAccessException exception dwhich client has to catch all following error messages:
     *                              error message: DND_FREE_DAYD_BL_NO_REQ
      *                              error message: DND_FREE_DAYS_RECORD_ADD_USER_REQ
     *                              error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * generate a DND Billing Status (Normal) record
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateBillingStatusContent(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * generate a DND Billing Status: Invoice (Cancel + Net Amount) record
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateInvoiceForAcos(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * generate a DND Billing Status: Summary by Status (USD) record
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateSummaryStatus(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * generate a DND Billing Status: Summary by Period (USD) record
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateSummaryPeriod(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * 
     * @param rowAt
     * @param rowTo
     * @param findBy
     * @param findIn
     * @param sortBy
     * @param sortIn
     * @param status
     * @return
     * @throws DataAccessException
     */
    public List listForDndWaiverDiscountScreen(int rowAt, int rowTo, String  findBy,String findIn, String sortBy, String sortIn,String status) throws DataAccessException;
    
    /**
     * 
     * @param findBy
     * @param findIn
     * @param sortBy
     * @param sortIn
     * @param status
     * @return
     * @throws DataAccessException
     */
    public int countListForDndWaiverDiscountScreen(String  findBy,String findIn, String sortBy, String sortIn,String status)throws DataAccessException;
}
