 /*------------------------------------------------------
 VmsAgencyCommissionExceptionDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sukit Narinsakchai 04/10/2010   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

package com.rclgroup.dolphin.web.dao.vms;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import org.springframework.dao.DataAccessException;

public interface VmsAgencyCommissionExceptionDao {
    
    /**
      * insert a Booking Against BSA record
      * @param mod a Booking Against BSA model
      * @return whether insertion is successful
      * @throws DataAccessException exception which client has to catch all following error messages:
      *                              error message: ORA-XXXXX (un_exceptional oracle error)
      */
     public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
}
