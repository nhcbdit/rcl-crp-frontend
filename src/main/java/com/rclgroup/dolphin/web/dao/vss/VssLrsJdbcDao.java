package com.rclgroup.dolphin.web.dao.vss;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.vss.VssLrsMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class VssLrsJdbcDao extends RrcStandardDao implements VssLrsDao{
    public VssLrsJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isLrsValid(String lrs) throws DataAccessException {
       System.out.println("[VssLrsJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT LRS_CODE ");
       sql.append("FROM VR_CAM_LRS_HELP ");
        if ((lrs!=null)&&(!lrs.trim().equals(""))) {
            sql.append("WHERE LRS_CODE = :lrs ");
            sql.append("    and rownum = 1 ");
        }        
       HashMap map = new HashMap();
       map.put("lrs",lrs);        
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[VssLrsJdbcDao][isValid]: Finished");
        return isValid;
    }
      
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[VssLrsJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SERVICE ");
        sql.append("       ,LRS_CODE ");        
        sql.append("FROM VR_CAM_LRS_HELP ");         
        sql.append(sqlCriteria);        
        System.out.println("[VssLrsJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[VssLrsJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE SERVICE " + sqlWild;            
            }else if(search.equalsIgnoreCase("L")){
                sqlCriteria = "WHERE LRS_CODE " + sqlWild;                                    
            }        
        }
        return sqlCriteria;
    }        
   
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    
    private VssLrsMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        VssLrsMod reason = new VssLrsMod();
        reason.setService(RutString.nullToStr(rs.getString("SERVICE")));   
        reason.setLrsNo(RutString.nullToStr(rs.getString("LRS_CODE")));                
        return reason;
    }     
}
