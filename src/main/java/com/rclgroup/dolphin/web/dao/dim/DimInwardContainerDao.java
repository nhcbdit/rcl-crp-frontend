package com.rclgroup.dolphin.web.dao.dim;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface DimInwardContainerDao {
    /**
     * check valid of B/L#
     * @param service
     * @param vessel
     * @param voyage
     * @param direction
     * @param pol
     * @param pod
     * @param podT
     * @param finalDes
     * @param lineCode
     * @param regionCode
     * @param agenCode
     * @param  fscCode
     * @return list of Inward Container
     * @throws DataAccessException
     */
    public List getListInwardContainer(String service,String vessel,String voyage,String direction,String pol,String pod, String podT, String finalDes, String lineCode,String regionCode,String agenCode,String fscCode) throws DataAccessException;
}
