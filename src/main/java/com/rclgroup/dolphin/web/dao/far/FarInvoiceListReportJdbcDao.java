/*--------------------------------------------------------
FarInvoiceListReportJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Leena Babu 20/07/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
01 17/10/13  LEE    PD_CR_20130830 :Allow Control FSC to print Invoice List under 
                      their Control  
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.far.FarInvoiceListMod;
import com.rclgroup.dolphin.web.util.RutDate;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class FarInvoiceListReportJdbcDao extends RrcStandardDao implements FarInvoiceListReportDao {
    private InsertStoreProcedure insertStoreProcedure;   
    
    
    protected void initDao() throws Exception {
        super.initDao();        
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
    } 

    public boolean insert(RrcStandardMod mod) throws DataAccessException{      
        return insertStoreProcedure.insert(mod);
    }

    private class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_RPT.PRR_FAR_RPT104";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);   
            declareParameter(new SqlParameter("p_usr_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));  
            declareParameter(new SqlParameter("p_fdate", Types.INTEGER));
            declareParameter(new SqlParameter("p_tdate", Types.INTEGER));   
            declareParameter(new SqlParameter("p_r", Types.VARCHAR));   
            declareParameter(new SqlParameter("p_a", Types.VARCHAR));
            declareParameter(new SqlParameter("p_l", Types.VARCHAR));
            declareParameter(new SqlParameter("p_f", Types.VARCHAR)); 
            declareParameter(new SqlParameter("p_cus", Types.VARCHAR));
            declareParameter(new SqlParameter("p_coll_fsc", Types.VARCHAR));     
            declareParameter(new SqlParameter("p_show_all_fsc", Types.VARCHAR));//##01
            declareParameter(new SqlParameter("p_control_fsc", Types.VARCHAR));//##01
            declareParameter(new SqlParameter("p_report_format", Types.VARCHAR));
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod) {
            return insertInvoiceList(mod);
        }
        
        public boolean insertInvoiceList(final RrcStandardMod inputMod){
            boolean isSuccess = false;       
             if((inputMod instanceof FarInvoiceListMod)){
                FarInvoiceListMod aInputMod = (FarInvoiceListMod)inputMod;               
                Map inParameters = new HashMap(15);
                String dateFromParam = RutDate.dateToStr(aInputMod.getDateFrm());
                String dateToParam = RutDate.dateToStr(aInputMod.getDateTo()); 
                inParameters.put("p_usr_id", (aInputMod.getUsrId().equals(""))?null:aInputMod.getUsrId());
                inParameters.put("p_session_id", (aInputMod.getSessionId().equals(""))?null:aInputMod.getSessionId());                    
                inParameters.put("p_fdate", new Integer((dateFromParam.equals(""))?"0":dateFromParam));
                inParameters.put("p_tdate", new Integer((dateToParam.equals(""))?"0":dateToParam));
                inParameters.put("p_r", (aInputMod.getTrade().equals(""))?null:aInputMod.getTrade());
                inParameters.put("p_a", (aInputMod.getAgent().equals(""))?null:aInputMod.getAgent());
                inParameters.put("p_l", (aInputMod.getLine().equals(""))?null:aInputMod.getLine());
                inParameters.put("p_f", (aInputMod.getFsc().equals(""))?null:aInputMod.getFsc());                
                inParameters.put("p_cus", (aInputMod.getCustomerCode().equals(""))?null:aInputMod.getCustomerCode());   
                inParameters.put("p_coll_fsc", (aInputMod.getCollFsc().equals(""))?null:aInputMod.getCollFsc());   
                inParameters.put("p_show_all_fsc", (aInputMod.getShowAllFsc().equals(""))?null:aInputMod.getShowAllFsc()); //##01  
                inParameters.put("p_control_fsc", (aInputMod.isIsControlFsc()?"Y":"N"));//##01
                inParameters.put("p_report_format", (aInputMod.getReportFormat().equals(""))?null:aInputMod.getReportFormat());   
                long time = System.currentTimeMillis();
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;                                            
                }         
             }
            return isSuccess;
        }
    }
}
