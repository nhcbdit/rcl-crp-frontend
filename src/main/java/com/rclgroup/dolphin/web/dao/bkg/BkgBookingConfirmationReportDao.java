/*--------------------------------------------------------
BkgBookingConfirmationReportDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Leena Babu 29/04/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface BkgBookingConfirmationReportDao {
    /**
     * insert the report data into temporary tables for the given search criteria
     * @param service
     * @param vessel
     * @param voyage
     * @param direction
     * @param bookingNumber
     * @param mtPickupDepot
     * @param mtDate
     * @param pol
     * @param pod
     * @param showRoutingDetails
     * @param showFreightCharges
     * @param showCommodityDetails
     * @param showMTPickupInfo
     * @param showPrintRemarks
     * @param userCode
     * @param sessionId
     * @return void
     * */
    public String insertDataIntoTempTables(String service, String vessel, String voyage, String direction, 
        String bookingNumber, String mtDepot, String mtDate, String pol, String pod,
        String showRoutDetails, String showFreightCharges, String showCommodityDetails,
        String showMTPickupInfo, String printRemarks, String userCode, String sessionId);
}
