 /*------------------------------------------------------
 VmsAgencyCommissionExceptionJdbcDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sukit Narinsakchai 04/10/2010   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */
package com.rclgroup.dolphin.web.dao.vms;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bkg.BkgBookingAgainstBsaMod;
import com.rclgroup.dolphin.web.model.vms.VmsAgencyCommissionExceptionMod;
import com.rclgroup.dolphin.web.util.RutString;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class VmsAgencyCommissionExceptionJdbcDao extends RrcStandardDao implements VmsAgencyCommissionExceptionDao{
    private InsertStoreProcedure insertStoreProcedure;    
    
    public VmsAgencyCommissionExceptionJdbcDao() {
    }

    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
    }
    
    public boolean insert(RrcStandardMod mod) {
        return insertStoreProcedure.insert(mod);
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VMS_RPT.PRR_VMS106_GEN_RPT";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate,STORED_PROCEDURE_NAME);            
            declareParameter(new SqlParameter("p_userid",Types.VARCHAR));
            declareParameter(new SqlParameter("p_sessionid",Types.INTEGER));
            declareParameter(new SqlParameter("p_agent",Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc",Types.VARCHAR));
            declareParameter(new SqlParameter("p_from_date",Types.INTEGER));
            declareParameter(new SqlParameter("p_to_date",Types.INTEGER));
            declareParameter(new SqlParameter("p_exc_type",Types.VARCHAR));
            declareParameter(new SqlParameter("p_exc_opt",Types.VARCHAR));
            declareParameter(new SqlParameter("p_rep_type",Types.VARCHAR));            
            declareParameter(new SqlParameter("p_rep_sum",Types.VARCHAR));
        }
        
        protected boolean insert(RrcStandardMod mod){
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod , RrcStandardMod outputMod){
            boolean isSuccess = false;
            VmsAgencyCommissionExceptionMod aInputMod = (VmsAgencyCommissionExceptionMod)inputMod;
            VmsAgencyCommissionExceptionMod aOutputMod = (VmsAgencyCommissionExceptionMod)outputMod;
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:Started ");
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_userid : "+aInputMod.getUserId());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_sessionid : "+aInputMod.getSessionId());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_agent : "+aInputMod.getAgent());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_fsc : "+aInputMod.getFsc());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_from_date : "+aInputMod.getDateFrom());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_to_date : "+aInputMod.getDateTo());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_exc_type : "+aInputMod.getExcType());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_exc_opt : "+aInputMod.getExcOpt());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_rep_type : "+aInputMod.getReportType());
            System.out.println("[VmsAgencyCommissionExceptionJdbcDao][insert]:p_rep_sum : "+aInputMod.getReportSum());
                
            if((inputMod instanceof VmsAgencyCommissionExceptionMod) && (outputMod instanceof VmsAgencyCommissionExceptionMod)){
                
                Map inParameters = new HashMap(10);                
                inParameters.put("p_userid",aInputMod.getUserId());
                inParameters.put("p_sessionid",new Integer(aInputMod.getSessionId()));                
                inParameters.put("p_agent",aInputMod.getAgent());
                inParameters.put("p_fsc",aInputMod.getFsc());
                inParameters.put("p_from_date",new Integer(aInputMod.getDateFrom()));
                inParameters.put("p_to_date",new Integer(aInputMod.getDateTo()));
                inParameters.put("p_exc_type",aInputMod.getExcType());
                inParameters.put("p_exc_opt",aInputMod.getExcOpt());                               
                inParameters.put("p_rep_type",aInputMod.getReportType());                               
                inParameters.put("p_rep_sum",aInputMod.getReportSum());
                Map outParameters = execute(inParameters);
                if(outParameters.size() >0){
                    isSuccess = true;
                    aOutputMod.setSessionId(((Integer)outParameters.get("p_sessionid")).intValue());                    
                }
            }
            
            return isSuccess;
            }
        }
    
}
