/*-----------------------------------------------------------------------------------------------------------  
CamFscPortMasterDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 19/08/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamFscMod;

import com.rclgroup.dolphin.web.model.cam.CamFscPortMasterMod;
import com.rclgroup.dolphin.web.model.cam.CamVesselMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamFscPortMasterDao {

    /**
     * @param fscCode fscCode
     * @return fsc port master model of fsc code
     * @throws DataAccessException
     */
    public CamFscPortMasterMod findFscPortMasterModByFscCode(String fscCode) throws DataAccessException;

}


