/*------------------------------------------------------
QtnExportToExcelJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2008
--------------------------------------------------------
Author Kitti Pongsirisakun 01/12/08  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
01 22/12/08  KIT    BUG.156   Display rows duplicate
02 15/01/09  KIT    BUG.163   Export data should be tested with Import data 
 
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.qtn.QtnExportToExcelMod;
import com.rclgroup.dolphin.web.model.qtn.QtnExportToExcelSurchargeMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class QtnExportToExcelJdbcDao extends RrcStandardDao implements QtnExportToExcelDao {
    private InsertStoreProcedure insertStoreProcedure;
    private deleteStoreProcedure deleteStoreProcedure;
    public QtnExportToExcelJdbcDao() {
    }    
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new deleteStoreProcedure(getJdbcTemplate());
    }
    protected class InsertStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_QTN_EXPORT_EXCEL.PRR_GET_QUOTATION";
    protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_quoNO", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_session", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
          //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
          //  declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));     
            compile();
            System.out.println("InsertStoreProcedure declare parameter end");
        }
    protected String insert(String quotationNo  , String userName,String session  ) {
        return insertPro(  quotationNo  , userName, session );
    }
    protected String insertPro(String quotationNo  , String userName,String session   ) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            inParameters.put("p_quoNO",RutDatabase.stringToDb(quotationNo));
            inParameters.put("p_session",RutDatabase.stringToDb(session));             
            inParameters.put("p_username",RutDatabase.stringToDb(userName));      
            
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_quoNO = "+inParameters.get("p_quoNO"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_session = "+inParameters.get("p_session"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_username = "+inParameters.get("p_username"));

          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_session");
               System.out.println("  sessionId >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    protected class deleteStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_QTN_EXPORT_EXCEL.PRR_DEL_QUOTATION";
    protected  deleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("deleteStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            compile();
            System.out.println("deleteStoreProcedure declare parameter end");
        }
    protected void delete(String userName ) {
         deletePro(userName );
    }
    protected void deletePro(String userName ) {
            System.out.println("deleteStoreProcedure assign value to  parameter begin");
            
            Map inParameters = new HashMap();        
            inParameters.put("p_username",RutDatabase.stringToDb(userName));      
            
            System.out.println("[FarExceptionLateBillingJdbcDao][deleteStoreProcedure][delete]: p_username = "+inParameters.get("p_username"));

            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               System.out.println("  a mount of row deleted  >>>>>>>>>>>>> "+outParameters.size());
            }
        }          
    } 
    public String  getSessionId(String quotationNo  , String userName,String session)throws DataAccessException {
                                                         
    return insertStoreProcedure.insert( quotationNo  , userName, session );
     }
    public void  deletePro( String userName)throws DataAccessException {
                 deleteStoreProcedure.delete(userName );
     }
    public boolean isValid(String quotationNo) throws DataAccessException {
        System.out.println("[QtnPortSurchargeJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT QUOTATION_NO ");
        sql.append("FROM VR_QTN_QUOTATION_DETAIL ");
        sql.append("WHERE QUOTATION_NO = :quotationNo ");
        HashMap map = new HashMap();
        map.put("quotationNo",quotationNo);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[QtnExportToExcelJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValid(String quotationNo, String status) throws DataAccessException {
        System.out.println("[QtnPortSurchargeJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT QUOTATION_NO ");
        sql.append("FROM VR_QTN_QUOTATION_DETAIL ");
        sql.append("WHERE QUOTATION_NO = :quotationNo ");
        HashMap map = new HashMap();
        map.put("quotationNo",quotationNo);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[QtnExportToExcelJdbcDao][isValid]: With status: Finished");
        return isValid;
    }    
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[QtnExportToExcelJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        //##01 BEGIN
//        sql.append("SELECT LINE ");
//        sql.append("      ,TRADE ");    
//        sql.append("      ,AGENT ");  
//        sql.append("      ,FSC ");             
//        sql.append("      ,POR "); 
//        sql.append("      ,POL "); 
//        sql.append("      ,POT1 ");
//        sql.append("      ,POT2 ");
//        sql.append("      ,POT3 ");
//        sql.append("      ,POD ");
        //##01 END
        sql.append("SELECT DISTINCT QUOTATION_NO AS QUONO ");
        sql.append("      ,Q_QUOREF AS QUOREF ");
        sql.append("      ,C_CODE AS PARTYCODE ");
        sql.append("      ,C_NAME AS PARTYNAME ");
        sql.append("      ,Q_STATUSDES AS STSDESC ");
        sql.append("      ,Q_EFFECTIVE AS EFFDATE ");
        sql.append("      ,Q_EXPIRY  AS EXPDATE ");
        sql.append("      ,Q_FOLLOW  AS FOLLOW ");
        sql.append("      ,Q_TEL AS TEL ");
        sql.append("      ,Q_EMAIL AS EMAIL ");
        sql.append("      ,Q_REMARKS AS RMK ");
        
        sql.append(" FROM VR_QTN_QUOTATION_HELP ");
        sql.append(" WHERE 1 = 1 ");
        sql.append(sqlCriteria);
//        sql.append (" GROUP BY ");
//        sql.append (" LINE, TRADE, AGENT, FSC, POR, POL, POT1, POT2, POT3, POD, "); // ##01
//        sql.append (" QUOTATION_NO , Q_QUOREF, C_CODE , ");
//        sql.append (" C_NAME , Q_STATUSDES , Q_EFFECTIVE , ");
//        sql.append (" Q_EXPIRY , Q_FOLLOW , Q_TEL , ");
//        sql.append (" Q_EMAIL , Q_REMARKS ");
        System.out.println("[QtnExportToExcelJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[QtnExportToExcelJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());   
    }
    
 
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("Q")){
                sqlCriteria = " AND QUOTATION_NO " + sqlWild;
            }else if(search.equalsIgnoreCase("RF")){
                sqlCriteria = " AND Q_QUOREF " + sqlWild;
            }else if(search.equalsIgnoreCase("P")){
                sqlCriteria = " AND C_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("PN")){
                sqlCriteria = " AND C_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = " AND Q_STATUSDES " + sqlWild;
            }else if(search.equalsIgnoreCase("DE")){
                sqlCriteria = " AND Q_EFFECTIVE " + sqlWild;
            }else if(search.equalsIgnoreCase("DX")){
                sqlCriteria = " AND Q_EXPIRY " + sqlWild;
            }else if(search.equalsIgnoreCase("F")){
                sqlCriteria = " AND Q_FOLLOW " + sqlWild;
            }else if(search.equalsIgnoreCase("T")){
                sqlCriteria = " AND Q_TEL " + sqlWild;
            }else if(search.equalsIgnoreCase("E")){
                sqlCriteria = " AND Q_EMAIL " + sqlWild;
            }else if(search.equalsIgnoreCase("R")){
                sqlCriteria = " AND Q_REMARKS " + sqlWild;              
        }
       
    }     
    return sqlCriteria;
    }
    
    public List listForHelpScreen(String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException {

        System.out.println("[QtnExportToExcelJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        HashMap map = new HashMap();
        StringBuffer sql = new StringBuffer();
        //##01 BEGIN
//        sql.append("SELECT LINE ");
//        sql.append("      ,TRADE ");    
//        sql.append("      ,AGENT ");  
//        sql.append("      ,FSC ");             
//        sql.append("      ,POR "); 
//        sql.append("      ,POL "); 
//        sql.append("      ,POT1 ");
//        sql.append("      ,POT2 ");
//        sql.append("      ,POT3 ");
//        sql.append("      ,POD ");
        //##01 END
        sql.append("SELECT DISTINCT QUOTATION_NO AS QUONO ");
        sql.append("      ,Q_QUOREF AS QUOREF ");
        sql.append("      ,C_CODE AS PARTYCODE ");
        sql.append("      ,C_NAME AS PARTYNAME ");
        sql.append("      ,Q_STATUSDES AS STSDESC ");
        sql.append("      ,Q_EFFECTIVE AS EFFDATE ");
        sql.append("      ,Q_EXPIRY AS EXPDATE  ");  
        sql.append("      ,Q_FOLLOW  AS FOLLOW  ");  
        sql.append("      ,Q_TEL AS TEL ");
        sql.append("      ,Q_EMAIL AS EMAIL ");
        sql.append("      ,Q_REMARKS AS RMK ");
        
        sql.append(" FROM VR_QTN_QUOTATION_HELP ");
        sql.append(" WHERE 1 = 1 ");
        sql.append(sqlCriteria);
        
        sql.append(" AND");
        sql.append(" ( ");
        sql.append("  (NVL(FSC,' ') = NVL(:fsc,NVL(FSC,' ')) )  ");
        sql.append("  OR ");
        sql.append("      NVL(POL,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE   NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                    ")" );
        sql.append(" OR ");         
        sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );
        sql.append(" OR "); 
        sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );
        sql.append(" OR "); 
        sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );

        sql.append(" ) "); 
        //--------------------------------
        map.put("line",line);    
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
//        sql.append (" GROUP BY ");
//        sql.append (" LINE, TRADE, AGENT, FSC, POR, POL, POT1, POT2, POT3, POD, ");//##01
//        sql.append (" QUOTATION_NO , Q_QUOREF, C_CODE , ");
//        sql.append (" C_NAME , Q_STATUSDES , Q_EFFECTIVE , ");
//        sql.append (" Q_EXPIRY , Q_FOLLOW , Q_TEL , ");
//        sql.append (" Q_EMAIL , Q_REMARKS ");
        System.out.println("[QtnExportToExcelJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
        System.out.println("[QtnExportToExcelJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());  
    }
    public int countListForNewHelpScreenByFsc(String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException {

        System.out.println("[QtnExportToExcelJdbcDao][countListForNewHelpScreenByFsc]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        HashMap map = new HashMap();
        StringBuffer sql = new StringBuffer();
        //##01 BEGIN
    //        sql.append("SELECT LINE ");
    //        sql.append("      ,TRADE ");
    //        sql.append("      ,AGENT ");
    //        sql.append("      ,FSC ");
    //        sql.append("      ,POR ");
    //        sql.append("      ,POL ");
    //        sql.append("      ,POT1 ");
    //        sql.append("      ,POT2 ");
    //        sql.append("      ,POT3 ");
    //        sql.append("      ,POD ");
        //##01 END
        sql.append("SELECT DISTINCT QUOTATION_NO AS QUONO ");
        sql.append("      ,Q_QUOREF AS QUOREF ");
        sql.append("      ,C_CODE AS PARTYCODE ");
        sql.append("      ,C_NAME AS PARTYNAME ");
        sql.append("      ,Q_STATUSDES AS STSDESC ");
        sql.append("      ,Q_EFFECTIVE AS EFFDATE ");
        sql.append("      ,Q_EXPIRY AS EXPDATE  ");  
        sql.append("      ,Q_FOLLOW  AS FOLLOW  ");  
        sql.append("      ,Q_TEL AS TEL ");
        sql.append("      ,Q_EMAIL AS EMAIL ");
        sql.append("      ,Q_REMARKS AS RMK ");
        
        sql.append(" FROM VR_QTN_QUOTATION_HELP ");
        sql.append(" WHERE 1 = 1 ");
        sql.append(sqlCriteria);
        
        sql.append(" AND");
        sql.append(" ( ");
        sql.append("  (NVL(FSC,' ') = NVL(:fsc,NVL(FSC,' ')) )  ");
        sql.append("  OR ");
        sql.append("      NVL(POL,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE   NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                    ")" );
        sql.append(" OR ");         
        sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );
        sql.append(" OR "); 
        sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );
        sql.append(" OR "); 
        sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );

        sql.append(" ) "); 
        System.out.println("line = "+line+"trade = "+trade+"agent = "+agent+"fsc = "+fsc);
        sql = getNumberOfAllData(sql);
        //--------------------------------
        map.put("line",line);    
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
//        sql.append (" GROUP BY ");
    //        sql.append (" LINE, TRADE, AGENT, FSC, POR, POL, POT1, POT2, POT3, POD, ");//##01
//        sql.append (" QUOTATION_NO , Q_QUOREF, C_CODE , ");
//        sql.append (" C_NAME , Q_STATUSDES , Q_EFFECTIVE , ");
//        sql.append (" Q_EXPIRY , Q_FOLLOW , Q_TEL , ");
//        sql.append (" Q_EMAIL , Q_REMARKS ");
        System.out.println("[QtnExportToExcelJdbcDao][countListForNewHelpScreenByFsc]: With status: SQL = " + sql.toString());
        System.out.println("[QtnExportToExcelJdbcDao][countListForNewHelpScreenByFsc]: Finished");
        Integer result =  (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map, Integer.class);
        return result;
    }
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException {

        System.out.println("[QtnExportToExcelJdbcDao][listForNewHelpScreenByFsc]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        HashMap map = new HashMap();
        StringBuffer sql = new StringBuffer();
        String  selectColumn = " DISTINCT QUOTATION_NO AS QUONO "+
                 "      ,Q_QUOREF AS QUOREF "+
                 "      ,C_CODE AS PARTYCODE "+
                 "      ,C_NAME AS PARTYNAME "+
                 "      ,Q_STATUSDES AS STSDESC "+
                 "      ,Q_EFFECTIVE  AS EFFDATE "+
                 "      ,Q_EXPIRY   AS EXPDATE  "+  
                 "      ,Q_FOLLOW  AS FOLLOW  "+
                 "      ,Q_TEL AS TEL"+
                 "      ,Q_EMAIL AS EMAIL "+
                 "      ,Q_REMARKS AS RMK ";
        //##01 BEGIN
    //        sql.append("SELECT LINE ");
    //        sql.append("      ,TRADE ");
    //        sql.append("      ,AGENT ");
    //        sql.append("      ,FSC ");
    //        sql.append("      ,POR ");
    //        sql.append("      ,POL ");
    //        sql.append("      ,POT1 ");
    //        sql.append("      ,POT2 ");
    //        sql.append("      ,POT3 ");
    //        sql.append("      ,POD ");
        //##01 END
        sql.append("SELECT "+selectColumn);   
        sql.append(" FROM VR_QTN_QUOTATION_HELP ");
        sql.append(" WHERE 1 = 1 ");
        sql.append(sqlCriteria);
        
        sql.append(" AND");
        sql.append(" ( ");
        sql.append("  (NVL(FSC,' ') = NVL(:fsc,NVL(FSC,' ')) )  ");
        sql.append("  OR ");
        sql.append("      NVL(POL,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE   NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                    ")" );
        sql.append(" OR ");         
        sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );
        sql.append(" OR "); 
        sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );
        sql.append(" OR "); 
        sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                              " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                              " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                              " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                              " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                   ")" );

        sql.append(" ) "); 
        sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);  
        //--------------------------------
        map.put("line",line);    
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
//        sql.append (" GROUP BY ");
    //        sql.append (" LINE, TRADE, AGENT, FSC, POR, POL, POT1, POT2, POT3, POD, ");//##01
//        sql.append (" QUOTATION_NO , Q_QUOREF, C_CODE , ");
//        sql.append (" C_NAME , Q_STATUSDES , Q_EFFECTIVE , ");
//        sql.append (" Q_EXPIRY , Q_FOLLOW , Q_TEL , ");
//        sql.append (" Q_EMAIL , Q_REMARKS ");
        System.out.println("[QtnExportToExcelJdbcDao][listForNewHelpScreenByFsc]: With status: SQL = " + sql.toString());
        System.out.println("[QtnExportToExcelJdbcDao][listForNewHelpScreenByFsc]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());  
    }
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("Q")){
                sqlCriteria = " AND QUOTATION_NO " + sqlWild;
            }else if(search.equalsIgnoreCase("RF")){
                sqlCriteria = " AND Q_QUOREF " + sqlWild;
            }else if(search.equalsIgnoreCase("P")){
                sqlCriteria = " AND C_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("PN")){
                sqlCriteria = " AND C_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = " AND Q_STATUSDES " + sqlWild;
            }else if(search.equalsIgnoreCase("DE")){
                sqlCriteria = " AND Q_EFFECTIVE " + sqlWild;
            }else if(search.equalsIgnoreCase("DX")){
                sqlCriteria = " AND Q_EXPIRY " + sqlWild;
            }else if(search.equalsIgnoreCase("F")){
                sqlCriteria = " AND Q_FOLLOW " + sqlWild;
            }else if(search.equalsIgnoreCase("T")){
                sqlCriteria = " AND Q_TEL " + sqlWild;
            }else if(search.equalsIgnoreCase("E")){
                sqlCriteria = " AND Q_EMAIL " + sqlWild;
            }else if(search.equalsIgnoreCase("R")){
                sqlCriteria = " AND Q_REMARKS " + sqlWild;              
            }
        }
        return sqlCriteria;
    }   
    public Vector getQuotationExportToExcel(String quotationNo,String sesseion,String userName) throws DataAccessException{
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationExportToExcel]: With status: Started");
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationExportToExcel]: With status: quotationNo "+quotationNo);
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationExportToExcel]: With status: sesseion "+sesseion);
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationExportToExcel]: With status: userName "+userName);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        int curQuotationSize = 0;
        String curQuotationType = "";
        int curQuotationCor = 0;
        int tmpQuotationSize = 99;
        String tmpRateType = "xxxx"; // --##02
        String tmpQuotationType = "xxxx";
        String curRateType = ""; // --##02
        int tmpQuotationCor = 0;                        
        Vector quotationVec = new Vector();
        Vector surchargeVec = new Vector();
        Vector commodityVec = new Vector();
        
        QtnExportToExcelMod qtnExpToExcMod = null;
        QtnExportToExcelSurchargeMod qtnExpToExcSurMod = null;
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationExportToExcel]: ");
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationExportToExcel]: Finished");
        // Get all commodity rate code
        commodityVec = getCommodityQtnExportToExcel(quotationNo,sesseion,userName);
        //
        sql.append("SELECT QUOTATION_NO ");
        sql.append(" ,QC_SEQ ");
        sql.append(" ,POR ");
        sql.append(" ,POL ");
        sql.append(" ,QT_S ");
        sql.append(" ,POD ");
        sql.append(" ,DEL ");
        sql.append(" ,SHIP_TYPE ");
        sql.append(" ,TERM ");
        sql.append(" ,COC_SOC ");
        sql.append(" ,SER ");
        sql.append(" ,POL_STATUS ");
        sql.append(" ,Q_SIZE ");
        sql.append(" ,Q_TYPE ");
        sql.append(" ,POD_STATUS ");
        sql.append(" ,FULL_QTY ");
        sql.append(" ,MT_QTY ");
        sql.append(" ,RATE_TYPE ");
        sql.append(" ,Q_PORT_CLASS ");
        sql.append(" ,Q_COM_CODE ");
        sql.append(" ,Q_RATE_TYPE ");
        sql.append(" ,SPECIAL_HANDLING ");// --##02
        sql.append(" ,Q_LATEST_FLAG ");
        sql.append(" ,FREIGHT_SURCHARGE ");
        sql.append(" ,Q_RATE ");
        sql.append(" ,Q_CURR ");
        sql.append(" ,PLS_SETTLE ");
        sql.append(" ,EX_DEM ");
        sql.append(" ,EX_DET ");
        sql.append(" ,IMP_DEM ");
        sql.append(" ,IMP_DET ");
        sql.append(" ,CODE ");
        sql.append(" ,INC ");
        sql.append(" ,Q_SUB_CHANGE ");
        sql.append(" ,PLS_SETTLE ");
        
        sql.append(" FROM QTN_EXPORT_EXCEL ");
        sql.append(" WHERE QUOTATION_NO = :quotationNo ");
        sql.append(" AND SESSIONID = :sesseion ");
        sql.append(" AND USERNAME = :userName ");
        sql.append(" ORDER BY QUOTATION_NO ,QC_SEQ,Q_SIZE,Q_TYPE,SPECIAL_HANDLING "); // --##02
        map.put("quotationNo",quotationNo);
        map.put("sesseion",sesseion);
        map.put("userName",userName);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        int i = 0,check = 0;
        int line = 0;
        rs.last();
        i = rs.getRow();
        rs.first();
        if(i != 0){
        do{
            
             curQuotationSize = rs.getInt("Q_SIZE");
             curQuotationType = RutString.nullToStr(rs.getString("Q_TYPE"));
             curQuotationCor  = rs.getInt("QC_SEQ");
             curRateType      = RutString.nullToStr(rs.getString("SPECIAL_HANDLING"));// --##02
            // # IF 1
            check++;
            if(curQuotationSize != tmpQuotationSize || !curQuotationType.equalsIgnoreCase(tmpQuotationType) || curQuotationCor != tmpQuotationCor || !curRateType.equalsIgnoreCase(tmpRateType)){// --##02
                tmpQuotationSize = curQuotationSize; 
                tmpQuotationType = curQuotationType;
                tmpRateType      = curRateType; // --##02
                 // --##02 BEGIN
                tmpQuotationCor = curQuotationCor; 
                // --##02 END
                if (qtnExpToExcMod != null){
                    qtnExpToExcMod.setSurchargeVec(surchargeVec);
                    quotationVec.add(qtnExpToExcMod);
                }
                qtnExpToExcMod = new QtnExportToExcelMod();
                qtnExpToExcMod.setQuotationNo(RutString.nullToStr(rs.getString("QUOTATION_NO")));
                qtnExpToExcMod.setPor(RutString.nullToStr(rs.getString("POR")));
                qtnExpToExcMod.setPol(RutString.nullToStr(rs.getString("POL")));
                qtnExpToExcMod.setTs(RutString.nullToStr(rs.getString("QT_S")));
                qtnExpToExcMod.setPod(RutString.nullToStr(rs.getString("POD")));
                qtnExpToExcMod.setDel(RutString.nullToStr(rs.getString("DEL")));
                qtnExpToExcMod.setShipType(RutString.nullToStr(rs.getString("SHIP_TYPE")));
                qtnExpToExcMod.setTerm(RutString.nullToStr(rs.getString("TERM")));
                qtnExpToExcMod.setLineOfBus(RutString.nullToStr(rs.getString("COC_SOC")));
                qtnExpToExcMod.setService(RutString.nullToStr(rs.getString("SER")));
                qtnExpToExcMod.setPolSts(RutString.nullToStr(rs.getString("POL_STATUS")));
                qtnExpToExcMod.setSize(rs.getInt("Q_SIZE"));
                qtnExpToExcMod.setType(RutString.nullToStr(rs.getString("Q_TYPE")));
                qtnExpToExcMod.setPodSts(RutString.nullToStr(rs.getString("POD_STATUS")));
                qtnExpToExcMod.setLdnQty(rs.getInt("FULL_QTY"));
                qtnExpToExcMod.setMtQty(rs.getInt("MT_QTY"));
                qtnExpToExcMod.setRateType(RutString.nullToStr(rs.getString("SPECIAL_HANDLING"))); // --##02
                qtnExpToExcMod.setPortClass(RutString.nullToStr(rs.getString("Q_PORT_CLASS")));
                // --##02 BEGIN
                 if(commodityVec != null && commodityVec.size()>0){
                    QtnExportToExcelMod qtnExpToExcModForCommodity = null;
                    for(int currentCommodity = 0; commodityVec.size()>currentCommodity; currentCommodity++){
                       if(commodityVec.get(currentCommodity) != null){
                           qtnExpToExcModForCommodity = (QtnExportToExcelMod)commodityVec.get(currentCommodity);
                           if(qtnExpToExcModForCommodity.getCorridorSeq() == curQuotationCor){
                                qtnExpToExcMod.setCommodityCode(qtnExpToExcModForCommodity.getCommodityCode());    
                                qtnExpToExcMod.setCommodityrRateType(qtnExpToExcModForCommodity.getCommodityrRateType());
                                commodityVec.remove(currentCommodity);
                                break;
                           }
                       }
                    }//end for commodity
                }//end if commodity vector
                // --##02 END
//                qtnExpToExcMod.setCommodityrRateType(RutString.nullToStr(rs.getString("Q_RATE_TYPE")));
                qtnExpToExcMod.setPolDem(rs.getInt("EX_DEM"));
                qtnExpToExcMod.setPolDet(rs.getInt("EX_DET"));
                qtnExpToExcMod.setPodDem(rs.getInt("IMP_DEM"));
                qtnExpToExcMod.setPodDet(rs.getInt("IMP_DET"));

                surchargeVec = new Vector();
            } // # End IF 1 
            // Freight
             if(   !RutString.nullToStr(rs.getString("FREIGHT_SURCHARGE")).equals("") 
                &&  RutString.nullToStr(rs.getString("FREIGHT_SURCHARGE")).equals("F") 
                && !RutString.nullToStr(rs.getString("Q_LATEST_FLAG")).equals("") 
                &&  RutString.nullToStr(rs.getString("Q_LATEST_FLAG")).equals("Y")) {
                
                 qtnExpToExcMod.setFreight(rs.getDouble("Q_RATE"));
                 qtnExpToExcMod.setFreigthCur(RutString.nullToStr(rs.getString("Q_CURR")));
                 qtnExpToExcMod.setFreightPlaceOfSettle(RutString.nullToStr(rs.getString("PLS_SETTLE")));
             }
            // End Freight
            // Surcharge
            if( RutString.nullToStr(rs.getString("FREIGHT_SURCHARGE")).equals("S")){
                qtnExpToExcSurMod = new  QtnExportToExcelSurchargeMod();                                    
                    
                qtnExpToExcSurMod.setSurchargeCode(RutString.nullToStr(rs.getString("CODE")));
                qtnExpToExcSurMod.setSurchargeValue(rs.getDouble("Q_RATE"));
                qtnExpToExcSurMod.setSurcharePlaceOfSettle(RutString.nullToStr(rs.getString("PLS_SETTLE")));
                qtnExpToExcSurMod.setSurchargeCur(RutString.nullToStr(rs.getString("Q_CURR")));
                qtnExpToExcSurMod.setSurchargeInc(RutString.nullToStr(rs.getString("INC")));
                qtnExpToExcSurMod.setSurchargeSubToChg(RutString.nullToStr(rs.getString("Q_SUB_CHANGE"))); // -- ##
                surchargeVec.add(qtnExpToExcSurMod);
            }// End Surcharge 
             if(check == i){
                 qtnExpToExcMod.setSurchargeVec(surchargeVec);
                 quotationVec.add(qtnExpToExcMod);
             }
        }while(rs.next()); 
    }
       return quotationVec;
    }
    
    public Vector getQuotationSurcharge(String quotationNo,String sesseion,String userName) throws DataAccessException{
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationSurcharge]: With status: Started");
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationSurcharge]: With status: quotationNo "+quotationNo);
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationSurcharge]: With status: sesseion "+sesseion);
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationSurcharge]: With status: userName "+userName);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();                      
        Vector surchargeVec = new Vector();
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationSurcharge]: ");
        System.out.println("[QtnExportToExcelJdbcDao][getQuotationSurcharge]: Finished");
        
        sql.append("SELECT DISTINCT(NVL(CODE,'-')) CODE ");        
        sql.append(" FROM QTN_EXPORT_EXCEL ");
        sql.append(" WHERE QUOTATION_NO = :quotationNo ");
        sql.append(" AND SESSIONID = :sesseion ");
        sql.append(" AND USERNAME = :userName ");
        sql.append(" AND FREIGHT_SURCHARGE = 'S' ");
        
        sql.append(" ORDER BY CODE ");

        map.put("quotationNo",quotationNo);
        map.put("sesseion",sesseion);
        map.put("userName",userName);
        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        
        while(rs.next()){
            surchargeVec.add(RutString.nullToStr(rs.getString("CODE")));
        } 
       return surchargeVec;
    }
     // --##02 BEGIN
    private Vector getCommodityQtnExportToExcel(String quotationNo,String sesseion,String userName) throws DataAccessException{
        System.out.println("[QtnExportToExcelJdbcDao][getCommodityQtnExportToExcel]");
        System.out.println("[QtnExportToExcelJdbcDao][getCommodityQtnExportToExcel]: With status: quotationNo "+quotationNo);
        System.out.println("[QtnExportToExcelJdbcDao][getCommodityQtnExportToExcel]: With status: sesseion "+sesseion);
        System.out.println("[QtnExportToExcelJdbcDao][getCommodityQtnExportToExcel]: With status: userName "+userName);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
            
        Vector commodityVec = new Vector();
        QtnExportToExcelMod qtnExpToExcMod = null;
        System.out.println("[QtnExportToExcelJdbcDao][getCommodityQtnExportToExcel]: ");
        System.out.println("[QtnExportToExcelJdbcDao][getCommodityQtnExportToExcel]: Finished");
        
        sql.append("SELECT DISTINCT ");
        sql.append(" QC_SEQ ");
        sql.append(" ,Q_COM_CODE ");
        sql.append(" ,Q_RATE_TYPE ");
        sql.append(" FROM QTN_EXPORT_EXCEL ");
        sql.append(" WHERE QUOTATION_NO = :quotationNo ");
        sql.append(" AND SESSIONID = :sesseion ");
        sql.append(" AND USERNAME = :userName ");
        sql.append(" AND NVL(Q_RATE_TYPE,' ') <> ' ' ");
//        sql.append(" GROUP BY QC_SEQ,Q_COM_CODE,Q_RATE_TYPE ");
        sql.append(" ORDER BY QC_SEQ,Q_COM_CODE,Q_RATE_TYPE ");
        map.put("quotationNo",quotationNo);
        map.put("sesseion",sesseion);
        map.put("userName",userName);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        while (rs.next()){
            qtnExpToExcMod = new QtnExportToExcelMod();
            qtnExpToExcMod.setCorridorSeq(rs.getInt("QC_SEQ"));
            qtnExpToExcMod.setCommodityCode(RutString.nullToStr(rs.getString("Q_COM_CODE")));
            qtnExpToExcMod.setCommodityrRateType(RutString.nullToStr(rs.getString("Q_RATE_TYPE")));
            commodityVec.add(qtnExpToExcMod);
        }
        return commodityVec;
        
    }
     // --##02 END
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    }  
    
    private QtnExportToExcelMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        QtnExportToExcelMod qtnQuotationMod = new QtnExportToExcelMod();
        qtnQuotationMod.setQuotationNo(RutString.nullToStr(rs.getString("QUONO")));   
        qtnQuotationMod.setRefQuotation(RutString.nullToStr(rs.getString("QUOREF")));     
        qtnQuotationMod.setPartyCode(RutString.nullToStr(rs.getString("PARTYCODE")));  
        qtnQuotationMod.setPartyName(RutString.nullToStr(rs.getString("PARTYNAME")));         
        qtnQuotationMod.setStatus(RutString.nullToStr(rs.getString("STSDESC")));
        qtnQuotationMod.setEffDate(RutDate.toDateFormatFromYYYYMMDD( RutString.nullToStr(rs.getString("EFFDATE"))));
        qtnQuotationMod.setExpDate(RutDate.toDateFormatFromYYYYMMDD( RutString.nullToStr(rs.getString("EXPDATE"))));
        qtnQuotationMod.setFollowDate(RutDate.toDateFormatFromYYYYMMDD(RutString.nullToStr(rs.getString("FOLLOW"))));
        qtnQuotationMod.setTel(RutString.nullToStr(rs.getString("TEL")));
        qtnQuotationMod.setEmail(RutString.nullToStr(rs.getString("EMAIL")));
        qtnQuotationMod.setRemarks(RutString.nullToStr(rs.getString("RMK")));

        return qtnQuotationMod;
    }
    
}
