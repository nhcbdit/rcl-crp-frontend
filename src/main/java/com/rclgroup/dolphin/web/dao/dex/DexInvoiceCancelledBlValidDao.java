package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface DexInvoiceCancelledBlValidDao {
    /**
     * generate a DEX Invoice cancelled BL valid:
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateDexInvoiceCancelledBlValid(RrcStandardMod mod) throws DataAccessException;
}
