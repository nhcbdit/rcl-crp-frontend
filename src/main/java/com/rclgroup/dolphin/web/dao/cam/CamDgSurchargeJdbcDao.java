/*-----------------------------------------------------------------------------------------------------------  
CamDgSurchargeJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 04/12/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamDgSurchargeMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;


public class CamDgSurchargeJdbcDao extends RrcStandardDao implements CamDgSurchargeDao {
   
    public CamDgSurchargeJdbcDao() {
    }
   
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List listForHelpScreen(String sort, String sortBy) throws DataAccessException {
        System.out.println("[CamDgSurchargeJdbcDao][listForHelpScreen]: Started");
        StringBuffer sql = new StringBuffer();
        sql.append("select SURCHARGE_CODE ");
        sql.append("    ,SURCHARGE_DESCRIPTION ");
        sql.append("from VR_RCM_DG_SURCHARGE ");
        if (!RutString.isEmptyString(sort)) {
            sql.append("order by " + sort + " " + ((RutString.isEmptyString(sortBy)) ? "ASC" : sortBy));
        }
        System.out.println("[CamDgSurchargeJdbcDao][listForHelpScreen]: sql = " + sql.toString());
        System.out.println("[CamDgSurchargeJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            CamDgSurchargeMod bean = new CamDgSurchargeMod();
                            bean.setSurchargeCode(RutString.nullToStr(rs.getString("SURCHARGE_CODE")));
                            bean.setSurchargeDescription(RutString.nullToStr(rs.getString("SURCHARGE_DESCRIPTION")));
                            return bean;
                        }
                    });
    }
    
}


