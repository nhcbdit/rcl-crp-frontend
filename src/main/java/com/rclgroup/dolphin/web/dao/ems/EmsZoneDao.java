 /*-----------------------------------------------------------------------------------------------------------  
 EmsZoneDao.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
  Author Sopon Dee-udomvongsa 29/04/08 
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description 
 01 29/04/08  SPD                       Change to new framework
 02 23/08/13  NIP                       Add function for Area and Zone
 -----------------------------------------------------------------------------------------------------------*/  


 package com.rclgroup.dolphin.web.dao.ems;

 import java.util.List;

 import org.springframework.dao.DataAccessException;


 public interface EmsZoneDao {

     /**
      * check valid of zone
      * @param zone
      * @return valid of zone
      * @throws DataAccessException
      */
     public boolean isValid(String zone) throws DataAccessException;

     /**
      * check valid of zone with status
      * @param zone
      * @param status
      * @return valid of zone with status
      * @throws DataAccessException
      */
     public boolean isValid(String zone, String status) throws DataAccessException;

     /**
      * list zone records for help screen
      * @param find
      * @param search
      * @param wild
      * @return lsit of zone
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

     /**
      * list zone records for help screen with status
      * @param find
      * @param search
      * @param wild
      * @param regionCode
      * @param areaCode
      * @param controlZoneCode
      * @param status
      * @return list of zone with status
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String controlZoneCode, String status) throws DataAccessException;

    /**
     * @param line
     * @param region
     * @param agent
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    public String getZoneView(String line,String region,String agent,String fsc) throws DataAccessException;//##02
 }
