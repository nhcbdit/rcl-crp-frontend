 /*------------------------------------------------------
 CamOperatorDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sukit Narinsakchai 23/02/2009
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */
package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamOperatorDao {

    /**
     * check valid of Operator code 
     * @param operatorCode
     * @param status
     * @return valid of Operator code
     * @throws DataAccessException
     */
    public boolean isValid(String operatorCode, String status) throws DataAccessException;
    
    /**
     * list Operator Code records for help screen
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of Operator Code
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException;
}
