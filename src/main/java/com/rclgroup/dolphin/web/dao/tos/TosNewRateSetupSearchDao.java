/*-----------------------------------------------------------------------------------------------------------  
TosDataMaintenanceDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Dhruv Parekh 26/04/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import java.util.List;
import org.springframework.dao.DataAccessException;

public interface TosNewRateSetupSearchDao extends RriStandardDao{

    /**
     * @param port
     * @param terminal
     * @param operation
     * @param reference
     * @param decs
     * @param status
     * @return List
     * @throws DataAccessException
     */
    public List getRateSearchList(String port,String terminal,String operation,String reference,String decs,String status, String sortBy, String sortIn) throws DataAccessException;
    
    public List getOperationTypeList() throws DataAccessException;
}
