package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bsa.BsaPortCallMod;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaPortCallMod;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaVoyageHeaderMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BsaVsaPortCallJdbcDao extends RrcStandardDao implements BsaVsaPortCallDao{
    /* SQL for Fetching Result based on Search criteria */
    private RutDate rutDt = new RutDate();
    private SelectStoreProcedure selectStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private List lst = new ArrayList();
    /* Holds Current Page No incase of pagination */
    public int intCurrPage = 0;
    public BsaVsaPortCallJdbcDao(){
    }
    private BsaVsaPortCallDao bsaVsaPortCallDao;
    
    public void setBsaVsaPortCallDao(BsaVsaPortCallDao bsaVsaPortCallDao) {
        this.bsaVsaPortCallDao = bsaVsaPortCallDao;
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        selectStoreProcedure = new SelectStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
    }
    /**
     * find VSA Port Call of voyage header ID
     * @param vsaVoyageHeaderID
     * @return
     * @throws DataAccessException
     */
    public List listPortCallByKeyVsaVoyageHeaderID(String vsaVoyageHeaderID, String startingVessel) throws DataAccessException{
        return selectStoreProcedure.select(vsaVoyageHeaderID, startingVessel);
    }
    
    protected class SelectStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA_PORT_CALL.PRR_GET_VSA_PORT_CALL";
        protected  SelectStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            System.out.println("BsaVsaPortCallJdbcDao: SelectStoreProcedure declare parameter begin");
            declareParameter(new SqlOutParameter("P_O_V_VSA_PORT_CALL", OracleTypes.CURSOR,
                new RowMapper(){                                   
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    BsaVsaPortCallMod bsaVsaPortCallMod = new BsaVsaPortCallMod();

                    bsaVsaPortCallMod.setVsaPortCallId(String.valueOf(rs.getInt("PK_VSA_PORT_CALL_ID")));
                    bsaVsaPortCallMod.setVoyageHeaderId(String.valueOf(rs.getInt("FK_VOYAGE_HEADER_ID")));
                    bsaVsaPortCallMod.setBsaPortCallId(String.valueOf(rs.getInt("FK_BSA_PORT_CALL_ID")));
                    bsaVsaPortCallMod.setProformaRefNo(RutString.nullToStr(rs.getString("FK_PROFORMA_REF_NO")));
                    bsaVsaPortCallMod.setDnPort(RutString.nullToStr(rs.getString("DN_PORT")));
                    bsaVsaPortCallMod.setPortSequence(String.valueOf(rs.getInt("FK_PORT_SEQUENCE_NO")));
                    bsaVsaPortCallMod.setDnTerminal(RutString.nullToStr(rs.getString("DN_TERMINAL")));         
                    bsaVsaPortCallMod.setStartingVoyno(RutString.nullToStr(rs.getString("FK_STARTING_VOYNO")));
                    bsaVsaPortCallMod.setSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));
                    bsaVsaPortCallMod.setSlotTons(RutString.nullToStr(rs.getString("SLOT_TONS")));
                    bsaVsaPortCallMod.setSlotReferPlugs(RutString.nullToStr(rs.getString("SLOT_REEFER_PLUGS")));
                    bsaVsaPortCallMod.setAvgCocTeuWeight(RutString.nullToStr(rs.getString("AVG_COC_TEU_WEIGHT")));
                    bsaVsaPortCallMod.setAvgSocTeuWeight(RutString.nullToStr(rs.getString("AVG_SOC_TEU_WEIGHT")));
                    bsaVsaPortCallMod.setMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));
                    bsaVsaPortCallMod.setTolerant(RutString.nullToStr(rs.getString("TOLERANT")));
                    bsaVsaPortCallMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                    bsaVsaPortCallMod.setPortGroup(RutString.nullToStr(rs.getString("DN_PORT_GROUP_CODE")));
                    bsaVsaPortCallMod.setStartingVessel(RutString.nullToStr(rs.getString("FK_STARTING_VESSEL")));
                    
                    
                    System.out.print(bsaVsaPortCallMod.getVoyageHeaderId() );
                    System.out.print(bsaVsaPortCallMod.getBsaPortCallId() );
                    System.out.print(bsaVsaPortCallMod.getProformaRefNo() );
                    System.out.print(bsaVsaPortCallMod.getDnPort() );
                    System.out.print(bsaVsaPortCallMod.getDnTerminal() );
                    System.out.print(bsaVsaPortCallMod.getSlotTeu() );
                    System.out.print(bsaVsaPortCallMod.getSlotReferPlugs() );
                    System.out.print(bsaVsaPortCallMod.getAvgCocTeuWeight() );
                    System.out.print(bsaVsaPortCallMod.getAvgSocTeuWeight() );
                    System.out.print(bsaVsaPortCallMod.getMinTeu() );
                    System.out.print(bsaVsaPortCallMod.getTolerant() );

                    lst.add(bsaVsaPortCallMod);
                    return  bsaVsaPortCallMod;
                }
            })); 
            declareParameter(new SqlParameter("P_I_V_VOY_HEADER_ID", Types.INTEGER));
            declareParameter(new SqlParameter("P_I_V_STRART_VESSEL", Types.VARCHAR));
            System.out.println("BsaVsaPortCallJdbcDao: SelectStoreProcedure declare parameter end");
            compile();
        }
        protected List select(String vsaVoyageHeaderID, String startingVessel) {
            return  selecta(vsaVoyageHeaderID, startingVessel);
        }
        
        protected List selecta(String vsaVoyageHeaderID, String startingVessel) {
                Map inParameters = new HashMap();
                lst.clear();
                
                inParameters.put("P_I_V_VOY_HEADER_ID", new Integer(vsaVoyageHeaderID));
                inParameters.put("P_I_V_STRART_VESSEL", startingVessel);
                System.out.println("[BsaVsaPortCallJdbcDao][SelectStoreProcedure][select]: P_I_V_VOY_HEADER_ID = "+inParameters.get("P_I_V_VOY_HEADER_ID"));
            System.out.println("[BsaVsaPortCallJdbcDao][SelectStoreProcedure][select]: P_I_V_STRART_VESSEL = "+inParameters.get("P_I_V_STRART_VESSEL"));
                
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
                return lst;
            }else{
                return null;
            }
           
        }
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA_PORT_CALL.PRR_UPD_VSA_PORT_CALL";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            
            declareParameter(new SqlInOutParameter("P_VSA_PORT_CALL_ID", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_VOYAGE_HEADER_ID", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_BSA_PORT_CALL_ID", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_PROFORMA_REF_NO", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_DN_PORT",Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_PORT_SEQUENCE_NO", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_DN_TERMINAL", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_STARTING_VOYNO", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_SLOT_TEU", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_SLOT_TONS", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_SLOT_REEFER_PLUGS", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_AVG_COC_TEU_WEIGHT", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("P_AVG_SOC_TEU_WEIGHT", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("P_MIN_TEU", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_RECORD_STATUS", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_RECORD_CHANGE_USER", Types.VARCHAR)); 
            declareParameter(new SqlInOutParameter("P_RECORD_CHANGE_DATE", Types.TIMESTAMP)); 
            declareParameter(new SqlInOutParameter("P_TOLERANT", Types.NUMERIC));   
           
            compile();
        }
        
        protected boolean update(RrcStandardMod mod ) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod ) {
            boolean isSuccess = false;
            if((inputMod instanceof BsaVsaPortCallMod)&&(outputMod instanceof BsaVsaPortCallMod)){
                BsaVsaPortCallMod aInputMod = (BsaVsaPortCallMod)inputMod;
                BsaVsaPortCallMod aOutputMod = (BsaVsaPortCallMod)outputMod;
                Map inParameters = new HashMap();
                    
                    inParameters.put("P_VSA_PORT_CALL_ID", RutDatabase.integerToDb(aInputMod.getVsaPortCallId()));
                    inParameters.put("P_VOYAGE_HEADER_ID", RutDatabase.integerToDb(aInputMod.getVoyageHeaderId()));
                    inParameters.put("P_BSA_PORT_CALL_ID", RutDatabase.integerToDb(aInputMod.getBsaPortCallId()));
                    inParameters.put("P_PROFORMA_REF_NO", RutDatabase.stringToDb(aInputMod.getProformaRefNo()));
                    inParameters.put("P_DN_PORT", RutDatabase.stringToDb(aInputMod.getDnPort()));
                    inParameters.put("P_PORT_SEQUENCE_NO", RutDatabase.integerToDb(aInputMod.getPortSequence()));
                    inParameters.put("P_DN_TERMINAL", aInputMod.getDnTerminal());
                    inParameters.put("P_STARTING_VOYNO", RutDatabase.stringToDb(aInputMod.getStartingVoyno()));
                    inParameters.put("P_SLOT_TEU", RutDatabase.integerToDb(aInputMod.getSlotTeu()));
                    inParameters.put("P_SLOT_TONS", RutDatabase.integerToDb(aInputMod.getSlotTons()));
                    inParameters.put("P_SLOT_REEFER_PLUGS", RutDatabase.integerToDb(aInputMod.getSlotReferPlugs()));
                    inParameters.put("P_AVG_COC_TEU_WEIGHT", RutDatabase.bigDecimalToDb(aInputMod.getAvgCocTeuWeight()));
                    inParameters.put("P_AVG_SOC_TEU_WEIGHT", RutDatabase.bigDecimalToDb(aInputMod.getAvgSocTeuWeight()));
                    inParameters.put("P_MIN_TEU", RutDatabase.integerToDb(aInputMod.getMinTeu()));
                    inParameters.put("P_RECORD_STATUS",  RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                    inParameters.put("P_RECORD_CHANGE_USER", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                    inParameters.put("P_RECORD_CHANGE_DATE", aInputMod.getRecordChangeDate());
                    inParameters.put("P_TOLERANT", RutDatabase.bigDecimalToDb(aInputMod.getTolerant()));
              
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_VSA_PORT_CALL_ID = "+inParameters.get("P_VSA_PORT_CALL_ID"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_VOYAGE_HEADER_ID = "+inParameters.get("P_VOYAGE_HEADER_ID"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_BSA_PORT_CALL_ID = "+inParameters.get("P_BSA_PORT_CALL_ID"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_PROFORMA_REF_NO = "+inParameters.get("P_PROFORMA_REF_NO"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_DN_PORT = "+inParameters.get("P_DN_PORT"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_PORT_SEQUENCE_NO = "+inParameters.get("P_PORT_SEQUENCE_NO"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_DN_TERMINAL = "+inParameters.get("P_DN_TERMINAL"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_STARTING_VOYNO = "+inParameters.get("P_STARTING_VOYNO"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_SLOT_TEU = "+inParameters.get("P_SLOT_TEU"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_SLOT_TONS = "+inParameters.get("P_SLOT_TONS"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_SLOT_REEFER_PLUGS = "+inParameters.get("P_SLOT_REEFER_PLUGS"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_AVG_COC_TEU_WEIGHT = "+inParameters.get("P_AVG_COC_TEU_WEIGHT"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_AVG_SOC_TEU_WEIGHT = "+inParameters.get("P_AVG_SOC_TEU_WEIGHT"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_MIN_TEU = "+inParameters.get("P_MIN_TEU"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_RECORD_STATUS = "+inParameters.get("P_RECORD_STATUS")); 
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_RECORD_CHANGE_USER = "+inParameters.get("P_RECORD_CHANGE_USER")); 
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_RECORD_CHANGE_DATE = "+inParameters.get("P_RECORD_CHANGE_DATE"));
                    System.out.println("[BsaVsaPortCallJdbcDao][UpdateStoreProcedure][update]: P_TOLERANT = "+inParameters.get("P_TOLERANT")); 
               
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVsaPortCallId(RutDatabase.dbToStrInteger(outParameters, "P_VSA_PORT_CALL_ID"));
                    aOutputMod.setVoyageHeaderId(RutDatabase.dbToStrInteger(outParameters, "P_VOYAGE_HEADER_ID"));
                    aOutputMod.setBsaPortCallId(RutDatabase.dbToStrInteger(outParameters, "P_BSA_PORT_CALL_ID"));
                    aOutputMod.setProformaRefNo(RutDatabase.dbToString(outParameters, "P_PROFORMA_REF_NO"));
                    aOutputMod.setDnPort(RutDatabase.dbToString(outParameters, "P_DN_PORT"));
                    aOutputMod.setPortSequence(RutDatabase.dbToStrInteger(outParameters, "P_PORT_SEQUENCE_NO"));
                    aOutputMod.setDnTerminal(RutDatabase.dbToString(outParameters, "P_DN_TERMINAL"));
                    aOutputMod.setStartingVoyno(RutDatabase.dbToString(outParameters, "P_STARTING_VOYNO"));
                    aOutputMod.setSlotTeu(RutDatabase.dbToStrInteger(outParameters, "P_SLOT_TEU"));
                    aOutputMod.setSlotTons(RutDatabase.dbToStrInteger(outParameters, "P_SLOT_TONS"));
                    aOutputMod.setSlotReferPlugs(RutDatabase.dbToStrInteger(outParameters, "P_SLOT_REEFER_PLUGS"));
                    aOutputMod.setAvgCocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "P_AVG_COC_TEU_WEIGHT"));
                    aOutputMod.setAvgSocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "P_AVG_SOC_TEU_WEIGHT"));
                    aOutputMod.setMinTeu(RutDatabase.dbToStrInteger(outParameters, "P_MIN_TEU"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    aOutputMod.setTolerant(RutDatabase.dbToStrBigDecimal(outParameters, "P_TOLERANT"));
                    
                } 
                }
                return isSuccess;
                      
        }
    }
    
    
}
