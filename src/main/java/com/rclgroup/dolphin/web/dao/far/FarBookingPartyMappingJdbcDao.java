/*-----------------------------------------------------------------------------------------------------------  
FarBookingPartyMappingJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 15/07/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.far.FarBookingPartyMappingMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class FarBookingPartyMappingJdbcDao extends RrcStandardDao implements FarBookingPartyMappingDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    
    protected void initDao() throws Exception {
        super.initDao();        
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());        
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
    }
    
    //begin: Shipper Code Mapping
    public boolean insert(FarBookingPartyMappingMod bean) throws DataAccessException {
        bean.setRecordAddUser(this.getUserId());
        bean.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(bean);
    }
     
    public boolean update(FarBookingPartyMappingMod bean) throws DataAccessException {
        bean.setRecordAddUser(this.getUserId());
        bean.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(bean);
    }
     
    public boolean delete(FarBookingPartyMappingMod bean) throws DataAccessException {
        bean.setRecordAddUser(this.getUserId());
        bean.setRecordChangeUser(this.getUserId());
        return deleteStoreProcedure.delete(bean);
    }
    
    public FarBookingPartyMappingMod findByKey(String pkBookingPartyMapId) throws DataAccessException {
        FarBookingPartyMappingMod bean = null;
        if (!RutString.isEmptyString(pkBookingPartyMapId)) {
            StringBuffer sb = new StringBuffer();  
            sb.append("select PK_BOOKING_PARTY_MAP_ID ");
            sb.append("    ,BILL_TO_PARTY ");
            sb.append("    ,BOOKING_PARTY ");
            sb.append("    ,BOOKING_PARTY_NAME ");
            sb.append("    ,RECORD_STATUS ");
            sb.append("    ,RECORD_ADD_USER ");
            sb.append("    ,RECORD_ADD_DATE ");
            sb.append("    ,RECORD_CHANGE_USER ");
            sb.append("    ,RECORD_CHANGE_DATE ");
            sb.append("from VR_FAR_BOOKING_PARTY_MAP vr ");
            sb.append("where PK_BOOKING_PARTY_MAP_ID = :pkBookingPartyMapId ");
            
            try {
                bean = (FarBookingPartyMappingMod) getNamedParameterJdbcTemplate().queryForObject(
                       sb.toString(),
                       Collections.singletonMap("pkBookingPartyMapId", pkBookingPartyMapId),
                       new RowMapper() {
                           public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                               FarBookingPartyMappingMod bean = new FarBookingPartyMappingMod();
                               bean.setPkBookingPartyMapId(RutString.nullToStr(rs.getString("PK_BOOKING_PARTY_MAP_ID")));
                               bean.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                               bean.setBookingParty(RutString.nullToStr(rs.getString("BOOKING_PARTY")));
                               bean.setBookingPartyName(RutString.nullToStr(rs.getString("BOOKING_PARTY_NAME")));
                               bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                               bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                               bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                               bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                               bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                               return bean;
                           }
                       });
            } catch (EmptyResultDataAccessException e) {
                bean = null;
            }
        }
        return bean;
    }
    
    public String makeBookingPartyMappingSqlStatment(String criteriaBy, String billToParty, String bookingParty, String recordStatus, String permissionUser) {
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct PK_BOOKING_PARTY_MAP_ID ");
        sb.append("    ,BILL_TO_PARTY ");
        sb.append("    ,BOOKING_PARTY ");
        sb.append("    ,BOOKING_PARTY_NAME ");
        sb.append("    ,RECORD_STATUS ");
        sb.append("    ,RECORD_ADD_USER ");
        sb.append("    ,RECORD_ADD_DATE ");
        sb.append("    ,RECORD_CHANGE_USER ");
        sb.append("    ,RECORD_CHANGE_DATE ");
        sb.append("from VR_FAR_BOOKING_PARTY_MAP vr ");
        
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(billToParty)) {
            sbWhere.append(" and BILL_TO_PARTY = '"+billToParty+"' ");
        }
        if (!RutString.isEmptyString(bookingParty)) {
            sbWhere.append(" and BOOKING_PARTY = '"+bookingParty+"' ");
        }
        if (!RutString.isEmptyString(recordStatus)) {
            sbWhere.append(" and RECORD_STATUS = '"+recordStatus+"' ");
        }
        /*if (!RutString.isEmptyString(permissionUser)) {
            sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }*/
         
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
            sb.append(" [and :columnName :conditionWild :columnFind] ");
        } else {
            sb.append("[where :columnName :conditionWild :columnFind] ");
        }
         
        sb.append("[order by :sortBy :sortIn] ");
        return sb.toString();
    }
    
    public int findCountByStatement(String columnName, String conditionWild, String columnFind
        ,String billToParty, String bookingParty, String recordStatus, String permissionUser) throws DataAccessException 
    {
        System.out.println("[FarBookingPartyMappingJdbcDao][findCountByStatement]: Started");
        
        String sqlStatement = this.makeBookingPartyMappingSqlStatment(null, billToParty, bookingParty, recordStatus, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, null, null);
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[FarBookingPartyMappingJdbcDao][findCountByStatement]: sql = "+sqlStatement);
        System.out.println("[FarBookingPartyMappingJdbcDao][findCountByStatement]: Finished");
        
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStatement, new HashMap() , Integer.class);
    }
    
    public List findByStatement(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortByIn
        ,String billToParty, String bookingParty, String recordStatus, String permissionUser) throws DataAccessException 
    {
        System.out.println("[FarBookingPartyMappingJdbcDao][findByStatement]: Started");
        
        String sqlStatement = this.makeBookingPartyMappingSqlStatment(null, billToParty, bookingParty, recordStatus, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, sortBy, sortByIn);
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[FarBookingPartyMappingJdbcDao][findByStatement]: sql = "+sqlStatement);
        System.out.println("[FarBookingPartyMappingJdbcDao][findByStatement]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           FarBookingPartyMappingMod bean = new FarBookingPartyMappingMod();
                           bean.setPkBookingPartyMapId(RutString.nullToStr(rs.getString("PK_BOOKING_PARTY_MAP_ID")));
                           bean.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                           bean.setBookingParty(RutString.nullToStr(rs.getString("BOOKING_PARTY")));
                           bean.setBookingPartyName(RutString.nullToStr(rs.getString("BOOKING_PARTY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
    }
    //end: Shipper Code Mapping
    
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_BOOKING_PARTY_MAP.PRR_INS_BOOKING_PARTY_MAP";
          
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
            declareParameter(new SqlInOutParameter("pk_booking_party_map_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bill_to_party", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_booking_party", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }

        protected boolean insert(RrcStandardMod mod) {
            return insert(mod, mod);
        }
          
        protected boolean insert(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;                                    
            if (inputMod instanceof FarBookingPartyMappingMod && outputMod instanceof FarBookingPartyMappingMod) {
                FarBookingPartyMappingMod aInputMod = (FarBookingPartyMappingMod) inputMod;
                FarBookingPartyMappingMod aOutputMod = (FarBookingPartyMappingMod) outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("pk_booking_party_map_id", RutDatabase.integerToDb(aInputMod.getPkBookingPartyMapId()));
                inParameters.put("p_bill_to_party", RutDatabase.stringToDb(aInputMod.getBillToParty()));
                inParameters.put("p_booking_party", RutDatabase.stringToDb(aInputMod.getBookingParty()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_add_user", RutDatabase.stringToDb(aInputMod.getRecordAddUser()));
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: pk_booking_party_map_id = "+inParameters.get("pk_booking_party_map_id"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_bill_to_party = "+inParameters.get("p_bill_to_party"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_booking_party = "+inParameters.get("p_booking_party"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_record_add_user = "+inParameters.get("p_record_add_user"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_record_add_date = "+inParameters.get("p_record_add_date"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[FarBookingPartyMappingJdbcDao][InsertStoreProcedure][insert]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setPkBookingPartyMapId(RutDatabase.dbToStrInteger(outParameters, "pk_booking_party_map_id"));
                    aOutputMod.setBillToParty(RutDatabase.dbToString(outParameters, "p_bill_to_party"));
                    aOutputMod.setBookingParty(RutDatabase.dbToString(outParameters, "p_booking_party"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordAddUser(RutDatabase.dbToString(outParameters, "p_record_add_user"));
                    aOutputMod.setRecordAddDate(RutDatabase.dbToTimestamp(outParameters, "p_record_add_date"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_BOOKING_PARTY_MAP.PRR_UPD_BOOKING_PARTY_MAP";
          
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
            declareParameter(new SqlInOutParameter("pk_booking_party_map_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bill_to_party", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_booking_party", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }

        protected boolean update(RrcStandardMod mod) {
            return update(mod, mod);
        }
          
        protected boolean update(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;                                    
            if (inputMod instanceof FarBookingPartyMappingMod && outputMod instanceof FarBookingPartyMappingMod) {
                FarBookingPartyMappingMod aInputMod = (FarBookingPartyMappingMod) inputMod;
                FarBookingPartyMappingMod aOutputMod = (FarBookingPartyMappingMod) outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("pk_booking_party_map_id", RutDatabase.integerToDb(aInputMod.getPkBookingPartyMapId()));
                inParameters.put("p_bill_to_party", RutDatabase.stringToDb(aInputMod.getBillToParty()));
                inParameters.put("p_booking_party", RutDatabase.stringToDb(aInputMod.getBookingParty()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[FarBookingPartyMappingJdbcDao][UpdateStoreProcedure][update]: pk_booking_party_map_id = "+inParameters.get("pk_booking_party_map_id"));
                System.out.println("[FarBookingPartyMappingJdbcDao][UpdateStoreProcedure][update]: p_bill_to_party = "+inParameters.get("p_bill_to_party"));
                System.out.println("[FarBookingPartyMappingJdbcDao][UpdateStoreProcedure][update]: p_booking_party = "+inParameters.get("p_booking_party"));
                System.out.println("[FarBookingPartyMappingJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[FarBookingPartyMappingJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[FarBookingPartyMappingJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setPkBookingPartyMapId(RutDatabase.dbToStrInteger(outParameters, "pk_booking_party_map_id"));
                    aOutputMod.setBillToParty(RutDatabase.dbToString(outParameters, "p_bill_to_party"));
                    aOutputMod.setBookingParty(RutDatabase.dbToString(outParameters, "p_booking_party"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordAddUser(RutDatabase.dbToString(outParameters, "p_record_add_user"));
                    aOutputMod.setRecordAddDate(RutDatabase.dbToTimestamp(outParameters, "p_record_add_date"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }

    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_BOOKING_PARTY_MAP.PRR_DEL_BOOKING_PARTY_MAP";

        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("pk_booking_party_map_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
         
        protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof FarBookingPartyMappingMod) {
                FarBookingPartyMappingMod aInputMod = (FarBookingPartyMappingMod) inputMod;
                 
                Map inParameters = new HashMap();
                inParameters.put("pk_booking_party_map_id", RutDatabase.integerToDb(aInputMod.getPkBookingPartyMapId()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                 
                System.out.println("[FarBookingPartyMappingJdbcDao][DeleteStoreProcedure][delete]: pk_booking_party_map_id = "+inParameters.get("pk_booking_party_map_id"));
                System.out.println("[FarBookingPartyMappingJdbcDao][DeleteStoreProcedure][delete]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[FarBookingPartyMappingJdbcDao][DeleteStoreProcedure][delete]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                 
                execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
}
