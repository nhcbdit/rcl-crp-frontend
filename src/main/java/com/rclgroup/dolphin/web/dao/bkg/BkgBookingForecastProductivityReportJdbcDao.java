/*--------------------------------------------------------
BkgBookingForecastProductivityReportJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Leena Babu 25/07/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.util.RutDatabase;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BkgBookingForecastProductivityReportJdbcDao extends RrcStandardDao implements BkgBookingForecastProductivityReportDao {
    private InsertStoreProcedure insertStoreProcedure;

    public BkgBookingForecastProductivityReportJdbcDao() {
    }

    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
    }
    
    /**
     * Generates the data for booking forecast productivity report for the 
     *  given search criteria and stores in the temporary table in database
     *  
     * @param userCode of the logged in user
     * @param sessionId of the session
     * @param startDate of the first week
     * @param endDate of the first week
     * @param socCoc booking type
     * @param equipmentSize 
     * @param equipmentType
     * @param dischargePort
     * 
     * @return true if successful else false
     * */
    public boolean generateReportData(String userCode, String sessionId, 
                                      String startDate, String endDate, 
                                      String socCoc, String equipmentSize, 
                                      String equipmentType, 
                                      String dischargePort) {
       return insertStoreProcedure.insert(userCode, sessionId, startDate, endDate, 
                                    socCoc, equipmentSize, equipmentType, 
                                    dischargePort);

    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = 
            "RCLAPPS.PCR_BKG_FORECAST_PRODTVITY_RPT.GENERATE_REPORT_DATA";

        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            System.out.println("BkgBookingForecastProductivityReportJdbcDao: InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_start_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_end_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bkg_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_equipment_size", 
                                              Types.VARCHAR));
            declareParameter(new SqlParameter("p_equipment_type", 
                                              Types.VARCHAR));
            declareParameter(new SqlParameter("p_discharge_port", 
                                              Types.VARCHAR));
            compile();
            System.out.println("BkgBookingForecastProductivityReportJdbcDao InsertStoreProcedure declare parameter end");
        }

        protected boolean insert(String userCode, String sessionId, 
                                 String startDate, String endDate, 
                                 String socCoc, String equipmentSize, 
                                 String equipmentType, String dischargePort) {
            return insertPro(userCode, sessionId, startDate, endDate, socCoc, 
                             equipmentSize, equipmentType, dischargePort);
        }

        protected boolean  insertPro(String userCode, String sessionId, 
                                    String startDate, String endDate, 
                                    String socCoc, String equipmentSize, 
                                    String equipmentType, 
                                    String dischargePort) {
            System.out.println("BkgBookingForecastProductivityReportJdbcDao InsertStoreProcedure assign value to  parameter begin");

            Map inParameters = new HashMap();
            inParameters.put("p_user_id", RutDatabase.stringToDb(userCode));
            inParameters.put("p_session_id", 
                             RutDatabase.stringToDb(sessionId));
            inParameters.put("p_start_date", 
                             RutDatabase.stringToDb(startDate));
            inParameters.put("p_end_date", RutDatabase.stringToDb(endDate));
            inParameters.put("p_bkg_type", RutDatabase.stringToDb(socCoc));
            inParameters.put("p_equipment_size", 
                             RutDatabase.stringToDb(equipmentSize));
            inParameters.put("p_equipment_type", 
                             RutDatabase.stringToDb(equipmentType));
            inParameters.put("p_discharge_port", 
                             RutDatabase.stringToDb(dischargePort));

            Map outParameters = execute(inParameters);
            return true;
        }
    }
}
