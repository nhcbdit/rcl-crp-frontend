package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;

public interface BkgBookingSummaryAgainstBsaDao {
    /**
     * 
     * @param sessionId
     * @return
     * @throws DataAccessException
     */
    public boolean isValidWithSessionId(String sessionId) throws DataAccessException;
    
    /**
      * insert a Booking Against BSA record
      * @param mod a Booking Against BSA model
      * @return whether insertion is successful
      * @throws DataAccessException exception which client has to catch all following error messages:
      *                              error message: ORA-XXXXX (un_exceptional oracle error)
      */
     public boolean insert(RrcStandardMod mod) throws DataAccessException;
     
    /**
     * delete a Booking Against BSA record
     * @param mod a Booking Against BSA model
     * @return wheter deletion is successful
     * @throws DataAccessException exception dwhich client has to catch all following error messages:
     *                              error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
}
