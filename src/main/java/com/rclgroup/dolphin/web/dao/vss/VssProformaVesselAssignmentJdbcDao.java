/*-----------------------------------------------------------------------------------------------------------  
VssProformaVesselAssignmentJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 13/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.vss;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.vss.VssProformaVesselAssignmentMod;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class VssProformaVesselAssignmentJdbcDao extends RrcStandardDao implements VssProformaVesselAssignmentDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;

    public VssProformaVesselAssignmentJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
    }
    
    public List findBsaVesselTypeByVssProformaId(String vssProformaId, String recordStatus) throws DataAccessException {
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct upper(pva.bsa_vessel_type) as BSA_VESSEL_TYPE ");
        sql.append("from VR_VSS_PROFORMA_VESSEL_ASSIGN pva ");
        sql.append("where pva.VSS_PROFORMA_ID = :vssProformaId "); 
        sql.append("  and pva.RECORD_STATUS = :recordStatus ");   
        sql.append("order by upper(pva.bsa_vessel_type) ");
        HashMap map = new HashMap();
        map.put("vssProformaId", vssProformaId);
        map.put("recordStatus", recordStatus);
        
        System.out.println("[VssProformaVesselAssignmentJdbcDao][findBsaVesselTypeByVssProformaId]: vssProformaId = "+vssProformaId);
        System.out.println("[VssProformaVesselAssignmentJdbcDao][findBsaVesselTypeByVssProformaId]: recordStatus = "+recordStatus);
        
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           String bsaVesselType = RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE"));
                           return bsaVesselType;
                       }
                   });
    }

    public List findByVssProformaIdRecordStatus(String vssProformaId,String recordStatus) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VSS_VESSEL_ASSIGNMENT_ID ");
        sql.append("      ,VSS_PROFORMA_ID ");
        sql.append("      ,VESSEL_SEQ_NO "); 
        sql.append("      ,VESSEL_CODE ");     
        sql.append("      ,VESSEL_NAME ");
        sql.append("      ,BSA_VESSEL_TYPE "); 
        sql.append("      ,VALID_FROM ");        
        sql.append("      ,VALID_TO ");     
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_VSS_PROFORMA_VESSEL_ASSIGN ");
        sql.append("WHERE VSS_PROFORMA_ID = :vssProformaId ");   
        sql.append("  AND RECORD_STATUS = :recordStatus ");   
        sql.append("ORDER BY VSS_PROFORMA_ID,VESSEL_SEQ_NO,VALID_FROM,VALID_TO ");
        HashMap map = new HashMap();
        map.put("vssProformaId",vssProformaId);
        map.put("recordStatus",recordStatus);
        
        System.out.println("[VssProformaVesselAssignmentJdbcDao][findByVssProformaIdRecordStatus]:vssProformaId: "+vssProformaId);
        System.out.println("[VssProformaVesselAssignmentJdbcDao][findByVssProformaIdRecordStatus]:recordStatus: "+recordStatus);

        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           VssProformaVesselAssignmentMod vssProformaVesselAssignmentMod = new VssProformaVesselAssignmentMod();
                           vssProformaVesselAssignmentMod.setVssVesselAssignmentId(RutString.nullToStr(rs.getString("VSS_VESSEL_ASSIGNMENT_ID")));
                           vssProformaVesselAssignmentMod.setVssProformaId(RutString.nullToStr(rs.getString("VSS_PROFORMA_ID")));
                           vssProformaVesselAssignmentMod.setVesselSeqNo(RutString.nullToStr(rs.getString("VESSEL_SEQ_NO")));
                           vssProformaVesselAssignmentMod.setVesselCode(RutString.nullToStr(rs.getString("VESSEL_CODE")));
                           vssProformaVesselAssignmentMod.setVesselName(RutString.nullToStr(rs.getString("VESSEL_NAME")));
                           vssProformaVesselAssignmentMod.setBsaVesselType(RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE")));
                           vssProformaVesselAssignmentMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                           vssProformaVesselAssignmentMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                           vssProformaVesselAssignmentMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           vssProformaVesselAssignmentMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           vssProformaVesselAssignmentMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE")); 
                           vssProformaVesselAssignmentMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           vssProformaVesselAssignmentMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return vssProformaVesselAssignmentMod;
                       }
                   });
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }

    public boolean update(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }

    public boolean delete(RrcStandardMod mod) throws DataAccessException {
        return deleteStoreProcedure.delete(mod);
    }
    
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException{
        checkConstraintStoreProcedure.checkConstraint(mod);
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PRFRM_VSSL_ASSGN.PRR_INS_PRFRM_VSSL_ASSGN";
     
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_vss_vssl_assgn_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vss_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vessel_seq_no", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vessel_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_bsa_vessel_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_valid_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_valid_to", Types.DATE));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }

        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
    
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if((inputMod instanceof VssProformaVesselAssignmentMod)&&(outputMod instanceof VssProformaVesselAssignmentMod)){
                VssProformaVesselAssignmentMod aInputMod = (VssProformaVesselAssignmentMod)inputMod;
                VssProformaVesselAssignmentMod aOutputMod = (VssProformaVesselAssignmentMod)outputMod;
                Map inParameters = new HashMap(12);
////
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_vss_vssl_assgn_id:"+aInputMod.getVssVesselAssignmentId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_vss_proforma_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_vessel_seq_no:"+aInputMod.getVesselSeqNo());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_vessel_code:"+aInputMod.getVesselCode());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_bsa_vessel_type:"+aInputMod.getBsaVesselType());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////

                inParameters.put("p_vss_vssl_assgn_id", new Integer((RutString.nullToStr(aInputMod.getVssVesselAssignmentId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssVesselAssignmentId())));
                inParameters.put("p_vss_proforma_id", aInputMod.getVssProformaId());
                inParameters.put("p_vessel_seq_no", aInputMod.getVesselSeqNo());
                inParameters.put("p_vessel_code", aInputMod.getVesselCode());
                inParameters.put("p_bsa_vessel_type", aInputMod.getBsaVesselType());
                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVssVesselAssignmentId((RutString.nullToStr(((Integer)outParameters.get("p_vss_vssl_assgn_id")).toString())));
                    aOutputMod.setVssProformaId((RutString.nullToStr(((Integer)outParameters.get("p_vss_proforma_id")).toString())));
                    aOutputMod.setVesselSeqNo((RutString.nullToStr(((Integer)outParameters.get("p_vessel_seq_no")).toString())));
                    aOutputMod.setVesselCode((RutString.nullToStr((String)outParameters.get("p_vessel_code"))));
                    aOutputMod.setBsaVesselType((RutString.nullToStr((String)outParameters.get("p_bsa_vessel_type"))));
                    aOutputMod.setValidFrom(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_from")))));
                    aOutputMod.setValidTo(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_to")))));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                    aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                }
            }
            return isSuccess;
        }
    }
 
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PRFRM_VSSL_ASSGN.PRR_UPD_PRFRM_VSSL_ASSGN";
     
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_vss_vssl_assgn_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vss_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vessel_seq_no", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vessel_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_bsa_vessel_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_valid_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_valid_to", Types.DATE));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
     
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof VssProformaVesselAssignmentMod)&&(outputMod instanceof VssProformaVesselAssignmentMod)){
                VssProformaVesselAssignmentMod aInputMod = (VssProformaVesselAssignmentMod)inputMod;
                VssProformaVesselAssignmentMod aOutputMod = (VssProformaVesselAssignmentMod)outputMod;
                Map inParameters = new HashMap(10);
////
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_vss_vssl_assgn_id:"+aInputMod.getVssVesselAssignmentId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_vss_proforma_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_vessel_seq_no:"+aInputMod.getVesselSeqNo());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_vessel_code:"+aInputMod.getVesselCode());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_bsa_vessel_type:"+aInputMod.getBsaVesselType());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][UpdateStoreProcedure][update]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////                
            
                inParameters.put("p_vss_vssl_assgn_id", new Integer((RutString.nullToStr(aInputMod.getVssVesselAssignmentId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssVesselAssignmentId())));
                inParameters.put("p_vss_proforma_id", aInputMod.getVssProformaId());
                inParameters.put("p_vessel_seq_no", aInputMod.getVesselSeqNo());
                inParameters.put("p_vessel_code", aInputMod.getVesselCode());
                inParameters.put("p_bsa_vessel_type", aInputMod.getBsaVesselType());
                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVssVesselAssignmentId((RutString.nullToStr(((Integer)outParameters.get("p_vss_vssl_assgn_id")).toString())));
                    aOutputMod.setVssProformaId((RutString.nullToStr(((Integer)outParameters.get("p_vss_proforma_id")).toString())));
                    aOutputMod.setVesselSeqNo((RutString.nullToStr(((Integer)outParameters.get("p_vessel_seq_no")).toString())));
                    aOutputMod.setVesselCode((RutString.nullToStr((String)outParameters.get("p_vessel_code"))));
                    aOutputMod.setBsaVesselType((RutString.nullToStr((String)outParameters.get("p_bsa_vessel_type"))));
                    aOutputMod.setValidFrom(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_from")))));
                    aOutputMod.setValidTo(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_to")))));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                } 
            }
            return isSuccess;
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PRFRM_VSSL_ASSGN.PRR_DEL_PRFRM_VSSL_ASSGN";
     
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_vss_vssl_assgn_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof VssProformaVesselAssignmentMod){
                VssProformaVesselAssignmentMod aInputMod = (VssProformaVesselAssignmentMod)inputMod;
                Map inParameters = new HashMap(3);
////
                System.out.println("[VssProformaVesselAssignmentJdbcDao][DeleteStoreProcedure][delete]:p_vss_vssl_assgn_id:"+aInputMod.getVssVesselAssignmentId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////
                inParameters.put("p_vss_vssl_assgn_id", new Integer((RutString.nullToStr(aInputMod.getVssVesselAssignmentId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssVesselAssignmentId())));
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class CheckConstraintStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PRFRM_VSSL_ASSGN.PRR_CHK_CONSTRAINT_RECORDS";
     
        protected CheckConstraintStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
                declareParameter(new SqlParameter("p_vss_proforma_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));  
            
                //declareParameter(new SqlParameter("p_vss_vssl_assgn_id", Types.INTEGER));            
                //declareParameter(new SqlParameter("p_vessel_seq_no", Types.INTEGER));
                //declareParameter(new SqlParameter("p_vessel_code", Types.VARCHAR));
                //declareParameter(new SqlParameter("p_bsa_vessel_type", Types.VARCHAR));
                //declareParameter(new SqlParameter("p_valid_from", Types.DATE));
                //declareParameter(new SqlParameter("p_valid_to", Types.DATE));
                //declareParameter(new SqlParameter("p_record_status", Types.VARCHAR));
                      
            compile();
        }

        protected void checkConstraint(RrcStandardMod mod) {
            checkConstraint(mod,mod);
        }
        
        protected void checkConstraint(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            if((inputMod instanceof VssProformaVesselAssignmentMod)||(outputMod instanceof VssProformaVesselAssignmentMod)){
                VssProformaVesselAssignmentMod aInputMod = (VssProformaVesselAssignmentMod)inputMod;
                Map inParameters = new HashMap(12);
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vss_vssl_assgn_id:"+aInputMod.getVssVesselAssignmentId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vss_proforma_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vessel_seq_no:"+aInputMod.getVesselSeqNo());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vessel_code:"+aInputMod.getVesselCode());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_bsa_vessel_type:"+aInputMod.getBsaVesselType());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[VssProformaVesselAssignmentJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_record_status:"+aInputMod.getRecordStatus());

                inParameters.put("p_vss_proforma_id", aInputMod.getVssProformaId());
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                
                //inParameters.put("p_vss_vssl_assgn_id", new Integer((RutString.nullToStr(aInputMod.getVssVesselAssignmentId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssVesselAssignmentId())));                
                //inParameters.put("p_vessel_seq_no", aInputMod.getVesselSeqNo());
                //inParameters.put("p_vessel_code", aInputMod.getVesselCode());
                //inParameters.put("p_bsa_vessel_type", aInputMod.getBsaVesselType());
                //inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                //inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                //inParameters.put("p_record_status", aInputMod.getRecordStatus());
                                
                execute(inParameters);
            }
        }
    }
}

