/*-----------------------------------------------------------------------------------------------------------  
CamDgSurchargeDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 04/12/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamDgSurchargeDao {

    /**
     * list DG surcharge records for help screen
     * @param sort
     * @param sortBy
     * @return list of DG surcharge
     * @throws DataAccessException
     */
    public List listForHelpScreen(String sort, String sortBy) throws DataAccessException;

}


