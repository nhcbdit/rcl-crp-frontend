/*-----------------------------------------------------------------------------------------------------------  
CamUtilityHeaderJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 25/08/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 26/05/11 NIP                       Added function delete header  
02 05/06/11 NIP                       Added function findUtilityHeaderByCriteria
03 17/05/16 THIWAT1                   Added function findUtilityHeaderByModuleCode with screenCode
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.cam.CamUtilityHeaderMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class CamUtilityHeaderJdbcDao extends RrcStandardDao implements CamUtilityHeaderDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
       
    public CamUtilityHeaderJdbcDao() {
        super();
    }
   
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
  
    // ##01 BEGIN
    public boolean deleteUtilityHeaderByUtilityHdrId(RrcStandardMod mod,String utilityHdrId[]) throws DataAccessException {
        return deleteStoreProcedure.delete( mod,utilityHdrId);
    }
    // ##01 END
    
    public CamUtilityHeaderMod findUtilityHeaderByKey(String utilityHdrId) throws DataAccessException {
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByKey]: Started");
        
        CamUtilityHeaderMod bean = null;
        StringBuffer sb = new StringBuffer();  
        sb.append("select PK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,COUNTRY ");
        sb.append(" ,COUNTRY_NAME ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,UTILITY_NAME ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_HDR ");
        sb.append("where PK_CAM_UTILITY_HDR_ID = :utilityHdrId ");
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByKey]: sql = " + sb.toString());
        try {
            bean = (CamUtilityHeaderMod) getNamedParameterJdbcTemplate().queryForObject(
                   sb.toString(),
                   Collections.singletonMap("utilityHdrId", utilityHdrId),
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                           bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                           bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                           bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                           bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                           bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
        } catch (EmptyResultDataAccessException e) {
            bean = null;
        }
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByKey]: Finished");
        return bean;
    }
    
    public List findUtilityHeaderByModuleCode(String moduleCode, String country) throws DataAccessException {
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: Started");
        
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct PK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,COUNTRY ");
        sb.append(" ,COUNTRY_NAME ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,UTILITY_NAME ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_HDR ");
        
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(moduleCode)) {
            sbWhere.append(" and MODULE_CODE = '"+moduleCode+"' ");
        }
        if (!RutString.isEmptyString(country)) {
            sbWhere.append(" and COUNTRY = '"+country+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
        }
        
        sb.append("order by COUNTRY ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,SCREEN_CODE ");
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: sql = " + sb.toString());
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                           bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                           bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                           bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                           bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                           bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
    }
    
    public List findUtilityHeaderByModuleCode(String moduleCode, String country, String fsc) throws DataAccessException {
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: Started");
        
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct PK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,COUNTRY ");
        sb.append(" ,COUNTRY_NAME ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,UTILITY_NAME ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_HDR ");
        
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(moduleCode)) {
            sbWhere.append(" and MODULE_CODE = '"+moduleCode+"' ");
        }
        if (!RutString.isEmptyString(country)) {
            sbWhere.append(" and COUNTRY = '"+country+"' ");
        }
        if (!RutString.isEmptyString(fsc) && !RcmConstant.FSC_CODE_ALL.equals(fsc)) {
            sbWhere.append(" and FSC = '"+fsc+"' ");
        }
        sbWhere.append(" and RECORD_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
        }
        
        sb.append("order by COUNTRY ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,UTILITY_NAME ");
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: sql = " + sb.toString());
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                           bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                           bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                           bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                           bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                           bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
    }
    
    // #03 START
    public List findUtilityHeaderByModuleCode(String moduleCode, String country, String fsc, String screenCode) throws DataAccessException {
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: Started");
        
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct PK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,COUNTRY ");
        sb.append(" ,COUNTRY_NAME ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,UTILITY_NAME ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_HDR ");
        
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(moduleCode)) {
            sbWhere.append(" and MODULE_CODE = '"+moduleCode+"' ");
        }
        if (!RutString.isEmptyString(country)) {
            sbWhere.append(" and COUNTRY = '"+country+"' ");
        }
        if (!RutString.isEmptyString(fsc) && !RcmConstant.FSC_CODE_ALL.equals(fsc) && !country.equalsIgnoreCase("TH")) {
            sbWhere.append(" and FSC = '"+fsc+"' ");
        }
        if (!RutString.isEmptyString(screenCode)) {
            sbWhere.append(" and SCREEN_CODE = '"+screenCode+"' ");
        }
        sbWhere.append(" and RECORD_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
        }
        
        sb.append("order by COUNTRY ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,UTILITY_NAME ");
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: sql = " + sb.toString());
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByModuleCode]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                           bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                           bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                           bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                           bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                           bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
    }
    // #03 END
    
    public List findUtilityHeaderByCriteria(String moduleCode, String country, String status) throws DataAccessException {
            System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: Started");
            
            StringBuffer sb = new StringBuffer();  
            sb.append("select distinct PK_CAM_UTILITY_HDR_ID ");
            sb.append(" ,MODULE_CODE ");
            sb.append(" ,COUNTRY ");
            sb.append(" ,COUNTRY_NAME ");
            sb.append(" ,SCREEN_CODE ");
            sb.append(" ,FSC ");
            sb.append(" ,UTILITY_NAME ");
            sb.append(" ,RECORD_STATUS ");
            sb.append(" ,RECORD_ADD_USER ");
            sb.append(" ,RECORD_ADD_DATE ");
            sb.append(" ,RECORD_CHANGE_USER ");
            sb.append(" ,RECORD_CHANGE_DATE ");
            sb.append("from VR_CAM_UTILITY_HDR ");
            
            StringBuffer sbWhere = new StringBuffer();
            if (!RutString.isEmptyString(moduleCode)) {
                sbWhere.append(" and MODULE_CODE = '"+moduleCode+"' ");
            }
            if (!RutString.isEmptyString(country)) {
                sbWhere.append(" and COUNTRY = '"+country+"' ");
            }
            if (!RutString.isEmptyString(status)) {
                sbWhere.append(" and RECORD_STATUS = '"+status+"' ");
            }
            
            if (sbWhere != null && sbWhere.length() > 0) {
                String sqlWhere = "where " + sbWhere.substring(4);
                sb.append(sqlWhere);
            }
            
            sb.append("order by COUNTRY ");
            sb.append(" ,MODULE_CODE ");
            sb.append(" ,FSC ");
            sb.append(" ,SCREEN_CODE ");
            
            System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: sql = " + sb.toString());
            System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: Finished");
            return getNamedParameterJdbcTemplate().query(
                    sb.toString(),
                    new HashMap(),
                    new RowMapper(){
                           public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                               CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                               bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                               bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                               bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                               bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                               bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                               bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                               bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                               bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                               bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                               bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                               bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                               bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                               return bean;
                           }
                       });
        }
    
    public CamUtilityHeaderMod findUtilityHeaderByCriteria(String moduleCode, String screenCode,String country,String fsc,String status) throws DataAccessException {
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: Started");
    
        CamUtilityHeaderMod bean = null;
        StringBuffer sb = new StringBuffer();  
        sb.append("select PK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,COUNTRY ");
        sb.append(" ,COUNTRY_NAME ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,UTILITY_NAME ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_HDR ");
        //sb.append("where PK_CAM_UTILITY_HDR_ID = :utilityHdrId ");
        
         StringBuffer sbWhere = new StringBuffer();
         if (!RutString.isEmptyString(moduleCode)) {
             sbWhere.append(" and MODULE_CODE = '"+moduleCode+"' ");
         }
        if (!RutString.isEmptyString(screenCode)) {
            sbWhere.append(" and SCREEN_CODE = '"+screenCode+"' ");
        }
         if (!RutString.isEmptyString(country)) {
             sbWhere.append(" and COUNTRY = '"+country+"' ");
         }
         //if (!RutString.isEmptyString(fsc) && !RcmConstant.FSC_CODE_ALL.equals(fsc)) {
         if (!RutString.isEmptyString(fsc)) {
             sbWhere.append(" and FSC = '"+fsc+"' ");
         }
         sbWhere.append(" and RECORD_STATUS = '"+RcmConstant.RECORD_STATUS_ACTIVE+"' ");
         
         if (sbWhere != null && sbWhere.length() > 0) {
             String sqlWhere = "where " + sbWhere.substring(4);
             sb.append(sqlWhere);
         }
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: sql = " + sb.toString());
        try {
            bean = (CamUtilityHeaderMod) getNamedParameterJdbcTemplate().queryForObject(
                   sb.toString(),
                    new HashMap(),
                   //Collections.singletonMap("utilityHdrId", utilityHdrId),
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                           bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                           bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                           bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                           bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                           bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
        } catch (EmptyResultDataAccessException e) {
            bean = null;
        }
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: Finished");
        return bean;
    }
    
    // ##01 BEGIN
    public boolean isDuplicate(String country,String fsc,String moduleCode,String screenCode) throws DataAccessException {
        System.out.println("[CamUtilityHeaderJdbcDao][isDuplicate]: Started");
        
        boolean result = true;
        List beanList = null;
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct PK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,MODULE_CODE ");
        sb.append(" ,COUNTRY ");
        sb.append(" ,COUNTRY_NAME ");
        sb.append(" ,SCREEN_CODE ");
        sb.append(" ,FSC ");
        sb.append(" ,UTILITY_NAME ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_HDR ");
        
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(moduleCode)) {
            sbWhere.append(" and MODULE_CODE = '"+moduleCode+"' ");
        }
        if (!RutString.isEmptyString(country)) {
            sbWhere.append(" and COUNTRY = '"+country+"' ");
        }
        /*if (!RutString.isEmptyString(status)) {
            sbWhere.append(" and RECORD_STATUS = '"+status+"' ");
        }*/
         if (!RutString.isEmptyString(screenCode)) {
             sbWhere.append(" and SCREEN_CODE = '"+screenCode+"' ");
         }
          //if (!RutString.isEmptyString(fsc) && !RcmConstant.FSC_CODE_ALL.equals(fsc)) {
          if (!RutString.isEmptyString(fsc)) {
              sbWhere.append(" and FSC = '"+fsc+"' ");
          }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
        }
        
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: sql = " + sb.toString());
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderByCriteria]: Finished");
        beanList = getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityHeaderMod bean = new CamUtilityHeaderMod();
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_HDR_ID")));
                           bean.setModuleCode(RutString.nullToStr(rs.getString("MODULE_CODE")));
                           bean.setCountry(RutString.nullToStr(rs.getString("COUNTRY")));
                           bean.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
                           bean.setScreenCode(RutString.nullToStr(rs.getString("SCREEN_CODE")));
                           bean.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           bean.setUtilityName(RutString.nullToStr(rs.getString("UTILITY_NAME")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
        
        if(beanList==null || beanList.size()==0)// not duplicate
            result = false;
            
        System.out.println("[CamUtilityHeaderJdbcDao][isDuplicate]: Finished");
        return result;
    }

    public String findUtilityHeaderId(String moduleCode, String screenCode, String country, String fsc) {
        System.out.println("[CamUtilityHeaderJdbcDao][findUtilityHeaderId]: Started");
        Integer countObj = 0;
        String id;
        StringBuffer sb = new StringBuffer();  
        sb.append(" select PK_CAM_UTILITY_HDR_ID ");
        sb.append(" from CAM_UTILITY_HDR ");
        sb.append(" WHERE RECORD_STATUS = 'A' ");
        sb.append(" AND MODULE_CODE = :moduleCode ");
        sb.append(" AND SCREEN_CODE = :screenCode ");
        sb.append(" AND COUNTRY = :country ");
        sb.append(" AND FSC = :fsc ");
        sb.append(" AND ROWNUM = 1 ");
        try {
        HashMap map = new HashMap();
        map.put("moduleCode",moduleCode);
        map.put("screenCode",screenCode);
        map.put("country",country);
        map.put("fsc",fsc);
        countObj = (Integer) getNamedParameterJdbcTemplate().queryForObject(
                sb.toString(),
                map, Integer.class);
        } catch (EmptyResultDataAccessException e) {
        	countObj = 0;
        }
        System.out.println("[TosServiceJdbcDao][isCalculatedByDate]: Finished");
        return String.valueOf(countObj);
    }
    // ##01 END
    
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_UTILITY_HDR.PRR_INS_UTILITY_HDR";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_cam_utility_hdr_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_module_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_screen_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_country", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_utility_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod) {
            return insert(mod, mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof CamUtilityHeaderMod) && (outputMod instanceof CamUtilityHeaderMod)) {
                Map inParameters = new HashMap();
                CamUtilityHeaderMod aInputMod = (CamUtilityHeaderMod) inputMod;
                CamUtilityHeaderMod aOutputMod = (CamUtilityHeaderMod) outputMod;
                
                int utilityHdrId = RutString.toInteger(aInputMod.getUtilityHdrId(), 0);
                inParameters.put("p_cam_utility_hdr_id", new Integer(utilityHdrId));
                inParameters.put("p_module_code", aInputMod.getModuleCode());
                inParameters.put("p_screen_code", aInputMod.getScreenCode());
                inParameters.put("p_country", aInputMod.getCountry());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_utility_name", aInputMod.getUtilityName());
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_cam_utility_hdr_id = "+inParameters.get("p_cam_utility_hdr_id"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_module_code = "+inParameters.get("p_module_code"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_screen_code = "+inParameters.get("p_screen_code"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_country = "+inParameters.get("p_country"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_utility_name = "+inParameters.get("p_utility_name"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_add_user = "+inParameters.get("p_record_add_user"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_add_date = "+inParameters.get("p_record_add_date"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setUtilityHdrId(RutDatabase.dbToStrInteger(outParameters, "p_cam_utility_hdr_id"));
                    aOutputMod.setModuleCode(RutDatabase.dbToString(outParameters, "p_module_code"));
                    aOutputMod.setScreenCode(RutDatabase.dbToString(outParameters, "p_screen_code"));
                    aOutputMod.setCountry(RutDatabase.dbToString(outParameters, "p_country"));
                    aOutputMod.setFsc(RutDatabase.dbToString(outParameters, "p_fsc"));
                    aOutputMod.setUtilityName(RutDatabase.dbToString(outParameters, "p_utility_name"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordAddUser(RutDatabase.dbToString(outParameters, "p_record_add_user"));
                    aOutputMod.setRecordAddDate(RutDatabase.dbToTimestamp(outParameters, "p_record_add_date"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_UTILITY_HDR.PRR_UPD_UTILITY_HDR";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_cam_utility_hdr_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_module_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_screen_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_country", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_utility_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean update(RrcStandardMod mod) {
            return update(mod, mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof CamUtilityHeaderMod) && (outputMod instanceof CamUtilityHeaderMod)) {
                Map inParameters = new HashMap();
                CamUtilityHeaderMod aInputMod = (CamUtilityHeaderMod) inputMod;
                CamUtilityHeaderMod aOutputMod = (CamUtilityHeaderMod) outputMod;
                
                int utilityHdrId = RutString.toInteger(aInputMod.getUtilityHdrId(), 0);
                inParameters.put("p_cam_utility_hdr_id", new Integer(utilityHdrId));
                inParameters.put("p_module_code", aInputMod.getModuleCode());
                inParameters.put("p_screen_code", aInputMod.getScreenCode());
                inParameters.put("p_country", aInputMod.getCountry());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_utility_name", aInputMod.getUtilityName());
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_cam_utility_hdr_id = "+inParameters.get("p_cam_utility_hdr_id"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_module_code = "+inParameters.get("p_module_code"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_screen_code = "+inParameters.get("p_screen_code"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_country = "+inParameters.get("p_country"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_utility_name = "+inParameters.get("p_utility_name"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setUtilityHdrId(RutDatabase.dbToStrInteger(outParameters, "p_cam_utility_hdr_id"));
                    aOutputMod.setModuleCode(RutDatabase.dbToString(outParameters, "p_module_code"));
                    aOutputMod.setScreenCode(RutDatabase.dbToString(outParameters, "p_screen_code"));
                    aOutputMod.setCountry(RutDatabase.dbToString(outParameters, "p_country"));
                    aOutputMod.setFsc(RutDatabase.dbToString(outParameters, "p_fsc"));
                    aOutputMod.setUtilityName(RutDatabase.dbToString(outParameters, "p_utility_name"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
    
    // ##01 BEGIN
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_UTILITY_HDR.PRR_DEL_UTILITY_HDR_DTL";
        
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_cam_utility_hdr_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            
            compile();
        }
        
        protected boolean delete(RrcStandardMod mod,String utilityHdrId[]) {
            boolean isSuccess = false; 
            if ((mod instanceof CamUtilityHeaderMod)) {
                Map inParameters = new HashMap();
                CamUtilityHeaderMod getMod = (CamUtilityHeaderMod) mod;
                if (utilityHdrId!=null && utilityHdrId.length>0) {
                    /*String strUtilityHdrId = "";
                    for(int i=0;i<utilityHdrId.length;i++)
                        if(utilityHdrId[i]!=null && !utilityHdrId.equals(""))
                            if(i==0)
                                strUtilityHdrId = utilityHdrId[i];
                            else
                                strUtilityHdrId = "," + utilityHdrId[i];
                    inParameters.put("p_cam_utility_hdr_id", strUtilityHdrId);
                    
                    System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_cam_utility_hdr_id = "+inParameters.get("p_cam_utility_hdr_id"));
                    
                    Map outParameters = execute(inParameters);
                    if (outParameters.size() > 0) {
                        isSuccess = true;
                    }
                    */
                    
                    for(int i=0;i<utilityHdrId.length;i++)// loop delete
                        if(utilityHdrId[i]!=null && !utilityHdrId[i].equals("")){
                            inParameters.put("p_cam_utility_hdr_id", Integer.valueOf(utilityHdrId[i]));
                            inParameters.put("p_record_change_user", getMod.getRecordChangeUser());
                            inParameters.put("p_record_change_date", getMod.getRecordChangeDate());
                            
                            System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_cam_utility_hdr_id = "+inParameters.get("p_cam_utility_hdr_id"));
                            System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                            System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                             
                            Map outParameters = execute(inParameters);
//                            if (outParameters.size() > 0) {
                                isSuccess = true;
//                            }else
//                                return false;
                        }
                }
            }
            return isSuccess;
        }
    }
    // ##01 END
    
}
