/*-----------------------------------------------------------------------------------------------------------  
TosReportDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 23/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import org.springframework.dao.DataAccessException;


public interface TosReportDao {

    /**
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateNoVslClosed(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateNoVslNotClosed(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateNoVslNotCreate(RrcStandardMod mod) throws DataAccessException;
    
}
