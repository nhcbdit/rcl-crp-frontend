 /*-----------------------------------------------------------------------------------------------------------  
 EmsAreaDao.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 29/04/08 
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description 
 01 29/04/08  SPD                       Change to new framework
 02 23/08/13  NIP                       Add function for Area and Zone
 -----------------------------------------------------------------------------------------------------------*/   

  package com.rclgroup.dolphin.web.dao.ems;

  import java.util.List;
  import org.springframework.dao.DataAccessException;

 public interface EmsAreaDao {

     /**
      * check valid of area 
      * @param area
      * @return valid of area
      * @throws DataAccessException
      */
     public boolean isValid(String area) throws DataAccessException;

     /**
      * check valid of area code with status
      * @param area
      * @param status
      * @return valid of area code with status
      * @throws DataAccessException
      */
     public boolean isValid(String area, String status) throws DataAccessException;

     /**
      * list area records for help screen
      * @param find
      * @param search
      * @param wild
      * @return list of area
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

     /**
      * list area records for help screen with status
      * @param find
      * @param search
      * @param wild
      * @param regionCode
      * @param status
      * @return list of area with status
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String status) throws DataAccessException;

    /**
     * @param line
     * @param region
     * @param agent
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    public String getAreaView(String line,String region,String agent,String fsc) throws DataAccessException;//##02
      
  }
