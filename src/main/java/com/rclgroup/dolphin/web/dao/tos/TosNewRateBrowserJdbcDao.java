/*-----------------------------------------------------------------------------------------------------------  
TosNewRateBrowserJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 06/06/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.tos.TosNewRateBrowserMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupActivityMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupCategoryMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupMaintainanceMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

public class TosNewRateBrowserJdbcDao extends RrcStandardDao implements TosNewRateBrowserDao{
    public TosNewRateBrowserJdbcDao() {
        super();
    }
    
   
    public List getRateDtlList(String rateSeq, String port, String terminal, String operation) throws DataAccessException{
        StringBuffer sb = new StringBuffer();
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        sb.append("SELECT TOS_RATE_DTL.LINE,  ");
        sb.append("TOS_RATE_DTL.PORT,  ");
        sb.append("TOS_RATE_DTL.TERMINAL,  ");
        sb.append("TOS_RATE_DTL.SERVICE,  ");
        sb.append("TOS_RATE_DTL.OPR_CODE,  ");
        sb.append("TOS_RATE_DTL.VENDOR_CODE,  ");
        sb.append("TOS_RATE_DTL.VENDOR_SEQ,  ");
        sb.append("TOS_RATE_DTL.ACTIVITY_CODE,  ");
        sb.append("V_GENERAL_ACTIVITY.PARENT_ACTIVITY_CODE,  ");
        sb.append("V_GENERAL_ACTIVITY.DESCRIPTION,  ");
        sb.append("TOS_RATE_DTL.OCEAN_COAST,  ");
        sb.append("V_GENERAL_ACTIVITY.CHARGE_CODE,  ");
        sb.append("TOS_RATE_DTL.POD_OCEAN_COAST,  ");
        sb.append("TOS_RATE_DTL.ACTIVITY_RATE,  ");
        sb.append("V_GENERAL_ACTIVITY.RATE_BASIS,  ");
        sb.append("TOS_RATE_DTL.COL1_RATE,  ");
        sb.append("TOS_RATE_DTL.COL2_RATE,  ");
        sb.append("TOS_RATE_DTL.COL3_RATE,  ");
        sb.append("TOS_RATE_DTL.COL4_RATE,  ");
        sb.append("TOS_RATE_DTL.COL5_RATE,  ");
        sb.append("TOS_RATE_DTL.COL6_RATE,  ");
        sb.append("TOS_RATE_DTL.COL7_RATE,  ");
        sb.append("TOS_RATE_DTL.COL8_RATE,  ");
        sb.append("TOS_RATE_DTL.COL9_RATE,  ");
        sb.append("TOS_RATE_DTL.COL10_RATE,  ");
        sb.append("TOS_RATE_DTL.COL11_RATE,  ");
        sb.append("TOS_RATE_DTL.COL12_RATE,  ");
        sb.append("TOS_RATE_DTL.COL13_RATE,  ");
        sb.append("TOS_RATE_DTL.COL14_RATE,  ");
        sb.append("TOS_RATE_DTL.COL15_RATE,  ");
        sb.append("TOS_RATE_DTL.COL16_RATE,  ");
        sb.append("TOS_RATE_DTL.COL17_RATE,  ");
        sb.append("TOS_RATE_DTL.COL18_RATE,  ");
        sb.append("TOS_RATE_DTL.COL19_RATE,  ");
        sb.append("TOS_RATE_DTL.COL20_RATE,  ");
        sb.append("TOS_RATE_DTL.RECORD_STATUS,  ");
        sb.append("TOS_RATE_DTL.TOS_RATE_SEQNO, ");         
        sb.append("V_GENERAL_ACTIVITY.INCLUDE_YN,  ");
        sb.append("V_GENERAL_ACTIVITY.CATG_BASED_YN,  ");
        sb.append("V_GENERAL_ACTIVITY.AUTO_YN, ");
        sb.append("TOS_RATE_DTL.COC_SOC, ");
        sb.append("TOS_RATE_DTL.SHIP_TERM, ");
        sb.append("TOS_RATE_DTL.CRANE_TYPE,  ");
        sb.append("TOS_RATE_DTL.VESSEL_TYPE,  "); 
        sb.append("TOS_RATE_DTL.OPERATION_TYPE,  ");
        sb.append("TOS_RATE_DTL.TOS_RATE_SEQ_DTL,  ");
        sb.append("TOS_RATE_DTL.LOAD_DISCH_TRAN_FLAG, ");
        sb.append("TOS_RATE_DTL.AUTO_ACTIVITY, ");
        sb.append("TOS_RATE_DTL.DESTINATION_TYPE, ");
        sb.append("TOS_RATE_DTL.DESTINATION_CODE, ");
        sb.append("TOS_RATE_DTL.MOT, ");
        sb.append("TOS_RATE_DTL.WEIGHT_MIN, ");
        sb.append("TOS_RATE_DTL.WEIGHT_MAX, ");
        sb.append("TOS_RATE_DTL.WEEKDAY_WEEKEND, ");
        sb.append("TOS_RATE_DTL.HOLIDAY, ");
        sb.append("TOS_RATE_DTL.DAY_NIGHT, ");
        sb.append("TOS_RATE_DTL.REASON_CODE, ");
        sb.append("TOS_RATE_DTL.DOM_INTER ");
        sb.append("FROM SEALINER.TOS_RATE_DTL, SEALINER.V_GENERAL_ACTIVITY  ");
        sb.append("WHERE TOS_RATE_DTL.PORT = V_GENERAL_ACTIVITY.PORT AND  ");
        sb.append("  TOS_RATE_DTL.TERMINAL = V_GENERAL_ACTIVITY.TERMINAL AND  ");
        sb.append("  TOS_RATE_DTL.OPR_CODE = V_GENERAL_ACTIVITY.OPR_CODE AND  ");
        sb.append("  TOS_RATE_DTL.ACTIVITY_CODE = V_GENERAL_ACTIVITY.ACTIVITY_CODE AND  ");
        sb.append("  TOS_RATE_DTL.PORT = '" + port + "' AND  ");
        sb.append("  TOS_RATE_DTL.TERMINAL = '" + terminal + "' AND  ");
        sb.append("  TOS_RATE_DTL.OPR_CODE = '" + operation + "' AND  ");
        sb.append("  TOS_RATE_DTL.TOS_RATE_SEQNO = '" + rateSeq + "' AND  ");
        sb.append("  (V_GENERAL_ACTIVITY.NON_VOY_FLAG <> 'Y' OR V_GENERAL_ACTIVITY.NON_VOY_FLAG is null)  ");
        sb.append("ORDER BY V_GENERAL_ACTIVITY.EXPORT_IMPORT, V_GENERAL_ACTIVITY.ACTIVITY_CODE");
        
        System.out.println("[TosNewRateBrowserJdbcDao][getRateDtlList] SQL:"+sb.toString());
                
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int row) throws SQLException {
                        TosNewRateBrowserMod bean = new TosNewRateBrowserMod();
                        
                        bean.setLine(RutString.nullToStr(rs.getString("LINE")));  
                        bean.setPort(RutString.nullToStr(rs.getString("PORT"))); 
                        bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
                        bean.setService(RutString.nullToStr(rs.getString("SERVICE")));  
                        bean.setOpr_code(RutString.nullToStr(rs.getString("OPR_CODE")));  
                        bean.setVendor_code(RutString.nullToStr(rs.getString("VENDOR_CODE")));  
                        bean.setVendor_seq(RutString.nullToStr(rs.getString("VENDOR_SEQ")));  
                        bean.setActivity_code(RutString.nullToStr(rs.getString("ACTIVITY_CODE")));
                        bean.setParent_activity_code(RutString.nullToStr(rs.getString("PARENT_ACTIVITY_CODE")));
                        bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));  
                        bean.setOcean_coast(RutString.nullToStr(rs.getString("OCEAN_COAST")));  
                        bean.setCharge_code(RutString.nullToStr(rs.getString("CHARGE_CODE")));  
                        bean.setPod_ocean_coast(RutString.nullToStr(rs.getString("POD_OCEAN_COAST")));  
                        if(RutString.nullToStr(rs.getString("ACTIVITY_RATE")).equals("")||RutString.nullToStr(rs.getString("ACTIVITY_RATE")).equals("0")){bean.setActivity_rate("0.00"); } else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setActivity_rate(dc.format((rs.getDouble("ACTIVITY_RATE"))));}                        
                        bean.setRate_basis(RutString.nullToStr(rs.getString("RATE_BASIS")));  
                        if(RutString.nullToStr(rs.getString("COL1_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol1_rate( dc.format(rs.getDouble("COL1_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL2_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol2_rate( dc.format(rs.getDouble("COL2_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL3_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol3_rate( dc.format(rs.getDouble("COL3_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL4_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol4_rate( dc.format(rs.getDouble("COL4_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL5_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol5_rate( dc.format(rs.getDouble("COL5_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL6_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol6_rate( dc.format(rs.getDouble("COL6_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL7_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol7_rate( dc.format(rs.getDouble("COL7_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL8_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol8_rate( dc.format(rs.getDouble("COL8_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL9_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat();  dc.applyPattern("######.00"); bean.setCol9_rate( dc.format(rs.getDouble("COL9_RATE") ));}
                        if(RutString.nullToStr(rs.getString("COL10_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol10_rate(dc.format(rs.getDouble("COL10_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL11_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol11_rate(dc.format(rs.getDouble("COL11_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL12_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol12_rate(dc.format(rs.getDouble("COL12_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL13_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol13_rate(dc.format(rs.getDouble("COL13_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL14_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol14_rate(dc.format(rs.getDouble("COL14_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL15_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol15_rate(dc.format(rs.getDouble("COL15_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL16_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol16_rate(dc.format(rs.getDouble("COL16_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL17_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol17_rate(dc.format(rs.getDouble("COL17_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL18_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol18_rate(dc.format(rs.getDouble("COL18_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL19_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol19_rate(dc.format(rs.getDouble("COL19_RATE")));}
                        if(RutString.nullToStr(rs.getString("COL20_RATE")).equals("")){ }else{ DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setCol20_rate(dc.format(rs.getDouble("COL20_RATE")));}                        
                        bean.setRecord_status(RutString.nullToStr(rs.getString("RECORD_STATUS")));  
                        bean.setTos_rate_seqno(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));          
                        bean.setInclude_yn(RutString.nullToStr(rs.getString("INCLUDE_YN")));  
                        bean.setCatg_based_yn(RutString.nullToStr(rs.getString("CATG_BASED_YN")));  
                        bean.setAuto_yn(RutString.nullToStr(rs.getString("AUTO_YN"))); 
                        bean.setCoc_soc(RutString.nullToStr(rs.getString("COC_SOC"))); 
                        bean.setShip_term(RutString.nullToStr(rs.getString("SHIP_TERM")));
                        bean.setCrane_type(RutString.nullToStr(rs.getString("CRANE_TYPE")));  
                        bean.setVessel_type(RutString.nullToStr(rs.getString("VESSEL_TYPE")));   
                        bean.setOperation_type(RutString.nullToStr(rs.getString("OPERATION_TYPE")));  
                        bean.setTos_rate_seq_dtl(RutString.nullToStr(rs.getString("TOS_RATE_SEQ_DTL")));                        
                        bean.setLoad_disch_tran_flag(RutString.nullToStr(rs.getString("Load_disch_tran_flag"))); 
                        bean.setAuto_activity(RutString.nullToStr(rs.getString("Auto_activity"))); 
                        bean.setDestination_type(RutString.nullToStr(rs.getString("Destination_type"))); 
                        bean.setDestination_code(RutString.nullToStr(rs.getString("Destination_code"))); 
                        bean.setMot(RutString.nullToStr(rs.getString("Mot"))); 
                        if(RutString.nullToStr(rs.getString("Weight_min")).equals("") || RutString.nullToStr(rs.getString("Weight_min")).equals("0")){bean.setWeight_min("0.00");}else{DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setWeight_min(dc.format(rs.getDouble("Weight_min"))); }
                        if(RutString.nullToStr(rs.getString("Weight_max")).equals("") || RutString.nullToStr(rs.getString("Weight_max")).equals("0")){bean.setWeight_max("0.00");}else{DecimalFormat dc=new DecimalFormat(); dc.applyPattern("######.00"); bean.setWeight_max(dc.format(rs.getDouble("Weight_max"))); }                        
                        bean.setWeekday_weekend(RutString.nullToStr(rs.getString("Weekday_weekend"))); 
                        bean.setHoliday(RutString.nullToStr(rs.getString("Holiday"))); 
                        bean.setDay_night(RutString.nullToStr(rs.getString("Day_night"))); 
                        bean.setReason_code(RutString.nullToStr(rs.getString("Reason_code")));
                        bean.setDom_inter(RutString.nullToStr(rs.getString("DOM_INTER")));

                        return bean;
                    }
                });        
    
    }
    
    public List getCategoryList(String port, String terminal, String operation) throws DataAccessException{
        StringBuffer sb = new StringBuffer();
        
        sb.append("SELECT LINE, ");  
        sb.append("PORT, ");  
        sb.append("TERMINAL, ");
        sb.append("OPR_CODE, ");  
        sb.append("(CASE EMPTY_FULL ");
        sb.append("   WHEN 'E' THEN 'EMPTY' ");
        sb.append("   WHEN 'F' THEN 'FULL'  ");
        sb.append(" END) ||' '|| ");
        sb.append(" CAT_CODE ||' '|| ");
        sb.append(" SIZE_FLAG ||' '|| ");
        sb.append(" (CASE DECK_FLAG ");
        sb.append("   WHEN 'C' THEN 'CNTR' ");
        sb.append("   WHEN 'A' THEN 'AUTO' ");
        sb.append("   WHEN 'R' THEN 'RORO' ");
        sb.append(" END) "); 
        sb.append("CATG_COL, ");                                         
        sb.append("SEQ_NO, ");  
        sb.append("DESCRIPTION, ");  
        sb.append("RATE_BASIS, ");  
        sb.append("CHARGE_CODE, ");  
        sb.append("LINK_COLUMN, ");  
        sb.append("TEU_FACTOR, ");  
        sb.append("RECORD_STATUS ");  
        sb.append("FROM SEALINER.V_GENERAL_CATEGORY ");
        sb.append(" WHERE OPR_CODE = '"+operation+"' AND ");
        sb.append("       PORT = '"+port+"' AND ");
        sb.append("       TERMINAL = '"+terminal+"' ");
        sb.append("ORDER BY SEQ_NO ");

        System.out.println("[TosNewRateBrowserJdbcDao][getCategoryList] SQL:"+sb.toString());
        
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int row) throws SQLException {
                        TosNewRateSetupCategoryMod bean = new TosNewRateSetupCategoryMod();
                        bean.setLine(RutString.nullToStr(rs.getString("LINE")));  
                        bean.setPort(RutString.nullToStr(rs.getString("PORT")));  
                        bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));  
                        bean.setOpr_code(RutString.nullToStr(rs.getString("OPR_CODE"))); ; 
                        bean.setCatg_col(RutString.nullToStr(rs.getString("CATG_COL")));
                        bean.setSeq_no(RutString.nullToStr(rs.getString("SEQ_NO")));  
                        bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));  
                        bean.setRate_basis(RutString.nullToStr(rs.getString("RATE_BASIS")));  
                        bean.setCharge_code(RutString.nullToStr(rs.getString("CHARGE_CODE")));  
                        bean.setLink_column(RutString.nullToStr(rs.getString("LINK_COLUMN")));  
                        bean.setTeu_factor(RutString.nullToStr(rs.getString("TEU_FACTOR")));  
                        bean.setRecord_status(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                        return bean;
                    }
                });

    }
    
    public List getActivityList(String port, String terminal, String operation) throws DataAccessException{
        StringBuffer sb = new StringBuffer();
        
        sb.append("SELECT LINE, " );
        sb.append("PORT, " );
        sb.append("TERMINAL, " );
        sb.append("OPR_CODE, " );
        sb.append("ACTIVITY_CODE, " );
        sb.append("SEQ_NO, " );
        sb.append("SUB_SEQ_NO, " );
        sb.append("PARENT_ACTIVITY_CODE, " );
        sb.append("DESCRIPTION, " );
        sb.append("RATE_BASIS, " );
        sb.append("VOLUME_UNITS, " );
        sb.append("VOLUME_MOVES, " );
        sb.append("VOLUME_TONS, " );
        sb.append("CHARGE_CODE, " );
        sb.append("AUTO_YN, " );
        sb.append("LOAD_DISCH_FLAG, " );
        sb.append("INCLUDE_YN, " );
        sb.append("RECORD_STATUS, " );
        sb.append("DISCH_ACTIVITY, " );
        sb.append("REEFER_YN, " );
        sb.append("RECOVERY_YN, " );
        sb.append("RECOVERY_CHG_CODE, " );
        sb.append("CATG_BASED_YN " );
        sb.append("FROM SEALINER.V_GENERAL_ACTIVITY ");
        sb.append("WHERE (NON_VOY_FLAG <> 'Y' OR NON_VOY_FLAG is null) AND " );
        sb.append("OPR_CODE = '" +operation + "' AND " );
        sb.append("PORT = '" + port + "' AND " );
        sb.append("TERMINAL = '" +terminal + "' " );
        sb.append("ORDER BY EXPORT_IMPORT, ACTIVITY_CODE ");
        System.out.println("[TosNewRateBrowserJdbcDao][getActivityList] SQL:"+sb.toString());
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int row) throws SQLException {
                        TosNewRateSetupActivityMod bean = new TosNewRateSetupActivityMod();                        
                        bean.setLine(RutString.nullToStr(rs.getString("line")));
                        bean.setPort(RutString.nullToStr(rs.getString("port")));
                        bean.setTerminal(RutString.nullToStr(rs.getString("terminal")));
                        bean.setOpr_code(RutString.nullToStr(rs.getString("opr_code")));
                        bean.setActivity_code(RutString.nullToStr(rs.getString("activity_code")));
                        bean.setSeq_no(RutString.nullToStr(rs.getString("seq_no")));
                        bean.setSub_seq_no(RutString.nullToStr(rs.getString("sub_seq_no")));
                        bean.setParent_activity_code(RutString.nullToStr(rs.getString("parent_activity_code")));
                        bean.setDescription(RutString.nullToStr(rs.getString("description")));
                        bean.setRate_basis(RutString.nullToStr(rs.getString("rate_basis")));
                        bean.setVolume_units(RutString.nullToStr(rs.getString("volume_units")));
                        bean.setVolume_moves(RutString.nullToStr(rs.getString("volume_moves")));
                        bean.setVolume_tons(RutString.nullToStr(rs.getString("volume_tons")));
                        bean.setCharge_code(RutString.nullToStr(rs.getString("charge_code")));
                        bean.setAuto_yn(RutString.nullToStr(rs.getString("auto_yn")));
                        bean.setLoad_disch_flag(RutString.nullToStr(rs.getString("load_disch_flag")));
                        bean.setInclude_yn(RutString.nullToStr(rs.getString("include_yn")));
                        bean.setRecord_status(RutString.nullToStr(rs.getString("record_status")));
                        bean.setDisch_activity(RutString.nullToStr(rs.getString("disch_activity")));
                        bean.setReefer_yn(RutString.nullToStr(rs.getString("reefer_yn")));
                        bean.setRecovery_yn(RutString.nullToStr(rs.getString("recovery_yn")));
                        bean.setRecovery_chg_code(RutString.nullToStr(rs.getString("recovery_chg_code")));
                        bean.setCatg_based_yn(RutString.nullToStr(rs.getString("catg_based_yn")));
                        return bean;
                    }
                });    
    }
}
