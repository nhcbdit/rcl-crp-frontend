/*-----------------------------------------------------------------------------------------------------------  
VsaVsaServiceVariantVolumePortPairJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 22/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaServiceVariantVolumePortPairMod;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BsaVsaServiceVariantVolumePortPairJdbcDao extends RrcStandardDao implements BsaVsaServiceVariantVolumePortPairDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;
    private GetVsaPortPairListProcedure getVsaPortPairListProcedure;
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());    
        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
        getVsaPortPairListProcedure = new GetVsaPortPairListProcedure(getJdbcTemplate(), new VsaListRowMapper());
    } 
    
    public BsaVsaServiceVariantVolumePortPairJdbcDao() {
    }
    
    public List listForSearchScreen(String voyageHdrId, String startingVessel) {
        List resultList = new ArrayList();
        HashMap map = new HashMap();
        
        map.put("p_i_v_voy_hdr_id",voyageHdrId);
        map.put("p_i_v_starting_vessel",startingVessel);
        System.out.println("--p_i_v_voy_hdr_id--:" +voyageHdrId);
        System.out.println("--p_i_v_starting_vessel--:" +startingVessel);
        resultList = getVsaPortPairListProcedure.getVsaPortPairList(map);        
        
        return resultList;                
    }
    
    public void saveDetails(RrcStandardMod masterMod, String masterOperationFlag, List detailMods, Vector detailOperationFlags) throws CustomDataAccessException{
        //begin: save record change date for concurrency control
        Vector detailRecordChangeDates = new Vector();
        for(int i=0;i<detailMods.size();i++){
            BsaVsaServiceVariantVolumePortPairMod detailMod = (BsaVsaServiceVariantVolumePortPairMod)detailMods.get(i);
            detailRecordChangeDates.add(Timestamp.valueOf(detailMod.getRecordChangeDate().toString()));
        }        
        //end: save record change date for concurrency control
        
        BsaVsaServiceVariantVolumePortPairMod detailMod;
        StringBuffer errorMsgBuffer = new StringBuffer();        
        int index = 0;        
        
        for(index=0;index<detailMods.size();index++){
            try{
                detailMod = (BsaVsaServiceVariantVolumePortPairMod)detailMods.get(index);
                
                if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)){                    
                    insert(detailMod);
                }else if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)){
                    update(detailMod);
                }else if(detailMod.getAddDeleteFlag().equals(RutOperationFlagManager.OPERATION_FLAG_DELETE)){
                    System.out.println("delete");
                    delete(detailMod); 
                }
            }catch(CustomDataAccessException e){                                
                errorMsgBuffer.append(e.getMessages()+"%"+index+"&");
                System.out.println(errorMsgBuffer.toString());               
            }
                
        }
        
        if(!errorMsgBuffer.toString().equals("")){
            //begin: put record change date back
            for(int i=0;i<detailMods.size();i++){
                detailMod = (BsaVsaServiceVariantVolumePortPairMod)detailMods.get(i);
                detailMod.setRecordChangeDate((Timestamp)detailRecordChangeDates.get(i));
            } 
            //end: put record change date back
             
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }        
    }
    
    public boolean insert(RrcStandardMod mod) throws CustomDataAccessException{
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
           
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException{
        checkConstraintStoreProcedure.checkConstraint(mod);
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA_ROUTE.PRR_INS_VSA_ROUTE_TS";
         
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);                            
            
            declareParameter(new SqlInOutParameter("P_PK_VSA_ROUTE_ID" , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_POL_VSA_PORT_CALL_ID",Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_POD_VSA_PORT_CALL_ID",Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_TS_SERVICE"      , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TS_INDICATOR"    , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TS_PORT"         , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TS_GROUP"        , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_COC_TEU_LADEN"   , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_TON_LADEN"   , Types.DECIMAL));
            declareParameter(new SqlInOutParameter("P_COC_20GP"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_40GP"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_45HC"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_REEFER_PLUGS", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_TEU_MT"      , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_20MT"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_40MT"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_SOC_TEU_LADEN"   , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_SOC_TON_LADEN"   , Types.DECIMAL));
            declareParameter(new SqlInOutParameter("P_SOC_TEU_MT"      , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_RECORD_STATUS"   , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_RECORD_ADD_USER" , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_RECORD_ADD_DATE" , Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("P_RECORD_CHANGE_USER", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_RECORD_CHANGE_DATE", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("P_TOLERANT", Types.DECIMAL));
            
            compile();
        }

        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
     
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if((inputMod instanceof BsaVsaServiceVariantVolumePortPairMod)&&(outputMod instanceof BsaVsaServiceVariantVolumePortPairMod)){
                BsaVsaServiceVariantVolumePortPairMod aInputMod = (BsaVsaServiceVariantVolumePortPairMod)inputMod;
                BsaVsaServiceVariantVolumePortPairMod aOutputMod = (BsaVsaServiceVariantVolumePortPairMod)outputMod;
                Map inParameters = new HashMap();
                
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_pk_vsa_route_id: "+ (aInputMod.getVsaRouteId().equals("")?null:new Integer(aInputMod.getVsaRouteId())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_pol_vsa_port_call_id: "+ (aInputMod.getPolPortCallId().equals("")?null:new Integer(aInputMod.getPolPortCallId())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_pod_vsa_port_call_id: "+ (aInputMod.getPodPortCallId().equals("")?null:new Integer(aInputMod.getPodPortCallId())));               
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_ts_service: "+ aInputMod.getTsService());
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_ts_indicator: "+ aInputMod.getTsIndic());                            
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_ts_port: "+ aInputMod.getTsPort());               
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_teu_laden: "+ (aInputMod.getCocTeuFull().equals("")?new Integer("0"):new Integer(aInputMod.getCocTeuFull())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_ton_laden: "+ (aInputMod.getCocFullWeight().equals("")?new Double("0"):new Double(aInputMod.getCocFullWeight())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_20gp: "+ (aInputMod.getCoc20Gp().equals("")?new Integer("0"):new Integer(aInputMod.getCoc20Gp())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_40gp: "+ (aInputMod.getCoc40Gp().equals("")?new Integer("0"):new Integer(aInputMod.getCoc40Gp())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_45hc: "+ (aInputMod.getCoc45Hc().equals("")?new Integer("0"):new Integer(aInputMod.getCoc45Hc())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_reefer_plugs: "+ (aInputMod.getCocReefer().equals("")?new Integer("0"):new Integer(aInputMod.getCocReefer())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_teu_mt: "+ (aInputMod.getCocTeuMt().equals("")?new Integer("0"):new Integer(aInputMod.getCocTeuMt())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_20mt: "+ (aInputMod.getCocMt20().equals("")?new Integer("0"):new Integer(aInputMod.getCocMt20())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_coc_40mt: "+ (aInputMod.getCocMt40().equals("")?new Integer("0"):new Integer(aInputMod.getCocMt40())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_soc_teu_laden: "+ (aInputMod.getSocTeuFull().equals("")?new Integer("0"):new Integer(aInputMod.getSocTeuFull())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_soc_ton_laden: "+ (aInputMod.getSocFullWeight().equals("")?new Double("0"):new Double(aInputMod.getSocFullWeight())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_soc_teu_mt: "+ (aInputMod.getSocTeuMt().equals("")?new Integer("0"):new Integer(aInputMod.getSocTeuMt())));
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] P_RECORD_STATUS: "+ aInputMod.getRecordStatus());
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_record_add_user: "+ aInputMod.getRecordAddUser());
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_record_add_date: "+ aInputMod.getRecordAddDate());
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_record_change_user: "+ aInputMod.getRecordChangeUser());
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_record_change_date: "+ aInputMod.getRecordChangeDate());
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_tolelant: "+ (aInputMod.getTolelant().equals("")?new Double("0.00"):new Double(roundTwoDecimals(aInputMod.getTolelant()))) );
                System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao][insertProcedure] p_ts_group: "+ aInputMod.getTsGroup());
                
                inParameters.put("P_PK_VSA_ROUTE_ID", (aInputMod.getVsaRouteId().equals("")?null:new Integer(aInputMod.getVsaRouteId())));
                inParameters.put("P_POL_VSA_PORT_CALL_ID", (aInputMod.getPolPortCallId().equals("")?null:new Integer(aInputMod.getPolPortCallId())));
                inParameters.put("P_POD_VSA_PORT_CALL_ID", (aInputMod.getPodPortCallId().equals("")?null:new Integer(aInputMod.getPodPortCallId())));               
                inParameters.put("P_TS_SERVICE", aInputMod.getTsService());
                inParameters.put("P_TS_INDICATOR", aInputMod.getTsIndic());                            
                inParameters.put("P_TS_PORT", aInputMod.getTsPort());               
                inParameters.put("P_COC_TEU_LADEN", (aInputMod.getCocTeuFull().equals("")?new Integer("0"):new Integer(aInputMod.getCocTeuFull())));
                inParameters.put("P_COC_TON_LADEN", (aInputMod.getCocFullWeight().equals("")?new Integer("0"):new Integer(aInputMod.getCocFullWeight())));
                inParameters.put("P_COC_20GP", (aInputMod.getCoc20Gp().equals("")?new Integer("0"):new Integer(aInputMod.getCoc20Gp())));
                inParameters.put("P_COC_40GP", (aInputMod.getCoc40Gp().equals("")?new Integer("0"):new Integer(aInputMod.getCoc40Gp())));
                inParameters.put("P_COC_45HC", (aInputMod.getCoc45Hc().equals("")?new Integer("0"):new Integer(aInputMod.getCoc45Hc())));
                inParameters.put("P_COC_REEFER_PLUGS", (aInputMod.getCocReefer().equals("")?new Integer("0"):new Integer(aInputMod.getCocReefer())));
                inParameters.put("P_COC_TEU_MT", (aInputMod.getCocTeuMt().equals("")?new Integer("0"):new Integer(aInputMod.getCocTeuMt())));
                inParameters.put("P_COC_20MT", (aInputMod.getCocMt20().equals("")?new Integer("0"):new Integer(aInputMod.getCocMt20())));
                inParameters.put("P_COC_40MT", (aInputMod.getCocMt40().equals("")?new Integer("0"):new Integer(aInputMod.getCocMt40())));
                inParameters.put("P_SOC_TEU_LADEN", (aInputMod.getSocTeuFull().equals("")?new Integer("0"):new Integer(aInputMod.getSocTeuFull())));
                inParameters.put("P_SOC_TON_LADEN", (aInputMod.getSocFullWeight().equals("")?new Integer("0"):new Integer(aInputMod.getSocFullWeight())));
                inParameters.put("P_SOC_TEU_MT", (aInputMod.getSocTeuMt().equals("")?new Integer("0"):new Integer(aInputMod.getSocTeuMt())));
                inParameters.put("P_RECORD_STATUS", aInputMod.getRecordStatus());
                inParameters.put("P_RECORD_ADD_USER", aInputMod.getRecordAddUser());
                inParameters.put("P_RECORD_ADD_DATE", aInputMod.getRecordAddDate());
                inParameters.put("P_RECORD_CHANGE_USER", aInputMod.getRecordChangeUser());
                inParameters.put("P_RECORD_CHANGE_DATE", aInputMod.getRecordChangeDate());                
                inParameters.put("P_TOLERANT",(aInputMod.getTolelant().equals("")?new Double("0.00"):new Double(roundTwoDecimals(aInputMod.getTolelant()))) );
                inParameters.put("P_TS_GROUP", aInputMod.getTsGroup());   
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;                        
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                    aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                }
            }
            return isSuccess;
        }
    }
     
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA_ROUTE.PRR_UPD_VSA_ROUTE_TS";
     
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);           
            declareParameter(new SqlInOutParameter("P_PK_VSA_ROUTE_ID" , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_FK_POL_VSA_PORT_CALL_ID" , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_FK_POD_VSA_PORT_CALL_ID" , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_TS_PORT"         , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TS_SERVICE"      , Types.VARCHAR));            
            declareParameter(new SqlInOutParameter("P_TS_INDICATOR"    , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_COC_TEU_LADEN"   , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_TON_LADEN"   , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_20GP"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_40GP"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_45HC"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_REEFER_PLUGS", Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_TEU_MT"      , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_20MT"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_COC_40MT"        , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_SOC_TEU_LADEN"   , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_SOC_TON_LADEN"   , Types.DECIMAL));
            declareParameter(new SqlInOutParameter("P_SOC_TEU_MT"      , Types.INTEGER));
            declareParameter(new SqlInOutParameter("P_RECORD_STATUS"   , Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_RECORD_CHANGE_USER", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_RECORD_CHANGE_DATE", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("P_TOLERANT", Types.DECIMAL));
            declareParameter(new SqlInOutParameter("P_TS_GROUP", Types.VARCHAR));
            compile();
        }
     
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
     
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;                
            if((inputMod instanceof BsaVsaServiceVariantVolumePortPairMod)&&(outputMod instanceof BsaVsaServiceVariantVolumePortPairMod)){
                BsaVsaServiceVariantVolumePortPairMod aInputMod = (BsaVsaServiceVariantVolumePortPairMod)inputMod;
                BsaVsaServiceVariantVolumePortPairMod aOutputMod = (BsaVsaServiceVariantVolumePortPairMod)outputMod;
                Map inParameters = new HashMap(25);

                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_pk_vsa_route_id:"+aInputMod.getVsaRouteId()); 
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_ts_indicator:"+aInputMod.getTsIndic());
                if(aInputMod.getTsIndic().equalsIgnoreCase("to")){
                    System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:P_NEXT_SERVICE:"+aInputMod.getTsService());
                }else if (aInputMod.getTsIndic().equalsIgnoreCase("from")){
                    System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:P_PREV_SERVICE:"+aInputMod.getTsService());
                }                
                if(aInputMod.getTsIndic().equalsIgnoreCase("to")){
                    System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:P_PREV_POL:"+aInputMod.getTsPort());
                }else if (aInputMod.getTsIndic().equalsIgnoreCase("from")){
                    System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:P_NEXT_POD:"+aInputMod.getTsPort());
                }
                
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_fk_pol_vsa_port_call_id:"+aInputMod.getPolPortCallId());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_fk_pod_vsa_port_call_id:"+aInputMod.getPodPortCallId());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_teu_laden:"+aInputMod.getCocTeuFull());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_ton_laden:"+aInputMod.getCocFullWeight());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_20gp:"+aInputMod.getCoc20Gp());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_40gp:"+aInputMod.getCoc40Gp());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_45hc:"+aInputMod.getCoc45Hc());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_reefer_plugs:"+aInputMod.getCocReefer());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_teu_mt:"+aInputMod.getCocTeuMt());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_20mt:"+aInputMod.getCocMt20());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_coc_40mt:"+aInputMod.getCocMt40());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_soc_teu_laden:"+aInputMod.getSocTeuFull());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_soc_ton_laden:"+aInputMod.getSocFullWeight());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_soc_teu_mt:"+aInputMod.getSocTeuMt());                
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_record_change_date:"+aInputMod.getRecordChangeDate());
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][UpdateStoreProcedure][update]:p_toloelant:"+aInputMod.getTolelant());

                inParameters.put("P_PK_VSA_ROUTE_ID", (aInputMod.getVsaRouteId().equals(""))?null:new Integer(aInputMod.getVsaRouteId()));
                inParameters.put("P_FK_POL_VSA_PORT_CALL_ID", (aInputMod.getPolPortCallId().equals(""))?null:new Integer(aInputMod.getPolPortCallId()));
                inParameters.put("P_FK_POD_VSA_PORT_CALL_ID", (aInputMod.getPodPortCallId().equals(""))?null:new Integer(aInputMod.getPodPortCallId()));                
                inParameters.put("P_TS_PORT", aInputMod.getTsPort());
                inParameters.put("P_TS_SERVICE", aInputMod.getTsService());                
                inParameters.put("P_TS_INDICATOR", aInputMod.getTsIndic());
                inParameters.put("P_COC_TEU_LADEN", (aInputMod.getCocTeuFull().equals("")?new Integer("0"):new Integer(aInputMod.getCocTeuFull())));
                inParameters.put("P_COC_TON_LADEN", (aInputMod.getCocFullWeight().equals("")?new Double("0"):new Double(aInputMod.getCocFullWeight())));
                inParameters.put("P_COC_20GP", (aInputMod.getCoc20Gp().equals("")?new Integer("0"):new Integer(aInputMod.getCoc20Gp())));
                inParameters.put("P_COC_40GP", (aInputMod.getCoc40Gp().equals("")?new Integer("0"):new Integer(aInputMod.getCoc40Gp())));
                inParameters.put("P_COC_45HC", (aInputMod.getCoc45Hc().equals("")?new Integer("0"):new Integer(aInputMod.getCoc45Hc())));
                inParameters.put("P_COC_REEFER_PLUGS", (aInputMod.getCocReefer().equals("")?new Integer("0"):new Integer(aInputMod.getCocReefer())));
                inParameters.put("P_COC_TEU_MT", (aInputMod.getCocTeuMt().equals("")?new Integer("0"):new Integer(aInputMod.getCocTeuMt())));
                inParameters.put("P_COC_20MT", (aInputMod.getCocMt20().equals("")?new Integer("0"):new Integer(aInputMod.getCocMt20())));
                inParameters.put("P_COC_40MT", (aInputMod.getCocMt40().equals("")?new Integer("0"):new Integer(aInputMod.getCocMt40())));
                inParameters.put("P_SOC_TEU_LADEN", (aInputMod.getSocTeuFull().equals("")?new Integer("0"):new Integer(aInputMod.getSocTeuFull())));
                inParameters.put("P_SOC_TON_LADEN", (aInputMod.getSocFullWeight().equals("")?new Double("0"):new Double(aInputMod.getSocFullWeight())));
                inParameters.put("P_SOC_TEU_MT", (aInputMod.getSocTeuMt().equals("")?new Integer("0"):new Integer(aInputMod.getSocTeuMt())));
                inParameters.put("P_RECORD_STATUS", aInputMod.getRecordStatus());
                inParameters.put("P_RECORD_CHANGE_USER", aInputMod.getRecordChangeUser());
                inParameters.put("P_RECORD_CHANGE_DATE", aInputMod.getRecordChangeDate());
                String test = aInputMod.getTolelant();
                if(aInputMod.getTolelant()==null||aInputMod.getTolelant().equals("")){
                    aInputMod.setTolelant("0.00");            
                }else{
                    aInputMod.setTolelant(String.valueOf(roundTwoDecimals(aInputMod.getTolelant())));
                }
                inParameters.put("P_TOLERANT",new Double(roundTwoDecimals(aInputMod.getTolelant())));
                inParameters.put("P_TS_GROUP", aInputMod.getTsGroup());
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setTsService((RutString.nullToStr((String)outParameters.get("P_TS_SERVICE"))));
                    aOutputMod.setTsIndic((RutString.nullToStr((String)outParameters.get(" P_TS_INDICATOR"))));
                    aOutputMod.setTsPort((RutString.nullToStr((String)outParameters.get("P_TS_PORT"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("P_RECORD_CHANGE_USER"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("P_RECORD_CHANGE_DATE"))));
                    aOutputMod.setTsGroup((RutString.nullToStr((String)outParameters.get("P_TS_GROUP"))));
                } 
            }
            return isSuccess;
        }
    }
        
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA_ROUTE.PRR_DEL_VSA_ROUTE_TS";
     
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_pk_vsa_route_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof BsaVsaServiceVariantVolumePortPairMod){
                BsaVsaServiceVariantVolumePortPairMod aInputMod = (BsaVsaServiceVariantVolumePortPairMod)inputMod;
                Map inParameters = new HashMap(3);
                
                System.out.println("[VsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_pk_vsa_route_id:"+aInputMod.getVsaRouteId());
                System.out.println("[VsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
                
                inParameters.put("p_pk_vsa_route_id", new Integer((RutString.nullToStr(aInputMod.getVsaRouteId())).equals("")?"0":RutString.nullToStr(aInputMod.getVsaRouteId())).intValue() );
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
        
    protected class CheckConstraintStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA_ROUTE.PRR_CHK_CONSTRAINT_RECORDS";
     
        protected CheckConstraintStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);            
            declareParameter(new SqlInOutParameter("p_voy_hdr_id", Types.INTEGER));            
            compile();
        }

        protected void checkConstraint(RrcStandardMod mod) {
            checkConstraint(mod,mod);
        }
        
        protected void checkConstraint(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            if((inputMod instanceof BsaVsaServiceVariantVolumePortPairMod)||(outputMod instanceof BsaVsaServiceVariantVolumePortPairMod)){
                BsaVsaServiceVariantVolumePortPairMod aInputMod = (BsaVsaServiceVariantVolumePortPairMod)inputMod;
                Map inParameters = new HashMap(1);
                
                System.out.println("[VsaVsaServiceVariantVolumePortPairJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_voy_hdr_id:"+aInputMod.getVsaRouteId());
                inParameters.put("p_voy_hdr_id", aInputMod.getVsaRouteId());
                execute(inParameters);
            }
        }
    }
    
    protected double roundTwoDecimals(String number) {
        double result = Double.parseDouble(number);
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        
        return  Double.parseDouble(twoDForm.format(result).toString());
     }
    
    protected class GetVsaPortPairListProcedure extends StoredProcedure{
        private static final String SQL_VSA_PORT_PAIR_LIST = "PCR_BSA_VSA_ROUTE.PRR_VSA_GET_PORT_PAIR_LIST";
        
        protected GetVsaPortPairListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper) {
            super(jdbcTemplate, SQL_VSA_PORT_PAIR_LIST);
            declareParameter(new SqlOutParameter("p_o_v_port_pair_rec", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_i_v_voy_hdr_id", OracleTypes.INTEGER));
            declareParameter(new SqlParameter("p_i_v_starting_vessel", OracleTypes.VARCHAR));
            compile();
        }
        
        protected List getVsaPortPairList(Map mapParams){
            Map outMap = new HashMap();            
            List<BsaVsaServiceVariantVolumePortPairMod> returnList = new ArrayList<BsaVsaServiceVariantVolumePortPairMod>();
            System.out.println("[BsaVsaServiceVariantVolumePortPairJdbcDao] [getVsaPortPairList]");
            try{
                outMap = execute(mapParams);               
                returnList = (List<BsaVsaServiceVariantVolumePortPairMod>) outMap.get("p_o_v_port_pair_rec");                
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
        }
    }
    
    private class VsaListRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            BsaVsaServiceVariantVolumePortPairMod bean = new BsaVsaServiceVariantVolumePortPairMod();
            
            bean.setVsaRouteId(RutString.nullToStr(rs.getString("PK_VSA_ROUTE_ID")));                        
            bean.setPolPortCall(RutString.nullToStr(rs.getString("POL_PORT_CODE")));       
            bean.setPolPortCallId(RutString.nullToStr(rs.getString("POL_PORT_ID")));
            bean.setPodPortCall(RutString.nullToStr(rs.getString("POD_PORT_CODE")));
            bean.setPodPortCallId(RutString.nullToStr(rs.getString("POD_PORT_ID")));
            bean.setTsRecord(RutString.nullToStr(rs.getString("TS_RECORD")));
            bean.setTsIndic(RutString.nullToStr(rs.getString("TS_INDICATOR")));
            bean.setTsService(RutString.nullToStr(rs.getString("TS_SERVICE")));       
                  
            bean.setCocTeuFull(RutString.nullToStr(rs.getString("COC_TEU_LADEN")));
            bean.setCocFullWeight(RutString.nullToStr(rs.getString("COC_TON_LADEN")));
            bean.setCoc20Gp(RutString.nullToStr(rs.getString("COC_20GP")));
            bean.setCoc40Gp(RutString.nullToStr(rs.getString("COC_40GP")));
            bean.setCoc45Hc(RutString.nullToStr(rs.getString("COC_45HC")));
            bean.setCocReefer(RutString.nullToStr(rs.getString("COC_REEFER_PLUGS")));
            bean.setCocTeuMt(RutString.nullToStr(rs.getString("COC_TEU_MT")));
            bean.setCocMt20(RutString.nullToStr(rs.getString("COC_20MT")));
            bean.setCocMt40(RutString.nullToStr(rs.getString("COC_40MT")));
            bean.setSocTeuFull(RutString.nullToStr(rs.getString("SOC_TEU_LADEN")));
            bean.setSocFullWeight(RutString.nullToStr(rs.getString("SOC_TON_LADEN")));
            bean.setSocTeuMt(RutString.nullToStr(rs.getString("SOC_TEU_MT")));               
            bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
            bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
            bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
            bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
            bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));            
            if(RutString.nullToStr(rs.getString("TOLERANT")).equals("")||rs.getString("TOLERANT").equals("0")){
                bean.setTolelant("0.00");
            }else{
                DecimalFormat dc=new DecimalFormat();
                dc.applyPattern("###.00");
                bean.setTolelant(dc.format(rs.getDouble("TOLERANT")));
            }
            bean.setPortCallAvgCocTeu(RutString.nullToStr(rs.getString("AVG_COC_TEU_WEIGHT")));
            bean.setPortCallAvgSocTeu(RutString.nullToStr(rs.getString("AVG_SOC_TEU_WEIGHT")));
            bean.setPolSeqNo(RutString.nullToStr(rs.getString("POL_PORT_SEQ_NO")));
            bean.setPodSeqNo(RutString.nullToStr(rs.getString("POD_PORT_SEQ_NO")));
//            bean.setPolCallLv("P");
            bean.setPolTsFlag(RutString.nullToStr(rs.getString("POL_TRANSHIPMENT_FLAG")));
            bean.setPodTsFlag(RutString.nullToStr(rs.getString("POD_TRANSHIPMENT_FLAG")));
            bean.setPortGroupFlag(RutString.nullToStr(rs.getString("TS_PORT_GROUP_FLAG")));
            bean.setPolPortGroupFlag(RutString.nullToStr(rs.getString("POL_PORT_GROUP_FLAG")));
            bean.setPodPortGroupFlag(RutString.nullToStr(rs.getString("POD_PORT_GROUP_FLAG")));
            bean.setPodCallLv(RutString.nullToStr(rs.getString("POD_PORT_CALL_LEVEL")));
            bean.setPolCallLv(RutString.nullToStr(rs.getString("POL_PORT_CALL_LEVEL")));
            if(bean.getPortGroupFlag().equals("Y")){
                bean.setTsGroup(RutString.nullToStr(rs.getString("TS_PORT_CODE")));
                bean.setTsPort("");
            }else{
                bean.setTsPort(RutString.nullToStr(rs.getString("TS_PORT_CODE"))); 
                bean.setTsGroup("");
            }
            
            bean.setPortCallSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));
            bean.setPortCallMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));
            return bean;
        }
    }
}

