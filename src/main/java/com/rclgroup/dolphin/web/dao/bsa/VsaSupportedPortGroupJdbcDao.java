/*-----------------------------------------------------------------------------------------------------------  
VsaSupportedPortGroupJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 22/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bsa.VsaSupportedPortGroupMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class VsaSupportedPortGroupJdbcDao extends RrcStandardDao implements VsaSupportedPortGroupDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;

    public VsaSupportedPortGroupJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
    } 
    
    public List findByVsaModelIdRecordStatus(String vsaModelId,String recordStatus) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT BSA_SUPPORTED_PORT_GROUP_ID ");
        sql.append("      ,BSA_MODEL_ID ");
        sql.append("      ,PORT_GROUP_CODE "); 
        sql.append("      ,PORT_GROUP_NAME ");         
        sql.append("      ,PORT_GRP_SOC_COC ");     
        sql.append("      ,SERVICE ");   
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_BSA_SUPPORTED_PORT_GROUP ");
        sql.append("WHERE BSA_MODEL_ID = :vsaModelId ");   
        sql.append("  AND RECORD_STATUS = :recordStatus ");   
        sql.append("ORDER BY BSA_MODEL_ID,PORT_GROUP_CODE,nvl(SERVICE, ' ') ");
        HashMap map = new HashMap();
        map.put("vsaModelId",vsaModelId);
        map.put("recordStatus",recordStatus);
    ////
        System.out.println("[VsaSupportedPortGroupJdbcDao][findByVsaModelIdRecordStatus]:vsaModelId: "+vsaModelId);
        System.out.println("[VsaSupportedPortGroupJdbcDao][findByVsaModelIdRecordStatus]:recordStatus: "+recordStatus);
    ////
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           VsaSupportedPortGroupMod vsaSupportedPortGroupMod = new VsaSupportedPortGroupMod();
                           vsaSupportedPortGroupMod.setVsaSupportedPortGroupId(RutString.nullToStr(rs.getString("BSA_SUPPORTED_PORT_GROUP_ID")));
                           vsaSupportedPortGroupMod.setVsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                           vsaSupportedPortGroupMod.setPortGroupCode(RutString.nullToStr(rs.getString("PORT_GROUP_CODE")));
                           vsaSupportedPortGroupMod.setPortGroupName(RutString.nullToStr(rs.getString("PORT_GROUP_NAME")));
                           vsaSupportedPortGroupMod.setPortGroupSocCoc(RutString.nullToStr(rs.getString("PORT_GRP_SOC_COC")));
                           vsaSupportedPortGroupMod.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           vsaSupportedPortGroupMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           vsaSupportedPortGroupMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           vsaSupportedPortGroupMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE")); 
                           vsaSupportedPortGroupMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           vsaSupportedPortGroupMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return vsaSupportedPortGroupMod;
                       }
                   });
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }

    public boolean update(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }

    public boolean delete(RrcStandardMod mod) throws DataAccessException {
        return deleteStoreProcedure.delete(mod);
    }
    
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException{
        checkConstraintStoreProcedure.checkConstraint(mod);
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SUPPORTED_PORT_GROUP.PRR_INS_BSA_PORT_GROUP";
     
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_vsa_port_group_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port_group_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_grp_soc_coc", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }

        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
    
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if((inputMod instanceof VsaSupportedPortGroupMod)&&(outputMod instanceof VsaSupportedPortGroupMod)){
                VsaSupportedPortGroupMod aInputMod = (VsaSupportedPortGroupMod)inputMod;
                VsaSupportedPortGroupMod aOutputMod = (VsaSupportedPortGroupMod)outputMod;
                Map inParameters = new HashMap(10);
////
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_vsa_port_group_id:"+aInputMod.getVsaSupportedPortGroupId());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_vsa_model_id:"+aInputMod.getVsaModelId());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_port_group_code:"+aInputMod.getPortGroupCode());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_port_grp_soc_coc:"+aInputMod.getPortGroupSocCoc());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_service:"+aInputMod.getService());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VsaSupportedPortGroupJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////

                inParameters.put("p_vsa_port_group_id", new Integer((RutString.nullToStr(aInputMod.getVsaSupportedPortGroupId())).equals("")?"0":RutString.nullToStr(aInputMod.getVsaSupportedPortGroupId())));
                inParameters.put("p_vsa_model_id", aInputMod.getVsaModelId());
                inParameters.put("p_port_group_code", aInputMod.getPortGroupCode());
                inParameters.put("p_port_grp_soc_coc", aInputMod.getPortGroupSocCoc());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVsaSupportedPortGroupId((RutString.nullToStr(((Integer)outParameters.get("p_vsa_port_group_id")).toString())));
                    aOutputMod.setVsaModelId((RutString.nullToStr(((Integer)outParameters.get("p_vsa_model_id")).toString())));
                    aOutputMod.setPortGroupCode((RutString.nullToStr((String)outParameters.get("p_port_group_code"))));
                    aOutputMod.setPortGroupSocCoc((RutString.nullToStr((String)outParameters.get("p_port_grp_soc_coc"))));
                    aOutputMod.setService((RutString.nullToStr((String)outParameters.get("p_service"))));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                    aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                }
            }
            return isSuccess;
        }
    }
 
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SUPPORTED_PORT_GROUP.PRR_UPD_BSA_PORT_GROUP";
     
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);           
            declareParameter(new SqlInOutParameter("p_vsa_port_group_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port_group_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_grp_soc_coc", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
     
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof VsaSupportedPortGroupMod)&&(outputMod instanceof VsaSupportedPortGroupMod)){
                VsaSupportedPortGroupMod aInputMod = (VsaSupportedPortGroupMod)inputMod;
                VsaSupportedPortGroupMod aOutputMod = (VsaSupportedPortGroupMod)outputMod;
                Map inParameters = new HashMap(8);
////
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_vsa_port_group_id:"+aInputMod.getVsaSupportedPortGroupId());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_vsa_model_id:"+aInputMod.getVsaModelId());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_port_group_code:"+aInputMod.getPortGroupCode());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_port_grp_soc_coc:"+aInputMod.getPortGroupSocCoc());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_service:"+aInputMod.getService());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VsaSupportedPortGroupJdbcDao][UpdateStoreProcedure][update]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////            
                inParameters.put("p_vsa_port_group_id", new Integer((RutString.nullToStr(aInputMod.getVsaSupportedPortGroupId())).equals("")?"0":RutString.nullToStr(aInputMod.getVsaSupportedPortGroupId())));
                inParameters.put("p_vsa_model_id", aInputMod.getVsaModelId());
                inParameters.put("p_port_group_code", aInputMod.getPortGroupCode());
                inParameters.put("p_port_grp_soc_coc", aInputMod.getPortGroupSocCoc());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;                    
                    aOutputMod.setVsaSupportedPortGroupId((RutString.nullToStr(((Integer)outParameters.get("p_vsa_port_group_id")).toString())));
                    aOutputMod.setVsaModelId((RutString.nullToStr(((Integer)outParameters.get("p_vsa_model_id")).toString())));
                    aOutputMod.setPortGroupCode((RutString.nullToStr((String)outParameters.get("p_port_group_code"))));
                    aOutputMod.setPortGroupSocCoc((RutString.nullToStr((String)outParameters.get("p_port_grp_soc_coc"))));
                    aOutputMod.setService((RutString.nullToStr((String)outParameters.get("p_service"))));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                }
                
            }
            return isSuccess;
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SUPPORTED_PORT_GROUP.PRR_DEL_BSA_PORT_GROUP";
     
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_vsa_port_group_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof VsaSupportedPortGroupMod){
                VsaSupportedPortGroupMod aInputMod = (VsaSupportedPortGroupMod)inputMod;
                Map inParameters = new HashMap(3);
////
                System.out.println("[VsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_vsa_port_group_id:"+aInputMod.getVsaSupportedPortGroupId());
                System.out.println("[VsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////
                inParameters.put("p_vsa_port_group_id", new Integer((RutString.nullToStr(aInputMod.getVsaSupportedPortGroupId())).equals("")?"0":RutString.nullToStr(aInputMod.getVsaSupportedPortGroupId())));
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class CheckConstraintStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SUPPORTED_PORT_GROUP.PRR_CHK_CONSTRAINT_RECORDS";
     
        protected CheckConstraintStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);            
            declareParameter(new SqlInOutParameter("p_vsa_model_id", Types.INTEGER));            
            compile();
        }

        protected void checkConstraint(RrcStandardMod mod) {
            checkConstraint(mod,mod);
        }
        
        protected void checkConstraint(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            if((inputMod instanceof VsaSupportedPortGroupMod)||(outputMod instanceof VsaSupportedPortGroupMod)){
                VsaSupportedPortGroupMod aInputMod = (VsaSupportedPortGroupMod)inputMod;
                Map inParameters = new HashMap(1);
////
                System.out.println("[VsaSupportedPortGroupJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vsa_model_id:"+aInputMod.getVsaModelId());
////
                inParameters.put("p_vsa_model_id", aInputMod.getVsaModelId());
                execute(inParameters);
            }
        }
    }
}


