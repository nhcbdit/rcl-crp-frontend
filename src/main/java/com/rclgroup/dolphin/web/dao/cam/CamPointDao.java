 /*------------------------------------------------------
 CamPointDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sopon Dee-udomvongsa 18/07/2008   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamPointDao {
 
     public boolean isValid(String PointCode) throws DataAccessException;
     
     public boolean isValid(String PointCode, String status) throws DataAccessException;
     
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String controlZoneCode, String zoneCode) throws DataAccessException;
     
     public List listForHelpScreenWithFsc(String find, String search, String wild, String fsc) throws DataAccessException;
     
     public List listForHelpScreenWithControlZone(String find, String search, String wild, String controlZone) throws DataAccessException;
    
 }