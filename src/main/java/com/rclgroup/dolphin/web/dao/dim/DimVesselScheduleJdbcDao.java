/*-----------------------------------------------------------------------------------------------------------  
DimSvcVslVoyDirJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Nipun Sutes 03/05/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-      -TaskRef-       -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dim.DimVesselScheduleMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DimVesselScheduleJdbcDao extends RrcStandardDao implements DimVesselScheduleDao {
    
    public DimVesselScheduleJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }

    public boolean isValidForService(String service) {
        System.out.println("[DimVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE ");
        sql.append("from VR_DIM_SVC_VSL_VOY_DIR ");
        if ((service!=null)&&(!service.trim().equals(""))) {
            sql.append("where SERVICE = :service ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("service",service);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DimVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidForVoyage(String voyage) {
        System.out.println("[DimVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VOYAGE ");
        sql.append("from VR_DIM_SVC_VSL_VOY_DIR ");
        if ((voyage!=null)&&(!voyage.trim().equals(""))) {
            sql.append("where VOYAGE = :voyage ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("voyage",voyage);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DimVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidForVessel(String vessel) {
        System.out.println("[DimVesselScheduleJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select VESSEL ");
        sql.append("from VR_DIM_SVC_VSL_VOY_DIR ");
        if ((vessel!=null)&&(!vessel.trim().equals(""))) {
            sql.append("where VESSEL = :vessel ");
            sql.append("    and rownum = 1 ");
        }
        HashMap map = new HashMap();
        map.put("vessel",vessel);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[DimVesselScheduleJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List getEstimateTime(String service,String veesel,String voyage,String direct,String bl) throws DataAccessException{
        System.out.println("[DimVesselScheduleJdbcDao][getEstimateTime]: Started");        
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        List listEstTime = new ArrayList();
        sql.append("SELECT (subStr(ETA,7,2)||'/'||subStr(ETA,5,2)||'/'||subStr(ETA,1,4)) AS ETA ");
        sql.append("      ,(subStr(ETD,7,2)||'/'||subStr(ETD,5,2)||'/'||subStr(ETD,1,4)) AS ETD ");
        sql.append("FROM VR_DIM_REEFERCARGO_LIST ");    
        sql.append("WHERE 1 = 1 ");   
        
        if(bl != null && !bl.equals("")){
            sql.append(" AND BL_NO = :bl ");
            
            map.put("bl",bl);
        }else{
            sql.append(" AND SERVICE = :service ");
            sql.append(" AND VESSEL  = :veesel ");
            sql.append(" AND VOYAGE  = :voyage ");
            sql.append(" AND DIRECTION = :direct ");
            
            map.put("service",service);
            map.put("veesel",veesel);
            map.put("voyage",voyage);
            map.put("direct",direct);
        }
            sql.append(" GROUP BY ETA,ETD ");
        System.out.println("[DimVesselScheduleJdbcDao][getEstimateTime]: SQL: " + sql.toString());
        System.out.println("[DimVesselScheduleJdbcDao][getEstimateTime]: Finished");


        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        while (rs.next()){
            listEstTime.add(rs.getString("ETA"));
            listEstTime.add(rs.getString("ETD"));
        }
        return listEstTime;
    }
}
