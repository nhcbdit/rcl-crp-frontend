/*-----------------------------------------------------------------------------------------------------------  
VssProformaMasterJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 13/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.vss;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.dao.vss.VssProformaMasterDao;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.rcm.RcmModifiedObjectMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;
import com.rclgroup.dolphin.web.model.vss.VssProformaMasterMod;
import com.rclgroup.dolphin.web.model.vss.VssProformaVesselAssignmentMod;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class VssProformaMasterJdbcDao extends RrcStandardDao implements VssProformaMasterDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;
    
    private VssProformaVesselAssignmentDao vssProformaVesselAssignmentDao;

    public VssProformaMasterJdbcDao() {
    }
    
    public void setVssProformaVesselAssignmentDao(VssProformaVesselAssignmentDao vssProformaVesselAssignmentDao) {
        this.vssProformaVesselAssignmentDao = vssProformaVesselAssignmentDao;
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
    }
    
    private String createSqlCriteriaForHelp(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (!RutString.isEmptyString(find)) {
            if (search.equalsIgnoreCase("C")) {
                sqlCriteria = "AND SERVICE_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("D")) {
                sqlCriteria = "AND SERVICE_DESCRIPTION " + sqlWild;
            } else if (search.equalsIgnoreCase("P")) {
                sqlCriteria = "AND PROFORMA_REF_NO " + sqlWild;
            }
        }
        return sqlCriteria;
    }

    public List listForHelpScreen(String find,String search,String wild, String status, String serviceCode) throws DataAccessException {
        System.out.println("[VssProformaMasterJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteriaForHelp(find,search,wild);
        StringBuffer sql = new StringBuffer();
        sql.append("select vpm.VSS_PROFORMA_ID ");
        sql.append("    ,vpm.SERVICE_CODE ");
        sql.append("    ,vpm.SERVICE_DESCRIPTION ");
        sql.append("    ,vpm.PROFORMA_REF_NO ");
        sql.append("    ,vpm.DESIGNED_NO_OF_VESSELS ");
        sql.append("    ,vpm.VALID_FROM ");
        sql.append("    ,vpm.VALID_TO ");
        sql.append("    ,vpm.RECORD_STATUS ");
        sql.append("from VR_VSS_PROFORMA_MASTER vpm ");
        sql.append("WHERE RECORD_STATUS = :status ");
        
        // Filter by service code
        if (!RutString.isEmptyString(serviceCode)) {
            sql.append("    and vpm.SERVICE_CODE = '"+serviceCode+"' ");
        }
        
        sql.append(sqlCriteria);            
        sql.append("ORDER BY SERVICE_CODE ");
        sql.append("    ,PROFORMA_REF_NO ");
        System.out.println("[VssProformaMasterJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[VssProformaMasterJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    Collections.singletonMap("status", status),
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            VssProformaMasterMod bean = new VssProformaMasterMod();
                            bean.setVssProformaId(RutString.nullToStr(rs.getString("VSS_PROFORMA_ID")));
                            bean.setServiceCode(RutString.nullToStr(rs.getString("SERVICE_CODE")));
                            bean.setServiceDescription(RutString.nullToStr(rs.getString("SERVICE_DESCRIPTION")));
                            bean.setProformaReferenceNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                            bean.setDesignedNoOfVessels(RutString.nullToStr(rs.getString("DESIGNED_NO_OF_VESSELS")));
                            bean.setValidFrom(RutString.nullToStr(rs.getString("VALID_FROM")));
                            bean.setValidTo(RutString.nullToStr(rs.getString("VALID_TO")));
                            bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            return bean;
                        }
                    });
    }
    
    public List listForSearchScreen(RcmSearchMod searchMod, String validAt) throws DataAccessException{
        Map columnMap = new HashMap();
        //for both findIn, and sortBy
        columnMap.put("SP","SERVICE_CODE_AND_PROFORMA_REF_NO");
        columnMap.put("SE","SERVICE_CODE");
        columnMap.put("PR","PROFORMA_REF_NO");
        columnMap.put("DV","DESIGNED_NO_OF_VESSELS");
        columnMap.put("VF","VALID_FROM");
        columnMap.put("VT","VALID_TO");
        String sqlSearchCriteria = createSqlSearchCriteria(searchMod,columnMap,validAt);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VSS_PROFORMA_ID ");
        sql.append("      ,SERVICE_CODE ");
        sql.append("      ,PROFORMA_REF_NO ");
        sql.append("      ,DESIGNED_NO_OF_VESSELS ");  
        sql.append("      ,VALID_FROM ");        
        sql.append("      ,VALID_TO ");     
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_VSS_PROFORMA_MASTER ");
        sql.append(sqlSearchCriteria);
        System.out.println("[VssProformaMasterJdbcDao][listForSearchScreen]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           VssProformaMasterMod vssProformaMasterMod = new VssProformaMasterMod();
                           vssProformaMasterMod.setVssProformaId(RutString.nullToStr(rs.getString("VSS_PROFORMA_ID")));
                           vssProformaMasterMod.setServiceCode(RutString.nullToStr(rs.getString("SERVICE_CODE")));
                           vssProformaMasterMod.setProformaReferenceNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                           vssProformaMasterMod.setDesignedNoOfVessels(RutString.nullToStr(rs.getString("DESIGNED_NO_OF_VESSELS")));
                           vssProformaMasterMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                           vssProformaMasterMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                           vssProformaMasterMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           vssProformaMasterMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           vssProformaMasterMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE")); 
                           vssProformaMasterMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           vssProformaMasterMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return vssProformaMasterMod;
                       }
                   });
    }
    
    private String createSqlSearchCriteria(RcmSearchMod searchMod, Map columnMap, String validAt) {
        String sqlCriteria = "";
        String sqlWildWithUpperCase = "";
        String sqlSortByIn = "";
        String find = RutString.changeQuoteForSqlStatement(searchMod.getFind());
        String findIn = searchMod.getFindIn();
        String status = searchMod.getStatus();
        String sortBy = searchMod.getSortBy();
        String sortByIn = searchMod.getSortByIn();
        String wild = searchMod.getWild();

        if (wild.equalsIgnoreCase("ON")) {
//            if(findIn.equalsIgnoreCase("VF")||findIn.equalsIgnoreCase("VT")){
//                sqlWildWithUpperCase = "LIKE TO_DATE('" + find.toUpperCase() + "','DD/MM/YYYY') ";
//            }else{
                sqlWildWithUpperCase = "LIKE '%" + find.toUpperCase() + "%' ";
//            }
        } else {
//            if(findIn.equalsIgnoreCase("VF")||findIn.equalsIgnoreCase("VT")){
//                sqlWildWithUpperCase = "= TO_DATE('" + find.toUpperCase() + "','DD/MM/YYYY') ";
//            }else{
                sqlWildWithUpperCase = "= '" + find.toUpperCase() + "' ";
//            }
        }

        if (sortByIn.equalsIgnoreCase("ASC")) {
            sqlSortByIn = "ASC";
        } else if (sortByIn.equalsIgnoreCase("DESC")) {
            sqlSortByIn = "DESC";
        }
        
        sqlCriteria += " WHERE 1 = 1";
        if(find.trim().length() != 0){
            String sqlWild = sqlWildWithUpperCase;
            sqlCriteria += " AND UPPER(" + (String)columnMap.get(findIn) + ") " + sqlWild;      
        }
        
        if(status.equalsIgnoreCase("ALL")){
        }else if(status.equalsIgnoreCase("ACTIVE")){
            sqlCriteria += " AND RECORD_STATUS = 'A'";
        }else if(status.equalsIgnoreCase("SUSPENDED")){
            sqlCriteria += " AND RECORD_STATUS = 'S'";
        }
        
        if(validAt.trim().length() != 0){
            String jdbcValidAt = RutDate.getJdbcDateStringFromDefaultDateString(validAt);
            sqlCriteria += " AND (TO_DATE('"+ jdbcValidAt +"','YYYY-MM-DD') BETWEEN VALID_FROM AND VALID_TO)";
        }
        
        if(sortBy.trim().length() != 0){
            if(sortBy.trim().equalsIgnoreCase("SP")){
                sqlCriteria += " ORDER BY SERVICE_CODE " + sqlSortByIn + ", PROFORMA_REF_NO  " + sqlSortByIn;                                    
            }else{
                sqlCriteria += " ORDER BY " + (String)columnMap.get(sortBy) + " " + sqlSortByIn;
            }
        }
        return sqlCriteria;
    } 

    public VssProformaMasterMod findByKeyVssProformaId(String vssProformaId) throws DataAccessException{
        VssProformaMasterMod vssProformaMasterMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VSS_PROFORMA_ID ");
        sql.append("      ,SERVICE_CODE ");
        sql.append("      ,PROFORMA_REF_NO ");
        sql.append("      ,DESIGNED_NO_OF_VESSELS ");  
        sql.append("      ,VALID_FROM ");        
        sql.append("      ,VALID_TO ");     
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_VSS_PROFORMA_MASTER ");
        sql.append("WHERE VSS_PROFORMA_ID = :vssProformaId ");        
        sql.append("ORDER BY VSS_PROFORMA_ID ");
        try{
            vssProformaMasterMod = (VssProformaMasterMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("vssProformaId", vssProformaId),
                   new RowMapper() {
                        public Object mapRow(ResultSet rs,int rowNum) throws SQLException {
                            VssProformaMasterMod vssProformaMasterMod = new VssProformaMasterMod();
                            vssProformaMasterMod.setVssProformaId(RutString.nullToStr(rs.getString("VSS_PROFORMA_ID")));
                            vssProformaMasterMod.setServiceCode(RutString.nullToStr(rs.getString("SERVICE_CODE")));
                            vssProformaMasterMod.setProformaReferenceNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                            vssProformaMasterMod.setDesignedNoOfVessels(RutString.nullToStr(rs.getString("DESIGNED_NO_OF_VESSELS")));
                            vssProformaMasterMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                            vssProformaMasterMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                            vssProformaMasterMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            vssProformaMasterMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                            vssProformaMasterMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                            vssProformaMasterMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                            vssProformaMasterMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                            return vssProformaMasterMod;
                        }
                    });
        }catch (EmptyResultDataAccessException e) {
            vssProformaMasterMod = null;
        }
        return vssProformaMasterMod;
    }
    
    public VssProformaMasterMod findByProformaNo(String proformaNo) throws DataAccessException {
        VssProformaMasterMod vssProformaMasterMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VSS_PROFORMA_ID ");
        sql.append("    ,SERVICE_CODE ");
        sql.append("    ,PROFORMA_REF_NO ");
        sql.append("    ,DESIGNED_NO_OF_VESSELS ");  
        sql.append("    ,VALID_FROM ");        
        sql.append("    ,VALID_TO ");     
        sql.append("    ,RECORD_STATUS ");
        sql.append("    ,RECORD_ADD_USER ");
        sql.append("    ,RECORD_ADD_DATE ");
        sql.append("    ,RECORD_CHANGE_USER ");
        sql.append("    ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_VSS_PROFORMA_MASTER ");
        sql.append("WHERE PROFORMA_REF_NO = :proformaNo ");
        sql.append("    and rownum = 1 ");
        sql.append("ORDER BY VSS_PROFORMA_ID ");
        try{
            vssProformaMasterMod = (VssProformaMasterMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("proformaNo", proformaNo),
                   new RowMapper() {
                        public Object mapRow(ResultSet rs,int rowNum) throws SQLException {
                            VssProformaMasterMod vssProformaMasterMod = new VssProformaMasterMod();
                            vssProformaMasterMod.setVssProformaId(RutString.nullToStr(rs.getString("VSS_PROFORMA_ID")));
                            vssProformaMasterMod.setServiceCode(RutString.nullToStr(rs.getString("SERVICE_CODE")));
                            vssProformaMasterMod.setProformaReferenceNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                            vssProformaMasterMod.setDesignedNoOfVessels(RutString.nullToStr(rs.getString("DESIGNED_NO_OF_VESSELS")));
                            vssProformaMasterMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                            vssProformaMasterMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                            vssProformaMasterMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            vssProformaMasterMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                            vssProformaMasterMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                            vssProformaMasterMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                            vssProformaMasterMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                            return vssProformaMasterMod;
                        }
                    });
        }catch (EmptyResultDataAccessException e) {
            vssProformaMasterMod = null;
        }
        return vssProformaMasterMod;
    }

    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, List dtlModifiedMods) throws DataAccessException{
        VssProformaMasterMod mod = (VssProformaMasterMod)masterMod;
        VssProformaVesselAssignmentMod detailMod = null;
        RcmModifiedObjectMod modifiedMod = null;
        
        // save record change date
        Timestamp masterRecChangeDate = mod.getRecordChangeDate();
        
        //begin: save master
        StringBuffer errorMsgBuffer = new StringBuffer();
        try {
            if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(masterOperationFlag)) {
                this.insert(masterMod);
            } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(masterOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_MODIFY.equals(masterOperationFlag)) {
                this.update(masterMod);
            }
        } catch (CustomDataAccessException e) {
            errorMsgBuffer.append(e.getMessages()+"&");
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        //end: save master
        
        String strOperationFlag = null;
        System.out.println("[VssProformaMasterJdbcDao][saveMasterDetails] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            mod.setRecordChangeDate(masterRecChangeDate);
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        
        //begin: save detail in master
        int index = 0;
        for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
            modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
            if (modifiedMod != null && modifiedMod.getObjectMod() instanceof VssProformaVesselAssignmentMod) { //if 1
             
                try {
                    strOperationFlag = modifiedMod.getOperationFlag();
                    detailMod = (VssProformaVesselAssignmentMod) modifiedMod.getObjectMod();
                    detailMod.setVssProformaId(mod.getVssProformaId());
                 
                    if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag)) {
                        vssProformaVesselAssignmentDao.insert(detailMod);
                    } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)) {
                        vssProformaVesselAssignmentDao.update(detailMod);
                    } else if (RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)) {
                        vssProformaVesselAssignmentDao.delete(detailMod);
                    }
                 
                } catch (CustomDataAccessException e) {
                    index = modifiedMod.getSeqNo() + 1;
                    errorMsgBuffer.append(e.getMessages()+"%"+index+"&");
                } catch(DataAccessException e) {
                    e.printStackTrace();
                }
             
            } //end if 1
        } //end for 1
        //end: save detail in master
        
        System.out.println("[VssProformaMasterJdbcDao][saveMasterDetails] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            mod.setRecordChangeDate(masterRecChangeDate);
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        
        try {
            // check constraint
            this.checkConstraintsMaster(masterMod, masterOperationFlag);
            this.checkConstraints(masterMod, dtlModifiedMods);
            
        } catch(CustomDataAccessException e) {
            mod.setRecordChangeDate(masterRecChangeDate);
            throw e;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    private void checkConstraintsMaster(RrcStandardMod masterMod, String masterOperationFlag) {
        StringBuffer errorMsgBuffer = new StringBuffer();        
        VssProformaMasterMod mod = (VssProformaMasterMod) masterMod;        
        
        if (mod != null) {         
            try {                            
                if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(masterOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(masterOperationFlag)) {                                     
                    checkConstraintStoreProcedure.checkConstraint(mod);
                } else {
                    //don't do anythings in checking constraints.
                }
                
            } catch (CustomDataAccessException e) {
                errorMsgBuffer.append(e.getMessages());
            } catch (DataAccessException e) {
                e.printStackTrace();
            }        
        }    
        
        System.out.println("[VssProformaMasterJdbcDao][checkConstraintsMaster] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
    }
    
    private void checkConstraints(RrcStandardMod masterMod, List dtlModifiedMods) {
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        VssProformaMasterMod mod = (VssProformaMasterMod) masterMod;
        VssProformaVesselAssignmentMod detailMod;
        RcmModifiedObjectMod modifiedMod = null;
        String strOperationFlag = null;
        
        if (dtlModifiedMods != null && dtlModifiedMods.size() > 0) { //if 1
            for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
                modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
                if (modifiedMod != null && modifiedMod.getObjectMod() instanceof VssProformaVesselAssignmentMod) { //if 2
                
                    try {
                        strOperationFlag = modifiedMod.getOperationFlag();
                        detailMod = (VssProformaVesselAssignmentMod) modifiedMod.getObjectMod();
                        detailMod.setVssProformaId(mod.getVssProformaId());
                        
                        if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)
                         || RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)) {
                            vssProformaVesselAssignmentDao.checkConstraint(detailMod);
                        } else {
                            //don't do anythings in checking constraints.
                        }
                        
                    } catch (CustomDataAccessException e) {
                        if (errorMsgBuffer.indexOf(e.getMessages()) == -1) {
                            System.out.println("### e.getMessages() = "+e.getMessages());
                            errorMsgBuffer.append(e.getMessages()+"&");
                        }
                    } catch (DataAccessException e) {
                        e.printStackTrace();
                    }
                
                } //end if 2
            } //end for 1
        } //end if 1
        
        System.out.println("[VssProformaMasterJdbcDao][checkConstraints] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PROFORMA_MASTER.PRR_INS_PROFORMA_MASTER";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_vss_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_ref_no", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_designed_no_of_vessels", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_valid_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_valid_to", Types.DATE));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }

        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
       
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if((inputMod instanceof VssProformaMasterMod)&&(outputMod instanceof VssProformaMasterMod)){
                VssProformaMasterMod aInputMod = (VssProformaMasterMod)inputMod;
                VssProformaMasterMod aOutputMod = (VssProformaMasterMod)outputMod;
                Map inParameters = new HashMap(11);
////                
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_vss_proforma_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_service:"+aInputMod.getServiceCode());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_proforma_ref_no:"+aInputMod.getProformaReferenceNo());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_designed_no_of_vessels:"+aInputMod.getDesignedNoOfVessels());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VssProformaMasterJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////                
                inParameters.put("p_vss_proforma_id", new Integer((RutString.nullToStr(aInputMod.getVssProformaId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssProformaId())));
                inParameters.put("p_service", aInputMod.getServiceCode());
                inParameters.put("p_proforma_ref_no", aInputMod.getProformaReferenceNo());
                inParameters.put("p_designed_no_of_vessels", aInputMod.getDesignedNoOfVessels());
                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVssProformaId((RutString.nullToStr(((Integer)outParameters.get("p_vss_proforma_id")).toString())));
                    aOutputMod.setServiceCode((RutString.nullToStr((String)outParameters.get("p_service"))));
                    aOutputMod.setProformaReferenceNo((RutString.nullToStr((String)outParameters.get("p_proforma_ref_no"))));
                    aOutputMod.setDesignedNoOfVessels((RutString.nullToStr(((Integer)outParameters.get("p_designed_no_of_vessels")).toString())));
                    aOutputMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_from")));
                    aOutputMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_to")));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                    aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PROFORMA_MASTER.PRR_UPD_PROFORMA_MASTER";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_vss_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_ref_no", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_designed_no_of_vessels", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_valid_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_valid_to", Types.DATE));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof VssProformaMasterMod)&&(outputMod instanceof VssProformaMasterMod)){
                VssProformaMasterMod aInputMod = (VssProformaMasterMod)inputMod;
                VssProformaMasterMod aOutputMod = (VssProformaMasterMod)outputMod;
                Map inParameters = new HashMap(9);
////                
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_vss_proforma_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_service:"+aInputMod.getServiceCode());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_proforma_ref_no:"+aInputMod.getProformaReferenceNo());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_designed_no_of_vessels:"+aInputMod.getDesignedNoOfVessels());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VssProformaMasterJdbcDao][UpdateStoreProcedure][update]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////
                inParameters.put("p_vss_proforma_id", new Integer((RutString.nullToStr(aInputMod.getVssProformaId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssProformaId())));
                inParameters.put("p_service", aInputMod.getServiceCode());
                inParameters.put("p_proforma_ref_no", aInputMod.getProformaReferenceNo());
                inParameters.put("p_designed_no_of_vessels", aInputMod.getDesignedNoOfVessels());
                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVssProformaId((RutString.nullToStr(((Integer)outParameters.get("p_vss_proforma_id")).toString())));
                    aOutputMod.setServiceCode((RutString.nullToStr((String)outParameters.get("p_service"))));
                    aOutputMod.setProformaReferenceNo((RutString.nullToStr((String)outParameters.get("p_proforma_ref_no"))));
                    aOutputMod.setDesignedNoOfVessels((RutString.nullToStr(((Integer)outParameters.get("p_designed_no_of_vessels")).toString())));
                    aOutputMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_from")));
                    aOutputMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_to")));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                } 
            }
            return isSuccess;
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PROFORMA_MASTER.PRR_DEL_PROFORMA_MASTER";
        
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_vss_proforma_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof VssProformaMasterMod){
                VssProformaMasterMod aInputMod = (VssProformaMasterMod)inputMod;
                Map inParameters = new HashMap(3);
////                
                System.out.println("[VssProformaMasterJdbcDao][DeleteStoreProcedure][delete]:p_vss_proforma_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaMasterJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[VssProformaMasterJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
////
                inParameters.put("p_vss_proforma_id", new Integer((RutString.nullToStr(aInputMod.getVssProformaId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssProformaId())));
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class CheckConstraintStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_VSS_PROFORMA_MASTER.PRR_CHK_CONSTRAINT_RECORDS";
     
        protected CheckConstraintStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
    //            declareParameter(new SqlParameter("p_vss_vssl_assgn_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_vss_proforma_id", Types.INTEGER));
    //            declareParameter(new SqlParameter("p_vessel_seq_no", Types.INTEGER));
    //            declareParameter(new SqlParameter("p_vessel_code", Types.VARCHAR));
    //            declareParameter(new SqlParameter("p_bsa_vessel_type", Types.VARCHAR));
    //            declareParameter(new SqlParameter("p_valid_from", Types.DATE));
    //            declareParameter(new SqlParameter("p_valid_to", Types.DATE));
    //            declareParameter(new SqlParameter("p_record_status", Types.VARCHAR));
            compile();
        }

        protected void checkConstraint(RrcStandardMod mod) {            
            checkConstraint(mod,mod);
        }
        
        protected void checkConstraint(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            if((inputMod instanceof VssProformaMasterMod)||(outputMod instanceof VssProformaMasterMod)){                
                VssProformaMasterMod aInputMod = (VssProformaMasterMod)inputMod;
                Map inParameters = new HashMap(1);                
    ////
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vss_vssl_assgn_id:"+aInputMod.getVssProformaId());
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vss_proforma_id:"+aInputMod.getServiceCode());
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vessel_seq_no:"+aInputMod.getProformaReferenceNo());
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_vessel_code:"+aInputMod.getDesignedNoOfVessels());
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_bsa_vessel_type:"+aInputMod.getValidFrom());                
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[VssProformaMasterJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_record_status:"+aInputMod.getRecordStatus());
    ////     
    //                inParameters.put("p_vss_vssl_assgn_id", new Integer((RutString.nullToStr(aInputMod.getVssVesselAssignmentId())).equals("")?"0":RutString.nullToStr(aInputMod.getVssVesselAssignmentId())));
                inParameters.put("p_vss_proforma_id", aInputMod.getVssProformaId());
    //                inParameters.put("p_vessel_seq_no", aInputMod.getVesselSeqNo());
    //                inParameters.put("p_vessel_code", aInputMod.getVesselCode());
    //                inParameters.put("p_bsa_vessel_type", aInputMod.getBsaVesselType());
    //                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
    //                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
    //                inParameters.put("p_record_status", aInputMod.getRecordStatus());     
                execute(inParameters);
            }
        }
    }
}

