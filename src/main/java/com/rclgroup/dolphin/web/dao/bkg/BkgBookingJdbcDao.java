package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.bkg.BkgBookingDao;
import com.rclgroup.dolphin.web.model.bkg.BkgBookingMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class BkgBookingJdbcDao extends RrcStandardDao implements BkgBookingDao{
    public BkgBookingJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String bookingNo) throws DataAccessException {
        System.out.println("[BkgBookingJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
         
        StringBuffer sql = new StringBuffer();
        
        sql.append("SELECT BABKNO");        
        sql.append(" FROM VR_BKG_BOOKING");
        sql.append(" WHERE  1=1       ");  
        if((bookingNo!=null)&&(!bookingNo.trim().equals(""))){
            sql.append("  AND BABKNO = :bookingNo ");
        }        
        HashMap map = new HashMap();        
        map.put("bookingNo",bookingNo);
        System.out.println("[BkgBookingJdbcDao][isValid]: SQL: " + sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[BkgBookingJdbcDao][isValid]: With status: Finished");
        return isValid;
    }
    
    
    
    public boolean isValidByFsc(String bookingNo,String line, String trade, String agent, String fsc) throws DataAccessException {
        System.out.println("[BkgBookingJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
         
        StringBuffer sql = new StringBuffer();
        
        sql.append("SELECT BABKNO");        
        sql.append(" FROM VR_BKG_BOOKING");
        sql.append(" WHERE   1=1      ");  
        if((bookingNo!=null)&&(!bookingNo.trim().equals(""))){
            sql.append("  AND BABKNO = :bookingNo ");
        }
        sql.append("      AND     ((NVL(BAOFFC,' ') = NVL(:CF_POS,NVL(BAOFFC,' ')) ) OR");
        sql.append("                  ( NVL(BAPOL,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                      NVL(BAPOD,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                    NVL(BAPOT1,' ') IN  ");
        sql.append("                        ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                     NVL(BAPOT2,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                NVL(BAPOT3,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC ");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' ')))))");
        
        HashMap map = new HashMap();        
        map.put("bookingNo",bookingNo);
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
        System.out.println("[BkgBookingJdbcDao][isValid]: SQL: " + sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[BkgBookingJdbcDao][isValid]: With status: Finished");
        return isValid;
    }
    
    
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("B")){
                sqlCriteria = "AND BABKNO " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND BABKDT " + sqlWild;            
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND UPPER(FULLSTAT) " + sqlWild;
            }        
        }
        return sqlCriteria;
    }
    
    private String getRealFsc(String line, String trade, String agent, String fsc) {
        String realFsc = "";
        if (fsc!= null && !fsc.equals("")){
            realFsc = fsc;
        }else if (agent!= null && !agent.equals("")){
            realFsc = agent;
        }else if (trade!= null && !trade.equals("")){
            realFsc = line+trade;
        }else {
            realFsc = line;
        }        
        return realFsc;
    }
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT BABKNO,BABKDT,FULLSTAT");        
        sql.append(" FROM VR_BKG_BOOKING");                      
        sql.append(sqlCriteria);
        HashMap map = new HashMap();                
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                BkgBookingMod bkg = new BkgBookingMod();
                bkg.setBookingNo(RutString.nullToStr(rs.getString("BABKNO")));
                bkg.setBookingDate(RutString.nullToStr(rs.getString("BABKDT")));                      
                bkg.setBookingStatus(RutString.nullToStr(rs.getString("FULLSTAT")));
                return bkg;
            }
        });
    }
    
    public List listForHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        String realFsc = getRealFsc(line,trade,agent,fsc);
        sql.append("SELECT BABKNO,BABKDT,FULLSTAT");        
        sql.append(" FROM VR_BKG_BOOKING");
        sql.append(" WHERE   1=1      ");          
        sql.append(sqlCriteria);
        sql.append("     AND      ((NVL(BAOFFC,' ') = NVL(:realFsc,NVL(BAOFFC,' ')) ) OR");
        sql.append("                  ( NVL(BAPOL,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                      NVL(BAPOD,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                    NVL(BAPOT1,' ') IN  ");
        sql.append("                        ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                     NVL(BAPOT2,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                NVL(BAPOT3,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC ");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' ')))))");
                              
            
        HashMap map = new HashMap();                
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
        map.put("realFsc",realFsc);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                BkgBookingMod bkg = new BkgBookingMod();
                bkg.setBookingNo(RutString.nullToStr(rs.getString("BABKNO")));
                bkg.setBookingDate(RutString.nullToStr(rs.getString("BABKDT")));                      
                bkg.setBookingStatus(RutString.nullToStr(rs.getString("FULLSTAT")));
                return bkg;
            }
        });
    }
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{

        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        String realFsc = getRealFsc(line,trade,agent,fsc);
        String  selectColumn = "BABKNO,BABKDT,FULLSTAT";
        sql.append("SELECT "+selectColumn);        
        sql.append(" FROM VR_BKG_BOOKING");
        sql.append(" WHERE   1=1      ");          
        sql.append(sqlCriteria);
        sql.append("     AND      ((NVL(BAOFFC,' ') = NVL(:realFsc,NVL(BAOFFC,' ')) ) OR");
        sql.append("                  ( NVL(BAPOL,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                      NVL(BAPOD,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                    NVL(BAPOT1,' ') IN  ");
        sql.append("                        ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                     NVL(BAPOT2,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                NVL(BAPOT3,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC ");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' ')))))");
        sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);                      
            
        HashMap map = new HashMap();                
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
        map.put("realFsc",realFsc);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                BkgBookingMod bkg = new BkgBookingMod();
                bkg.setBookingNo(RutString.nullToStr(rs.getString("BABKNO")));
                bkg.setBookingDate(RutString.nullToStr(rs.getString("BABKDT")));                      
                bkg.setBookingStatus(RutString.nullToStr(rs.getString("FULLSTAT")));
                return bkg;
            }
        });
    }
    public int countListForNewHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        String realFsc = getRealFsc(line,trade,agent,fsc);
        sql.append("SELECT BABKNO,BABKDT,FULLSTAT");       
        sql.append(" FROM VR_BKG_BOOKING");
        sql.append(" WHERE   1=1      ");          
        sql.append(sqlCriteria);
        sql.append("     AND      ((NVL(BAOFFC,' ') = NVL(:realFsc,NVL(BAOFFC,' ')) ) OR");
        sql.append("                  ( NVL(BAPOL,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                      NVL(BAPOD,' ')  IN");
        sql.append("                     ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                       WHERE NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                         AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                    NVL(BAPOT1,' ') IN  ");
        sql.append("                        ( SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                     NVL(BAPOT2,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' '))) OR");
        sql.append("                NVL(BAPOT3,' ') IN  ");
        sql.append("                        (SELECT PORT_CODE FROM VR_CAM_FSC_PORT FSC ");
        sql.append("                          WHERE     NVL(FSC.LINE_CODE,' ') = NVL(:line,NVL(FSC.LINE_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.REGION_CODE,' ') = NVL(:trade,NVL(FSC.REGION_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.AGENT_CODE,' ') = NVL(:agent,nvl(FSC.AGENT_CODE,' '))");
        sql.append("                                                            AND NVL(FSC.FSC_CODE,' ') = NVL(:fsc,nvl(FSC.FSC_CODE,' ')))))");
        sql = getNumberOfAllData(sql);
        System.out.println("line = "+line+"trade = "+trade+"agent = "+agent+"fsc = "+fsc+"realFsc = "+realFsc);
        HashMap map = new HashMap();                
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
        map.put("realFsc",realFsc);
    //        map.put("fsc",realFsc);
        Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map, Integer.class);
        return result.intValue();
    }
        
}
