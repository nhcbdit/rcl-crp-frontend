/*-----------------------------------------------------------------------------------------------------------  
BkgBookingAgainstBsaDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 02/04/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;


import com.rclgroup.dolphin.web.model.bkg.BkgBookingAgainstBsaMod;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.core.SqlParameter;

public class BkgBookingAgainstBsaJdbcDao extends RrcStandardDao implements BkgBookingAgainstBsaDao{
    private InsertStoreProcedure insertStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    
    public BkgBookingAgainstBsaJdbcDao() {
    }

    public boolean isValidWithSessionId(String sessionId) {
        System.out.println("[BkgBookingAgainstBsaJdbcDao][isValidWithSessionId]: Started ");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append(" select distinct session_id ");
        sql.append(" from BKG_VS_BSA_SUM_RPT ");
        sql.append(" where session_id = '"+sessionId+"' ");
        sql.append("     and rownum =1 ");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),new HashMap());
        if(rs.next()){
            isValid = true;
        }else{
            isValid = false;
        }
        
       // System.out.println("[BkgBookingAgainstBsaJdbcDao][isValidWithSessionId]: sql : "+sql.toString());
       // System.out.println("[BkgBookingAgainstBsaJdbcDao][isValidWithSessionId]: isValid : "+isValid);
        System.out.println("[BkgBookingAgainstBsaJdbcDao][isValidWithSessionId]: Finished ");
        return isValid;
    }

    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
    }
    
    public boolean insert(RrcStandardMod mod) {
        return insertStoreProcedure.insert(mod);
    }

    public boolean delete(RrcStandardMod mod) {
        boolean isSuccess = false;
        if(mod instanceof BkgBookingAgainstBsaMod){
            BkgBookingAgainstBsaMod aInputMod = (BkgBookingAgainstBsaMod)mod;
            if(isValidWithSessionId(aInputMod.getSessionId())){
                deleteStoreProcedure.delete(mod);
                isSuccess =true;
            }else{
                isSuccess = false;
            }
        }// end if;
        return isSuccess;
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BKG_AGAINST_BSA_RPT.PRR_INS_BKG_VS_BSA_SUM_RPT";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate,STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session",Types.VARCHAR));
            declareParameter(new SqlParameter("p_add_user",Types.VARCHAR));
            declareParameter(new SqlParameter("p_permission",Types.VARCHAR));
            declareParameter(new SqlParameter("p_fdate",Types.VARCHAR));
            declareParameter(new SqlParameter("p_tdate",Types.VARCHAR));
            declareParameter(new SqlParameter("p_service",Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel",Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage",Types.VARCHAR));
            declareParameter(new SqlParameter("p_direction",Types.VARCHAR));
            declareParameter(new SqlParameter("p_cs",Types.VARCHAR));
            declareParameter(new SqlParameter("p_port",Types.VARCHAR));
            declareParameter(new SqlParameter("p_port_tml",Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol",Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_tml",Types.VARCHAR));
            declareParameter(new SqlParameter("p_ts",Types.VARCHAR));
        }
        
        protected boolean insert(RrcStandardMod mod){
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod , RrcStandardMod outputMod){
            boolean isSuccess = false;
            BkgBookingAgainstBsaMod aInputMod = (BkgBookingAgainstBsaMod)inputMod;
            BkgBookingAgainstBsaMod aOutputMod = (BkgBookingAgainstBsaMod)outputMod;
           /* System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:Started ");
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_session : "+aInputMod.getSessionId());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_add_user : "+aInputMod.getRecordAddUser());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_permission : "+aInputMod.getPermissionUser());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_fdate : "+aInputMod.getFromDate());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_tdate : "+aInputMod.getToDate());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_service : "+aInputMod.getService());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_vessel : "+aInputMod.getVessel());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_voyage : "+aInputMod.getVoyage());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_direction : "+aInputMod.getDirection());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_cs : "+aInputMod.getCocSoc());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_port : "+aInputMod.getPortcall());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_port_tml : "+aInputMod.getPortcallTerminal());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_pol : "+aInputMod.getPol());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_pol_tml : "+aInputMod.getPolTerminal());
            System.out.println("[BkgBookingAgainstBsaJdbcDao][insert]:p_ts : "+aInputMod.getLocalTs());*/
                
            if((inputMod instanceof BkgBookingAgainstBsaMod) && (outputMod instanceof BkgBookingAgainstBsaMod)){
                
                Map inParameters = new HashMap(16);
                
                inParameters.put("p_session",aInputMod.getSessionId());
                inParameters.put("p_add_user",aInputMod.getRecordAddUser());
                inParameters.put("p_permission",aInputMod.getPermissionUser());
                inParameters.put("p_fdate",aInputMod.getFromDate());
                inParameters.put("p_tdate",aInputMod.getToDate());
                inParameters.put("p_service",aInputMod.getService());
                inParameters.put("p_vessel",aInputMod.getVessel());
                inParameters.put("p_voyage",aInputMod.getVoyage());
                inParameters.put("p_direction",aInputMod.getDirection());
                inParameters.put("p_cs",aInputMod.getCocSoc());
                inParameters.put("p_port",aInputMod.getPortcall());
                inParameters.put("p_port_tml",aInputMod.getPortcallTerminal());
                inParameters.put("p_pol",aInputMod.getPol());
                inParameters.put("p_pol_tml",aInputMod.getPolTerminal());
                inParameters.put("p_ts",aInputMod.getLocalTs());
                Map outParameters = execute(inParameters);
//                if(outParameters.size() >0){
                    isSuccess = true;
//                }
            }
            
            return isSuccess;
            }
        }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BKG_AGAINST_BSA_RPT.PRR_DEL_BKG_VS_BSA_SUM_RPT";
        
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session",Types.VARCHAR));
            declareParameter(new SqlParameter("p_add_user",Types.VARCHAR));
            compile();
        }
        
        protected boolean delete(final RrcStandardMod inputMod){
            boolean isSuccess = false;
            if(inputMod instanceof BkgBookingAgainstBsaMod){
                BkgBookingAgainstBsaMod aInputMod = (BkgBookingAgainstBsaMod)inputMod;
                    Map inParameters = new HashMap(2);
                    inParameters.put("p_session",aInputMod.getSessionId());
                    inParameters.put("p_add_user",aInputMod.getRecordAddUser());
                    execute(inParameters);
                    isSuccess = true;
            }
            System.out.println("[bkgBookingAgainstBsaJdbcDao][delete]:start : isSuccess :"+isSuccess);
            return isSuccess;
        }
    }
}
