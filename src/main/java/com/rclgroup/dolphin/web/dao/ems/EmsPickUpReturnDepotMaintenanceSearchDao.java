/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotMaintenanceSearchDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 02/12/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface EmsPickUpReturnDepotMaintenanceSearchDao extends RriStandardDao {
    
      /**
       * list billing statement by header records for search screen
       * @param rowAt
       * @param rowTo
       * @param depotCode
       * @param pointCode
       * @param porrFlag
       * @param modeOfReturn
       * @param service
       * @param vessel
       * @param voyage
       * @param supplier
       * @param shipper
       * @param consignee
       * @param contractParty
       * @param bkg
       * @param bl     
       * @param sortBy
       * @param sortByIn
       * @return list of Mapping for Pick up or Return depot model(pickUpReturnDepotAssignmentMod)
       * @throws DataAccessException
       */
      public List listMappingForStatement(int rowAt, int rowTo, String depotCode, String pointCode, String porrFlag, String modeOfReturn, String service, String vessel, String voyage,String supplier, String shipper, String consignee, String contractParty, String bl, String bkg,String sortBy, String sortByIn) throws DataAccessException;
      
      /**
       * number of list of Mapping
       * @param depotCode
       * @param pointCode
       * @param porrFlag
       * @param modeOfReturn
       * @param service
       * @param vessel
       * @param voyage
       * @param supplier
       * @param shipper
       * @param consignee
       * @param contractParty
       * @param bkg
       * @param bl     
       * @return number of Mapping for Pick up or Return depot records
       * @throws DataAccessException
       */
      public int countListMappingForStatement(String depotCode, String pointCode, String porrFlag, String modeOfReturn, String service, String vessel, String voyage,String supplier, String shipper, String consignee, String contractParty, String bl, String bkg) throws DataAccessException;
    
    /**
     * update a multi depot header record 
     * @param mod a Multi Depot Header model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: BILL_TO_PARTY_REQ
     *                              error message: BL_REQ
     *                              error message: INV_NO_REQ
     *                              error message: DATA_NOT_FOUND      
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public boolean updateHdr (RrcStandardMod mod) throws DataAccessException;
    
    /**
    * delete a multi depot header record 
    * @param mod a Multi Depot Header model as input and output
    * @return whether delete is successful
    * @throws DataAccessException exceptin which client has to catch all following error messages:
    *                              error message: PK_DATA_MAP_ID_REQ     
    *                              error message: DATA_NOT_FOUND      
    *                              error message: ORA-XXXXX (un-exceptional oracle error)  
    */
    public boolean deleteHdr (RrcStandardMod mod) throws DataAccessException;
    
    /**
    * delete a multi depot header record 
    * @param mod a Multi Depot Header model as input and output
    * @return whether delete is successful
    * @throws DataAccessException exceptin which client has to catch all following error messages:
    *                              error message: PK_DATA_MAP_ID_REQ     
    *                              error message: DATA_NOT_FOUND      
    *                              error message: ORA-XXXXX (un-exceptional oracle error)  
    */
    public boolean deleteDtlByHdr (RrcStandardMod mod) throws DataAccessException;
    
    /**
     * insert a multi depot header record 
     * @param mod a Multi Depot Header model as input and output
     * @return whether insert is successful
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: BILL_TO_PARTY_REQ
     *                              error message: INV_NO_REQ
     *                              error message: DATA_NOT_FOUND      
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public boolean insertHdr (RrcStandardMod mod) throws DataAccessException;
}
