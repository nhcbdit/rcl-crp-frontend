/*-----------------------------------------------------------------------------------------------------------  
BkgReportJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 23/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bkg.BkgBookingCreatedSummaryReportMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class BkgReportJdbcDao extends RrcStandardDao implements BkgReportDao {
    private GenerateBookingCreatedSummaryStoreProcedure generateBookingCreatedSummaryStoreProcedure;
    
    public BkgReportJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        generateBookingCreatedSummaryStoreProcedure = new GenerateBookingCreatedSummaryStoreProcedure(getJdbcTemplate());
    }
    
    public boolean generateBookingCreatedSummaryReport(RrcStandardMod mod) throws DataAccessException{
        return generateBookingCreatedSummaryStoreProcedure.generate(mod);
    }

    protected class GenerateBookingCreatedSummaryStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BKG_RPT.PRR_GEN_BKG_116_CREATE_SUM_RPT";
        
        protected GenerateBookingCreatedSummaryStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_rpt_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_fr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_date_to", Types.VARCHAR));
            declareParameter(new SqlParameter("p_spec_username", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof BkgBookingCreatedSummaryReportMod && outputMod instanceof BkgBookingCreatedSummaryReportMod) {
                BkgBookingCreatedSummaryReportMod aInputMod = (BkgBookingCreatedSummaryReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_rpt_type", aInputMod.getReportType());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_date_fr", aInputMod.getDateFrom());
                inParameters.put("p_date_to", aInputMod.getDateTo());
                inParameters.put("p_spec_username", aInputMod.getSpecificUser());
                
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_rpt_type = "+inParameters.get("p_rpt_type"));
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_date_fr = "+inParameters.get("p_date_fr"));
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_date_to = "+inParameters.get("p_date_to"));
                System.out.println("[BkgReportJdbcDao][GenerateBookingCreatedSummaryStoreProcedure]: p_spec_username = "+inParameters.get("p_spec_username"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
}
