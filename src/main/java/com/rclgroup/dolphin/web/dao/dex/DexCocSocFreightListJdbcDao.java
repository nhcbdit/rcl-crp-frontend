package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.dex.DexCocSocFreightListMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class DexCocSocFreightListJdbcDao extends RrcStandardDao implements DexCocSocFreightListDao{
    private GenerateTempBLProcedure generateTempBLProcedure;

    public DexCocSocFreightListJdbcDao() {
    }
    
    
    protected void initDao() throws Exception {
        super.initDao();
       generateTempBLProcedure = new GenerateTempBLProcedure(getJdbcTemplate());
    }
    
    public boolean generateTempBL(RrcStandardMod mod) throws DataAccessException {
        return generateTempBLProcedure.generate(mod);
    }
    
    protected class GenerateTempBLProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_DIM_DEX_FREIGHT_LIST.PRR_GEN_DIM_DEX_107_BL";
        
        protected GenerateTempBLProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("pUsrId", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("pSessionId", Types.VARCHAR));
            declareParameter(new SqlParameter("pBound", Types.VARCHAR));
            declareParameter(new SqlParameter("pCS", Types.VARCHAR));
            declareParameter(new SqlParameter("pBlSts", Types.VARCHAR));
            declareParameter(new SqlParameter("pSvr", Types.VARCHAR));
            declareParameter(new SqlParameter("pVsl", Types.VARCHAR));
            declareParameter(new SqlParameter("pVoy", Types.VARCHAR));
            declareParameter(new SqlParameter("pDir", Types.VARCHAR));
            declareParameter(new SqlParameter("pPol", Types.VARCHAR));
            declareParameter(new SqlParameter("pPod", Types.VARCHAR));
            declareParameter(new SqlParameter("pL", Types.VARCHAR));
            declareParameter(new SqlParameter("pT", Types.VARCHAR));
            declareParameter(new SqlParameter("pA", Types.VARCHAR));
            declareParameter(new SqlParameter("pF", Types.VARCHAR));
            declareParameter(new SqlParameter("polTer", Types.VARCHAR));
            declareParameter(new SqlParameter("pDF", Types.NUMERIC));
            declareParameter(new SqlParameter("pDT", Types.NUMERIC));
            declareParameter(new SqlParameter("pCollFsc", Types.VARCHAR));
            declareParameter(new SqlParameter("pR", Types.VARCHAR));
            declareParameter(new SqlParameter("pReport", Types.VARCHAR));
            //declareParameter(new SqlInOutParameter("pInvoyage_SearchBy", Types.VARCHAR));
            //declareParameter(new SqlInOutParameter("pInvoyage_Session", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if ((inputMod instanceof DexCocSocFreightListMod) && (outputMod instanceof DexCocSocFreightListMod)) {
                Map inParameters = new HashMap();
                DexCocSocFreightListMod aInputMod = (DexCocSocFreightListMod) inputMod;
                inParameters.put("pUsrId", aInputMod.getUsername());
                inParameters.put("pSessionId", aInputMod.getSessionId());
                inParameters.put("pBound", aInputMod.getBound());
                inParameters.put("pCS", aInputMod.getCocSoc());
                inParameters.put("pBlSts", aInputMod.getBlStatus());
                inParameters.put("pSvr", aInputMod.getService());
                inParameters.put("pVsl", aInputMod.getVessle());
                inParameters.put("pVoy", aInputMod.getVoyage());
                inParameters.put("pDir", aInputMod.getDirection());
                inParameters.put("pPol", aInputMod.getPol());
                inParameters.put("pPod", aInputMod.getPod());
                inParameters.put("pL", aInputMod.getLine());
                inParameters.put("pT", aInputMod.getRegion());
                inParameters.put("pA", aInputMod.getAgent());
                inParameters.put("pF", aInputMod.getFscCodeOfUser());
                inParameters.put("polTer", aInputMod.getPolTer());
                inParameters.put("pCollFsc", aInputMod.getCollFsc());
                inParameters.put("pR", aInputMod.getRevenue());
                inParameters.put("pReport", aInputMod.getReport());
                //inParameters.put("pInvoyage_SearchBy", null);
                //inParameters.put("pInvoyage_Session", null);
                
                if(aInputMod.getFromDate()!=null & !aInputMod.getFromDate().equals("")){
                    String arrPeriod[] = aInputMod.getFromDate().split("/");
                    String p_period = arrPeriod[2]+arrPeriod[1]+arrPeriod[0];
                    inParameters.put("p_from_date", p_period);
                }else inParameters.put("pDF", null);
                
                if(aInputMod.getToDate()!=null & !aInputMod.getToDate().equals("")){
                    String arrPeriod[] = aInputMod.getToDate().split("/");
                    String p_period = arrPeriod[2]+arrPeriod[1]+arrPeriod[0];
                    inParameters.put("p_to_date", p_period);
                }else inParameters.put("pDT", null);
                
                Map outParameters = execute(inParameters);
                if (outParameters.size()>0) {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }
    }
}
