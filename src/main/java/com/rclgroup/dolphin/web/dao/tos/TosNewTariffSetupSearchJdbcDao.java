/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupSearchJdbcDao.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Dhruv Parekh 25/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupSearchMod;

import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupSearchMod;

import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class TosNewTariffSetupSearchJdbcDao extends RrcStandardDao implements TosNewTariffSetupSearchDao{
    private List lst = new ArrayList();
    public int intCurrPage = 0;
    private GetTosSetupPortListProcedure getTosSetupPortListProcedure;
    
    public void initDao() throws Exception {
        super.initDao();        
        
        getTosSetupPortListProcedure = new GetTosSetupPortListProcedure(getJdbcTemplate(), new TosPortListMapper());
    }
    
    public TosNewTariffSetupSearchJdbcDao() {
    }
    
    public List getPortTermList(String port, String terminal, String sortBy, String sortIn) throws DataAccessException{
        HashMap paramMap = new HashMap();
        List resultList = new ArrayList();
        
        paramMap.put("p_i_v_port", port);
        paramMap.put("p_i_v_terminal", terminal);
        paramMap.put("p_i_v_sortby", sortBy);
        paramMap.put("p_i_v_sortin", sortIn);
        
        resultList = getTosSetupPortListProcedure.getTosportlist(paramMap);
        
        return resultList;
    }
    
    protected class GetTosSetupPortListProcedure extends StoredProcedure{
        private static final String SQL_TOS_TARIFF_PORT = "PCR_TOS_TARIFF_SETUP.PRR_GET_SETUP_PORT_LIST";
        
        protected GetTosSetupPortListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_TARIFF_PORT);
            
            declareParameter(new SqlOutParameter("p_o_v_rate_list", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));            
            declareParameter(new SqlInOutParameter("p_i_v_sortby",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_sortin",OracleTypes.VARCHAR,rowMapper));
            
            compile();
        }
        
        protected List<TosNewTariffSetupSearchMod> getTosportlist(Map mapParams){
            Map outMap = new HashMap();
            
            System.out.println("p_i_v_port: " + (String)mapParams.get("p_i_v_port"));
            System.out.println("p_i_v_terminal: "+ (String)mapParams.get("p_i_v_terminal"));
            
            List<TosNewTariffSetupSearchMod> returnList = new ArrayList<TosNewTariffSetupSearchMod>();
            
            try{
                outMap = execute(mapParams);
                returnList = (List<TosNewTariffSetupSearchMod>) outMap.get("p_o_v_rate_list");
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
        }
    }
    
    private class TosPortListMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException { 
            TosNewTariffSetupSearchMod bean = new TosNewTariffSetupSearchMod();
            bean.setPort(RutString.nullToStr(rs.getString("PORT")));
            bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
            bean.setPort_desc(RutString.nullToStr(rs.getString("PORT_DESC")));
            bean.setTerminal_desc(RutString.nullToStr(rs.getString("TERMINAL_DESC")));
            bean.setCurrency(RutString.nullToStr(rs.getString("CURRENCY")));
            return bean;
        }
    }
}
