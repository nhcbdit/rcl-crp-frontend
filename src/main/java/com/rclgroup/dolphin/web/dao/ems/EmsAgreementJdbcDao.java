/*-----------------------------------------------------------------------------------------------------------  
EmsAgreementJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsAgreementMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsAgreementJdbcDao extends RrcStandardDao implements EmsAgreementDao {
     
     public EmsAgreementJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String Agreement) throws DataAccessException {
         System.out.println("[EmsAgreementJdbcDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AGREEMENT_NO ");
         sql.append("FROM VR_EMS_AGREEMENT ");
         sql.append("WHERE AGREEMENT_NO = :Agreement");
         HashMap map = new HashMap();
         map.put("Agreement",Agreement);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[EmsAgreementJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValid(String Agreement, String status) throws DataAccessException {
         System.out.println("[EmsAgreementDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AGREEMENT_NO ");
         sql.append("FROM VR_EMS_AGREEMENT ");
         sql.append("WHERE AGREEMENT_CODE = :Agreement ");
         sql.append("AND STATUS = :status ");
         HashMap map = new HashMap();
         map.put("Agreement",Agreement);
         map.put("status",status);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[EmsAgreementJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[EmsAgreementDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AGREEMENT_NO ");
         sql.append("      ,SUB_AGREEMENT_NO "); 
         sql.append("      ,LESSOR_CODE ");
         sql.append("      ,STATUS "); 
         sql.append("FROM VR_EMS_AGREEMENT ");
         sql.append(sqlCriteria);
         System.out.println("[EmsAgreementJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
         System.out.println("[EmsAgreementJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowModMapper());         
     }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "WHERE AGREEMENT_NO " + sqlWild;
             }else if(search.equalsIgnoreCase("SA")){
                 sqlCriteria = "WHERE SUB_AGREEMENT_NO " + sqlWild;
             }else if(search.equalsIgnoreCase("L")){
                 sqlCriteria = "WHERE LESSOR_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "WHERE STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }      
     
     public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
         System.out.println("[EmsAgreementJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AGREEMENT_NO ");
         sql.append("      ,SUB_AGREEMENT_NO ");
         sql.append("      ,LESSOR_CODE ");             
         sql.append("      ,STATUS ");              
         sql.append("FROM VR_EMS_AGREEMENT ");
         sql.append("WHERE STATUS = :status ");   
         sql.append(sqlCriteria);
         System.out.println("[EmsAgreementJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         
         HashMap map = new HashMap();
         map.put("status",status);
         System.out.println("[EmsAgreementJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());  
     }      
     
     private String createSqlCriteriaWithStatus(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND AGREEMENT_NO " + sqlWild;
             }else if(search.equalsIgnoreCase("SA")){
                 sqlCriteria = "AND SUB_AGREEMENT_NO " + sqlWild;
             }else if(search.equalsIgnoreCase("L")){
                 sqlCriteria = "AND LESSOR_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }        
     
     protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModelForHelp(rs);
         }
     } 
     
     private EmsAgreementMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
         EmsAgreementMod Agreement = new EmsAgreementMod();
         Agreement.setAgreementCode(RutString.nullToStr(rs.getString("AGREEMENT_NO")));   
         Agreement.setSubAgreement(RutString.nullToStr(rs.getString("SUB_AGREEMENT_NO")));   
         Agreement.setLessorCode(RutString.nullToStr(rs.getString("LESSOR_CODE")));   
         Agreement.setAgreementStatus(RutString.nullToStr(rs.getString("STATUS")));

         return Agreement;
     }     
 }
