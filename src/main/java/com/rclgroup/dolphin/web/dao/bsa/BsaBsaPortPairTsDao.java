package com.rclgroup.dolphin.web.dao.bsa;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface BsaBsaPortPairTsDao {
    /**
     * check valid of data for finding
     * @param find
     * @param search     
     * @param status
     * @return valid of data for finding
     * @throws DataAccessException
     */
    public boolean isValidWithStatus(String find,String search,String status) throws DataAccessException;
    
    /**
     * check valid of data for finding
     * @param tsIndic
     * @param service
     * @param variant
     * @param port
     * @param group
     * @param status
     * @return valid of data for finding
     * @throws DataAccessException
     */
    public boolean isValidWithStatus(String tsIndic,String service,String variant,String port,String group,String status) throws DataAccessException;

    /**
     * list agent records for help screen
     * @param find
     * @param search
     * @param wild    
     * @param status    
     * @param tsIndic
     * @param serviceVariantId
     * @param modelId
     * @param tsPolPort
     * @param tsPodPort
     * @param polTsFlag
     * @param podTsFlag
     * @return list of Port Pair Transshipment data
     * @throws DataAccessException
     */
    public List listForHelpScreenWithStatus(String find,String search,String wild,String status,String tsIndic,int serviceVariantId,int modelId,String tsPolPort,String tsPodPort, String polTsFlag, String podTsFlag) throws DataAccessException;
    
    public List listForHelpScreenForVolumnPortPair(String find, String search, String wild, String status, String tsIndic, String ownService, int modelId, String ownProfRefNo, String tsPolPort,String tsPodPort) throws DataAccessException;
}
