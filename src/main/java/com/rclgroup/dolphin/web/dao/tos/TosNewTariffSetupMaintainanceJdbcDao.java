package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupCustomersMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupNonRclPartnerMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupPortTariffMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupServiceMod;

import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupSlotPartnerMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupVendorsMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class TosNewTariffSetupMaintainanceJdbcDao extends RrcStandardDao implements TosNewTariffSetupMaintainanceDao{
    private GetTariffServiceProcedure getTariffServiceProcedure;
    private GetTariffPortProcedure getTariffPortProcedure;
    private GetTariffVendorProcedure getTariffVendorProcedure;
    private GetTariffCustProcedure getTariffCustProcedure;
    private GetTariffSlotPartnerProcedure getTariffSlotPartnerProcedure;
    private GetTariffNonSlotPartnerProcedure getTariffNonSlotPartnerProcedure;
    private UpdateSetupPortProcedure updateSetupPortProcedure;
    private InsertRateServiceProcedure insertServiceProcedure;
    private InsertTerminalTariffProcedure insertTerminalTariffProcedure;
    
    public void initDao() throws Exception {
        super.initDao();        
        getTariffServiceProcedure = new GetTariffServiceProcedure(getJdbcTemplate(), new TariffServiceRowMapper());
        getTariffPortProcedure = new GetTariffPortProcedure(getJdbcTemplate(), new TariffPortRowMapper());
        getTariffVendorProcedure = new GetTariffVendorProcedure(getJdbcTemplate(), new TosTariffVednorRowMapper());
        getTariffCustProcedure = new GetTariffCustProcedure(getJdbcTemplate(), new TosTariffCustomerRowMapper());
        getTariffSlotPartnerProcedure = new GetTariffSlotPartnerProcedure(getJdbcTemplate(), new TosTariffSlotPartnerRowMapper());
        getTariffNonSlotPartnerProcedure = new GetTariffNonSlotPartnerProcedure(getJdbcTemplate(), new TosTariffNonSlotPartnerRowMapper());
        updateSetupPortProcedure = new UpdateSetupPortProcedure(getJdbcTemplate());
        insertServiceProcedure = new InsertRateServiceProcedure(getJdbcTemplate());
        insertTerminalTariffProcedure = new InsertTerminalTariffProcedure(getJdbcTemplate());
    }
    
    public TosNewTariffSetupMaintainanceJdbcDao() {
    }
    
    /**
     * @param port
     * @param terminal    
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupServiceMod> getTariffServiceList(String port, String terminal) throws DataAccessException{
         HashMap mapParam = new HashMap();
         
         mapParam.put("p_i_v_port", port);
         mapParam.put("p_i_v_terminal", terminal);
         
         List<TosNewTariffSetupServiceMod> resultedList = getTariffServiceProcedure.getTosTariffServiceList(mapParam);
         
         return resultedList;
     }
     
    /**
     * @param port
     * @param terminal    
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupPortTariffMod> getPortTariffList(String port, String terminal) throws DataAccessException{
         HashMap mapParam = new HashMap();
         
         mapParam.put("p_i_v_port", port);
         mapParam.put("p_i_v_terminal", terminal);
         
         List<TosNewTariffSetupPortTariffMod> resultedList = getTariffPortProcedure.getTariffPortList(mapParam);
         
         return resultedList;
     }
     
    /**
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupVendorsMod> getTariffVednorsList(String port, String terminal, String service) throws DataAccessException{
         HashMap mapParam = new HashMap();
         
         mapParam.put("p_i_v_port", port);
         mapParam.put("p_i_v_terminal", terminal);
         mapParam.put("p_i_v_service", service);
         
         List<TosNewTariffSetupVendorsMod> resultedList = getTariffVendorProcedure.getVednorsList(mapParam);
         
         return resultedList;
     }
     
    /**
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupCustomersMod> getTariffCustomerList(String port, String terminal, String service) throws DataAccessException{
         HashMap mapParam = new HashMap();
         
         mapParam.put("p_i_v_port", port);
         mapParam.put("p_i_v_terminal", terminal);
         mapParam.put("p_i_v_service", service);
         
         List<TosNewTariffSetupCustomersMod> customerList = getTariffCustProcedure.getTariffCustomers(mapParam);
         
         return customerList;
     }
         
    /**
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupSlotPartnerMod> getTariffSlotPartnerList(String port, String terminal, String service) throws DataAccessException{
         HashMap mapParam = new HashMap();
         
         mapParam.put("p_i_v_port", port);
         mapParam.put("p_i_v_terminal", terminal);
         mapParam.put("p_i_v_service", service);
         
         List<TosNewTariffSetupSlotPartnerMod> partnerList = getTariffSlotPartnerProcedure.getTariffCustomers(mapParam);
         
         return partnerList;
     }
     
    /**
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupNonRclPartnerMod> getTariffNonSlotPartnerList(String port, String terminal, String service) throws DataAccessException{
         HashMap mapParam = new HashMap();
         
         mapParam.put("p_i_v_port", port);
         mapParam.put("p_i_v_terminal", terminal);
         mapParam.put("p_i_v_service", service);
         
         List<TosNewTariffSetupNonRclPartnerMod> partnerList = getTariffNonSlotPartnerProcedure.getTariffNonSlotPartners(mapParam);
         
         return partnerList;
     }
    
    /**     
     * @param serviceCd
     * @return String
     * @throws DataAccessException
     */
     public String getServiceDesc(String serviceCd) throws DataAccessException{
         StringBuffer sqlStmt = new StringBuffer();                  
         sqlStmt.append("SELECT SERVICE_NAME ");
         sqlStmt.append(" FROM VR_SERVICE_MASTER ");
         sqlStmt.append(" WHERE SERVICE_CODE = '"+serviceCd+"' ");
         
         return getNamedParameterJdbcTemplate().queryForObject(sqlStmt.toString(), 
                                                                new HashMap(), 
                                                                new RowMapper(){
                                                                     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                                                                        String serviceDesc =  RutString.nullToStr(rs.getString("SERVICE_NAME"));
                                                                        return serviceDesc;
                                                                     }
                                                                }).toString();
         
     }
     
    /**     
     * Returns Customer Name based on Customer Code parameter
     * @param custCd
     * @return String
     * @throws DataAccessException     
     */
     public String getCustDesc(String custCd) throws DataAccessException{
         StringBuffer sqlStmt = new StringBuffer();
         sqlStmt.append("SELECT CUNAME FROM ITP010 WHERE CUCUST = '"+custCd+"' ");
         return getNamedParameterJdbcTemplate().queryForObject(sqlStmt.toString(), 
                                                                new HashMap(), 
                                                                new RowMapper(){
                                                                     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                                                                        String custDesc = RutString.nullToStr(rs.getString("CUNAME"));
                                                                        return custDesc;
                                                                     }
                                                                }).toString();
 
     }
     
    /**     
     * Returns Vendor Name based on Vendor Code parameter
     * @param vendorCd
     * @return String
     * @throws DataAccessException     
     */
     public String getVendorDesc(String vendorCd) throws DataAccessException{
         StringBuffer sqlStmt = new StringBuffer();
         sqlStmt.append("SELECT VCVDNM FROM ITP025 WHERE VCVNCD = '"+vendorCd+"' ");
         return getNamedParameterJdbcTemplate().queryForObject(sqlStmt.toString(), 
                                                                new HashMap(), 
                                                                new RowMapper(){
                                                                     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                                                                        String vendorDesc = RutString.nullToStr(rs.getString("VCVDNM"));
                                                                        return vendorDesc;
                                                                     }
                                                                }).toString();
     }
     
    /**     
     * @param rateRef
     * @return String
     * @throws DataAccessException
     */
     public String getOprDetail(String port, String terminal, String rateRef) throws DataAccessException{
         StringBuffer sqlStmt = new StringBuffer();                  
         sqlStmt.append("SELECT OPERATION_TYPE ||'~'||(SELECT DESCR FROM TOS_OPR_CODE_MASTER OPR_MST WHERE OPR_MST.OPR_CODE = TOS_RATE_HEADER.OPERATION_TYPE) OPR_DTL ");
         sqlStmt.append(" FROM TOS_RATE_HEADER WHERE PORT = '"+port+"' ");
         sqlStmt.append(" AND TERMINAL = '"+terminal+"' ");
         sqlStmt.append(" AND TOS_RATE_REF = '"+rateRef+"' ");
         
         return getNamedParameterJdbcTemplate().queryForObject(sqlStmt.toString(), 
                                                               new HashMap(), 
                                                               new RowMapper(){
                                                                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                                                                        String oprDtl = RutString.nullToStr(rs.getString("OPR_DTL"));
                                                                        return oprDtl;
                                                                    }
                                                              }).toString();
     }
     
    /**     
     * Saves Tariff Setup Details into Database.
     * @param port
     * @param terminal
     * @param currency
     * @param serviceList
     * @param portList
     * @param slotPartnerList
     * @param custList
     * @param vendorsList
     * @param nonSlotPartnerList     
     * @return 
     * @throws DataAccessException
     */
     public void saveTariffDetail(String line,
                                  String trade,
                                  String agent,
                                  String user,
                                  String port, 
                                  String terminal, 
                                  String currency, 
                                  List<TosNewTariffSetupServiceMod> serviceList,
                                  List<TosNewTariffSetupPortTariffMod> portList,
                                  List<TosNewTariffSetupSlotPartnerMod> slotPartnerList, 
                                  List<TosNewTariffSetupCustomersMod> custList,
                                  List<TosNewTariffSetupVendorsMod> vendorsList,
                                  List<TosNewTariffSetupNonRclPartnerMod> nonSlotPartnerList,
                                  int selectServiceIndx)throws DataAccessException{        
        Map mapParams = new HashMap();
        StringBuffer msgBuffer = new StringBuffer();
        
        mapParams.put("p_i_v_line", line);
        mapParams.put("p_i_v_trade", trade);
        mapParams.put("p_i_v_agent", agent);
        mapParams.put("p_i_v_user", user);
        mapParams.put("p_i_v_port", port);
        mapParams.put("p_i_v_terminal", terminal);
        mapParams.put("p_i_v_currency", currency);             
        
        try{
            updateSetupPortProcedure.updateSetupPort(mapParams);
            
            if(serviceList.size() > 0){
                for (int i = 0; i < serviceList.size(); i++)  {
                    TosNewTariffSetupServiceMod serviceMod = serviceList.get(i);
                    
                    if(serviceMod.getRowStatus() == serviceMod.INSERT){
                        mapParams.put("p_i_v_service", serviceMod.getServiceCd());
                        insertServiceProcedure.insertRateService(mapParams);   
                    }
                }                                                
            }
            
            if(portList.size() > 0){
                for (int i = 0; i < portList.size(); i++)  {
                    TosNewTariffSetupPortTariffMod portBean = portList.get(i);
                                                           
                    mapParams.put("p_i_v_opr_cd", portBean.getOperationCd());
                    mapParams.put("p_i_v_rate_ref", portBean.getRateRef());
                    mapParams.put("p_i_v_eff_dt", portBean.getEffDate());
                    mapParams.put("p_i_v_exp_dt", portBean.getExpDate());
                    mapParams.put("p_i_v_soc_prof", portBean.getSocProforma());                    
                    mapParams.put("p_i_v_fifo_prof", "");
                    mapParams.put("p_i_v_term_row_sts", portBean.getRowStatus());
                    mapParams.put("p_i_v_term_seq_no", portBean.getSeqNo());
                    
                    insertTerminalTariffProcedure.insertTermTariff(mapParams);
                    
                    if( i == selectServiceIndx){
                        //Insert Slot Data
                    }
                    
                }
                    
            }
            
        }catch(CustomDataAccessException dae){
            dae.printStackTrace();
            msgBuffer.append(dae.getMessages());
        }
        
        if(!msgBuffer.toString().equals("")){
            throw new CustomDataAccessException(msgBuffer.toString());
        }
                
    }
     
    protected class GetTariffServiceProcedure extends StoredProcedure{
         private static final String SQL_TOS_TARIFF_SERVICE = "PCR_TOS_TARIFF_SETUP.PRR_GET_TARIFF_SERVICE_LIST";
                  
         protected  GetTariffServiceProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
             super(jdbcTemplate, SQL_TOS_TARIFF_SERVICE);
             
             declareParameter(new SqlOutParameter("p_o_v_service_list", OracleTypes.CURSOR, rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));   
             
             compile();
         }
         
         protected List<TosNewTariffSetupServiceMod> getTosTariffServiceList(Map mapParams){
             Map outMap = new HashMap();
             List<TosNewTariffSetupServiceMod> returnList = new ArrayList<TosNewTariffSetupServiceMod>();
             
             try{
                 outMap = execute(mapParams);
                 returnList = (List<TosNewTariffSetupServiceMod>) outMap.get("p_o_v_service_list");
             }catch(Exception ex){
                ex.printStackTrace();
             }
             
             return returnList;
         }
     }
     
    private class TariffServiceRowMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int row) throws SQLException { 
             TosNewTariffSetupServiceMod serviceBean = new TosNewTariffSetupServiceMod();             
             serviceBean.setServiceCd(RutString.nullToStr(rs.getString("SERVICE")));
             serviceBean.setServiceDesc(RutString.nullToStr(rs.getString("SERVICE_DESCR")));
             serviceBean.setIsDelete(false);             
             serviceBean.setRowStatus(serviceBean.UPDATE);
             return serviceBean;
         }
     }
     
    protected class GetTariffPortProcedure extends StoredProcedure{
         private static final String SQL_TOS_TARIFF_PORT = "PCR_TOS_TARIFF_SETUP.PRR_GET_TARIFF_PORT_LIST";
         
         protected GetTariffPortProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
             super(jdbcTemplate, SQL_TOS_TARIFF_PORT);
             declareParameter(new SqlOutParameter("p_o_v_port_list", OracleTypes.CURSOR, rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));   
             
             compile();
         }
         
         protected List<TosNewTariffSetupPortTariffMod> getTariffPortList(Map paramsMap){
             Map outMap = new HashMap();
             List<TosNewTariffSetupPortTariffMod> resultList = new ArrayList<TosNewTariffSetupPortTariffMod>();
             
             try{
                 outMap = execute(paramsMap);
                 resultList = (List<TosNewTariffSetupPortTariffMod>) outMap.get("p_o_v_port_list");
             }catch(Exception ex){
                 ex.printStackTrace();
             }
             
             return resultList;
         }
     }
     
    private class TariffPortRowMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int row) throws SQLException{
             TosNewTariffSetupPortTariffMod portBean = new TosNewTariffSetupPortTariffMod();             
             portBean.setOperationCd(RutString.nullToStr(rs.getString("OPR_CODE")));
             portBean.setOperationDesc(RutString.nullToStr(rs.getString("OPR_DESCR")));
             portBean.setRateRef(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
             portBean.setTosRateSeq(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));
             portBean.setSocProforma(RutString.nullToStr(rs.getString("SOC_PROFORMA")));
             portBean.setSeqNo(RutString.nullToStr(rs.getString("SEQ_NO")));
             portBean.setEffDate(RutString.nullToStr(rs.getString("EFFECTIVE_DATE")));
             portBean.setExpDate(RutString.nullToStr(rs.getString("EXPIRY_DATE")));
             portBean.setIsDelete(false);
             portBean.setRowStatus(portBean.UPDATE);
             return portBean;
         }
     }
     
    protected class GetTariffVendorProcedure extends StoredProcedure{
         private static final String SQL_TOS_TARIFF_VENDORS = "PCR_TOS_TARIFF_SETUP.PRR_GET_TARIFF_VENDOR_LIST";
         
         protected GetTariffVendorProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
             super(jdbcTemplate, SQL_TOS_TARIFF_VENDORS);
             declareParameter(new SqlOutParameter("p_o_v_vendor_list", OracleTypes.CURSOR, rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));   
             declareParameter(new SqlInOutParameter("p_i_v_service",OracleTypes.VARCHAR,rowMapper));   
             compile();             
         }
         
         protected List<TosNewTariffSetupVendorsMod> getVednorsList(Map paramsMap){
             Map outMap = new HashMap();
             
             List<TosNewTariffSetupVendorsMod> vendorList = new ArrayList<TosNewTariffSetupVendorsMod>();
             
             try{
                 outMap = execute(paramsMap);
                 vendorList = (List<TosNewTariffSetupVendorsMod>) outMap.get("p_o_v_vendor_list");
             }catch(Exception ex){
                 ex.printStackTrace();
             }
             
             return vendorList;
         }
     }
     
    private class TosTariffVednorRowMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int row) throws SQLException{
             TosNewTariffSetupVendorsMod vendorBean = new TosNewTariffSetupVendorsMod();
             vendorBean.setVesselOpr(RutString.nullToStr(rs.getString("VESSEL_OPR")));
             vendorBean.setVendorCode(RutString.nullToStr(rs.getString("VENDOR_CODE1")));
             vendorBean.setVendorDesc(RutString.nullToStr(rs.getString("VENDOR_DESCR")));
             vendorBean.setOprType(RutString.nullToStr(rs.getString("OPR_CODE")));
             vendorBean.setOprTypeDesc(RutString.nullToStr(rs.getString("OPR_DESCR")));
             vendorBean.setPayByFCS(RutString.nullToStr(rs.getString("PAY_BY_FSC")));
             vendorBean.setEffDate(RutString.nullToStr(rs.getString("EFF_DATE")));
             vendorBean.setExpDate(RutString.nullToStr(rs.getString("EXP_DATE")));
             vendorBean.setRateRef(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
             vendorBean.setReference(RutString.nullToStr(rs.getString("REF_NO")));
             vendorBean.setIsDelete(false);
             vendorBean.setRowStatus(vendorBean.UPDATE);
             return vendorBean;
         }
     }
    
    protected class GetTariffCustProcedure extends StoredProcedure{
         private static final String SQL_TOS_TARIFF_CUST = "PCR_TOS_TARIFF_SETUP.PRR_GET_TARIFF_CUSTOMER_LIST";
         
         protected GetTariffCustProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
             super(jdbcTemplate, SQL_TOS_TARIFF_CUST);
             declareParameter(new SqlOutParameter("p_o_v_cust_list", OracleTypes.CURSOR, rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
             declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));   
             declareParameter(new SqlInOutParameter("p_i_v_service",OracleTypes.VARCHAR,rowMapper));   
             compile();
         }
         
         protected List<TosNewTariffSetupCustomersMod> getTariffCustomers(Map paramsMap){
             Map outMap = new HashMap();
             List<TosNewTariffSetupCustomersMod> returList = new ArrayList<TosNewTariffSetupCustomersMod>();
             try{
                 outMap = execute(paramsMap);
                 returList = (List<TosNewTariffSetupCustomersMod>) outMap.get("p_o_v_cust_list");
             } catch(Exception ex){
                 ex.printStackTrace();
             }
             
             return returList;
         }
     }
     
    private class TosTariffCustomerRowMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int row) throws SQLException{
             TosNewTariffSetupCustomersMod custBean = new TosNewTariffSetupCustomersMod();
             custBean.setVesselOpr(RutString.nullToStr(rs.getString("VESSEL_OPERATOR")));
             custBean.setCustomerCd(RutString.nullToStr(rs.getString("CUSTOMER_CODE")));
             custBean.setCustomerDesc(RutString.nullToStr(rs.getString("CUST_DESCR")));
             custBean.setOprType(RutString.nullToStr(rs.getString("OPR_CODE")));
             custBean.setOprTypeDesc(RutString.nullToStr(rs.getString("OPR_DESCR")));
             custBean.setEffDate(RutString.nullToStr(rs.getString("EFFECTIVE_DATE")));
             custBean.setExpDate(RutString.nullToStr(rs.getString("EXPIRY_DATE")));
             custBean.setSeqNo(RutString.nullToStr(rs.getString("SEQ_NO")));
             custBean.setRecoveryRateRef(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
             custBean.setTosRateSeqNo(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));
             custBean.setRowStatus(custBean.UPDATE);
             return custBean;
         }
     }
     
    protected class GetTariffSlotPartnerProcedure extends StoredProcedure{
        private static final String SQL_TOS_TARIFF_SLOT = "PCR_TOS_TARIFF_SETUP.PRR_GET_SLOT_PARTNER_LIST";
        
        protected GetTariffSlotPartnerProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_TARIFF_SLOT);
            declareParameter(new SqlOutParameter("p_o_v_cust_list", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));   
            declareParameter(new SqlInOutParameter("p_i_v_service",OracleTypes.VARCHAR,rowMapper));   
            compile();
        }
        
        protected List<TosNewTariffSetupSlotPartnerMod> getTariffCustomers(Map paramsMap){
            Map outMap = new HashMap();
            List<TosNewTariffSetupSlotPartnerMod> returList = new ArrayList<TosNewTariffSetupSlotPartnerMod>();
            try{
                outMap = execute(paramsMap);
                returList = (List<TosNewTariffSetupSlotPartnerMod>) outMap.get("p_o_v_cust_list");
            } catch(Exception ex){
                ex.printStackTrace();
            }
            
            return returList;
        }
    }
    
    private class TosTariffSlotPartnerRowMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException{
            TosNewTariffSetupSlotPartnerMod custBean = new TosNewTariffSetupSlotPartnerMod();
            custBean.setVesselOpr(RutString.nullToStr(rs.getString("VESSEL_OPERATOR")));
            custBean.setSlotPartner(RutString.nullToStr(rs.getString("CUSTOMER_CODE")));
            custBean.setSlotPartnerDesc(RutString.nullToStr(rs.getString("CUST_DESCR")));
            custBean.setOprType(RutString.nullToStr(rs.getString("OPR_CODE")));
            custBean.setOprTypeDesc(RutString.nullToStr(rs.getString("OPR_DESCR")));
            custBean.setEffDate(RutString.nullToStr(rs.getString("EFFECTIVE_DATE")));
            custBean.setExpDate(RutString.nullToStr(rs.getString("EXPIRY_DATE")));
            custBean.setSeqNo(RutString.nullToStr(rs.getString("SEQ_NO")));
            custBean.setRecoveryRateRef(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
            custBean.setTosRateSeqNo(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));
            custBean.setProforma(RutString.nullToStr(rs.getString("PROFORMA")));
            custBean.setRowStatus(custBean.UPDATE);
            return custBean;
        }
    }
    
    protected class GetTariffNonSlotPartnerProcedure extends StoredProcedure{
        private static final String SQL_TOS_NON_SLOT = "PCR_TOS_TARIFF_SETUP.PRR_GET_NON_PARTNER_LIST";
        
        protected GetTariffNonSlotPartnerProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_NON_SLOT);
            declareParameter(new SqlOutParameter("p_o_v_non_list", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));   
            declareParameter(new SqlInOutParameter("p_i_v_service",OracleTypes.VARCHAR,rowMapper));   
            compile();
        }
        
        protected List<TosNewTariffSetupNonRclPartnerMod> getTariffNonSlotPartners(Map paramsMap){
            Map outMap = new HashMap();
            List<TosNewTariffSetupNonRclPartnerMod> returList = new ArrayList<TosNewTariffSetupNonRclPartnerMod>();
            try{
                outMap = execute(paramsMap);
                returList = (List<TosNewTariffSetupNonRclPartnerMod>) outMap.get("p_o_v_non_list");
            } catch(Exception ex){
                ex.printStackTrace();
            }
            
            return returList;
        }
    }
    
    private class TosTariffNonSlotPartnerRowMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException{
            TosNewTariffSetupNonRclPartnerMod bean = new TosNewTariffSetupNonRclPartnerMod();
            bean.setVesselOpr(RutString.nullToStr(rs.getString("VESSEL_OPERATOR")));
            bean.setVendorCode(RutString.nullToStr(rs.getString("VENDOR_CODE")));
            bean.setVendorDesc(RutString.nullToStr(rs.getString("VENDOR_DESCR")));
            bean.setOprType(RutString.nullToStr(rs.getString("OPR_CODE")));
            bean.setOprTypeDesc(RutString.nullToStr(rs.getString("OPR_DESCR")));
            bean.setEffDate(RutString.nullToStr(rs.getString("EFFECTIVE_DATE")));
            bean.setExpDate(RutString.nullToStr(rs.getString("EXPIRY_DATE")));
            bean.setSeqNo(RutString.nullToStr(rs.getString("SEQ_NO")));
            bean.setRateRef(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
            bean.setTosRateSeqNo(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));
            bean.setPayTo(RutString.nullToStr(rs.getString("PAY_TO")));
            bean.setBuyTerm(RutString.nullToStr(rs.getString("BUY_TERM")));
            bean.setRowStatus(bean.UPDATE);
            return bean;
        }
    }
    
    private class UpdateSetupPortProcedure extends StoredProcedure{
        private static final String SQL_INS_RATE_HDR = "PCR_TOS_TARIFF_SETUP.PRR_UPD_TOS_SETUP_PORT";
        
        protected UpdateSetupPortProcedure (JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, SQL_INS_RATE_HDR);
            
            declareParameter(new SqlParameter("p_i_v_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_currency", Types.VARCHAR));
            compile();
        }
        
        protected boolean updateSetupPort(Map mapParams){
            boolean isSuccess = false;
            Map inParam = new HashMap();
            
            inParam.put("p_i_v_port", (String) mapParams.get("p_i_v_port"));
            inParam.put("p_i_v_terminal", (String) mapParams.get("p_i_v_terminal"));
            inParam.put("p_i_v_currency", (String) mapParams.get("p_i_v_currency"));
            Map outParameters = execute(inParam);
            
            if(outParameters.size() > 0){
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    private class InsertRateServiceProcedure extends StoredProcedure{
        private static final String SQL_UPD_SERVICE = "PCR_TOS_TARIFF_SETUP.PRR_INS_RATE_SERVICE";
        
        protected InsertRateServiceProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, SQL_UPD_SERVICE);
            
            declareParameter(new SqlParameter("p_i_v_line", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_trade", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_service", Types.VARCHAR));            
            compile();
        }
        
        protected boolean insertRateService(Map mapParams){
            boolean isSuccess = false;
            Map inParam = new HashMap();
            
            inParam.put("p_i_v_line", (String) mapParams.get    ("p_i_v_line"));
            inParam.put("p_i_v_trade", (String) mapParams.get   ("p_i_v_trade"));
            inParam.put("p_i_v_agent", (String) mapParams.get   ("p_i_v_agent"));
            inParam.put("p_i_v_user", (String) mapParams.get    ("p_i_v_user"));
            inParam.put("p_i_v_port", (String) mapParams.get    ("p_i_v_port"));
            inParam.put("p_i_v_terminal", (String) mapParams.get("p_i_v_terminal"));
            inParam.put("p_i_v_service", (String) mapParams.get ("p_i_v_service"));
                        
            Map outParameters = execute(inParam);
            
            if(outParameters.size() > 0){
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    private class InsertTerminalTariffProcedure extends StoredProcedure{
        private static final String SQL_UPD_SERVICE = "PCR_TOS_TARIFF_SETUP.PRR_INS_UPD_TERM_TARIFF";
        
        protected InsertTerminalTariffProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, SQL_UPD_SERVICE);
            
            declareParameter(new SqlParameter("p_i_v_line", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_trade", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_terminal", Types.VARCHAR));                
            declareParameter(new SqlParameter("p_i_v_opr_cd", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_rate_ref", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_eff_dt", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_exp_dt", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_soc_prof", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_fifo_prof", Types.VARCHAR));          
            declareParameter(new SqlParameter("p_i_v_row_sts", Types.VARCHAR));    
            declareParameter(new SqlParameter("p_i_v_seq_no", Types.VARCHAR));    
            compile();
        }
        
        protected boolean insertTermTariff(Map mapParams){
            boolean isSuccess = false;
            Map inParam = new HashMap();
            
            inParam.put("p_i_v_line", (String) mapParams.get    ("p_i_v_line"));
            inParam.put("p_i_v_trade", (String) mapParams.get   ("p_i_v_trade"));
            inParam.put("p_i_v_agent", (String) mapParams.get   ("p_i_v_agent"));
            inParam.put("p_i_v_user", (String) mapParams.get    ("p_i_v_user"));
            inParam.put("p_i_v_port", (String) mapParams.get    ("p_i_v_port"));
            inParam.put("p_i_v_terminal", (String) mapParams.get("p_i_v_terminal"));                
            inParam.put("p_i_v_opr_cd", (String) mapParams.get ("p_i_v_opr_cd"));
            inParam.put("p_i_v_rate_ref", (String) mapParams.get ("p_i_v_rate_ref"));
            inParam.put("p_i_v_eff_dt", (String) mapParams.get ("p_i_v_eff_dt"));
            inParam.put("p_i_v_exp_dt", (String) mapParams.get ("p_i_v_exp_dt"));
            inParam.put("p_i_v_soc_prof", (String) mapParams.get ("p_i_v_soc_prof"));
            inParam.put("p_i_v_fifo_prof", (String) mapParams.get ("p_i_v_fifo_prof"));
            inParam.put("p_i_v_row_sts", mapParams.get ("p_i_v_term_row_sts").toString());
            inParam.put("p_i_v_seq_no",(String) mapParams.get ("p_i_v_term_seq_no"));
            
            Map outParameters = execute(inParam);
            
            if(outParameters.size() > 0){
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    private class InsertSlotPartnerProcedure extends StoredProcedure{
        private static final String SQL_UPD_SERVICE = "PCR_TOS_TARIFF_SETUP.PRR_INS_UPD_SLOT";
        
        protected InsertSlotPartnerProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, SQL_UPD_SERVICE);
            
            declareParameter(new SqlParameter("p_i_v_line", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_trade", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_terminal", Types.VARCHAR));         
            declareParameter(new SqlParameter("p_i_v_service", Types.VARCHAR));            
            declareParameter(new SqlParameter("p_i_v_opr_cd", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_rate_ref", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_eff_dt", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_exp_dt", Types.VARCHAR));      
            declareParameter(new SqlParameter("p_i_v_soc_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_prof", Types.VARCHAR));            
            declareParameter(new SqlParameter("p_i_v_cust_cd", Types.VARCHAR));
            declareParameter(new SqlParameter("p_i_v_vsl_opr", Types.VARCHAR));            
            compile();
        }
        
        protected boolean insertTermTariff(Map mapParams){
            boolean isSuccess = false;
            Map inParam = new HashMap();
            
            inParam.put("p_i_v_line", (String) mapParams.get    ("p_i_v_line"));
            inParam.put("p_i_v_trade", (String) mapParams.get   ("p_i_v_trade"));
            inParam.put("p_i_v_agent", (String) mapParams.get   ("p_i_v_agent"));
            inParam.put("p_i_v_user", (String) mapParams.get    ("p_i_v_user"));
            inParam.put("p_i_v_port", (String) mapParams.get    ("p_i_v_port"));
            inParam.put("p_i_v_terminal", (String) mapParams.get("p_i_v_terminal"));                
            inParam.put("p_i_v_opr_cd", (String) mapParams.get ("p_i_v_slot_opr_cd"));
            inParam.put("p_i_v_rate_ref", (String) mapParams.get ("p_i_v_rate_ref"));
            inParam.put("p_i_v_eff_dt", (String) mapParams.get ("p_i_v_eff_dt"));
            inParam.put("p_i_v_exp_dt", (String) mapParams.get ("p_i_v_exp_dt"));
            inParam.put("p_i_v_soc_prof", (String) mapParams.get ("p_i_v_soc_prof"));
            inParam.put("p_i_v_fifo_prof", (String) mapParams.get ("p_i_v_fifo_prof"));
            
            Map outParameters = execute(inParam);
            
            if(outParameters.size() > 0){
                isSuccess = true;
            }
            return isSuccess;
        }
    }
}
