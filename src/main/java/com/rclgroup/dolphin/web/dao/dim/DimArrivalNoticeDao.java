/*-----------------------------------------------------------------------------------------------------------  
DimArrivalNoticeDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 29/21/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.dim;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface DimArrivalNoticeDao {

    public String makeArrivalNoticeSqlStatment(String criteriaBy, String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException;

    public int findCountForArrivalNoticeByVesselVoyageList(String columnName, String conditionWild, String columnFind 
        ,String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException;

    public String makeArrivalNoticeSqlStatment(String criteriaBy, String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException;
    
    public int findCountForArrivalNoticeByInVoyageList(String columnName, String conditionWild, String columnFind 
        ,String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException;
    
    /**
     * Get list of Arrival Notice (Vessel / Voyage List)
     * @param recFirst
     * @param recLast
     * @param columnName
     * @param conditionWild
     * @param columnFind
     * @param sortBy
     * @param sortIn
     * @param service
     * @param vessel
     * @param voyage
     * @param direction
     * @param pol
     * @param pod
     * @param podTerminal
     * @param blId
     * @param cocSoc
     * @param permissionUser
     * @return list of Arrival Notice
     * @throws DataAccessException
     */
    public List findArrivalNoticeByVesselVoyageList(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortIn
        ,String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException;
    
    /**
     * Get list of Arrival Notice (InVoyage List)
     * @param recFirst
     * @param recLast
     * @param columnName
     * @param conditionWild
     * @param columnFind
     * @param sortBy
     * @param sortIn
     * @param invoyagePort
     * @param sessionId
     * @param pol
     * @param pod
     * @param podTerminal
     * @param blId
     * @param cocSoc
     * @param permissionUser
     * @return list of Arrival Notice
     * @throws DataAccessException
     */
    public List findArrivalNoticeByInVoyageList(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortIn
        ,String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException;
    
    /**
     * Get list of Delivery Order Print (BL List)
     * @param columnName
     * @param conditionWild
     * @param columnFind
     * @param vessel
     * @param voyage
     * @param pol
     * @param pod
     * @param podTerminal
     * @param permissionUser
     * @return list of Delivery Order Print
     * @throws DataAccessException
     */
    public List findArrivalNoticeByBlList(String columnName, String conditionWild, String columnFind, String vessel, String voyage, String pol, String pod, String podTerminal, String permissionUser) throws DataAccessException;
    
}
