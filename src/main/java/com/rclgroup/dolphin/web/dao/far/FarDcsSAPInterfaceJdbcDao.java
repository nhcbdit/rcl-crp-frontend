package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.far.FarDcsSAPInterfaceMod;

import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class FarDcsSAPInterfaceJdbcDao extends RrcStandardDao implements FarDcsSAPInterfaceDao{

    private GetFarDcsSAPInterfaceSapTaxListProcedure getArDcsSAPInterfaceSapTaxList;  
    private  InsertArDcsSAPInterfaceSapTaxListProcedure insertArDcsSAPInterfaceSapTaxList;
    
    
    public FarDcsSAPInterfaceJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        
        getArDcsSAPInterfaceSapTaxList = new GetFarDcsSAPInterfaceSapTaxListProcedure(getJdbcTemplate(), new FarSAPInterfaceMapper());     
        insertArDcsSAPInterfaceSapTaxList = new InsertArDcsSAPInterfaceSapTaxListProcedure(getJdbcTemplate());     
    }
    
    public Map<String,String> insertIntoTable(String table,String country,String FSC,String chargeCode,String taxCode,String taxRate,String sapTaxCode,String remark,String user){
    

    
    return  insertArDcsSAPInterfaceSapTaxList.insertArDcsSAPInterfaceSapTaxList(table,country, FSC,chargeCode, taxCode, taxRate, sapTaxCode, remark, user);         
    
    }
    
    public List listSAPTax(String table,String country,String FSC,String chargeCode,String taxCode,String taxRate,String sapTaxCode){
      
        Map map = new HashMap();
        
       
        map.put("P_DCS_COUNTRY",country);
        map.put("P_DCS_FSC",FSC);
        map.put("P_DCS_CHARGE_CODE",chargeCode);
        map.put("P_DCS_TAX_CODE",taxCode);
        map.put("P_DCS_TAX_RATE",taxRate);
        map.put("P_SAP_VALUE",sapTaxCode);
        map.put("P_TABLE",table);
        
        List resultList = getArDcsSAPInterfaceSapTaxList.getArDcsSAPInterfaceSapTaxList(map);             
        
        return resultList;
    }
    
    protected class InsertArDcsSAPInterfaceSapTaxListProcedure extends StoredProcedure{
        private static final String AR_DCS_SAP_INTERFACE = "VASAPPS.PCR_AR_DCS_SAP_MAPPING.PRR_MANAGE_DCS_SAP_MAPPING";
        
        protected InsertArDcsSAPInterfaceSapTaxListProcedure(JdbcTemplate jdbcTemplate){
        
            super(jdbcTemplate, AR_DCS_SAP_INTERFACE);
            
            
            declareParameter(new SqlInOutParameter("P_ACTIVITY", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TABLE",Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_FSC",Types.VARCHAR));        
            declareParameter(new SqlInOutParameter("P_FRT_SUR_CHARGE_CODE", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TAX_CODE",Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_TAX_RATE",Types.VARCHAR)); // Types.FLOAT       
            declareParameter(new SqlInOutParameter("P_SAP_VALUE", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("P_REMARK",Types.VARCHAR));    
            declareParameter(new SqlInOutParameter("P_USER",Types.VARCHAR));   
            declareParameter(new SqlOutParameter("P_RESULT",Types.VARCHAR));   
            compile();
        }
    
        protected Map<String,String>  insertArDcsSAPInterfaceSapTaxList(String table,String country,String FSC,String chargeCode,String taxCode,String taxRate,String sapTaxCode,String remark,String user){
             Map mapReturn = new HashMap();
             Map map = new HashMap(9);
                     map.put("P_ACTIVITY","I");
                     map.put("P_TABLE",table);
                     map.put("P_FSC",FSC);
                     map.put("P_FRT_SUR_CHARGE_CODE",chargeCode);
                     map.put("P_TAX_CODE",taxCode);
                     map.put("P_TAX_RATE",taxRate);//new Float(RutString.toFloat(taxRate))); //new Integer(RutString.toInteger(taxRate)));
                     map.put("P_SAP_VALUE",sapTaxCode);
                     map.put("P_REMARK",remark);
                     map.put("P_USER",user);
             String insertResult = "";             
            
             try
             {
                 Map outParameters = execute(map);
                 //System.out.println("[outParameters.size() : ]"+outParameters.size());
                     
                 if (outParameters.size() > 0) 
                 {
                    
                     insertResult = (String) outParameters.get("P_RESULT");   
                     mapReturn.put("P_RESULT",insertResult);                     
                 }
             }
             catch(Exception e)
             {
                 e.printStackTrace();
                 throw new CustomDataAccessException(e.toString());
             }
            
            return mapReturn;
         }
    }
   
    
    //  separate   code by mooyong
    
    protected class GetFarDcsSAPInterfaceSapTaxListProcedure extends StoredProcedure{
            private static final String FAR_DCS_SAP_INTERFACE = "VASAPPS.PCR_AR_DCS_SAP_MAPPING.PRR_GET_DCS_SAP_MAPPING";
        
        protected GetFarDcsSAPInterfaceSapTaxListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
        
            super(jdbcTemplate,FAR_DCS_SAP_INTERFACE);
                            
            declareParameter(new SqlOutParameter("P_DCS_SAP_VALUE", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("P_TABLE",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_DCS_COUNTRY",OracleTypes.VARCHAR,rowMapper));        
            declareParameter(new SqlInOutParameter("P_DCS_FSC", OracleTypes.VARCHAR, rowMapper));
            declareParameter(new SqlInOutParameter("P_DCS_CHARGE_CODE",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_DCS_TAX_CODE",OracleTypes.VARCHAR,rowMapper));        
            declareParameter(new SqlInOutParameter("P_DCS_TAX_RATE", OracleTypes.VARCHAR, rowMapper));
            declareParameter(new SqlInOutParameter("P_SAP_VALUE",OracleTypes.VARCHAR,rowMapper));   
            
            compile();
        }
    
        protected List  getArDcsSAPInterfaceSapTaxList(Map mapParams){
            Map outMap = new HashMap();     
            
            List<FarDcsSAPInterfaceMod> returnList = new ArrayList<FarDcsSAPInterfaceMod>();
            
            try{
                outMap = execute(mapParams);
                returnList = (List<FarDcsSAPInterfaceMod>) outMap.get("P_DCS_SAP_VALUE");
                //System.out.println("Raja:" +returnList.size());
                
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
         }
    }
    private class FarSAPInterfaceMapper implements RowMapper{
    
        public Object mapRow(ResultSet rs, int row) throws SQLException {
        
            FarDcsSAPInterfaceMod bean = new FarDcsSAPInterfaceMod();
            
            bean.setCharge(rs.getString("FRT_SUR_CHARGE_CODE"));
            bean.setFsc(rs.getString("FSC"));
            bean.setNumber("ROWNUM");
            bean.setSapTaxCode(rs.getString("TAX_RATE_TRANS"));
            bean.setTaxCode(rs.getString("TAX_CODE"));
            bean.setTaxRate(rs.getString("TAX_RATE"));
            return bean;
        }
    }
}

