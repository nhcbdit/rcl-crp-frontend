/*------------------------------------------------------
DexManifestBLPrintingJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
Author Kitti Pongsirisakun 01/12/10
- Change Log -------------------------------------------
## DD/MM/YY �User-      -TaskRef-           -ShortDescription-
01 04/08/10 KIT         N/A                 Created
02 21/06/12 NIP         PD_CR_20120425-01   DEX_DIM_Add function to select some BL
03 06/10/16 SarawutA.                       Create new method showHideGenExcelButton,generateExcelHeader,generateExcelDetail
04 27/06/18 Sarawut							Add search condition POL Terminal & coc/soc
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dex.DexManifestBLPrintingExcelMod;
import com.rclgroup.dolphin.web.model.dex.DexManifestBLPrintingMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DexManifestBLPrintingJdbcDao extends RrcStandardDao implements DexManifestBLPrintingDao {
    private InsertStoreProcedure insertStoreProcedure;
    private InsertSelectBlStoreProcedure insertSelectBlStoreProcedure;//##01
    private ClearPrintedBlStoreProcedure clearPrintedBlStoreProcedure;//##01
    private ShowHideGenExcelButtonProcedure showHideGenExcelButtonProcedure;
    private GenerateExcelHeaderProcedure generateExcelHeaderProcedure;
    private GenerateExcelDetailProcedure generateExcelDetailProcedure;
    public DexManifestBLPrintingJdbcDao() {
    }    
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        insertSelectBlStoreProcedure = new InsertSelectBlStoreProcedure(getJdbcTemplate());
        clearPrintedBlStoreProcedure = new ClearPrintedBlStoreProcedure(getJdbcTemplate());//##02
        generateExcelHeaderProcedure = new GenerateExcelHeaderProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelHdrList());
        generateExcelDetailProcedure = new GenerateExcelDetailProcedure(this.getJdbcTemplate(),new RowMapperGenerateExcelDtlList());
        showHideGenExcelButtonProcedure = new ShowHideGenExcelButtonProcedure(this.getJdbcTemplate());
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_MANIFEST_PRINT.PR_MAIN_MANIFEST";
    protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
           System.out.println("InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_ser", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ves", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voy", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_off", Types.VARCHAR));
            declareParameter(new SqlParameter("p_act", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_l_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_r_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_a_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc_code", Types.VARCHAR));
//            declareParameter(new SqlParameter("P_POL_TERMINAL", Types.VARCHAR));
//            declareParameter(new SqlParameter("P_COC_SOC", Types.VARCHAR));
            
          //  declareParameter(new SqlOutParameter("p_cur_temp", OracleTypes.CURSOR));
          //  declareParameter(new SqlOutParameter("p_sysdate", Types.VARCHAR));     
            compile();
            System.out.println("InsertStoreProcedure declare parameter end");
        }
    protected String insert(String service  , String vessel, String voyage,  String direction, String pol,  String pod,  String bl,  String office,  String act ,String fsc,
                                                String userName,String session,String line,String region,String agent,String fscLogin,String polTerminal,String cocSoc  ) {
        return insertPro(  service  ,  vessel,  voyage,   direction,  pol,   pod,   bl,   office,  act ,fsc, userName, session,line,region,agent,fscLogin,polTerminal,cocSoc   );
    }
    protected String insertPro(String service  , String vessel, String voyage,  String direction, String pol,  String pod,  String bl,  String office,  String act ,String fsc,
                                                       String userName,String session,String line,String region,String agent,String fscLogin,String polTerminal,String cocSoc) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            inParameters.put("p_ser",RutDatabase.stringToDb(service));
            inParameters.put("p_ves",RutDatabase.stringToDb(vessel));
            inParameters.put("p_voy",RutDatabase.stringToDb(voyage));
            inParameters.put("p_dir",RutDatabase.stringToDb(direction));
            inParameters.put("p_pol",RutDatabase.stringToDb(pol));
            inParameters.put("p_pod",RutDatabase.stringToDb(pod));
            inParameters.put("p_bl",RutDatabase.stringToDb(bl));
            inParameters.put("p_off",RutDatabase.stringToDb(office));
            inParameters.put("p_act",RutDatabase.stringToDb(act));
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            inParameters.put("p_usr",RutDatabase.stringToDb(userName));             
            inParameters.put("p_sid",RutDatabase.stringToDb(session));      
            inParameters.put("p_l_code",RutDatabase.stringToDb(line));
            inParameters.put("p_r_code",RutDatabase.stringToDb(region));
            inParameters.put("p_a_code",RutDatabase.stringToDb(agent));
            inParameters.put("p_fsc_code",RutDatabase.stringToDb(fscLogin));
//            inParameters.put("P_POL_TERMINAL",RutDatabase.stringToDb(polTerminal));
//            inParameters.put("P_COC_SOC",RutDatabase.stringToDb(cocSoc));
           
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_ser = "+inParameters.get("p_ser"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_ves = "+inParameters.get("p_ves"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_voy = "+inParameters.get("p_voy"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_dir = "+inParameters.get("p_dir"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_pol = "+inParameters.get("p_pol"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_pod = "+inParameters.get("p_pod"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_bl = "+inParameters.get("p_bl"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_off = "+inParameters.get("p_off"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_act = "+inParameters.get("p_act"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_fsc = "+inParameters.get("p_fsc"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_usr = "+inParameters.get("p_usr"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_sid = "+inParameters.get("p_sid"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_l_code = "+inParameters.get("p_l_code"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_r_code = "+inParameters.get("p_r_code"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_a_code = "+inParameters.get("p_a_code"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: p_fsc_code = "+inParameters.get("p_fsc_code"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: P_POL_TERMINAL = "+inParameters.get("P_POL_TERMINAL"));
            System.out.println("[FarExceptionLateBillingJdbcDao][InsertStoreProcedure][insert]: P_COC_SOC = "+inParameters.get("P_COC_SOC"));
            
          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_sid");
               System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    
    /*************** ##02 **************/
    protected class InsertSelectBlStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_MANIFEST_PRINT.PR_INSERT_SELECT_BL";
    protected  InsertSelectBlStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            System.out.println("InsertSelectBlStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session", Types.VARCHAR));
            declareParameter(new SqlParameter("p_selectBl", Types.VARCHAR));
              
            compile();
            System.out.println("InsertSelectBlStoreProcedure declare parameter end");
        }
    protected String insertSelectBl(String user  , String session, String selectBl) {
        return insertSelectBlPro(  user  ,  session,  selectBl);
    }
    protected String insertSelectBlPro(String user  , String session, String selectBl) {
            System.out.println("InsertStoreProcedure assign value to  parameter begin");
            
            String sessionId = "";
            
            Map inParameters = new HashMap();
            inParameters.put("p_user",RutDatabase.stringToDb(user));
            inParameters.put("p_session",RutDatabase.stringToDb(session));
            inParameters.put("p_selectBl",RutDatabase.stringToDb(selectBl));          
           
            System.out.println("[DexManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_user = "+inParameters.get("p_user"));
            System.out.println("[DexManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_session = "+inParameters.get("p_session"));
            System.out.println("[DexManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_selectBl = "+inParameters.get("p_selectBl"));
            
          /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_sid");
               System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
            }
            return sessionId;
        }          
    } 
    
    public String  insertSelectBl(DexManifestBLPrintingMod bean) throws DataAccessException {
                                                         
            return insertSelectBlStoreProcedure.insertSelectBl( bean.getUserName()  ,  bean.getSessionId(),  bean.getSelectBl());
    }
    /*************** ##02 end **************/
    
     /*************** ##02 **************/
     protected class ClearPrintedBlStoreProcedure extends StoredProcedure {
     private static final String STORED_PROCEDURE_NAME = "PCR_DEX_MANIFEST_PRINT.PR_CLEAR_BL_PRINTED";
     protected  ClearPrintedBlStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);
             System.out.println("ClearPrintedBlStoreProcedure declare parameter begin");
             declareParameter(new SqlParameter("p_user", Types.VARCHAR));
             declareParameter(new SqlParameter("p_session", Types.VARCHAR));
               
             compile();
             System.out.println("ClearPrintedBlStoreProcedure declare parameter end");
         }
     protected String clearPrintedBl(String user  , String session) {
         return clearPrintedBlStorePro(  user  ,  session);
     }
     protected String clearPrintedBlStorePro(String user  , String session) {
             System.out.println("ClearPrintedBlStoreProcedure assign value to  parameter begin");
             
             String sessionId = "";
             
             Map inParameters = new HashMap();
             inParameters.put("p_user",RutDatabase.stringToDb(user));
             inParameters.put("p_session",RutDatabase.stringToDb(session));       
            
             System.out.println("[DexManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_user = "+inParameters.get("p_user"));
             System.out.println("[DexManifestBLPrintingJdbcDao][InsertSelectBlStoreProcedure][insert]: p_session = "+inParameters.get("p_session"));
             
           /*  System.out.println("InsertStoreProcedure assign value to  parameter end");*/
             Map outParameters = execute(inParameters);
             if (outParameters.size() > 0) {
                sessionId = RutDatabase.dbToString(outParameters, "p_sid");
                System.out.println("  p_sid >>>>>>>>>>>>> "+sessionId);
             }
             return sessionId;
         }          
     } 
     
     public String  clearPrintedBl(DexManifestBLPrintingMod bean) throws DataAccessException {
                                                          
             return clearPrintedBlStoreProcedure.clearPrintedBl( bean.getUserName()  ,  bean.getSessionId());
     }
     /*************** ##02 end **************/
     
    public String  getSessionId(String service  , String vessel, String voyage,  String direction, String pol,  String pod,  String bl,  String office,  String act ,String fsc,
                                String userName,String session,String line,String region,String agent,String fscLogin,String polTerminal,String cocSoc)throws DataAccessException {
                                                         
            return insertStoreProcedure.insert(  service  ,  vessel,  voyage,   direction,  pol,   pod,   bl,   office, act, fsc,  userName, session,line,region,agent,fscLogin,polTerminal,cocSoc );
    }
    
    public Vector getListManifestPrint(String session, String userName) throws DataAccessException{
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]");
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]: With status: sesseion "+session);
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]: With status: userName "+userName);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
            
        Vector manifestPrintVec = new Vector();
        DexManifestBLPrintingMod dexManifestBLPrintingMod = null;
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]: ");
        System.out.println("[DexManifestBLPrintingJdbcDao][getListManifestPrint]: Finished");
        
        sql.append("SELECT DISTINCT  ");
        sql.append("   mnf.SESSIONID");
        sql.append(" , mnf.BLNO");
        sql.append(" ,  mnf.BLIDDES");
        sql.append(" , mnf.BLCDTEC");
        sql.append(" , mnf.BLSTATUSDESC");
        sql.append(" , mnf.POL ");
        sql.append(" ,  mnf.POLT ");
        sql.append(" , mnf.POT ");
        sql.append(" , mnf.POD ");
        sql.append("  ,mnf.PODT");
        
        /********** ##02 ***********/
        //sql.append(" , PRINTFLAGDESC ");
        sql.append(" ,(");
        sql.append("    select decode(count(*),0,'N','Y') from rclapps.DEX_PRI_MNF_PRINTED_BL_TMP pbl");
        sql.append("    where pbl.BLNO=mnf.BLNO");
        sql.append("    and pbl.SESSION_ID=mnf.SESSIONID");
        sql.append("    and pbl.MODULE_CODE='DEX'");
        sql.append("    and pbl.RECORD_ADD_USER=mnf.USERNAME");
        sql.append("  )PRINTFLAGDESC");
        /********** ##02 end ***********/
        
        sql.append(" , mnf.USERNAME ");
        sql.append(" FROM DEX_PRI_MNF mnf ");
        sql.append(" WHERE mnf.SESSIONID = :sesseion ");
        sql.append(" AND mnf.USERNAME = :userName ");
        sql.append(" ORDER BY mnf.BLNO,mnf.POL,mnf.POD,mnf.BLCDTEC ");
        map.put("sesseion",session);
        map.put("userName",userName);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map); 
        while (rs.next()){
            dexManifestBLPrintingMod = new DexManifestBLPrintingMod();
            dexManifestBLPrintingMod.setSessionId(RutString.nullToStr(rs.getString("SESSIONID")));
            dexManifestBLPrintingMod.setBlNo(RutString.nullToStr(rs.getString("BLNO")));
            dexManifestBLPrintingMod.setBlIdDesc(RutString.nullToStr(rs.getString("BLIDDES")));
            dexManifestBLPrintingMod.setBlCreateDateConvert(RutString.nullToStr(rs.getString("BLCDTEC")));
            dexManifestBLPrintingMod.setBlStatusDesc(RutString.nullToStr(rs.getString("BLSTATUSDESC")));
            dexManifestBLPrintingMod.setPol(RutString.nullToStr(rs.getString("POL")));
            dexManifestBLPrintingMod.setPolTerminal(RutString.nullToStr(rs.getString("POLT")));
            dexManifestBLPrintingMod.setPot(RutString.nullToStr(rs.getString("POT")));
            dexManifestBLPrintingMod.setPod(RutString.nullToStr(rs.getString("POD")));
            dexManifestBLPrintingMod.setPodTerminal(RutString.nullToStr(rs.getString("PODT")));
            dexManifestBLPrintingMod.setPrintFlagDesc(RutString.nullToStr(rs.getString("PRINTFLAGDESC")));
            dexManifestBLPrintingMod.setUserName(RutString.nullToStr(rs.getString("USERNAME")));
            
            manifestPrintVec.add(dexManifestBLPrintingMod);
        }
        return manifestPrintVec;
        
    }
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException{
        return showHideGenExcelButtonProcedure.validateFsc(fsc);
    }
    
    public List<DexManifestBLPrintingExcelMod> generateExcelHeader(String userCode,String sessionid,String selectBl) throws DataAccessException{
        return generateExcelHeaderProcedure.generate(userCode,sessionid,selectBl);
    }
    
    public List<DexManifestBLPrintingExcelMod> generateExcelDetail(String userCode,String sessionid,String selectBl) throws DataAccessException{
        return generateExcelDetailProcedure.generate(userCode,sessionid,selectBl);
    }
    
    protected class ShowHideGenExcelButtonProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_MANIFEST_PRINT.PR_CHK_SHOW_GEN_EXCEL_BUTTON";
    protected  ShowHideGenExcelButtonProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            compile();

        }
    protected String validateFsc(String fsc) {         
        return validateFscPro(fsc);
    }
    protected String validateFscPro(String fsc) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_fsc",RutDatabase.stringToDb(fsc));
            String result = "";
            
            try  {
                outMap = execute(inParameters);
                if (outMap.size() > 0) {
                   result = RutDatabase.dbToString(outMap, "P_O_V_RESULT");
                }
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return result;
        }          
    }
    
    protected class GenerateExcelHeaderProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_MANIFEST_PRINT.PR_GET_EXCEL_HDR";
    protected  GenerateExcelHeaderProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_blno", Types.VARCHAR));
            compile();

        }
    protected List<DexManifestBLPrintingExcelMod> generate(String userCode,String sessionid,String selectBl) {
                                                       
        return generateExcelHdrPro(userCode, sessionid,selectBl);
    }
    protected List<DexManifestBLPrintingExcelMod> generateExcelHdrPro(String userCode, String sessionid,String selectBl) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_blno",RutDatabase.stringToDb(selectBl));
            List<DexManifestBLPrintingExcelMod> returnList = new ArrayList<DexManifestBLPrintingExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DexManifestBLPrintingExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelHdrList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DexManifestBLPrintingExcelMod bean = new DexManifestBLPrintingExcelMod();

            bean.setVesselCallSign(rs.getString("LVES_NAME"));
            bean.setVoyageNo(rs.getString("LVES"));
            bean.setCarrierCode(rs.getString("LVOY"));
            bean.setPortOfDischargeCode(rs.getString("POD_CODE"));
            bean.setPortOfDischargeSuffix(rs.getString("POD_SUFFIX"));
            bean.setEstimateDateOfArrival(rs.getString("EST_DATE_ARRIVAL"));
            
            return bean;
        }
    }
    
    protected class GenerateExcelDetailProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_DEX_MANIFEST_PRINT.PR_GET_EXCEL_DTL";
    protected  GenerateExcelDetailProcedure(JdbcTemplate jdbcTemplate,RowMapper rowMapper) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("P_O_V_RESULT_LIST", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlParameter("p_usr", Types.VARCHAR));
            declareParameter(new SqlParameter("p_sid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_blno", Types.VARCHAR));
            compile();

        }
    protected List<DexManifestBLPrintingExcelMod> generate(String userCode,String sessionid,String selectBl) {
                                                       
        return generateExcelDtlPro(userCode, sessionid,selectBl);
    }
    protected List<DexManifestBLPrintingExcelMod> generateExcelDtlPro(String userCode, String sessionid,String selectBl) {
            
            Map outMap = new HashMap();
            Map inParameters = new HashMap();
            inParameters.put("p_usr",RutDatabase.stringToDb(userCode));
            inParameters.put("p_sid",RutDatabase.stringToDb(sessionid));
            inParameters.put("p_blno",RutDatabase.stringToDb(selectBl));
            List<DexManifestBLPrintingExcelMod> returnList = new ArrayList<DexManifestBLPrintingExcelMod>();
            
            try  {
                outMap = execute(inParameters);
                returnList = (List<DexManifestBLPrintingExcelMod>)outMap.get("P_O_V_RESULT_LIST");
            } catch (DataAccessException ex)  {
                throw ex;
            }
            return returnList;
        }          
    }
    
    private class RowMapperGenerateExcelDtlList implements RowMapper {
        public Object mapRow(ResultSet rs, int rowNum)  throws SQLException{
            DexManifestBLPrintingExcelMod bean = new DexManifestBLPrintingExcelMod();

            bean.setCyOperator(rs.getString("OPERATOR_CODE"));
            bean.setRetentionClassification(rs.getString("RETENTION_CLASSIFICATION"));
            bean.setBlNo(rs.getString("BLNO"));
            bean.setPortOfLoadingCode(rs.getString("POL"));
            bean.setFinalDestinationCode(rs.getString("POD"));
            bean.setFinalDestinationName(rs.getString("POD_NAME"));
            bean.setPlaceOfTransferCode(rs.getString("POT"));
            bean.setPlaceOfTransferName(rs.getString("POT_NAME"));
            bean.setConsignorName(rs.getString("CONSIGNOR_NAME"));
            bean.setConsignorAddress(rs.getString("CONSIGNOR_ADDRESS"));
            bean.setConsignorPostCode(rs.getString("CONSIGNOR_POST_CODE"));
            bean.setConsignorCountryCode(rs.getString("CONSIGNOR_COUNTRY_CODE"));
            bean.setConsignorTelephoneNo(rs.getString("CONSIGNOR_TELEPHONE"));
            bean.setConsigneeName(rs.getString("CONSIGNEE_NAME"));
            bean.setConsigneeAddress(rs.getString("CONSIGNEE_ADDRESS"));
            bean.setConsigneePostCode(rs.getString("CONSIGNEE_POST_CODE"));
            bean.setConsigneeCountryCode(rs.getString("CONSIGNEE_COUNTRY_CODE"));
            bean.setConsigneeTelephoneNo(rs.getString("CONSIGNEE_TELEPHONE"));
            bean.setNotifyParty(rs.getString("NOTIFY_PARTY"));
            bean.setFreightCharges(rs.getString("FREIGHT_CHARGE"));
            bean.setFreightCurrencyCode(rs.getString("FREIGHT_CURRENCY_CODE"));
            bean.setTransship(rs.getString("TRANSSHIP"));
            bean.setTransshipReason(rs.getString("TRANSSHIP_REASON"));
            bean.setTransshipDuration(rs.getString("TRANSSHIP_DURATION"));
            bean.setRemarks(rs.getString("REMARKS"));
            bean.setDescriptionOfGoods(rs.getString("DESCRIPTION_GOODS"));
            bean.setSpecialCargoSign(rs.getString("SPECIAL_CARGO"));
            bean.setNumberOfPackage(rs.getString("PACKAGE_NUMBER"));
            bean.setUnitCodeOfPackage(rs.getString("PACKAGE_UNIT_CODE"));
            bean.setGrossWeight(rs.getString("GROSS_WEIGHT"));
            bean.setUnitCodeOfWeight(rs.getString("GROSS_WEIGHT_UNIT_CODE"));
            bean.setNetWeight(rs.getString("NET_WEIGHT"));
            bean.setUnitCodeOfNet(rs.getString("NET_WEIGHT_UNIT_CODE"));
            bean.setMeasurement(rs.getString("MEANSUREMENT"));
            bean.setUnitCodeOfMeasurement(rs.getString("MEANSUREMENT_UNIT_CODE"));
            bean.setMarksAndNos(rs.getString("MARKS_NOS"));
            bean.setContainerInformation(rs.getString("CONTAINER_INFORMATION"));
            
            return bean;
        }
    }
    
}
