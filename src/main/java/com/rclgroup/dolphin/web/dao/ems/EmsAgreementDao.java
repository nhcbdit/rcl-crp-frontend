 /*-----------------------------------------------------------------------------------------------------------  
 EmsAgreementDao.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 03/07/2008
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description 
 -----------------------------------------------------------------------------------------------------------*/   

  package com.rclgroup.dolphin.web.dao.ems;

  import java.util.List;
  import org.springframework.dao.DataAccessException;

 public interface EmsAgreementDao {

     /**
      * check valid of Agreement 
      * @param Agreement
      * @return valid of Agreement
      * @throws DataAccessException
      */
     public boolean isValid(String Agreement) throws DataAccessException;

     /**
      * check valid of Agreement code with status
      * @param Agreement
      * @param status
      * @return valid of Agreement code with status
      * @throws DataAccessException
      */
     public boolean isValid(String Agreement, String status) throws DataAccessException;

     /**
      * list Agreement records for help screen
      * @param find
      * @param search
      * @param wild
      * @return list of Agreement
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

     /**
      * list Agreement records for help screen with status
      * @param find
      * @param search
      * @param wild
      * @param status
      * @return list of Agreement with status
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;    
      
  }
