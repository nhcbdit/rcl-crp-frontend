/*-----------------------------------------------------------------------------------------------------------  
EzlExcelUploadOnlineDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Nipun Sutes 28/11/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;


import com.rclgroup.dolphin.web.model.bsa.BsaExportBsaModelMod;

import java.util.List;
import org.springframework.dao.DataAccessException;


public interface BsaExportBsaModelDao extends RriStandardDao {

    public BsaExportBsaModelMod findByKeyBsaModelID(String bsaModelId) throws DataAccessException;
}
