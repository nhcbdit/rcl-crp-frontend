package com.rclgroup.dolphin.web.dao.tos;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TosServiceDao {
    /**
     * check valid of Service#
     * @param service
     * @param status
     * @return valid of service
     * @throws DataAccessException
     */
    public boolean isServiceValid(String service, String status) throws DataAccessException;
    
    /**
     * check valid of port#
     * @param port
     * @param status
     * @return valid of port
     * @throws DataAccessException
     */
    public boolean isPortValid(String port, String status) throws DataAccessException;
    
    /**
     * check valid of terminal#
     * @param terminal
     * @param status
     * @return valid of terminal
     * @throws DataAccessException
     */
    public boolean isTerminalValid(String terminal, String status) throws DataAccessException;
        
    /**
     * list Service records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Service
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
    public boolean isCalculatedByDate(String terminal) throws DataAccessException;
}
