/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotMaintenanceSearchJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 02/12/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.ems.EmsPickUpReturnDepotMaintenanceMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class EmsPickUpReturnDepotMaintenanceSearchJdbcDao  extends RrcStandardDao implements EmsPickUpReturnDepotMaintenanceSearchDao{        
    private InsertHdrStoreProcedure insertHdrStoreProcedure;
    private UpdateHdrStoreProcedure updateHdrStoreProcedure;
    private DeleteHdrStoreProcedure deleteHdrStoreProcedure;
    private DeleteDtlByHdrStoreProcedure deleteDtlByHdrStoreProcedure;
    
    public EmsPickUpReturnDepotMaintenanceSearchJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao(); 
        insertHdrStoreProcedure = new InsertHdrStoreProcedure(getJdbcTemplate());
        updateHdrStoreProcedure = new UpdateHdrStoreProcedure(getJdbcTemplate());        
        deleteHdrStoreProcedure = new DeleteHdrStoreProcedure(getJdbcTemplate());  
        deleteDtlByHdrStoreProcedure = new DeleteDtlByHdrStoreProcedure(getJdbcTemplate());  
    }                                             
    
    public List listMappingForStatement(int rowAt, int rowTo,String depotCode, String pointCode, String porrFlag, String modeOfReturn, String service, String vessel, String voyage,String supplier, String shipper, String consignee, String contractParty, String bl, String bkg,String sortBy, String sortByIn) {
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: Started");              
        String sqlColumn = "PK_EMS_MULTI_DEPOT_HDR_ID, FK_FSC, FK_DEPOT_CODE, FK_POINT_CODE, MPORR,FK_SERVICE,VESSEL,VOYAGE,FK_CONTRACT_PARTY,FK_SHIPPER,FK_CONSIGNEE,FK_BL_NO,FK_BOOKING_NO,PORR_FLAG,RECORD_STATUS,RECORD_ADD_USER,RECORD_ADD_DATE,RECORD_CHANGE_USER,RECORD_CHANGE_DATE";
        StringBuffer sql = new StringBuffer();

        sql.append("SELECT PK_EMS_MULTI_DEPOT_HDR_ID ");
        sql.append(", FK_FSC ");
        sql.append(", FK_DEPOT_CODE ");
        sql.append(", FK_POINT_CODE ");
        sql.append(", MPORR ");
        sql.append(", FK_SERVICE ");
        sql.append(", VESSEL ");
        sql.append(", VOYAGE ");
        sql.append(", FK_CONTRACT_PARTY ");
        sql.append(", FK_SHIPPER ");
        sql.append(", FK_SUPPLIER ");
        sql.append(", FK_CONSIGNEE ");
        sql.append(", FK_BL_NO ");
        sql.append(", FK_BOOKING_NO ");
        sql.append(", PORR_FLAG ");
        sql.append(", RECORD_STATUS ");
        sql.append(", RECORD_ADD_USER ");
        sql.append(", RECORD_ADD_DATE ");
        sql.append(", RECORD_CHANGE_USER ");
        sql.append(", RECORD_CHANGE_DATE ");
        sql.append("FROM EMS_MULTI_DEPOT_HDR ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");        
        if (!modeOfReturn.equalsIgnoreCase("")){
            strWhere.append(" and MPORR = '"+modeOfReturn+"' ");
        }
            if (modeOfReturn.equalsIgnoreCase("SV")){
                if (service != null && service.length() > 0) {
                    strWhere.append(" and FK_SERVICE = '"+service+"' ");
                }
            }else if(modeOfReturn.equalsIgnoreCase("VV")){
                if (service != null && service.length() > 0) {
                    strWhere.append(" and FK_SERVICE = '"+service+"' ");
                }
                if (service != null && vessel.length() > 0) {
                    strWhere.append(" and VESSEL = '"+vessel+"' ");
                }
                if (service != null && voyage.length() > 0) {
                    strWhere.append(" and VOYAGE = '"+voyage+"' ");
                }
            }else if(modeOfReturn.equalsIgnoreCase("SP")){
                if (service != null && shipper.length() > 0) {
                    strWhere.append(" and FK_SHIPPER = '"+shipper+"' ");        
                }
            }else if(modeOfReturn.equalsIgnoreCase("SU")){
                if (service != null && supplier.length() > 0) {
                    strWhere.append(" and FK_SUPPLIER = '"+supplier+"' ");     
                }
            }else if(modeOfReturn.equalsIgnoreCase("CN")){
                if (service != null && consignee.length() > 0) {
                    strWhere.append(" and FK_CONSIGNEE = '"+consignee+"' ");                
                }
            }else if(modeOfReturn.equalsIgnoreCase("CP")){
                if (service != null && contractParty.length() > 0) {
                    strWhere.append(" and FK_CONTRACT_PARTY = '"+contractParty+"' ");
                }
            }else if(modeOfReturn.equalsIgnoreCase("BK")){
                if (service != null && bkg.length() > 0) {
                    strWhere.append(" and FK_BOOKING_NO = '"+bkg+"' ");
                }
            }else if(modeOfReturn.equalsIgnoreCase("BL")){
                if (service != null && bl.length() > 0) {
                    strWhere.append(" and FK_BL_NO = '"+bl+"' ");
                }
            }
        
        if (!RutString.isEmptyString(depotCode)) {
            strWhere.append(" and FK_DEPOT_CODE = '"+depotCode+"' ");
        }        
        if (!RutString.isEmptyString(pointCode)) {
            strWhere.append(" and FK_POINT_CODE = '"+pointCode+"' ");
        }
        if (!RutString.isEmptyString(porrFlag)) {
            strWhere.append(" and PORR_FLAG = '"+porrFlag+"' ");
        }
        
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        // Query by record number range
        sql.append(createSqlCriteriaMappingSortBy(sortBy, sortByIn));
        System.out.println("where = "+strWhere.toString());
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: SQL ="+sql.toString());
        sql = this.addSqlForNewHelp(sql, rowAt, rowTo, sqlColumn);
        
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: SQL new ="+sql.toString());
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][listDepotForStatementByHeader]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           EmsPickUpReturnDepotMaintenanceMod ff = new EmsPickUpReturnDepotMaintenanceMod();
                           ff.setMultiDepotHdrId(RutString.nullToStr(rs.getString("PK_EMS_MULTI_DEPOT_HDR_ID")));
                           ff.setFsc(RutString.nullToStr(rs.getString("FK_FSC")));
                           ff.setDepotCode(RutString.nullToStr(rs.getString("FK_DEPOT_CODE")));
                           ff.setPointCode(RutString.nullToStr(rs.getString("FK_POINT_CODE")));
                           ff.setMporr(RutString.nullToStr(rs.getString("MPORR")));
                           ff.setService(RutString.nullToStr(rs.getString("FK_SERVICE")));
                           ff.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           ff.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           ff.setContractParty(RutString.nullToStr(rs.getString("FK_CONTRACT_PARTY")));
                           ff.setShipper(RutString.nullToStr(rs.getString("FK_SHIPPER")));
                           ff.setSupplier(RutString.nullToStr(rs.getString("FK_SUPPLIER")));
                           ff.setConsignee(RutString.nullToStr(rs.getString("FK_CONSIGNEE")));
                           ff.setBl(RutString.nullToStr(rs.getString("FK_BL_NO")));
                           ff.setBkg(RutString.nullToStr(rs.getString("FK_BOOKING_NO")));
                           ff.setPorrFlag(RutString.nullToStr(rs.getString("PORR_FLAG")));
                           ff.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));                                                                                     
                           ff.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           ff.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           ff.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           ff.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           ff.setActivityFlag(ff.ACTIVITY_READ);
                           ff.setActivityFlagOri(ff.ACTIVITY_READ);
                           return ff;
                    }
                });
    }
    
    public int countListMappingForStatement(String depotCode, String pointCode, String porrFlag, String modeOfReturn, String service, String vessel, String voyage,String supplier, String shipper, String consignee, String contractParty, String bl, String bkg) {
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][countListDepotForStatementByHeader]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PK_EMS_MULTI_DEPOT_HDR_ID ");
        sql.append(", FK_FSC ");
        sql.append(", FK_DEPOT_CODE ");
        sql.append(", FK_POINT_CODE ");
        sql.append(", MPORR ");
        sql.append(", FK_SERVICE ");
        sql.append(", VESSEL ");
        sql.append(", VOYAGE ");
        sql.append(", FK_CONTRACT_PARTY ");
        sql.append(", FK_SHIPPER ");
        sql.append(", FK_SUPPLIER ");
        sql.append(", FK_CONSIGNEE ");
        sql.append(", FK_BL_NO ");
        sql.append(", FK_BOOKING_NO ");
        sql.append(", PORR_FLAG ");
        sql.append(", RECORD_STATUS ");
        sql.append(", RECORD_ADD_USER ");
        sql.append(", RECORD_ADD_DATE ");
        sql.append(", RECORD_CHANGE_USER ");
        sql.append(", RECORD_CHANGE_DATE ");
        sql.append("FROM EMS_MULTI_DEPOT_HDR ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");        
        if (!modeOfReturn.equalsIgnoreCase("")){
            strWhere.append(" and MPORR = '"+modeOfReturn+"' ");
        }
        if (modeOfReturn.equalsIgnoreCase("SV")){
            if (service != null && service.length() > 0) {
                strWhere.append(" and FK_SERVICE = '"+service+"' ");
            }
        }else if(modeOfReturn.equalsIgnoreCase("VV")){
            if (service != null && service.length() > 0) {
                strWhere.append(" and FK_SERVICE = '"+service+"' ");
            }
            if (service != null && vessel.length() > 0) {
                strWhere.append(" and VESSEL = '"+vessel+"' ");
            }
            if (service != null && voyage.length() > 0) {
                strWhere.append(" and VOYAGE = '"+voyage+"' ");
            }
        }else if(modeOfReturn.equalsIgnoreCase("SP")){
            if (service != null && shipper.length() > 0) {
                strWhere.append(" and FK_SHIPPER = '"+shipper+"' ");        
            }
        }else if(modeOfReturn.equalsIgnoreCase("SU")){
            if (service != null && supplier.length() > 0) {
                strWhere.append(" and FK_SUPPLIER = '"+supplier+"' ");     
            }
        }else if(modeOfReturn.equalsIgnoreCase("CN")){
            if (service != null && consignee.length() > 0) {
                strWhere.append(" and FK_CONSIGNEE = '"+consignee+"' ");                
            }
        }else if(modeOfReturn.equalsIgnoreCase("CP")){
            if (service != null && contractParty.length() > 0) {
                strWhere.append(" and FK_CONTRACT_PARTY = '"+contractParty+"' ");
            }
        }else if(modeOfReturn.equalsIgnoreCase("BK")){
            if (service != null && bkg.length() > 0) {
                strWhere.append(" and FK_BOOKING_NO = '"+bkg+"' ");
            }
        }else if(modeOfReturn.equalsIgnoreCase("BL")){
            if (service != null && bl.length() > 0) {
                strWhere.append(" and FK_BL_NO = '"+bl+"' ");
            }
        }
        
        if (!RutString.isEmptyString(depotCode)) {
            strWhere.append(" and FK_DEPOT_CODE = '"+depotCode+"' ");
        }        
        if (!RutString.isEmptyString(pointCode)) {
            strWhere.append(" and FK_POINT_CODE = '"+pointCode+"' ");
        }
        if (!RutString.isEmptyString(porrFlag)) {
            strWhere.append(" and PORR_FLAG = '"+porrFlag+"' ");
        }
         
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
         
        // Find a number of all data
        sql = this.getNumberOfAllData(sql);
        
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][countListDepotForStatementByHeader]: SQL ="+sql.toString());
        System.out.println("[EmsPickUpReturnDepotAssignmentJdbcDao][countListDepotForStatementByHeader]: Finished");
        Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(), new HashMap(), Integer.class);
        return result.intValue();
    }
    
    private String createSqlCriteriaMappingSortBy (String sortBy, String sortByIn) {
        System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][createSqlCriteriaMappingSortBy]: Started");
        String sqlCriteria = "";
        String sqlSortByIn = "";
        if (!RutString.isEmptyString(sortBy)) {
            if ("A".equals(sortByIn)) {
                sqlSortByIn = "ASC";
            } else if ("D".equals(sortByIn)) {
                sqlSortByIn = "DESC";
            }
            
            Map map = new HashMap();
            map.put("SV", "FK_SERVICE");
            map.put("VV", "VESSEL,VOYAGE");
            map.put("SP", "FK_SHIPPER");
            map.put("SU", "FK_SUPPLIER");
            map.put("CN", "FK_CONSIGNEE");            
            map.put("CP", "FK_CONTRACT_PARTY");
            map.put("BL", "FK_BL_NO");
            map.put("BK", "FK_BOOKING_NO");
            sqlCriteria = " order by "+ map.get(sortBy)+" "+sqlSortByIn;
        }
        System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][createSqlCriteriaSortBy]: sql = "+sqlCriteria);
        System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][createSqlCriteriaSortBy]: Finished");
        return sqlCriteria;
    }
    
    public boolean updateHdr(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());        
        return updateHdrStoreProcedure.updateMultiDepotHeader(mod);
    }
    
    public boolean deleteHdr(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId()); 
    //        mod.setRecordChangeUser("DEV_TEAM");
        return deleteHdrStoreProcedure.deleteMultiDepotHeader(mod);
    }
    public boolean deleteDtlByHdr(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId()); 
    //        mod.setRecordChangeUser("DEV_TEAM");
        return deleteDtlByHdrStoreProcedure.deleteMultiDepotDtlByHeader(mod);
    }

    public boolean insertHdr(RrcStandardMod mod) throws DataAccessException{      
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
    //        mod.setRecordAddUser("DEV_TEAM");
    //        mod.setRecordChangeUser("DEV_TEAM");
        return insertHdrStoreProcedure.insertMultiDepotHeader(mod);
    }
    
    protected class UpdateHdrStoreProcedure extends StoredProcedure{
     private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_HDR.PRR_UPD_MULTI_DEPOT_HDR";
     
     protected  UpdateHdrStoreProcedure(JdbcTemplate jdbcTemplate){
         super(jdbcTemplate, STORED_PROCEDURE_NAME);
         declareParameter(new SqlInOutParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
         declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_depot_code", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_point_code", Types.VARCHAR));                                          
         declareParameter(new SqlInOutParameter("p_mporr", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_vessel", Types.VARCHAR));    
         declareParameter(new SqlInOutParameter("p_voyage", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_contract_party", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_supplier", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_shipper", Types.VARCHAR));         
         declareParameter(new SqlInOutParameter("p_consignee", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_bl_no", Types.VARCHAR));
         declareParameter(new SqlInOutParameter("p_booking_no", Types.VARCHAR));    
         declareParameter(new SqlInOutParameter("p_porr_flag", Types.VARCHAR));             
         declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));                                      
         declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR)); 
         declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP)); 
         compile();
     }
     
     protected boolean updateMultiDepotHeader(RrcStandardMod mod) {
         return update(mod,mod);
     }
     
     protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
         boolean isSuccess = false;
         if((inputMod instanceof EmsPickUpReturnDepotMaintenanceMod)&&(outputMod instanceof EmsPickUpReturnDepotMaintenanceMod)){
             EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
             EmsPickUpReturnDepotMaintenanceMod aOutputMod = (EmsPickUpReturnDepotMaintenanceMod)outputMod;
             Map inParameters = new HashMap(18);
             inParameters.put("p_ems_multi_depot_hdr_id", aInputMod.getMultiDepotHdrId());
             inParameters.put("p_fsc", aInputMod.getFsc());
             inParameters.put("p_depot_code", aInputMod.getDepotCode());
             inParameters.put("p_point_code", aInputMod.getPointCode());   
             inParameters.put("p_mporr",aInputMod.getMporr());
             inParameters.put("p_service",aInputMod.getService());
             inParameters.put("p_vessel",aInputMod.getVessel());            
             inParameters.put("p_voyage",aInputMod.getVoyage());                
             inParameters.put("p_contract_party", aInputMod.getContractParty());   
             inParameters.put("p_supplier", aInputMod.getSupplier());   
             inParameters.put("p_shipper", aInputMod.getShipper());   
             inParameters.put("p_consignee", aInputMod.getConsignee());   
             inParameters.put("p_bl_no", aInputMod.getBl());   
             inParameters.put("p_booking_no",aInputMod.getBkg());
             inParameters.put("p_porr_flag",aInputMod.getPorrFlag());                 
             inParameters.put("p_record_status",aInputMod.getStatus());                         
             inParameters.put("p_record_change_user",aInputMod.getRecordChangeUser());            
             inParameters.put("p_record_change_date",aInputMod.getRecordChangeDate());  
             execute(inParameters);            
             isSuccess = true;
         }
         return isSuccess;
     }
    }
    
    protected class InsertHdrStoreProcedure extends StoredProcedure {
         private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_HDR.PRR_INS_MULTI_DEPOT_HDR";
      
         protected InsertHdrStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
             declareParameter(new SqlInOutParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
             declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_depot_code", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_point_code", Types.VARCHAR));                                          
             declareParameter(new SqlInOutParameter("p_mporr", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_vessel", Types.VARCHAR));    
             declareParameter(new SqlInOutParameter("p_voyage", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_contract_party", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_supplier", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_shipper", Types.VARCHAR));             
             declareParameter(new SqlInOutParameter("p_consignee", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_bl_no", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_booking_no", Types.VARCHAR));    
             declareParameter(new SqlInOutParameter("p_porr_flag", Types.VARCHAR));             
             declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));                        
             declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR)); 
             declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));                 
             declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR)); 
             declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));                 
             compile();
         }

         protected boolean insertMultiDepotHeader(RrcStandardMod mod) {
             return insert(mod,mod);
         }
      
         protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
             boolean isSuccess = false;
              if((inputMod instanceof EmsPickUpReturnDepotMaintenanceMod)&&(outputMod instanceof EmsPickUpReturnDepotMaintenanceMod)){
                 EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
                 EmsPickUpReturnDepotMaintenanceMod aOutputMod = (EmsPickUpReturnDepotMaintenanceMod)outputMod;
                 Map inParameters = new HashMap(20);
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_ems_multi_depot_hdr_id:"+aInputMod.getMultiDepotHdrId());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_fsc:"+aInputMod.getFsc());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_depot_code:"+aInputMod.getDepotCode());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_point_code:"+aInputMod.getPointCode());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_mporr:"+aInputMod.getMporr());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_service:"+aInputMod.getService());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_vessel:"+aInputMod.getVessel());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_voyage:"+aInputMod.getVoyage());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_contract_party:"+aInputMod.getContractParty());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_shipper:"+aInputMod.getShipper());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_supplier:"+aInputMod.getSupplier());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_consignee:"+aInputMod.getConsignee());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_bl_no:"+aInputMod.getBl());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_booking_no:"+aInputMod.getBkg());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_porr_flag:"+aInputMod.getPorrFlag());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordChangeUser());                
                 System.out.println("[EmsPickUpReturnDepotMaintenanceDetailJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordChangeDate());                
                                         
                 inParameters.put("p_ems_multi_depot_hdr_id", (aInputMod.getMultiDepotHdrId().equals(""))?null:new Integer(aInputMod.getMultiDepotHdrId()));
                 inParameters.put("p_fsc", aInputMod.getFsc());
                 inParameters.put("p_depot_code", aInputMod.getDepotCode());
                 inParameters.put("p_point_code", aInputMod.getPointCode());   
                 inParameters.put("p_mporr",aInputMod.getMporr());
                 inParameters.put("p_service",aInputMod.getService());
                 inParameters.put("p_vessel",aInputMod.getVessel());            
                 inParameters.put("p_voyage",aInputMod.getVoyage());                
                 inParameters.put("p_contract_party", aInputMod.getContractParty());                       
                 inParameters.put("p_supplier", aInputMod.getSupplier());
                 inParameters.put("p_shipper", aInputMod.getShipper());
                 inParameters.put("p_consignee", aInputMod.getConsignee());   
                 inParameters.put("p_bl_no", aInputMod.getBl());   
                 inParameters.put("p_booking_no",aInputMod.getBkg());
                 inParameters.put("p_porr_flag",aInputMod.getPorrFlag());                 
                 inParameters.put("p_record_status",aInputMod.getRecordStatus());            
                 inParameters.put("p_record_add_user",aInputMod.getRecordAddUser());            
                 inParameters.put("p_record_add_date",aInputMod.getRecordAddDate());  
                 inParameters.put("p_record_change_user",aInputMod.getRecordChangeUser());            
                 inParameters.put("p_record_change_date",aInputMod.getRecordChangeDate());  
                 Map outParameters = execute(inParameters);
    
                 if (outParameters.size() > 0) {
                         aOutputMod.setMultiDepotHdrId(((Integer)outParameters.get("p_ems_multi_depot_hdr_id")).toString());                        
                     isSuccess = true;                                            
                 }         
              }
             return isSuccess;
         }
     }
    
    protected class DeleteHdrStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_HDR.PRR_DEL_MULTI_DEPOT_HDR";
         
            protected DeleteHdrStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                declareParameter(new SqlParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
                declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
                compile();
            }
         
            protected boolean deleteMultiDepotHeader(RrcStandardMod mod) {
                return delete(mod);
            }
            protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof EmsPickUpReturnDepotMaintenanceMod){
                EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
                Map inParameters = new HashMap(3);
    ////
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_pk_multi_depot_dtl_id:"+aInputMod.getMultiDepotHdrId());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////
                    inParameters.put("p_ems_multi_depot_hdr_id", new Integer((RutString.nullToStr(aInputMod.getMultiDepotHdrId()).equals("")?"0":RutString.nullToStr(aInputMod.getMultiDepotHdrId()))));
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                    execute(inParameters);
                    isSuccess = true;                
            }
            return isSuccess;
        }
    }
    
    protected class DeleteDtlByHdrStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_EMS_MULTI_DEPOT_DTL.PRR_DEL_MULTI_DEPOT_DTL_BY_HDR";
     
        protected DeleteDtlByHdrStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_ems_multi_depot_hdr_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
     
        protected boolean deleteMultiDepotDtlByHeader(RrcStandardMod mod) {
            return delete(mod);
        }
        protected boolean delete(final RrcStandardMod inputMod) {
        boolean isSuccess = false;
        if(inputMod instanceof EmsPickUpReturnDepotMaintenanceMod){
            EmsPickUpReturnDepotMaintenanceMod aInputMod = (EmsPickUpReturnDepotMaintenanceMod)inputMod;
            Map inParameters = new HashMap(3);
    ////
                System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_pk_multi_depot_dtl_id:"+aInputMod.getMultiDepotHdrId());
                System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[EmsPickUpReturnDepotMaintenanceJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////
                inParameters.put("p_ems_multi_depot_hdr_id", new Integer((RutString.nullToStr(aInputMod.getMultiDepotHdrId()).equals("")?"0":RutString.nullToStr(aInputMod.getMultiDepotHdrId()))));
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;                
        }
        return isSuccess;
    }
    }
}
