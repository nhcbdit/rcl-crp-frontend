package com.rclgroup.dolphin.web.dao.dim;
import org.springframework.dao.DataAccessException;

public interface DimBLStatusReportDao {
  
    /**
     * check valid of B/L#
     * @param blBy
     * @param ser
     * @param ves
     * @param voy
     * @param dir
     * @param pol
     * @param pod
     * @param etatedFrom
     * @param etatedTo
     * @param blNo
     * @param instructFrom
     * @param instructTo
     * @param invNoFrom
     * @param invNoTo
     * @param collectFsc
     * @return String sessionID
     * @throws DataAccessException
     */
    public String[] getSessionId( String blBy
                                                      , String ser
                                                      , String ves
                                                      , String voy
                                                      , String dir
                                                      , String pol
                                                      , String pod
                                                      , String etatedFrom
                                                      , String etatedTo
                                                      , String blNo
                                                      , String instructFrom
                                                      , String instructTo
                                                      , String invNoFrom
                                                      , String invNoTo
                                                      , String collectFsc
                                                      , String sessionID
                                                      , String cocsoc
                                                      ) throws DataAccessException;
       
}
