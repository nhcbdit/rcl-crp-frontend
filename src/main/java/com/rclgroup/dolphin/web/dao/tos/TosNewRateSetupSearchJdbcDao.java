/*-----------------------------------------------------------------------------------------------------------  
TosNewRateSetupSearchJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Dhruv Parekh 26/04/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY       -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.model.bsa.BsaVsaServiceVariantVolumePortPairMod;
import com.rclgroup.dolphin.web.model.tos.TosNewRateSetupSearchMod;

import com.rclgroup.dolphin.web.model.tos.TosOperationTypeMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class TosNewRateSetupSearchJdbcDao extends RrcStandardDao implements TosNewRateSetupSearchDao{
    private List lst = new ArrayList();
    public int intCurrPage = 0;
    private GetRateHeaderListProcedure getRateHeaderListProcedure;
    
    public void initDao() throws Exception {
        super.initDao();
        getRateHeaderListProcedure = new GetRateHeaderListProcedure(getJdbcTemplate(), new TosRateListMapper());
    }
    
    public TosNewRateSetupSearchJdbcDao(){
        
    }
    
    public List getRateSearchList(String port,String terminal,String operation,String reference,String decs,String status, String sortBy, String sortIn) throws DataAccessException{        
//        StringBuffer sb = new StringBuffer();
//    
//        sb.append("SELECT TRH.LINE,  ");
//        sb.append("TRH.PORT,  ");
//        sb.append("TRH.TERMINAL,  ");
//        sb.append("(SELECT DESCR FROM TOS_OPR_CODE_MASTER WHERE OPR_CODE = TRH.OPERATION_TYPE) OPERATION_TYPE,  ");
//        sb.append("TRH.OPERATION_TYPE OPERATION_TYPE_CODE, ");
//        sb.append("TRH.TOS_RATE_REF,  ");
//        sb.append("TRH.DESCR,  ");
//        sb.append("TO_CHAR(TRH.EFFECTIVE_DATE,'YYYYMMDD') EFFECTIVE_DATE, ");
//        sb.append("TO_CHAR(TRH.EXPIRY_DATE,'YYYYMMDD') EXPIRY_DATE, ");
//        sb.append("(CASE TRH.RATE_STATUS ");
//        sb.append("   WHEN 'F' THEN 'Final' ");
//        sb.append("   WHEN 'O' THEN 'Open' ");
//        sb.append("   WHEN 'I' THEN 'Final-Interim' ");
//        sb.append("   WHEN 'S' THEN 'Final-Superseding' ");
//        sb.append(" END) RATE_STATUS, ");
//        sb.append("TRH.RATE_STATUS RATE_STS_CODE, ");
//        sb.append("TRH.CURRENCY,  ");
//        sb.append("TRH.TOS_RATE_SEQNO  ");
//        sb.append("FROM SEALINER.TOS_RATE_HEADER TRH ");
//        sb.append("WHERE ('"+port+"' IS NULL OR TRH.PORT = '"+port+"' ) ");
//        sb.append("  AND ('"+terminal+"' IS NULL OR TRH.TERMINAL = '"+terminal+"') ");
//        sb.append("  AND ('"+operation+"' IS NULL OR TRH.OPERATION_TYPE = '"+operation+"') ");
//        sb.append("  AND ('"+reference+"' IS NULL OR TRH.TOS_RATE_REF LIKE '"+reference+"%') ");
//        sb.append("  AND ('"+decs+"' IS NULL OR TRH.DESCR LIKE '"+decs+"%') ");
//        sb.append("  AND ('"+status+"' IS NULL OR TRH.RATE_STATUS = '"+status+"' ) ");
//        
//        
//        if(!sortBy.equals("")){        
//            sb.append(" ORDER BY "+sortBy+"   "+sortIn);
//        }
//        
//        System.out.println("[TosNewRateSetupSearchJdbcDao][getRateSearchList] SQL:"+sb.toString());
        
//        return getNamedParameterJdbcTemplate().query(
//                sb.toString(),
//                new HashMap(),
//                new RowMapper(){
//                    public Object mapRow(ResultSet rs, int row) throws SQLException {
//                        TosNewRateSetupSearchMod bean = new TosNewRateSetupSearchMod();
//                        bean.setPort(RutString.nullToStr(rs.getString("PORT")));
//                        bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
//                        bean.setOperationType(RutString.nullToStr(rs.getString("OPERATION_TYPE")));
//                        bean.setRateReference(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
//                        bean.setDesc(RutString.nullToStr(rs.getString("DESCR")));
//                        bean.setEffDate(RutString.nullToStr(rs.getString("EFFECTIVE_DATE")));
//                        bean.setExpDate(RutString.nullToStr(rs.getString("EXPIRY_DATE")));
//                        bean.setRateStatus(RutString.nullToStr(rs.getString("RATE_STATUS")));
//                        bean.setCurrency(RutString.nullToStr(rs.getString("CURRENCY")));
//                        bean.setRateSeqNo(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));
//                        bean.setOprCode(RutString.nullToStr(rs.getString("OPERATION_TYPE_CODE")));
//                        bean.setStsCode(RutString.nullToStr(rs.getString("RATE_STS_CODE")));
//                        return bean;
//                    }
//                });
        Map map = new HashMap();
        map.put("p_i_v_port",port);
        map.put("p_i_v_terminal",terminal);
        map.put("p_i_v_operation",operation);
        map.put("p_i_v_reference",reference);
        map.put("p_i_v_decs",decs);
        map.put("p_i_v_status",status);
        map.put("p_i_v_sortBy",sortBy);
        map.put("p_i_v_sortIn",sortIn);
        
        List resultList = getRateHeaderListProcedure.getRateList(map);     
        
        return resultList;
    }
    
    public List getOperationTypeList()throws DataAccessException{        
        String sqlStmt = "select opr_code, descr from TOS_OPR_CODE_MASTER WHERE RECORD_STATUS = 'A' ORDER BY OPR_CODE";
        
        System.out.println("[TosNewRateSetupSearchJdbcDao] [getOperationTypeList] :" + sqlStmt);
        
        return getNamedParameterJdbcTemplate().query(
            sqlStmt,
            new HashMap(),
            new RowMapper(){
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    TosOperationTypeMod bean = new TosOperationTypeMod();                    
                    bean.setOprCode(RutString.nullToStr(rs.getString("OPR_CODE")));
                    bean.setOprDescr(RutString.nullToStr(rs.getString("DESCR")));
                    
                    return bean;
                }
            }
        );
        
        //return resultList;
    }
    
    protected class GetRateHeaderListProcedure extends StoredProcedure{
        private static final String SQL_TOS_RATE_HDR = "PCR_TOS_RATE_SETUP.PRR_GET_RATE_LIST";
        
        protected GetRateHeaderListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_RATE_HDR);
                            
            declareParameter(new SqlOutParameter("p_o_v_rate_list", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_port",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_terminal",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_operation",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_reference",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_decs",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_status",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_sortBy",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("p_i_v_sortIn",OracleTypes.VARCHAR,rowMapper));
            
            compile();
        }
        
        protected List getRateList(Map mapParams){
            Map outMap = new HashMap();
            
            System.out.println("p_i_v_port: " + (String)mapParams.get("p_i_v_port"));
            System.out.println("p_i_v_terminal: "+ (String)mapParams.get("p_i_v_terminal"));
            System.out.println("p_i_v_operation: "+ (String)mapParams.get("p_i_v_operation"));
            System.out.println("p_i_v_reference: "+ (String)mapParams.get("p_i_v_reference"));
            System.out.println("p_i_v_decs: "+ (String)mapParams.get("p_i_v_decs"));
            System.out.println("p_i_v_status: "+ (String)mapParams.get("p_i_v_status"));
            System.out.println("p_i_v_sortBy: "+ (String)mapParams.get("p_i_v_sortBy"));
            System.out.println("p_i_v_sortIn: "+ (String)mapParams.get("p_i_v_sortIn"));
            
            List<TosNewRateSetupSearchMod> returnList = new ArrayList<TosNewRateSetupSearchMod>();
            
            try{
                outMap = execute(mapParams);
                returnList = (List<TosNewRateSetupSearchMod>) outMap.get("p_o_v_rate_list");
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
        }
    }
    
    private class TosRateListMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            TosNewRateSetupSearchMod bean = new TosNewRateSetupSearchMod();
            bean.setPort(RutString.nullToStr(rs.getString("PORT")));
            bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
            bean.setOperationType(RutString.nullToStr(rs.getString("OPERATION_TYPE")));
            bean.setRateReference(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
            bean.setDesc(RutString.nullToStr(rs.getString("DESCR")));
            bean.setEffDate(RutString.nullToStr(rs.getString("EFFECTIVE_DATE")));
            bean.setExpDate(RutString.nullToStr(rs.getString("EXPIRY_DATE")));
            bean.setRateStatus(RutString.nullToStr(rs.getString("RATE_STATUS")));
            bean.setCurrency(RutString.nullToStr(rs.getString("CURRENCY")));
            bean.setRateSeqNo(RutString.nullToStr(rs.getString("TOS_RATE_SEQNO")));
            bean.setOprCode(RutString.nullToStr(rs.getString("OPERATION_TYPE_CODE")));
            bean.setStsCode(RutString.nullToStr(rs.getString("RATE_STS_CODE")));
            return bean;
        }
    }
}
