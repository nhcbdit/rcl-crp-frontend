/*-----------------------------------------------------------------------------------------------------------  
FarFunlocMappingJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 01/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaModelMod;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaServiceVariantVolumePortPairMod;
import com.rclgroup.dolphin.web.model.far.FarFunlocMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.sql.Types;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class FarFunlocMappingJdbcDao extends RrcStandardDao implements FarFunlocMappingDao{
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    
    protected void initDao() throws Exception {
        super.initDao();        
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());        
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());    
    }                                         
    public List listForStatementByHeader(String bl_no,String billToParty,String inv_no,String funloc,String status,String sortBy, String sortByIn) throws DataAccessException {
        return this.listForStatement(0, 0, bl_no, billToParty, inv_no, funloc, status,sortBy ,sortByIn);
    }
    
    public List listForStatement(int rowAt, int rowTo, String bl_no,String billToParty,String inv_no,String funloc,String status,String sortBy, String sortByIn) {
        System.out.println("[FarFunlocMappingJdbcDao][listForStatementByHeader]: Started");
        
        String sqlColumn = "BILL_TO_PARTY, BL_NO, INVOICE_NO_LIKE, FUN_LOC, STATUS,ADD_USER,ADD_DATE,CHANGE_USEr,CHANGE_DATE";
//        String sqlColumn = "BILL_TO_PARTY, BL_NO, INVOICE_NO_LIKE, FUN_LOC, MARKS_DESC, STATUS";
        StringBuffer sql = new StringBuffer();
//        sql.append("SELECT BILL_TO_PARTY ,BL_NO ,INVOICE_NO_LIKE ,FUN_LOC,STATUS ,");
//        sql.append(" (SELECT TM.MARKS_DESC FROM VR_FAR_FUNLOC_MAPPING TM WHERE TM.BL_NO = TM.BL_NO AND ROWNUM = 1 ) AS MARKS_DESC ");
//        sql.append(" FROM ( ");
        sql.append("select PK_DATA_MAP_ID");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,BL_NO ");
        sql.append("    ,INVOICE_NO_LIKE ");
        sql.append("    ,FUN_LOC ");
        sql.append("    ,MARKS_DESC ");
        sql.append("    ,STATUS ");        
        sql.append("    ,ADD_USER ");        
        sql.append("    ,ADD_DATE ");        
        sql.append("    ,CHANGE_USER ");        
        sql.append("    ,CHANGE_DATE ");        
        sql.append("from VR_FAR_FUNLOC_MAPPING FM ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(bl_no)) {
            strWhere.append(" and BL_NO = '"+bl_no+"' ");
        }
        if (!RutString.isEmptyString(billToParty)) {
            strWhere.append(" and BILL_TO_PARTY = '"+billToParty+"' ");
        }
        if (!RutString.isEmptyString(inv_no)) {
            strWhere.append(" and INVOICE_NO_LIKE like '"+inv_no+"%' ");
        }
        if (!RutString.isEmptyString(funloc)) {
            strWhere.append(" and FUN_LOC like '"+funloc+"%' ");
        }        
        if(!RutString.isEmptyString(status) && !"A".equals(status)) {
            strWhere.append(" and STATUS = '"+status +"'");
        }
        
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
//        sql.append(" ) ");
        // Query by record number range
        sql.append(createSqlCriteriaSortBy(sortBy, sortByIn));
        sql = this.addSqlForNewHelp(sql, rowAt, rowTo, sqlColumn);
        
        System.out.println("[FarFunlocMappingJdbcDao][listForStatementByHeader]: SQL ="+sql.toString());
        System.out.println("[FarFunlocMappingJdbcDao][listForStatementByHeader]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           FarFunlocMod ff = new FarFunlocMod();
                           ff.setDataMapId(RutString.nullToStr(rs.getString("PK_DATA_MAP_ID")));
                           ff.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                           ff.setBlNo(RutString.nullToStr(rs.getString("BL_NO")));
                           ff.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO_LIKE")));
                           ff.setFunloc(RutString.nullToStr(rs.getString("FUN_LOC")));
                           ff.setMarkDesc(RutString.nullToStr(rs.getClob("MARKS_DESC")));                           
                           ff.setStatus(RutString.nullToStr(rs.getString("STATUS")));                           
                           ff.setAddUser(RutString.nullToStr(rs.getString("ADD_USER")));
                           ff.setAddDate(rs.getTimestamp("ADD_DATE")); 
                           ff.setChangeUser(RutString.nullToStr(rs.getString("CHANGE_USER")));
                           ff.setChangeDate(rs.getTimestamp("CHANGE_DATE")); 
                           return ff;
                    }
                });
    }
    
    public int countListForStatement(String bl_no,String billToParty,String inv_no,String funloc,String status) {
        System.out.println("[FarFunlocMappingJdbcDao][countListForStatementByHeader]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("select ");
        sql.append("    BILL_TO_PARTY ");
        sql.append("    ,BL_NO ");
        sql.append("    ,INVOICE_NO_LIKE ");
        sql.append("    ,FUN_LOC ");
//        sql.append("    ,MARKS_DESC ");
        sql.append("    ,STATUS ");        
        sql.append("from VR_FAR_FUNLOC_MAPPING ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
            if (!RutString.isEmptyString(bl_no)) {
                strWhere.append(" and BL_NO = '"+bl_no+"' ");
            }
        if (!RutString.isEmptyString(billToParty)) {
            strWhere.append(" and BILL_TO_PARTY = '"+billToParty+"' ");
        }
            if (!RutString.isEmptyString(inv_no)) {
                strWhere.append(" and INVOICE_NO_LIKE like '"+inv_no+"%' ");
            }
            if (!RutString.isEmptyString(funloc)) {
                strWhere.append(" and FUN_LOC like '"+funloc+"%' ");
            }        
            if(!RutString.isEmptyString(status) && !"A".equals(status)) {
                strWhere.append(" and STATUS = '"+status +"'");
            }
         
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
         
        // Find a number of all data
        sql = this.getNumberOfAllData(sql);
        
        System.out.println("[FarFunlocMappingJdbcDao][countListForStatementByHeader]: SQL ="+sql.toString());
        System.out.println("[FarFunlocMappingJdbcDao][countListForStatementByHeader]: Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(), new HashMap(), Integer.class);
    }
    
  
    private String createSqlCriteriaSortBy (String sortBy, String sortByIn) {
        System.out.println("[FarFunlocMappingJdbcDao][createSqlCriteriaSortBy]: Started");
        String sqlCriteria = "";
        String sqlSortByIn = "";
        if (!RutString.isEmptyString(sortBy)) {
            if ("A".equals(sortByIn)) {
                sqlSortByIn = "ASC";
            } else if ("D".equals(sortByIn)) {
                sqlSortByIn = "DESC";
            }
            
            Map map = new HashMap();
            map.put("BL_NO", "BL_NO");
            map.put("BILL_TO_PARTY", "BILL_TO_PARTY");
            map.put("INVOICE_NO_LIKE", "INVOICE_NO_LIKE");
            map.put("FUN_LOC", "FUN_LOC");            
            
            sqlCriteria = " order by "+ map.get(sortBy)+" "+sqlSortByIn;
        }
        System.out.println("[FarFunlocMappingJdbcDao][createSqlCriteriaSortBy]: sql = "+sqlCriteria);
        System.out.println("[FarFunlocMappingJdbcDao][createSqlCriteriaSortBy]: Finished");
        return sqlCriteria;
    }
    
    public boolean updateFunLoc(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());        
        return updateStoreProcedure.updateFunLoc(mod);
    }
    
    public boolean deleteFunLoc(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId()); 
        return deleteStoreProcedure.deleteFunLoc(mod);
    }

    public boolean insertFunLoc(RrcStandardMod mod) throws DataAccessException{      
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insertFunLoc(mod);
    }
    
//    public int insertFunLoc(String billToParty, String inv_no,String funloc,String userId,int numIns) throws DataAccessException{        
//        return insertStoreProcedure.insertFunLoc(billToParty,inv_no,funloc,userId,numIns);
//    }
    protected class UpdateStoreProcedure extends StoredProcedure{
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_SB_DATA_MAP.PRR_UPD_SB_DATA_MAP";
        
        protected  UpdateStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_data_map_id",Types.NUMERIC));
            declareParameter(new SqlParameter("p_bill_to_party",Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl",Types.VARCHAR));
            declareParameter(new SqlParameter("p_invoice_no_like",Types.VARCHAR));
            declareParameter(new SqlParameter("p_fun_loc",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_status",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_user",Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date",Types.TIMESTAMP));
            compile();
        }
        
        protected boolean updateFunLoc(RrcStandardMod mod) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof FarFunlocMod)&&(outputMod instanceof FarFunlocMod)){
                FarFunlocMod aInputMod = (FarFunlocMod)inputMod;
                FarFunlocMod aOutputMod = (FarFunlocMod)outputMod;
                Map inParameters = new HashMap(8);
                inParameters.put("p_data_map_id",aInputMod.getDataMapId());
                inParameters.put("p_bill_to_party",aInputMod.getBillToParty());
                inParameters.put("p_bl",aInputMod.getBlNo());            
                inParameters.put("p_invoice_no_like",aInputMod.getInvNo());
                inParameters.put("p_fun_loc",aInputMod.getFunloc());
                inParameters.put("p_record_status",aInputMod.getStatus());            
                inParameters.put("p_record_change_user",aInputMod.getRecordAddUser());            
                inParameters.put("p_record_change_date",aInputMod.getChangeDate());  
                execute(inParameters);            
                isSuccess = true;
            }
            return isSuccess;
        }
    }
     
    protected class InsertStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_FAR_SB_DATA_MAP.PRR_INS_SB_DATA_MAP";
         
            protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);                 
                declareParameter(new SqlInOutParameter("p_bill_to_party", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_invoice_no_like", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_fun_loc", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));                                          
                declareParameter(new SqlInOutParameter("p_num_ins", Types.INTEGER));                                          
                compile();
            }

            protected boolean insertFunLoc(RrcStandardMod mod) {
                return insert(mod,mod);
            }
         
            protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
                boolean isSuccess = false;                                                    
                 if((inputMod instanceof FarFunlocMod)&&(outputMod instanceof FarFunlocMod)){
                     FarFunlocMod aInputMod = (FarFunlocMod)inputMod;
                     FarFunlocMod aOutputMod = (FarFunlocMod)outputMod;
                     Map inParameters = new HashMap(5);
                    System.out.println("[FarFunLocMappingJdbcDao][InsertStoreProcedure][insert]:p_bill_to_party:"+aInputMod.getBillToParty());
                    System.out.println("[FarFunLocMappingJdbcDao][InsertStoreProcedure][insert]:p_inv_no:"+aInputMod.getInvNo());
                    System.out.println("[FarFunLocMappingJdbcDao][InsertStoreProcedure][insert]:p_fun_loc:"+aInputMod.getFunloc());
                    System.out.println("[FarFunLocMappingJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());                
                                            
                    inParameters.put("p_bill_to_party", (aInputMod.getBillToParty().equals(""))?null:aInputMod.getBillToParty());
                    inParameters.put("p_invoice_no_like", (aInputMod.getInvNo().equals(""))?null:aInputMod.getInvNo());
                    inParameters.put("p_fun_loc", (aInputMod.getFunloc().equals(""))?null:aInputMod.getFunloc());
                    inParameters.put("p_record_add_user", (aInputMod.getRecordAddUser().equals(""))?null:aInputMod.getRecordAddUser());                
                    inParameters.put("p_num_ins", new Integer(aInputMod.getNumIns()));
                    Map outParameters = execute(inParameters);
    
                    if (outParameters.size() > 0) {
                        aInputMod.setNumIns(((Integer)outParameters.get("p_num_ins")).intValue());                    
                        isSuccess = true;                                            
                    }         
                 }
                return isSuccess;
            }
        }
     
    protected class DeleteStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_FAR_SB_DATA_MAP.PRR_DEL_SB_DATA_MAP";
         
            protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                declareParameter(new SqlParameter("p_data_map_id", Types.INTEGER));
                declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
                compile();
            }
         
            protected boolean deleteFunLoc(RrcStandardMod mod) {
                return delete(mod);
            }
            protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof FarFunlocMod){
                FarFunlocMod aInputMod = (FarFunlocMod)inputMod;
                Map inParameters = new HashMap(3);
    ////
                    System.out.println("[FarFunLocMappingJdbcDao][DeleteStoreProcedure][delete]:\"p_data_map_id:"+aInputMod.getDataMapId());
                    System.out.println("[FarFunLocMappingJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordAddUser());
                    System.out.println("[FarFunLocMappingJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getChangeDate());
    ////
                    inParameters.put("p_data_map_id", new Integer((RutString.nullToStr(aInputMod.getDataMapId()).equals("")?"0":RutString.nullToStr(aInputMod.getDataMapId()))));
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getChangeDate());
                    execute(inParameters);
                    isSuccess = true;                
                
            }
            return isSuccess;
        }
    }
}
