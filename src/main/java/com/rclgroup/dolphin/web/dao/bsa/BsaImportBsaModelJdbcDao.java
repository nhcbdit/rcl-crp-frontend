/*-----------------------------------------------------------------------------------------------------------  
EzlExcelUploadOnlineJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Nipun Sutes 28/11/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.bsa.BsaImportBsaModelMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class BsaImportBsaModelJdbcDao extends RrcStandardDao implements BsaImportBsaModelDao {
    private UpdateLoadListStoreProcedure updateLoadListStoreProcedure;
       
    public BsaImportBsaModelJdbcDao() {
        super();
    }
   
    protected void initDao() throws Exception {
        super.initDao();
        updateLoadListStoreProcedure = new UpdateLoadListStoreProcedure(getJdbcTemplate());
    }
    
    public boolean updateLoadList(RrcStandardMod mod) throws DataAccessException {
        return updateLoadListStoreProcedure.updateLoadList(mod);
    }
    
    protected class UpdateLoadListStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_EZL_LOAD_LIST.PRR_UPD_BOOKED_LOAD";
        
        protected UpdateLoadListStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_local_container", Types.VARCHAR));
            declareParameter(new SqlParameter("p_con_mlo_ves", Types.VARCHAR));
            declareParameter(new SqlParameter("p_con_mlo_voy", Types.VARCHAR));
            declareParameter(new SqlParameter("p_mlo_pod1", Types.VARCHAR));
            declareParameter(new SqlParameter("p_mlo_pod2", Types.VARCHAR));
            declareParameter(new SqlParameter("p_mlo_pod3", Types.VARCHAR));
            declareParameter(new SqlParameter("p_place_of_del", Types.VARCHAR));
            declareParameter(new SqlParameter("p_tight_con_flag1", Types.VARCHAR));
            declareParameter(new SqlParameter("p_tight_con_flag2", Types.VARCHAR));
            declareParameter(new SqlParameter("p_tight_con_flag3", Types.VARCHAR));
            
            declareParameter(new SqlParameter("p_container_no", Types.VARCHAR));
            declareParameter(new SqlParameter("p_booking_no", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_terminal", Types.VARCHAR));
            
            compile();
        }
        
        protected boolean updateLoadList(RrcStandardMod mod) {
            return updateLoadList(mod, mod);
        }
        
        protected boolean updateLoadList(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof BsaImportBsaModelMod) && (outputMod instanceof BsaImportBsaModelMod)) {
                Map inParameters = new HashMap();
                BsaImportBsaModelMod aInputMod = (BsaImportBsaModelMod) inputMod;
                //BsaImportBsaModelMod aOutputMod = (BsaImportBsaModelMod) outputMod;
                
                inParameters.put("p_local_container", aInputMod.getLocalContainer());
                inParameters.put("p_con_mlo_ves", aInputMod.getConnectingMloVessel());
                inParameters.put("p_con_mlo_voy", aInputMod.getConnectingMloVoyage());
                inParameters.put("p_mlo_pod1", aInputMod.getMloPod1());
                inParameters.put("p_mlo_pod2", aInputMod.getMloPod2());
                inParameters.put("p_mlo_pod3", aInputMod.getMloPod3());
                inParameters.put("p_place_of_del", aInputMod.getPlaceofDelivery());
                inParameters.put("p_tight_con_flag1", aInputMod.getTightConFlag1());
                inParameters.put("p_tight_con_flag2", aInputMod.getTightConFlag2());
                inParameters.put("p_tight_con_flag3", aInputMod.getTightConFlag3());
                
                inParameters.put("p_container_no", aInputMod.getContainerNo());
                inParameters.put("p_booking_no", aInputMod.getBookingNo());
                inParameters.put("p_pol_pod", aInputMod.getPolPod());
                inParameters.put("p_terminal", aInputMod.getTerminal());
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size() > 0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
}
