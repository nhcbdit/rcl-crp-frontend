/*------------------------------------------------------
QtnPortSurchargeJdbcTypeDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Manop Wanngam 15/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.qtn.QtnPortSurchargeTypeMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class QtnPortSurchargeTypeJdbcDao extends RrcStandardDao implements QtnPortSurchargeTypeDao {

    public QtnPortSurchargeTypeJdbcDao(){
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String surchargeType) throws DataAccessException {
        System.out.println("[QtnPortSurchargeTypeJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_TYPE ");
        sql.append("FROM VR_QTN_SURCHARGE_TYPE ");
        sql.append("WHERE SURCHARGE_TYPE = :surchargeType ");
        HashMap map = new HashMap();
        map.put("surchargeType",surchargeType);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[QtnPortSurchargeTypeJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValid(String surchargeType, String status) throws DataAccessException {
        System.out.println("[QtnPortSurchargeTypeJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_TYPE ");
        sql.append("FROM VR_QTN_SURCHARGE_TYPE ");
        sql.append("WHERE SURCHARGE_TYPE = :surchargeType ");
        sql.append("AND SURCHARGE_TYPE_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("surchargeType",surchargeType);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[QtnPortSurchargeTypeJdbcDao][isValid]: With status: Finished");
        return isValid;
    }    
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[QtnPortSurchargeTypeJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_TYPE ");
        sql.append("      ,SURCHARGE_DESCRIPTION "); 
        sql.append("      ,TERM_REQUIRED "); 
        sql.append("      ,SHIPMENT_REQUIRED "); 
        sql.append("      ,SIZE_REQUIRED "); 
        sql.append("      ,SURCHARGE_TYPE_STATUS "); 
        sql.append("FROM VR_QTN_SURCHARGE_TYPE ");
        sql.append(sqlCriteria);
        System.out.println("[QtnPortSurchargeTypeJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[QtnPortSurchargeTypeJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper()); 
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("T")){
                sqlCriteria = "WHERE SURCHARGE_TYPE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "WHERE SURCHARGE_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE SURCHARGE_TYPE_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[QtnPortSurchargeTypeJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SURCHARGE_TYPE ");
        sql.append("      ,SURCHARGE_DESCRIPTION "); 
        sql.append("      ,TERM_REQUIRED "); 
        sql.append("      ,SHIPMENT_REQUIRED "); 
        sql.append("      ,SIZE_REQUIRED "); 
        sql.append("      ,SURCHARGE_TYPE_STATUS "); 
        sql.append("FROM VR_QTN_SURCHARGE_TYPE ");
        sql.append("WHERE SURCHARGE_TYPE_STATUS = :status ");
        sql.append(sqlCriteria);
        HashMap map = new HashMap();
        map.put("status",status);
        System.out.println("[QtnPortSurchargeTypeJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper()); 
    }
    
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("T")){
                sqlCriteria = "AND SURCHARGE_TYPE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND SURCHARGE_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND SURCHARGE_TYPE_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }    
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private QtnPortSurchargeTypeMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        QtnPortSurchargeTypeMod surchargeType = new QtnPortSurchargeTypeMod();
        surchargeType.setSurchargeType(RutString.nullToStr(rs.getString("SURCHARGE_TYPE")));
        surchargeType.setSurchargeDescription(RutString.nullToStr(rs.getString("SURCHARGE_DESCRIPTION")));
        surchargeType.setTermRequired(RutString.nullToStr(rs.getString("TERM_REQUIRED")));
        surchargeType.setShipmentRequired(RutString.nullToStr(rs.getString("SHIPMENT_REQUIRED")));
        surchargeType.setSizeRequired(RutString.nullToStr(rs.getString("SIZE_REQUIRED")));
        surchargeType.setSurchargeTypeStatus(RutString.nullToStr(rs.getString("SURCHARGE_TYPE_STATUS")));
        return surchargeType;
    }

}


