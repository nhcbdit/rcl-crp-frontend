/*-----------------------------------------------------------------------------------------------------------  
CamCountryJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 24/04/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamCountryMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class CamCountryJdbcDao extends RrcStandardDao implements CamCountryDao {

    public CamCountryJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String countryCode) throws DataAccessException {
        System.out.println("[CamCountryJdbcDao][isValid]: Started");
        boolean isValid = false;    
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT COUNTRY_CODE ");
        sql.append("FROM VR_CAM_COUNTRY ");
        sql.append("WHERE COUNTRY_CODE = :countryCode");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("countryCode", countryCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[CamCountryJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValid(String countryCode, String status) throws DataAccessException {
        System.out.println("[CamCountryDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT COUNTRY_CODE ");
        sql.append("FROM VR_CAM_COUNTRY ");
        sql.append("WHERE COUNTRY_CODE = :countryCode ");
        sql.append("AND COUNTRY_STATUS = :status ");
        
        HashMap map = new HashMap();
        map.put("countryCode",countryCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[CamCountryJdbcDao][isValid]: Finished");
        return isValid;
        
    }    
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[CamCountryJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT COUNTRY_CODE ");
        sql.append("      ,COUNTRY_NAME ");
        sql.append("      ,COUNTRY_STATUS "); 
        sql.append("FROM VR_CAM_COUNTRY ");
        sql.append(sqlCriteria);            
        System.out.println("[CamCountryJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[CamCountryJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "WHERE COUNTRY_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "WHERE COUNTRY_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE COUNTRY_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    public List listForHelpScreenWithUserLevel(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[CamCountryJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT COUNTRY_CODE ");
        sql.append("      ,COUNTRY_NAME ");
        sql.append("      ,COUNTRY_STATUS "); 
        sql.append("FROM VR_CAM_COUNTRY ");
        sql.append("WHERE COUNTRY_STATUS = :status ");
        sql.append(sqlCriteria);            
        System.out.println("[CamCountryJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
        System.out.println("[CamCountryJdbcDao][listForHelpScreen]: With status: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   Collections.singletonMap("status", status),
                   new RowModMapper());      
    }
    
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND COUNTRY_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "AND COUNTRY_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND COUNTRY_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private CamCountryMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        CamCountryMod countryCode = new CamCountryMod();
        countryCode.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
        countryCode.setCountryName(RutString.nullToStr(rs.getString("COUNTRY_NAME")));
        countryCode.setCountryStatus(RutString.nullToStr(rs.getString("COUNTRY_STATUS")));

        return countryCode;
    }

}
