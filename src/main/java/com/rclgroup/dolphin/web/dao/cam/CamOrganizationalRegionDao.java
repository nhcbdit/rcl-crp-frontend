/*-----------------------------------------------------------------------------------------------------------  
CamOrganizationalRegionDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 24/04/08  
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 24/04/08  SPD                       Change to new framework
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;
import org.springframework.dao.DataAccessException;

public interface CamOrganizationalRegionDao {

    /**
     * check valid of (organization)region code 
     * @param regionCode
     * @param status
     * @return valid of (organization)region code
     * @throws DataAccessException
     */
    public boolean isValid(String regionCode, String status) throws DataAccessException;

    /**
     * list (organization)region records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of (organization)region
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;

    /**
     * list (organization)region records with user level for help screen
     * @param find
     * @param search
     * @param wild
     * @param lineCode
     * @param status
     * @return list of (organization)region with user level
     * @throws DataAccessException
     */
    public List listForHelpScreenWithUserLevel(String find,String search,String wild,String lineCode,String status) throws DataAccessException;

}

