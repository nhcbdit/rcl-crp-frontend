package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.model.cam.CamOperatorMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class CamOperatorJdbcDao extends RrcStandardDao implements CamOperatorDao{
    public CamOperatorJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String opCode,String status) throws DataAccessException {
        System.out.println("[CamOperatorJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
         
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT OPERATOR_CODE ");
        sql.append("      ,DESCRIPTION ");                        
        sql.append("FROM OPERATOR_CODE_MASTER ");
        sql.append("WHERE 1 = 1 "); 
        if((opCode!=null)&&(!opCode.trim().equals(""))){
            sql.append("  AND OPERATOR_CODE = :opCode ");
        }
        sql.append("  AND STATUS = :status "); 
        HashMap map = new HashMap();        
        map.put("opCode",opCode);
        map.put("status",status);
        System.out.println("[CamOperatorJdbcDao][isValid]: SQL: " + sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[CamOperatorJdbcDao][isValid]: With status: Finished");
        return isValid;
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND OPERATOR_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND DESCRIPTION " + sqlWild;            
            }        
        }
        return sqlCriteria;
    }
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT OPERATOR_CODE ");
        sql.append("      ,DESCRIPTION ");                        
        sql.append("FROM OPERATOR_CODE_MASTER ");
        sql.append("WHERE 1 = 1 ");        
        sql.append("  AND STATUS = :status ");
        sql.append(sqlCriteria);
        HashMap map = new HashMap();        
        map.put("status",status);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                CamOperatorMod op = new CamOperatorMod();
                op.setOperatorCode(RutString.nullToStr(rs.getString("OPERATOR_CODE")));
                op.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));                      
                return op;
            }
        });
    }
}
