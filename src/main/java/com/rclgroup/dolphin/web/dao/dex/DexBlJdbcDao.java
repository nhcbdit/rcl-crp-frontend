 /*
 --------------------------------------------------------
 DexBlJdbcDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2008
 --------------------------------------------------------
 Author Kitti Pongsirisakun 28/11/2008
 - Change Log--------------------------------------------
 ## DD/MM/YY -User- -TaskRef- -ShortDescription
 01 16/01/09  KIT    BUG.174   BL Lookup problems
 02 03/06/09  POR              created  list for list B/L screen
 03 06/12/11  NIP    BUG561    use new view for BL and select rownum=100 because have many data.
 --------------------------------------------------------
 */
 package com.rclgroup.dolphin.web.dao.dex;

 import com.rclgroup.dolphin.web.common.RcmConstant;
 import com.rclgroup.dolphin.web.common.RrcStandardDao;
 import com.rclgroup.dolphin.web.model.dex.DexBlMod;
 import com.rclgroup.dolphin.web.util.RutString;
  
 import java.sql.ResultSet;
 import java.sql.SQLException;

 import java.util.Collections;
 import java.util.HashMap;
 import java.util.List;

 import org.springframework.dao.DataAccessException;
 import org.springframework.dao.EmptyResultDataAccessException;
 import org.springframework.jdbc.core.RowMapper;
 import org.springframework.jdbc.support.rowset.SqlRowSet;

 public class DexBlJdbcDao extends RrcStandardDao implements DexBlDao{
     public DexBlJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     // --##BEGIN 02
     public boolean isValid(String bl) {
         System.out.println("[DimBlDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT * ");
         sql.append("FROM VR_DEX_BL ");
         sql.append("WHERE AYBLNO = :bl"); 
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("bl", bl));            
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[DimBlDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValidforHbl(String hbl) {
         System.out.println("[DimBlDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT * ");
         sql.append("FROM VR_DEX_BL ");
         sql.append("WHERE HOUSE_BL_NO = :hbl");
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("bl", hbl.toUpperCase()));            
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[DimBlDao][isValid]: Finished");
         return isValid;
     }
     
     
     // --##END 02
     
     public boolean isValidForBankGuarantee(String bl) {
         System.out.println("[DimBlDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT BL_NO ");
 //        sql.append("FROM VR_DEX_BL_FOR_BG ");
         sql.append("FROM VR_DIM_BANKGUARANTEE ");
         sql.append("WHERE BL_NO = :bl");
         
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("bl", bl.toUpperCase()));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[DimBlDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValidForDangerousCargoManifest(String bl) {
         System.out.println("[DimBlDao][isValid]: Started");
         boolean isValid = false;
         
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT BL_NO ");
     //        sql.append("FROM VR_DEX_BL_FOR_BG ");
         sql.append("FROM VR_DEX_DGMANIFEST ");
         sql.append("WHERE BL_NO = :blNo ");
         
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("blNo", bl));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[DimBlDao][isValid]: Finished");
         return isValid;
     }
     
      // --##BEGIN 02
     public List listForHelpScreenWithFsc(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException{
         System.out.println("[dexBlJdbcDao][listForHelpScreenWithFsc]: started");
         String sqlCriteria = createSqlCriteriaForHblHelpScreen(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AYBKNO ");           //BOOKING
         sql.append("      ,AYBLNO ");           //BL_NO
         sql.append("      ,HOUSE_BL_NO ");      //HOUSE_BL_NO
         sql.append("      ,AYOPCD ");           //OP_CODE
         sql.append("      ,AYSORC ");           //COC_SOC
         sql.append("      ,BL_ISSUE_DATE ");    //BL_ISSUE_DATE
         sql.append("      ,AYSTAT ");           //OUTSTATUS
         sql.append("      ,AYIMST ");           //INSTATUS
         sql.append("      ,AYSVES ");           // VESSEL
         sql.append("      ,AYSVOY ");           // VOYAGE
         sql.append("      ,AYPOLC ");           // POL
         sql.append("      ,AYPODC ");           // POD
         sql.append("FROM VR_DEX_BL ");
         
         if(sqlCriteria.trim().equals("")){
             sql.append(" WHERE ");
         }else{
             sql.append(sqlCriteria);
             sql.append(" AND ");
         }
         
         //sql.append(sqlCriteria);
         //  -- CHECK PORT
          sql.append(" ( ");
          sql.append("      NVL(AYPODC,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );
          sql.append(" OR ");         
          sql.append("      NVL(TSHIPMENTPORT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR "); 
          sql.append("      NVL(TSHIPMENTPORT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR "); 
          sql.append("      NVL(TSHIPMENTPORT3,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                     ")" );

             sql.append(" ) "); 
         //--------------------------------
         sql.append(" GROUP BY ");
         sql.append("      AYBKNO ");    
         sql.append("      ,AYBLNO ");         
         sql.append("      ,HOUSE_BL_NO ");      
         sql.append("      ,AYOPCD ");         
         sql.append("      ,AYSORC ");        
         sql.append("      ,BL_ISSUE_DATE ");  
         sql.append("      ,AYSTAT ");       
         sql.append("      ,AYIMST ");        
         sql.append("      ,AYSVES ");          
         sql.append("      ,AYSVOY ");         
         sql.append("      ,AYPOLC ");            
         sql.append("      ,AYPODC ");          
         sql.append(" ORDER BY ");
         sql.append("      AYBKNO ");    
         sql.append("      ,AYBLNO ");
         sql.append("      ,BL_ISSUE_DATE ");
         System.out.println("[DexBlJdbcDao][listForHelpScreenWithFsc]: SQL = "+sql.toString());
         System.out.println("[DexBlJdbcDao][listForHelpScreenWithFsc]: Finished");
         return getNamedParameterJdbcTemplate().query(
                             sql.toString(),
                             new HashMap(),
         new RowModMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 DexBlMod bl = new DexBlMod();
                 bl.setBooking(RutString.nullToStr(rs.getString("AYBKNO")));
                 bl.setBl(RutString.nullToStr(rs.getString("AYBLNO")));
                 bl.setHbl(RutString.nullToStr(rs.getString("HOUSE_BL_NO")));
                 bl.setOpCode(RutString.nullToStr(rs.getString("AYOPCD")));
                 bl.setCocSoc(RutString.nullToStr(rs.getString("AYSORC")));
                 bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                 bl.setOutStatus(RutString.nullToStr(rs.getString("AYSTAT")));
                 bl.setInStatus(RutString.nullToStr(rs.getString("AYIMST")));
                 bl.setVessel(RutString.nullToStr(rs.getString("AYSVES")));
                 bl.setVoyage(RutString.nullToStr(rs.getString("AYSVOY")));
                 bl.setPol(RutString.nullToStr(rs.getString("AYPOLC")));
                 bl.setPod(RutString.nullToStr(rs.getString("AYPODC")));      
                 return bl;
             }
             });
     }
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException{
         System.out.println("[dexBlJdbcDao][listForHelpScreen]: started");
         String sqlCriteria = createSqlCriteriaForHblHelpScreen(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT AYBKNO ");           //BOOKING
         sql.append("      ,AYBLNO ");           //BL_NO
         sql.append("      ,HOUSE_BL_NO ");      //HOUSE_BL_NO
         sql.append("      ,AYOPCD ");           //OP_CODE
         sql.append("      ,AYSORC ");           //COC_SOC
         sql.append("      ,BL_ISSUE_DATE ");    //BL_ISSUE_DATE
         sql.append("      ,AYSTAT ");           //OUTSTATUS
         sql.append("      ,AYIMST ");           //INSTATUS
         sql.append("      ,AYSVES ");           // VESSEL
         sql.append("      ,AYSVOY ");           // VOYAGE
         sql.append("      ,AYPOLC ");           // POL
         sql.append("      ,AYPODC ");           // POD
         sql.append("FROM VR_DEX_BL ");
         
         if(sqlCriteria.trim().equals("")){
             //sql.append("WHERE 1=1 ");
         }else{
             sql.append(sqlCriteria);
//             sql.append(" AND");
         }
         
//         sql.append(sqlCriteria);
        
         sql.append(" GROUP BY ");
         sql.append("      AYBKNO ");    
         sql.append("      ,AYBLNO ");         
         sql.append("      ,HOUSE_BL_NO ");      
         sql.append("      ,AYOPCD ");         
         sql.append("      ,AYSORC ");        
         sql.append("      ,BL_ISSUE_DATE ");  
         sql.append("      ,AYSTAT ");       
         sql.append("      ,AYIMST ");        
         sql.append("      ,AYSVES ");          
         sql.append("      ,AYSVOY ");         
         sql.append("      ,AYPOLC ");            
         sql.append("      ,AYPODC ");          
         sql.append(" ORDER BY ");
         sql.append("      AYBKNO ");    
         sql.append("      ,AYBLNO ");
         sql.append("      ,BL_ISSUE_DATE ");
         System.out.println("[DexBlJdbcDao][listForHelpScreen]: SQL = "+sql.toString());
         System.out.println("[DexBlJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                             sql.toString(),
                             new HashMap(),
         new RowModMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 DexBlMod bl = new DexBlMod();
                 bl.setBooking(RutString.nullToStr(rs.getString("AYBKNO")));
                 bl.setBl(RutString.nullToStr(rs.getString("AYBLNO")));
                 bl.setHbl(RutString.nullToStr(rs.getString("HOUSE_BL_NO")));
                 bl.setOpCode(RutString.nullToStr(rs.getString("AYOPCD")));
                 bl.setCocSoc(RutString.nullToStr(rs.getString("AYSORC")));
                 bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                 bl.setOutStatus(RutString.nullToStr(rs.getString("AYSTAT")));
                 bl.setInStatus(RutString.nullToStr(rs.getString("AYIMST")));
                 bl.setVessel(RutString.nullToStr(rs.getString("AYSVES")));
                 bl.setVoyage(RutString.nullToStr(rs.getString("AYSVOY")));
                 bl.setPol(RutString.nullToStr(rs.getString("AYPOLC")));
                 bl.setPod(RutString.nullToStr(rs.getString("AYPODC")));      
                 return bl;
             }
             });
     }
          
      private String createSqlCriteriaForHblHelpScreen(String find, 
                                                       String search, 
                                                       String wild) {
          System.out.println("[DexBlJdbcDao][createSqlCriteriaForHblHelpScreen]: Started");
          String sqlCriteria = "";
          String sqlWild = "";
          
          if (wild.equalsIgnoreCase("ON")) {
              sqlWild = "LIKE '" + find.toUpperCase() + "%' ";
          } else{
              sqlWild ="= '"+find.toUpperCase() + "' ";
          }
              if(find.trim().length() == 0){
              }else{
                  if(search.equalsIgnoreCase("B")){
                      sqlCriteria = "WHERE AYBKNO " + sqlWild;
                  }else if(search.equalsIgnoreCase("BL")){
                      sqlCriteria = "WHERE AYBLNO " + sqlWild;
                  }else if(search.equalsIgnoreCase("HBL")){
                      sqlCriteria = "WHERE HOUSE_BL_NO " + sqlWild; 
                  }else if(search.equalsIgnoreCase("OC")){
                      sqlCriteria = "WHERE AYOPCD " + sqlWild;
                  }else if(search.equalsIgnoreCase("CS")){
                      sqlCriteria = "WHERE UPPER(AYSORC) " + sqlWild;
                  }else if(search.equalsIgnoreCase("ID")){
                      sqlCriteria = "WHERE TO_CHAR(BL_ISSUE_DATE,'DD/MM/YYYY') " + sqlWild;
                  }else if(search.equalsIgnoreCase("OS")){
                      sqlCriteria = "WHERE AYSTAT " + sqlWild;
                  }else if(search.equalsIgnoreCase("IS")){
                      sqlCriteria = "WHERE AYIMST " + sqlWild;
                  }else if(search.equalsIgnoreCase("VS")){
                      sqlCriteria = "WHERE UPPER(AYSVES) " + sqlWild;
                  }else if(search.equalsIgnoreCase("VY")){
                      sqlCriteria = "WHERE UPPER(AYSVOY) " + sqlWild;    
                  }else if(search.equalsIgnoreCase("POL")){
                      sqlCriteria = "WHERE AYPOLC " + sqlWild;    
                  }else if(search.equalsIgnoreCase("POD")){
                      sqlCriteria = "WHERE AYPODC " + sqlWild;
                  }        
              }
              System.out.println("[DexBlJdbcDao][createSqlCriteriaForHblHelpScreen]: Finished");
              return sqlCriteria;
        
      }
      // --##END 02
      
     public List listForHelpScreenForBankGuarantee(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException{
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT BOOKING ");
         sql.append("      ,BL_NO ");
         sql.append("      ,OP_CODE ");
         sql.append("      ,COC_SOC ");
         sql.append("      ,BL_ISSUE_DATE ");
         sql.append("      ,OUTSTATUS ");
         sql.append("      ,INSTATUS ");
         sql.append("      ,VESSEL ");
         sql.append("      ,VOYAGE ");
         sql.append("      ,POL ");
         sql.append("      ,POD ");        
 //        sql.append("FROM VR_DEX_BL_FOR_BG ");
         sql.append("FROM VR_DIM_BANKGUARANTEE ");
         if(sqlCriteria.trim().equals("")){
             sql.append("WHERE");
         }else{
             sql.append(sqlCriteria);
             sql.append(" AND");
         }
         
         HashMap map = new HashMap();
         if(!line.equals("") ){
             sql.append(" LINE = :line");
             map.put("line",line);
         }
         if(!trade.equals("") ){
             sql.append(" AND TRADE = :trade");            
             map.put("trade",trade);
         }
         if(!agent.equals("") ){
             sql.append(" AND AGENT = :agent");
             map.put("agent",agent);
         }
         if(!fsc.equals("") ){
             sql.append(" AND RECEIVED_FSC = :fsc");
             map.put("fsc",fsc);
         }
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 DexBlMod bl = new DexBlMod();
                 bl.setBooking(RutString.nullToStr(rs.getString("BOOKING")));
                 bl.setBl(RutString.nullToStr(rs.getString("BL_NO")));
                 bl.setOpCode(RutString.nullToStr(rs.getString("OP_CODE")));
                 bl.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
                 bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                 bl.setOutStatus(RutString.nullToStr(rs.getString("OUTSTATUS")));
                 bl.setInStatus(RutString.nullToStr(rs.getString("INSTATUS")));
                 bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                 bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                 bl.setPol(RutString.nullToStr(rs.getString("POL")));
                 bl.setPod(RutString.nullToStr(rs.getString("POD")));      
                 return bl;
             }
         });
     }

     
     public List listForHelpScreenForDangerousCargoManifest(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException{
         String sqlCriteria = createSqlCriteriaBL(find, search, wild);
         StringBuffer sql = new StringBuffer();
         // ##03 BEGIN
         /*sql.append("SELECT BOOKING ");
          sql.append("      ,BL_NO ");
          sql.append("      ,OP_CODE ");
          sql.append("      ,COC_SOC ");
          sql.append("      ,BL_ISSUE_DATE ");
          sql.append("      ,OUTSTATUS ");
          sql.append("      ,INSTATUS ");
          sql.append("      ,VESSEL ");
          sql.append("      ,VOYAGE ");
          sql.append("      ,POL ");
          sql.append("      ,POD ");  
          sql.append("      ,MANIFEST_DATE ");
        */
         sql.append("SELECT /*+ FIRST_ROWS(100) */ aybkno as BOOKING ");
         sql.append("      ,ayblno as BL_NO ");
         sql.append("      ,ayopcd as OP_CODE ");
         sql.append("      ,aysorc as COC_SOC ");
         sql.append("      ,BL_ISSUE_DATE as BL_ISSUE_DATE ");
         sql.append("      ,aystat as OUTSTATUS ");
         sql.append("      ,ayimst as INSTATUS ");
         sql.append("      ,aysves as VESSEL ");
         sql.append("      ,aysvoy as VOYAGE ");
         sql.append("      ,AYMPOL as POL ");
         sql.append("      ,AYMPOD as POD ");  
         sql.append("      ,aymndt as MANIFEST_DATE ");
     //        sql.append("FROM VR_DEX_BL_FOR_BG ");
     //    sql.append("FROM VR_DEX_DGMANIFEST "); // befor ##03
         sql.append("FROM VR_DEX_BL v ");
         if(sqlCriteria.trim().equals("")){
             sql.append("WHERE");
         }else{
             sql.append(sqlCriteria);
             sql.append(" AND");
         }
         
        /* HashMap map = new HashMap();
         if(!line.equals("") ){
             sql.append(" LINE = :line");
             map.put("line",line);
         }
         if(!trade.equals("") ){
             sql.append(" AND TRADE = :trade");            
             map.put("trade",trade);
         }
         if(!agent.equals("") ){
             sql.append(" AND AGENT = :agent");
             map.put("agent",agent);
         }
         if(!fsc.equals("") ){
             sql.append(" AND RECEIVED_FSC = :fsc");
             map.put("fsc",fsc);
         }*/
         
         HashMap map = new HashMap();
         
         //--------------------------------
         //  -- CHECK PORT
          //sql.append(" ( ");
          /*sql.append("      NVL(POD,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE   NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                      ")" );
          sql.append(" OR ");         
          sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR "); 
          sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR "); 
          sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                     ")" );
           */
           /*sql.append("      NVL(AYMPOD,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                       ")" );
           sql.append(" OR ");         
           sql.append("      NVL(TSHIPMENTPORT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                      ")" );
           sql.append(" OR "); 
           sql.append("      NVL(TSHIPMENTPORT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                      ")" );
           sql.append(" OR "); 
           sql.append("      NVL(TSHIPMENTPORT3,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                      ")" );  */
           //sql.append(" ) "); 
          //--------------------------------
        sql.append(" rownum<=100 ");// limit data
          
         // ##03 END
         
         System.out.println("[DexBlJdbcDao][listForHelpScreenForDangerousCargoManifest]: "+sql.toString()); 
         
         map.put("line",line);    
         map.put("trade",trade);
         map.put("agent",agent);
         map.put("fsc",fsc);
                 
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 DexBlMod bl = new DexBlMod();
                 bl.setBooking(RutString.nullToStr(rs.getString("BOOKING")));
                 bl.setBl(RutString.nullToStr(rs.getString("BL_NO")));
                 bl.setOpCode(RutString.nullToStr(rs.getString("OP_CODE")));
                 bl.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
                 bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                 bl.setOutStatus(RutString.nullToStr(rs.getString("OUTSTATUS")));
                 bl.setInStatus(RutString.nullToStr(rs.getString("INSTATUS")));
                 bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                 bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                 bl.setPol(RutString.nullToStr(rs.getString("POL")));
                 bl.setPod(RutString.nullToStr(rs.getString("POD")));    
                 bl.setManifestDate(RutString.nullToStr(rs.getString("MANIFEST_DATE")));
                 return bl;
             }
         });
     }

     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";
         String sqlWildWithUpperCase = "";
 //        String sqlSortByIn = "";
 //        String sortBy = "";
 //        String sortByIn = "";
         
 //        if (sortByIn.equalsIgnoreCase("ASC")) {
 //            sqlSortByIn = "ASC";
 //        } else if (sortByIn.equalsIgnoreCase("DESC")) {
 //            sqlSortByIn = "DESC";
 //        }   
 //      COC/SOC screen display is COC/SOC but database store data is 'C' or 'S'
         if(search.equalsIgnoreCase("CS")){
             if(find.equalsIgnoreCase("COC")){
                 find = "C";
             }else if(find.equalsIgnoreCase("SOC")){
                 find = "S";
             }
         }else if(search.equalsIgnoreCase("OS")){
             find = getBlDescStatus(find,"A");
         }else if(search.equalsIgnoreCase("IS")){
             find = getBlDescStatus(find,"A");
         }
         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find + "%' ";
         }else{
             sqlWild = "= '" + find + "' ";
         }
                 
         if(search.equalsIgnoreCase("OS")||search.equalsIgnoreCase("IS")){  
             sqlWildWithUpperCase = "= "+find;
         }else if(search.equalsIgnoreCase("ID")){              
             //String strDate = RutDate.dateToStr(find);
             //java.util.Date utilDate = new java.util.Date(new Integer(strDate).intValue());            
             //Date date = new Date(utilDate.getTime());
             sqlWildWithUpperCase = "= '"+find+"'";
         }else if(wild.equalsIgnoreCase("ON")){
             sqlWildWithUpperCase = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWildWithUpperCase = "= '" + find.toUpperCase() + "' ";
         }
         
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("B")){
                 sqlCriteria = "WHERE BOOKING " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("BL")){
                 sqlCriteria = "WHERE BL_NO " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("OC")){
                 sqlCriteria = "WHERE OP_CODE " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("CS")){
                 sqlCriteria = "WHERE UPPER(COC_SOC) " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("ID")){
                 sqlCriteria = "WHERE TO_CHAR(BL_ISSUE_DATE,'DD/MM/YYYY') " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("OS")){
                 sqlCriteria = "WHERE OUTSTATUS " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("IS")){
                 sqlCriteria = "WHERE INSTATUS " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("VS")){
                 sqlCriteria = "WHERE UPPER(VESSEL) " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("VY")){
                 sqlCriteria = "WHERE UPPER(VOYAGE) " + sqlWildWithUpperCase;    
             }else if(search.equalsIgnoreCase("POL")){
                 sqlCriteria = "WHERE POL " + sqlWildWithUpperCase;    
             }else if(search.equalsIgnoreCase("POD")){
                 sqlCriteria = "WHERE POD " + sqlWildWithUpperCase;
             }        
         }
         return sqlCriteria;
     }  
     
     private String createSqlCriteriaBL(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";
         String sqlWildWithUpperCase = "";
     //        String sqlSortByIn = "";
     //        String sortBy = "";
     //        String sortByIn = "";
         
     //        if (sortByIn.equalsIgnoreCase("ASC")) {
     //            sqlSortByIn = "ASC";
     //        } else if (sortByIn.equalsIgnoreCase("DESC")) {
     //            sqlSortByIn = "DESC";
     //        }
     //      COC/SOC screen display is COC/SOC but database store data is 'C' or 'S'
         if(search.equalsIgnoreCase("CS")){
             if(find.equalsIgnoreCase("COC")){
                 find = "C";
             }else if(find.equalsIgnoreCase("SOC")){
                 find = "S";
             }
         }else if(search.equalsIgnoreCase("OS")){
             find = getBlDescStatus(find,"A");
         }else if(search.equalsIgnoreCase("IS")){
             find = getBlDescStatus(find,"A");
         }
         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find + "%' ";
         }else{
             sqlWild = "= '" + find + "' ";
         }
                 
         if(search.equalsIgnoreCase("OS")||search.equalsIgnoreCase("IS")){  
             sqlWildWithUpperCase = "= "+find;
         }else if(search.equalsIgnoreCase("ID")){              
             //String strDate = RutDate.dateToStr(find);
             //java.util.Date utilDate = new java.util.Date(new Integer(strDate).intValue());            
             //Date date = new Date(utilDate.getTime());
             sqlWildWithUpperCase = "= '"+find+"'";
         }else if(wild.equalsIgnoreCase("ON")){
             sqlWildWithUpperCase = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWildWithUpperCase = "= '" + find.toUpperCase() + "' ";
         }
         
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("B")){
                 sqlCriteria = "WHERE aybkno " + sqlWildWithUpperCase;// BOOKING 
             }else if(search.equalsIgnoreCase("BL")){
                 sqlCriteria = "WHERE ayblno " + sqlWildWithUpperCase;// BL_NO
             }else if(search.equalsIgnoreCase("OC")){
                 sqlCriteria = "WHERE ayopcd " + sqlWildWithUpperCase;// OP_CODE
             }else if(search.equalsIgnoreCase("CS")){
                 sqlCriteria = "WHERE UPPER(aysorc) " + sqlWildWithUpperCase;// COC_SOC
             }else if(search.equalsIgnoreCase("ID")){
                 sqlCriteria = "WHERE TO_CHAR(BL_ISSUE_DATE,'DD/MM/YYYY') " + sqlWildWithUpperCase;
             }else if(search.equalsIgnoreCase("OS")){
                 sqlCriteria = "WHERE aystat " + sqlWildWithUpperCase;// OUTSTATUS
             }else if(search.equalsIgnoreCase("IS")){
                 sqlCriteria = "WHERE ayimst " + sqlWildWithUpperCase;// INSTATUS
             }else if(search.equalsIgnoreCase("VS")){
                 sqlCriteria = "WHERE UPPER(aysves) " + sqlWildWithUpperCase;// VESSEL
             }else if(search.equalsIgnoreCase("VY")){
                 sqlCriteria = "WHERE UPPER(aysvoy) " + sqlWildWithUpperCase;// VOYAGE
             }else if(search.equalsIgnoreCase("POL")){
                 sqlCriteria = "WHERE AYMPOL " + sqlWildWithUpperCase;// POL
             }else if(search.equalsIgnoreCase("POD")){
                 sqlCriteria = "WHERE AYMPOD " + sqlWildWithUpperCase;// POD
             }        
         }
         return sqlCriteria;
     }  

     public DexBlMod findByKeyCode(String code) {
         DexBlMod blMod = null;
         StringBuffer sql = new StringBuffer();        
         sql.append("SELECT BOOKING ");
         sql.append("      ,BL_NO ");
         sql.append("      ,OP_CODE ");
         sql.append("      ,COC_SOC ");
         sql.append("      ,BL_ISSUE_DATE ");
         sql.append("      ,OUTSTATUS ");
         sql.append("      ,INSTATUS ");
         sql.append("      ,VESSEL ");
         sql.append("      ,VOYAGE ");
         sql.append("      ,POL ");
         sql.append("      ,POD ");        
         sql.append("FROM VR_DEX_BL ");
       sql.append("WHERE BL = :code");  
         try{
             blMod = (DexBlMod)getNamedParameterJdbcTemplate().queryForObject(
                    sql.toString(),
                    Collections.singletonMap("code", code),
                    new RowMapper(){
                         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                             DexBlMod bl = new DexBlMod();
                             bl.setBooking(RutString.nullToStr(rs.getString("BOOKING")));
                             bl.setBl(RutString.nullToStr(rs.getString("BL_NO")));
                             bl.setOpCode(RutString.nullToStr(rs.getString("OP_CODE")));
                             bl.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
                             bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                             bl.setOutStatus(RutString.nullToStr(rs.getString("OUTSTATUS")));
                             bl.setInStatus(RutString.nullToStr(rs.getString("INSTATUS")));
                             bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                             bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                             bl.setPol(RutString.nullToStr(rs.getString("POL")));
                             bl.setPod(RutString.nullToStr(rs.getString("POD")));      
                             return bl;
                         }
             });
         }catch (EmptyResultDataAccessException e) {
             blMod = null;
         }                    
                     
         return blMod;
     }

   

    protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModel(rs);
         }
     } 
     
     private DexBlMod moveDbToModel(ResultSet rs) throws SQLException {        
         DexBlMod bl = new DexBlMod();
         bl.setBooking(RutString.nullToStr(rs.getString("BOOKING")));
         bl.setBl(RutString.nullToStr(rs.getString("BL_NO")));
         bl.setOpCode(RutString.nullToStr(rs.getString("OP_CODE")));
         bl.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
         bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
         bl.setOutStatus(RutString.nullToStr(rs.getString("OUTSTATUS")));
         bl.setInStatus(RutString.nullToStr(rs.getString("INSTATUS")));
         bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
         bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
         bl.setPol(RutString.nullToStr(rs.getString("POL")));
         bl.setPod(RutString.nullToStr(rs.getString("POD")));        
         return bl;
     }

     public List listForHelpScreenForReeferManifest(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException{
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT BOOKING ");
         sql.append("      ,BL_NO ");
         sql.append("      ,OP_CODE ");
         sql.append("      ,CASE WHEN (NVL(TRIM(COC_SOC),'') = 'C') THEN 'COC' ELSE "); // --##01
         sql.append("       CASE WHEN (NVL(TRIM(COC_SOC),'') = 'S') THEN 'SOC' ELSE ' ' END "); // --##01
         sql.append("       END COC_SOC ");// --##01
         sql.append("      ,BL_ISSUE_DATE ");
         sql.append("      ,NVL(OUTSTATUS,999) OUTSTATUS ");// --##01
         sql.append("      ,NVL(INSTATUS,999) INSTATUS ");// --##01
         sql.append("      ,VESSEL ");
         sql.append("      ,VOYAGE ");
         sql.append("      ,POL ");
         sql.append("      ,POD ");        
         
         sql.append("FROM VR_DEX_REEFERCARGO_LIST ");
         
         if(sqlCriteria.trim().equals("")){
             sql.append("WHERE");
         }else{
             sql.append(sqlCriteria);
             sql.append(" AND  ");
         }
         
         HashMap map = new HashMap();
         
         //--------------------------------
         //  -- CHECK PORT
             sql.append(" ( ");
             sql.append("      NVL(POL,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                         ")" );
             sql.append(" OR ");         
             sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                        ")" );
             sql.append(" OR "); 
             sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                        ")" );
             sql.append(" OR "); 
             sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                        ")" );
             sql.append(" OR NVL(AYCLOF,' ') = NVL(:fsc,NVL(AYCLOF,' ')) ");
             sql.append(" ) "); 
         //--------------------------------
          // --##01 BEGIN
          sql.append(" GROUP BY ");
          sql.append("      BOOKING ");    
          sql.append("      ,BL_NO ");
          sql.append("      ,OP_CODE ");
          sql.append("      ,COC_SOC ");
          sql.append("      ,BL_ISSUE_DATE ");
          sql.append("      ,OUTSTATUS ");
          sql.append("      ,INSTATUS ");
          sql.append("      ,VESSEL ");
          sql.append("      ,VOYAGE ");
          sql.append("      ,POL ");
          sql.append("      ,POD ");
          sql.append(" ORDER BY ");
          sql.append("      BOOKING ");    
          sql.append("      ,BL_NO ");
          sql.append("      ,BL_ISSUE_DATE ");
         // --##01 END 
          map.put("line",line);    
          map.put("trade",trade);
          map.put("agent",agent);
          map.put("fsc",fsc);
                 
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 String inStatus  = "";
                 String outStatus = "";
                 DexBlMod bl = new DexBlMod();
                 // --##01 BEGIN
                 inStatus  = getBlDescStatus(RutString.nullToStr(rs.getString("INSTATUS")),"N");
                 outStatus = getBlDescStatus(RutString.nullToStr(rs.getString("OUTSTATUS")),"N");
                  // --##01 END
                 bl.setBooking(RutString.nullToStr(rs.getString("BOOKING")));
                 bl.setBl(RutString.nullToStr(rs.getString("BL_NO")));
                 bl.setOpCode(RutString.nullToStr(rs.getString("OP_CODE")));
                 bl.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
                 bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                 bl.setOutStatus(outStatus);// --##01
                 bl.setInStatus(inStatus);// --##01
                 bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                 bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                 bl.setPol(RutString.nullToStr(rs.getString("POL")));
                 bl.setPod(RutString.nullToStr(rs.getString("POD")));    
                 return bl;
             }
         });
         }
     public List listForHelpScreenForReeferManifestImport(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException{
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT BOOKING ");
         sql.append("      ,BL_NO ");
         sql.append("      ,OP_CODE ");
         sql.append("      ,CASE WHEN (NVL(TRIM(COC_SOC),'') = 'C') THEN 'COC' ELSE "); // --## 01
         sql.append("       CASE WHEN (NVL(TRIM(COC_SOC),'') = 'S') THEN 'SOC' ELSE ' ' END ");// --## 01
         sql.append("       END COC_SOC ");// --## 01
         sql.append("      ,BL_ISSUE_DATE ");
         sql.append("      ,NVL(OUTSTATUS,0) OUTSTATUS ");// --## 01
         sql.append("      ,NVL(INSTATUS,0) INSTATUS ");// --## 01
         sql.append("      ,VESSEL ");
         sql.append("      ,VOYAGE ");
         sql.append("      ,POL ");
         sql.append("      ,POD ");        
         
         sql.append(" FROM VR_DEX_REEFERCARGO_LIST ");   
         
         if(sqlCriteria.trim().equals("")){
             sql.append("WHERE");
         }else{
             sql.append(sqlCriteria);
             sql.append(" AND  ");
         }
         
         HashMap map = new HashMap();
         
         //--------------------------------
         //  -- CHECK PORT
          sql.append(" ( ");
          sql.append("      NVL(POD,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                             " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                             " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                             " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                             " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                      ")" );
          sql.append(" OR ");         
          sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                             " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                             " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                             " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                             " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR "); 
          sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                             " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                             " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                             " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                             " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR "); 
          sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                             " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                             " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                             " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                             " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                     ")" );
          sql.append(" OR NVL(AYCLOF,' ') = NVL(:fsc,NVL(AYCLOF,' ')) ");
          sql.append(" ) "); 
         //--------------------------------
          // --##01 BEGIN
          sql.append(" GROUP BY ");
          sql.append("      BOOKING ");    
          sql.append("      ,BL_NO ");
          sql.append("      ,OP_CODE ");
          sql.append("      ,COC_SOC ");
          sql.append("      ,BL_ISSUE_DATE ");
          sql.append("      ,OUTSTATUS ");
          sql.append("      ,INSTATUS ");
          sql.append("      ,VESSEL ");
          sql.append("      ,VOYAGE ");
          sql.append("      ,POL ");
          sql.append("      ,POD ");
          sql.append(" ORDER BY ");
          sql.append("      BOOKING ");    
          sql.append("      ,BL_NO ");
          sql.append("      ,BL_ISSUE_DATE ");
         // --##01 END
          map.put("line",line);    
          map.put("trade",trade);
          map.put("agent",agent);
          map.put("fsc",fsc);
                 
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 String inStatus  = "";
                 String outStatus = "";
                 DexBlMod bl = new DexBlMod();
                 // --##01 BEGIN
                 inStatus  = getBlDescStatus(RutString.nullToStr(rs.getString("INSTATUS")),"A");
                 outStatus = getBlDescStatus(RutString.nullToStr(rs.getString("OUTSTATUS")),"A");
                 // --##01 END
                 bl.setBooking(RutString.nullToStr(rs.getString("BOOKING")));
                 bl.setBl(RutString.nullToStr(rs.getString("BL_NO")));
                 bl.setOpCode(RutString.nullToStr(rs.getString("OP_CODE")));
                 bl.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
                 bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                 bl.setOutStatus(inStatus); // --##01 
                 bl.setInStatus(outStatus);// --##01 BEGIN
                 bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                 bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                 bl.setPol(RutString.nullToStr(rs.getString("POL")));
                 bl.setPod(RutString.nullToStr(rs.getString("POD")));    
                 return bl;
             }
         });
         }
     public boolean isValidForCargoManifestList(String bl) {
         System.out.println("[DimBlDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT * ");
     //        sql.append("FROM VR_DEX_BL_FOR_BG ");
         sql.append("FROM VR_DEX_REEFERCARGO_LIST  ");
         sql.append("WHERE BL_NO = :bl");
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("bl", bl.toUpperCase()));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[DimBlDao][isValid]: Finished");
         return isValid;
     }
     // --##01 BEGIN
    private String getBlDescStatus (String blCode,String mode){
        String descBl = "";
        int blCodeNo = 999;
        String blCodeStr = "";
        if(mode.equalsIgnoreCase("A")){
            blCodeStr = RutString.nullToStr(blCode);
            if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_ONE) ){
                descBl = "1";
            }else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_TWO) ){
                descBl = "2";
            }else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_THREE) ){
                descBl = "3";
            }else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_FOUR) ){
                descBl = "4";       
            } else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_FIVE) ){
                descBl = "5";       
            }else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_SIX) ){
                descBl = "6";       
            }else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_SEVEN) ){
                descBl = "7" ;     
            }else  if(blCodeStr.equalsIgnoreCase(RcmConstant.BL_DESC_NINE) ){
                descBl = "9";
            }else {
                descBl = "";
            }
        }else if(mode.equalsIgnoreCase("N")){
            blCodeNo = Integer.parseInt(blCode);
            if(blCodeNo == 1 ){
                descBl = RcmConstant.BL_DESC_ONE;
            }else  if(blCodeNo == 2 ){
                descBl = RcmConstant.BL_DESC_TWO;
            }else  if(blCodeNo == 3 ){
                descBl = RcmConstant.BL_DESC_THREE;
            }else  if(blCodeNo == 4 ){
                descBl = RcmConstant.BL_DESC_FOUR;       
            } else  if(blCodeNo == 5 ){
                descBl = RcmConstant.BL_DESC_FIVE;       
            }else  if(blCodeNo == 6 ){
                descBl = RcmConstant.BL_DESC_SIX;       
            }else  if(blCodeNo == 7 ){
                descBl = RcmConstant.BL_DESC_SEVEN ;     
            }else  if(blCodeNo == 9 ){
                descBl = RcmConstant.BL_DESC_NINE;
            }else {
                descBl = "";
            }
        }
       
        return descBl;
    }
     // --##01 END
     
      //--##04 Begin
       public List listForSearchScreenByBlEquipmentNo(String line,String trade
                                                 ,String agent,String fsc
                                                 ,String cOrS,String blNo
                                                 ,String equipmentNo,String pol
                                                 ,String pod,String service
                                                 ,String vessel,String voy,String direction,String invoyagePort,String sessionId) throws DataAccessException{
           
           System.out.println("[DexBlNo][listForSearchScreenByBlEquipmentNo]:  Started");
           StringBuffer sql = new StringBuffer();
           HashMap map = new HashMap();
           
           sql.append("SELECT DISTINCT BKGNO ");
           sql.append("       ,BLNO ");
           sql.append("       ,BLID ");
           sql.append("       ,BLTYPE ");
           sql.append("       ,BLVERSION ");
           sql.append("       ,COCSOC ");
           sql.append("       ,SERVICE ");
           sql.append("       ,VESSEL ");
           sql.append("       ,VOYAGE ");
           sql.append("       ,DIRECTION ");
           sql.append("       ,IMPSTATUS ");
           sql.append("       ,EXPSTATUS ");
           sql.append("       ,POL ");
           sql.append("       ,POT1 ");
           sql.append("       ,POD ");
           sql.append("FROM VR_DIM_IMPORT_RELEASE_ORDER VR ");
           sql.append("WHERE 1=1  ");
      // -- waiting for p'lin confirm
      //          sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
      //          sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:trade,NVL(COLL_TRADE,' '))");
      //          sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
      //          sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
      //-- waiting for p'lin confirm
           sql.append(" AND COCSOC   = :cOrS ");
           
           //--------------------------------
           //  -- CHECK PORT
            sql.append(" AND ( ");
            sql.append("      NVL(POD,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                               " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                               " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                               " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                               " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                        ")" );
            sql.append(" OR ");         
            sql.append("      NVL(POT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                               " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                               " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                               " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                               " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                       ")" );
            sql.append(" OR "); 
            sql.append("      NVL(POT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                               " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                               " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                               " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                               " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                       ")" );
            sql.append(" OR "); 
            sql.append("      NVL(POT3,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                               " WHERE  NVL(LINE_CODE,' ') = NVL(:line,NVL(LINE_CODE,' ')) " +
                                               " AND NVL(REGION_CODE,' ') = NVL(:trade,NVL(REGION_CODE,' ')) " +
                                               " AND NVL(AGENT_CODE,' ') = NVL(:agent,nvl(AGENT_CODE,' ')) " +
                                               " AND NVL(FSC_CODE,' ') = NVL(:fsc,nvl(FSC_CODE,' ')) " +
                                       ")" );
//            sql.append(" OR NVL(FSC,' ') = NVL(:fsc,NVL(FSC,' ')) ");
            sql.append(" ) ");           
            // -- END CHECK PORT
           
           if (blNo != null && !blNo.equals("")) {
               sql.append("AND BLNO=:blNo ");
           }
           if (equipmentNo != null && !equipmentNo.equals("")){
               sql.append("AND EQUIPMENTNO=:equipmentNo ");
           }
           if (pol != null && !pol.equals("")) {
               sql.append("AND POL= :pol  ");
           }
           if (pod != null && !pod.equals("")){
               sql.append("AND POD= :pod  ");
           }
           if (invoyagePort != null && !invoyagePort.equals("")){
               sql.append(" AND EXISTS ( SELECT 1 ");
               sql.append("    FROM IDP005 I05 ");
               sql.append("        ,VR_RCM_INVOYAGE_BROWSER IB ");
               sql.append("    WHERE I05.SYBLNO = VR.BLNO ");
               sql.append("        AND I05.SERVICE = IB.SERVICE ");
               sql.append("        AND I05.VESSEL = IB.VESSEL ");
               sql.append("        AND I05.VOYAGE = IB.VOYAGE ");
               sql.append("        AND I05.LOAD_PORT = IB.PORT ");
               sql.append("        AND I05.DISCHARGE_PORT = '"+invoyagePort+"' ");
               sql.append("        AND IB.SESSION_ID = '"+sessionId+"' ) ");               
           }else {
               if (service != null && !service.equals("")){
                   sql.append("AND SERVICE= :service ");
               }
               if (vessel != null && !vessel.equals("")){
                   sql.append("AND VESSEL=  :vessel  ");
               }
               if (voy != null && !voy.equals("")){
                   sql.append("AND VOYAGE=  :voy  ");
               }
               if (direction != null && !direction.equals("")){
                   sql.append("AND DIRECTION=  :direction  ");
               }
           }
           sql.append("ORDER BY BLNO  ");
           
           map.put("fsc",fsc);
           map.put("line",line);
           map.put("trade",trade);
           map.put("agent",agent);
           map.put("cOrS",cOrS);
           map.put("blNo",blNo);
           map.put("equipmentNo",equipmentNo);
           map.put("pol",pol);
           map.put("pod",pod);
           map.put("service",service);
           map.put("vessel",vessel);
           map.put("voy",voy);
           map.put("direction",direction);
           map.put("invoyagePort",invoyagePort);
           map.put("sessionId",sessionId);
           System.out.println("[DexBlJdbcDao][Criteria] : cOrS :" + cOrS );
           System.out.println("[DexBlJdbcDao][Criteria] : blNo :" + blNo );
           System.out.println("[DexBlJdbcDao][Criteria] : equipmentNo :" + equipmentNo );
           System.out.println("[DexBlJdbcDao][Criteria] : pol :" + pol );
           System.out.println("[DexBlJdbcDao][Criteria] : pod :" + pod );
           System.out.println("[DexBlJdbcDao][Criteria] : service :" + service );
           System.out.println("[DexBlJdbcDao][Criteria] : vessel :" + vessel );
           System.out.println("[DexBlJdbcDao][Criteria] : voy :" + voy );
           System.out.println("[DexBlJdbcDao][Criteria] : direction :" + direction );
           System.out.println("[DexBlJdbcDao][Criteria] : line :" + line );
           System.out.println("[DexBlJdbcDao][Criteria] : trade :" + trade );
           System.out.println("[DexBlJdbcDao][Criteria] : agent :" + agent );
           System.out.println("[DexBlJdbcDao][Criteria] : fsc :" + fsc );
           System.out.println("[DexBlJdbcDao][Criteria] : invoagePort :" + invoyagePort );
           System.out.println("[DexBlJdbcDao][Criteria] : sessionId :" + sessionId );
           
           System.out.println("[DexBlJdbcDao][listForSearchScreenByBlEquipmentNo]: SQL: " + sql.toString());
           System.out.println("[DexBlJdbcDao][listForSearchScreenByBlEquipmentNo]:  Finished");
           return getNamedParameterJdbcTemplate().query(
                      sql.toString(),
                      map,
           new RowMapper(){
               public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                   DexBlMod bl = new DexBlMod();
                   bl.setBooking(RutString.nullToStr(rs.getString("BKGNO")));
                   bl.setBl(RutString.nullToStr(rs.getString("BLNO")));                      
                   bl.setBlId(RutString.nullToStr(rs.getString("BLID")));   
                   bl.setBlType(RutString.nullToStr(rs.getString("BLTYPE")));
                   bl.setBlVersion(RutString.nullToStr(rs.getString("BLVERSION")));
                   bl.setCocSoc(RutString.nullToStr(rs.getString("COCSOC")));
                   bl.setService(RutString.nullToStr(rs.getString("SERVICE")));
                   bl.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                   bl.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                   bl.setInStatus(RutString.nullToStr(rs.getString("IMPSTATUS")));
                   bl.setOutStatus(RutString.nullToStr(rs.getString("EXPSTATUS")));
                   bl.setPol(RutString.nullToStr(rs.getString("POL")));
                   bl.setPot(RutString.nullToStr(rs.getString("POT1")));
                   bl.setPod(RutString.nullToStr(rs.getString("POD")));
                   return bl;
               }
           });
           
       }
      public List listForNewHelpScreen(int rowAt,int rowTo,String find, String search, String wild) throws DataAccessException{
          System.out.println("[dexBlJdbcDao][listForNewHelpScreen]: started");
          String sqlCriteria = createSqlCriteriaForHblHelpScreen(find, search, wild);
          StringBuffer sql = new StringBuffer();
          String selectColumn = "AYBKNO,AYBLNO, HOUSE_BL_NO, AYOPCD, AYSORC , BL_ISSUE_DATE , AYSTAT , AYIMST , AYSVES , AYSVOY , AYPOLC , AYPODC ";        
          sql.append("SELECT "+selectColumn);
          sql.append("FROM VR_DEX_BL ");

          
          if(sqlCriteria.trim().equals("")){
              //sql.append("WHERE 1=1 ");
          }else{
              sql.append(sqlCriteria);
             // sql.append(" AND");
          }
          
          //sql.append(sqlCriteria);
         
          sql.append(" GROUP BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");         
          sql.append("      ,HOUSE_BL_NO ");      
          sql.append("      ,AYOPCD ");         
          sql.append("      ,AYSORC ");        
          sql.append("      ,BL_ISSUE_DATE ");  
          sql.append("      ,AYSTAT ");       
          sql.append("      ,AYIMST ");        
          sql.append("      ,AYSVES ");          
          sql.append("      ,AYSVOY ");         
          sql.append("      ,AYPOLC ");            
          sql.append("      ,AYPODC ");          
          sql.append(" ORDER BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");
          sql.append("      ,BL_ISSUE_DATE ");
          sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);          
          System.out.println("[DexBlJdbcDao][listForNewHelpScreen]: SQL = "+sql.toString());
          System.out.println("[DexBlJdbcDao][listForNewHelpScreen]: Finished");
          return getNamedParameterJdbcTemplate().query(
                              sql.toString(),
                              new HashMap(),
          new RowModMapper(){
              public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                  DexBlMod bl = new DexBlMod();
                  bl.setBooking(RutString.nullToStr(rs.getString("AYBKNO")));
                  bl.setBl(RutString.nullToStr(rs.getString("AYBLNO")));
                  bl.setHbl(RutString.nullToStr(rs.getString("HOUSE_BL_NO")));
                  bl.setOpCode(RutString.nullToStr(rs.getString("AYOPCD")));
                  bl.setCocSoc(RutString.nullToStr(rs.getString("AYSORC")));
                  bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                  bl.setOutStatus(RutString.nullToStr(rs.getString("AYSTAT")));
                  bl.setInStatus(RutString.nullToStr(rs.getString("AYIMST")));
                  bl.setVessel(RutString.nullToStr(rs.getString("AYSVES")));
                  bl.setVoyage(RutString.nullToStr(rs.getString("AYSVOY")));
                  bl.setPol(RutString.nullToStr(rs.getString("AYPOLC")));
                  bl.setPod(RutString.nullToStr(rs.getString("AYPODC")));      
                  return bl;
              }
              });
      }
               
      public int countListForNewHelpScreen(String find, String search, String wild) throws DataAccessException{
          String sqlCriteria = createSqlCriteriaForHblHelpScreen(find, search, wild);
          StringBuffer sql = new StringBuffer();
         
          sql.append("SELECT AYBKNO ");           //BOOKING
          sql.append("      ,AYBLNO ");           //BL_NO
          sql.append("      ,HOUSE_BL_NO ");      //HOUSE_BL_NO
          sql.append("      ,AYOPCD ");           //OP_CODE
          sql.append("      ,AYSORC ");           //COC_SOC
          sql.append("      ,BL_ISSUE_DATE ");    //BL_ISSUE_DATE
          sql.append("      ,AYSTAT ");           //OUTSTATUS
          sql.append("      ,AYIMST ");           //INSTATUS
          sql.append("      ,AYSVES ");           // VESSEL
          sql.append("      ,AYSVOY ");           // VOYAGE
          sql.append("      ,AYPOLC ");           // POL
          sql.append("      ,AYPODC ");           // POD
          sql.append(" FROM VR_DEX_BL ");  
//          sql.append(" WHERE   1=1      ");  
          if(sqlCriteria.trim().equals("")){
              //sql.append("WHERE 1=1 ");
          }else{
              sql.append(sqlCriteria);
          //    sql.append(" AND");
          }
          
         // sql.append(sqlCriteria);
          
          sql.append(" GROUP BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");         
          sql.append("      ,HOUSE_BL_NO ");      
          sql.append("      ,AYOPCD ");         
          sql.append("      ,AYSORC ");        
          sql.append("      ,BL_ISSUE_DATE ");  
          sql.append("      ,AYSTAT ");       
          sql.append("      ,AYIMST ");        
          sql.append("      ,AYSVES ");          
          sql.append("      ,AYSVOY ");         
          sql.append("      ,AYPOLC ");            
          sql.append("      ,AYPODC ");          
          sql.append(" ORDER BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");
          sql.append("      ,BL_ISSUE_DATE ");
          System.out.println("sql :" +sql.toString());
          sql = getNumberOfAllData(sql);

          HashMap map = new HashMap();                

      //        map.put("fsc",realFsc);
          Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map, Integer.class);
          return result;
      }  
      public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException{
          System.out.println("[dexBlJdbcDao][listForNewHelpScreenWithFsc]: started");
          String sqlCriteria = createSqlCriteriaForHblHelpScreen(find, search, wild);
          StringBuffer sql = new StringBuffer();
          String selectColumn = "AYBKNO,AYBLNO, HOUSE_BL_NO, AYOPCD, AYSORC , BL_ISSUE_DATE , AYSTAT , AYIMST , AYSVES , AYSVOY , AYPOLC , AYPODC ";        
          sql.append("SELECT "+selectColumn);
          sql.append("FROM VR_DEX_BL ");
          
          if(sqlCriteria.trim().equals("")){
              sql.append("WHERE ");
          }else{
              sql.append(sqlCriteria);
              sql.append(" AND");
          }
          
         // sql.append(sqlCriteria);
          //  -- CHECK PORT
           sql.append(" ( ");
           sql.append("      NVL(AYPODC,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                       ")" );
           sql.append(" OR ");         
           sql.append("      NVL(TSHIPMENTPORT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );
           sql.append(" OR "); 
           sql.append("      NVL(TSHIPMENTPORT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );
           sql.append(" OR "); 
           sql.append("      NVL(TSHIPMENTPORT3,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );

              sql.append(" ) "); 
          //--------------------------------
          sql.append(" GROUP BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");         
          sql.append("      ,HOUSE_BL_NO ");      
          sql.append("      ,AYOPCD ");         
          sql.append("      ,AYSORC ");        
          sql.append("      ,BL_ISSUE_DATE ");  
          sql.append("      ,AYSTAT ");       
          sql.append("      ,AYIMST ");        
          sql.append("      ,AYSVES ");          
          sql.append("      ,AYSVOY ");         
          sql.append("      ,AYPOLC ");            
          sql.append("      ,AYPODC ");          
          sql.append(" ORDER BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");
          sql.append("      ,BL_ISSUE_DATE ");
          sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);
          System.out.println("[DexBlJdbcDao][listForNewHelpScreenWithFsc]: SQL = "+sql.toString());
          System.out.println("[DexBlJdbcDao][listForNewHelpScreenWithFsc]: Finished");
          HashMap map = new HashMap();                
          map.put("line",line);
          map.put("trade",trade);
          map.put("agent",agent);
          map.put("fsc",fsc);
          
          return getNamedParameterJdbcTemplate().query(
                     sql.toString(),
                     map,
         new RowModMapper(){
              public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                  DexBlMod bl = new DexBlMod();
                  bl.setBooking(RutString.nullToStr(rs.getString("AYBKNO")));
                  bl.setBl(RutString.nullToStr(rs.getString("AYBLNO")));
                  bl.setHbl(RutString.nullToStr(rs.getString("HOUSE_BL_NO")));
                  bl.setOpCode(RutString.nullToStr(rs.getString("AYOPCD")));
                  bl.setCocSoc(RutString.nullToStr(rs.getString("AYSORC")));
                  bl.setIssueDate(rs.getDate("BL_ISSUE_DATE"));
                  bl.setOutStatus(RutString.nullToStr(rs.getString("AYSTAT")));
                  bl.setInStatus(RutString.nullToStr(rs.getString("AYIMST")));
                  bl.setVessel(RutString.nullToStr(rs.getString("AYSVES")));
                  bl.setVoyage(RutString.nullToStr(rs.getString("AYSVOY")));
                  bl.setPol(RutString.nullToStr(rs.getString("AYPOLC")));
                  bl.setPod(RutString.nullToStr(rs.getString("AYPODC")));      
                  return bl;
              }
              });
      }
      public int countListForNewHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{
          String sqlCriteria = createSqlCriteriaForHblHelpScreen(find, search, wild);
          StringBuffer sql = new StringBuffer();

          sql.append("SELECT AYBKNO ");           //BOOKING
          sql.append("      ,AYBLNO ");           //BL_NO
          sql.append("      ,HOUSE_BL_NO ");      //HOUSE_BL_NO
          sql.append("      ,AYOPCD ");           //OP_CODE
          sql.append("      ,AYSORC ");           //COC_SOC
          sql.append("      ,BL_ISSUE_DATE ");    //BL_ISSUE_DATE
          sql.append("      ,AYSTAT ");           //OUTSTATUS
          sql.append("      ,AYIMST ");           //INSTATUS
          sql.append("      ,AYSVES ");           // VESSEL
          sql.append("      ,AYSVOY ");           // VOYAGE
          sql.append("      ,AYPOLC ");           // POL
          sql.append("      ,AYPODC ");           // POD
          sql.append("FROM VR_DEX_BL ");
          
          if(sqlCriteria.trim().equals("")){
              sql.append("WHERE ");
          }else{
              sql.append(sqlCriteria);
              sql.append(" AND ");
          }
          
        //  sql.append(sqlCriteria);
          //  -- CHECK PORT
           sql.append(" ( ");
           sql.append("      NVL(AYPODC,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                       ")" );
           sql.append(" OR ");         
           sql.append("      NVL(TSHIPMENTPORT1,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );
           sql.append(" OR "); 
           sql.append("      NVL(TSHIPMENTPORT2,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );
           sql.append(" OR "); 
           sql.append("      NVL(TSHIPMENTPORT3,' ')  IN ( SELECT   PORT_CODE  FROM ( VR_CAM_FSC_PORT ) " +
                                                 " WHERE   NVL(LINE_CODE,' ') = NVL('" +line+ "',NVL(LINE_CODE,' ')) " +
                                                 " AND NVL(REGION_CODE,' ') = NVL('" +trade+ "',NVL(REGION_CODE,' ')) " +
                                                 " AND NVL(AGENT_CODE,' ') = NVL('"+ agent+ "',nvl(AGENT_CODE,' ')) " +
                                                 " AND NVL(FSC_CODE,' ') = NVL('" +fsc+ "',nvl(FSC_CODE,' ')) " +
                                      ")" );

              sql.append(" ) "); 
          //--------------------------------
          sql.append(" GROUP BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");         
          sql.append("      ,HOUSE_BL_NO ");      
          sql.append("      ,AYOPCD ");         
          sql.append("      ,AYSORC ");        
          sql.append("      ,BL_ISSUE_DATE ");  
          sql.append("      ,AYSTAT ");       
          sql.append("      ,AYIMST ");        
          sql.append("      ,AYSVES ");          
          sql.append("      ,AYSVOY ");         
          sql.append("      ,AYPOLC ");            
          sql.append("      ,AYPODC ");          
          sql.append(" ORDER BY ");
          sql.append("      AYBKNO ");    
          sql.append("      ,AYBLNO ");
          sql.append("      ,BL_ISSUE_DATE ");
          sql = getNumberOfAllData(sql);
          
          HashMap map = new HashMap();                
          map.put("line",line);
          map.put("trade",trade);
          map.put("agent",agent);
          map.put("fsc",fsc);
      //        map.put("fsc",realFsc);
          Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map, Integer.class);
          return result;
      }
      
      //--##04 End     
 }
