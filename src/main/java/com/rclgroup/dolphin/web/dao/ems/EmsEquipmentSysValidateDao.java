/*-----------------------------------------------------------------------------------------------------------  
EmsEquipmentSysValidateDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/04/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 30/04/2009   WAC                   Added isValid,getErrDescription method.
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface EmsEquipmentSysValidateDao extends RriStandardDao {

    /**
     * list equipment system validate records for help screen
     * @param find
     * @param search
     * @param wild
     * @param regionCode
     * @param errorType
     * @param status
     * @return list of equipment system validate
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild, String regionCode, String errorType, String status) throws DataAccessException;

    /**
     * check valid of errcode
     * @param errcode
     * @return valid of errcode
     * @throws DataAccessException
     */
    public boolean isValid(String errcode) throws DataAccessException;
    /**
     * get error description
     * @param errcode
     * @return error description
     * @throws DataAccessException
     */
    public String getErrDescription(String errcode) throws DataAccessException;
 
 
}
