 /*------------------------------------------------------
 CamPointJdbcDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sopon Dee-udomvongsa 18/07/2008    
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamPointMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class CamPointJdbcDao extends RrcStandardDao implements CamPointDao {
     
     public CamPointJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String pointCode) throws DataAccessException {
        System.out.println("[CamPointJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT POINT_CODE ");
        sql.append("FROM VR_CAM_POINT ");
        sql.append("WHERE POINT_CODE = :pointCode");        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("pointCode", pointCode));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         
         System.out.println("[CamPointJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValid(String pointCode, String status) throws DataAccessException {
         System.out.println("[CamPointJdbcDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT POINT_CODE ");
         sql.append("FROM VR_CAM_POINT ");
         sql.append("WHERE POINT_CODE = :pointCode ");
         sql.append("AND STATUS = :status ");
         
         HashMap map = new HashMap();
         map.put("pointCode",pointCode);
         map.put("status",status);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[CamPointJdbcDao][isValid]: Finished");
         return isValid;
     }   
     
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String controlZoneCode, String zoneCode) throws DataAccessException {
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT POINT_CODE ");
         sql.append("      ,POINT_NAME ");
         sql.append("      ,ZONE ");   
         sql.append("      ,CONTROL_ZONE ");
         sql.append("      ,ZONE ");
         sql.append("      ,AREA ");            
         sql.append("      ,REGION "); 
         sql.append("      ,COUNTRY ");
         sql.append("      ,STATUS ");
         sql.append("FROM VR_CAM_POINT ");
         sql.append("WHERE 1 = 1 ");
         if((regionCode!=null)&&(!regionCode.trim().equals(""))){
             sql.append("  AND REGION = :regionCode "); 
         }
         if((areaCode!=null)&&(!areaCode.trim().equals(""))){
             sql.append("  AND AREA = :areaCode "); 
         }  
         if((controlZoneCode!=null)&&(!controlZoneCode.trim().equals(""))){
             sql.append("  AND CONTROL_ZONE = :controlZoneCode "); 
         }
         if((zoneCode!=null)&&(!zoneCode.trim().equals(""))){
             sql.append("  AND ZONE = :zoneCode "); 
         }
         sql.append(sqlCriteria);
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
         HashMap map = new HashMap();
         map.put("regionCode",regionCode);
         map.put("areaCode",areaCode);
         map.put("controlZoneCode",controlZoneCode);
         map.put("zoneCode",zoneCode);
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());
     }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND POINT_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "AND POINT_NAME " + sqlWild;
             }else if(search.equalsIgnoreCase("Z")){
                 sqlCriteria = "AND ZONE " + sqlWild;
             }else if(search.equalsIgnoreCase("CZ")){
                 sqlCriteria = "AND CONTROL_ZONE " + sqlWild;
             }else if(search.equalsIgnoreCase("A")){
                 sqlCriteria = "AND AREA " + sqlWild;
             }else if(search.equalsIgnoreCase("R")){
                 sqlCriteria = "AND REGION " + sqlWild;
             }else if(search.equalsIgnoreCase("CO")){
                 sqlCriteria = "AND COUNTRY " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND STATUS " + sqlWild;
             }        
         }
         return sqlCriteria;
     }    
     
     public List listForHelpScreenWithFsc(String find, String search, String wild, String fsc) throws DataAccessException {
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT POINT_CODE ");
         sql.append("      ,POINT_NAME ");
         sql.append("      ,ZONE ");   
         sql.append("      ,CONTROL_ZONE ");
         sql.append("      ,AREA ");            
         sql.append("      ,REGION "); 
         sql.append("      ,COUNTRY ");
         sql.append("      ,STATUS ");
         sql.append("FROM VR_CAM_POINT ");
         sql.append("WHERE 1 = 1 ");
         if((fsc!=null)&&(!fsc.trim().equals(""))){
             sql.append("  AND FSC = :fsc "); 
         } 
         sql.append(sqlCriteria);
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: With status: Finished");
         HashMap map = new HashMap();
         map.put("fsc",fsc);
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());         
     }
     
     public List listForHelpScreenWithControlZone(String find, String search, String wild, String controlZone) throws DataAccessException {
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT POINT_CODE ");
         sql.append("      ,POINT_NAME ");
         sql.append("      ,ZONE ");   
         sql.append("      ,CONTROL_ZONE ");
         sql.append("      ,AREA ");            
         sql.append("      ,REGION "); 
         sql.append("      ,COUNTRY ");
         sql.append("      ,STATUS ");
         sql.append("FROM VR_CAM_POINT ");
         sql.append("WHERE 1 = 1 ");
         if((controlZone!=null)&&(!controlZone.trim().equals(""))){
             sql.append("  AND CONTROL_ZONE = :controlZone "); 
         } 
         sql.append(sqlCriteria);
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         System.out.println("[CamPointJdbcDao][listForHelpScreen]: With status: Finished");
         HashMap map = new HashMap();
         map.put("controlZone",controlZone);
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());         
     }
     
     private String createSqlCriteriaWithStatus(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND POINT_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "AND POINT_NAME " + sqlWild;
             }else if(search.equalsIgnoreCase("Z")){
                 sqlCriteria = "AND ZONE " + sqlWild;
             }else if(search.equalsIgnoreCase("CZ")){
                 sqlCriteria = "AND CONTROL_ZONE " + sqlWild;
             }else if(search.equalsIgnoreCase("A")){
                 sqlCriteria = "AND AREA " + sqlWild;
             }else if(search.equalsIgnoreCase("R")){
                 sqlCriteria = "AND REGION " + sqlWild;
             }else if(search.equalsIgnoreCase("CO")){
                 sqlCriteria = "AND COUNTRY " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND STATUS " + sqlWild;
             }            
         }
         return sqlCriteria;
     }

    protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModelForHelp(rs);
         }
     } 
     
    
     private CamPointMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
         CamPointMod Point = new CamPointMod();
         Point.setPointCode(RutString.nullToStr(rs.getString("POINT_CODE")));   
         Point.setPointName(RutString.nullToStr(rs.getString("POINT_NAME")));        
         Point.setPointZone(RutString.nullToStr(rs.getString("ZONE")));
         Point.setPointControlZone(RutString.nullToStr(rs.getString("CONTROL_ZONE")));
         Point.setPointArea(RutString.nullToStr(rs.getString("AREA"))); 
         Point.setPointRegion(RutString.nullToStr(rs.getString("REGION")));        
         Point.setPointCountryCode(RutString.nullToStr(rs.getString("COUNTRY")));
         Point.setPointStatus(RutString.nullToStr(rs.getString("STATUS")));
         return Point;
     }     
       
 }