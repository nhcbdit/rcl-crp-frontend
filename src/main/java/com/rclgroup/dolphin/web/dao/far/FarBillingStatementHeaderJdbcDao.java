/*-----------------------------------------------------------------------------------------------------------  
FarBillingStatementHeaderJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 30/07/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.far.FarBillingStatementHeaderMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;


public class FarBillingStatementHeaderJdbcDao extends RrcStandardDao implements FarBillingStatementHeaderDao{
   
    protected void initDao() throws Exception {
        super.initDao();
    }

    public FarBillingStatementHeaderMod findByKey(String billNo, int billVersion) throws DataAccessException {
        System.out.println("[FarBillingStatementHeaderJdbcDao][findByKey]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("select BILL_NO ");
        sql.append("    ,BILL_VERSION ");
        sql.append("    ,BATCH_NUMBER ");
        sql.append("    ,TEMPLATE_CODE ");
        sql.append("    ,IB_OB_XT ");
        sql.append("    ,COLL_FSC ");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,PO_NUMBER ");
        sql.append("    ,VENDOR_ID ");
        sql.append("    ,BILL_DATE ");
        sql.append("    ,BILL_FROM ");
        sql.append("    ,BILL_TO ");
        sql.append("    ,BILL_ATTN ");
        sql.append("    ,DESCRIPTION ");
        sql.append("    ,BILL_STATUS ");
        sql.append("    ,RECORD_ADD_DATE ");
        sql.append("    ,RECORD_ADD_USER ");
        sql.append("from VR_FAR_BILL_STATEMENT_HDR ");
        sql.append("where BILL_NO = :billNo ");
        sql.append("    and BILL_VERSION = :billVersion ");
            
        HashMap map = new HashMap();
        map.put("billNo", billNo);
        map.put("billVersion", String.valueOf(billVersion));
            
        System.out.println("[FarBillingStatementHeaderJdbcDao][findByKey]: SQL = " + sql.toString());
        System.out.println("[FarBillingStatementHeaderJdbcDao][findByKey]: Finished");
        return (FarBillingStatementHeaderMod) getNamedParameterJdbcTemplate().queryForObject(
                    sql.toString(),
                    map,
                    new RowMapper() {
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            FarBillingStatementHeaderMod bean = new FarBillingStatementHeaderMod();
                            bean.setBillNo(RutString.nullToStr(rs.getString("BILL_NO")));
                            bean.setBillVersion(rs.getInt("BILL_VERSION"));
                            bean.setBatchNumber(rs.getInt("BATCH_NUMBER"));
                            bean.setTemplateCode(RutString.nullToStr(rs.getString("TEMPLATE_CODE")));
                            bean.setIbObXt(RutString.nullToStr(rs.getString("IB_OB_XT")));
                            bean.setCollFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                            bean.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                            bean.setPoNumber(RutString.nullToStr(rs.getString("PO_NUMBER")));
                            bean.setVendorId(RutString.nullToStr(rs.getString("VENDOR_ID")));
                            bean.setBillDate(RutString.nullToStr(rs.getString("BILL_DATE")));
                            bean.setBillFromDate(RutString.nullToStr(rs.getString("BILL_FROM")));
                            bean.setBillToDate(RutString.nullToStr(rs.getString("BILL_TO")));
                            bean.setBillAttn(RutString.nullToStr(rs.getString("BILL_ATTN")));
                            bean.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));
                            bean.setBillStatus(RutString.nullToStr(rs.getString("BILL_STATUS")));
                            bean.setRecordAddDate(RutString.nullToStr(rs.getString("RECORD_ADD_DATE")));
                            bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                            return bean;
                    }
            });
    }

}
