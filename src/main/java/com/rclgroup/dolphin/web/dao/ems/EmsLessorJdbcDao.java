/*-----------------------------------------------------------------------------------------------------------  
EmsLessorJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 03/07/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsLessorMod;
import com.rclgroup.dolphin.web.util.RutString;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class EmsLessorJdbcDao extends RrcStandardDao implements EmsLessorDao {

    public EmsLessorJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String LessorCode) throws DataAccessException {
        System.out.println("[EmsLessorJdbcDao][isValid]: Started");
        boolean isValid = false;    
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT LESSOR_CODE ");
        sql.append("FROM VR_EMS_LESSOR ");
        sql.append("WHERE LESSOR_CODE = :LessorCode");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("LessorCode", LessorCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[EmsLessorJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[EmsLessorJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT LESSOR_CODE ");
        sql.append("      ,LESSOR_NAME ");
        sql.append("      ,STATUS "); 
        sql.append("FROM VR_EMS_LESSOR ");
        sql.append("WHERE STATUS = 'Active'");
        sql.append(sqlCriteria);            
        System.out.println("[EmsLessorJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[EmsLessorJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND LESSOR_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND LESSOR_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private EmsLessorMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        EmsLessorMod LessorCode = new EmsLessorMod();
        LessorCode.setLessorCode(RutString.nullToStr(rs.getString("LESSOR_CODE")));
        LessorCode.setLessorName(RutString.nullToStr(rs.getString("LESSOR_NAME")));
        LessorCode.setLessorStatus(RutString.nullToStr(rs.getString("STATUS")));

        return LessorCode;
    }

}
