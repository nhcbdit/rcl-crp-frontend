package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaServiceVariantMod;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaVoyageHeaderMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;

public interface BsaVsaVoyageHeaderDao extends RriStandardDao{
    
    /**
     * list all VSA records for search screen
     * @param modelId, proforma, service, vessel, voyage, direction, effectiveDate, expiryDate
     * @return list of (BsaVsaVoyageHeaderDao)
     * @throws DataAccessException
     */
    
    public List listForSearchScreen(String modelId, String proforma, String service, String vessel, String voyage, String direction, String effectiveDate, String expiryDate) throws DataAccessException;
    
    /**
     * @param service
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForService(String service) throws DataAccessException;
    
    /**
     * @param vessel
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVessel(String vessel) throws DataAccessException;
    
    /**
     * @param voyage
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVoyage(String voyage) throws DataAccessException;
    
    /**
     * list all VSA records for search screen
     * @param modelId, proforma, service, vessel, voyage, direction, effectiveDate, expiryDate
     * @return List of VSA
     * @throws DataAccessException exception which client has to catch all following error messages:                             
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public List getCursor (String modelId, String proforma, String service, String vessel, String voyage, String direction, String effectiveDate, String expiryDate, String sortBy, String sortByIn) throws DataAccessException;
    
    public BsaVsaVoyageHeaderMod findByKeyVoyageHeaderId(String voyageHeaderId) throws DataAccessException;
        
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException;
}
