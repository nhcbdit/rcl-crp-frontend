/*-----------------------------------------------------------------------------------------------------------  
FarBookingPartyMappingDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 15/07/10
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.far.FarBookingPartyMappingMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface FarBookingPartyMappingDao extends RriStandardDao {
    
    /**
      * @param pkBookingPartyMapId
      * @return booking party mapping model (FarBookingPartyMappingMod)
      * @throws DataAccessException
      */
    public FarBookingPartyMappingMod findByKey(String pkBookingPartyMapId) throws DataAccessException;
    
    /**
      * number of list of booking party mapping
      * @param columnName 
      * @param conditionWild
      * @param columnFind
      * @param billToParty      
      * @param bookingParty
      * @param recordStatus
      * @param permissionUser
      * @return number
      * @throws DataAccessException
      */
    public int findCountByStatement(String columnName, String conditionWild, String columnFind
        ,String billToParty, String bookingParty, String recordStatus, String permissionUser) throws DataAccessException;
     
    /**
      * list booking party mapping for search screen
      * @param recFirst
      * @param recLast
      * @param columnName
      * @param conditionWild
      * @param columnFind
      * @param sortBy
      * @param sortByIn
      * @param billToParty 
      * @param bookingParty 
      * @param recordStatus
      * @param permissionUser
      * @return list of booking party mapping model (FarBookingPartyMappingMod)
      * @throws DataAccessException
      */
    public List findByStatement(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortByIn
        ,String billToParty, String bookingParty, String recordStatus, String permissionUser) throws DataAccessException;
        
    /**
      * insert a booking party mapping record 
      * @param bean
      * @throws DataAccessException 
      */
    public boolean insert(FarBookingPartyMappingMod bean) throws DataAccessException;
    
    /**
      * update a booking party mapping record 
      * @param bean
      * @throws DataAccessException 
      */
    public boolean update(FarBookingPartyMappingMod bean) throws DataAccessException;
    
    /**
     * delete a booking party mapping record 
     * @param bean     
     * @throws DataAccessException
     */
    public boolean delete(FarBookingPartyMappingMod bean) throws DataAccessException;
     
}
