 /*------------------------------------------------------
 VmsPerformaAgencyDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2010
 --------------------------------------------------------
 Author Kitti Pongsirisakun 02/08/2010   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.dao.vms;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface VmsPerformaAgencyDao {
 
     public boolean isValid(String performaNo) throws DataAccessException;
          
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

 
 }
