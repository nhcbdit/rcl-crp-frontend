/*-----------------------------------------------------------------------------------------------------------  
BsaBsaPortPairTsJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Songkran Totiya 12/04/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/ 
package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.bsa.BsaBsaPortPairTsDao;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaPortPairTsMod;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaPortPairTsMod2;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class BsaBsaPortPairTsJdbcDao extends RrcStandardDao implements BsaBsaPortPairTsDao {
    
    public BsaBsaPortPairTsJdbcDao() { 
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValidWithStatus(String find,String search,String status) {
        System.out.println("[BsaBsaPortPairTsJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("select "+search+" ");
        sql.append("from VR_BSA_PORT_PAIR_TS ");
        sql.append(" where RECORD_STATUS = :status ");        
//        sql.append(" and rownum = 1 ");
        HashMap map = new HashMap();
        map.put("status",status);
        if ((find!=null)&&(!find.trim().equals(""))) {
            sql.append(" and "+search+" = :find ");            
            map.put(search,find);
        }
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[BsaBsaPortPairTsJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValidWithStatus(String tsIndic,String service,String vCode,String portCode,String groupCode,String status) {
        System.out.println("[BsaBsaPortPairTsJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        String whereClause = "";
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE ");
        sql.append("from VR_BSA_PORT_PAIR_TS ");        
        HashMap map = new HashMap();
        if ((tsIndic!=null)&&(!tsIndic.trim().equals(""))) {
            whereClause = "AND TS_INDIC = :tsIndic ";            
            map.put("tsIndic",tsIndic);
        }        
        if ((service!=null)&&(!service.trim().equals(""))) {
            whereClause = "AND SERVICE = :service ";            
            map.put("service",service);
        }
        if ((vCode!=null)&&(!vCode.trim().equals(""))) {
            whereClause = "AND V_CODE = :vCode ";            
            map.put("vCode",vCode);
        }
        if ((portCode!=null)&&(!portCode.trim().equals(""))) {
            whereClause = "AND PORT_CODE = :portCode ";            
            map.put("portCode",portCode);
        }
        if ((groupCode!=null)&&(!groupCode.trim().equals(""))) {
            whereClause = "AND GROUP_CODE = :groupCode ";            
            map.put("groupCode",groupCode);
        }
        if ((status!=null)&&(!status.trim().equals(""))) {
            whereClause = "AND RECORD_STATUS = :status ";            
            map.put("status",status);
        }
        if (!whereClause.equals("")){
            sql.append(" where rownum = 1 "+whereClause);
        }
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[BsaBsaPortPairTsJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreenWithStatus(String find, String search, String wild, String status, String tsIndic, int serviceVariantId, int modelId, String tsPolPort,String tsPodPort, String polTsFlag, String podTsFlag) {
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct TS_INDIC ");
        sql.append("    ,SERVICE ");
        sql.append("    ,V_CODE ");
        sql.append("    ,PORT_CODE ");
        sql.append("    ,GROUP_CODE "); 
        sql.append("    ,GROUP_FLAG ");
        sql.append("    ,NVL(SUB_GROUP_FLAG,'G') AS SUB_GROUP_FLAG ");
        sql.append("    ,RECORD_STATUS ");
        sql.append("from VR_BSA_PORT_PAIR_TS ");
        sql.append(" where RECORD_STATUS = :status "); 
        sql.append(" and SERVICE NOT IN  (SELECT SV.SERVICE FROM VR_BSA_SERVICE_VARIANT SV where BSA_SERVICE_VARIANT_ID = :sv_id1) ");
        HashMap map = new HashMap();        
        map.put("status",status);
        map.put("sv_id1",new Integer(serviceVariantId));
        /*
        if(tsIndic!=null && !tsIndic.equals("")){
            sql.append(" AND UPPER(TS_INDIC) = :ts_indic ");
            map.put("ts_indic",tsIndic.toUpperCase());
            
            if(tsIndic.toUpperCase().equals("FROM")){
                sql.append(" AND L_PORT = :ts_polport ");
                map.put("ts_polport",tsPolPort.toUpperCase());
            }else{
                sql.append(" AND D_PORT = :ts_podport ");
                map.put("ts_podport",tsPodPort.toUpperCase());
            }            
        }
        */
         if(polTsFlag.toUpperCase().equals("Y") && podTsFlag.toUpperCase().equals("Y")){
             sql.append(" AND (L_PORT = :ts_polport OR D_PORT = :ts_podport) ");
             map.put("ts_polport",tsPolPort.toUpperCase());
             map.put("ts_podport",tsPodPort.toUpperCase());
         }else if(polTsFlag.toUpperCase().equals("Y")){
             sql.append(" AND L_PORT = :ts_polport ");
             map.put("ts_polport",tsPolPort.toUpperCase());
         }else if(podTsFlag.toUpperCase().equals("Y")){
             sql.append(" AND D_PORT = :ts_podport ");
             map.put("ts_podport",tsPodPort.toUpperCase());
         }            
        if(serviceVariantId>0){
            sql.append(" AND SV_ID = :sv_id ");
            map.put("sv_id",new Integer(serviceVariantId));
        }
        if(modelId>0){
            sql.append(" AND MOD_ID = :mod_id ");
            map.put("mod_id",new Integer(modelId));
        }
        if(tsPolPort!=null && !tsPolPort.equals("")){
            sql.append(" AND PORT_CODE <> :ts_polport ");
            map.put("ts_polport",tsPolPort);
        }
        if(tsPodPort!=null && !tsPodPort.equals("")){
            sql.append(" AND PORT_CODE <> :ts_podport ");
            map.put("ts_podport",tsPodPort);
        }
        if(sqlCriteria!=null&&!sqlCriteria.equals("")){
            sql.append(sqlCriteria);
        }
        sql.append("order by TS_INDIC ");
        sql.append("    ,SERVICE ");
        sql.append("    ,V_CODE ");
        sql.append("    ,PORT_CODE ");
        sql.append("    ,GROUP_CODE ");
//        System.out.println("tspol = "+tsPolPort+" tspod = "+tsPodPort+"  tsindic = "+tsIndic+"   sv = "+serviceVariantId);
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaBsaPortPairTsMod bean = new BsaBsaPortPairTsMod();
                           bean.setTsIndic(RutString.nullToStr(rs.getString("TS_INDIC")));
                           bean.setBsaService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setBsaVariant(RutString.nullToStr(rs.getString("V_CODE")));
                           bean.setBsaPort(RutString.nullToStr(rs.getString("PORT_CODE")));
                           bean.setBsaGroup(RutString.nullToStr(rs.getString("GROUP_CODE")));                           
                           bean.setBsaPortGroupFlag(RutString.nullToStr(rs.getString("GROUP_FLAG")));  
                           bean.setBsaSubGroupFlag(RutString.nullToStr(rs.getString("SUB_GROUP_FLAG")));  
                           bean.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));  
                           return bean;
                       }
                   }
                );
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("T")) {
                sqlCriteria = "AND UPPER(TS_INDIC) " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND UPPER(SERVICE) " + sqlWild;
            } else if (search.equalsIgnoreCase("V")) {
                sqlCriteria = "AND UPPER(V_CODE) " + sqlWild;
            } else if (search.equalsIgnoreCase("P")) {
                sqlCriteria = "AND UPPER(PORT_CODE) " + sqlWild;
            } else if (search.equalsIgnoreCase("G")) {
                sqlCriteria = "AND UPPER(GROUP_CODE) " + sqlWild;
            }
        }
        return sqlCriteria;
    } 
    
    public List listForHelpScreenForVolumnPortPair(String find, String search, String wild, String status, String tsIndic, String ownService,int modelId, String ownProfRefNo, String tsPolPort,String tsPodPort) {
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreenForVolumnPortPair]: Started");
        String sqlCriteria = createSqlCriteriaForVolumnPortPair(find,search,wild);
        String port ="";
        //Check tsIndic if FROM select POL elseif TO select POD
        if(tsIndic.toUpperCase().equalsIgnoreCase("TO")){
            port = tsPodPort;
        }else{
            port = tsPolPort;
        }
        StringBuffer sql = new StringBuffer();
        /*
        sql.append("SELECT :tsIndic as TS_INDIC,SERVICE,PROFORMA_REFNO,PORT as PORT_CODE, ");
        sql.append("(SELECT B.VARIANT_CODE FROM BSA_SERVICE_VARIANT B, VSS_PROFORMA_MASTER C ");
        sql.append("     WHERE B.FK_PROFORMA_ID = C.PK_VSS_PROFORMA_ID ");
        sql.append("     AND C.FK_PROFORMA_REF_NO = PROFORMA_REFNO ");
        sql.append("     AND C.FK_SERVICE = SERVICE "); 
        sql.append("     AND EXISTS (SELECT 'Y' FROM BSA_BASE_ALLOCATION_MODEL WHERE MODEL_NAME = :modelName) ");
        sql.append("     AND ROWNUM = 1) AS VARIANT_CODE, ");
        sql.append("     :status as RECORD_STATUS ");
        sql.append("FROM (SELECT A.SPSRVC           AS SERVICE, ");
        sql.append("             A.SIMULATION_REFNO AS PROFORMA_REFNO, "); 
        sql.append("             A.SPPCAL           AS PORT "); 
        sql.append("             FROM ITP086 A "); 
        sql.append("             WHERE A.TRANSIT_FLAG = 'Y' "); 
        sql.append("             AND A.SPPCAL = :port "); 
        sql.append("             AND A.SPSRVC <> :ownService "); 
        sql.append("             AND A.SIMULATION_REFNO <> :ownProfRefNo "); 
        sql.append("             AND NVL(A.SPFORL, 'O') <> 'L'  "); 
        sql.append("             GROUP BY A.SPSRVC, A.SIMULATION_REFNO, A.SPPCAL) "); 
        sql.append("WHERE 1 = 1 "); 
        */
         sql.append("select * from ( ");
         sql.append("( SELECT :tsIndic as TS_INDIC,spsrvc as SERVICE,simulation_refno as PROFORMA_REFNO,sppcal as PORT_CODE, null as port_group_code, ");
         sql.append("       (SELECT B.VARIANT_CODE FROM BSA_SERVICE_VARIANT B, VSS_PROFORMA_MASTER C ");
         sql.append("           WHERE B.FK_PROFORMA_ID = C.PK_VSS_PROFORMA_ID ");
         sql.append("           AND C.FK_PROFORMA_REF_NO = simulation_refno ");
         sql.append("           AND C.FK_SERVICE = spsrvc "); 
         sql.append("           and b.fk_bsa_model_id = :modelId ");
         sql.append("           AND ROWNUM = 1) AS VARIANT_CODE, ");
         sql.append("           :status as RECORD_STATUS ");
         sql.append("           from ( select * FROM itp086 x  where SPRCST = 'A' AND X.SPSRVC not in (:ownService ,'DFS','AFS') AND NVL(X.SPFORL, 'O') <> 'L' ");
         sql.append("           AND exists (SELECT * FROM ITP085 WHERE SWSRVC = SPSRVC AND SWRCST = 'A') " );
         sql.append("           AND exists ");
         sql.append("        (SELECT A.SPSRVC    AS SERVICE, "); 
         sql.append("         A.SIMULATION_REFNO AS PROFORMA_REFNO, "); 
         sql.append("         A.SPPCAL           AS PORT "); 
         sql.append("         FROM ITP086 A "); 
         sql.append("         WHERE A.TRANSIT_FLAG = 'Y' "); 
         sql.append("         AND A.SPSRVC not in (:ownService ,'DFS','AFS') "); 
         sql.append("         AND A.SIMULATION_REFNO <> :ownProfRefNo "); 
         sql.append("         AND NVL(A.SPFORL, 'O') <> 'L'  "); 
         sql.append("         and sppcal = :port "); 
         sql.append("         and a.simulation_refno = x.simulation_refno  "); 
         sql.append("         GROUP BY A.SPSRVC, A.SIMULATION_REFNO, A.SPPCAL) "); 
         sql.append("         and sppcal <> :port) x "); 
         sql.append("         GROUP BY x.SPSRVC, x.SIMULATION_REFNO, x.SPPCAL ) "); 
         sql.append("    union ");
         sql.append(" ( select distinct * from ( ");
         sql.append("   SELECT :tsIndic as TS_INDIC, spsrvc as SERVICE, simulation_refno as PROFORMA_REFNO, null as PORT_CODE, ");
         sql.append("   (select distinct port_grp_cde from port_group_details d ");
         sql.append("     where exists (select fk_port_group_code from bsa_supported_port_group where fk_bsa_model_id = :modelId and fk_port_group_code = port_grp_cde and nvl(fk_service,spsrvc) = spsrvc) ");
         sql.append("   and port = sppcal ) as port_group_code, ");
         sql.append("      (SELECT B.VARIANT_CODE ");
         sql.append("       FROM BSA_SERVICE_VARIANT B, VSS_PROFORMA_MASTER C ");
         sql.append("       WHERE B.FK_PROFORMA_ID = C.PK_VSS_PROFORMA_ID ");
         sql.append("       AND C.FK_PROFORMA_REF_NO = simulation_refno ");
         sql.append("       AND C.FK_SERVICE = spsrvc ");
         sql.append("       and b.fk_bsa_model_id = :modelId ");
         sql.append("       AND ROWNUM = 1) AS VARIANT_CODE, ");
         sql.append("   :status as RECORD_STATUS ");
         sql.append("       from (select * FROM itp086 x where SPRCST = 'A' ");
         sql.append("       AND X.SPSRVC not in (:ownService, 'DFS', 'AFS') ");
         sql.append("       AND NVL(X.SPFORL, 'O') <> 'L'");
         sql.append("       AND exists (SELECT * FROM ITP085 WHERE SWSRVC = SPSRVC AND SWRCST = 'A') ");
         sql.append("       AND exists (SELECT A.SPSRVC AS SERVICE, ");
         sql.append("                   A.SIMULATION_REFNO AS PROFORMA_REFNO, ");
         sql.append("                   A.SPPCAL AS PORT ");
         sql.append("                   FROM ITP086 A WHERE A.TRANSIT_FLAG = 'Y' ");
         sql.append("                    AND A.SPSRVC not in (:ownService , 'DFS', 'AFS')");
         sql.append("                    AND A.SIMULATION_REFNO <> :ownProfRefNo ");
         sql.append("                    AND NVL(A.SPFORL, 'O') <> 'L' ");
         sql.append("                    and sppcal = :port ");
         sql.append("                    and a.simulation_refno = x.simulation_refno GROUP BY A.SPSRVC, A.SIMULATION_REFNO, A.SPPCAL ) ");
         sql.append("       and sppcal <> :port ) x");
         sql.append("     GROUP BY x.SPSRVC, x.SIMULATION_REFNO, x.SPPCAL");
         sql.append("   ) where port_group_code is not null ");
         sql.append(" ) ");         
         sql.append(" )WHERE 1 = 1 "); 
         
        if(sqlCriteria!=null&&!sqlCriteria.equals("")){
            sql.append(sqlCriteria);
        }
        HashMap map = new HashMap();       
        map.put("tsIndic",tsIndic.toUpperCase());
        map.put("modelId",new Integer(modelId));
        //map.put("modelName",modelName.toUpperCase());
        map.put("status",status);
        map.put("port",port.toUpperCase());
        map.put("ownService",ownService.toUpperCase());
        map.put("ownProfRefNo",ownProfRefNo.toUpperCase());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: tsIndic: " + tsIndic.toUpperCase());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: modelId: " + modelId);
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: status: " + status);
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: port: " + port.toUpperCase());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: ownService: " + ownService.toUpperCase());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: ownProfRefNo: " + ownProfRefNo.toUpperCase());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[BsaBsaPortPairTsJdbcDao][listForHelpScreen]: Finished");  
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaBsaPortPairTsMod2 bean = new BsaBsaPortPairTsMod2();
                           bean.setTsIndic(RutString.nullToStr(rs.getString("TS_INDIC")));
                           bean.setBsaService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setBsaVariant(RutString.nullToStr(rs.getString("VARIANT_CODE")));
                           bean.setBsaPort(RutString.nullToStr(rs.getString("PORT_CODE")));
                           bean.setBsaProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REFNO")));
                           bean.setBsaGroup(RutString.nullToStr(rs.getString("port_group_code")));                            
                           bean.setStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));  
                           return bean;
                       }
                   }
                );
    }    
    
    private String createSqlCriteriaForVolumnPortPair(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND UPPER(SERVICE) " + sqlWild;
            } else if (search.equalsIgnoreCase("R")) {
                sqlCriteria = "AND UPPER(PROFORMA_REFNO) " + sqlWild;
            } else if (search.equalsIgnoreCase("P")) {
                sqlCriteria = "AND UPPER(PORT_CODE) " + sqlWild;
            }  else if (search.equalsIgnoreCase("G")) {
                sqlCriteria = "AND UPPER(port_group_code) " + sqlWild;
            } 
        }
        return sqlCriteria;
    } 
}
