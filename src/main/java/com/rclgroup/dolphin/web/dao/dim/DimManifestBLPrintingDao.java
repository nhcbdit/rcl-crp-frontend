/*------------------------------------------------------
DimManifestBLPrintingDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
 Author Kitti Pongsirisakun 25/08/2010
- Change Log -------------------------------------------
## DD/MM/YY �User-      -TaskRef-           -ShortDescription-
01 21/06/12 NIP         PD_CR_20120425-01   DEX_DIM_Add function to select some BL
02 06/10/16 Sarawut A.                      Add new method for generate excel
03 10/08/17 Onsinee S.                      Add new method for generate text
04 27/06/18 Sarawut							Add search condition POL Terminal & coc/soc
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintContainerExcelMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintExcelMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintMod;
import com.rclgroup.dolphin.web.model.dim.DimManifestBLPrintTextMod; //##03

import java.util.List;
import java.util.Vector;
import org.springframework.dao.DataAccessException;

public interface DimManifestBLPrintingDao {

    
    /**
     * @param session
     * @param userName
     * @return
     * @throws DataAccessException
     */
    public Vector getListManifestPrint(String session, String userName) throws DataAccessException;
    
    /**
     * @param service   
     * @return vessel
     * @param voyage   
     * @return direction
     * @param pol   
     * @return pod
     * @param bl   
     * @param l_code   
     * @return r_code
     * @param a_code   
     * @return fsc_code
     * @return option
     * @param userName   
     * @return session
     * 
     * @throws DataAccessException
     */
    public String getSessionId(String service,  String vessel,      String voyage,  String direction, String pol,  String pod,  String bl,  
                               String l_code,   String r_code,      String a_code,  String fsc_code, 
                               String option,   String userName,    String session, String podTerminal, String cocSoc) throws DataAccessException;

    /**
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    public String getPort(String lineCode,String regionCode,String agentCode,String fscFromLogin) throws DataAccessException;

    /**
     * @param bean
     * @return
     * @throws DataAccessException
     */
    public String  insertSelectBl(DimManifestBLPrintMod bean) throws DataAccessException ;//##01

     /**
      * @param bean
      * @return
      * @throws DataAccessException
      */
     public String  clearPrintedBl(DimManifestBLPrintMod bean)throws DataAccessException ;//##01
     
     public String showHideGenExcelButton(String fsc) throws DataAccessException;//02
     public String showHideGenTextButton(String fsc) throws DataAccessException;//03
     public List<DimManifestBLPrintExcelMod> generateExcelHeader(String userCode,String sessionid,String selectBl) throws DataAccessException;//02
     public List<DimManifestBLPrintExcelMod> generateExcelDetail(String userCode,String sessionid,String selectBl) throws DataAccessException;//02
     public List<DimManifestBLPrintContainerExcelMod> generateExcelContainer(String selectBl) throws DataAccessException;//02
     public List<DimManifestBLPrintTextMod> generateTextDetail(String userCode,String sessionid,String selectBl) throws DataAccessException;//03
}

