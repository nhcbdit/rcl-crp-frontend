package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamGroupMandatoryMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface CamGroupMandatoryVasDao {

    public List<CamGroupMandatoryMod> getListGroupCode(String groupMandatoryCode,String mandatorySet,String parameterName,String status,String sortBy,String sortIn) throws DataAccessException;
    public List<CamGroupMandatoryMod> getList(String groupMandatoryCode) throws DataAccessException;
    public StringBuffer save(boolean isInsert, List<CamGroupMandatoryMod> newDataList, List<CamGroupMandatoryMod> updateList,String delIds) throws Exception;
    public String[] deleteByGroupCodes(String[] groupCodeArray) throws Exception;
}
