package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.dim.DimReeferCargoManifestMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class DimReeferCargoManifestJdbcDao  extends RrcStandardDao implements DimReeferCargoManifestDao{
    private GenerateTempBLProcedure generateTempBLProcedure;

    public DimReeferCargoManifestJdbcDao() {
    }
    
    
    protected void initDao() throws Exception {
        super.initDao();
       generateTempBLProcedure = new GenerateTempBLProcedure(getJdbcTemplate());
    }
    
    public boolean generateTempBL(RrcStandardMod mod) throws DataAccessException {
        return generateTempBLProcedure.generate(mod);
    }
    
    protected class GenerateTempBLProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_DIM_REEFER_CARGO_MNF.PRR_GEN_DIM_102_PRT";
        
        protected GenerateTempBLProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_from_date", Types.NUMERIC));
            declareParameter(new SqlParameter("p_to_date", Types.NUMERIC));
            declareParameter(new SqlParameter("p_coc_soc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_line", Types.VARCHAR));
            declareParameter(new SqlParameter("p_region", Types.VARCHAR));
            declareParameter(new SqlParameter("p_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_ter", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod_ter", Types.VARCHAR));
            declareParameter(new SqlParameter("p_bl", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dir", Types.VARCHAR));
            
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if ((inputMod instanceof DimReeferCargoManifestMod) && (outputMod instanceof DimReeferCargoManifestMod)) {
                Map inParameters = new HashMap();
                DimReeferCargoManifestMod aInputMod = (DimReeferCargoManifestMod) inputMod;
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                
                if(aInputMod.getFromDate()!=null & !aInputMod.getFromDate().equals("")){
                    String arrPeriod[] = aInputMod.getFromDate().split("/");
                    String p_period = arrPeriod[2]+arrPeriod[1]+arrPeriod[0];
                    inParameters.put("p_from_date", p_period);
                }else inParameters.put("p_from_date", null);
                
                if(aInputMod.getToDate()!=null & !aInputMod.getToDate().equals("")){
                    String arrPeriod[] = aInputMod.getToDate().split("/");
                    String p_period = arrPeriod[2]+arrPeriod[1]+arrPeriod[0];
                    inParameters.put("p_to_date", p_period);
                }else inParameters.put("p_to_date", null);
                
                inParameters.put("p_coc_soc", aInputMod.getCocSoc());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessle());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_line", aInputMod.getLine());
                inParameters.put("p_region", aInputMod.getRegion());
                inParameters.put("p_agent", aInputMod.getAgent());
                inParameters.put("p_fsc", aInputMod.getFsc());
                inParameters.put("p_pol", aInputMod.getPol());
                inParameters.put("p_pod", aInputMod.getPod());
                inParameters.put("p_pol_ter", aInputMod.getPolTer());
                inParameters.put("p_pod_ter", aInputMod.getPodTer());
                inParameters.put("p_bl", aInputMod.getBl());
                inParameters.put("p_dir", aInputMod.getDirection());
                
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_from_date = "+inParameters.get("p_from_date"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_to_date = "+inParameters.get("p_to_date"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_service = "+inParameters.get("p_service"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_line = "+inParameters.get("p_line"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_region = "+inParameters.get("p_region"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_agent = "+inParameters.get("p_agent"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_fsc = "+inParameters.get("p_fsc"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_coc_soc = "+inParameters.get("p_coc_soc"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_pol = "+inParameters.get("p_pol"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_pod = "+inParameters.get("p_pod"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_pol_ter = "+inParameters.get("p_pol_ter"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_pod_ter = "+inParameters.get("p_pod_ter"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_bl = "+inParameters.get("p_bl"));
                System.out.println("[DimReeferCargoManifestJdbcDao][GenerateTempBLProcedure]: p_dir = "+inParameters.get("p_dir"));
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
}
