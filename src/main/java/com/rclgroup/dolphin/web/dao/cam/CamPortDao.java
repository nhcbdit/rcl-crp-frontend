 /*------------------------------------------------------
 CamPortDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Manop Wanngam 10/10/2007   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamPortDao {
 
     public boolean isValid(String portCode) throws DataAccessException;
     
     public boolean isValidByFSC(String portCode, String fsc) throws DataAccessException;
     
     public boolean isValid(String portCode, String status) throws DataAccessException;
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;
     
     public List listForHelpScreenDisplayTerminal(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
     
     public List listForHelpScreen(String find, String search, String wild, String zone, String fsc, String status) throws DataAccessException;    
    
     public boolean isValidTerminal(String terminalCode) throws DataAccessException;
 }
