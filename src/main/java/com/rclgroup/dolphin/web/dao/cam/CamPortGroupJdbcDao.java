/*------------------------------------------------------
CamPortGroupJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
 Author Sopon Dee-udomvongsa 02/09/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
   25/03/09 PIE              Add listSupportedPortGroupForHelpScreen
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.cam;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamPortGroupMod;
import com.rclgroup.dolphin.web.util.RutString;


public class CamPortGroupJdbcDao  extends RrcStandardDao implements CamPortGroupDao {
   
    public CamPortGroupJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String portGroupCode) throws DataAccessException {
        System.out.println("[CamPortGroupJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_GROUP_CODE ");
        sql.append("FROM VR_CAM_PORT_GROUP ");
        sql.append("WHERE PORT_GROUP_CODE = :portGroupCode");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("portGroupCode", portGroupCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[CamPortGroupJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValid(String portGroupCode, String status) throws DataAccessException {
        System.out.println("[CamPortGroupJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_GROUP_CODE ");
        sql.append("FROM VR_CAM_PORT_GROUP ");
        sql.append("WHERE PORT_GROUP_CODE = :portGroupCode ");
        sql.append("AND PORT_GROUP_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("portGroupCode",portGroupCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[CamPortGroupJdbcDao][isValid]: With status: Finished");
        return isValid;
    }  
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[CamPortGroupJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_GROUP_CODE ");
        sql.append("      ,PORT_GROUP_NAME ");   
        sql.append("      ,COUNTRY_CODE ");
        sql.append("      ,PORT_GROUP_STATUS "); 
        sql.append("FROM VR_CAM_PORT_GROUP ");
        sql.append(sqlCriteria);  
        System.out.println("[CamPortGroupJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[CamPortGroupJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());   
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "WHERE PORT_GROUP_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "WHERE PORT_GROUP_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("Co")){
                sqlCriteria = "WHERE COUNTRY_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE PORT_GROUP_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }   
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[CamPortGroupJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_GROUP_CODE ");
        sql.append("      ,PORT_GROUP_NAME ");
        sql.append("      ,COUNTRY_CODE ");
        sql.append("      ,PORT_GROUP_STATUS "); 
        sql.append("FROM VR_CAM_PORT_GROUP ");
        sql.append("WHERE PORT_GROUP_STATUS = :status ");
        sql.append(sqlCriteria);
        System.out.println("[CamPortGroupJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
        
        HashMap map = new HashMap();
        map.put("status",status);
        System.out.println("[CamPortGroupJdbcDao][listForHelpScreen]: Finished");
                 return getNamedParameterJdbcTemplate().query(
                            sql.toString(),
                            map,
                            new RowModMapper());  
    }
    
    public List listSupportedPortGroupForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[CamPortGroupJdbcDao][listSupportedPortGroupForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatusForSupportedPortGroup(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_GROUP_CODE ");
        sql.append("      ,DESCRIPTION ");
        sql.append("      ,COUNTRY_CODE "); 
        sql.append("      ,STATUS ");     
        sql.append("FROM VR_CTF_PG_SOC_COC_SAME_PORT ");
        sql.append("WHERE STATUS = :status ");   
        sql.append(sqlCriteria);
        sql.append("ORDER BY PORT_GROUP_CODE ");
        
        System.out.println("[CamPortGroupJdbcDao][listSupportedPortGroupForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[CamPortGroupJdbcDao][listSupportedPortGroupForHelpScreen]: status: "+status);
        
        HashMap map = new HashMap();
        map.put("status",status);
        System.out.println("[CamPortGroupJdbcDao][listSupportedPortGroupForHelpScreen]: Finished");
                 return getNamedParameterJdbcTemplate().query(
                            sql.toString(),
                            map,
                            new RowMapper(){
                                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                                    CamPortGroupMod portGroup = new CamPortGroupMod();
                                    portGroup.setPortGroupCode(RutString.nullToStr(rs.getString("PORT_GROUP_CODE")));   
                                    portGroup.setPortGroupName(RutString.nullToStr(rs.getString("DESCRIPTION")));        
                                    portGroup.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
                                    portGroup.setPortGroupStatus(RutString.nullToStr(rs.getString("STATUS")));
                                    return portGroup;
                                }
                            });
    }
    
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND PORT_GROUP_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "AND PORT_GROUP_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("Co")){
                sqlCriteria = "AND COUNTRY_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND PORT_GROUP_STATUS " + sqlWild;
            }
               
        }
        return sqlCriteria;
    }
    //  Begin
    //  Add method for listSupportedPortGroupForHelpScreen , Panadda.P, 21/05/2012
    private String createSqlCriteriaWithStatusForSupportedPortGroup(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
                if(search.equalsIgnoreCase("C")){
                    sqlCriteria = "AND PORT_GROUP_CODE " + sqlWild;
                }else if(search.equalsIgnoreCase("N")){
                    sqlCriteria = "AND DESCRIPTION " + sqlWild;
                }else if(search.equalsIgnoreCase("Co")){
                    sqlCriteria = "AND COUNTRY_CODE " + sqlWild;
                }else if(search.equalsIgnoreCase("S")){
                    sqlCriteria = "AND STATUS " + sqlWild;
                }             
        }
        return sqlCriteria;
    }
    // End
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private CamPortGroupMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        CamPortGroupMod portGroup = new CamPortGroupMod();
        portGroup.setPortGroupCode(RutString.nullToStr(rs.getString("PORT_GROUP_CODE")));   
        portGroup.setPortGroupName(RutString.nullToStr(rs.getString("PORT_GROUP_NAME")));        
        portGroup.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
        portGroup.setPortGroupStatus(RutString.nullToStr(rs.getString("PORT_GROUP_STATUS")));

        return portGroup;
    }       
}
