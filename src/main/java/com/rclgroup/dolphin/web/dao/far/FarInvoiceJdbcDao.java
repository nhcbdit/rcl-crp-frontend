/*--------------------------------------------------------
FarInvoiceJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author -
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
 01  10/11/09       KIT      -355-         Few incorrect data on Philips Template
 02  09/02/12       SON      53,081        Tunning query in listForExistingInvoiceItemByBillNo
 03  08/09/14       BHAMOH1   382          Add Invoice print date as date type filter in countListForSearchScreenInvItemByInvDetail, listForSearchScreenInvItemByInvDetail 
                                            For 'Invoice date' filter change the filter from RECORD_ADD_DATE to INVOICE_DATE
 04  22/09/14       BHAMOH1   382          Add INVOICE_DATE,INVOICE_PRINTING_DATE as select columns in 
                                            countListForSearchScreenInvItemByInvHeader,listForSearchScreenInvItemByInvHeader, countListForSearchScreenInvItemByInvDetail, listForSearchScreenInvItemByInvDetail
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.far.FarInvoiceDao;
import com.rclgroup.dolphin.web.model.far.FarBillingStatementGenerateMod;
import com.rclgroup.dolphin.web.model.far.FarInvoiceMod;
import com.rclgroup.dolphin.web.model.far.FarInvoicePhilipMod;
import com.rclgroup.dolphin.web.model.far.FarPoMod;
import com.rclgroup.dolphin.web.model.far.FarReportTemplateMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class FarInvoiceJdbcDao extends RrcStandardDao implements FarInvoiceDao{
    private GenBillStatementStoreProcedure genBillStatementStoreProcedure;
    private GenBillNoStoreProcedure genBillNoStoreProcedure;

    public FarInvoiceJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        genBillStatementStoreProcedure = new GenBillStatementStoreProcedure(getJdbcTemplate());
        genBillNoStoreProcedure = new GenBillNoStoreProcedure(getJdbcTemplate());
    }
    
    protected class GenBillStatementStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_SPECIAL_BILLING.PRR_GEN_BILLING_STATEMENT";
        
        protected GenBillStatementStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_batch_number", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_template", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_bill_to_party", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_bill_no", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_bill_version", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bill_date", Types.DATE));
            declareParameter(new SqlInOutParameter("p_bill_date_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_bill_date_to", Types.DATE));    
            declareParameter(new SqlInOutParameter("p_attn", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_description", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_fsc", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_invoice_selected_str", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_ib_ob_xt", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_current_number", Types.INTEGER));
            compile();
        }
        
        protected FarBillingStatementGenerateMod generateBillStatement(String batchNumber, String template, String port, String billToParty, String billNo, String billVersion, String billDate, String billDateFrom, String billDateTo, String attn, String description, String fsc, String invoiceSelectedStr, String recordAddUser, String ibObXt, int currentNumber) {
            FarBillingStatementGenerateMod bean = new FarBillingStatementGenerateMod();
            Map map = new HashMap();
            map.put("p_batch_number", RutDatabase.integerToDb(batchNumber));
            map.put("p_template", RutDatabase.stringToDb(template));
            map.put("p_port", RutDatabase.stringToDb(port));
            map.put("p_bill_to_party", RutDatabase.stringToDb(billToParty));
            map.put("p_bill_no", RutDatabase.stringToDb(billNo));
            map.put("p_bill_version", RutDatabase.integerToDb(billVersion));
            map.put("p_bill_date", RutDatabase.dateToDb(billDate));
            map.put("p_bill_date_from", RutDatabase.dateToDb(billDateFrom));
            map.put("p_bill_date_to", RutDatabase.dateToDb(billDateTo));
            map.put("p_attn", RutDatabase.stringToDb(attn));
            map.put("p_description", RutDatabase.stringToDb(description));
            map.put("p_fsc", RutDatabase.stringToDb(fsc));
            map.put("p_invoice_selected_str", RutDatabase.stringToDb(invoiceSelectedStr));
            map.put("p_record_add_user", RutDatabase.stringToDb(recordAddUser));
            map.put("p_ib_ob_xt", RutDatabase.stringToDb(ibObXt));
            map.put("p_current_number", RutDatabase.integerToDb(String.valueOf(currentNumber)));
            
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_batch_number = "+map.get("p_batch_number"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_template = "+map.get("p_template"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_port = "+map.get("p_port"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_bill_to_party = "+map.get("p_bill_to_party"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_bill_no = "+map.get("p_bill_no"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_bill_version = "+map.get("p_bill_version"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_bill_date = "+map.get("p_bill_date"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_bill_date_from = "+map.get("p_bill_date_from"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_bill_date_to = "+map.get("p_bill_date_to"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_attn = "+map.get("p_attn"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_description = "+map.get("p_description"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_fsc = "+map.get("p_fsc"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_invoice_selected_str = "+map.get("p_invoice_selected_str"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_record_add_user = "+map.get("p_record_add_user"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_ib_ob_xt = "+map.get("p_ib_ob_xt"));
            System.out.println("[FarInvoiceJdbcDao][GenBillStatementStoreProcedure][generateBillStatement]: p_current_number = "+map.get("p_current_number"));
            
            Map outMap = execute(map);
            if (outMap.size() > 0) {
                bean.setBatchNumber(new Long(RutDatabase.dbToInteger(outMap, "p_batch_number")).longValue());
                bean.setBillNo(RutDatabase.dbToString(outMap, "p_bill_no"));
                bean.setBillVersion(new Integer(RutDatabase.dbToInteger(outMap, "p_bill_version")).intValue());
                bean.setBillNoCurrent(new Integer(RutDatabase.dbToInteger(outMap, "p_current_number")).intValue());
            }    
            return bean;
        }
        
    }
    
    protected class GenBillNoStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_SPECIAL_BILLING.PRR_GEN_NEXT_STATEMENT_NUMBER";
        
        protected GenBillNoStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_template", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_inb_oub_xt", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_bill_no", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_current_number", Types.INTEGER));
            compile();
        }
        
        protected FarBillingStatementGenerateMod generateBillNumber(String template, String ibObXt, String billNumber, int currentNumber) {
            FarBillingStatementGenerateMod bean = new FarBillingStatementGenerateMod();
            Map map = new HashMap();
            map.put("p_template", RutDatabase.stringToDb(template));
            map.put("p_inb_oub_xt", RutDatabase.stringToDb(ibObXt));
            map.put("p_bill_no", RutDatabase.stringToDb(billNumber));
            map.put("p_current_number", RutDatabase.integerToDb(String.valueOf(currentNumber)));
            
            System.out.println("[FarInvoiceJdbcDao][GenBillNoStoreProcedure][generateBillNumber]: p_template = "+map.get("p_template"));
            System.out.println("[FarInvoiceJdbcDao][GenBillNoStoreProcedure][generateBillNumber]: p_inb_oub_xt = "+map.get("p_inb_oub_xt"));
            System.out.println("[FarInvoiceJdbcDao][GenBillNoStoreProcedure][generateBillNumber]: p_bill_no = "+map.get("p_bill_no"));
            System.out.println("[FarInvoiceJdbcDao][GenBillNoStoreProcedure][generateBillNumber]: p_current_number = "+map.get("p_current_number"));
            
            Map outMap = execute(map);
            if (outMap.size() > 0) {
                bean.setBillNo(RutDatabase.dbToString(outMap, "p_bill_no"));
                bean.setBillNoCurrent(new Integer(RutDatabase.dbToInteger(outMap, "p_current_number")).intValue());
            }    
            return bean;
        }
        
    }
    
    public FarBillingStatementGenerateMod generateBillingStatement(String batchNumber, String template, String port, String billToParty, String billNo, String billVersion, String billDate, String billDateFrom, String billDateTo, String attn, String description, String fsc, String invoiceSelectedStr, String recordAddUser, String ibObXt, int currentNumber) {
        System.out.println("[FarInvoiceJdbcDao][generateBillingStatement]: Started");
        
        // Call genBillStatementStoreProcedure for generate Billing Statement.
        FarBillingStatementGenerateMod bean = genBillStatementStoreProcedure.generateBillStatement(batchNumber
            ,template
            ,port
            ,billToParty
            ,billNo
            ,billVersion
            ,billDate
            ,billDateFrom
            ,billDateTo
            ,attn
            ,description
            ,fsc
            ,invoiceSelectedStr
            ,recordAddUser
            ,ibObXt
            ,currentNumber);
            
        System.out.println("[FarInvoiceJdbcDao][generateBillingStatement]: batchNumber = "+bean.getBatchNumber());
        System.out.println("[FarInvoiceJdbcDao][generateBillingStatement]: billNo = "+bean.getBillNo());
        System.out.println("[FarInvoiceJdbcDao][generateBillingStatement]: billVersion = "+bean.getBillVersion());
        System.out.println("[FarInvoiceJdbcDao][generateBillingStatement]: Finished");
        return bean;
    }
    
    public FarBillingStatementGenerateMod generateNextBillNumber(String template, String inbOubXT) {
        System.out.println("[FarInvoiceJdbcDao][generateNextBillNumber]: Started");
        
        // Call genBillNoStoreProcedure for generate Bill No.
        FarBillingStatementGenerateMod bean = genBillNoStoreProcedure.generateBillNumber(template, inbOubXT, "", 0);
            
        System.out.println("[FarInvoiceJdbcDao][generateNextBillNumber]: billNo = "+bean.getBillNo());
        System.out.println("[FarInvoiceJdbcDao][generateNextBillNumber]: billNoCurrent = "+bean.getBillNoCurrent());
        System.out.println("[FarInvoiceJdbcDao][generateNextBillNumber]: Finished");
        return bean;
    }
    
    public boolean isValid(String invNo) throws DataAccessException {
        System.out.println("[FarInvoiceJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
         
        StringBuffer sql = new StringBuffer();
        
        sql.append("SELECT INVOICE_NO");        
        sql.append(" FROM VR_FAR_SPECIAL_BILLING");
        sql.append(" WHERE  1=1       ");  
        if((invNo!=null)&&(!invNo.trim().equals(""))){
            sql.append("  AND UPPER(INVOICE_NO) = :invNo ");
        }        
        HashMap map = new HashMap();        
        map.put("invNo",invNo.toUpperCase());
        System.out.println("[FarInvoiceJdbcDao][isValid]: SQL: " + sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[FarInvoiceJdbcDao][isValid]: With status: Finished");
        return isValid;
    }
    
    public boolean isValidByFsc(String invNo,String line, String trade, String agent, String fsc) throws DataAccessException {
        System.out.println("[FarInvoiceJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
         
        StringBuffer sql = new StringBuffer();
        
        sql.append("SELECT INVOICE_NO");        
        sql.append(" FROM VR_FAR_SPECIAL_BILLING");
        sql.append(" WHERE   1=1      ");      
        sql.append(" AND UPPER(INVOICE_NO) = :invNo ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:trade,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        
                
        HashMap map = new HashMap();        
        map.put("invNo",invNo.toUpperCase());
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
        System.out.println("[FarInvoiceJdbcDao][isValid]: SQL: " + sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[FarInvoiceJdbcDao][isValid]: With status: Finished");
        return isValid;
    }
    
    
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("F")){
                sqlCriteria = "AND UPPER(COLL_FSC) " + sqlWild;
            }else if(search.equalsIgnoreCase("I")){
                sqlCriteria = "AND UPPER(INVOICE_NO) " + sqlWild;            
            }else if(search.equalsIgnoreCase("B")){
                sqlCriteria = "AND UPPER(BILL_TO_PARTY) " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND UPPER(INVOICE_STATUS) " + sqlWild;
            }        
        }
        return sqlCriteria;
    }
    
    private String getRealFsc(String line, String trade, String agent, String fsc) {
        String realFsc = "";
        if (fsc!= null && !fsc.equals("")){
            realFsc = fsc;
        }else if (agent!= null && !agent.equals("")){
            realFsc = agent;
        }else if (trade!= null && !trade.equals("")){
            realFsc = line+trade;
        }else {
            realFsc = line;
        }        
        return realFsc;
    }
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS");        
        sql.append(" FROM VR_FAR_SPECIAL_BILLING");  
        sql.append(" WHERE   1=1      ");  
        sql.append(sqlCriteria);
        HashMap map = new HashMap();                
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarInvoiceMod inv = new FarInvoiceMod();
                inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                inv.setInvNo(RutString.nullToStr(rs.getString("BABKDT")));                      
                inv.setBillToParty(RutString.nullToStr(rs.getString("BABKDT")));                      
                inv.setStatus(RutString.nullToStr(rs.getString("BABKDT")));                                      
                return inv;
            }
        });
    }
    
    public List listForHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        String realFsc = getRealFsc(line,trade,agent,fsc);
        sql.append("SELECT COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS");        
        sql.append(" FROM VR_FAR_SPECIAL_BILLING");  
        sql.append(" WHERE   1=1      ");  
        sql.append(sqlCriteria);        
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:trade,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS IN ('P') ");
        sql.append(" ORDER BY COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS");
           
        System.out.println("line = "+line+"trade = "+trade+"agent = "+agent+"fsc = "+fsc+"realFsc = "+realFsc);
        HashMap map = new HashMap();                
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
//        map.put("fsc",realFsc);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarInvoiceMod inv = new FarInvoiceMod();
                inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                inv.setStatus(RutString.nullToStr(rs.getString("INVOICE_STATUS")));
                return inv;
            }
        });
    }
    
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        String selectColumn = "COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS";        
        String realFsc = getRealFsc(line,trade,agent,fsc);
        sql.append("SELECT "+selectColumn);        
        sql.append(" FROM VR_FAR_SPECIAL_BILLING");  
        sql.append(" WHERE   1=1      ");  
        sql.append(sqlCriteria);        
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:trade,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS IN ('P') ");
        sql.append(" ORDER BY COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS");        
        sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);
        
        System.out.println("line = "+line+"trade = "+trade+"agent = "+agent+"fsc = "+fsc+"realFsc = "+realFsc);
        HashMap map = new HashMap();                
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
    //        map.put("fsc",realFsc);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarInvoiceMod inv = new FarInvoiceMod();
                inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                inv.setStatus(RutString.nullToStr(rs.getString("INVOICE_STATUS")));
                return inv;
            }
        });
    }
    
    public int countListForNewHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        String realFsc = getRealFsc(line,trade,agent,fsc);
        sql.append("SELECT COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS");        
        sql.append(" FROM VR_FAR_SPECIAL_BILLING");  
        sql.append(" WHERE   1=1      ");  
        sql.append(sqlCriteria);        
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:trade,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS IN ('P') ");
        sql.append(" ORDER BY COLL_FSC, INVOICE_NO, BILL_TO_PARTY, INVOICE_STATUS");
        sql = getNumberOfAllData(sql);
        System.out.println("line = "+line+"trade = "+trade+"agent = "+agent+"fsc = "+fsc+"realFsc = "+realFsc);
        HashMap map = new HashMap();                
        map.put("line",line);
        map.put("trade",trade);
        map.put("agent",agent);
        map.put("fsc",fsc);
    //        map.put("fsc",realFsc);
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map , Integer.class);
    }
    
    public List listTemplateByInvNo(String invNo) {
        System.out.println("[CrmCustomerDao][isValid]: Started");        
        Vector templateVec = new Vector();
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT CUST_INV_TEXT, CUST_INV_CODE,REPORT_FILENAME,INVOICE_NO ");
        sql.append("FROM VR_FAR_GEN_TEMPLATE ");
        sql.append("WHERE UPPER(INVOICE_NO) = :invNo");        
        HashMap map = new HashMap();                
        map.put("invNo",invNo.toUpperCase());        
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarReportTemplateMod inv = new FarReportTemplateMod();
                inv.setTemplateName(RutString.nullToStr(rs.getString("CUST_INV_TEXT")));
                inv.setCustInvCode(RutString.nullToStr(rs.getString("CUST_INV_CODE")));                      
                inv.setReportName(RutString.nullToStr(rs.getString("REPORT_FILENAME")));                      
                inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));
                return inv;
            }
        });
    }
    public List listForSearchScreenByInvHeader(String invDateFrom,String invDateTo
                                              ,String invNumFrom,String invNumTo
                                              ,String remark,String status
                                              ,String line,String region
                                              ,String agent,String fsc
                                              ,String sortBy,String sortByIn) throws DataAccessException{
        
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenByInvHeader]:  Started");
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        Map columMap = new HashMap();
        columMap.put("COLL_FSC","COLL_FSC");
        columMap.put("BILL_TO_PARTY","BILL_TO_PARTY");
        columMap.put("BILL_TO_PARTY_NAME","BILL_TO_PARTY_NAME");
        columMap.put("INVOICE_NO","INVOICE_NO");
        columMap.put("INVOICE_DATE","INVOICE_DATE");
        columMap.put("REMARKS","REMARKS");
        columMap.put("INVOICE_STATUS","INVOICE_STATUS");
        
        sql.append("SELECT DISTINCT COLL_FSC ");
        sql.append("       ,BILL_TO_PARTY ");
        sql.append("       ,BILL_TO_PARTY_NAME ");
        sql.append("       ,INVOICE_NO ");
        sql.append("       ,INVOICE_DATE ");
        sql.append("       ,REMARKS ");
        sql.append("       ,INVOICE_STATUS ");
        sql.append("FROM VR_FAR_INVOICE_BY_HEADER ");
        sql.append("WHERE 1=1  ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:region,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS <> 'D' ");        
        
        if (invDateFrom != null && !invDateFrom.equals("")) {
            sql.append("AND INVOICE_DATE >="+ Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(invDateFrom))))+" ");
        }
        if (invDateTo != null && !invDateTo.equals("")){
            sql.append("AND INVOICE_DATE <=" + Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(invDateTo)))) + " ");
        }
        if (invNumFrom != null && !invNumFrom.equals("")) {
            sql.append("AND INVOICE_NO >= :invNumFrom  ");
        }
        if (invNumTo != null && !invNumTo.equals("")){
            sql.append("AND INVOICE_NO <= :invNumTo  ");
        }
        if (remark != null && !remark.equals("")){
            sql.append("AND REMARKS LIKE '%"+remark+"%'  ");
        }
        if (status != null && !status.equals("")){
            sql.append("AND INVOICE_STATUS =  :status  ");
        }
//        if (!fsc.equals("")){
//            sql.append("AND COLL_FSC= :fsc ");
//        }
        if (sortBy != null && !sortBy.equals("")){
            sql.append("ORDER BY "+(String)columMap.get(sortBy));
            if (sortByIn.equals("A")) {
                sql.append("  ASC");
            }else{sql.append("  DESC");}
        }

        
        map.put("invDateFrom",invDateFrom);
        map.put("invDateTo",invDateTo);
        map.put("invNumFrom",invNumFrom);
        map.put("invNumTo",invNumTo);
        map.put("remark",remark);
        map.put("status",status);
        map.put("sortBy",sortBy);
        map.put("sortByIn",sortByIn);
        map.put("fsc",fsc);
        map.put("line",line);
        map.put("region",region);
        map.put("agent",agent);
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invDateFrom :" + invDateFrom );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invDateTo :" + invDateTo );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invNumFrom :" + invNumFrom );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invNumTo :" + invNumTo );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : remark :" + remark );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : status :" + status );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : sortBy :" + sortBy );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : sortByIn :" + sortByIn );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : fsc :" + fsc );
        
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenByInvHeader]: SQL: " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenByInvHeader]:  Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarInvoiceMod inv = new FarInvoiceMod();
                inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                inv.setStatus(RutString.nullToStr(rs.getString("INVOICE_STATUS")));
                inv.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                inv.setInvDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));
                inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                return inv;
            }
        });
        
    }
    public List listV02ForSearchScreenByInvHeader(int rowAt,int rowTo
                                              ,String invDateFrom,String invDateTo
                                              ,String invNumFrom,String invNumTo
                                              ,String remark,String status
                                              ,String line,String region
                                              ,String agent,String fsc
                                              ,String sortBy,String sortByIn) throws DataAccessException{

    
        System.out.println("[FarInvoiceJdbcDao][ListV02ForSearchScreenByInvHeader]:  Started");

        String selectColumn = "COLL_FSC,BILL_TO_PARTY,BILL_TO_PARTY_NAME,INVOICE_NO,INVOICE_DATE,REMARKS,INVOICE_STATUS";
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        Map columMap = new HashMap();
        columMap.put("COLL_FSC","COLL_FSC");
        columMap.put("BILL_TO_PARTY","BILL_TO_PARTY");
        columMap.put("BILL_TO_PARTY_NAME","BILL_TO_PARTY_NAME");
        columMap.put("INVOICE_NO","INVOICE_NO");
        columMap.put("INVOICE_DATE","INVOICE_DATE");
        columMap.put("REMARKS","REMARKS");
        columMap.put("INVOICE_STATUS","INVOICE_STATUS");
        
        sql.append("SELECT DISTINCT COLL_FSC ");
        sql.append("       ,BILL_TO_PARTY ");
        sql.append("       ,BILL_TO_PARTY_NAME ");
        sql.append("       ,INVOICE_NO ");
        sql.append("       ,INVOICE_DATE ");
        sql.append("       ,REMARKS ");
        sql.append("       ,INVOICE_STATUS ");
        sql.append("FROM VR_FAR_INVOICE_BY_HEADER ");
        sql.append("WHERE 1=1  ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:region,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS <> 'D' ");        
        
        if (invDateFrom != null && !invDateFrom.equals("")) {
            sql.append("AND INVOICE_DATE >="+ Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(invDateFrom))))+" ");
        }
        if (invDateTo != null && !invDateTo.equals("")){
            sql.append("AND INVOICE_DATE <=" + Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(invDateTo)))) + " ");
        }
        if (invNumFrom != null && !invNumFrom.equals("")) {
            sql.append("AND INVOICE_NO >= :invNumFrom  ");
        }
        if (invNumTo != null && !invNumTo.equals("")){
            sql.append("AND INVOICE_NO <= :invNumTo  ");
        }
        if (remark != null && !remark.equals("")){
            sql.append("AND REMARKS LIKE '%"+remark+"%'  ");
        }
        if (status != null && !status.equals("")){
            sql.append("AND INVOICE_STATUS =  :status  ");
        }
        //        if (!fsc.equals("")){
        //            sql.append("AND COLL_FSC= :fsc ");
        //        }
        if (sortBy != null && !sortBy.equals("")){
            sql.append("ORDER BY "+(String)columMap.get(sortBy));
            if (sortByIn.equals("A")) {
                sql.append("  ASC");
            }else{sql.append("  DESC");}
        }
        map.put("invDateFrom",invDateFrom);
        map.put("invDateTo",invDateTo);
        map.put("invNumFrom",invNumFrom);
        map.put("invNumTo",invNumTo);
        map.put("remark",remark);
        map.put("status",status);
        map.put("sortBy",sortBy);
        map.put("sortByIn",sortByIn);
        map.put("fsc",fsc);
        map.put("line",line);
        map.put("region",region);
        map.put("agent",agent);
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invDateFrom :" + invDateFrom );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invDateTo :" + invDateTo );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invNumFrom :" + invNumFrom );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invNumTo :" + invNumTo );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : remark :" + remark );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : status :" + status );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : sortBy :" + sortBy );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : sortByIn :" + sortByIn );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : fsc :" + fsc );

        sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);
        System.out.println("[FarInvoiceJdbcDao][ListV02ForSearchScreenByInvHeader]: SQL: " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][ListV02ForSearchScreenByInvHeader]:  Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            FarInvoiceMod inv = new FarInvoiceMod();
                            inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                            inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                            inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                            inv.setStatus(RutString.nullToStr(rs.getString("INVOICE_STATUS")));
                            inv.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                            inv.setInvDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));
                            inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                            return inv;
                        }
                    });
        
    }
    
    public int countListForSearchScreenByInvHeader(String invDateFrom,String invDateTo
                                              ,String invNumFrom,String invNumTo
                                              ,String remark,String status
                                              ,String line,String region
                                              ,String agent,String fsc
                                              ,String sortBy,String sortByIn) throws DataAccessException{
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenByInvHeader]:  Started");       
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();
        Map columMap = new HashMap();
        columMap.put("COLL_FSC","COLL_FSC");
        columMap.put("BILL_TO_PARTY","BILL_TO_PARTY");
        columMap.put("BILL_TO_PARTY_NAME","BILL_TO_PARTY_NAME");
        columMap.put("INVOICE_NO","INVOICE_NO");
        columMap.put("INVOICE_DATE","INVOICE_DATE");
        columMap.put("REMARKS","REMARKS");
        columMap.put("INVOICE_STATUS","INVOICE_STATUS");
        
        sql.append("SELECT DISTINCT COLL_FSC ");
        sql.append("       ,BILL_TO_PARTY ");
        sql.append("       ,BILL_TO_PARTY_NAME ");
        sql.append("       ,INVOICE_NO ");
        sql.append("       ,INVOICE_DATE ");
        sql.append("       ,REMARKS ");
        sql.append("       ,INVOICE_STATUS ");
        sql.append("FROM VR_FAR_INVOICE_BY_HEADER ");
        sql.append("WHERE 1=1  ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:region,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS <> 'D' ");
        
        if (invDateFrom != null && !invDateFrom.equals("")) {
            sql.append("AND INVOICE_DATE >="+ Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(invDateFrom))))+" ");
        }
        if (invDateTo != null && !invDateTo.equals("")){
            sql.append("AND INVOICE_DATE <=" + Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(invDateTo)))) + " ");
        }
        if (invNumFrom != null && !invNumFrom.equals("")) {
            sql.append("AND INVOICE_NO >= :invNumFrom  ");
        }
        if (invNumTo != null && !invNumTo.equals("")){
            sql.append("AND INVOICE_NO <= :invNumTo  ");
        }
        if (remark != null && !remark.equals("")){
            sql.append("AND REMARKS LIKE '%"+remark+"%'  ");
        }
        if (status != null && !status.equals("")){
            sql.append("AND INVOICE_STATUS =  :status  ");
        }
        //        if (!fsc.equals("")){
        //            sql.append("AND COLL_FSC= :fsc ");
        //        }
        if (sortBy != null && !sortBy.equals("")){
            sql.append("ORDER BY "+(String)columMap.get(sortBy));
            if (sortByIn.equals("A")) {
                sql.append("  ASC");
            }else{sql.append("  DESC");}
        }

        
        map.put("invDateFrom",invDateFrom);
        map.put("invDateTo",invDateTo);
        map.put("invNumFrom",invNumFrom);
        map.put("invNumTo",invNumTo);
        map.put("remark",remark);
        map.put("status",status);
        map.put("sortBy",sortBy);
        map.put("sortByIn",sortByIn);
        map.put("fsc",fsc);
        map.put("line",line);
        map.put("region",region);
        map.put("agent",agent);
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invDateFrom :" + invDateFrom );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invDateTo :" + invDateTo );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invNumFrom :" + invNumFrom );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : invNumTo :" + invNumTo );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : remark :" + remark );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : status :" + status );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : sortBy :" + sortBy );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : sortByIn :" + sortByIn );
        System.out.println("[FarInvoiceJdbcDao][Criteria] : fsc :" + fsc );
        sql = getNumberOfAllData(sql);
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]: SQL: " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]:  Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map , Integer.class);        
    }
        
    public List listForSearchScreenByInvDetail(String billToParty,String dateType
                                                ,String fromDate,String toDate
                                                ,String service,String vessel
                                                ,String voyage,String port
                                                ,String inOutXT,String blNo
                                                ,String bkgNo,String equipNo
                                                ,String line,String region
                                                ,String agent
                                                ,String fsc, String status
                                                ,String sortBy,String sortByIn) throws DataAccessException{
        
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenByInvDetail]:  Started");
        StringBuffer sql = new StringBuffer();
        Map columMap = new HashMap();
        columMap.put("COLL_FSC","COLL_FSC");
        columMap.put("BILL_TO_PARTY","BILL_TO_PARTY");
        columMap.put("BILL_TO_PARTY_NAME","BILL_TO_PARTY_NAME");
        columMap.put("INVOICE_NO","INVOICE_NO");
        columMap.put("INVOICE_DATE","INVOICE_DATE");
        columMap.put("REMARKS","REMARKS");
        columMap.put("INVOICE_STATUS","INVOICE_STATUS");

        sql.append("SELECT DISTINCT COLL_FSC ");
        sql.append("       ,BILL_TO_PARTY ");
        sql.append("       ,BILL_TO_PARTY_NAME ");
        sql.append("       ,INVOICE_NO ");
        sql.append("       ,INVOICE_DATE ");
        sql.append("       ,REMARKS ");
        sql.append("       ,INVOICE_STATUS ");
        sql.append("FROM  VR_FAR_INVOICE_BY_DETAIL  ");
        sql.append("WHERE 1=1  ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:region,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS <> 'D' ");
        
        if (billToParty != null && !billToParty.equals("")) {
            sql.append("AND BILL_TO_PARTY = :billToParty  ");
        }
        if (service != null && !service.equals("")){
            sql.append("AND SERVICE = :service  ");
        }
        if (vessel != null && !vessel.equals("")) {
            sql.append("AND VESSEL = :vessel  ");
        }
        if (voyage != null && !voyage.equals("")){
            sql.append("AND VOYAGE = :voyage  ");
        }
        if (blNo != null && !blNo.equals("")){
            sql.append("AND APP_REFERENCE = :blNo  ");
        }
        if (bkgNo != null && !bkgNo.equals("")){
            sql.append("AND BOOKING_NO =  :bkgNo  ");
        }
        if (equipNo != null && !equipNo.equals("")){
            sql.append("AND EQUIPMENT_NO =  :equipNo  ");
        }
        if (status != null && !status.equals("")){
            sql.append("AND INVOICE_STATUS =  :status  ");
        }
     
//        if (!fsc.equals("")){
//            sql.append("AND COLL_FSC= :fsc ");
//        }
        if (dateType.equals("I")){
            if (fromDate != null && !fromDate.equals("")){
                sql.append("AND RECORD_ADD_DATE >="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(fromDate))))+" ");    
            }
            if (toDate != null && !toDate.equals("")){
                sql.append("AND RECORD_ADD_DATE <="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(toDate))))+" "); }
        }else if (dateType.equals("S")){
            if (fromDate != null && !fromDate.equals("")){
                sql.append("AND SAIL_DATE >="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(fromDate))))+" ");    
            }
            if (toDate != null && !toDate.equals("")){
                sql.append("AND SAIL_DATE <="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(toDate))))+" "); }
        }        
        if (inOutXT.equals("I")) {
            sql.append("AND PORT_OF_DISCHARGE = :port  ");
        }else if (inOutXT.equals("O")){
            sql.append("AND PORT_OF_LOAD = :port  ");
        }else if (inOutXT.equals("X")){
            sql.append("AND PORT_OF_DISCHARGE <> :port  ");
            sql.append("AND PORT_OF_LOAD <> :port  ");
        }
    
        if (sortBy != null && !sortBy.equals("")){
            sql.append("ORDER BY "+(String)columMap.get(sortBy));
            if (sortByIn.equals("A")) {
                sql.append("  ASC");
            }else if (sortByIn.equals("D")){
                sql.append("  DESC");}
        }

        
        HashMap map = new HashMap();
        map.put("billToParty",billToParty);
        map.put("dateType",dateType);
        map.put("fromDate",fromDate);
        map.put("toDate",toDate);
        map.put("service",service);
        map.put("vessel",vessel);
        map.put("voyage",voyage);
        map.put("port",port);
        map.put("inOutXT",inOutXT);
        map.put("blNo",blNo);
        map.put("bkgNo",bkgNo);
        map.put("equipNo",equipNo);
        map.put("status",status);
        map.put("sortBy",sortBy);
        map.put("sortByIn",sortByIn);
        map.put("fsc",fsc);
        map.put("line",line);
        map.put("region",region);
        map.put("agent",agent);

        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenByInvDetail]: SQL: " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenByInvDetail]:  Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarInvoiceMod inv = new FarInvoiceMod();
                inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                inv.setStatus(RutString.nullToStr(rs.getString("INVOICE_STATUS")));
                inv.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                inv.setInvDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));
                inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                return inv;
            }
        });
        
    }
    

    public List listV02ForSearchScreenByInvDetail(int rowAt,int rowTo
                                                    ,String billToParty,String dateType
                                                    ,String fromDate,String toDate
                                                    ,String service,String vessel
                                                    ,String voyage,String port
                                                    ,String inOutXT,String blNo
                                                    ,String bkgNo,String equipNo
                                                    ,String line,String region
                                                    ,String agent
                                                    ,String fsc, String status
                                                    ,String sortBy,String sortByIn) throws DataAccessException{

    
        System.out.println("[FarInvoiceJdbcDao][ListV02ForSearchScreenByInvDetail]:  Started");
        
        String selectColumn = "COLL_FSC,BILL_TO_PARTY,BILL_TO_PARTY_NAME,INVOICE_NO,INVOICE_DATE,REMARKS,INVOICE_STATUS";        
        StringBuffer sql = new StringBuffer();
        Map columMap = new HashMap();
        columMap.put("COLL_FSC","COLL_FSC");
        columMap.put("BILL_TO_PARTY","BILL_TO_PARTY");
        columMap.put("BILL_TO_PARTY_NAME","BILL_TO_PARTY_NAME");
        columMap.put("INVOICE_NO","INVOICE_NO");
        columMap.put("INVOICE_DATE","INVOICE_DATE");
        columMap.put("REMARKS","REMARKS");
        columMap.put("INVOICE_STATUS","INVOICE_STATUS");

        sql.append("SELECT distinct COLL_FSC ");
        sql.append("       ,BILL_TO_PARTY ");
        sql.append("       ,BILL_TO_PARTY_NAME ");
        sql.append("       ,INVOICE_NO ");
        sql.append("       ,INVOICE_DATE ");
        sql.append("       ,REMARKS ");
        sql.append("       ,INVOICE_STATUS ");
        sql.append("FROM  VR_FAR_INVOICE_BY_DETAIL  ");
        sql.append("WHERE 1=1  ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:region,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS <> 'D' ");        
        
        if (billToParty != null && !billToParty.equals("")) {
            sql.append("AND BILL_TO_PARTY = :billToParty  ");
        }
        if (service != null && !service.equals("")){
            sql.append("AND SERVICE = :service  ");
        }
        if (vessel != null && !vessel.equals("")) {
            sql.append("AND VESSEL = :vessel  ");
        }
        if (voyage != null && !voyage.equals("")){
            sql.append("AND VOYAGE = :voyage  ");
        }
        if (blNo != null && !blNo.equals("")){
            sql.append("AND APP_REFERENCE = :blNo  ");
        }
        if (bkgNo != null && !bkgNo.equals("")){
            sql.append("AND BOOKING_NO =  :bkgNo  ");
        }
        if (equipNo != null && !equipNo.equals("")){
            sql.append("AND EQUIPMENT_NO =  :equipNo  ");
        }
        if (status != null && !status.equals("")){
            sql.append("AND INVOICE_STATUS =  :status  ");
        }
        //        if (!fsc.equals("")){
        //            sql.append("AND COLL_FSC= :fsc ");
        //        }
        if (dateType.equals("I")){
            if (fromDate != null && !fromDate.equals("")){
                sql.append("AND INVOICE_DATE >="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(fromDate))))+" ");    
            }
            if (toDate != null && !toDate.equals("")){
                sql.append("AND INVOICE_DATE <="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(toDate))))+" "); }
        }else if (dateType.equals("S")){
            if (fromDate != null && !fromDate.equals("")){
                sql.append("AND SAIL_DATE >="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(fromDate))))+" ");    
            }
            if (toDate != null && !toDate.equals("")){
                sql.append("AND SAIL_DATE <="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(toDate))))+" "); }
        }        
        if (inOutXT.equals("I")) {
            sql.append("AND PORT_OF_DISCHARGE = :port  ");
        }else if (inOutXT.equals("O")){
            sql.append("AND PORT_OF_LOAD = :port  ");
        }else if (inOutXT.equals("X")){
            sql.append("AND PORT_OF_DISCHARGE <> :port  ");
            sql.append("AND PORT_OF_LOAD <> :port  ");
        }
        
        if (sortBy != null && !sortBy.equals("")){
            sql.append("ORDER BY "+(String)columMap.get(sortBy));
            if (sortByIn.equals("A")) {
                sql.append("  ASC");
            }else if (sortByIn.equals("D")){
                sql.append("  DESC");}
        }

        
        HashMap map = new HashMap();
        map.put("billToParty",billToParty);
        map.put("dateType",dateType);
        map.put("fromDate",fromDate);
        map.put("toDate",toDate);
        map.put("service",service);
        map.put("vessel",vessel);
        map.put("voyage",voyage);
        map.put("port",port);
        map.put("inOutXT",inOutXT);
        map.put("blNo",blNo);
        map.put("bkgNo",bkgNo);
        map.put("equipNo",equipNo);
        map.put("status",status);
        map.put("sortBy",sortBy);
        map.put("sortByIn",sortByIn);
        map.put("fsc",fsc);
        map.put("line",line);
        map.put("region",region);
        map.put("agent",agent);

        sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);
        System.out.println("[FarInvoiceJdbcDao][ListV02ForSearchScreenByInvDetail]: SQL: " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][ListV02ForSearchScreenByInvDetail]:  Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            FarInvoiceMod inv = new FarInvoiceMod();
                            inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                            inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                            inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                            inv.setStatus(RutString.nullToStr(rs.getString("INVOICE_STATUS")));
                            inv.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                            inv.setInvDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));
                            inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                            return inv;
                        }
                    });
        
    }

    public int countListForSearchScreenByInvDetail(String billToParty,String dateType
                                                ,String fromDate,String toDate
                                                ,String service,String vessel
                                                ,String voyage,String port
                                                ,String inOutXT,String blNo
                                                ,String bkgNo,String equipNo
                                                ,String line,String region
                                                ,String agent
                                                ,String fsc, String status
                                                ,String sortBy,String sortByIn) throws DataAccessException{
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenByInvDetail]:  Started");       
        StringBuffer sql = new StringBuffer();
        
        Map columMap = new HashMap();
        columMap.put("COLL_FSC","COLL_FSC");
        columMap.put("BILL_TO_PARTY","BILL_TO_PARTY");
        columMap.put("BILL_TO_PARTY_NAME","BILL_TO_PARTY_NAME");
        columMap.put("INVOICE_NO","INVOICE_NO");
        columMap.put("INVOICE_DATE","INVOICE_DATE");
        columMap.put("REMARKS","REMARKS");
        columMap.put("INVOICE_STATUS","INVOICE_STATUS");

        sql.append("SELECT DISTINCT COLL_FSC ");
        sql.append("       ,BILL_TO_PARTY ");
        sql.append("       ,BILL_TO_PARTY_NAME ");
        sql.append("       ,INVOICE_NO ");
        sql.append("       ,INVOICE_DATE ");
        sql.append("       ,REMARKS ");
        sql.append("       ,INVOICE_STATUS ");
        sql.append("FROM  VR_FAR_INVOICE_BY_DETAIL  ");
        sql.append("WHERE 1=1  ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:region,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        sql.append(" AND NVL(COLL_FSC,' ')   = NVL(:fsc,nvl(COLL_FSC,' ')) ");
        sql.append(" AND INVOICE_STATUS <> 'D' ");
        
        if (billToParty != null && !billToParty.equals("")) {
            sql.append("AND BILL_TO_PARTY = :billToParty  ");
        }
        if (service != null && !service.equals("")){
            sql.append("AND SERVICE = :service  ");
        }
        if (vessel != null && !vessel.equals("")) {
            sql.append("AND VESSEL = :vessel  ");
        }
        if (voyage != null && !voyage.equals("")){
            sql.append("AND VOYAGE = :voyage  ");
        }
        if (blNo != null && !blNo.equals("")){
            sql.append("AND APP_REFERENCE = :blNo  ");
        }
        if (bkgNo != null && !bkgNo.equals("")){
            sql.append("AND BOOKING_NO =  :bkgNo  ");
        }
        if (equipNo != null && !equipNo.equals("")){
            sql.append("AND EQUIPMENT_NO =  :equipNo  ");
        }
        
        if (status != null && !status.equals("")){
            sql.append("AND INVOICE_STATUS =  :status  ");
        }
        //        if (!fsc.equals("")){
        //            sql.append("AND COLL_FSC= :fsc ");
        //        }
        if (dateType.equals("I")){
            if (fromDate != null && !fromDate.equals("")){
                sql.append("AND INVOICE_DATE >="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(fromDate))))+" ");    
            }
            if (toDate != null && !toDate.equals("")){
                sql.append("AND INVOICE_DATE <="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(toDate))))+" "); }
        }else if (dateType.equals("S")){
            if (fromDate != null && !fromDate.equals("")){
                sql.append("AND SAIL_DATE >="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(fromDate))))+" ");    
            }
            if (toDate != null && !toDate.equals("")){
                sql.append("AND SAIL_DATE <="+Integer.parseInt((RutDate.dateToStr(RutString.nullToStr(toDate))))+" "); }
        }        
        if (inOutXT.equals("I")) {
            sql.append("AND PORT_OF_DISCHARGE = :port  ");
        }else if (inOutXT.equals("O")){
            sql.append("AND PORT_OF_LOAD = :port  ");
        }else if (inOutXT.equals("X")){
            sql.append("AND PORT_OF_DISCHARGE <> :port  ");
            sql.append("AND PORT_OF_LOAD <> :port  ");
        }
        
        if (sortBy != null && !sortBy.equals("")){
            sql.append("ORDER BY "+(String)columMap.get(sortBy));
            if (sortByIn.equals("A")) {
                sql.append("  ASC");
            }else if (sortByIn.equals("D")){
                sql.append("  DESC");}
        }

        
        HashMap map = new HashMap();
        map.put("billToParty",billToParty);
        map.put("dateType",dateType);
        map.put("fromDate",fromDate);
        map.put("toDate",toDate);
        map.put("service",service);
        map.put("vessel",vessel);
        map.put("voyage",voyage);
        map.put("port",port);
        map.put("inOutXT",inOutXT);
        map.put("blNo",blNo);
        map.put("bkgNo",bkgNo);
        map.put("equipNo",equipNo);
        map.put("status",status);
        map.put("sortBy",sortBy);
        map.put("sortByIn",sortByIn);
        map.put("fsc",fsc);
        map.put("line",line);
        map.put("region",region);
        map.put("agent",agent);
        sql = getNumberOfAllData(sql);
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenByInvDetail]: SQL: " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenByInvDetail]:  Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map , Integer.class);    
    }    
    public List listForSearchScreenInvItemByInvHeader(String invDateFrom,String invDateTo,String invNoFrom,String invNoTo,String remark,String status, String inbOubXT,String port,String repTemplate,String line,String region,String agent,String fsc,String sortBy,String sortByIn) {
        return this.listForSearchScreenInvItemByInvHeader(0, 0, invDateFrom, invDateTo, invNoFrom, invNoTo, remark, status, inbOubXT, port, repTemplate, line, region, agent, fsc, sortBy, sortByIn);
    } 
    
    public List listForSearchScreenInvItemByInvHeader(int rowAt,int rowTo,String invDateFrom,String invDateTo,String invNoFrom,String invNoTo,String remark,String status, String inbOubXT,String port,String repTemplate,String line,String region,String agent,String fsc,String sortBy,String sortByIn) {
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]: Started");
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]: rowAt = "+rowAt);
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]: rowTo = "+rowTo);
        
        String sqlColumn = "COLL_FSC, BILL_TO_CUSTOMER, CUNAME, SAIL_DATE, INVOICE_DATE, INVOICE_PRINTING_DATE, SERVICE, VESSEL, VOYAGE, PORT_OF_LOAD, PORT_OF_DISCHARGE, APP_REFERENCE, INVOICE_NO, CUSTOMER_REFERENCE, IB_OB_XT, REMARKS";//#04 BHAMOH1
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct vh.COLL_FSC ");
        sql.append("    ,vh.BILL_TO_CUSTOMER ");
        sql.append("    ,vh.CUNAME ");
        sql.append("    ,vh.SAIL_DATE ");
        sql.append("    ,vh.INVOICE_DATE ");//#04 BHAMOH1
        sql.append("    ,vh.INVOICE_PRINTING_DATE ");//#04 BHAMOH1
        sql.append("    ,vh.SERVICE ");
        sql.append("    ,vh.VESSEL ");
        sql.append("    ,vh.VOYAGE ");
        sql.append("    ,vh.PORT_OF_LOAD ");
        sql.append("    ,vh.PORT_OF_DISCHARGE ");
        sql.append("    ,vh.APP_REFERENCE ");
        sql.append("    ,vh.INVOICE_NO ");
        sql.append("    ,vh.CUSTOMER_REFERENCE ");
        
        //begin: get columns (IB_OB_XT, REMARKS)
        if ("A".equals(inbOubXT) && !RutString.isEmptyString(port)) {
            sql.append("    ,decode(vh.PORT_OF_DISCHARGE, '"+port+"', 'I', ");
            sql.append("        decode(vh.PORT_OF_LOAD, '"+port+"', 'O', 'X')) as IB_OB_XT ");
        } else {
            sql.append("    ,'"+inbOubXT+"' as IB_OB_XT ");    
        }
        if (!RutString.isEmptyString(repTemplate)) {
            sql.append("    ,PCR_FAR_SPECIAL_BILLING.FR_GET_REMARK_INV_BY_TEMPLATE('"+repTemplate+"', vh.CHARGE_CODE, vh.CUSTOMER_REFERENCE, vh.APP_REFERENCE, vh.REMARKS) as REMARKS ");
        } else {
            sql.append("    ,nvl(vh.REMARKS, '') as REMARKS ");
        }
        //end: get columns (IB_OB_XT, REMARKS)
        
        sql.append("from VR_FAR_INVOICE_ITEM vh ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(line)) {
            strWhere.append(" and vh.COLL_LINE = '"+line+"' ");
        }
        if (!RutString.isEmptyString(region)) {
            strWhere.append(" and vh.COLL_TRADE = '"+region+"' ");
        }
        if (!RutString.isEmptyString(agent)) {
            strWhere.append(" and vh.COLL_AGENT = '"+agent+"' ");
        }
        if (!RutString.isEmptyString(fsc) && !"R".equals(fsc)) {
            strWhere.append("    and vh.COLL_FSC = '"+fsc+"' ");    
        }
        if (!RutString.isEmptyString(invDateFrom)) {
            strWhere.append(" and vh.INVOICE_DATE >= "+invDateFrom+" ");//#03 BHAMOH1
        }
        if (!RutString.isEmptyString(invDateTo)) {
            strWhere.append(" and vh.INVOICE_DATE <= "+invDateTo+" ");//#03 BHAMOH1
        }
        if (!RutString.isEmptyString(invNoFrom)) {
            strWhere.append(" and vh.INVOICE_NO >= '"+invNoFrom+"' ");
        }
        if (!RutString.isEmptyString(invNoTo)) {
            strWhere.append(" and vh.INVOICE_NO <= '"+invNoTo+"' ");
        }
        if (!RutString.isEmptyString(remark)) {
            strWhere.append(" and vh.REMARKS like '%"+remark+"%' ");
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and vh.INVOICE_STATUS_HEADER = '"+status+"' ");
        }
        if (!RutString.isEmptyString(port) && !RutString.isEmptyString(inbOubXT)) {
            if ("I".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE = '"+port+"' ");
            } else if ("O".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_LOAD = '"+port+"' ");
            } else if ("X".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE <> '"+port+"' ");
                strWhere.append(" and vh.PORT_OF_LOAD <> '"+port+"' ");
            }
        }
                
        // concat sqlWhere with sql
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
                
        // concat order by
        if (!RutString.isEmptyString(sortBy)) {
            Map columMap = new HashMap();
            columMap.put("COLL_FSC", "COLL_FSC");
            columMap.put("BILL_TO_PARTY", "BILL_TO_CUSTOMER");
            columMap.put("CUNAME", "CUNAME");
            columMap.put("SAIL_DATE", "SAIL_DATE");
            columMap.put("INVOICE_DATE", "INVOICE_DATE");//#04 BHAMOH1
            columMap.put("INVOICE_PRINTING_DATE", "INVOICE_PRINTING_DATE");//#04 BHAMOH1
            columMap.put("SERVICE", "SERVICE");
            columMap.put("VESSEL", "VESSEL");
            columMap.put("VOYAGE", "VOYAGE");
            columMap.put("PORT_OF_LOAD", "PORT_OF_LOAD");
            columMap.put("PORT_OF_DISCHARGE", "PORT_OF_DISCHARGE");
            columMap.put("APP_REFERENCE", "APP_REFERENCE");
            columMap.put("INVOICE_NO", "INVOICE_NO");
            columMap.put("INVOICE_STATUS", "INVOICE_STATUS_HEADER");
            columMap.put("CUST_REFERENCE", "CUST_REFERENCE");
            columMap.put("REMARKS", "REMARKS");
            columMap.put("IB_OB_XT", "IB_OB_XT");
            
            String tmpSortByIn = null;
            if (RcmConstant.SORT_DESCENDING.equals(sortByIn)) {
                tmpSortByIn = " DESC ";
            } else {
                tmpSortByIn = " ASC ";
            }
                               
            if ("IB_OB_XT".equals(sortBy)) {
                if ("A".equals(inbOubXT) && !RutString.isEmptyString(port)) {
                    String tmpOrderBy = "decode(vh.PORT_OF_DISCHARGE, '"+port+"', 'I', decode(vh.PORT_OF_LOAD, '"+port+"', 'O', 'X')) ";
                    sql.append("order by "+tmpOrderBy+tmpSortByIn);
                }
            } else {
                sql.append("order by "+(String)columMap.get(sortBy)+tmpSortByIn);
            }
        }
        
        // Query by record number range
        String sqlStmt = RutDatabase.optimizeSQL(sql.toString(), rowAt, rowTo);
        
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]: SQL: " + sqlStmt);
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvHeader]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sqlStmt,
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            FarPoMod inv = new FarPoMod();
                            inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                            inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_CUSTOMER")));
                            inv.setBillToPartyName(RutString.nullToStr(rs.getString("CUNAME")));
                            inv.setEtaEtd(RutString.nullToStr(rs.getString("SAIL_DATE")));
                            inv.setInvoiceDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));
                            inv.setInvoicePrintDate(RutString.nullToStr(rs.getString("INVOICE_PRINTING_DATE")));
                            inv.setService(RutString.nullToStr(rs.getString("SERVICE")));
                            inv.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                            inv.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                            inv.setPol(RutString.nullToStr(rs.getString("PORT_OF_LOAD")));
                            inv.setPod(RutString.nullToStr(rs.getString("PORT_OF_DISCHARGE")));
                            inv.setBlNo(RutString.nullToStr(rs.getString("APP_REFERENCE")));
                            inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                            inv.setPoNo(RutString.nullToStr(rs.getString("CUSTOMER_REFERENCE")));                      
                            inv.setInOutXt(RutString.nullToStr(rs.getString("IB_OB_XT")));
                            inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                            return inv;
                        }
                    });
    }
    
    public int countListForSearchScreenInvItemByInvHeader(String invDateFrom,String invDateTo,String invNoFrom,String invNoTo,String remark,String status, String inbOubXT,String port,String repTemplate,String line,String region,String agent,String fsc,String sortBy,String sortByIn) {
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenInvItemByInvHeader]: Started");       
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct vh.COLL_FSC ");
        sql.append("    ,vh.BILL_TO_CUSTOMER ");
        sql.append("    ,vh.CUNAME ");
        sql.append("    ,vh.SAIL_DATE ");
        sql.append("    ,vh.INVOICE_DATE ");//#04 BHAMOH1
        sql.append("    ,vh.INVOICE_PRINTING_DATE ");//#04 BHAMOH1
        sql.append("    ,vh.SERVICE ");
        sql.append("    ,vh.VESSEL ");
        sql.append("    ,vh.VOYAGE ");
        sql.append("    ,vh.PORT_OF_LOAD ");
        sql.append("    ,vh.PORT_OF_DISCHARGE ");
        sql.append("    ,vh.APP_REFERENCE ");
        sql.append("    ,vh.INVOICE_NO ");
        sql.append("    ,vh.CUSTOMER_REFERENCE ");
        
        //begin: get columns (IB_OB_XT, REMARKS)
        if ("A".equals(inbOubXT) && !RutString.isEmptyString(port)) {
            sql.append("    ,decode(vh.PORT_OF_DISCHARGE, '"+port+"', 'I', ");
            sql.append("        decode(vh.PORT_OF_LOAD, '"+port+"', 'O', 'X')) as IB_OB_XT ");
        } else {
            sql.append("    ,'"+inbOubXT+"' as IB_OB_XT ");    
        }
        if (!RutString.isEmptyString(repTemplate)) {
            sql.append("    ,PCR_FAR_SPECIAL_BILLING.FR_GET_REMARK_INV_BY_TEMPLATE('"+repTemplate+"', vh.CHARGE_CODE, vh.CUSTOMER_REFERENCE, vh.APP_REFERENCE, vh.REMARKS) as REMARKS ");
        } else {
            sql.append("    ,nvl(vh.REMARKS, '') as REMARKS ");
        }
        //end: get columns (IB_OB_XT, REMARKS)
        
        sql.append("from VR_FAR_INVOICE_ITEM vh ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(line)) {
            strWhere.append(" and vh.COLL_LINE = '"+line+"' ");
        }
        if (!RutString.isEmptyString(region)) {
            strWhere.append(" and vh.COLL_TRADE = '"+region+"' ");
        }
        if (!RutString.isEmptyString(agent)) {
            strWhere.append(" and vh.COLL_AGENT = '"+agent+"' ");
        }
        if (!RutString.isEmptyString(fsc) && !"R".equals(fsc)) {
            strWhere.append("    and vh.COLL_FSC = '"+fsc+"' ");    
        }
        if (!RutString.isEmptyString(invDateFrom)) {
            strWhere.append(" and vh.INVOICE_DATE >= "+invDateFrom+" ");//#03 BHAMOH1
        }
        if (!RutString.isEmptyString(invDateTo)) {
            strWhere.append(" and vh.INVOICE_DATE <= "+invDateTo+" ");//#03 BHAMOH1
        }
        if (!RutString.isEmptyString(invNoFrom)) {
            strWhere.append(" and vh.INVOICE_NO >= '"+invNoFrom+"' ");
        }
        if (!RutString.isEmptyString(invNoTo)) {
            strWhere.append(" and vh.INVOICE_NO <= '"+invNoTo+"' ");
        }
        if (!RutString.isEmptyString(remark)) {
            strWhere.append(" and vh.REMARKS like '%"+remark+"%' ");
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and vh.INVOICE_STATUS_HEADER = '"+status+"' ");
        }
        if (!RutString.isEmptyString(port) && !RutString.isEmptyString(inbOubXT)) {
            if ("I".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE = '"+port+"' ");
            } else if ("O".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_LOAD = '"+port+"' ");
            } else if ("X".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE <> '"+port+"' ");
                strWhere.append(" and vh.PORT_OF_LOAD <> '"+port+"' ");
            }
        }
                
        // concat sqlWhere with sql
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        
        // Find number of all data
        String sqlStmt = RutDatabase.optimizeCountRecordSQL(sql.toString());
        
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenInvItemByInvHeader]: SQL: " + sqlStmt);
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenInvItemByInvHeader]: Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStmt, new HashMap() , Integer.class); 
    }
    
    public List listForSearchScreenInvItemByInvDetail(String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String blNo,String bkgNo,String equipNo,String status, String inbOubXT,String line,String region,String agent,String fsc,String repTemplate,String sortBy,String sortByIn) throws DataAccessException {
        return this.listForSearchScreenInvItemByInvDetail(0, 0, billToParty, dateType, fromDate, toDate, service, vessel, voyage, port, blNo, bkgNo, equipNo, status, inbOubXT, line, region, agent, fsc, repTemplate, sortBy, sortByIn);
    }
    
    public List listForSearchScreenInvItemByInvDetail(int rowAt,int rowTo,String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String blNo,String bkgNo,String equipNo,String status, String inbOubXT,String line,String region,String agent,String fsc,String repTemplate,String sortBy,String sortByIn) throws DataAccessException {
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvDetail]: Started");
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvDetail]: rowAt = "+rowAt);
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvDetail]: rowTo = "+rowTo); 
        
        String sqlColumn = "COLL_FSC, BILL_TO_CUSTOMER, CUNAME, SAIL_DATE, INVOICE_DATE, INVOICE_PRINTING_DATE, SERVICE, VESSEL, VOYAGE, PORT_OF_LOAD, PORT_OF_DISCHARGE, APP_REFERENCE, INVOICE_NO, CUSTOMER_REFERENCE, IB_OB_XT, REMARKS";//#04 BHAMOH1
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct vh.COLL_FSC ");
        sql.append("    ,vh.BILL_TO_CUSTOMER ");
        sql.append("    ,vh.CUNAME ");
        sql.append("    ,vh.SAIL_DATE ");
        sql.append("    ,vh.INVOICE_DATE ");//#04 BHAMOH1
        sql.append("    ,vh.INVOICE_PRINTING_DATE ");//#04 BHAMOH1
        sql.append("    ,vh.SERVICE ");
        sql.append("    ,vh.VESSEL ");
        sql.append("    ,vh.VOYAGE ");
        sql.append("    ,vh.PORT_OF_LOAD "); 
        sql.append("    ,vh.PORT_OF_DISCHARGE ");
        sql.append("    ,vh.APP_REFERENCE ");
        sql.append("    ,vh.INVOICE_NO ");
        sql.append("    ,vh.CUSTOMER_REFERENCE ");
        
        //begin: get columns (IB_OB_XT, REMARKS)
        if ("A".equals(inbOubXT) && !RutString.isEmptyString(port)) {
            sql.append("    ,decode(vh.PORT_OF_DISCHARGE, '"+port+"', 'I', ");
            sql.append("        decode(vh.PORT_OF_LOAD, '"+port+"', 'O', 'X')) as IB_OB_XT ");
        } else {
            sql.append("    ,'"+inbOubXT+"' as IB_OB_XT ");    
        }
        if (!RutString.isEmptyString(repTemplate)) {
            sql.append("    ,PCR_FAR_SPECIAL_BILLING.FR_GET_REMARK_INV_BY_TEMPLATE('"+repTemplate+"', vh.CHARGE_CODE, vh.CUSTOMER_REFERENCE, vh.APP_REFERENCE, vh.REMARKS) as REMARKS ");
        } else {
            sql.append("    ,nvl(vh.REMARKS, '') as REMARKS ");
        }
        //end: get columns (IB_OB_XT, REMARKS)
        
        sql.append("from VR_FAR_INVOICE_ITEM vh ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(line)) {
            strWhere.append(" and vh.COLL_LINE = '"+line+"' ");
        }
        if (!RutString.isEmptyString(region)) {
            strWhere.append(" and vh.COLL_TRADE = '"+region+"' ");
        }
        if (!RutString.isEmptyString(agent)) {
            strWhere.append(" and vh.COLL_AGENT = '"+agent+"' ");
        }
        if (!RutString.isEmptyString(fsc) && !"R".equals(fsc)) {
            strWhere.append("    and vh.COLL_FSC = '"+fsc+"' ");    
        }
        if (!RutString.isEmptyString(billToParty)) {
            strWhere.append(" and vh.BILL_TO_CUSTOMER = '"+billToParty+"' ");
        }
        if (!RutString.isEmptyString(service)) {
            strWhere.append(" and vh.SERVICE = '"+service+"' ");
        }
        if (!RutString.isEmptyString(vessel)) {
            strWhere.append(" and vh.VESSEL = '"+vessel+"' ");
        }
        if (!RutString.isEmptyString(voyage)) {
            strWhere.append(" and vh.VOYAGE = '"+voyage+"' ");
        }
        if (!RutString.isEmptyString(blNo)) {
            strWhere.append(" and vh.APP_REFERENCE = '"+blNo+"' ");
        }
        
        if (!RutString.isEmptyString(bkgNo)) {
            strWhere.append(" and vh.BOOKING_NO = '"+bkgNo+"' ");
        }
        if (!RutString.isEmptyString(equipNo)) {
            strWhere.append(" and vh.EQUIPMENT_NO = '"+equipNo+"' ");
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and vh.INVOICE_STATUS_DETAIL = '"+status+"' ");
        }
        
        if ("I".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vh.INVOICE_DATE >= "+fromDate+" ");//##03 BHAMOH1
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vh.INVOICE_DATE <= "+toDate+" ");//##03 BHAMOH1
            }
        } else if ("P".equals(dateType)) { //##03 START BHAMOH1
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vh.INVOICE_PRINTING_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vh.INVOICE_PRINTING_DATE <= "+toDate+" ");
            }
            //##03 END BHAMOH1
        } else if ("S".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vh.SAIL_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vh.SAIL_DATE <= "+toDate+" ");
            }
        }
        
        if (!RutString.isEmptyString(port) && !RutString.isEmptyString(inbOubXT)) {
            if ("I".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE = '"+port+"' ");
            } else if ("O".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_LOAD = '"+port+"' ");
            } else if ("X".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE <> '"+port+"' ");
                strWhere.append(" and vh.PORT_OF_LOAD <> '"+port+"' ");
            }
        }
                
        // concat sqlWhere with sql
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        
        // concat order by
        if (!RutString.isEmptyString(sortBy)) {
            Map columMap = new HashMap();
            columMap.put("COLL_FSC", "COLL_FSC");
            columMap.put("BILL_TO_PARTY", "BILL_TO_CUSTOMER");
            columMap.put("CUNAME", "CUNAME");
            columMap.put("SAIL_DATE", "SAIL_DATE");
            columMap.put("INVOICE_DATE", "INVOICE_DATE");//#04 BHAMOH1
            columMap.put("INVOICE_PRINTING_DATE", "INVOICE_PRINTING_DATE");//#04 BHAMOH1
            columMap.put("SERVICE", "SERVICE");
            columMap.put("VESSEL", "VESSEL");
            columMap.put("VOYAGE", "VOYAGE");
            columMap.put("PORT_OF_LOAD", "PORT_OF_LOAD");
            columMap.put("PORT_OF_DISCHARGE", "PORT_OF_DISCHARGE");
            columMap.put("APP_REFERENCE", "APP_REFERENCE");
            columMap.put("INVOICE_NO", "INVOICE_NO");
            columMap.put("INVOICE_STATUS", "INVOICE_STATUS_DETAIL");
            columMap.put("CUST_REFERENCE", "CUST_REFERENCE");
            columMap.put("REMARKS", "REMARKS");
            columMap.put("IB_OB_XT", "IB_OB_XT");
            
            String tmpSortByIn = null;
            if (RcmConstant.SORT_DESCENDING.equals(sortByIn)) {
                tmpSortByIn = " DESC ";
            } else {
                tmpSortByIn = " ASC ";
            }
                               
            if ("IB_OB_XT".equals(sortBy)) {
                if ("A".equals(inbOubXT) && !RutString.isEmptyString(port)) {
                    String tmpOrderBy = "decode(vh.PORT_OF_DISCHARGE, '"+port+"', 'I', decode(vh.PORT_OF_LOAD, '"+port+"', 'O', 'X')) ";
                    sql.append("order by "+tmpOrderBy+tmpSortByIn);
                }
            } else {
                sql.append("order by "+(String)columMap.get(sortBy)+tmpSortByIn);
            }
        }

        // Query by record number range
        String sqlStmt = RutDatabase.optimizeSQL(sql.toString(), rowAt, rowTo);
        
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvDetail]: SQL = " + sqlStmt);
        System.out.println("[FarInvoiceJdbcDao][listForSearchScreenInvItemByInvDetail]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sqlStmt,
                new HashMap(),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                        FarPoMod inv = new FarPoMod();
                        inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                        inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_CUSTOMER")));
                        inv.setBillToPartyName(RutString.nullToStr(rs.getString("CUNAME")));
                        inv.setEtaEtd(RutString.nullToStr(rs.getString("SAIL_DATE")));
                        inv.setInvoiceDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));//#04 BHAMOH1
                        inv.setInvoicePrintDate(RutString.nullToStr(rs.getString("INVOICE_PRINTING_DATE")));//#04 BHAMOH1
                        inv.setService(RutString.nullToStr(rs.getString("SERVICE")));
                        inv.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                        inv.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                        inv.setPol(RutString.nullToStr(rs.getString("PORT_OF_LOAD")));
                        inv.setPod(RutString.nullToStr(rs.getString("PORT_OF_DISCHARGE")));
                        inv.setBlNo(RutString.nullToStr(rs.getString("APP_REFERENCE")));
                        inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                        inv.setPoNo(RutString.nullToStr(rs.getString("CUSTOMER_REFERENCE")));                      
                        inv.setInOutXt(RutString.nullToStr(rs.getString("IB_OB_XT")));
                        inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                        return inv;
                    }
                });
    }
    
    public int countListForSearchScreenInvItemByInvDetail(String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String blNo,String bkgNo,String equipNo,String status, String inbOubXT,String line,String region,String agent,String fsc,String repTemplate,String sortBy,String sortByIn) throws DataAccessException {
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenInvItemByInvDetail]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct vh.COLL_FSC ");
        sql.append("    ,vh.BILL_TO_CUSTOMER ");
        sql.append("    ,vh.CUNAME ");
        sql.append("    ,vh.SAIL_DATE ");
        sql.append("    ,vh.INVOICE_DATE ");
        sql.append("    ,vh.INVOICE_PRINTING_DATE ");
        sql.append("    ,vh.SERVICE ");
        sql.append("    ,vh.VESSEL ");
        sql.append("    ,vh.VOYAGE ");
        sql.append("    ,vh.PORT_OF_LOAD ");
        sql.append("    ,vh.PORT_OF_DISCHARGE ");
        sql.append("    ,vh.APP_REFERENCE ");
        sql.append("    ,vh.INVOICE_NO ");
        sql.append("    ,vh.CUSTOMER_REFERENCE ");
        
        //begin: get columns (IB_OB_XT, REMARKS)
        if ("A".equals(inbOubXT) && !RutString.isEmptyString(port)) {
            sql.append("    ,decode(vh.PORT_OF_DISCHARGE, '"+port+"', 'I', ");
            sql.append("        decode(vh.PORT_OF_LOAD, '"+port+"', 'O', 'X')) as IB_OB_XT ");
        } else {
            sql.append("    ,'"+inbOubXT+"' as IB_OB_XT ");    
        }
        if (!RutString.isEmptyString(repTemplate)) {
            sql.append("    ,PCR_FAR_SPECIAL_BILLING.FR_GET_REMARK_INV_BY_TEMPLATE('"+repTemplate+"', vh.CHARGE_CODE, vh.CUSTOMER_REFERENCE, vh.APP_REFERENCE, vh.REMARKS) as REMARKS ");
        } else {
            sql.append("    ,nvl(vh.REMARKS, '') as REMARKS ");
        }
        //end: get columns (IB_OB_XT, REMARKS)
        
        sql.append("from VR_FAR_INVOICE_ITEM vh ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(line)) {
            strWhere.append(" and vh.COLL_LINE = '"+line+"' ");
        }
        if (!RutString.isEmptyString(region)) {
            strWhere.append(" and vh.COLL_TRADE = '"+region+"' ");
        }
        if (!RutString.isEmptyString(agent)) {
            strWhere.append(" and vh.COLL_AGENT = '"+agent+"' ");
        }
        if (!RutString.isEmptyString(fsc) && !"R".equals(fsc)) {
            strWhere.append("    and vh.COLL_FSC = '"+fsc+"' ");    
        }
        if (!RutString.isEmptyString(billToParty)) {
            strWhere.append(" and vh.BILL_TO_CUSTOMER = '"+billToParty+"' ");
        }
        if (!RutString.isEmptyString(service)) {
            strWhere.append(" and vh.SERVICE = '"+service+"' ");
        }
        if (!RutString.isEmptyString(vessel)) {
            strWhere.append(" and vh.VESSEL = '"+vessel+"' ");
        }
        if (!RutString.isEmptyString(voyage)) {
            strWhere.append(" and vh.VOYAGE = '"+voyage+"' ");
        }
        if (!RutString.isEmptyString(blNo)) {
            strWhere.append(" and vh.APP_REFERENCE = '"+blNo+"' ");
        }
        
        if (!RutString.isEmptyString(bkgNo)) {
            strWhere.append(" and vh.BOOKING_NO = '"+bkgNo+"' ");
        }
        if (!RutString.isEmptyString(equipNo)) {
            strWhere.append(" and vh.EQUIPMENT_NO = '"+equipNo+"' ");
        }
        if (!RutString.isEmptyString(status)) {
            strWhere.append(" and vh.INVOICE_STATUS_DETAIL = '"+status+"' ");
        }
        
        if ("I".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vh.INVOICE_DATE >= "+fromDate+" ");//##03 BHAMOH1
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vh.INVOICE_DATE <= "+toDate+" ");//##03 BHAMOH1
            }
        } else if ("P".equals(dateType)) { //##03 START BHAMOH1
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vh.INVOICE_PRINTING_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vh.INVOICE_PRINTING_DATE <= "+toDate+" ");
            }
            //##03 END BHAMOH1
        } else if ("S".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vh.SAIL_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vh.SAIL_DATE <= "+toDate+" ");
            }
        }
        
        if (!RutString.isEmptyString(port) && !RutString.isEmptyString(inbOubXT)) {
            if ("I".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE = '"+port+"' ");
            } else if ("O".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_LOAD = '"+port+"' ");
            } else if ("X".equals(inbOubXT)) {
                strWhere.append(" and vh.PORT_OF_DISCHARGE <> '"+port+"' ");
                strWhere.append(" and vh.PORT_OF_LOAD <> '"+port+"' ");
            }
        }
                
        // concat sqlWhere with sql
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        
        // Find number of all data
        String sqlStmt = RutDatabase.optimizeCountRecordSQL(sql.toString());
         
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenInvItemByInvDetail]: SQL = " + sqlStmt);
        System.out.println("[FarInvoiceJdbcDao][countListForSearchScreenInvItemByInvDetail]: Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStmt, new HashMap() , Integer.class);
    }
    
    public List listTemplate(String type) {
        System.out.println("[FarInvoiceJdbcDao][listTemplate]: Started");        
        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TEMPLATE_CODE,TEMPLATE_DESC ");
        sql.append("FROM VR_FAR_TEMPLATE ");
        sql.append("WHERE UPPER(TEMPLATE_CODE) LIKE '%"+type.toUpperCase()+"%'");   
        System.out.println("[FarInvoiceJdbcDao][listTemplate]: SQL = " + sql.toString());
        HashMap map = new HashMap();                
        map.put("type",type.toUpperCase());        
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            FarReportTemplateMod inv = new FarReportTemplateMod(); 
                            inv.setTemplateCode(RutString.nullToStr(rs.getString("TEMPLATE_CODE")));
                            inv.setTemplateName(RutString.nullToStr(rs.getString("TEMPLATE_DESC")));
                            return inv;
                        }
                });
    }
    
    public String findReportNameByTemplateCode(String templateCode) {
        System.out.println("[FarInvoiceJdbcDao][findReportNameByTemplateCode]: Started");        
        
        String reportName = "";
        StringBuffer sql = new StringBuffer();
        sql.append("select REPORT_FILE_NAME ");
        sql.append("from VR_FAR_TEMPLATE ");
        sql.append("where TEMPLATE_CODE = :templateCode ");
        
        HashMap map = new HashMap();                
        map.put("templateCode", templateCode);        
        
        System.out.println("[FarInvoiceJdbcDao][findReportNameByTemplateCode]: sql = "+sql.toString());
        reportName = (String) getNamedParameterJdbcTemplate().queryForObject(
                sql.toString()
                ,map
                ,new RowMapper(){
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return RutString.nullToStr(rs.getString("REPORT_FILE_NAME"));
                    }
            });
        
        System.out.println("[FarInvoiceJdbcDao][findReportNameByTemplateCode]: Finished");
        return reportName;
    }
    
    public List listForExistingInvoiceItemByBillNo(String billToParty, String billNo, int billVersion, String templateCode, String inbOubXT) {
        System.out.println("[FarInvoiceJdbcDao][listForExistingInvoiceItemByBillNo]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append(" WITH inv AS (select bd.INVOICE_NO ");
        sql.append("  from BILL_STATEMENT_DTL bd ");
        sql.append("  where bd.BILL_NO = :billNo ");
        sql.append("  and bd.BILL_VERSION = :billVersion ) ");
        sql.append(" select distinct ");
        sql.append(" vd.COLL_FSC ");
        sql.append(" ,vd.BILL_TO_CUSTOMER ");
        sql.append(" ,( select CUNAME from ITP010 where CUCUST = vd.BILL_TO_CUSTOMER and rownum = 1 ) as CUNAME ");     
        sql.append(" ,vd.SAIL_DATE ");         
        sql.append(" ,vd.SERVICE ");         
        sql.append(" ,vd.VESSEL ");      
        sql.append(" ,vd.VOYAGE ");       
        sql.append(" ,vd.PORT_OF_LOAD ");    
        sql.append(" ,vd.PORT_OF_DISCHARGE ");        
        sql.append(" ,vd.APP_REFERENCE ");         
        sql.append(" ,hw.INVOICE_NO  ");  
        sql.append(" ,vd.CUSTOMER_REFERENCE  "); 
        sql.append(" ,hw.INVOICE_DATE  "); 
        sql.append(" ,hw.INVOICE_PRINTING_DATE  "); 
        //begin: get columns (IB_OB_XT, REMARKS)
        sql.append("    ,'"+inbOubXT+"' as IB_OB_XT ");    
        if (!RutString.isEmptyString(templateCode)) {
        sql.append(" ,PCR_FAR_SPECIAL_BILLING.FR_GET_REMARK_INV_BY_TEMPLATE('"+templateCode+"', vd.CHARGE_CODE, vd.CUSTOMER_REFERENCE, vd.APP_REFERENCE, cw.REMARKS) as REMARKS  ");      
        } else {
            sql.append("    ,nvl(cw.REMARKS, '') as REMARKS ");
        }    
        sql.append(" from AR_INVOICE_DETAIL_WEB vd ");            
        sql.append(" ,AR_INVOICE_HDR_COMMENTS_WEB cw  ");          
        sql.append(" ,AR_INVOICE_HEADER_WEB hw    ");          
        sql.append(" ,inv     ");              
        sql.append("  where hw.INV_RECORD_SEQ = vd.INV_RECORD_SEQ ");         
        sql.append("  and hw.INVOICE_TYPE = vd.INVOICE_TYPE ");        
        sql.append("  and hw.INVOICE_NO = vd.INVOICE_NO ");        
        sql.append("  and hw.BATCH_NUMBER = vd.BATCH_NUMBER ");            
        sql.append("  and hw.INV_RECORD_SEQ = cw.INV_RECORD_SEQ ");      
        sql.append("  and hw.BATCH_TYPE = cw.BATCH_TYPE ");        
        sql.append("  and hw.BATCH_NUMBER = cw.BATCH_NUMBER ");         
        sql.append("  and vd.TAX_SEQ is null ");         
        sql.append("  and vd.BILL_TO_CUSTOMER = :billToParty ");      
        sql.append("  and inv.INVOICE_NO = hw.INVOICE_NO ");          
        sql.append("  order by hw.INVOICE_NO ");        
       
       /*
        sql.append("select distinct vh.COLL_FSC ");
        sql.append("    ,vh.BILL_TO_CUSTOMER ");
        sql.append("    ,vh.CUNAME ");
        sql.append("    ,vh.SAIL_DATE ");
        sql.append("    ,vh.SERVICE ");
        sql.append("    ,vh.VESSEL ");
        sql.append("    ,vh.VOYAGE ");
        sql.append("    ,vh.PORT_OF_LOAD ");
        sql.append("    ,vh.PORT_OF_DISCHARGE ");
        sql.append("    ,vh.APP_REFERENCE ");
        sql.append("    ,vh.INVOICE_NO ");
        sql.append("    ,vh.CUSTOMER_REFERENCE ");
        
        //begin: get columns (IB_OB_XT, REMARKS)
        sql.append("    ,'"+inbOubXT+"' as IB_OB_XT ");    
        if (!RutString.isEmptyString(templateCode)) {
            sql.append("    ,PCR_FAR_SPECIAL_BILLING.FR_GET_REMARK_INV_BY_TEMPLATE('"+templateCode+"', vh.CHARGE_CODE, vh.CUSTOMER_REFERENCE, vh.APP_REFERENCE, vh.REMARKS) as REMARKS ");
        } else {
            sql.append("    ,nvl(vh.REMARKS, '') as REMARKS ");
        }
        //end: get columns (IB_OB_XT, REMARKS)
        
        sql.append("from VR_FAR_INVOICE_ITEM vh ");
        sql.append("where vh.BILL_TO_CUSTOMER = :billToParty ");
        sql.append("    and exists ( ");
        sql.append("            select 1 ");
        sql.append("            from VR_FAR_BILL_STATEMENT_DTL bd ");
        sql.append("            where bd.BILL_NO = :billNo ");
        sql.append("                and bd.BILL_VERSION = :billVersion ");
        sql.append("                and bd.INVOICE_NO = vh.INVOICE_NO ");
        sql.append("                and rownum = 1 ");
        sql.append("        ) ");
        sql.append("order by INVOICE_NO ");
        */
        HashMap map = new HashMap();
        map.put("billToParty", billToParty);
        map.put("billNo", billNo);
        map.put("billVersion", String.valueOf(billVersion));
        
        System.out.println("[FarInvoiceJdbcDao][listForExistingInvoiceItemByBillNo]: SQL = " + sql.toString());
        System.out.println("[FarInvoiceJdbcDao][listForExistingInvoiceItemByBillNo]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sql.toString(),
                map,
                new RowMapper() {
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                        FarPoMod inv = new FarPoMod();
                        inv.setFsc(RutString.nullToStr(rs.getString("COLL_FSC")));
                        inv.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_CUSTOMER")));
                        inv.setBillToPartyName(RutString.nullToStr(rs.getString("CUNAME")));
                        inv.setEtaEtd(RutString.nullToStr(rs.getString("SAIL_DATE")));
                        inv.setInvoiceDate(RutString.nullToStr(rs.getString("INVOICE_DATE")));
                        inv.setInvoicePrintDate(RutString.nullToStr(rs.getString("INVOICE_PRINTING_DATE")));
                        inv.setService(RutString.nullToStr(rs.getString("SERVICE")));
                        inv.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                        inv.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                        inv.setPol(RutString.nullToStr(rs.getString("PORT_OF_LOAD")));
                        inv.setPod(RutString.nullToStr(rs.getString("PORT_OF_DISCHARGE")));
                        inv.setBlNo(RutString.nullToStr(rs.getString("APP_REFERENCE")));
                        inv.setInvNo(RutString.nullToStr(rs.getString("INVOICE_NO")));                      
                        inv.setPoNo(RutString.nullToStr(rs.getString("CUSTOMER_REFERENCE")));                      
                        inv.setInOutXt(RutString.nullToStr(rs.getString("IB_OB_XT")));
                        inv.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                        return inv;
                    }
                });
    }
    
    // ##BEGIN 01
    public List listPhilips(int batchNo,String billNo,int version,int flag) throws DataAccessException{
        System.out.println("[FarBillingStatementAndInvoiceLinkageJdbcDao][listPhilips]: Started");
        System.out.println(" batchNo >>> "+batchNo);
        System.out.println(" billNo >>> "+billNo);
        System.out.println(" version >>> "+version);
        StringBuffer sql = new StringBuffer();
        HashMap map = new HashMap();                      
        sql.append(" SELECT  ");
        sql.append("   V_BILL_NO AS BILL_NO ");
        sql.append(" , V_BILL_VERSION AS B_VER ");
        sql.append("  ,V_BL ");
        sql.append("  ,V_POR ");
        sql.append("  ,V_POL ");
        sql.append("  ,V_POD ");
        sql.append("  ,V_DEL ");
        sql.append("  ,V_VESSEL|| ' / ' || V_VOYAGE AS VES_VOY ");
        sql.append("  ,TO_CHAR(TO_DATE(V_ETA_ETD,'YYYYMMDD'),'YYYY/MM/DD') AS ETA_ETD ");
        sql.append("  , V_UNIT_20_GP AS GP20 ");
        sql.append("   , V_UNIT_40_GP AS GP40 ");
        sql.append("   , V_UNIT_40_HC AS HC40 ");
        sql.append("  , V_SHIPMENT_TERM S_TERM ");
        sql.append("  ,( CASE WHEN (  TRIM(V_PRE_COLL) = 'P' )   THEN  'Prepaid' " +
                             "                  WHEN (  TRIM(V_PRE_COLL) = 'C' )   THEN  'Collect' ELSE ' ' END ) AS FREIGHT_TYPE ");
        sql.append("  , V_CHARGE_DESC AS CHARGE ");
        sql.append("   , V_UNIT AS UNIT ");
        sql.append(" , V_RATE_CURRENCY AS RATE ");
        sql.append("  , V_RATE_20_GP AS RGP20 ");
        sql.append(" , V_RATE_40_GP AS RGP40 ");
        sql.append("  , V_RATE_40_HC AS RHC40 ");
        sql.append(" , V_SUB_TOTAL AS SUB_TOTAL ");
        sql.append("  , V_EXCH_RATE AS EXCH ");
        sql.append(" , V_SUB_TOTAL_IN_SGD AS SGDTOT ");
        sql.append("  , V_TAX_YN AS TAX_YN ");
        sql.append("  , V_TAX_AMT AS TAX_AMT ");
        sql.append("  , V_TOTAL_AMT AS TOT_AMT ");
        sql.append("  , TO_CHAR(V_BILL_DATE ,'YYYY/MM/DD')AS  BILL_DATE  ");
        sql.append("  , V_SHIPPER");
        sql.append("  , V_SHIPPER_DESC");
        sql.append("  , V_PAYMENT_CURRENCY");
        sql.append("  , V_INVOICE_NO");
        sql.append("  , CASE WHEN V_IB_OB_XT = 'I' THEN 'ETA' ELSE  'ETD'  END AS IMEX ");
        
        sql.append(" FROM  VR_FAR_CUST_INV_005");        
        sql.append(" WHERE 1 = 1 ");  
        
        if (flag == 1){
            sql.append(" AND V_BATCH_NUMBER =  :batchNo ");  
            map.put("batchNo",new Integer(batchNo));
        }else{
            sql.append(" AND V_BILL_NO =  :billNo ");    
            map.put("billNo",billNo);
            sql.append(" AND V_BILL_VERSION =  :version "); 
            map.put("version",new Integer(version));
        }
        
        System.out.println("[FarBillingStatementAndInvoiceLinkageJdbcDao][listPhilips]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarInvoicePhilipMod mod = new FarInvoicePhilipMod();
                mod.setBillNo(RutString.nullToStr(rs.getString("BILL_NO")));
                mod.setBlNo(RutString.nullToStr(rs.getString("V_BL")));                      
                mod.setPor(RutString.nullToStr(rs.getString("V_POR")));
                mod.setPol(RutString.nullToStr(rs.getString("V_POL")));
                mod.setPod(RutString.nullToStr(rs.getString("V_POD")));
                mod.setDel(RutString.nullToStr(rs.getString("V_DEL")));                      
                mod.setVesVoy(RutString.nullToStr(rs.getString("VES_VOY")));
                mod.setEtd(RutString.nullToStr(rs.getString("ETA_ETD")));
                mod.setAmount20(RutString.nullToStr(rs.getString("GP20")));
                mod.setAmount40(RutString.nullToStr(rs.getString("GP40")));
                mod.setAmountHC(RutString.nullToStr(rs.getString("HC40")));
                mod.setShiptmentTerm(RutString.nullToStr(rs.getString("S_TERM")));
                mod.setFreightType(RutString.nullToStr(rs.getString("FREIGHT_TYPE")));
                mod.setCharge(RutString.nullToStr(rs.getString("CHARGE")));
                mod.setUnit(RutString.nullToStr(rs.getString("UNIT")));
                mod.setCur(RutString.nullToStr(rs.getString("RATE")));
                mod.setUnitCost20ft(RutString.nullToStr(rs.getString("RGP20")));
                mod.setUnitCost40ft(RutString.nullToStr(rs.getString("RGP40")));
                mod.setUnitCost40HC(RutString.nullToStr(rs.getString("RHC40")));
                mod.setSubTotal(RutString.nullToStr(rs.getString("SUB_TOTAL")));
                mod.setExchangeRate(RutString.nullToStr(rs.getString("EXCH")));
                mod.setSubTotaldollar(RutString.nullToStr(rs.getString("SGDTOT")));
                mod.setGstApplicable(RutString.nullToStr(rs.getString("TAX_YN")));
                mod.setGstFivePercent(RutString.nullToStr(rs.getString("TAX_AMT")));
                mod.setTotAmountdollar(RutString.nullToStr(rs.getString("TOT_AMT")));
                mod.setBillDate(RutString.nullToStr(rs.getString("BILL_DATE")));
                mod.setShipper(RutString.nullToStr(rs.getString("V_SHIPPER")));
                mod.setShipperDesc(RutString.nullToStr(rs.getString("V_SHIPPER_DESC")));
                mod.setPayeeName("PayeeName");
                mod.setPaymentCur(RutString.nullToStr(rs.getString("V_PAYMENT_CURRENCY")));
                mod.setImex(RutString.nullToStr(rs.getString("IMEX")));
                mod.setInvoiceNo(RutString.nullToStr(rs.getString("V_INVOICE_NO")));
                return mod;
            }
        });
        // ##END 01
    }
}
