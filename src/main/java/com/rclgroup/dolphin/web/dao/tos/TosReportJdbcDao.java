/*-----------------------------------------------------------------------------------------------------------  
TosReportJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 23/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.tos.TosStatisticReportMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class TosReportJdbcDao extends RrcStandardDao implements TosReportDao {
    private GenerateNoVslClosedStoreProcedure generateNoVslClosedStoreProcedure;
    private GenerateNoVslNotClosedStoreProcedure generateNoVslNotClosedStoreProcedure;
    private GenerateNoVslNotCreateStoreProcedure generateNoVslNotCreateStoreProcedure;
    
    public TosReportJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        generateNoVslClosedStoreProcedure = new GenerateNoVslClosedStoreProcedure(getJdbcTemplate());
        generateNoVslNotClosedStoreProcedure = new GenerateNoVslNotClosedStoreProcedure(getJdbcTemplate());
        generateNoVslNotCreateStoreProcedure = new GenerateNoVslNotCreateStoreProcedure(getJdbcTemplate());
    }
    
    public boolean generateNoVslClosed(RrcStandardMod mod) throws DataAccessException{
        return generateNoVslClosedStoreProcedure.generate(mod);
    }
    
    public boolean generateNoVslNotClosed(RrcStandardMod mod) throws DataAccessException{
        return generateNoVslNotClosedStoreProcedure.generate(mod);
    }

    public boolean generateNoVslNotCreate(RrcStandardMod mod) throws DataAccessException{
        return generateNoVslNotCreateStoreProcedure.generate(mod);
    }

    protected class GenerateNoVslClosedStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_TOS_RPT.PRR_GEN_TOS_116_CNT_CLOSED";
        
        protected GenerateNoVslClosedStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_condition", Types.VARCHAR));
            declareParameter(new SqlParameter("p_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_from_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_to_date", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof TosStatisticReportMod && outputMod instanceof TosStatisticReportMod) {
                TosStatisticReportMod aInputMod = (TosStatisticReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_condition", aInputMod.getCondition());
                inParameters.put("p_port", aInputMod.getPort());
                inParameters.put("p_terminal", aInputMod.getTerminal());
                inParameters.put("p_from_date", aInputMod.getFromDate());
                inParameters.put("p_to_date", aInputMod.getToDate());
                
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_condition = "+inParameters.get("p_condition"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_port = "+inParameters.get("p_port"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_terminal = "+inParameters.get("p_terminal"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_from_date = "+inParameters.get("p_from_date"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslClosedStoreProcedure]: p_to_date = "+inParameters.get("p_to_date"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class GenerateNoVslNotClosedStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_TOS_RPT.PRR_GEN_TOS_116_CNT_NOT_CLOSED";
        
        protected GenerateNoVslNotClosedStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_condition", Types.VARCHAR));
            declareParameter(new SqlParameter("p_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_from_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_to_date", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof TosStatisticReportMod && outputMod instanceof TosStatisticReportMod) {
                TosStatisticReportMod aInputMod = (TosStatisticReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_condition", aInputMod.getCondition());
                inParameters.put("p_port", aInputMod.getPort());
                inParameters.put("p_terminal", aInputMod.getTerminal());
                inParameters.put("p_from_date", aInputMod.getFromDate());
                inParameters.put("p_to_date", aInputMod.getToDate());
                
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_condition = "+inParameters.get("p_condition"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_port = "+inParameters.get("p_port"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_terminal = "+inParameters.get("p_terminal"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_from_date = "+inParameters.get("p_from_date"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotClosedStoreProcedure]: p_to_date = "+inParameters.get("p_to_date"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class GenerateNoVslNotCreateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_TOS_RPT.PRR_GEN_TOS_116_CNT_NOT_CREATE";
        
        protected GenerateNoVslNotCreateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_username", Types.VARCHAR));
            declareParameter(new SqlParameter("p_condition", Types.VARCHAR));
            declareParameter(new SqlParameter("p_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_from_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_to_date", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof TosStatisticReportMod && outputMod instanceof TosStatisticReportMod) {
                TosStatisticReportMod aInputMod = (TosStatisticReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_username", aInputMod.getUsername());
                inParameters.put("p_condition", aInputMod.getCondition());
                inParameters.put("p_port", aInputMod.getPort());
                inParameters.put("p_terminal", aInputMod.getTerminal());
                inParameters.put("p_from_date", aInputMod.getFromDate());
                inParameters.put("p_to_date", aInputMod.getToDate());
                
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_username = "+inParameters.get("p_username"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_condition = "+inParameters.get("p_condition"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_port = "+inParameters.get("p_port"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_terminal = "+inParameters.get("p_terminal"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_from_date = "+inParameters.get("p_from_date"));
                System.out.println("[TosReportJdbcDao][GenerateNoVslNotCreateStoreProcedure]: p_to_date = "+inParameters.get("p_to_date"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
}
