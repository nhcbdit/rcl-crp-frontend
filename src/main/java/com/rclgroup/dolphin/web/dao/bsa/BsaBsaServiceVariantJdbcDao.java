/*-----------------------------------------------------------------------------------------------------------  
BsaBsaServiceVariantJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/04/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaServiceVariantMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class BsaBsaServiceVariantJdbcDao extends RrcStandardDao implements BsaBsaServiceVariantDao {
    private DeleteStoreProcedure deleteStoreProcedure;
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private GetCalculateDurationStoreProcedure getCalculateDurationStoreProcedure;
    private GetCalculateFrequencyStoreProcedure getCalculateFrequencyStoreProcedure;

    private BsaBsaServiceVariantDao bsaBsaServiceVariantDao;

    private UpdateStoreProcedureTolelant updateStoreProcedureTolelant;

    public BsaBsaServiceVariantJdbcDao() {
    }

    public void setBsaBsaServiceVariantDao(BsaBsaServiceVariantDao bsaBsaServiceVariantDao) {
        this.bsaBsaServiceVariantDao = bsaBsaServiceVariantDao;
    }

    protected void initDao() throws Exception {
        super.initDao();
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        getCalculateDurationStoreProcedure = new GetCalculateDurationStoreProcedure(getJdbcTemplate());
        getCalculateFrequencyStoreProcedure = new GetCalculateFrequencyStoreProcedure(getJdbcTemplate());
        updateStoreProcedureTolelant = new UpdateStoreProcedureTolelant(getJdbcTemplate());
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    
    public List listForSearchScreen(RcmSearchMod searchMod, String modelName, String effectiveDate, String expiredDate) {
        Map columnMap = new HashMap();
        //for both findIn, and sortBy
        columnMap.put("SV", "SERVICE_AND_VARIANT");
        columnMap.put("SE", "SERVICE");
        columnMap.put("VA", "VARIANT_CODE");
        columnMap.put("PR", "PROFORMA_REF_NO");
        columnMap.put("VT", "BSA_VESSEL_TYPE");
        
        String sqlSearchCriteria = createSqlSearchCriteria(searchMod, columnMap, modelName, effectiveDate, expiredDate);
        StringBuffer sql = new StringBuffer();
        sql.append("select BSA_SERVICE_VARIANT_ID ");
        sql.append("      ,BSA_MODEL_ID ");
        sql.append("      ,SERVICE ");
        sql.append("      ,VARIANT_CODE ");
        sql.append("      ,PROFORMA_REF_NO ");
        sql.append("      ,BSA_VESSEL_TYPE ");
        sql.append("      ,NO_VESSELS ");
        sql.append("      ,ROTATION_DURATION ");
        sql.append("      ,USAGE_RULE ");
        sql.append("      ,CALC_FREQUENCY ");
        sql.append("      ,CALC_DURATION ");
        sql.append("      ,DEF_SLOT_TEU ");
        sql.append("      ,DEF_SLOT_TONS ");
        sql.append("      ,DEF_SLOT_REEFER ");
        sql.append("      ,DEF_AVG_COC_TEU_WEIGHT ");
        sql.append("      ,DEF_AVG_SOC_TEU_WEIGHT ");
        sql.append("      ,DEF_MIN_TEU ");
//        sql.append("      ,OVERBOOKING_PERC ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
//        sql.append("      ,REF_VARIANT_CODE ");  /* Start */
//        sql.append("      ,REF_BSA_VESSEL_TYPE ");
        sql.append("      , EXPIRY_DATE ");  
        sql.append("      , EFFECTIVE_DATE ");
        sql.append("      ,TOLERANT ");
         sql.append("     ,SERVICE_VARIANT_TYPE ");/* End */
        sql.append("from VR_BSA_SERVICE_VARIANT vsv ");
        sql.append(sqlSearchCriteria);
        System.out.println("[BsaBsaServiceVariantJdbcDao][listForSearchScreen]: sql: [" + sql.toString() + "]");
        
        return getNamedParameterJdbcTemplate().query(sql.toString(), 
            new HashMap(), 
            new RowMapper() {
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    BsaBsaServiceVariantMod bean = new BsaBsaServiceVariantMod();
                    bean.setBsaServiceVariantId(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                    bean.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                    bean.setService(RutString.nullToStr(rs.getString("SERVICE")));
                    bean.setVariantCode(RutString.nullToStr(rs.getString("VARIANT_CODE")));
                    bean.setProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                    bean.setBsaVesselType(RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE")));
                    bean.setNoVessels(RutString.nullToStr(rs.getString("NO_VESSELS")));
                    bean.setRotationDuration(RutString.nullToStr(rs.getString("ROTATION_DURATION")));
                    bean.setUsageRule(RutString.nullToStr(rs.getString("USAGE_RULE")));
                    bean.setCalcFrequency(RutString.nullToStr(rs.getString("CALC_FREQUENCY")));
                    bean.setCalcDuration(RutString.nullToStr(rs.getString("CALC_DURATION")));
                    bean.setDefSlotTeu(RutString.nullToStr(rs.getString("DEF_SLOT_TEU")));
                    bean.setDefSlotTons(RutString.nullToStr(rs.getString("DEF_SLOT_TONS")));
                    bean.setDefSlotReefer(RutString.nullToStr(rs.getString("DEF_SLOT_REEFER")));
                    bean.setDefAvgCocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_COC_TEU_WEIGHT")));
                    bean.setDefAvgSocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_SOC_TEU_WEIGHT")));
                    bean.setDefMinTeu(RutString.nullToStr(rs.getString("DEF_MIN_TEU")));
//                    bean.setOverbookingPerc(RutString.nullToStr(rs.getString("OVERBOOKING_PERC")));
                    bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                    bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                    bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                    bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                    bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                    /* Start */
//                    bean.setRefVariant(RutString.nullToStr(rs.getString("REF_VARIANT_CODE")));
//                    bean.setRefVesselType(RutString.nullToStr(rs.getString("REF_BSA_VESSEL_TYPE")));
                    bean.setEffectiveDate(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("EFFECTIVE_DATE")));
                    bean.setExpireDate(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("EXPIRY_DATE")));
                  //  bean.setOverbookingPerc(RutString.nullToStr(rs.getString("TOLERANT")));
                    if(RutString.nullToStr(rs.getString("TOLERANT")).equals("")||rs.getString("TOLERANT").equals("0")){
                        bean.setOverbookingPerc("0.00");
                    }else{
                        DecimalFormat dc=new DecimalFormat();
                        dc.applyPattern("###.00");
                        bean.setOverbookingPerc(dc.format(rs.getDouble("TOLERANT")));
                    }      
                    bean.setServiceVariantType(RutString.nullToStr(rs.getString("SERVICE_VARIANT_TYPE")));
                    
                    /* End */
                    return bean;
                }
            });
    }

    private String createSqlSearchCriteria(RcmSearchMod searchMod, Map columnMap, String modelName, String effDate, String expDate) {
        String sqlCriteria = "";
        String sqlWildWithUpperCase = "";
        String sqlSortByIn = "";
        String find = RutString.changeQuoteForSqlStatement(searchMod.getFind());
        String findIn = searchMod.getFindIn();
        String status = searchMod.getStatus();
        String sortBy = searchMod.getSortBy();
        String sortByIn = searchMod.getSortByIn();
        String wild = searchMod.getWild();

        if (wild.equalsIgnoreCase("ON")) {
            sqlWildWithUpperCase = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWildWithUpperCase = "= '" + find.toUpperCase() + "' ";
        }

        if (sortByIn.equalsIgnoreCase("ASC")) {
            sqlSortByIn = "ASC";
        } else if (sortByIn.equalsIgnoreCase("DESC")) {
            sqlSortByIn = "DESC";
        }

        StringBuffer sb = new StringBuffer("");
        if (find.trim().length() != 0) {
            String sqlWild = sqlWildWithUpperCase;
            sb.append(" and UPPER(" + (String)columnMap.get(findIn) + ") " + sqlWild);
        }

        if (status.equalsIgnoreCase("ALL")) {
        } else if (status.equalsIgnoreCase("ACTIVE")) {
            sb.append(" and RECORD_STATUS = 'A'");
        } else if (status.equalsIgnoreCase("SUSPENDED")) {
            sb.append(" and RECORD_STATUS = 'S'");
        }
        
         //add date criteria
         if(!effDate.equalsIgnoreCase("")&& !expDate.equalsIgnoreCase("")){
             sb.append(" AND (EFFECTIVE_DATE BETWEEN TO_DATE('"+RutDate.dateToStr(effDate)+"', 'yyyyMMdd')  " );
             sb.append(" AND TO_DATE('"+RutDate.dateToStr(expDate)+"', 'yyyyMMdd')  " );
             sb.append(" OR  EXPIRY_DATE BETWEEN TO_DATE('"+RutDate.dateToStr(effDate)+"', 'yyyyMMdd')  " );
             sb.append(" AND TO_DATE('"+RutDate.dateToStr(expDate)+"', 'yyyyMMdd')  " );
             sb.append(" OR  (EFFECTIVE_DATE<=TO_DATE('"+RutDate.dateToStr(effDate)+"', 'yyyyMMdd')  " );
             sb.append(" AND EXPIRY_DATE>=TO_DATE('"+RutDate.dateToStr(expDate)+"', 'yyyyMMdd')))  " );
         }

        if (modelName != null && !"".equals(modelName)) {
            sb.append(" and exists ( ");
            sb.append("     select 1 ");
            sb.append("     from VR_BSA_BASE_ALLOCATION_MODEL tmp ");
            sb.append("     where vsv.BSA_MODEL_ID = tmp.BSA_MODEL_ID ");
            sb.append("         and tmp.MODEL_NAME = '" + modelName + "' ");
            sb.append("         and tmp.RECORD_STATUS = 'A' ");
            sb.append(" )");
        }
        
        if (sb != null && sb.length() > 5) {
            sqlCriteria = " where " + sb.substring(5, sb.length());
        }
        
        // find order by
        if (sortBy.trim().length() != 0) {
            if ("SV".equals(sortBy)) {
                sqlCriteria += " ORDER BY " + (String)columnMap.get("SE") + " " + sqlSortByIn;
                sqlCriteria += "    ," + (String)columnMap.get("VA") + " " + sqlSortByIn;
            } else {
                sqlCriteria += " ORDER BY " + (String)columnMap.get(sortBy) + " " + sqlSortByIn;    
            }
        }
        return sqlCriteria;
    }
    
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException {
        BsaBsaServiceVariantMod mod = (BsaBsaServiceVariantMod) masterMod;
        
        StringBuffer errorMsgBuffer = new StringBuffer();
        try {
            if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)) {
                //insert(masterMod);
            } else if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)) {
                //update(masterMod);
            }
        } catch(CustomDataAccessException e) {
            errorMsgBuffer.append(e.getMessages()+"&");
        } catch(DataAccessException e) {
            e.printStackTrace();
        }
        
        //begin: delete record of bsa service variant
        if (isDeletes != null && !isDeletes.isEmpty()) { //if 1
            BsaBsaServiceVariantMod bean = null;
            ArrayList listDelete = new ArrayList(isDeletes.values());
            for (int i=0;i<listDelete.size();i++) { //for 1
                bean = (BsaBsaServiceVariantMod) listDelete.get(i);
                this.delete(bean);
                
            } //end for 1
        } //end if 1
        //end: delete record of bsa service variant
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
         private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_DEL_SERVICE_VARIANT";

         protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);
             declareParameter(new SqlParameter("p_bsa_service_variant_id", Types.INTEGER));
             declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
             declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
             compile();
         }
         
         protected boolean delete(final RrcStandardMod inputMod) {
             boolean isSuccess = false;
             if (inputMod instanceof BsaBsaServiceVariantMod) {
                 BsaBsaServiceVariantMod aInputMod = (BsaBsaServiceVariantMod) inputMod;
                 
                 Map inParameters = new HashMap(3);
                 inParameters.put("p_bsa_service_variant_id", new Integer((RutString.nullToStr(aInputMod.getBsaServiceVariantId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaServiceVariantId())));
                 inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                 inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                 
                 System.out.println("[BsaBsaServiceVariantJdbcDao][DeleteStoreProcedure][delete]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][DeleteStoreProcedure][delete]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][DeleteStoreProcedure][delete]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                 
                 execute(inParameters);
                 isSuccess = true;
             }
             return isSuccess;
         }
     }
   
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_INS_SERVICE_VARIANT";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_bsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_variant_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bsa_vessel_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_usage_rule", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_calc_frequency", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_calc_duration", Types.NUMERIC));    
            declareParameter(new SqlInOutParameter("p_def_slot_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_tons", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_reefer_plugs", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_avg_coc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_avg_soc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_min_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            /* Start */
//             declareParameter(new SqlInOutParameter("p_ref_variant_code", Types.VARCHAR));
//             declareParameter(new SqlInOutParameter("p_ref_bas_vessel_type", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_effect_date", Types.DATE));
             declareParameter(new SqlInOutParameter("p_expire_date", Types.DATE));
             declareParameter(new SqlInOutParameter("p_tolerant", Types.DECIMAL));
             declareParameter(new SqlInOutParameter("p_ser_var_type", Types.VARCHAR));
            /* End */
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof BsaBsaServiceVariantMod)&&(outputMod instanceof BsaBsaServiceVariantMod)) {
                BsaBsaServiceVariantMod aInputMod = (BsaBsaServiceVariantMod)inputMod;
                BsaBsaServiceVariantMod aOutputMod = (BsaBsaServiceVariantMod)outputMod;
                
                Map inParameters = new HashMap();
                
                
                inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getBsaServiceVariantId()));
                inParameters.put("p_bsa_model_id", RutDatabase.integerToDb(aInputMod.getBsaModelId()));
                inParameters.put("p_variant_code", RutDatabase.stringToDb(aInputMod.getVariantCode()));
                inParameters.put("p_proforma_id", RutDatabase.integerToDb(aInputMod.getProformaId()));
                inParameters.put("p_bsa_vessel_type", RutDatabase.stringToDb(aInputMod.getBsaVesselType()));
                inParameters.put("p_usage_rule", RutDatabase.stringToDb(aInputMod.getUsageRule()));
                inParameters.put("p_calc_frequency", RutDatabase.bigDecimalToDb(aInputMod.getCalcFrequency()));
                inParameters.put("p_calc_duration", RutDatabase.bigDecimalToDb(aInputMod.getCalcDuration()));
                inParameters.put("p_def_slot_teu", RutDatabase.integerToDb(aInputMod.getDefSlotTeu()));
                inParameters.put("p_def_slot_tons", RutDatabase.integerToDb(aInputMod.getDefSlotTons()));
                inParameters.put("p_def_slot_reefer_plugs", RutDatabase.integerToDb(aInputMod.getDefSlotReefer()));
                inParameters.put("p_def_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgCocTeuWeight()));
                inParameters.put("p_def_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgSocTeuWeight()));
                inParameters.put("p_def_min_teu", RutDatabase.integerToDb(aInputMod.getDefMinTeu()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_add_user", RutDatabase.stringToDb(aInputMod.getRecordAddUser()));
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                /* Start */
//                 inParameters.put("p_ref_variant_code", RutDatabase.stringToDb(aInputMod.getRefVariant()));
//                 inParameters.put("p_ref_bas_vessel_type", RutDatabase.stringToDb(aInputMod.getRefVesselType()));
                

                 inParameters.put("p_effect_date",RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getEffectiveDate()));
                 inParameters.put("p_expire_date", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getExpireDate()));
                 inParameters.put("p_tolerant", RutDatabase.bigDecimalToDb(aInputMod.getOverbookingPerc()));
                 inParameters.put("p_ser_var_type", RutDatabase.stringToDb(aInputMod.getServiceVariantType()));
                /* End */
 
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_bsa_model_id = "+inParameters.get("p_bsa_model_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_variant_code = "+inParameters.get("p_variant_code"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_proforma_id = "+inParameters.get("p_proforma_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_bsa_vessel_type = "+inParameters.get("p_bsa_vessel_type"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_usage_rule = "+inParameters.get("p_usage_rule"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_calc_frequency = "+inParameters.get("p_calc_frequency"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_calc_duration = "+inParameters.get("p_calc_duration"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_slot_teu = "+inParameters.get("p_def_slot_teu"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_slot_tons = "+inParameters.get("p_def_slot_tons"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_slot_reefer_plugs = "+inParameters.get("p_def_slot_reefer_plugs"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_avg_coc_teu_weight = "+inParameters.get("p_def_avg_coc_teu_weight"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_avg_soc_teu_weight = "+inParameters.get("p_def_avg_soc_teu_weight"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_min_teu = "+inParameters.get("p_def_min_teu"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_add_user = "+inParameters.get("p_record_add_user"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_add_date = "+inParameters.get("p_record_add_date"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                /* Start */
//                 System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_ref_variant_code = "+inParameters.get("p_ref_variant_code"));
//                 System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_ref_bas_vessel_type = "+inParameters.get("p_ref_bas_vessel_type"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_effect_date = "+inParameters.get("p_effect_date"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_expire_date = "+inParameters.get("p_expire_date"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_tolerant = "+inParameters.get("p_tolerant"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_ser_var_type = "+inParameters.get("p_ser_var_type"));
                 
                /* End */
                
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setBsaServiceVariantId(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
                    aOutputMod.setBsaModelId(RutDatabase.dbToStrInteger(outParameters, "p_bsa_model_id"));
                    aOutputMod.setVariantCode(RutDatabase.dbToString(outParameters, "p_variant_code"));
                    aOutputMod.setProformaId(RutDatabase.dbToString(outParameters, "p_proforma_id"));
                    aOutputMod.setBsaVesselType(RutDatabase.dbToString(outParameters, "p_bsa_vessel_type"));
                    aOutputMod.setUsageRule(RutDatabase.dbToString(outParameters, "p_usage_rule"));
                    aOutputMod.setCalcFrequency(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_frequency"));
                    aOutputMod.setCalcDuration(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_duration"));
                    aOutputMod.setDefSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_teu"));
                    aOutputMod.setDefSlotTons(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_tons"));
                    aOutputMod.setDefSlotReefer(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_reefer_plugs"));
                    aOutputMod.setDefAvgCocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_coc_teu_weight"));
                    aOutputMod.setDefAvgSocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_soc_teu_weight"));
                    aOutputMod.setDefMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_min_teu"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordAddUser(RutDatabase.dbToString(outParameters, "p_record_add_user"));
                    aOutputMod.setRecordAddDate(RutDatabase.dbToTimestamp(outParameters, "p_record_add_date"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    /* Start */
//                     aOutputMod.setRefVariant(RutDatabase.dbToString(outParameters, "p_ref_variant_code"));
//                     aOutputMod.setRefVesselType(RutDatabase.dbToString(outParameters, "p_ref_bas_vessel_type"));
                     aOutputMod.setEffectiveDate((((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_effect_date"))))));
                     aOutputMod.setExpireDate((((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_expire_date"))))));
                     aOutputMod.setOverbookingPerc(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolerant"));
                     aOutputMod.setServiceVariantType(RutDatabase.dbToString(outParameters, "p_ser_var_type"));
                    /* End */
                    
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_UPD_SERVICE_VARIANT";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            declareParameter(new SqlInOutParameter("p_bsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_variant_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bsa_vessel_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_usage_rule", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_calc_frequency", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_calc_duration", Types.NUMERIC));    
            declareParameter(new SqlInOutParameter("p_def_slot_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_tons", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_reefer_plugs", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_avg_coc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_avg_soc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_min_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            /* Start */
//             declareParameter(new SqlInOutParameter("p_ref_variant_code", Types.VARCHAR));
//             declareParameter(new SqlInOutParameter("p_ref_bas_vessel_type", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_effect_date", Types.DATE));
             declareParameter(new SqlInOutParameter("p_expire_date", Types.DATE));
             declareParameter(new SqlInOutParameter("p_tolerant", Types.DECIMAL));
            declareParameter(new SqlInOutParameter("p_ser_var_type", Types.VARCHAR));
            /* End */
            
            compile();
        }
        
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof BsaBsaServiceVariantMod)&&(outputMod instanceof BsaBsaServiceVariantMod)){
                BsaBsaServiceVariantMod aInputMod = (BsaBsaServiceVariantMod)inputMod;
                BsaBsaServiceVariantMod aOutputMod = (BsaBsaServiceVariantMod)outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getBsaServiceVariantId()));
                inParameters.put("p_bsa_model_id", RutDatabase.integerToDb(aInputMod.getBsaModelId()));
                inParameters.put("p_variant_code", RutDatabase.stringToDb(aInputMod.getVariantCode()));
                inParameters.put("p_proforma_id", RutDatabase.integerToDb(aInputMod.getProformaId()));
                inParameters.put("p_bsa_vessel_type", RutDatabase.stringToDb(aInputMod.getBsaVesselType()));
                inParameters.put("p_usage_rule", RutDatabase.stringToDb(aInputMod.getUsageRule()));
                inParameters.put("p_calc_frequency", RutDatabase.bigDecimalToDb(aInputMod.getCalcFrequency()));
                inParameters.put("p_calc_duration", RutDatabase.bigDecimalToDb(aInputMod.getCalcDuration()));
                inParameters.put("p_def_slot_teu", RutDatabase.integerToDb(aInputMod.getDefSlotTeu()));
                inParameters.put("p_def_slot_tons", RutDatabase.integerToDb(aInputMod.getDefSlotTons()));
                inParameters.put("p_def_slot_reefer_plugs", RutDatabase.integerToDb(aInputMod.getDefSlotReefer()));
                inParameters.put("p_def_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgCocTeuWeight()));
                inParameters.put("p_def_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgSocTeuWeight()));
                inParameters.put("p_def_min_teu", RutDatabase.integerToDb(aInputMod.getDefMinTeu()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                /* Start */
//                 inParameters.put("p_ref_variant_code", RutDatabase.stringToDb(aInputMod.getRefVariant()));
//                 inParameters.put("p_ref_bas_vessel_type", RutDatabase.stringToDb(aInputMod.getRefVesselType()));
                 inParameters.put("p_effect_date", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getEffectiveDate()));
                 inParameters.put("p_expire_date", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getExpireDate()));
                 inParameters.put("p_tolerant", RutDatabase.bigDecimalToDb(aInputMod.getOverbookingPerc()));
                inParameters.put("p_ser_var_type", RutDatabase.stringToDb(aInputMod.getServiceVariantType()));
                /* End */
                
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_bsa_model_id = "+inParameters.get("p_bsa_model_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_variant_code = "+inParameters.get("p_variant_code"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_proforma_id = "+inParameters.get("p_proforma_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_bsa_vessel_type = "+inParameters.get("p_bsa_vessel_type"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_usage_rule = "+inParameters.get("p_usage_rule"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_calc_frequency = "+inParameters.get("p_calc_frequency"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_calc_duration = "+inParameters.get("p_calc_duration"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_slot_teu = "+inParameters.get("p_def_slot_teu"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_slot_tons = "+inParameters.get("p_def_slot_tons"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_slot_reefer_plugs = "+inParameters.get("p_def_slot_reefer_plugs"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_avg_coc_teu_weight = "+inParameters.get("p_def_avg_coc_teu_weight"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_avg_soc_teu_weight = "+inParameters.get("p_def_avg_soc_teu_weight"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_min_teu = "+inParameters.get("p_def_min_teu"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                /* Start */
//                 System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_ref_variant_code = "+inParameters.get("p_ref_variant_code"));
//                 System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_ref_bas_vessel_type = "+inParameters.get("p_ref_bas_vessel_type"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_effect_date = "+inParameters.get("p_effect_date"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_expire_date = "+inParameters.get("p_expire_date"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_tolerant = "+inParameters.get("p_tolerant"));
                 System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_ser_var_type = "+inParameters.get("p_ser_var_type"));
                /* End */
                
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setBsaServiceVariantId(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
                    aOutputMod.setBsaModelId(RutDatabase.dbToStrInteger(outParameters, "p_bsa_model_id"));
                    aOutputMod.setVariantCode(RutDatabase.dbToString(outParameters, "p_variant_code"));
                    aOutputMod.setProformaId(RutDatabase.dbToStrInteger(outParameters, "p_proforma_id"));
                    aOutputMod.setBsaVesselType(RutDatabase.dbToString(outParameters, "p_bsa_vessel_type"));
                    aOutputMod.setUsageRule(RutDatabase.dbToString(outParameters, "p_usage_rule"));
                    aOutputMod.setCalcFrequency(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_frequency"));
                    aOutputMod.setCalcDuration(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_duration"));
                    aOutputMod.setDefSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_teu"));
                    aOutputMod.setDefSlotTons(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_tons"));
                    aOutputMod.setDefSlotReefer(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_reefer_plugs"));
                    aOutputMod.setDefAvgCocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_coc_teu_weight"));
                    aOutputMod.setDefAvgSocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_soc_teu_weight"));
                    aOutputMod.setDefMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_min_teu"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    /* Start */
//                     aOutputMod.setRefVariant(RutDatabase.dbToString(outParameters, "p_ref_variant_code"));
//                     aOutputMod.setRefVesselType(RutDatabase.dbToString(outParameters, "p_ref_bas_vessel_type"));
                    

                     aOutputMod.setEffectiveDate((((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_effect_date"))))));
                     aOutputMod.setExpireDate((((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_expire_date"))))));
                     aOutputMod.setOverbookingPerc(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolerant"));
                     aOutputMod.setServiceVariantType(RutDatabase.dbToString(outParameters, "p_ser_var_type"));
                    /* End */
                } 
            }
            return isSuccess;
        }
    }
     
    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
 
    public BsaBsaServiceVariantMod findByKeyBsaServiceVariantID(String bsaServiceVariantId) throws DataAccessException{
        BsaBsaServiceVariantMod bsaBsaServiceVariantMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select VR.BSA_SERVICE_VARIANT_ID ");
        sql.append("      ,VR.BSA_MODEL_ID  ");
        sql.append("      ,VR.SERVICE ");
        sql.append("      ,VR.VARIANT_CODE  ");
        sql.append("      ,VR.PROFORMA_REF_NO ");
        sql.append("      ,VR.PROFORMA_ID ");
        sql.append("      ,VR.BSA_VESSEL_TYPE ");
        sql.append("      ,VR.NO_VESSELS ");
        sql.append("      ,VR.ROTATION_DURATION  ");
        sql.append("      ,VR.USAGE_RULE ");
        sql.append("      ,VR.CALC_FREQUENCY ");
        sql.append("      ,VR.CALC_DURATION ");
        sql.append("      ,VR.DEF_SLOT_TEU  ");
        sql.append("      ,VR.DEF_SLOT_TONS ");
        sql.append("      ,VR.DEF_SLOT_REEFER ");
        sql.append("      ,VR.DEF_AVG_COC_TEU_WEIGHT ");
        sql.append("      ,VR.DEF_AVG_SOC_TEU_WEIGHT ");
        sql.append("      ,VR.DEF_MIN_TEU  ");
       // sql.append("      ,VR.OVERBOOKING_PERC ");
        sql.append("      ,VR.RECORD_STATUS ");
        sql.append("      ,VR.RECORD_ADD_USER ");
        sql.append("      ,VR.RECORD_ADD_DATE ");
        sql.append("      ,VR.RECORD_CHANGE_USER ");
        sql.append("      ,VR.RECORD_CHANGE_DATE ");
//        sql.append("      ,BSV.REF_VARIANT_CODE ");
//        sql.append("      ,BSV.REF_BSA_VESSEL_TYPE ");
        sql.append("      ,VR.EFFECTIVE_DATE ");
        sql.append("      ,VR.EXPIRY_DATE ");
        sql.append("      ,VR.TOLERANT ");
        sql.append("      ,VR.SERVICE_VARIANT_TYPE ");
        sql.append("from VR_BSA_SERVICE_VARIANT VR ");
        sql.append("WHERE VR.BSA_SERVICE_VARIANT_ID = :bsaServiceVariantId ");
        sql.append("ORDER BY BSA_SERVICE_VARIANT_ID ");
        try{
            bsaBsaServiceVariantMod = (BsaBsaServiceVariantMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("bsaServiceVariantId", bsaServiceVariantId),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            BsaBsaServiceVariantMod bsaBsaServiceVariantMod = new BsaBsaServiceVariantMod();
                            bsaBsaServiceVariantMod.setBsaServiceVariantId(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                            bsaBsaServiceVariantMod.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                            //bsaBsaServiceVariantMod.setModelName(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                            bsaBsaServiceVariantMod.setService(RutString.nullToStr(rs.getString("SERVICE")));
                            bsaBsaServiceVariantMod.setVariantCode(RutString.nullToStr(rs.getString("VARIANT_CODE")));
                            bsaBsaServiceVariantMod.setProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                            bsaBsaServiceVariantMod.setProformaId(RutString.nullToStr(rs.getString("PROFORMA_ID")));
                            bsaBsaServiceVariantMod.setBsaVesselType(RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE")));
                            bsaBsaServiceVariantMod.setNoVessels(RutString.nullToStr(rs.getString("NO_VESSELS")));
                            bsaBsaServiceVariantMod.setRotationDuration(RutString.nullToStr(rs.getString("ROTATION_DURATION")));
                            bsaBsaServiceVariantMod.setUsageRule(RutString.nullToStr(rs.getString("USAGE_RULE")));
                            bsaBsaServiceVariantMod.setCalcFrequency(RutString.nullToStr(rs.getString("CALC_FREQUENCY")));
                            bsaBsaServiceVariantMod.setCalcDuration(RutString.nullToStr(rs.getString("CALC_DURATION")));
                            bsaBsaServiceVariantMod.setDefSlotTeu(RutString.nullToStr(rs.getString("DEF_SLOT_TEU")));
                            bsaBsaServiceVariantMod.setDefSlotTons(RutString.nullToStr(rs.getString("DEF_SLOT_TONS")));
                            bsaBsaServiceVariantMod.setDefSlotReefer(RutString.nullToStr(rs.getString("DEF_SLOT_REEFER")));
                            bsaBsaServiceVariantMod.setDefAvgCocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_COC_TEU_WEIGHT")));
                            bsaBsaServiceVariantMod.setDefAvgSocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_SOC_TEU_WEIGHT")));
                            bsaBsaServiceVariantMod.setDefMinTeu(RutString.nullToStr(rs.getString("DEF_MIN_TEU")));
                           // bsaBsaServiceVariantMod.setOverbookingPerc(RutString.nullToStr(rs.getString("TOLERANT")));
                            
                           if(RutString.nullToStr(rs.getString("TOLERANT")).equals("")||rs.getString("TOLERANT").equals("0")){
                              bsaBsaServiceVariantMod.setOverbookingPerc("0.00");
                           }else{
                               DecimalFormat dc=new DecimalFormat();
                               dc.applyPattern("###.00");
                            bsaBsaServiceVariantMod.setOverbookingPerc(dc.format(rs.getDouble("TOLERANT")));
                           }  
                            
                            
                            bsaBsaServiceVariantMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            bsaBsaServiceVariantMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                            bsaBsaServiceVariantMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                            bsaBsaServiceVariantMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                            bsaBsaServiceVariantMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
//                            bsaBsaServiceVariantMod.setRefVariant(RutString.nullToStr(rs.getString("REF_VARIANT_CODE")));
//                            bsaBsaServiceVariantMod.setRefVesselType(RutString.nullToStr(rs.getString("REF_BSA_VESSEL_TYPE")));
                            bsaBsaServiceVariantMod.setEffectiveDate(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("EFFECTIVE_DATE")));
                            bsaBsaServiceVariantMod.setExpireDate(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("EXPIRY_DATE")));
                           bsaBsaServiceVariantMod.setServiceVariantType(RutString.nullToStr(rs.getString("SERVICE_VARIANT_TYPE")));
                            
                            return bsaBsaServiceVariantMod;
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            bsaBsaServiceVariantMod = null;
        }
        return bsaBsaServiceVariantMod;
    }
    
    public int findNoOfVesselsByProformaIdBsaVesselType(int proformaId, String bsaVesselType) throws DataAccessException {
        int noOfVessels = 0;
        StringBuffer sql = new StringBuffer();
        sql.append("select PCR_BSA_SERVICE_VARIANT.FR_GET_NO_OF_VESSEL(:proformaId, :bsaVesselType) as no_of_vessels from dual");
        try {
            HashMap map = new HashMap();
            map.put("proformaId", new Integer(proformaId));
            map.put("bsaVesselType", bsaVesselType);
        
            Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(
                    sql.toString(),
                    map, Integer.class);
            noOfVessels = result.intValue();
        } catch (EmptyResultDataAccessException e) {
            noOfVessels = 0;
        }
        
        return noOfVessels;
    }
    
    public int findVoyageDaysByServiceProformaNo(String serviceCode, String proformaNo) throws DataAccessException {
        int voyageDays = 0;
        StringBuffer sql = new StringBuffer();
        sql.append("select PCR_BSA_SERVICE_VARIANT.FR_GET_VOYAGE_DAYS(:serviceCode, :proformaNo) as voyage_days from dual");
        try {
            HashMap map = new HashMap();
            map.put("serviceCode", serviceCode);
            map.put("proformaNo", proformaNo);
        
            Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(
                    sql.toString(),
                    map, Integer.class);
            voyageDays = result.intValue();
        } catch (EmptyResultDataAccessException e) {
            voyageDays = 0;
        }
        return voyageDays;
    }
    
    public long calculateDurationByArrayNumber(long[] number) throws DataAccessException {
        long calculateDuration = 0;
        try {
            Long[] arLong = null;
            if (number != null && number.length > 0) {
                arLong = new Long[number.length];    
                for (int i=0;i<number.length;i++) {
                    arLong[i] = new Long(number[i]);
                }
            }
            
            calculateDuration = getCalculateDurationStoreProcedure.getCalculateDuration(arLong);
                        
        } catch(Exception e) {
            e.printStackTrace();
        }
        return calculateDuration;
    }
    
    protected class GetCalculateDurationStoreProcedure extends StoredProcedure {
        private static final String FUNCTION_NAME = "PCR_BSA_SERVICE_VARIANT.FR_CALCULATE_DURATION";
    
        protected GetCalculateDurationStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, FUNCTION_NAME);
            setFunction(true);
            declareParameter(new SqlOutParameter("p_calculate_duration", Types.INTEGER));
            declareParameter(new SqlParameter("p_voyage_days", Types.ARRAY));
            compile();
        }
    
        protected long getCalculateDuration(final Long[] voyageDays) {
            System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateDuration]: Started");
            long calculateDuration = 0;
            try {
                Map map = new HashMap(1);
                map.put("p_voyage_days", new ArraySqlTypeValue("ARRAY_NUMBER", voyageDays));
                
                //Begin: DEBUG
                if (voyageDays != null && voyageDays.length > 0) {
                    for (int i=0;i<voyageDays.length;i++) {
                        System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateDuration]: voyageDays["+i+"] = "+voyageDays[i]);
                    }
                }
                //END: DEBUG
                
                Map outMap = execute(map);
                if (outMap.size() > 0) {
                    calculateDuration = new Long(RutDatabase.dbToInteger(outMap, "p_calculate_duration")).longValue();
                }    
                
            } catch(Exception e) {
                e.printStackTrace();
            }
            System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateDuration]: calculateDuration = "+calculateDuration);
            System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateDuration]: Finished");
            return calculateDuration;
        }
    }
    
    public long calculateFrequencyByArrayNumber(long[] number) throws DataAccessException {
        long calculateFrequency = 0;
        try {
            Long[] arLong = null;
            if (number != null && number.length > 0) {
                arLong = new Long[number.length];    
                for (int i=0;i<number.length;i++) {
                    arLong[i] = new Long(number[i]);
                }
            }
            
            calculateFrequency = getCalculateFrequencyStoreProcedure.getCalculateFrequency(arLong);
                        
        } catch(Exception e) {
            e.printStackTrace();
        }
        return calculateFrequency;
    }
    
    protected class GetCalculateFrequencyStoreProcedure extends StoredProcedure {
        private static final String FUNCTION_NAME = "PCR_BSA_SERVICE_VARIANT.FR_CALCULATE_FREQUENCY";
    
        protected GetCalculateFrequencyStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, FUNCTION_NAME);
            setFunction(true);
            declareParameter(new SqlOutParameter("p_calculate_frequency", Types.INTEGER));
            declareParameter(new SqlParameter("p_numbers", Types.ARRAY));
            compile();
        }
    
        protected long getCalculateFrequency(final Long[] values) {
            System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateFrequency]: Started");
            long calculateFrequency = 0;
            try {
                Map map = new HashMap(1);
                map.put("p_numbers", new ArraySqlTypeValue("ARRAY_NUMBER", values));
                
                //Begin: DEBUG
                if (values != null && values.length > 0) {
                    for (int i=0;i<values.length;i++) {
                        System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateFrequency]: values["+i+"] = "+values[i]);
                    }
                }
                //END: DEBUG
                
                Map outMap = execute(map);
                if (outMap.size() > 0) {
                    calculateFrequency = new Long(RutDatabase.dbToInteger(outMap, "p_calculate_frequency")).longValue();
                }    
                
            } catch(Exception e) {
                e.printStackTrace();
            }
            System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateFrequency]: calculateFrequency = "+calculateFrequency);
            System.out.println("[BsaBsaServiceVariantJdbcDao][getCalculateFrequency]: Finished");
            return calculateFrequency;
        }
    }

/*Update Tolelant */
 public boolean updateTolelant(RrcStandardMod mod) throws DataAccessException{
     mod.setRecordChangeUser(this.getUserId());
     return updateStoreProcedureTolelant.updateTolelant(mod);
 }
 
    protected class UpdateStoreProcedureTolelant extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_UPD_ALL_TOLELANT_IN_SVZ_ID";
        
        protected UpdateStoreProcedureTolelant(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            declareParameter(new SqlParameter("p_bsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_tolerant", Types.DECIMAL));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
           // declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
          
            compile();
        }
        
        protected boolean updateTolelant(RrcStandardMod mod) {
            return updateTolelant(mod,mod);
        }
        
        protected boolean updateTolelant(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof BsaBsaServiceVariantMod)&&(outputMod instanceof BsaBsaServiceVariantMod)){
                BsaBsaServiceVariantMod aInputMod = (BsaBsaServiceVariantMod)inputMod;
                BsaBsaServiceVariantMod aOutputMod = (BsaBsaServiceVariantMod)outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getBsaServiceVariantId()));
                inParameters.put("p_tolerant", RutDatabase.bigDecimalToDb(aInputMod.getOverbookingPerc()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
               // inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
               
               
                
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_tolerant = "+inParameters.get("p_tolerant"));
                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
//                System.out.println("[BsaBsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
   
                Map outParameters = execute(inParameters);
//                if (outParameters.size() > 0) {
                    isSuccess = true;
//                    aOutputMod.setBsaServiceVariantId(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
//                    aOutputMod.setOverbookingPerc(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolerant"));
//                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setBsaServiceVariantId(aInputMod.getBsaServiceVariantId());
                    aOutputMod.setOverbookingPerc(aInputMod.getOverbookingPerc());
                    aOutputMod.setRecordChangeUser(aInputMod.getRecordChangeUser());
//                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                     
//                } 
            }
            return isSuccess;
        }
    }

}

