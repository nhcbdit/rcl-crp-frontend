package com.rclgroup.dolphin.web.dao.ijs;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface IjsReasonDao {
    /**
     * check valid of reasonCode#
     * @param reasonCode     
     * @return valid of reasonCode
     * @throws DataAccessException
     */
    public boolean isValid(String reasonCode) throws DataAccessException;
        
    /**
     * list Reson records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of reasonCode
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
}
