package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import org.springframework.dao.DataAccessException;

public interface DexCocSocFreightListDao extends RriStandardDao{
    /**
     * generate a BL record
     * @param mod
     * @return wheter generation is successful
     * @throws DataAccessException error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean generateTempBL(RrcStandardMod mod) throws DataAccessException;
}
