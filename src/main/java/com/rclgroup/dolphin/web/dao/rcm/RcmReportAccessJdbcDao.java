package com.rclgroup.dolphin.web.dao.rcm;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.dao.bkg.BkgBookingAvailabilityReportJdbcDao;
import com.rclgroup.dolphin.web.model.bkg.BkgBookingAvailabilityReportMod;
import com.rclgroup.dolphin.web.model.rcm.RcmReportAccessMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class RcmReportAccessJdbcDao extends RrcStandardDao implements RcmReportAccessDao{ 

    private RcmReportAccessStoreProcedure rcmReportAccessStoreProcedure;
    
    public RcmReportAccessJdbcDao() {
    }

    public boolean updateReportAccess(RcmReportAccessMod mod ) {
        return rcmReportAccessStoreProcedure.updateReportAccess(mod);
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        rcmReportAccessStoreProcedure = new RcmReportAccessStoreProcedure(getJdbcTemplate());
    }

   
    protected class RcmReportAccessStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_RCM_REPORT_ACCESS.PRR_INSERT_REPORT_ACCESS";
        
        protected RcmReportAccessStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_report_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_level", Types.VARCHAR));
            declareParameter(new SqlParameter("p_region", Types.VARCHAR));
            declareParameter(new SqlParameter("p_agent", Types.VARCHAR));
            declareParameter(new SqlParameter("p_fsc", Types.VARCHAR));
          //  declareParameter(new SqlParameter("p_order_no", Types.NUMERIC));
           
            compile();
        }
        
              
        protected boolean updateReportAccess(final RcmReportAccessMod inputMod) {
            System.out.println("[RcmReportAccessJdbcDao][RcmReportAccessStoreProcedure]: updateReportAccess");
            boolean isSuccess = false;
                          
                Map inParameters = new HashMap();
                inParameters.put("p_user_id", inputMod.getUserLoginId());
                inParameters.put("p_session_id", inputMod.getSessionId());
                inParameters.put("p_report_code", inputMod.getReportCode());
                inParameters.put("p_level", inputMod.getLevel());
                inParameters.put("p_region", inputMod.getRegion());
                inParameters.put("p_agent", inputMod.getAgent());
                inParameters.put("p_fsc", inputMod.getFsc());
               // inParameters.put("p_order_no", inputMod.getReportGenerateNo());
                               
                System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_user_id = "+inParameters.get("p_user_id"));
                System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_report_code = "+inParameters.get("p_report_code"));
                System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_level = "+inParameters.get("p_level"));
                System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_region = "+inParameters.get("p_region"));
                System.out.println("[BkgBookingListJdbcDao][updateReportAccess]: p_agent = "+inParameters.get("p_agent"));
                System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_fsc = "+inParameters.get("p_fsc"));
               // System.out.println("[RcmReportAccessJdbcDao][updateReportAccess]: p_order_no = "+inParameters.get("p_order_no"));
                               
                try{
                    System.out.println("[RcmReportAccessJdbcDao][RcmReportAccessStoreProcedure]: updateReportAccess: execute");
                    this.execute(inParameters);
                    isSuccess = true;
                }catch(Exception e){
                    System.out.print("Error in report access log : "+e.getMessage());
                    isSuccess =false;
                }
                
            return isSuccess;
        }
    }
}
