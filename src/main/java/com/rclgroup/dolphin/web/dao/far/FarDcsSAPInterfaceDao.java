package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.model.far.FarBookingPartyMappingMod;

import java.util.List;

import java.util.Map;

import org.springframework.dao.DataAccessException;

public interface FarDcsSAPInterfaceDao {

    
    
    public     List listSAPTax(String table,String country,String FSC,String chargeCode,String taxCode,String taxRate,String sapTaxCode) throws DataAccessException;
    public    Map<String,String> insertIntoTable(String table,String country,String FSC,String chargeCode,String taxCode,String taxRate,String sapTaxCode,String remark,String user) throws DataAccessException;

}
