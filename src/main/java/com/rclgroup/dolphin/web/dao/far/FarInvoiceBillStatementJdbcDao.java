/*--------------------------------------------------------
FarInvoiceBillStatementJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Porntip Aramrattana 17/06/2009
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
01 23/09/14  BHAMOH1  488    Update queries in countListForStatementByDetail, listForStatementByDetail to filter by 'Invoice Print Date'. Change the query where condition for filter by 'Invoice Date' to use INVOICE_DATE col instead of RECORD_ADD_DATE col
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.far.FarInvoicBillStatementMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class FarInvoiceBillStatementJdbcDao extends RrcStandardDao implements FarInvoiceBillStatementDao{
    private UpdateStoreProcedure updateStoreProcedure;
    
    protected void initDao() throws Exception {
        super.initDao();
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
    }
    
    public List listForStatementByHeader(String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String description,String line,String region,String agent,String fsc,String status) throws DataAccessException {
        return this.listForStatementByHeader(0, 0, billDateFrom, billDateTo, billNumFrom, billNumTo, description,line,region,agent, fsc, status);
    }
    
    public List listForStatementByHeader(int rowAt, int rowTo, String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String description,String line,String region,String agent,String fsc,String status) {
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByHeader]: Started");
        
        String sqlColumn = "FSC, BILL_TO_PARTY, BILL_TO_PARTY_NAME, BILL_NUM, BILL_VERSION, BILL_DATE, DESCRIPTION,BILL_STATUS";
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct FSC ");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,BILL_TO_PARTY_NAME ");
        sql.append("    ,BILL_NUM ");
        sql.append("    ,BILL_VERSION ");
        sql.append("    ,BILL_DATE ");
        sql.append("    ,DESCRIPTION ");
        sql.append("    ,BILL_STATUS ");
        sql.append("from VR_FAR_STATEMENT_BY_HEADER sh ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(billDateFrom)) {
            strWhere.append(" and BILL_DATE >= '"+billDateFrom+"' ");
        }
        if (!RutString.isEmptyString(billDateTo)) {
            strWhere.append(" and BILL_DATE <= '"+billDateTo+"' ");
        }
        if (!RutString.isEmptyString(billNumFrom)) {
            strWhere.append(" and BILL_NUM >= '"+billNumFrom+"' ");
        }
        if (!RutString.isEmptyString(billNumTo)) {
            strWhere.append(" and BILL_NUM <= '"+billNumTo+"' ");
        }
        if (!RutString.isEmptyString(description)) {
            strWhere.append(" and DESCRIPTION LIKE '%"+description +"%'");
        }
        if(!RutString.isEmptyString(status) && !"A".equals(status)) {
            strWhere.append(" and BILL_STATUS = '"+status +"'");
        }
        
        if (!"R".equals(fsc)) { //if 1
            if (!RutString.isEmptyString(line) || !RutString.isEmptyString(region) || !RutString.isEmptyString(agent) || !RutString.isEmptyString(fsc)) { //if 2
                strWhere.append(" and exists ( ");
                strWhere.append("       select 1 ");
                strWhere.append("       from VR_CAM_FSC vc ");
                strWhere.append("       where FSC_CODE = sh.FSC ");
                
                if (!RutString.isEmptyString(line)) {
                    strWhere.append("            and LINE_CODE = '"+line +"'");
                }  
                if (!RutString.isEmptyString(region)) {
                    strWhere.append("            and REGION_CODE = '"+region +"'");
                }  
                if (!RutString.isEmptyString(agent)) {
                    strWhere.append("            and AGENT_CODE = '"+agent +"'");
                }  
                if (!RutString.isEmptyString(fsc)) {
                    strWhere.append("            and FSC_CODE = '"+fsc +"'");
                }
                strWhere.append("            and rownum = 1 ");
                strWhere.append("   ) ");
                                
            } //end if 2
        } //end if 1
        
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
        
        // Query by record number range
        sql = this.addSqlForNewHelp(sql, rowAt, rowTo, sqlColumn);
        
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByHeader]: SQL ="+sql.toString());
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByHeader]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           FarInvoicBillStatementMod fs = new FarInvoicBillStatementMod();
                           fs.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           fs.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                           fs.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                           fs.setBillNo(RutString.nullToStr(rs.getString("BILL_NUM")));
                           fs.setVersion(rs.getInt("BILL_VERSION"));
                           fs.setBillDate(RutString.nullToStr(rs.getString("BILL_DATE")));
                           fs.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));
                           fs.setBillStatus(RutString.nullToStr(rs.getString("BILL_STATUS")));
                           return fs;
                    }
                });
    }
    
    public int countListForStatementByHeader(String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String description,String line,String region,String agent,String fsc,String status) {
        System.out.println("[FarInvoiceBillStatementJdbcDao][countListForStatementByHeader]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct FSC ");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,BILL_TO_PARTY_NAME ");
        sql.append("    ,BILL_NUM ");
        sql.append("    ,BILL_VERSION ");
        sql.append("    ,BILL_DATE ");
        sql.append("    ,DESCRIPTION ");
        sql.append("    ,BILL_STATUS ");
        sql.append("from VR_FAR_STATEMENT_BY_HEADER sh ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(billDateFrom)) {
            strWhere.append(" and BILL_DATE >= '"+billDateFrom+"' ");
        }
        if (!RutString.isEmptyString(billDateTo)) {
            strWhere.append(" and BILL_DATE <= '"+billDateTo+"' ");
        }
        if (!RutString.isEmptyString(billNumFrom)) {
            strWhere.append(" and BILL_NUM >= '"+billNumFrom+"' ");
        }
        if (!RutString.isEmptyString(billNumTo)) {
            strWhere.append(" and BILL_NUM <= '"+billNumTo+"' ");
        }
        if (!RutString.isEmptyString(description)) {
            strWhere.append(" and DESCRIPTION LIKE '%"+description +"%'");
        }
        if(!RutString.isEmptyString(status) && !"A".equals(status)) {
            strWhere.append(" and BILL_STATUS = '"+status +"'");
        }
         
        if (!"R".equals(fsc)) { //if 1
            if (!RutString.isEmptyString(line) || !RutString.isEmptyString(region) || !RutString.isEmptyString(agent) || !RutString.isEmptyString(fsc)) { //if 2
                strWhere.append(" and exists ( ");
                strWhere.append("       select 1 ");
                strWhere.append("       from VR_CAM_FSC vc ");
                strWhere.append("       where FSC_CODE = sh.FSC ");
                
                if (!RutString.isEmptyString(line)) {
                    strWhere.append("            and LINE_CODE = '"+line +"'");
                }  
                if (!RutString.isEmptyString(region)) {
                    strWhere.append("            and REGION_CODE = '"+region +"'");
                }  
                if (!RutString.isEmptyString(agent)) {
                    strWhere.append("            and AGENT_CODE = '"+agent +"'");
                }  
                if (!RutString.isEmptyString(fsc)) {
                    strWhere.append("            and FSC_CODE = '"+fsc +"'");
                }
                strWhere.append("            and rownum = 1 ");
                strWhere.append("   ) ");
                                 
            } //end if 2
        } //end if 1
         
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
         
        // Find a number of all data
        sql = this.getNumberOfAllData(sql);
        
        System.out.println("[FarInvoiceBillStatementJdbcDao][countListForStatementByHeader]: SQL ="+sql.toString());
        System.out.println("[FarInvoiceBillStatementJdbcDao][countListForStatementByHeader]: Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(), new HashMap(), Integer.class);
    }
    
    public List listForStatementByHeaderWithLinkage(String billDateFrom,String billDateTo,String billNumFrom,String billNumTo,String billToParty,String line,String region,String agent ,String fsc ,String status) throws DataAccessException {
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByHeaderWithLinkage]: Started");
        StringBuffer sql = new StringBuffer();
        sql.append("select FSC ");
        sql.append("    ,BILL_TO_PARTY ");
        sql.append("    ,BILL_TO_PARTY_NAME ");
        sql.append("    ,BILL_NUM ");
        sql.append("    ,BILL_VERSION ");
        sql.append("    ,BILL_DATE ");
        sql.append("    ,DESCRIPTION ");
        sql.append("from VR_FAR_STATEMENT_BY_HEADER ");
        sql.append("where 1=1 ");
        
        // create where condition
        if(!RutString.isEmptyString(billDateFrom)){
            sql.append("    and BILL_DATE >= '"+ billDateFrom + "'");
        }
        if(!RutString.isEmptyString(billDateTo)){
            sql.append("    and BILL_DATE <= '" + billDateTo + "'");
        }
        if(!RutString.isEmptyString(billNumFrom)){
            sql.append("    and BILL_NUM >= '" + billNumFrom + "'");
        }
        if(!RutString.isEmptyString(billNumTo)){
            sql.append("    and BILL_NUM <= '"+billNumTo+ "'");
        }
        if(!RutString.isEmptyString(billToParty)){
            sql.append("    and BILL_TO_PARTY LIKE '%"+billToParty +"%'");
        }
        if(!RutString.isEmptyString(status) && !"A".equals(status)){
            sql.append("    and BILL_STATUS = '"+status +"'");
        }
        
        sql.append(" and FSC IN (SELECT FSC_CODE " );
        sql.append("            FROM VR_CAM_FSC " );
        sql.append("            WHERE 1 = 1 " );
        
        if(!RutString.isEmptyString(line)){
            sql.append("    and LINE_CODE = '"+line +"'");
        }  
        if(!RutString.isEmptyString(region)){
            sql.append("    and REGION_CODE = '"+region +"'");
        }  
        if(!RutString.isEmptyString(agent)){
            sql.append("    and AGENT_CODE = '"+agent +"'");
        }  
        if(!RutString.isEmptyString(fsc)){
            sql.append("    and FSC_CODE = '"+fsc +"'");
        }
        sql.append("            ) ");
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByHeaderWithLinkage]: SQL ="+sql.toString());
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByHeaderWithLinkage]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           FarInvoicBillStatementMod fs = new FarInvoicBillStatementMod();
                           fs.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           fs.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                           fs.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                           fs.setBillNo(RutString.nullToStr(rs.getString("BILL_NUM")));
                           fs.setVersion(rs.getInt("BILL_VERSION"));
                           fs.setBillDate(RutString.nullToStr(rs.getString("BILL_DATE")));
                           fs.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));
                           return fs;
                    }
                });
    }

    public List listForStatementByDetail(String billToPartyName, String dateType, String fromDate, String toDate, String service, String vessel, String voyage, String port, String shipmentDirection, String blNo, String bkgNo, String invoiceNo, String invoiceStatus, String sortBy, String sortByIn, String line, String region, String agent, String fsc, String status) throws DataAccessException {
        return this.listForStatementByDetail(0, 0, billToPartyName, dateType, fromDate, toDate, service, vessel, voyage, port, shipmentDirection, blNo, bkgNo, invoiceNo, invoiceStatus, sortBy, sortByIn, line, region, agent, fsc, status);
    }
    
    public List listForStatementByDetail(int rowAt, int rowTo, String billToPartyName, String dateType, String fromDate, String toDate, String service, String vessel, String voyage, String port, String shipmentDirection, String blNo, String bkgNo, String invoiceNo, String invoiceStatus, String sortBy, String sortByIn, String line, String region, String agent, String fsc, String status) {
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByDetail]: Started");
        
        String sqlColumn = "FSC, BILL_TO_PARTY, BILL_TO_PARTY_NAME, BILL_NUM, BILL_VERSION, BILL_DATE, DESCRIPTION, BILL_STATUS";
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct vsd.FSC ");
        sql.append("    ,vsd.BILL_TO_PARTY ");
        sql.append("    ,vsd.BILL_TO_PARTY_NAME ");
        sql.append("    ,vsd.BILL_NUM ");
        sql.append("    ,vsd.BILL_VERSION ");
        sql.append("    ,vsd.BILL_DATE ");
        sql.append("    ,vsd.DESCRIPTION ");
        sql.append("    ,vsd.BILL_STATUS ");
        sql.append("from VR_FAR_STATEMENT_BY_DETAIL vsd ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(billToPartyName)) {
            strWhere.append(" and vsd.BILL_TO_CUSTOMER = '"+billToPartyName+"' ");
        }
        if (!RutString.isEmptyString(service)) {
            strWhere.append(" and vsd.SERVICE = '"+service+"' ");
        }
        if (!RutString.isEmptyString(vessel)) {
            strWhere.append(" and vsd.VESSEL = '"+vessel+"' ");
        }
        if (!RutString.isEmptyString(voyage)) {
            strWhere.append(" and vsd.VOYAGE = '"+voyage+"' ");
        }
        if (!RutString.isEmptyString(blNo)) {
            strWhere.append(" and vsd.APP_REFERENCE = '"+blNo+"' ");
        }
        if (!RutString.isEmptyString(bkgNo)) {
            strWhere.append(" and vsd.BOOKING_NO = '"+bkgNo+"' ");
        }
        if (!RutString.isEmptyString(invoiceNo)) {
            strWhere.append(" and vsd.INVOICE_NO = '"+invoiceNo+"' ");
        }
        if (!RutString.isEmptyString(status) && !"A".equals(status)) {
            strWhere.append(" and vsd.BILL_STATUS = '"+status+"' ");
        }
        
        // where condition: fromDate & toDate
        if ("I".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vsd.INVOICE_DATE >= "+fromDate+" ");//#01 BHAMOH1
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vsd.INVOICE_DATE <= "+toDate+" ");//#01 BHAMOH1
            }
        }else if ("P".equals(dateType)) {//#01 BHAMOH1 START
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vsd.INVOICE_PRINTING_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vsd.INVOICE_PRINTING_DATE <= "+toDate+" ");
            }//#01 BHAMOH1 END
        } else if ("S".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vsd.SAIL_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vsd.SAIL_DATE <= "+toDate+" ");
            }
        }
        
        // where condition: port
        if ("I".equals(shipmentDirection)) {
            if (!RutString.isEmptyString(port)) {
                strWhere.append(" and vsd.PORT_OF_DISCHARGE = '"+port+"' ");
            }
        } else if ("O".equals(shipmentDirection)) {
            if (!RutString.isEmptyString(port)) {
                strWhere.append(" and vsd.PORT_OF_LOAD = '"+port+"' ");
            }
        } else if ("X".equals(shipmentDirection)) {
            if (!RutString.isEmptyString(port)) {
                strWhere.append(" and vsd.PORT_OF_DISCHARGE <> '"+port+"' ");
                strWhere.append(" and vsd.PORT_OF_LOAD <> '"+port+"' ");
            }
        }
        
        // where condition: invoiceStatus
        if (!RutString.isEmptyString(invoiceStatus) && !"A".equals(invoiceStatus)) {
            strWhere.append(" and vsd.INVOICE_STATUS = '"+invoiceStatus+"' ");
        }
        
        if (!"R".equals(fsc)) { //if 1
            if (!RutString.isEmptyString(line) || !RutString.isEmptyString(region) || !RutString.isEmptyString(agent) || !RutString.isEmptyString(fsc)) { //if 2
                strWhere.append(" and exists ( ");
                strWhere.append("       select 1 ");
                strWhere.append("       from VR_CAM_FSC vc ");
                strWhere.append("       where FSC_CODE = vsd.FSC ");
                
                if (!RutString.isEmptyString(line)) {
                    strWhere.append("            and LINE_CODE = '"+line +"'");
                }  
                if (!RutString.isEmptyString(region)) {
                    strWhere.append("            and REGION_CODE = '"+region +"'");
                }  
                if (!RutString.isEmptyString(agent)) {
                    strWhere.append("            and AGENT_CODE = '"+agent +"'");
                }  
                if (!RutString.isEmptyString(fsc)) {
                    strWhere.append("            and FSC_CODE = '"+fsc +"'");
                }
                strWhere.append("            and rownum = 1 ");
                strWhere.append("   ) ");
                                 
            } //end if 2
        } //end if 1
        
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
         
        String sqlCriteriaSortBy = createSqlCriteriaSortBy(sortBy, sortByIn);
        sql.append(sqlCriteriaSortBy);
        
        // Query by record number range
        sql = this.addSqlForNewHelp(sql, rowAt, rowTo, sqlColumn);
        
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByDetail]: sql = "+sql.toString());
        System.out.println("[FarInvoiceBillStatementJdbcDao][listForStatementByDetail]: Finished");
        return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           FarInvoicBillStatementMod fs = new FarInvoicBillStatementMod();
                           fs.setFsc(RutString.nullToStr(rs.getString("FSC")));
                           fs.setBillToParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
                           fs.setBillToPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                           fs.setBillNo(RutString.nullToStr(rs.getString("BILL_NUM")));
                           fs.setVersion(rs.getInt("BILL_VERSION"));
                           fs.setBillDate(RutString.nullToStr(rs.getString("BILL_DATE")));
                           fs.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));
                           fs.setBillStatus(RutString.nullToStr(rs.getString("BILL_STATUS")));
                           return fs;
                    }
                });
    }
    
    public int countListForStatementByDetail(String billToPartyName, String dateType, String fromDate, String toDate, String service, String vessel, String voyage, String port, String shipmentDirection, String blNo, String bkgNo, String invoiceNo, String invoiceStatus, String sortBy, String sortByIn, String line, String region, String agent, String fsc, String status) {
        System.out.println("[FarInvoiceBillStatementJdbcDao][countListForStatementByDetail]: Started");
        
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct vsd.FSC ");
        sql.append("    ,vsd.BILL_TO_PARTY ");
        sql.append("    ,vsd.BILL_TO_PARTY_NAME ");
        sql.append("    ,vsd.BILL_NUM ");
        sql.append("    ,vsd.BILL_VERSION ");
        sql.append("    ,vsd.BILL_DATE ");
        sql.append("    ,vsd.DESCRIPTION ");
        sql.append("    ,vsd.BILL_STATUS ");
        sql.append("from VR_FAR_STATEMENT_BY_DETAIL vsd ");
        
        // create where condition
        StringBuffer strWhere = new StringBuffer("");
        if (!RutString.isEmptyString(billToPartyName)) {
            strWhere.append(" and vsd.BILL_TO_CUSTOMER = '"+billToPartyName+"' ");
        }
        if (!RutString.isEmptyString(service)) {
            strWhere.append(" and vsd.SERVICE = '"+service+"' ");
        }
        if (!RutString.isEmptyString(vessel)) {
            strWhere.append(" and vsd.VESSEL = '"+vessel+"' ");
        }
        if (!RutString.isEmptyString(voyage)) {
            strWhere.append(" and vsd.VOYAGE = '"+voyage+"' ");
        }
        if (!RutString.isEmptyString(blNo)) {
            strWhere.append(" and vsd.APP_REFERENCE = '"+blNo+"' ");
        }
        if (!RutString.isEmptyString(bkgNo)) {
            strWhere.append(" and vsd.BOOKING_NO = '"+bkgNo+"' ");
        }
        if (!RutString.isEmptyString(invoiceNo)) {
            strWhere.append(" and vsd.INVOICE_NO = '"+invoiceNo+"' ");
        }
            if (!RutString.isEmptyString(status) && !"A".equals(status)) {
                strWhere.append(" and vsd.BILL_STATUS = '"+status+"' ");
            }
        
        // where condition: fromDate & toDate
        if ("I".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vsd.INVOICE_DATE >= "+fromDate+" ");//#01 BHAMOH1
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vsd.INVOICE_DATE <= "+toDate+" ");//#01 BHAMOH1
            }
        } else if ("P".equals(dateType)) {//#01 BHAMOH1 START
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vsd.INVOICE_PRINTING_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vsd.INVOICE_PRINTING_DATE <= "+toDate+" ");
            }//#01 BHAMOH1 END
        } else if ("S".equals(dateType)) {
            if (!RutString.isEmptyString(fromDate)) {
                strWhere.append(" and vsd.SAIL_DATE >= "+fromDate+" ");
            }
            if (!RutString.isEmptyString(toDate)) {
                strWhere.append(" and vsd.SAIL_DATE <= "+toDate+" ");
            }
        }
        
        // where condition: port
        if ("I".equals(shipmentDirection)) {
            if (!RutString.isEmptyString(port)) {
                strWhere.append(" and vsd.PORT_OF_DISCHARGE = '"+port+"' ");
            }
        } else if ("O".equals(shipmentDirection)) {
            if (!RutString.isEmptyString(port)) {
                strWhere.append(" and vsd.PORT_OF_LOAD = '"+port+"' ");
            }
        } else if ("X".equals(shipmentDirection)) {
            if (!RutString.isEmptyString(port)) {
                strWhere.append(" and vsd.PORT_OF_DISCHARGE <> '"+port+"' ");
                strWhere.append(" and vsd.PORT_OF_LOAD <> '"+port+"' ");
            }
        }
        
        // where condition: invoiceStatus
        if (!RutString.isEmptyString(invoiceStatus) && !"A".equals(invoiceStatus)) {
            strWhere.append(" and vsd.INVOICE_STATUS = '"+invoiceStatus+"' ");
        }
        
        if (!"R".equals(fsc)) { //if 1
            if (!RutString.isEmptyString(line) || !RutString.isEmptyString(region) || !RutString.isEmptyString(agent) || !RutString.isEmptyString(fsc)) { //if 2
                strWhere.append(" and exists ( ");
                strWhere.append("       select 1 ");
                strWhere.append("       from VR_CAM_FSC vc ");
                strWhere.append("       where FSC_CODE = vsd.FSC ");
                
                if (!RutString.isEmptyString(line)) {
                    strWhere.append("            and LINE_CODE = '"+line +"'");
                }  
                if (!RutString.isEmptyString(region)) {
                    strWhere.append("            and REGION_CODE = '"+region +"'");
                }  
                if (!RutString.isEmptyString(agent)) {
                    strWhere.append("            and AGENT_CODE = '"+agent +"'");
                }  
                if (!RutString.isEmptyString(fsc)) {
                    strWhere.append("            and FSC_CODE = '"+fsc +"'");
                }
                strWhere.append("            and rownum = 1 ");
                strWhere.append("   ) ");
                                 
            } //end if 2
        } //end if 1
        
        if (strWhere != null && strWhere.length() > 0) {
            sql.append("where "+strWhere.substring(4));
        }
         
        // Find a number of all data
        sql = this.getNumberOfAllData(sql);
        
        System.out.println("[FarInvoiceBillStatementJdbcDao][countListForStatementByDetail]: sql = "+sql.toString());
        System.out.println("[FarInvoiceBillStatementJdbcDao][countListForStatementByDetail: Finished");
        return (int) getNamedParameterJdbcTemplate().queryForObject(sql.toString(), new HashMap(), Integer.class);
    }
  
    private String createSqlCriteriaSortBy (String sortBy, String sortByIn) {
        System.out.println("[FarInvoiceBillStatementJdbcDao][createSqlCriteriaSortBy]: Started");
        String sqlCriteria = "";
        String sqlSortByIn = "";
        if (!RutString.isEmptyString(sortBy)) {
            if ("A".equals(sortByIn)) {
                sqlSortByIn = "ASC";
            } else if ("D".equals(sortByIn)) {
                sqlSortByIn = "DESC";
            }
            
            Map map = new HashMap();
            map.put("FSC", "FSC");
            map.put("BILL_TO_PARTY", "BILL_TO_PARTY");
            map.put("BILL_TO_PARTY_NAME", "BILL_TO_PARTY_NAME");
            map.put("BILL_NUM", "BILL_NUM");
            map.put("VERSION", "VERSION");
            map.put("BILL_DATE", "BILL_DATE");
            map.put("DESCRIPTION", "DESCRIPTION");
            
            sqlCriteria = " order by "+ map.get(sortBy)+" "+sqlSortByIn;
        }
        System.out.println("[FarInvoiceBillStatementJdbcDao][createSqlCriteriaSortBy]: sql = "+sqlCriteria);
        System.out.println("[FarInvoiceBillStatementJdbcDao][createSqlCriteriaSortBy]: Finished");
        return sqlCriteria;
    }

    public void updateStatement(String billNo, String version) throws DataAccessException{
        updateStoreProcedure.updateStatement(billNo,version);
    }


    protected class UpdateStoreProcedure extends StoredProcedure{
        private static final String STORED_PROCEDURE_NAME = "PCR_FAR_SPECIAL_BILLING.PRR_UPD_STATEMENT_CANCELLATION";
        
        protected  UpdateStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_bill_number",Types.VARCHAR));
            declareParameter(new SqlParameter("p_bill_version",Types.NUMERIC));
            compile();
        }
        
        protected void updateStatement(String billNo,String version){
            Map inParameters = new HashMap(2);
            inParameters.put("p_bill_number",billNo);
            inParameters.put("p_bill_version",version);
            execute(inParameters);
            
        }
    }
 
    public String findTemplateCodeByBill(String billNo, int billVersion) {
        System.out.println("[FarInvoiceBillStatementJdbcDao][findTemplateCodeByBill]: Started");
        
        String templateCode = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select PCR_FAR_SPECIAL_BILLING.FR_GET_TEMPLATE_BY_BILL(:billNo, :billVersion) as TEMPLATE_CODE ");
        sql.append("from dual ");
        
        HashMap map = new HashMap();
        map.put("billNo", billNo);
        map.put("billVersion", new Integer(billVersion));
        
        System.out.println("[FarInvoiceBillStatementJdbcDao][findTemplateCodeByBill]: sql = "+sql.toString());
        templateCode = (String) getNamedParameterJdbcTemplate().queryForObject(
                                    sql.toString(), 
                                    map, 
                                    new RowMapper(){
                                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                                           return RutString.nullToStr(rs.getString("TEMPLATE_CODE"));
                                        }
                                    }
                                );
        
        System.out.println("[FarInvoiceBillStatementJdbcDao][findTemplateCodeByBill]: templateCode = "+templateCode);
        System.out.println("[FarInvoiceBillStatementJdbcDao][findTemplateCodeByBill]: Finished");
        return templateCode;
    }
    
}
