/*------------------------------------------------------
QtnTermJdbcDao.Jjava
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Manop Wanngam 15/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.qtn.QtnTermMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class QtnTermJdbcDao extends RrcStandardDao implements QtnTermDao {
    public QtnTermJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String TermCode) throws DataAccessException {
        System.out.println("[QtnTermJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERM_CODE ");
        sql.append("FROM VR_QTN_TERM ");
        sql.append("WHERE TERM_CODE = :TermCode");
        HashMap map = new HashMap();
        map.put("TermCode",TermCode);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[QtnTermJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public boolean isValid(String termCode, String status) throws DataAccessException {
        System.out.println("[QtnTermJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERM_CODE ");
        sql.append("FROM VR_QTN_TERM ");
        sql.append("WHERE TERM_CODE = :termCode ");
        sql.append("AND TERM_STATUS = :status ");
        HashMap map = new HashMap();
        map.put("termCode",termCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[QtnTermJdbcDao][isValid]: With status: Finished");
        return isValid;
    }    
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[QtnTermJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERM_CODE ");
        sql.append("      ,TERM_DESCRIPTION ");            
        sql.append("      ,TERM_STATUS "); 
        sql.append("FROM VR_QTN_TERM ");
        sql.append(sqlCriteria);
        System.out.println("[QtnTermJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[QtnTermJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());  
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "WHERE TERM_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "WHERE TERM_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE TERM_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }    
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[QtnTermJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERM_CODE ");
        sql.append("      ,TERM_DESCRIPTION ");            
        sql.append("      ,TERM_STATUS "); 
        sql.append("FROM VR_QTN_TERM ");
        sql.append("WHERE TERM_STATUS = :status ");
        sql.append(sqlCriteria);
        HashMap map = new HashMap();
        map.put("status",status);
        System.out.println("[QtnTermJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());  
    }
    
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND TERM_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND TERM_DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND TERM_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }    
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private QtnTermMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        QtnTermMod termCode = new QtnTermMod();
        termCode.setTermCode(RutString.nullToStr(rs.getString("TERM_CODE")));   
        termCode.setTermDescription(RutString.nullToStr(rs.getString("TERM_DESCRIPTION")));         
        termCode.setTermStatus(RutString.nullToStr(rs.getString("TERM_STATUS")));

        return termCode;
    }    
}
