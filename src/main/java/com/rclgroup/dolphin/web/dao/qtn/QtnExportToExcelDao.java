/*------------------------------------------------------
QtnExportToExcelDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2008
--------------------------------------------------------
 Author Kitti Pongsirisakun 01/12/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import com.rclgroup.dolphin.web.model.qtn.QtnExportToExcelSurchargeMod;

import java.util.List;

import java.util.Vector;

import org.springframework.dao.DataAccessException;


public interface QtnExportToExcelDao {

    /**
     * @param quotation_no
     * @return
     * @throws DataAccessException
     */
    public boolean isValid(String quotation_no) throws DataAccessException;

    /**
     * @param quotation_no
     * @param status
     * @return
     * @throws DataAccessException
     */
    public boolean isValid(String quotation_no, String status) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @param status
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    
    public List listForHelpScreen(String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException ;
    
    /**
     * @param rowAt
     * @param rowTo
     * @param find
     * @param search
     * @param wild
     * @param status
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException ;
    
    /**
     * @param find
     * @param search
     * @param wild
     * @param status
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return
     * @throws DataAccessException
     */
    
    public int countListForNewHelpScreenByFsc(String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException ;
    

    /**
     * @param quotationNo
     * @return
     * @throws DataAccessException
     */
    public Vector getQuotationExportToExcel(String quotationNo,String sesseion,String userName) throws DataAccessException;
    
    /**
     * @param quotationNo
     * @return
     * @throws DataAccessException
     */
    public Vector getQuotationSurcharge(String quotationNo,String sesseion,String userName) throws DataAccessException;
    /**
     *  @param quotationNo
     * @param sessionId
     * @return username
     * @throws DataAccessException
     */
    public String getSessionId(String quotationNo, String session,String username) throws DataAccessException;
    /**
     * @return username
     * @throws DataAccessException
     */
    public void deletePro(String username) throws DataAccessException;
}

