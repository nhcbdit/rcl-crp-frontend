/*-----------------------------------------------------------------------------------------------------------  
CamUtilityHeaderDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 08/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
01 26/05/11 NIP                       Added function delete header   
02 05/06/11 NIP                       Added function findUtilityHeaderByCriteria
03 12/09/11 NIP                       Added function isDuplicate
04 29/06/12 SON                       Added function findUtilityHeaderId
05 17/05/16 THIWAT1                   Added function findUtilityHeaderByModuleCode with screenCode
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamUtilityHeaderMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamUtilityHeaderDao extends RriStandardDao {
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
    public boolean update(RrcStandardMod mod) throws DataAccessException;
    
    public CamUtilityHeaderMod findUtilityHeaderByKey(String utilityHdrId) throws DataAccessException;
    
    public List findUtilityHeaderByModuleCode(String moduleCode, String country) throws DataAccessException;
    
    public List findUtilityHeaderByModuleCode(String moduleCode, String country, String fsc) throws DataAccessException;
    
    public List findUtilityHeaderByModuleCode(String moduleCode, String country, String fsc, String screenCode) throws DataAccessException; // #05
    
    public String findUtilityHeaderId(String moduleCode, String screenCode, String country, String fsc) throws DataAccessException;// ##04

    public List findUtilityHeaderByCriteria(String moduleCode, String country, String status) throws DataAccessException;
    
    public boolean isDuplicate(String country,String fsc,String moduleCode,String screenCode) throws DataAccessException;// ##03
    
    public boolean deleteUtilityHeaderByUtilityHdrId(RrcStandardMod mod,String utilityHdrId[]) throws DataAccessException;// ##01

    public CamUtilityHeaderMod findUtilityHeaderByCriteria(String moduleCode, String screenCode,String country,String fsc,String status) throws DataAccessException;// ##02
}
