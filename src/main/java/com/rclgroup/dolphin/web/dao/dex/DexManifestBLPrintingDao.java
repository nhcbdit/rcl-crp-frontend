/*------------------------------------------------------
DexManifestBLPrintingDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
 Author Kitti Pongsirisakun 04/08/2010
- Change Log -------------------------------------------
## DD/MM/YY �User- 		-TaskRef- 			-ShortDescription-
01 21/06/12 NIP    		PD_CR_20120425-01 	DEX_DIM_Add function to select some BL
02 06/10/16 Sarawut A.                  	Add new method for generate excel
03 27/06/18 Sarawut							Add search condition POL Terminal & coc/soc
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.model.dex.DexManifestBLPrintingExcelMod;
import com.rclgroup.dolphin.web.model.dex.DexManifestBLPrintingMod;

import java.util.List;
import java.util.Vector;
import org.springframework.dao.DataAccessException;

public interface DexManifestBLPrintingDao {

    
    /**
     * @param quotationNo
     * @return
     * @throws DataAccessException
     */
    public Vector getListManifestPrint(String session, String userName) throws DataAccessException;
    
    /**
     *  @param quotationNo
     * @param sessionId   
     * @return username
     * @throws DataAccessException
     */
    public String getSessionId(String service  , String vessel, String voyage,  String direction, String pol,  String pod,
                               String bl,  String office, String act,String fsc, String userName,String session ,
                               String line,String region,String agent,String fscLogin,String polTerminal,String cocSoc) throws DataAccessException;


    /**
     * @param bean
     * @return
     * @throws DataAccessException
     */
    public String  insertSelectBl(DexManifestBLPrintingMod bean) throws DataAccessException ;//##01

    /**
     * @param bean
     * @return
     * @throws DataAccessException
     */
    public String  clearPrintedBl(DexManifestBLPrintingMod bean)throws DataAccessException ;//##01
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException;//02
    public List<DexManifestBLPrintingExcelMod> generateExcelHeader(String userCode,String sessionid,String selectBl) throws DataAccessException;//02
    public List<DexManifestBLPrintingExcelMod> generateExcelDetail(String userCode,String sessionid,String selectBl) throws DataAccessException;//02
}

