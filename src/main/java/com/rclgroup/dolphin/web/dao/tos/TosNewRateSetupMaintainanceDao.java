package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import com.rclgroup.dolphin.web.exception.CustomDataAccessException;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TosNewRateSetupMaintainanceDao extends RriStandardDao{
    /**
     * @param rate_seq
     * @param port
     * @param terminal     
     * @return List
     * @throws DataAccessException
     */
    public List getRateDtlList(String rate_seq, String port,String terminal,String operation) throws DataAccessException;
    
    public List getCategoryList(String port,String terminal,String operation) throws DataAccessException;
        
    public List getActivityList(String port, String terminal, String operation) throws DataAccessException;
    
    public int insertRateHdr(String line, String trade, String agent, String port, String terminal, String rateRef, String oprType, String effDate, String expDate, String currency, String description, String status, int rateSeqNo, String userId) throws DataAccessException;
    
    public int updateRateHdr(String line, String port, String terminal, String rateRef, String oprType, String effDate, String expDate, String currency, String description, String status, String rateSeqNo, String userId) throws DataAccessException;
    
    public int getRateSeqNo(String line, String port, String terminal, String rateRef, String oprType) throws DataAccessException;
    
    public void saveRateDtl(String line, String trade, String agent, String port, String terminal, String operation, String rateSeq, String userId, String rateRef, List rateList) throws Exception;
    
    public StringBuffer checkConstraint(String port, String terminal, String operation, String rateRef) throws DataAccessException;
    
    public List getMidStreamDropdownList() throws DataAccessException;
}
