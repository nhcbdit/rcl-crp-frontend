/*-----------------------------------------------------------------------------------------------------------  
DimSvcVslVoyDirDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Nipun Sutes 03/05/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.dim;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface DimVesselScheduleDao {

    /**
     * @param service
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForService(String service) throws DataAccessException;
    
    /**
     * @param voyage
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVoyage(String voyage) throws DataAccessException;
    
    /**
     * @param vessel
     * @return
     * @throws DataAccessException
     */
    public boolean isValidForVessel(String vessel) throws DataAccessException;
    
    /**
     * @param service
     * @param veesel
     * @param voyage
     * @param direct
     * @param bl
     * @return
     * @throws DataAccessException
     */
    public List getEstimateTime(String service,String veesel,String voyage,String direct,String bl) throws DataAccessException;
    
}

