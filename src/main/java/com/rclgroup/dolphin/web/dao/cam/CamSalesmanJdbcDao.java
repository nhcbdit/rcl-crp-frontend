/*-----------------------------------------------------------------------------------------------------------  
CamSalesmanJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Leena Babu 22/08/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamSalesmanMod;

import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;

import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class CamSalesmanJdbcDao extends RrcStandardDao implements CamSalesmanDao {

    public CamSalesmanJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
         
    public boolean isValid(String salesmanCode, String status) throws DataAccessException {
        System.out.println("[CamSalesmanJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SALESMAN_CODE ");
        sql.append("FROM VR_CAM_SALESMAN ");
        sql.append("WHERE SALESMAN_CODE = :salesmanCode ");
        sql.append("AND SALESMAN_STATUS = :status ");
        
        HashMap map = new HashMap();
        map.put("salesmanCode",salesmanCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[CamSalesmanJdbcDao][isValid]: Finished");
        return isValid;
        
    }       
    public CamSalesmanMod findSalesmanForUserId(String userId) throws DataAccessException{
        
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: Started");
        CamSalesmanMod camSalesmanMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT slslno AS salesman_code  , sloffc AS fsc_code ");
        sql.append(", crcncd AS country_code, slmgyn AS is_a_manager, fsc.crflv2 as region_code ");
        sql.append(" FROM itp005, itp188 fsc ");
        sql.append(" WHERE slusid = :userId ");
        sql.append(" AND sloffc = fsc.crcntr ");
        sql.append(" AND ROWNUM = 1 ");        
       
                 
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: sql: [" + sql.toString()+"]");
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: userId: " + userId);
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: Finished");
        try{
            camSalesmanMod = (CamSalesmanMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("userId", userId),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamSalesmanMod mod = new CamSalesmanMod();
                           mod.setSalesmanCode(RutString.nullToStr(rs.getString("salesman_code")));
                           mod.setFscCode(RutString.nullToStr(rs.getString("fsc_code")));
                           mod.setCountryCode(RutString.nullToStr(rs.getString("country_code")));
                           mod.setIsAManager(RutString.nullToStr(rs.getString("is_a_manager")).equals("Y")?true:false); 
                           mod.setRegionCode(RutString.nullToStr(rs.getString("region_code")));
                           return mod; 
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            camSalesmanMod = null;
        }
        return camSalesmanMod;
    }
    
    /**
     * Checks whether the given salesman comes under the given manager
     * @param salesmanCode
     * @param mgrSalesmanCode
     * @return true if salesman comes under the manager else false
     * @throws DataAccessException
     */
    public boolean isSalesManUnderManager(String salesmanCode, String mgrSalesmanCode) throws DataAccessException{
        boolean isSalesManUnderManager = false;
        Boolean salesManUnderManager = null;
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: Started");
        CamSalesmanMod camSalesmanMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT COUNT (slslno) ");
        sql.append("  FROM itp005 ");
        sql.append("  WHERE slmngr = '" + mgrSalesmanCode +"' AND slslno = '"+salesmanCode+"' ");
       
                 
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: sql: [" + sql.toString()+"]");
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: salesmanCode: " + salesmanCode + "mgrSalesmanCode" + mgrSalesmanCode);
        System.out.println("[CamSalesmanJdbcDao][findSalesmanForUserId]: Finished");
        Map paramMap = new HashMap();
        paramMap.put("salesmanCode", salesmanCode);
        paramMap.put("mgrSalesmanCode", mgrSalesmanCode);
        try{
            salesManUnderManager = (Boolean)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   paramMap,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           if(rs.getInt(1) == 0){
                                return Boolean.valueOf(false); 
                           }else{
                                return Boolean.valueOf(true);
                           }
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            salesManUnderManager = null;
        }
        return salesManUnderManager==null? false: salesManUnderManager.booleanValue();
    }

    /**
     * Checks whether the given salesman code is a salesmanager
     * 
     * @param salesmanCode
     * @return boolean true if salesmancode is manager else false
     * @throws DataAccessException
     */
    public boolean isSalesManager(String salesmanCode) throws DataAccessException {
        System.out.println("[CamSalesmanJdbcDao][isSalesManager]: Started");
        boolean isASalesManager = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT SALESMAN_CODE ");
        sql.append("FROM VR_CAM_SALESMAN ");
        sql.append("WHERE MANAGER = :salesmanCode ");
        sql.append("AND ROWNUM = 1 ");
        
        HashMap map = new HashMap();
        map.put("salesmanCode",salesmanCode);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isASalesManager = true;
        } else { 
            isASalesManager = false;
        }
        System.out.println("[CamSalesmanJdbcDao][isSalesManager]: Finished");
        return isASalesManager;
        
    }   
}
