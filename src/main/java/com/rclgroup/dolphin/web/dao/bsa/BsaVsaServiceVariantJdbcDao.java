/*-----------------------------------------------------------------------------------------------------------  
VsaVsaServiceVariantJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaServiceVariantMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BsaVsaServiceVariantJdbcDao extends RrcStandardDao implements BsaVsaServiceVariantDao {
    private DeleteStoreProcedure deleteStoreProcedure;
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private GetCalculateDurationStoreProcedure getCalculateDurationStoreProcedure;
    private GetCalculateFrequencyStoreProcedure getCalculateFrequencyStoreProcedure;    
    private GetBsaVsaListStoreProcedure getBsaVsaListStoreProcedure;
    private BsaVsaServiceVariantDao bsaVsaServiceVariantDao;
    
    public BsaVsaServiceVariantJdbcDao() {
    }

    public void setBsaVsaServiceVariantDao(BsaVsaServiceVariantDao bsaVsaServiceVariantDao) {
        this.bsaVsaServiceVariantDao = bsaVsaServiceVariantDao;
    }

    protected void initDao() throws Exception {
        super.initDao();
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        getCalculateDurationStoreProcedure = new GetCalculateDurationStoreProcedure(getJdbcTemplate());
        getCalculateFrequencyStoreProcedure = new GetCalculateFrequencyStoreProcedure(getJdbcTemplate());
        getBsaVsaListStoreProcedure = new GetBsaVsaListStoreProcedure(getJdbcTemplate(), new VsaListRowMapper());
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    
    public Map listForSearchScreen(RcmSearchMod searchMod, String modelName, String effectiveDate, String expiredDate) {
        Map mapParam = new HashMap();                                      
        
        Map outMap = new HashMap();
                
         mapParam.put("P_I_V_ARRIVAL_FROM", "");
         mapParam.put("P_I_V_ARRIVAL_TO", "");
         mapParam.put("P_I_V_MODEL_NAME", "");
         mapParam.put("P_I_V_PROFORMA", "");
         mapParam.put("P_I_V_SERVICE", "");
         mapParam.put("P_I_V_VESSEL", "");
         mapParam.put("P_I_V_VOYAGE", "");
         mapParam.put("P_I_V_DIRECTION", "");
         mapParam.put("P_I_V_SORT_EXP", "");        
        
         outMap = getBsaVsaListStoreProcedure.getBsaVsaList(mapParam);                  
         
         return outMap;
        
    }

    private String createSqlSearchCriteria(RcmSearchMod searchMod, Map columnMap, String modelName, String effDate, String expDate) {
        String sqlCriteria = "";
        String sqlWildWithUpperCase = "";
        String sqlSortByIn = "";
        String find = RutString.changeQuoteForSqlStatement(searchMod.getFind());
        String findIn = searchMod.getFindIn();
        String status = searchMod.getStatus();
        String sortBy = searchMod.getSortBy();
        String sortByIn = searchMod.getSortByIn();
        String wild = searchMod.getWild();

        if (wild.equalsIgnoreCase("ON")) {
            sqlWildWithUpperCase = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWildWithUpperCase = "= '" + find.toUpperCase() + "' ";
        }

        if (sortByIn.equalsIgnoreCase("ASC")) {
            sqlSortByIn = "ASC";
        } else if (sortByIn.equalsIgnoreCase("DESC")) {
            sqlSortByIn = "DESC";
        }

        StringBuffer sb = new StringBuffer("");
        if (find.trim().length() != 0) {
            String sqlWild = sqlWildWithUpperCase;
            sb.append(" and UPPER(" + (String)columnMap.get(findIn) + ") " + sqlWild);
        }

        if (status.equalsIgnoreCase("ALL")) {
        } else if (status.equalsIgnoreCase("ACTIVE")) {
            sb.append(" and RECORD_STATUS = 'A'");
        } else if (status.equalsIgnoreCase("SUSPENDED")) {
            sb.append(" and RECORD_STATUS = 'S'");
        }
        //add date criteria
        if(!effDate.equalsIgnoreCase("")&& !expDate.equalsIgnoreCase("")){
            sb.append(" and EFFECT_DATE BETWEEN '"+effDate+"' and '"+ expDate+"' " );
        }

        if (modelName != null && !"".equals(modelName)) {
            sb.append(" and exists ( ");
            sb.append("     select 1 ");
            sb.append("     from VR_BSA_BASE_ALLOCATION_MODEL tmp ");
            sb.append("     where vsv.BSA_MODEL_ID = tmp.BSA_MODEL_ID ");
            sb.append("         and tmp.MODEL_NAME = '" + modelName + "' ");
            sb.append("         and tmp.RECORD_STATUS = 'A' ");
            sb.append(" )");
        }
        
        if (sb != null && sb.length() > 5) {
            sqlCriteria = " where " + sb.substring(5, sb.length());
        }
        
        // find order by
        if (sortBy.trim().length() != 0) {
            if ("SV".equals(sortBy)) {
                sqlCriteria += " ORDER BY " + (String)columnMap.get("SE") + " " + sqlSortByIn;
                sqlCriteria += "    ," + (String)columnMap.get("VA") + " " + sqlSortByIn;
            } else {
                sqlCriteria += " ORDER BY " + (String)columnMap.get(sortBy) + " " + sqlSortByIn;    
            }
        }
        return sqlCriteria;
    }
    
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException {
        BsaVsaServiceVariantMod mod = (BsaVsaServiceVariantMod) masterMod;
        
        StringBuffer errorMsgBuffer = new StringBuffer();
        try {
            if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)) {
                //insert(masterMod);
            } else if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)) {
                //update(masterMod);
            }
        } catch(CustomDataAccessException e) {
            errorMsgBuffer.append(e.getMessages()+"&");
        } catch(DataAccessException e) {
            e.printStackTrace();
        }
        
        //begin: delete record of vsa service variant
        if (isDeletes != null && !isDeletes.isEmpty()) { //if 1
            BsaVsaServiceVariantMod bean = null;
            ArrayList listDelete = new ArrayList(isDeletes.values());
            for (int i=0;i<listDelete.size();i++) { //for 1
                bean = (BsaVsaServiceVariantMod) listDelete.get(i);
                this.delete(bean);
                
            } //end for 1
        } //end if 1
        //end: delete record of vsa service variant
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
         private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_DEL_SERVICE_VARIANT";

         protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);
             declareParameter(new SqlParameter("p_vsa_service_variant_id", Types.INTEGER));
             declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
             declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
             compile();
         }
         
         protected boolean delete(final RrcStandardMod inputMod) {
             boolean isSuccess = false;
             if (inputMod instanceof BsaVsaServiceVariantMod) {
                 BsaVsaServiceVariantMod aInputMod = (BsaVsaServiceVariantMod) inputMod;
                 
                 Map inParameters = new HashMap(3);
                 inParameters.put("p_vsa_service_variant_id", new Integer((RutString.nullToStr(aInputMod.getVsaServiceVariantId())).equals("")?"0":RutString.nullToStr(aInputMod.getVsaServiceVariantId())));
                 inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                 inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                 
                 System.out.println("[VsaVsaServiceVariantJdbcDao][DeleteStoreProcedure][delete]: p_vsa_service_variant_id = "+inParameters.get("p_vsa_service_variant_id"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][DeleteStoreProcedure][delete]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][DeleteStoreProcedure][delete]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                 
                 execute(inParameters);
                 isSuccess = true;
             }
             return isSuccess;
         }
     }
   
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_INS_SERVICE_VARIANT";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_vsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_variant_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_vessel_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_usage_rule", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_calc_frequency", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_calc_duration", Types.NUMERIC));    
            declareParameter(new SqlInOutParameter("p_def_slot_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_tons", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_reefer_plugs", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_avg_coc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_avg_soc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_min_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            /* Start */
             declareParameter(new SqlInOutParameter("p_ref_variant_code", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_ref_bas_vessel_type", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_effect_date", Types.NUMERIC));
             declareParameter(new SqlInOutParameter("p_expire_date", Types.NUMERIC));
             declareParameter(new SqlInOutParameter("p_tolerant", Types.DECIMAL));
            /* End */
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof BsaVsaServiceVariantMod)&&(outputMod instanceof BsaVsaServiceVariantMod)) {
                BsaVsaServiceVariantMod aInputMod = (BsaVsaServiceVariantMod)inputMod;
                BsaVsaServiceVariantMod aOutputMod = (BsaVsaServiceVariantMod)outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_vsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVsaServiceVariantId()));
                inParameters.put("p_vsa_model_id", RutDatabase.integerToDb(aInputMod.getVsaModelId()));
                inParameters.put("p_variant_code", RutDatabase.stringToDb(aInputMod.getVariantCode()));
                inParameters.put("p_proforma_id", RutDatabase.integerToDb(aInputMod.getProformaId()));
                inParameters.put("p_vsa_vessel_type", RutDatabase.stringToDb(aInputMod.getVsaVesselType()));
                inParameters.put("p_usage_rule", RutDatabase.stringToDb(aInputMod.getUsageRule()));
                inParameters.put("p_calc_frequency", RutDatabase.bigDecimalToDb(aInputMod.getCalcFrequency()));
                inParameters.put("p_calc_duration", RutDatabase.bigDecimalToDb(aInputMod.getCalcDuration()));
                inParameters.put("p_def_slot_teu", RutDatabase.integerToDb(aInputMod.getDefSlotTeu()));
                inParameters.put("p_def_slot_tons", RutDatabase.integerToDb(aInputMod.getDefSlotTons()));
                inParameters.put("p_def_slot_reefer_plugs", RutDatabase.integerToDb(aInputMod.getDefSlotReefer()));
                inParameters.put("p_def_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgCocTeuWeight()));
                inParameters.put("p_def_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgSocTeuWeight()));
                inParameters.put("p_def_min_teu", RutDatabase.integerToDb(aInputMod.getDefMinTeu()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_add_user", RutDatabase.stringToDb(aInputMod.getRecordAddUser()));
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                /* Start */
                 inParameters.put("p_ref_variant_code", RutDatabase.stringToDb(aInputMod.getRefVariant()));
                 inParameters.put("p_ref_bas_vessel_type", RutDatabase.stringToDb(aInputMod.getRefVesselType()));
                 inParameters.put("p_effect_date", RutDatabase.integerToDb(aInputMod.getEffectiveDate()));
                 inParameters.put("p_expire_date", RutDatabase.integerToDb(aInputMod.getExpireDate()));
                 inParameters.put("p_tolerant", RutDatabase.bigDecimalToDb(aInputMod.getOverbookingPerc()));
                /* End */
 
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_vsa_service_variant_id = "+inParameters.get("p_vsa_service_variant_id"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_vsa_model_id = "+inParameters.get("p_vsa_model_id"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_variant_code = "+inParameters.get("p_variant_code"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_proforma_id = "+inParameters.get("p_proforma_id"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_vsa_vessel_type = "+inParameters.get("p_vsa_vessel_type"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_usage_rule = "+inParameters.get("p_usage_rule"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_calc_frequency = "+inParameters.get("p_calc_frequency"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_calc_duration = "+inParameters.get("p_calc_duration"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_slot_teu = "+inParameters.get("p_def_slot_teu"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_slot_tons = "+inParameters.get("p_def_slot_tons"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_slot_reefer_plugs = "+inParameters.get("p_def_slot_reefer_plugs"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_avg_coc_teu_weight = "+inParameters.get("p_def_avg_coc_teu_weight"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_avg_soc_teu_weight = "+inParameters.get("p_def_avg_soc_teu_weight"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_def_min_teu = "+inParameters.get("p_def_min_teu"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_add_user = "+inParameters.get("p_record_add_user"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_add_date = "+inParameters.get("p_record_add_date"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                /* Start */
                 System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_ref_variant_code = "+inParameters.get("p_ref_variant_code"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_ref_bas_vessel_type = "+inParameters.get("p_ref_bas_vessel_type"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_effect_date = "+inParameters.get("p_effect_date"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_expire_date = "+inParameters.get("p_expire_date"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][InsertStoreProcedure][insert]: p_tolerant = "+inParameters.get("p_tolerant"));
                /* End */
                
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVsaServiceVariantId(RutDatabase.dbToStrInteger(outParameters, "p_vsa_service_variant_id"));
                    aOutputMod.setVsaModelId(RutDatabase.dbToStrInteger(outParameters, "p_vsa_model_id"));
                    aOutputMod.setVariantCode(RutDatabase.dbToString(outParameters, "p_variant_code"));
                    aOutputMod.setProformaId(RutDatabase.dbToString(outParameters, "p_proforma_id"));
                    aOutputMod.setVsaVesselType(RutDatabase.dbToString(outParameters, "p_vsa_vessel_type"));
                    aOutputMod.setUsageRule(RutDatabase.dbToString(outParameters, "p_usage_rule"));
                    aOutputMod.setCalcFrequency(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_frequency"));
                    aOutputMod.setCalcDuration(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_duration"));
                    aOutputMod.setDefSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_teu"));
                    aOutputMod.setDefSlotTons(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_tons"));
                    aOutputMod.setDefSlotReefer(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_reefer_plugs"));
                    aOutputMod.setDefAvgCocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_coc_teu_weight"));
                    aOutputMod.setDefAvgSocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_soc_teu_weight"));
                    aOutputMod.setDefMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_min_teu"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordAddUser(RutDatabase.dbToString(outParameters, "p_record_add_user"));
                    aOutputMod.setRecordAddDate(RutDatabase.dbToTimestamp(outParameters, "p_record_add_date"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    /* Start */
                     aOutputMod.setRefVariant(RutDatabase.dbToString(outParameters, "p_ref_variant_code"));
                     aOutputMod.setRefVesselType(RutDatabase.dbToString(outParameters, "p_ref_bas_vessel_type"));
                     aOutputMod.setEffectiveDate(RutDatabase.dbToStrInteger(outParameters, "p_effect_date"));
                     aOutputMod.setExpireDate(RutDatabase.dbToStrInteger(outParameters, "p_expire_date"));
                     aOutputMod.setOverbookingPerc(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolerant"));
                    /* End */
                    
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_SERVICE_VARIANT.PRR_UPD_SERVICE_VARIANT";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            declareParameter(new SqlInOutParameter("p_vsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_variant_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_vessel_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_usage_rule", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_calc_frequency", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_calc_duration", Types.NUMERIC));    
            declareParameter(new SqlInOutParameter("p_def_slot_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_tons", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_slot_reefer_plugs", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_def_avg_coc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_avg_soc_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_def_min_teu", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            /* Start */
             declareParameter(new SqlInOutParameter("p_ref_variant_code", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_ref_bas_vessel_type", Types.VARCHAR));
             declareParameter(new SqlInOutParameter("p_effect_date", Types.NUMERIC));
             declareParameter(new SqlInOutParameter("p_expire_date", Types.NUMERIC));
             declareParameter(new SqlInOutParameter("p_tolerant", Types.DECIMAL));
            /* End */
            
            compile();
        }
        
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof BsaVsaServiceVariantMod)&&(outputMod instanceof BsaVsaServiceVariantMod)){
                BsaVsaServiceVariantMod aInputMod = (BsaVsaServiceVariantMod)inputMod;
                BsaVsaServiceVariantMod aOutputMod = (BsaVsaServiceVariantMod)outputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_vsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVsaServiceVariantId()));
                inParameters.put("p_vsa_model_id", RutDatabase.integerToDb(aInputMod.getVsaModelId()));
                inParameters.put("p_variant_code", RutDatabase.stringToDb(aInputMod.getVariantCode()));
                inParameters.put("p_proforma_id", RutDatabase.integerToDb(aInputMod.getProformaId()));
                inParameters.put("p_vsa_vessel_type", RutDatabase.stringToDb(aInputMod.getVsaVesselType()));
                inParameters.put("p_usage_rule", RutDatabase.stringToDb(aInputMod.getUsageRule()));
                inParameters.put("p_calc_frequency", RutDatabase.bigDecimalToDb(aInputMod.getCalcFrequency()));
                inParameters.put("p_calc_duration", RutDatabase.bigDecimalToDb(aInputMod.getCalcDuration()));
                inParameters.put("p_def_slot_teu", RutDatabase.integerToDb(aInputMod.getDefSlotTeu()));
                inParameters.put("p_def_slot_tons", RutDatabase.integerToDb(aInputMod.getDefSlotTons()));
                inParameters.put("p_def_slot_reefer_plugs", RutDatabase.integerToDb(aInputMod.getDefSlotReefer()));
                inParameters.put("p_def_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgCocTeuWeight()));
                inParameters.put("p_def_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getDefAvgSocTeuWeight()));
                inParameters.put("p_def_min_teu", RutDatabase.integerToDb(aInputMod.getDefMinTeu()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                /* Start */
                 inParameters.put("p_ref_variant_code", RutDatabase.stringToDb(aInputMod.getRefVariant()));
                 inParameters.put("p_ref_bas_vessel_type", RutDatabase.stringToDb(aInputMod.getRefVesselType()));
                 inParameters.put("p_effect_date", RutDatabase.integerToDb(aInputMod.getEffectiveDate()));
                 inParameters.put("p_expire_date", RutDatabase.integerToDb(aInputMod.getExpireDate()));
                 inParameters.put("p_tolerant", RutDatabase.bigDecimalToDb(aInputMod.getOverbookingPerc()));
                /* End */
                
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_vsa_service_variant_id = "+inParameters.get("p_vsa_service_variant_id"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_vsa_model_id = "+inParameters.get("p_vsa_model_id"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_variant_code = "+inParameters.get("p_variant_code"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_proforma_id = "+inParameters.get("p_proforma_id"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_vsa_vessel_type = "+inParameters.get("p_vsa_vessel_type"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_usage_rule = "+inParameters.get("p_usage_rule"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_calc_frequency = "+inParameters.get("p_calc_frequency"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_calc_duration = "+inParameters.get("p_calc_duration"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_slot_teu = "+inParameters.get("p_def_slot_teu"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_slot_tons = "+inParameters.get("p_def_slot_tons"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_slot_reefer_plugs = "+inParameters.get("p_def_slot_reefer_plugs"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_avg_coc_teu_weight = "+inParameters.get("p_def_avg_coc_teu_weight"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_avg_soc_teu_weight = "+inParameters.get("p_def_avg_soc_teu_weight"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_def_min_teu = "+inParameters.get("p_def_min_teu"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                /* Start */
                 System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_ref_variant_code = "+inParameters.get("p_ref_variant_code"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_ref_bas_vessel_type = "+inParameters.get("p_ref_bas_vessel_type"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_effect_date = "+inParameters.get("p_effect_date"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_expire_date = "+inParameters.get("p_expire_date"));
                 System.out.println("[VsaVsaServiceVariantJdbcDao][UpdateStoreProcedure][update]: p_tolerant = "+inParameters.get("p_tolerant"));
                /* End */
                
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setVsaServiceVariantId(RutDatabase.dbToStrInteger(outParameters, "p_vsa_service_variant_id"));
                    aOutputMod.setVsaModelId(RutDatabase.dbToStrInteger(outParameters, "p_vsa_model_id"));
                    aOutputMod.setVariantCode(RutDatabase.dbToString(outParameters, "p_variant_code"));
                    aOutputMod.setProformaId(RutDatabase.dbToStrInteger(outParameters, "p_proforma_id"));
                    aOutputMod.setVsaVesselType(RutDatabase.dbToString(outParameters, "p_vsa_vessel_type"));
                    aOutputMod.setUsageRule(RutDatabase.dbToString(outParameters, "p_usage_rule"));
                    aOutputMod.setCalcFrequency(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_frequency"));
                    aOutputMod.setCalcDuration(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_duration"));
                    aOutputMod.setDefSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_teu"));
                    aOutputMod.setDefSlotTons(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_tons"));
                    aOutputMod.setDefSlotReefer(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_reefer_plugs"));
                    aOutputMod.setDefAvgCocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_coc_teu_weight"));
                    aOutputMod.setDefAvgSocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_soc_teu_weight"));
                    aOutputMod.setDefMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_min_teu"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    /* Start */
                     aOutputMod.setRefVariant(RutDatabase.dbToString(outParameters, "p_ref_variant_code"));
                     aOutputMod.setRefVesselType(RutDatabase.dbToString(outParameters, "p_ref_bas_vessel_type"));
                     aOutputMod.setEffectiveDate(RutDatabase.dbToStrInteger(outParameters, "p_effect_date"));
                     aOutputMod.setExpireDate(RutDatabase.dbToStrInteger(outParameters, "p_expire_date"));
                     aOutputMod.setOverbookingPerc(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolerant"));
                    /* End */
                } 
            }
            return isSuccess;
        }
    }
     
    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
 
    public BsaVsaServiceVariantMod findByKeyVsaServiceVariantID(String vsaServiceVariantId) throws DataAccessException{
        BsaVsaServiceVariantMod vsaVsaServiceVariantMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select VR.BSA_SERVICE_VARIANT_ID ");
        sql.append("      ,VR.BSA_MODEL_ID  ");
        sql.append("      ,VR.SERVICE ");
        sql.append("      ,BSV.VARIANT_CODE  ");
        sql.append("      ,VR.PROFORMA_REF_NO ");
        sql.append("      ,BSV.BSA_VESSEL_TYPE ");
        sql.append("      ,VR.NO_VESSELS ");
        sql.append("      ,VR.ROTATION_DURATION  ");
        sql.append("      ,BSV.USAGE_RULE ");
        sql.append("      ,BSV.CALC_FREQUENCY ");
        sql.append("      ,BSV.CALC_DURATION ");
        sql.append("      ,BSV.DEF_SLOT_TEU  ");
        sql.append("      ,BSV.DEF_SLOT_TONS ");
        sql.append("      ,BSV.DEF_SLOT_REEFER_PLUGS ");
        sql.append("      ,BSV.DEF_AVG_COC_TEU_WEIGHT ");
        sql.append("      ,BSV.DEF_AVG_SOC_TEU_WEIGHT ");
        sql.append("      ,BSV.DEF_MIN_TEU  ");
        sql.append("      ,VR.OVERBOOKING_PERC ");
        sql.append("      ,BSV.RECORD_STATUS ");
        sql.append("      ,BSV.RECORD_ADD_USER ");
        sql.append("      ,BSV.RECORD_ADD_DATE ");
        sql.append("      ,BSV.RECORD_CHANGE_USER ");
        sql.append("      ,BSV.RECORD_CHANGE_DATE ");
        sql.append("      ,BSV.REF_VARIANT_CODE ");
        sql.append("      ,BSV.REF_BSA_VESSEL_TYPE ");
        sql.append("      ,BSV.EFFECT_DATE  ");
        sql.append("      ,BSV.EXPIRE_DATE ");
        sql.append("      ,BSV.TOLERANT ");
        
        sql.append("from BSA_SERVICE_VARIANT BSV,VR_BSA_SERVICE_VARIANT VR ");
        sql.append("WHERE VR.BSA_SERVICE_VARIANT_ID=BSV.PK_BSA_SERVICE_VARIANT_ID ");
        sql.append("      AND VR.BSA_MODEL_ID=BSV.FK_BSA_MODEL_ID");
        sql.append("      AND BSA_SERVICE_VARIANT_ID = :vsaServiceVariantId ");
        sql.append("ORDER BY BSA_SERVICE_VARIANT_ID ");
        try{
            vsaVsaServiceVariantMod = (BsaVsaServiceVariantMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("vsaServiceVariantId", vsaServiceVariantId),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            BsaVsaServiceVariantMod vsaVsaServiceVariantMod = new BsaVsaServiceVariantMod();
                            vsaVsaServiceVariantMod.setVsaServiceVariantId(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                            vsaVsaServiceVariantMod.setVsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                            vsaVsaServiceVariantMod.setService(RutString.nullToStr(rs.getString("SERVICE")));
                            vsaVsaServiceVariantMod.setVariantCode(RutString.nullToStr(rs.getString("VARIANT_CODE")));
                            vsaVsaServiceVariantMod.setProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));
                            vsaVsaServiceVariantMod.setVsaVesselType(RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE")));
                            vsaVsaServiceVariantMod.setNoVessels(RutString.nullToStr(rs.getString("NO_VESSELS")));
                            vsaVsaServiceVariantMod.setRotationDuration(RutString.nullToStr(rs.getString("ROTATION_DURATION")));
                            vsaVsaServiceVariantMod.setUsageRule(RutString.nullToStr(rs.getString("USAGE_RULE")));
                            vsaVsaServiceVariantMod.setCalcFrequency(RutString.nullToStr(rs.getString("CALC_FREQUENCY")));
                            vsaVsaServiceVariantMod.setCalcDuration(RutString.nullToStr(rs.getString("CALC_DURATION")));
                            vsaVsaServiceVariantMod.setDefSlotTeu(RutString.nullToStr(rs.getString("DEF_SLOT_TEU")));
                            vsaVsaServiceVariantMod.setDefSlotTons(RutString.nullToStr(rs.getString("DEF_SLOT_TONS")));
                            vsaVsaServiceVariantMod.setDefSlotReefer(RutString.nullToStr(rs.getString("DEF_SLOT_REEFER_PLUGS")));
                            vsaVsaServiceVariantMod.setDefAvgCocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_COC_TEU_WEIGHT")));
                            vsaVsaServiceVariantMod.setDefAvgSocTeuWeight(RutString.nullToStr(rs.getString("DEF_AVG_SOC_TEU_WEIGHT")));
                            vsaVsaServiceVariantMod.setDefMinTeu(RutString.nullToStr(rs.getString("DEF_MIN_TEU")));
                            vsaVsaServiceVariantMod.setOverbookingPerc(RutString.nullToStr(rs.getString("TOLERANT")));
                            vsaVsaServiceVariantMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            vsaVsaServiceVariantMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                            vsaVsaServiceVariantMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                            vsaVsaServiceVariantMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                            vsaVsaServiceVariantMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                            vsaVsaServiceVariantMod.setRefVariant(RutString.nullToStr(rs.getString("REF_VARIANT_CODE")));
                            vsaVsaServiceVariantMod.setRefVesselType(RutString.nullToStr(rs.getString("REF_BSA_VESSEL_TYPE")));
                            vsaVsaServiceVariantMod.setEffectiveDate(RutString.nullToStr(rs.getString("EFFECT_DATE")));
                            vsaVsaServiceVariantMod.setExpireDate(RutString.nullToStr(rs.getString("EXPIRE_DATE")));
                            
                            
                            return vsaVsaServiceVariantMod;
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            vsaVsaServiceVariantMod = null;
        }
        return vsaVsaServiceVariantMod;
    }
    
    public int findNoOfVesselsByProformaIdVsaVesselType(int proformaId, String vsaVesselType) throws DataAccessException {
        int noOfVessels = 0;
        StringBuffer sql = new StringBuffer();
        sql.append("select PCR_BSA_SERVICE_VARIANT.FR_GET_NO_OF_VESSEL(:proformaId, :vsaVesselType) as no_of_vessels from dual");
        try {
            HashMap map = new HashMap();
            map.put("proformaId", new Integer(proformaId));
            map.put("vsaVesselType", vsaVesselType);
        
            Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(
                    sql.toString(),
                    map, Integer.class);
            noOfVessels = result.intValue();
        } catch (EmptyResultDataAccessException e) {
            noOfVessels = 0;
        }
        return noOfVessels;
    }
    
    public int findVoyageDaysByServiceProformaNo(String serviceCode, String proformaNo) throws DataAccessException {
        int voyageDays = 0;
        StringBuffer sql = new StringBuffer();
        sql.append("select PCR_BSA_SERVICE_VARIANT.FR_GET_VOYAGE_DAYS(:serviceCode, :proformaNo) as voyage_days from dual");
        try {
            HashMap map = new HashMap();
            map.put("serviceCode", serviceCode);
            map.put("proformaNo", proformaNo);
            
            Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(
                    sql.toString(),
                    map, Integer.class);
            voyageDays = result.intValue();
        } catch (EmptyResultDataAccessException e) {
            voyageDays = 0;
        }
        return voyageDays;
    }
    
    public long calculateDurationByArrayNumber(long[] number) throws DataAccessException {
        long calculateDuration = 0;
        try {
            Long[] arLong = null;
            if (number != null && number.length > 0) {
                arLong = new Long[number.length];    
                for (int i=0;i<number.length;i++) {
                    arLong[i] = new Long(number[i]);
                }
            }
            
            calculateDuration = getCalculateDurationStoreProcedure.getCalculateDuration(arLong);
                        
        } catch(Exception e) {
            e.printStackTrace();
        }
        return calculateDuration;
    }
    
    protected class GetCalculateDurationStoreProcedure extends StoredProcedure {
        private static final String FUNCTION_NAME = "PCR_BSA_SERVICE_VARIANT.FR_CALCULATE_DURATION";
    
        protected GetCalculateDurationStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, FUNCTION_NAME);
            setFunction(true);
            declareParameter(new SqlOutParameter("p_calculate_duration", Types.INTEGER));
            declareParameter(new SqlParameter("p_voyage_days", Types.ARRAY));
            compile();
        }
    
        protected long getCalculateDuration(final Long[] voyageDays) {
            System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateDuration]: Started");
            long calculateDuration = 0;
            try {
                Map map = new HashMap(1);
                map.put("p_voyage_days", new ArraySqlTypeValue("ARRAY_NUMBER", voyageDays));
                
                //Begin: DEBUG
                if (voyageDays != null && voyageDays.length > 0) {
                    for (int i=0;i<voyageDays.length;i++) {
                        System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateDuration]: voyageDays["+i+"] = "+voyageDays[i]);
                    }
                }
                //END: DEBUG
                
                Map outMap = execute(map);
                if (outMap.size() > 0) {
                    calculateDuration = new Long(RutDatabase.dbToInteger(outMap, "p_calculate_duration")).longValue();
                }    
                
            } catch(Exception e) {
                e.printStackTrace();
            }
            System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateDuration]: calculateDuration = "+calculateDuration);
            System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateDuration]: Finished");
            return calculateDuration;
        }
    }
    
    public long calculateFrequencyByArrayNumber(long[] number) throws DataAccessException {
        long calculateFrequency = 0;
        try {
            Long[] arLong = null;
            if (number != null && number.length > 0) {
                arLong = new Long[number.length];    
                for (int i=0;i<number.length;i++) {
                    arLong[i] = new Long(number[i]);
                }
            }
            
            calculateFrequency = getCalculateFrequencyStoreProcedure.getCalculateFrequency(arLong);
                        
        } catch(Exception e) {
            e.printStackTrace();
        }
        return calculateFrequency;
    }
    
    protected class GetCalculateFrequencyStoreProcedure extends StoredProcedure {
        private static final String FUNCTION_NAME = "PCR_BSA_SERVICE_VARIANT.FR_CALCULATE_FREQUENCY";
    
        protected GetCalculateFrequencyStoreProcedure(JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, FUNCTION_NAME);
            setFunction(true);
            declareParameter(new SqlOutParameter("p_calculate_frequency", Types.INTEGER));
            declareParameter(new SqlParameter("p_numbers", Types.ARRAY));
            compile();
        }
    
        protected long getCalculateFrequency(final Long[] values) {
            System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateFrequency]: Started");
            long calculateFrequency = 0;
            try {
                Map map = new HashMap(1);
                map.put("p_numbers", new ArraySqlTypeValue("ARRAY_NUMBER", values));
                
                //Begin: DEBUG
                if (values != null && values.length > 0) {
                    for (int i=0;i<values.length;i++) {
                        System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateFrequency]: values["+i+"] = "+values[i]);
                    }
                }
                //END: DEBUG
                
                Map outMap = execute(map);
                if (outMap.size() > 0) {
                    calculateFrequency = new Long(RutDatabase.dbToInteger(outMap, "p_calculate_frequency")).longValue();
                }    
                
            } catch(Exception e) {
                e.printStackTrace();
            }
            System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateFrequency]: calculateFrequency = "+calculateFrequency);
            System.out.println("[VsaVsaServiceVariantJdbcDao][getCalculateFrequency]: Finished");
            return calculateFrequency;
        }
    }    
    
    protected class GetBsaVsaListStoreProcedure extends StoredProcedure{
        /* SQL for Fetching Result based on Search criteria */
        public static final String SQL_VSA_LIST = "PCR_BSA_VSA.PRR_VSA_GET_LIST";
        
        public GetBsaVsaListStoreProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper) {          
            super(jdbcTemplate, SQL_VSA_LIST);
            setFunction(false);
            
            declareParameter(new SqlOutParameter("P_O_V_VSA_REC", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_ARRIVAL_FROM", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_ARRIVAL_TO", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_MODEL_NAME", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_PROFORMA", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_SERVICE", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_VESSEL", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_VOYAGE", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_DIRECTION", OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_I_V_SORT_EXP", OracleTypes.VARCHAR,rowMapper));
            
            compile();
        }
        
        protected Map getBsaVsaList(Map mapParams){
            Map outMap = new HashMap();
            
            try{
                outMap = execute(mapParams);
            }catch(Exception ex){
                ex.printStackTrace();
            }
            
            return outMap;
        }
    }
    
    private class VsaListRowMapper implements RowMapper {
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            BsaVsaServiceVariantMod bean = new BsaVsaServiceVariantMod();
            bean.setModelName("MODEL_NAME");
            bean.setService("SERVICE");           
            bean.setVessel("VESSEL");
            bean.setVesselName("VESSEL_NAME");
            bean.setVoyage("VOYAGE");
            bean.setDirection("DIRECTION");
         
            return bean;
        }
    }

}

