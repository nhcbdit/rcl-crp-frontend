package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.tos.TosApprovalInternalPSEditMod;

import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class TosApprovalInternalPSEditJdbcDao extends RrcStandardDao implements TosApprovalInternalPSEditDao{

    private GetProformaHeaderListProcedure getProformaHeaderListProcedure;
    private GetProformaDetailListProcedure getProformaDetailListProcedure;
    private UpdateApprovalProcedure updateApprovalProcedure;
    
    public TosApprovalInternalPSEditJdbcDao() {
    }
    
    public void initDao() throws Exception {
        super.initDao();
        getProformaHeaderListProcedure = new GetProformaHeaderListProcedure(getJdbcTemplate(), new ProFormaHeaderListMapper());
        getProformaDetailListProcedure = new GetProformaDetailListProcedure(getJdbcTemplate(), new ProFormaDetailListMapper());
        updateApprovalProcedure = new UpdateApprovalProcedure(getJdbcTemplate());
    }
    
    protected class GetProformaHeaderListProcedure extends StoredProcedure{
    private static final String SQL_TOS_PROFORMA_HDR = "PKG_TOS_RCL_APPROVAL.PRC_TOS_GET_APPROVAL_PROF";
    
    protected GetProformaHeaderListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
        super(jdbcTemplate, SQL_TOS_PROFORMA_HDR);
                        
        declareParameter(new SqlOutParameter("p_o_v_data", OracleTypes.CURSOR, rowMapper));
        declareParameter(new SqlInOutParameter("p_port",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_terminal",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_service",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_vessel",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_voyage",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_pcsq",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_sort",OracleTypes.VARCHAR,rowMapper));
        
        
        compile();
    }
    
    protected List<TosApprovalInternalPSEditMod> getProformaHeaderList(Map mapParams){
        Map outMap = new HashMap();
        
        System.out.println("getProformaHeaderList p_port: " + (String)mapParams.get("p_port"));
        System.out.println("getProformaHeaderList p_terminal: "+ (String)mapParams.get("p_terminal"));
        
        List<TosApprovalInternalPSEditMod> returnList = new ArrayList<TosApprovalInternalPSEditMod>();
        
        try{
            outMap = execute(mapParams);
            returnList = (List<TosApprovalInternalPSEditMod>) outMap.get("p_o_v_data");
        }catch(Exception ex){
            ex.printStackTrace();
        }            
        
        return returnList;
     }
    }
    
    private class ProFormaHeaderListMapper implements RowMapper{
    public Object mapRow(ResultSet rs, int row) throws SQLException {
        TosApprovalInternalPSEditMod bean = new TosApprovalInternalPSEditMod();
        bean.setProformaRef(RutString.nullToStr(rs.getString("TOS_PRO_REF")));
        bean.setVendorName(RutString.nullToStr(rs.getString("VENDOR_NAME")));
        bean.setVendorCode(RutString.nullToStr(rs.getString("VENDOR_CODE")));

        return bean;
    }
    }
    
    
    protected class GetProformaDetailListProcedure extends StoredProcedure{
    private static final String SQL_TOS_PROFORMA_DTL = "PKG_TOS_RCL_APPROVAL.PRC_TOS_GET_APPROVAL_PROF_DTL";
    
    protected GetProformaDetailListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
        super(jdbcTemplate, SQL_TOS_PROFORMA_DTL);
                        
        declareParameter(new SqlOutParameter("p_o_v_data", OracleTypes.CURSOR, rowMapper));
        declareParameter(new SqlInOutParameter("p_tos_pro_ref",OracleTypes.VARCHAR,rowMapper));
        declareParameter(new SqlInOutParameter("p_sort",OracleTypes.VARCHAR,rowMapper));
        
        
        compile();
    }
    
    protected List<TosApprovalInternalPSEditMod> getProformaDetailList(Map mapParams){
        Map outMap = new HashMap();
        
        System.out.println("p_tos_pro_ref: " + (String)mapParams.get("p_tos_pro_ref"));
        System.out.println("p_sort: "+ (String)mapParams.get("p_sort"));
        
        List<TosApprovalInternalPSEditMod> returnList = new ArrayList<TosApprovalInternalPSEditMod>();
        
        try{
            outMap = execute(mapParams);
            returnList = (List<TosApprovalInternalPSEditMod>) outMap.get("p_o_v_data");
            
            System.out.println("getProformaDetailList returnList Size : "+returnList.size());
        }catch(Exception ex){
            ex.printStackTrace();
        }            
        
        return returnList;
     }
    }
    
    private class ProFormaDetailListMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            TosApprovalInternalPSEditMod bean = new TosApprovalInternalPSEditMod();
            bean.setActivity(RutString.nullToStr(rs.getString("ACTIVITY_DESC")));
            bean.setChangeCode(RutString.nullToStr(rs.getString("CHARGE_CODE")));
            bean.setRemark(RutString.nullToStr(rs.getString("REMARKS")));
            bean.setSize(RutString.nullToStr(rs.getString("EQSIZE")));
            bean.setType(RutString.nullToStr(rs.getString("EQTYPE")));
            bean.setUnit(RutString.nullToStr(rs.getString("QTY")));
            bean.setAdjustUnit(RutString.nullToStr(rs.getString("ADJ_QTY")));
            bean.setCurrency(RutString.nullToStr(rs.getString("CURRENCY")));
            bean.setTariffRate(RutString.nullToStr(rs.getString("RATE")));
            bean.setTariffAmount(RutString.nullToStr(rs.getString("AMOUNT")));
            bean.setActualCostRate(RutString.nullToStr(rs.getString("ADJ_RATE")));
            bean.setActualCostAmount(RutString.nullToStr(rs.getString("ADJ_AMOUNT")));
            bean.setTosProRef(RutString.nullToStr(rs.getString("TOS_PRO_REF")));
            bean.setTosProSeq(rs.getInt("TOS_PRO_SEQ"));
    //        bean.setApproveStatus(RutString.nullToStr(rs.getString("ADJ_AMOUNT")));
    
            return bean;
        }
    }
    
    public List<TosApprovalInternalPSEditMod> getProformaHeaderList(String port,String terminal,String service,String vessel,String voyage,String pcsq,String sort) throws DataAccessException{        

        Map map = new HashMap();
        map.put("p_port",port);
        map.put("p_terminal",terminal);
        map.put("p_service",service);
        map.put("p_vessel",vessel);
        map.put("p_voyage",voyage);
        map.put("p_pcsq",pcsq);
        map.put("p_sort",sort);
        
        List<TosApprovalInternalPSEditMod> resultList = getProformaHeaderListProcedure.getProformaHeaderList(map);     
        
        return resultList;
    }
    
    public List<TosApprovalInternalPSEditMod> getProformaDetailList(String tosProRef,String sort) throws DataAccessException{        

        Map map = new HashMap();
        map.put("p_tos_pro_ref",tosProRef);
        map.put("p_sort",sort);
        
        List<TosApprovalInternalPSEditMod> resultList = getProformaDetailListProcedure.getProformaDetailList(map);     
        
        return resultList;
    }
    
    private class UpdateApprovalProcedure extends StoredProcedure{
        private static final String SQL_UPD_APP = "PKG_TOS_RCL_APPROVAL.PRC_TOS_APPROVE_PROF_DTL";
            
        protected UpdateApprovalProcedure (JdbcTemplate jdbcTemplate){
            super(jdbcTemplate, SQL_UPD_APP);
                
            declareParameter(new SqlParameter("p_tos_pro_ref", OracleTypes.VARCHAR));
            declareParameter(new SqlParameter("p_tos_pro_seq", OracleTypes.NUMBER));
            declareParameter(new SqlParameter("p_approve_reject", OracleTypes.VARCHAR));
            declareParameter(new SqlParameter("p_user", OracleTypes.VARCHAR));
            declareParameter(new SqlParameter("p_remark", OracleTypes.VARCHAR));
            compile();
        }
            
        protected boolean updateProformaStatus(Map mapParams){
            boolean isSuccess = false;
            Map inParam = new HashMap();
                
            inParam.put("p_tos_pro_ref", (String) mapParams.get("p_tos_pro_ref"));
            inParam.put("p_tos_pro_seq", (Integer) mapParams.get("p_tos_pro_seq"));
            inParam.put("p_approve_reject", (String) mapParams.get("p_approve_reject"));
            inParam.put("p_user", (String) mapParams.get("p_user"));
            inParam.put("p_remark", (String) mapParams.get("p_remark"));
            Map outParameters = execute(inParam);
                
            if(outParameters.size() > 0){
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    public void updateProforma(List<TosApprovalInternalPSEditMod> modList,String user) throws DataAccessException{        

        Map map = new HashMap();
        for(int i = 0 ; i < modList.size() ; i++){
            
            TosApprovalInternalPSEditMod mod = modList.get(i);
            if((mod.getApproveStatus() != null) && (!mod.getApproveStatus().equals(""))){
                map.put("p_tos_pro_ref",mod.getTosProRef());
                map.put("p_tos_pro_seq",mod.getTosProSeq());
                map.put("p_approve_reject",RutString.nullToStr(mod.getApproveStatus()));
                map.put("p_user",this.getUserId());
                map.put("p_remark",mod.getRemark());
//                System.out.println("getApproveStatus >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. "+RutString.nullToStr(mod.getApproveStatus()));
//                System.out.println("remark >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. "+mod.getRemark());
                updateApprovalProcedure.updateProformaStatus(map);
            }
            
        }
        
    }
}
