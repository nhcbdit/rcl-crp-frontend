/*-----------------------------------------------------------------------------------------------------------  
CamUtilityDetailJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 11/08/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.cam.CamUtilityDetailMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class CamUtilityDetailJdbcDao extends RrcStandardDao implements CamUtilityDetailDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
   
    public CamUtilityDetailJdbcDao() {
        super();
    }
   
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    public CamUtilityDetailMod findUtilityDetailByKey(String utilityDtlId) throws DataAccessException {
        System.out.println("[CamUtilityDetailJdbcDao][findUtilityDetailByKey]: Started");
        
        CamUtilityDetailMod bean = null;
        StringBuffer sb = new StringBuffer();  
        sb.append("select PK_CAM_UTILITY_DTL_ID ");
        sb.append(" ,FK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,DETAIL_NAME ");
        sb.append(" ,UTILITY_TYPE ");
        sb.append(" ,FILE_NAME ");
        sb.append(" ,TEXT_MESSAGE ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_DTL ");
        sb.append("where PK_CAM_UTILITY_DTL_ID = :utilityDtlId ");
        
        System.out.println("[CamUtilityDetailJdbcDao][findUtilityDetailByKey]: sql = " + sb.toString());
        try {
            bean = (CamUtilityDetailMod) getNamedParameterJdbcTemplate().queryForObject(
                   sb.toString(),
                   Collections.singletonMap("utilityDtlId", utilityDtlId),
                   new RowMapper() {
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamUtilityDetailMod bean = new CamUtilityDetailMod();
                           bean.setUtilityDtlId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_DTL_ID")));
                           bean.setUtilityHdrId(RutString.nullToStr(rs.getString("FK_CAM_UTILITY_HDR_ID")));
                           bean.setDetailName(RutString.nullToStr(rs.getString("DETAIL_NAME")));
                           bean.setUtilityType(RutString.nullToStr(rs.getString("UTILITY_TYPE")));
                           bean.setFileName(RutString.nullToStr(rs.getString("FILE_NAME")));
                           bean.setTextMessage(RutString.nullToStr(rs.getString("TEXT_MESSAGE")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bean;
                       }
                   });
        } catch (EmptyResultDataAccessException e) {
            bean = null;
        }
        
        System.out.println("[CamUtilityDetailJdbcDao][findUtilityDetailByKey]: Finished");
        return bean;
    }
    
    public List findUtilityDetailByHdrId(String utilityHdrId, String status) throws DataAccessException {
        System.out.println("[CamUtilityDetailJdbcDao][findUtilityDetailByHdrId]: Started");
        
        StringBuffer sb = new StringBuffer();  
        sb.append("select PK_CAM_UTILITY_DTL_ID ");
        sb.append(" ,FK_CAM_UTILITY_HDR_ID ");
        sb.append(" ,DETAIL_NAME ");
        sb.append(" ,UTILITY_TYPE ");
        sb.append(" ,FILE_NAME ");
        sb.append(" ,TEXT_MESSAGE ");
        sb.append(" ,RECORD_STATUS ");
        sb.append(" ,RECORD_ADD_USER ");
        sb.append(" ,RECORD_ADD_DATE ");
        sb.append(" ,RECORD_CHANGE_USER ");
        sb.append(" ,RECORD_CHANGE_DATE ");
        sb.append("from VR_CAM_UTILITY_DTL ");
        sb.append("where FK_CAM_UTILITY_HDR_ID = :utilityHdrId ");
        
        if (!RutString.isEmptyString(status)) {
            sb.append(" and RECORD_STATUS = '"+status+"' ");
        }
        
        sb.append("order by DETAIL_NAME ");
        sb.append(" ,UTILITY_TYPE ");
        
        System.out.println("[CamUtilityDetailJdbcDao][findUtilityDetailByHdrId]: sql = " + sb.toString());
        System.out.println("[CamUtilityDetailJdbcDao][findUtilityDetailByHdrId]: Finished");
        return getNamedParameterJdbcTemplate().query(
                sb.toString(),
                Collections.singletonMap("utilityHdrId", utilityHdrId),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                        CamUtilityDetailMod bean = new CamUtilityDetailMod();
                        bean.setUtilityDtlId(RutString.nullToStr(rs.getString("PK_CAM_UTILITY_DTL_ID")));
                        bean.setUtilityHdrId(RutString.nullToStr(rs.getString("FK_CAM_UTILITY_HDR_ID")));
                        bean.setDetailName(RutString.nullToStr(rs.getString("DETAIL_NAME")));
                        bean.setUtilityType(RutString.nullToStr(rs.getString("UTILITY_TYPE")));
                        bean.setFileName(RutString.nullToStr(rs.getString("FILE_NAME")));
                        bean.setTextMessage(RutString.nullToStr(rs.getString("TEXT_MESSAGE")));
                        bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                        bean.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                        bean.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));
                        bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                        bean.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                        return bean;
                    }
                });
    }
    
    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_UTILITY_DTL.PRR_INS_UTILITY_DTL";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_cam_utility_dtl_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_cam_utility_hdr_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_detail_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_utility_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_file_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_text_message", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod) {
            return insert(mod, mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof CamUtilityDetailMod) && (outputMod instanceof CamUtilityDetailMod)) {
                Map inParameters = new HashMap();
                CamUtilityDetailMod aInputMod = (CamUtilityDetailMod) inputMod;
                CamUtilityDetailMod aOutputMod = (CamUtilityDetailMod) outputMod;
                
                int utilityDtlId = RutString.toInteger(aInputMod.getUtilityDtlId(), 0);
                int utilityHdrId = RutString.toInteger(aInputMod.getUtilityHdrId(), 0);
                inParameters.put("p_cam_utility_dtl_id", new Integer(utilityDtlId));
                inParameters.put("p_cam_utility_hdr_id", new Integer(utilityHdrId));
                inParameters.put("p_detail_name", aInputMod.getDetailName());
                inParameters.put("p_utility_type", aInputMod.getUtilityType());
                inParameters.put("p_file_name", aInputMod.getFileName());
                inParameters.put("p_text_message", aInputMod.getTextMessage());
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_cam_utility_dtl_id = "+inParameters.get("p_cam_utility_dtl_id"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_cam_utility_hdr_id = "+inParameters.get("p_cam_utility_hdr_id"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_detail_name = "+inParameters.get("p_detail_name"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_utility_type = "+inParameters.get("p_utility_type"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_file_name = "+inParameters.get("p_file_name"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_text_message = "+inParameters.get("p_text_message"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_add_user = "+inParameters.get("p_record_add_user"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_add_date = "+inParameters.get("p_record_add_date"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[CamUtilityHeaderJdbcDao][InsertStoreProcedure]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setUtilityDtlId(RutDatabase.dbToStrInteger(outParameters, "p_cam_utility_dtl_id"));
                    aOutputMod.setUtilityHdrId(RutDatabase.dbToStrInteger(outParameters, "p_cam_utility_hdr_id"));
                    aOutputMod.setDetailName(RutDatabase.dbToString(outParameters, "p_detail_name"));
                    aOutputMod.setUtilityType(RutDatabase.dbToString(outParameters, "p_utility_type"));
                    aOutputMod.setFileName(RutDatabase.dbToString(outParameters, "p_file_name"));
                    aOutputMod.setTextMessage(RutDatabase.dbToString(outParameters, "p_text_message"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordAddUser(RutDatabase.dbToString(outParameters, "p_record_add_user"));
                    aOutputMod.setRecordAddDate(RutDatabase.dbToTimestamp(outParameters, "p_record_add_date"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_UTILITY_DTL.PRR_UPD_UTILITY_DTL";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_cam_utility_dtl_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_cam_utility_hdr_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_detail_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_utility_type", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_file_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_text_message", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean update(RrcStandardMod mod) {
            return update(mod, mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if ((inputMod instanceof CamUtilityDetailMod) && (outputMod instanceof CamUtilityDetailMod)) {
                Map inParameters = new HashMap();
                CamUtilityDetailMod aInputMod = (CamUtilityDetailMod) inputMod;
                CamUtilityDetailMod aOutputMod = (CamUtilityDetailMod) outputMod;
                
                int utilityDtlId = RutString.toInteger(aInputMod.getUtilityDtlId(), 0);
                int utilityHdrId = RutString.toInteger(aInputMod.getUtilityHdrId(), 0);
                inParameters.put("p_cam_utility_dtl_id", new Integer(utilityDtlId));
                inParameters.put("p_cam_utility_hdr_id", new Integer(utilityHdrId));
                inParameters.put("p_detail_name", aInputMod.getDetailName());
                inParameters.put("p_utility_type", aInputMod.getUtilityType());
                inParameters.put("p_file_name", aInputMod.getFileName());
                
                System.out.println("#### aInputMod.getTextMessage() = "+aInputMod.getTextMessage());
                inParameters.put("p_text_message", aInputMod.getTextMessage());
                System.out.println("#### p_text_message = "+inParameters.get("p_text_message"));
                
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_cam_utility_dtl_id = "+inParameters.get("p_cam_utility_dtl_id"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_cam_utility_hdr_id = "+inParameters.get("p_cam_utility_hdr_id"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_detail_name = "+inParameters.get("p_detail_name"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_utility_type = "+inParameters.get("p_utility_type"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_file_name = "+inParameters.get("p_file_name"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_text_message = "+inParameters.get("p_text_message"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[CamUtilityHeaderJdbcDao][UpdateStoreProcedure]: p_record_change_date = "+inParameters.get("p_record_change_date"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setUtilityDtlId(RutDatabase.dbToStrInteger(outParameters, "p_cam_utility_dtl_id"));
                    aOutputMod.setUtilityHdrId(RutDatabase.dbToStrInteger(outParameters, "p_cam_utility_hdr_id"));
                    aOutputMod.setDetailName(RutDatabase.dbToString(outParameters, "p_detail_name"));
                    aOutputMod.setUtilityType(RutDatabase.dbToString(outParameters, "p_utility_type"));
                    aOutputMod.setFileName(RutDatabase.dbToString(outParameters, "p_file_name"));
                    aOutputMod.setTextMessage(RutDatabase.dbToString(outParameters, "p_text_message"));
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                }
            }
            return isSuccess;
        }
    }
    
}



