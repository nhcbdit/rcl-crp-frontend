/*-----------------------------------------------------------------------------------------------------------  
CamAgentJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 24/04/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamAgentMod;
import com.rclgroup.dolphin.web.util.RutString;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class CamAgentJdbcDao extends RrcStandardDao implements CamAgentDao {

    public CamAgentJdbcDao() {
    }
        
    protected void initDao() throws Exception {
        super.initDao();
    }
        
    public boolean isValid(String agentCode,String status) throws DataAccessException {
        System.out.println("[CamAgentJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT LINE_CODE ");
        sql.append("      ,REGION_CODE ");
        sql.append("      ,AGENT_CODE ");
        sql.append("FROM VR_CAM_AGENT ");
        sql.append("WHERE 1 = 1 ");
        if((agentCode!=null)&&(!agentCode.trim().equals(""))){
            sql.append("  AND AGENT_CODE = :agentCode ");
        }
        sql.append("  AND STATUS = :status "); 
        HashMap map = new HashMap();
        map.put("agentCode",agentCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[CamAgentJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreen(String find,String search,String wild,String regionCode,String countryCode) throws DataAccessException {
        System.out.println("[CamAgentJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);        
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT LINE_CODE ");
        sql.append("      ,REGION_CODE ");
        sql.append("      ,COUNTRY_CODE ");
        sql.append("      ,AGENT_CODE "); 
        sql.append("      ,AGENT_NAME ");
        sql.append("      ,STATUS ");
        sql.append("FROM VR_CAM_AGENT ");
        sql.append("WHERE 1 = 1 ");
        if((regionCode!=null)&&(!regionCode.trim().equals(""))){
            sql.append("AND REGION_CODE = :regionCode "); 
        }
        if((countryCode!=null)&&(!countryCode.trim().equals(""))){
            sql.append("AND COUNTRY_CODE = :countryCode "); 
        }
        sql.append(sqlCriteria);         
        sql.append("ORDER BY LINE_CODE ");
        sql.append("        ,REGION_CODE ");
        sql.append("        ,AGENT_CODE ");
        System.out.println("[CamAgentJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        
        HashMap map = new HashMap();
        map.put("regionCode",regionCode);
        map.put("countryCode",countryCode);
        System.out.println("[CamAgentJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("L")) {
                sqlCriteria = "AND LINE_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("R")) {
                sqlCriteria = "AND REGION_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("A")) {
                sqlCriteria = "AND AGENT_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("N")) {
                sqlCriteria = "AND AGENT_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    } 
    
    public List listForHelpScreenWithUserLevel(String find,String search,String wild,String lineCode,String regionCode,String status) throws DataAccessException {
        System.out.println("[CamAgentJdbcDao][listForHelpScreenWithUserLevel]: Started");
        String sqlCriteria = createSqlCriteriaForHelpScreenWithUserLevel(find,search,wild);          
        
        StringBuffer sql = new StringBuffer(); 
        sql.append("SELECT LINE_CODE ");
        sql.append("      ,REGION_CODE ");
        sql.append("      ,AGENT_CODE "); 
        sql.append("      ,AGENT_NAME ");
        sql.append("      ,STATUS ");
        sql.append("FROM VR_CAM_AGENT ");
        sql.append("WHERE STATUS = :status "); 
        if((lineCode!=null)&&(!lineCode.trim().equals(""))){
            sql.append("  AND LINE_CODE = :lineCode "); 
        }
        if((regionCode!=null)&&(!regionCode.trim().equals(""))){
            sql.append("  AND REGION_CODE = :regionCode "); 
        }
        sql.append(sqlCriteria);         
        sql.append("ORDER BY LINE_CODE ");
        sql.append("        ,REGION_CODE ");
        sql.append("        ,AGENT_CODE ");    
        System.out.println("[CamAgentJdbcDao][listForHelpScreenWithUserLevel]: SQL: " + sql.toString());
       
        HashMap map = new HashMap();
        map.put("status",status);
        map.put("lineCode",lineCode);
        map.put("regionCode",regionCode); 
        System.out.println("[CamAgentJdbcDao][listForHelpScreenWithUserLevel]: Finished");
        return getNamedParameterJdbcTemplate().query(
                       sql.toString(),
                       map,
                       new RowModMapperForHelpWithUserLevel()); 
    }
    
    private String createSqlCriteriaForHelpScreenWithUserLevel(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("L")) {
                sqlCriteria = "AND LINE_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("R")) {
                sqlCriteria = "AND REGION_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("A")) {
                sqlCriteria = "AND AGENT_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("N")) {
                sqlCriteria = "AND AGENT_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelpWithCountryCode(rs);
        }
    } 
    
    protected class RowModMapperForHelpWithUserLevel implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelpScreenWithUserLevel(rs);
        }
    }
    
    private CamAgentMod moveDbToModelForHelpScreenWithUserLevel(ResultSet rs) throws SQLException {
        CamAgentMod agent = new CamAgentMod();
        agent.setLineCode(RutString.nullToStr(rs.getString("LINE_CODE")));
        agent.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));
        agent.setAgentCode(RutString.nullToStr(rs.getString("AGENT_CODE")));
        agent.setAgentName(RutString.nullToStr(rs.getString("AGENT_NAME")));
        agent.setStatus(RutString.nullToStr(rs.getString("STATUS")));
        return agent;
    } 
    
    private CamAgentMod moveDbToModelForHelpWithCountryCode(ResultSet rs) throws SQLException {
        CamAgentMod agent = new CamAgentMod();
        agent.setLineCode(RutString.nullToStr(rs.getString("LINE_CODE")));
        agent.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));
        agent.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
        agent.setAgentCode(RutString.nullToStr(rs.getString("AGENT_CODE")));
        agent.setAgentName(RutString.nullToStr(rs.getString("AGENT_NAME")));
        agent.setStatus(RutString.nullToStr(rs.getString("STATUS")));
        return agent;
    }  
}

