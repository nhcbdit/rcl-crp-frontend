/*-----------------------------------------------------------------------------------------------------------  
EmsEquipmentSysValidateDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 28/04/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 30/04/2009   WAC                   Added isValid,getErrDescription method.
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dex.DexVesselScheduleMod;
import com.rclgroup.dolphin.web.model.ems.EmsEquipmentSysValidateMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsEquipmentSysValidateJdbcDao extends RrcStandardDao implements EmsEquipmentSysValidateDao {
    static final String REGION_ALL = "*";

    private EmsEquipmentSysValidateDao emsEquipmentSysValidateDao;

    public EmsEquipmentSysValidateJdbcDao() {
        super();
    }

    public void setEmsEquipmentSysValidateDao(EmsEquipmentSysValidateDao emsEquipmentSysValidateDao) {
        this.emsEquipmentSysValidateDao = emsEquipmentSysValidateDao;
    }

    protected void initDao() throws Exception {
        super.initDao();
    }
    public boolean isValid(String errcode) throws DataAccessException {
        System.out.println("[EmsEquipmentSysValidateJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ERROR_CODE ");
        sql.append("FROM VR_EMS_EQ_SYS_VALIDATION ");
        sql.append("WHERE ERROR_CODE = :errcode ");
        HashMap map = new HashMap();
        map.put("errcode",errcode);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }         
        System.out.println("[EmsEquipmentSysValidateJdbcDao][isValid]: Finished");
        return isValid;
    }   
    public String getErrDescription(String errcode) {
        System.out.println("[EmsEquipmentSysValidateJdbcDao][getErrDescription]: Started");
        String errDesc = "";
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ERROR_DESCRIPTION ");
        sql.append("FROM VR_EMS_EQ_SYS_VALIDATION ");
        sql.append("WHERE ERROR_CODE = :errcode");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("errcode", errcode.toUpperCase()));
        if(rs.next()) {
            errDesc = RutString.nullToStr(rs.getString("ERROR_DESCRIPTION"));
        }
        System.out.println("[EmsEquipmentSysValidateJdbcDao][getErrDescription]: Finished");
        return errDesc;
    }


    public List listForHelpScreen(String find, String search, String wild, String regionCode, String errorType, String status) throws DataAccessException{
        System.out.println("[EmsEquipmentSysValidateJdbcDao][listForHelpScreen]: Started");
        
        regionCode = (regionCode==null) ? "" : regionCode.trim();
        errorType = (errorType==null) ? "" : errorType.trim();
        status = (status==null) ? "" : status.trim();
        String sqlCriteria = createSqlHelpCriteria(find, search, wild, regionCode, errorType);
        
        StringBuffer sql = new StringBuffer();
        if ("*".equals(regionCode)) {
            sql.append("select distinct '"+REGION_ALL+"' as REGION_CODE ");
        } else {
            sql.append("select REGION_CODE ");    
        }
        sql.append("    ,ERROR_CODE ");
        sql.append("    ,ERROR_DESCRIPTION ");
        sql.append("    ,ERROR_TYPE ");
        sql.append("    ,STATUS ");
        sql.append("from VR_EMS_EQ_SYS_VALIDATION ");
        sql.append("where STATUS = :status ");
        sql.append(sqlCriteria);
        sql.append("order by ERROR_CODE");
        
        HashMap map = new HashMap();
        map.put("status", status);
        
        System.out.println("[EmsEquipmentSysValidateJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[EmsEquipmentSysValidateJdbcDao][listForHelpScreen]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            EmsEquipmentSysValidateMod bean = new EmsEquipmentSysValidateMod();
                            bean.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));
                            bean.setErrorCode(RutString.nullToStr(rs.getString("ERROR_CODE")));
                            bean.setErrorDescription(RutString.nullToStr(rs.getString("ERROR_DESCRIPTION")));
                            bean.setErrorType(RutString.nullToStr(rs.getString("ERROR_TYPE")));
                            bean.setStatus(RutString.nullToStr(rs.getString("STATUS")));
                            return bean;
                        }
                    });
    }
    
    private String createSqlHelpCriteria(String find, String search, String wild, String regionCode, String errorType) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("EC")) {
                sqlCriteria = " and ERROR_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("ED")) {
                sqlCriteria = " and ERROR_DESCRIPTION " + sqlWild;
            }
        }

        if (regionCode != null && !"".equals(regionCode) && !REGION_ALL.equals(regionCode)) {
            sqlCriteria += " and REGION_CODE = '" + regionCode + "' ";
        }
        if (errorType != null && !"".equals(errorType)) {
            sqlCriteria += " and ERROR_TYPE = '" + errorType + "' ";
        }

        return sqlCriteria;
    }
 
}
