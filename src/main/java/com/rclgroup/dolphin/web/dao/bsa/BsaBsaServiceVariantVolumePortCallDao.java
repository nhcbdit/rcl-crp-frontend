/*-----------------------------------------------------------------------------------------------------------  
BsaBsaServiceVariantVolumePortCallDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 12/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.sql.Timestamp;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface BsaBsaServiceVariantVolumePortCallDao extends RriStandardDao {
    
    /**
     * update a BSA model record
     * @param mod a BSA model model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: BSA_PCZ01_BSA_PORT_CALL_ID_REQ
     *                             error message: BSA_PCZ01_BSA_SERVICE_VARIANT_ID_REQ
     *                             error message: BSA_PCZ01_SERVICE_REQ
     *                             error message: BSA_PCZ01_PROFORMA_REF_NO_REQ
     *                             error message: BSA_PCZ01_PORT_REQ
     *                             error message: BSA_PCZ01_PORT_SEQ_NO_FROM_REQ
     *                             error message: BSA_PCZ01_DIRECTION_FROM_REQ
     *                             error message: BSA_PCZ01_PORT_SEQ_NO_TO_REQ 
     *                             error message: BSA_PCZ01_DIRECTION_TO_REQ
     *                             error message: BSA_PCZ01_LOAD_DISCHARGE_FLAG_REQ
     *                             error message: BSA_PCZ01_TRANSHIPMENT_FLAG_REQ
     *                             error message: BSA_PCZ01_WAYPORT_TRUNK_INDICATOR_REQ
     *                             error message: BSA_PCZ01_PORT_CALL_LEVEL_REQ  
     *                             error message: BSA_PCZ01_SLOT_TEU_REQ
     *                             error message: BSA_PCZ01_SLOT_TONS_REQ
     *                             error message: BSA_PCZ01_SLOT_REEFER_PLUGS_REQ
     *                             error message: BSA_PCZ01_AVG_COC_TEU_WEIGHT_REQ
     *                             error message: BSA_PCZ01_AVG_SOC_TEU_WEIGHT_REQ
     *                             error message: BSA_PCZ01_MIN_TEU_REQ
     *                             error message: BSA_PCZ01_RECORD_STATUS_REQ
     *                             error message: BSA_PCZ01_STATUS_NOT_IN_RANGE
     *                             error message: BSA_PCZ01_RECORD_CHANGE_USER_REQ
     *                             error message: BSA_PCZ01_RECORD_CHANGE_DATE_REQ
     *                             error message: BSA_PCZ01_UPDATE_CON
     *                             error message: BSA_PCZ01_BSA_PORT_CALL_NOT_FOUND
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean update(RrcStandardMod mod) throws DataAccessException;
    
    public List listPortCallFindByKeyBsaServiceVariantID(String bsaServiceVariantId) throws DataAccessException;

    /**
     * find BSA Port Call of service variant ID
     * @param bsaServiceVariantId
     * @return
     * @throws DataAccessException
     */
     
     
//public List listAddPortCallFindByKeyBsaServiceVariantID(String bsaServiceVariantId) throws DataAccessException;
     
     

     
 //public void saveMasterDetails(String masterOperationFlag, List dtlModifiedMods,  Timestamp copyRecordChangeDate) throws DataAccessException; // ##02
     
     
  //public boolean insertAddPortCall(RrcStandardMod mod) throws DataAccessException;
  
  
//  public boolean updateAddPortCall(RrcStandardMod mod) throws DataAccessException;

  /**
   * delete a supported port group record
   * @param mod a supported port group model
   * @return whether deletion is successful
   * @throws DataAccessException exception which client has to catch all following error messages:  
   *                             error message: BSA_SPG01_RECORD_CHANGE_DATE_REQ 
   *                             error message: BSA_SPG01_DELETE_CON  
   *                             error message: BSA_SPG01_BSA_PORT_GROUP_NOT_FOUND with primary key from input value 
   *                             error message: ORA-XXXXX (un-exceptional oracle error)
   */
  
  /**
   * check constraint on a supported port group record
   * @param mod a supported port group model
   * @throws DataAccessException exception which client has to catch all following error messages:  
   *                             error message: BSA_SPG01_PORT_GROUP_CONSTRAINT01
   *                             error message: BSA_SPG01_PORT_GROUP_CONSTRAINT01
   *                             error message: ORA-XXXXX (un-exceptional oracle error)     
   */
  // public void checkConstraint(RrcStandardMod mod) throws DataAccessException;
   
   
 //   public boolean delete(RrcStandardMod mod) throws DataAccessException;
//   public List findBsaAddLoadDischByPortCallId(String portCallId, String recordStatus) throws DataAccessException;
   
   
}


