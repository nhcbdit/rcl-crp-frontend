/*-----------------------------------------------------------------------------------------------------------  
DimDeliveryOrderPrintJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 25/09/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dim.DimDeliveryOrderPrintMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;


public class DimDeliveryOrderPrintJdbcDao extends RrcStandardDao implements DimDeliveryOrderPrintDao{
    
    public DimDeliveryOrderPrintJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    //begin: Delivery Order Print
    public String makeDoPrintSqlStatment(String criteriaBy, String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String permissionUser) {
        StringBuffer sb = new StringBuffer();  
        
        if ("CRITERIA_BY_VSL".equals(criteriaBy)) {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");    
            sb.append("    ,POD_TERMINAL ");
        } else {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");
            sb.append("    ,POD_TERMINAL ");
        }
        sb.append("from VR_DIM_DELIVERY_ORDER_PRINT vr ");
           
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(service)) {
            sbWhere.append(" and SERVICE = '"+service+"' ");
        }
        if (!RutString.isEmptyString(vessel)) {
            sbWhere.append(" and VESSEL = '"+vessel+"' ");
        }
        if (!RutString.isEmptyString(voyage)) {
            sbWhere.append(" and VOYAGE = '"+voyage+"' ");
        }
        if (!RutString.isEmptyString(direction)) {
            sbWhere.append(" and DIRECTION = '"+direction+"' ");
        }
        if (!RutString.isEmptyString(pol)) {
            sbWhere.append(" and POL = '"+pol+"' ");
        }
        if (!RutString.isEmptyString(pod)) {
            sbWhere.append(" and POD = '"+pod+"' ");
        }
        if (!RutString.isEmptyString(podTerminal)) {
            sbWhere.append(" and POD_TERMINAL = '"+podTerminal+"' ");
        }
        if (!RutString.isEmptyString(blId)) {
            sbWhere.append(" and BL_ID = '"+blId+"' ");
        }
        if (!RutString.isEmptyString(permissionUser)) {
            sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
            sb.append(" [and :columnName :conditionWild :columnFind] ");
        } else {
            sb.append("[where :columnName :conditionWild :columnFind] ");
        }
        
        sb.append("[order by :sortBy :sortIn] ");
        return sb.toString();
    }
    
    public int findCountForDoPrintByVesselVoyageList(String columnName, String conditionWild, String columnFind
        ,String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findCountForDoPrintByVesselVoyageList]: Started");
        
        String sqlStatement = this.makeDoPrintSqlStatment("CRITERIA_BY_VSL", service, vessel, voyage, direction, pol, pod, podTerminal, blId, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, "", "");
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findCountForDoPrintByVesselVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findCountForDoPrintByVesselVoyageList]: Finished");
        
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStatement, new HashMap(), Integer.class);
    }
    
    public String makeDoPrintSqlStatment(String criteriaBy, String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String permissionUser) throws DataAccessException {
        StringBuffer sb = new StringBuffer();  
        
        if ("CRITERIA_BY_VSL".equals(criteriaBy)) {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");    
            sb.append("    ,POD_TERMINAL ");
        } else {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");
            sb.append("    ,POD_TERMINAL ");
        }
        sb.append("from VR_DIM_DELIVERY_ORDER_PRINT vr ");
           
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(sessionId) && !RutString.isEmptyString(invoyagePort)) {
            sbWhere.append(" and exists ( select 1 ");
            sbWhere.append("    from IDP005 i05 ");
            sbWhere.append("        ,VR_RCM_INVOYAGE_BROWSER ib ");
            sbWhere.append("    where i05.SYBLNO = vr.BL_NO ");
            sbWhere.append("        and i05.SERVICE = ib.SERVICE ");
            sbWhere.append("        and i05.VESSEL = ib.VESSEL ");
            sbWhere.append("        and i05.VOYAGE = ib.VOYAGE ");
            sbWhere.append("        and i05.LOAD_PORT = ib.PORT ");
            sbWhere.append("        and i05.DISCHARGE_PORT = '"+invoyagePort+"' ");
            sbWhere.append("        and ib.SESSION_ID = '"+sessionId+"' ) ");
        }
        if (!RutString.isEmptyString(pol)) {
            sbWhere.append(" and POL = '"+pol+"' ");
        }
        if (!RutString.isEmptyString(pod)) {
            sbWhere.append(" and POD = '"+pod+"' ");
        }
        if (!RutString.isEmptyString(podTerminal)) {
            sbWhere.append(" and POD_TERMINAL = '"+podTerminal+"' ");
        }
        if (!RutString.isEmptyString(blId)) {
            sbWhere.append(" and BL_ID = '"+blId+"' ");
        }
        if (!RutString.isEmptyString(permissionUser)) {
            sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
            sb.append(" [and :columnName :conditionWild :columnFind] ");
        } else {
            sb.append("[where :columnName :conditionWild :columnFind] ");
        }
        
        sb.append("[order by :sortBy :sortIn] ");
        return sb.toString();
    }
    
    public int findCountForDoPrintByInVoyageList(String columnName, String conditionWild, String columnFind 
        ,String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findCountForDoPrintByInVoyageList]: Started");
        
        String sqlStatement = this.makeDoPrintSqlStatment("CRITERIA_BY_VSL", invoyagePort, sessionId, pol, pod, podTerminal, blId, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, "", "");
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findCountForDoPrintByInVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findCountForDoPrintByInVoyageList]: Finished");
        
        return (int) getNamedParameterJdbcTemplate().queryForObject(sqlStatement, new HashMap(), Integer.class);
    }
    
    public List findDoPrintByVesselVoyageList(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortIn
        ,String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByVesselVoyageList]: Started");
        
        String sqlStatement = this.makeDoPrintSqlStatment("CRITERIA_BY_VSL", service, vessel, voyage, direction, pol, pod, podTerminal, blId, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, sortBy, sortIn);
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByVesselVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByVesselVoyageList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimDeliveryOrderPrintMod bean = new DimDeliveryOrderPrintMod();
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setPol(RutString.nullToStr(rs.getString("POL")));
                           bean.setPod(RutString.nullToStr(rs.getString("POD")));
                           bean.setPodTerminal(RutString.nullToStr(rs.getString("POD_TERMINAL")));
                           return bean;
                       }
                   });
    }
    
    public List findDoPrintByInVoyageList(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortIn
        ,String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByInVoyageList]: Started");
        
        String sqlStatement = this.makeDoPrintSqlStatment("CRITERIA_BY_VSL", invoyagePort, sessionId, pol, pod, podTerminal, blId, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, sortBy, sortIn);
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByInVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByInVoyageList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimDeliveryOrderPrintMod bean = new DimDeliveryOrderPrintMod();
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setPol(RutString.nullToStr(rs.getString("POL")));
                           bean.setPod(RutString.nullToStr(rs.getString("POD")));
                           bean.setPodTerminal(RutString.nullToStr(rs.getString("POD_TERMINAL")));
                           return bean;
                       }
                   });
    }
    
    public List findDoPrintByBlList(String columnName, String conditionWild, String columnFind, String vessel, String voyage, String pol, String pod, String podTerminal, String permissionUser, String blId) {
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByBlList]: Started");
        
        //String checkBL_ID = "";
        
        StringBuffer sb = new StringBuffer();  
        sb.append("select distinct SERVICE ");
        sb.append(" ,VESSEL ");
        sb.append(" ,VOYAGE ");
        sb.append(" ,DIRECTION ");
        sb.append(" ,POL ");
        sb.append(" ,POD ");    
        sb.append(" ,POD_TERMINAL ");
        sb.append(" ,BL_ID ");
        sb.append(" ,BL_NO ");
        sb.append(" ,HBL_NO ");
        sb.append(" ,DELIVERY_ORDER_NO ");
        sb.append(" ,DELIVERY_ORDER_DATE ");
        sb.append(" ,REMARKS ");
        sb.append("from VR_DIM_DELIVERY_ORDER_PRINT ");
        sb.append("where VESSEL = :vessel ");
        sb.append(" and VOYAGE = :voyage ");
        sb.append(" and POL = :pol ");
        sb.append(" and POD = :pod ");
        sb.append(" and POD_TERMINAL = :podTerminal ");
        sb.append(" and BL_ID = nvl('"+blId+"',BL_ID) ");
        if (!RutString.isEmptyString(permissionUser)) {
            sb.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }
        /*if(blId.equals("F")){
            System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByBlList]: ---> BL ID = 'F'");
            sb.append(" and BL_NO != nvl(HBL_NO,BL_NO) ");
        }*/
        if (!RutString.isEmptyString(columnName) && !RutString.isEmptyString(columnFind)) {
            if (RcmConstant.WILD_DEFAULT.equals(conditionWild)) {
                sb.append(" and "+columnName+" like '%"+columnFind+"%' ");
            } else {
                sb.append(" and "+columnName+" = '"+columnFind+"' ");
            }
        }
        
        sb.append("order by REMARKS desc ");
        sb.append(" ,DELIVERY_ORDER_DATE ");
        sb.append(" ,BL_NO ");
        
        HashMap map = new HashMap();
        map.put("vessel", vessel);
        map.put("voyage", voyage);
        map.put("pol", pol);
        map.put("pod", pod);
        map.put("podTerminal", podTerminal);
        
        String sqlStatement = sb.toString();
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByBlList]: sql = "+sqlStatement);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = vessel:"+vessel);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = voyage:"+voyage);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = pol:"+pol);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = pod:"+pod);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = podTerminal:"+podTerminal);
        System.out.println("[DimDeliveryOrderPrintJdbcDao][findDoPrintByBlList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                map,
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimDeliveryOrderPrintMod bean = new DimDeliveryOrderPrintMod();
                           bean.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                           bean.setPol(RutString.nullToStr(rs.getString("POL")));
                           bean.setPod(RutString.nullToStr(rs.getString("POD")));
                           bean.setPodTerminal(RutString.nullToStr(rs.getString("POD_TERMINAL")));
                           bean.setBlId(RutString.nullToStr(rs.getString("BL_ID")));
                           bean.setBlNo(RutString.nullToStr(rs.getString("BL_NO")));
                           bean.setHblNo(RutString.nullToStr(rs.getString("HBL_NO")));
                           bean.setDeliveryOrderNo(RutString.nullToStr(rs.getString("DELIVERY_ORDER_NO")));
                           bean.setDeliveryOrderDate(RutString.nullToStr(rs.getString("DELIVERY_ORDER_DATE")));
                           bean.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                           return bean;
                       }
                   });
    }
    //end: Delivery Order Print
    
}
