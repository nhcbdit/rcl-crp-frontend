/*-----------------------------------------------------------------------------------------------------------  
EmsTerminalJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 10/06/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsTerminalMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsTerminalJdbcDao extends RrcStandardDao implements EmsTerminalDao {

    public EmsTerminalJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String terminalCode) throws DataAccessException {
        System.out.println("[EmsTerminalJdbcDao][isValid]: Started");
        boolean isValid = false;    
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERMINAL_CODE ");
        sql.append("FROM VR_EMS_TERMINAL ");
        sql.append("WHERE TERMINAL_CODE = :terminalCode");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("terminalCode", terminalCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[EmsTerminalJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String controlZoneCode, String zoneCode, String point) throws DataAccessException {
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERMINAL_CODE ");
        sql.append("      ,TERMINAL_NAME ");
        sql.append("      ,POINT_CODE ");
        sql.append("      ,FLAG ");
        sql.append("      ,ZONE ");
        sql.append("      ,CONTROL_ZONE ");
        sql.append("      ,AREA ");
        sql.append("      ,REGION ");
        sql.append("      ,STATUS "); 
        sql.append("FROM VR_EMS_TERMINAL ");
        sql.append("WHERE STATUS = 'Active' ");
        if((regionCode!=null)&&(!regionCode.trim().equals(""))){
            sql.append("  AND REGION = :regionCode "); 
        }
        if((areaCode!=null)&&(!areaCode.trim().equals(""))){
            sql.append("  AND AREA = :areaCode "); 
        }  
        if((controlZoneCode!=null)&&(!controlZoneCode.trim().equals(""))){
            sql.append("  AND CONTROL_ZONE = :controlZoneCode "); 
        }
        if((zoneCode!=null)&&(!zoneCode.trim().equals(""))){
            sql.append("  AND ZONE = :zoneCode "); 
        }
        if((point!=null)&&(!point.trim().equals(""))){
            sql.append("  AND POINT_CODE = :point "); 
        }
        sql.append(sqlCriteria);            
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        HashMap map = new HashMap();
        map.put("regionCode",regionCode);
        map.put("areaCode",areaCode);
        map.put("controlZoneCode",controlZoneCode);
        map.put("zoneCode",zoneCode);
        map.put("point",point);
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());
    }
    
    public List listForHelpScreenWithPoint(String find, String search, String wild, String point) throws DataAccessException {
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERMINAL_CODE ");
        sql.append("      ,TERMINAL_NAME ");
        sql.append("      ,POINT_CODE ");
        sql.append("      ,FLAG ");
        sql.append("      ,ZONE ");
        sql.append("      ,CONTROL_ZONE ");
        sql.append("      ,AREA ");
        sql.append("      ,REGION ");
        sql.append("      ,STATUS "); 
        sql.append("FROM VR_EMS_TERMINAL ");
        sql.append("WHERE STATUS = 'Active' ");
        if((point!=null)&&(!point.trim().equals(""))){
            sql.append("  AND POINT_CODE = :point "); 
        } 
        sql.append(sqlCriteria);            
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Finished");
        HashMap map = new HashMap();
        map.put("point",point);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());
    }
    
    public List listForHelpScreenWithFsc(String find, String search, String wild, String fsc, String point) throws DataAccessException {
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERMINAL_CODE ");
        sql.append("      ,TERMINAL_NAME ");
        sql.append("      ,POINT_CODE ");
        sql.append("      ,FLAG ");
        sql.append("      ,ZONE ");
        sql.append("      ,CONTROL_ZONE ");
        sql.append("      ,AREA ");
        sql.append("      ,REGION ");
        sql.append("      ,STATUS "); 
        sql.append("FROM VR_EMS_TERMINAL ");
        sql.append("WHERE STATUS = 'Active' ");
        if((fsc!=null)&&(!fsc.trim().equals(""))){
            sql.append("  AND FSC = :fsc "); 
        }
        if((point!=null)&&(!point.trim().equals(""))){
            sql.append("  AND POINT_CODE = :point "); 
        } 
        sql.append(sqlCriteria);            
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Finished");
        HashMap map = new HashMap();
        map.put("fsc",fsc);
        map.put("point",point);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());
    }
    
    public List listForHelpScreenWithControlZone(String find, String search, String wild, String controlZone, String point) throws DataAccessException {
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TERMINAL_CODE ");
        sql.append("      ,TERMINAL_NAME ");
        sql.append("      ,POINT_CODE ");
        sql.append("      ,FLAG ");
        sql.append("      ,ZONE ");
        sql.append("      ,CONTROL_ZONE ");
        sql.append("      ,AREA ");
        sql.append("      ,REGION ");
        sql.append("      ,STATUS "); 
        sql.append("FROM VR_EMS_TERMINAL ");
        sql.append("WHERE STATUS = 'Active' ");
        if((controlZone!=null)&&(!controlZone.trim().equals(""))){
            sql.append("  AND CONTROL_ZONE = :controlZone "); 
        }
        if((point!=null)&&(!point.trim().equals(""))){
            sql.append("  AND POINT_CODE = :point "); 
        } 
        sql.append(sqlCriteria);            
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[EmsTerminalJdbcDao][listForHelpScreen]: Finished");
        HashMap map = new HashMap();
        map.put("controlZone",controlZone);
        map.put("point",point);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper()); 
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND TERMINAL_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "AND TERMINAL_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("P")){
                sqlCriteria = "AND POINT_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("F")){
                sqlCriteria = "AND FLAG " + sqlWild;
            }else if(search.equalsIgnoreCase("Z")){
                sqlCriteria = "AND ZONE " + sqlWild;
            }else if(search.equalsIgnoreCase("CZ")){
                sqlCriteria = "AND CONTROL_ZONE " + sqlWild;
            }else if(search.equalsIgnoreCase("A")){
                sqlCriteria = "AND AREA " + sqlWild;
            }else if(search.equalsIgnoreCase("R")){
                sqlCriteria = "AND REGION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private EmsTerminalMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        EmsTerminalMod terminalCode = new EmsTerminalMod();
        terminalCode.setTerminalCode(RutString.nullToStr(rs.getString("TERMINAL_CODE")));
        terminalCode.setTerminalName(RutString.nullToStr(rs.getString("TERMINAL_NAME")));
        terminalCode.setPointCode(RutString.nullToStr(rs.getString("POINT_CODE")));
        terminalCode.setFlag(RutString.nullToStr(rs.getString("FLAG")));
        terminalCode.setZone(RutString.nullToStr(rs.getString("ZONE")));
        terminalCode.setControlZone(RutString.nullToStr(rs.getString("CONTROL_ZONE")));
        terminalCode.setArea(RutString.nullToStr(rs.getString("AREA")));
        terminalCode.setRegion(RutString.nullToStr(rs.getString("REGION")));
        terminalCode.setTerminalStatus(RutString.nullToStr(rs.getString("STATUS")));

        return terminalCode;
    }

}
