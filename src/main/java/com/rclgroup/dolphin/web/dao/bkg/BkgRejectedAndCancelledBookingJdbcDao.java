package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.bkg.BkgRejectedAndCancelledBookingReportMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BkgRejectedAndCancelledBookingJdbcDao extends RrcStandardDao implements BkgRejectedAndCancelledBookingDao{
    private BkgRejectedAndCancelledBookingStoreProcedure bkgRejectedAndCancelledBookingStoreProcedure;
    
    public BkgRejectedAndCancelledBookingJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        bkgRejectedAndCancelledBookingStoreProcedure = new BkgRejectedAndCancelledBookingStoreProcedure(getJdbcTemplate());
    }
    
    public boolean generateTempTable(RrcStandardMod mod) throws DataAccessException{
        return bkgRejectedAndCancelledBookingStoreProcedure.generate(mod);
    }

    protected class BkgRejectedAndCancelledBookingStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "RCLAPPS.PCR_BKG_REJECTED_CANCELLED.PRR_GEN_BKG_121_BOOKING";
        
        protected BkgRejectedAndCancelledBookingStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
            declareParameter(new SqlParameter("p_report_by", Types.VARCHAR));
            declareParameter(new SqlParameter("p_etd_from", Types.INTEGER));
            declareParameter(new SqlParameter("p_etd_to", Types.INTEGER));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_direction", Types.VARCHAR));
            declareParameter(new SqlParameter("p_booking_status", Types.VARCHAR));
            declareParameter(new SqlParameter("p_coc_soc", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_booking_no", Types.VARCHAR));
            declareParameter(new SqlParameter("p_port_call", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if (inputMod instanceof BkgRejectedAndCancelledBookingReportMod && outputMod instanceof BkgRejectedAndCancelledBookingReportMod) {
                BkgRejectedAndCancelledBookingReportMod aInputMod = (BkgRejectedAndCancelledBookingReportMod) inputMod;
                
                Map inParameters = new HashMap();
                inParameters.put("p_user_id", aInputMod.getUserId());
                inParameters.put("p_session_id", aInputMod.getSessionId());
                inParameters.put("p_report_by", aInputMod.getReportBy());
                inParameters.put("p_etd_from", aInputMod.getEtdFrom());
                inParameters.put("p_etd_to", aInputMod.getEtdTo());
                inParameters.put("p_service", aInputMod.getService());
                inParameters.put("p_vessel", aInputMod.getVessel());
                inParameters.put("p_voyage", aInputMod.getVoyage());
                inParameters.put("p_direction", aInputMod.getDirection());
                inParameters.put("p_booking_status", aInputMod.getBookingStatus());
                inParameters.put("p_coc_soc", aInputMod.getCocSoc());
                inParameters.put("p_pol", aInputMod.getPol());
                inParameters.put("p_pol_terminal", aInputMod.getPodTerminal());
                inParameters.put("p_pod", aInputMod.getPod());
                inParameters.put("p_pod_terminal", aInputMod.getPodTerminal());
                inParameters.put("p_booking_no", aInputMod.getBookingNo());
                inParameters.put("p_port_call", aInputMod.getPortCall());
                
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_user_id = "+inParameters.get("p_user_id"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_session_id = "+inParameters.get("p_session_id"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_report_by = "+inParameters.get("p_report_by"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_etd_from = "+inParameters.get("p_etd_from"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_etd_to = "+inParameters.get("p_etd_to"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_service = "+inParameters.get("p_service"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_vessel = "+inParameters.get("p_vessel"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_voyage = "+inParameters.get("p_voyage"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_direction = "+inParameters.get("p_direction"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_booking_status = "+inParameters.get("p_booking_status"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_coc_soc = "+inParameters.get("p_coc_soc"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_pol = "+inParameters.get("p_pol"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_pol_terminal = "+inParameters.get("p_pol_terminal"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_pod = "+inParameters.get("p_pod"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_pod_terminal = "+inParameters.get("p_pod_terminal"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_booking_no = "+inParameters.get("p_booking_no"));
                System.out.println("[BkgRejectedAndCancelledBookingJdbcDao][generateTempTable]: p_port_call = "+inParameters.get("p_port_call"));
                
                this.execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
}
