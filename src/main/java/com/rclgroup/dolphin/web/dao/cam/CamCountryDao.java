/*-----------------------------------------------------------------------------------------------------------  
CamCountryDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 24/04/08 
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
01 24/04/08  SPD                       Change to new framework
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamCountryDao {

    /**
     * check valid of country code 
     * @param countryCode
     * @return valid of country code 
     * @throws DataAccessException
     */
    public boolean isValid(String countryCode) throws DataAccessException;

    /**
     * check valid of country code with status
     * @param countryCode
     * @param status
     * @return valid of country code with status
     * @throws DataAccessException
     */
    public boolean isValid(String countryCode, String status) throws DataAccessException;

    /**
     * list country records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of country
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

    /**
     * list country records with user level for help screen
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of country with user level
     * @throws DataAccessException
     */
    public List listForHelpScreenWithUserLevel(String find, String search, String wild, String status) throws DataAccessException;

}
