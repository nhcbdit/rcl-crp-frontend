/*-----------------------------------------------------------------------------------------------------------  
BsaBsaModelJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 11/03/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 09/09/09 KIT        MODI           Add method getCalDuration  
02 16/09/09 WUT                       Implemented for changing supported port group
03 14/10/09 WUT                       Implemented flag page util
04 05/01/10 WUT                       Implemented a copy BSA Model function
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaBsaModelMod;
import com.rclgroup.dolphin.web.model.bsa.BsaSupportedPortGroupMod;
import com.rclgroup.dolphin.web.model.rcm.RcmModifiedObjectMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;

import java.math.BigDecimal;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class BsaBsaModelJdbcDao extends RrcStandardDao implements BsaBsaModelDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private CopyStoreProcedure copyStoreProcedure;
    private ChangePortGroupStoreProcedure changePortGroupStoreProcedure;
    private VerifyStoreProcedure verifyStoreProcedure;
    private GenerateVsaAllocation generateVsaAllocation;

    private BsaSupportedPortGroupDao bsaSupportedPortGroupDao;
    
    public BsaBsaModelJdbcDao() {
    }
    
    public void setBsaSupportedPortGroupDao(BsaSupportedPortGroupDao bsaSupportedPortGroupDao) {
        this.bsaSupportedPortGroupDao = bsaSupportedPortGroupDao;
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        copyStoreProcedure = new CopyStoreProcedure(getJdbcTemplate());
        changePortGroupStoreProcedure = new ChangePortGroupStoreProcedure(getJdbcTemplate());
        verifyStoreProcedure = new VerifyStoreProcedure(getJdbcTemplate());
        generateVsaAllocation = new GenerateVsaAllocation(getJdbcTemplate());
    }
    
    public List listForHelpScreen(String find, String search, String wild, String sortBy, String sortByIn, String status) throws DataAccessException{
        System.out.println("[BsaBsaModelDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlHelpCriteria(find,search,wild);
        String sqlOrderBy = createSqlHelpOrderBy(sortBy, sortByIn);
        status = (status==null) ? "" : status.trim();
        
        StringBuffer sql = new StringBuffer();
        sql.append("select BSA_MODEL_ID ");
        sql.append("    ,MODEL_NAME ");
        sql.append("    ,SIMULATION_FLAG ");
        sql.append("    ,VALID_FROM ");
        sql.append("    ,VALID_TO ");
        sql.append("    ,RECORD_STATUS ");
        sql.append("FROM VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append("WHERE RECORD_STATUS = :status ");
        sql.append(sqlCriteria);
        sql.append(sqlOrderBy);
        
        HashMap map = new HashMap();
        map.put("status", status);
        
        System.out.println("[BsaBsaModelDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[BsaBsaModelDao][listForHelpScreen]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            BsaBsaModelMod bean = new BsaBsaModelMod();
                            bean.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                            bean.setModelName(RutString.nullToStr(rs.getString("MODEL_NAME")));
                            bean.setSimulationFlag(RutString.nullToStr(rs.getString("SIMULATION_FLAG")));
                            bean.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                            bean.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                            bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            return bean;
                        }
                    });
    }
    
    private String createSqlHelpCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find + "%' ";
        } else {
            sqlWild = "= '" + find + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("MN")) {
                sqlCriteria = "AND MODEL_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("SF")) {
                sqlCriteria = "AND SIMULATION_FLAG " + sqlWild;
            }
        }
        return sqlCriteria;
    }
    
    private String createSqlHelpOrderBy(String sortBy, String sortByIn) {
        String sqlOrderBy = "";
        String sqlWild = "";

        if ((sortByIn!=null) && ("DESC".equals(sortByIn.trim()) || "ASC".equals(sortByIn.trim()))) {
            sqlWild = sortByIn.trim();
        } else {
            //default sort by in
            sqlWild = "ASC";
        }
        if (sortBy == null || "".equals(sortBy.trim())) {
        } else {
            if (sortBy.equalsIgnoreCase("MN")) {
                sqlOrderBy = "ORDER BY MODEL_NAME " + sqlWild;
            } else if (sortBy.equalsIgnoreCase("SF")) {
                sqlOrderBy = "ORDER BY SIMULATION_FLAG " + sqlWild;
            } else if (sortBy.equalsIgnoreCase("VF")) {
                sqlOrderBy = "ORDER BY VALID_FROM " + sqlWild;
            } else if (sortBy.equalsIgnoreCase("VT")) {
                sqlOrderBy = "ORDER BY VALID_TO " + sqlWild;
            }
        }
        return sqlOrderBy;
    }

    public List listForSearchScreen(RcmSearchMod searchMod, String validAt, String simulation) throws DataAccessException{
        Map columnMap = new HashMap();
        //for both findIn, and sortBy
        columnMap.put("M","MODEL_NAME");
        columnMap.put("S","SIMULATION_FLAG");
        columnMap.put("F","VALID_FROM");
        String sqlSearchCriteria = createSqlSearchCriteria(searchMod,columnMap,validAt,simulation);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT BSA_MODEL_ID ");
        sql.append("      ,MODEL_NAME ");
        sql.append("      ,SIMULATION_FLAG ");
        sql.append("      ,VALID_FROM ");        
        sql.append("      ,VALID_TO ");        
        sql.append("      ,AVG_MT_TEU_WEIGHT ");
        sql.append("      ,IMBALANCE_WARNING_PERC ");        
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append(sqlSearchCriteria);
        System.out.println("[BsaBsaModelJdbcDao][listForSearchScreen]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaBsaModelMod bsaModelMod = new BsaBsaModelMod();
                           bsaModelMod.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                           bsaModelMod.setModelName(RutString.nullToStr(rs.getString("MODEL_NAME")));
                           bsaModelMod.setSimulationFlag(RutString.nullToStr(rs.getString("SIMULATION_FLAG")));
                           bsaModelMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                           bsaModelMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                           bsaModelMod.setAverageMtTeuWeight(RutString.nullToStr(rs.getString("AVG_MT_TEU_WEIGHT")));
                           bsaModelMod.setImbalanceWarningPercentage(RutString.nullToStr(rs.getString("IMBALANCE_WARNING_PERC")));
                           bsaModelMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bsaModelMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                           bsaModelMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE")); 
                           bsaModelMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bsaModelMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                           return bsaModelMod;
                        }
                   });
    }
    
    private String createSqlSearchCriteria(RcmSearchMod searchMod, Map columnMap, String validAt, String simulation) {
        String sqlCriteria = "";
        String sqlWildWithUpperCase = "";
        String sqlSortByIn = "";
        String find = RutString.changeQuoteForSqlStatement(searchMod.getFind());
        String findIn = searchMod.getFindIn();
        String status = searchMod.getStatus();
        String sortBy = searchMod.getSortBy();
        String sortByIn = searchMod.getSortByIn();
        String wild = searchMod.getWild();

        if (wild.equalsIgnoreCase("ON")) {
            sqlWildWithUpperCase = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWildWithUpperCase = "= '" + find.toUpperCase() + "' ";
        }

        if (sortByIn.equalsIgnoreCase("ASC")) {
            sqlSortByIn = "ASC";
        } else if (sortByIn.equalsIgnoreCase("DESC")) {
            sqlSortByIn = "DESC";
        }
        
        sqlCriteria += " WHERE 1 = 1";
        if(find.trim().length() != 0){
            String sqlWild = sqlWildWithUpperCase;
            sqlCriteria += " AND UPPER(" + (String)columnMap.get(findIn) + ") " + sqlWild;      
        }
        
        if(status.equalsIgnoreCase("ALL")){
        }else if(status.equalsIgnoreCase("ACTIVE")){
            sqlCriteria += " AND RECORD_STATUS = 'A'";
        }else if(status.equalsIgnoreCase("SUSPENDED")){
            sqlCriteria += " AND RECORD_STATUS = 'S'";
        }
        
        if(validAt.trim().length() != 0){
            String jdbcValidAt = RutDate.getJdbcDateStringFromDefaultDateString(validAt);
            sqlCriteria += " AND (TO_DATE('"+ jdbcValidAt +"','YYYY-MM-DD') BETWEEN VALID_FROM AND VALID_TO)";
        }
        
        if(simulation.equalsIgnoreCase("S")){
            sqlCriteria += " AND SIMULATION_FLAG = 'S'";
        }else if(simulation.equalsIgnoreCase("R")){
            sqlCriteria += " AND SIMULATION_FLAG = 'R'";            
        }
        
        if(sortBy.trim().length() != 0){
            sqlCriteria += " ORDER BY " + (String)columnMap.get(sortBy) + " " + sqlSortByIn;                   
        }
        return sqlCriteria;
    } 

    public BsaBsaModelMod findByKeyBsaModelId(String bsaModelId) throws DataAccessException{
        BsaBsaModelMod bsaModelMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select BSA_MODEL_ID ");
        sql.append("    ,MODEL_NAME ");
        sql.append("    ,SIMULATION_FLAG ");
        sql.append("    ,VALID_FROM ");        
        sql.append("    ,VALID_TO ");
        sql.append("    ,CALC_DURATION ");
        sql.append("    ,AVG_MT_TEU_WEIGHT ");
        sql.append("    ,IMBALANCE_WARNING_PERC ");        
        sql.append("    ,RECORD_STATUS ");
        sql.append("    ,RECORD_ADD_USER ");
        sql.append("    ,RECORD_ADD_DATE ");
        sql.append("    ,RECORD_CHANGE_USER ");
        sql.append("    ,RECORD_CHANGE_DATE ");
        sql.append("from VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append("where BSA_MODEL_ID = :bsaModelId ");        
        try {
            bsaModelMod = (BsaBsaModelMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("bsaModelId", bsaModelId),
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            BsaBsaModelMod bsaModelMod = new BsaBsaModelMod();
                            bsaModelMod.setBsaModelId(RutString.nullToStr(rs.getString("BSA_MODEL_ID")));
                            bsaModelMod.setModelName(RutString.nullToStr(rs.getString("MODEL_NAME")));
                            bsaModelMod.setSimulationFlag(RutString.nullToStr(rs.getString("SIMULATION_FLAG")));
                            bsaModelMod.setValidFrom(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_FROM")));
                            bsaModelMod.setValidTo(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("VALID_TO")));
                            bsaModelMod.setCalculatedDuration(RutString.nullToStr(rs.getString("CALC_DURATION")));
                            bsaModelMod.setAverageMtTeuWeight(RutString.nullToStr(rs.getString("AVG_MT_TEU_WEIGHT")));
                            bsaModelMod.setImbalanceWarningPercentage(RutString.nullToStr(rs.getString("IMBALANCE_WARNING_PERC")));
                            bsaModelMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                            bsaModelMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
                            bsaModelMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE")); 
                            bsaModelMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                            bsaModelMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));
                            return bsaModelMod;
                       }
                   });
        } catch (EmptyResultDataAccessException e) {
            bsaModelMod = null;
        }
        return bsaModelMod;
    }
    
    public boolean insert(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return insertStoreProcedure.insert(mod);
    }
    
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    
    public boolean copy(RrcStandardMod mod, int copyBsaModelId, Timestamp copyRecordChangeDate) throws DataAccessException {
        mod.setRecordAddUser(this.getUserId());
        mod.setRecordChangeUser(this.getUserId());
        return copyStoreProcedure.copy(mod, mod, copyBsaModelId, copyRecordChangeDate);
    }
    
    public boolean changePortGroupByAction(String bsaModelId, String portGroupId, String portGroupCode, String portGroupService, String action) throws DataAccessException {
        return changePortGroupStoreProcedure.allocationByPortGroup(bsaModelId, portGroupId, portGroupCode, portGroupService, this.getUserId(), action);
    }
    
    public boolean verify(String bsaModelId) throws DataAccessException{
        return verifyStoreProcedure.verify(bsaModelId, this.getUserId());
    }
    
    // ##03 BEGIN
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, List dtlModifiedMods, String copyBsaModelId, Timestamp copyRecordChangeDate) throws DataAccessException {
        BsaBsaModelMod mod = (BsaBsaModelMod) masterMod;
        BsaSupportedPortGroupMod detailMod = null;
        RcmModifiedObjectMod modifiedMod = null;
        
        // save record change date
        Timestamp masterRecChangeDate = mod.getRecordChangeDate();
        
        //begin: save master
        StringBuffer errorMsgBuffer = new StringBuffer();
        try {
            if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(masterOperationFlag)) {
                insert(masterMod);
            } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(masterOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_MODIFY.equals(masterOperationFlag)) {
                update(masterMod);
                
                if(mod.getSimulationFlag().equals("R")){
                    generateVsaAllocation(mod.getBsaModelId());
                }
            }
        } catch (CustomDataAccessException e) {
            errorMsgBuffer.append(e.getMessages()+"&");
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        //end: save master
        
        boolean isChangePortGroup = false;
        String strOperationFlag = null;
        System.out.println("[BsaBsaModelJdbcDao][saveMasterDetails] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            isChangePortGroup = false;
            mod.setRecordChangeDate(masterRecChangeDate);
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        
        //begin: save detail in master
        int index = 0;
        for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
            modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
            if (modifiedMod != null && modifiedMod.getObjectMod() instanceof BsaSupportedPortGroupMod) { //if 1
                
                try {
                    strOperationFlag = modifiedMod.getOperationFlag();
                    detailMod = (BsaSupportedPortGroupMod) modifiedMod.getObjectMod();
                    detailMod.setBsaModelId(mod.getBsaModelId());
                    
                    if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag)) {
                        isChangePortGroup = true;
                        bsaSupportedPortGroupDao.insert(detailMod);
                    } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)) {
                        isChangePortGroup = true;
                        bsaSupportedPortGroupDao.update(detailMod);
                    } else if (RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)) {
                        isChangePortGroup = true;
                        bsaSupportedPortGroupDao.delete(detailMod);
                    }
                    
                } catch (CustomDataAccessException e) {
                    index = modifiedMod.getSeqNo() + 1;
                    errorMsgBuffer.append(e.getMessages()+"%"+index+"&");    
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
                
            } //end if 1
        } //end for 1
        //end: save detail in master  
        
        if (!errorMsgBuffer.toString().equals("")) {
            mod.setRecordChangeDate(masterRecChangeDate);
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
        
        try {
            // check constraint
            this.checkConstraints(mod, dtlModifiedMods);
            
            // ##02 BEGIN
            if (isChangePortGroup) {
                // change supported port group
                this.changeSupportedPortGroup(dtlModifiedMods);
                
            }
            // ##02 END
            
            // ##04 BEGIN
            if (!RutString.isEmptyString(copyBsaModelId) && copyRecordChangeDate != null) {
                this.copy(masterMod, RutString.toInteger(copyBsaModelId), copyRecordChangeDate);
            }
            // ##04 END
            
            // verify all service variant model in BSA model id
            this.verify(mod.getBsaModelId());
            
        } catch(CustomDataAccessException e) {
            mod.setRecordChangeDate(masterRecChangeDate);
            throw e;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    // ##03 END
    
    // ##02 BEGIN
    private void changeSupportedPortGroup(List dtlModifiedMods) {
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        BsaSupportedPortGroupMod bean = null;
        RcmModifiedObjectMod modifiedMod = null;
        String strOperationFlag = null;
        
        if (dtlModifiedMods != null && dtlModifiedMods.size() > 0) { //if 1
            String plAction = null;
            String bsaModelId = null;
            String portGroupId = null;
            String portGroupCode = null;
            String portGroupService = null;
            
            int index = 0;
            for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
                modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
                if (modifiedMod != null && modifiedMod.getObjectMod() instanceof BsaSupportedPortGroupMod) { //if 2
                    strOperationFlag = modifiedMod.getOperationFlag();
                    
                    if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag) || 
                        RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag) ||
                        RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)) 
                    {
                        index++;
                        try {
                            bean = (BsaSupportedPortGroupMod) modifiedMod.getObjectMod();
                            bsaModelId = bean.getBsaModelId();
                            portGroupId = bean.getBsaSupportedPortGroupId();
                            portGroupCode = bean.getPortGroupCode();
                            portGroupService = bean.getService();
                            
                            plAction = "";
                            if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag)) {
                                plAction = RcmConstant.PL_ACTION_INSERT;
                            } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)) {
                                plAction = RcmConstant.PL_ACTION_UPDATE;
                            } else if (RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)) {
                                plAction = RcmConstant.PL_ACTION_DELETE;
                            }
                            
                            this.changePortGroupByAction(bsaModelId, portGroupId, portGroupCode, portGroupService, plAction);
                        } catch (CustomDataAccessException e) {
                            errorMsgBuffer.append(e.getMessages()+"%"+index+"&");
                        } catch (DataAccessException e) {
                            e.printStackTrace();
                        }
                    } else if (RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)) {
                        //don't do anythings in change supported port group.
                    } else {
                        index++;
                    }
                
                } //end if 2
            } //end for 1
        } //end if 1
        
        System.out.println("[BsaBsaModelJdbcDao][changeSupportedPortGroup] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
    }
    // ##02 END
    
    private void checkConstraints(RrcStandardMod masterMod, List dtlModifiedMods) {
        StringBuffer errorMsgBuffer = new StringBuffer();
        
        BsaBsaModelMod mod = (BsaBsaModelMod) masterMod;
        BsaSupportedPortGroupMod detailMod = null;
        RcmModifiedObjectMod modifiedMod = null;
        String strOperationFlag = null;
        
        for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
            modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
            if (modifiedMod != null && modifiedMod.getObjectMod() instanceof BsaSupportedPortGroupMod) { //if 1
            
                try {
                    strOperationFlag = modifiedMod.getOperationFlag();
                    detailMod = (BsaSupportedPortGroupMod) modifiedMod.getObjectMod();
                    detailMod.setBsaModelId(mod.getBsaModelId());
                    
                    if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)) {
                        bsaSupportedPortGroupDao.checkConstraint(detailMod);
                    } else {
                        //don't do anythings in checking constraints.
                    }
                    
                } catch (CustomDataAccessException e) {
                    if (errorMsgBuffer.indexOf(e.getMessages()) == -1) {
                        errorMsgBuffer.append(e.getMessages()+"&");
                    }
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
            
            } //end if 1
        } //end for 1
        
        System.out.println("[BsaBsaModelJdbcDao][checkConstraints] : errorMsgBuffer = "+errorMsgBuffer.toString());
        if (!errorMsgBuffer.toString().equals("")) {
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_BASE_ALLOCATION_MODEL.PRR_INS_BSA_MODEL";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_model_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_simulation_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_valid_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_valid_to", Types.DATE));
            declareParameter(new SqlInOutParameter("p_avg_mt_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_imbalance_warning_perc", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_calc_duration", Types.NUMERIC));            
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean insert(RrcStandardMod mod) {
            return insert(mod,mod);
        }
        
        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false; 
            if((inputMod instanceof BsaBsaModelMod)&&(outputMod instanceof BsaBsaModelMod)){
                BsaBsaModelMod aInputMod = (BsaBsaModelMod)inputMod;
                BsaBsaModelMod aOutputMod = (BsaBsaModelMod)outputMod;
                Map inParameters = new HashMap(13);
        ////
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_bsa_model_id:"+new Integer((RutString.nullToStr(aInputMod.getBsaModelId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaModelId())));
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_model_name:"+aInputMod.getModelName());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_simulation_flag:"+aInputMod.getSimulationFlag());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_avg_mt_teu_weight:"+new Double(aInputMod.getAverageMtTeuWeight()));
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_imbalance_warning_perc:"+new Double(aInputMod.getImbalanceWarningPercentage()));
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_calc_duration:"+new Long(aInputMod.getCalculatedDuration()));
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[BsaBsaModelJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
        ////
                inParameters.put("p_bsa_model_id", new Integer((RutString.nullToStr(aInputMod.getBsaModelId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaModelId())));
                inParameters.put("p_model_name", aInputMod.getModelName());
                inParameters.put("p_simulation_flag", aInputMod.getSimulationFlag());
                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                inParameters.put("p_avg_mt_teu_weight", new BigDecimal(aInputMod.getAverageMtTeuWeight()));
                inParameters.put("p_imbalance_warning_perc", new BigDecimal(aInputMod.getImbalanceWarningPercentage()));
                inParameters.put("p_calc_duration", new BigDecimal(aInputMod.getCalculatedDuration()));
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setBsaModelId((RutString.nullToStr(((Integer)outParameters.get("p_bsa_model_id")).toString())));
                    aOutputMod.setModelName((RutString.nullToStr((String)outParameters.get("p_model_name"))));
                    aOutputMod.setSimulationFlag((RutString.nullToStr((String)outParameters.get("p_simulation_flag"))));
                    aOutputMod.setValidFrom(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_from")))));
                    aOutputMod.setValidTo(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_to")))));
                    aOutputMod.setAverageMtTeuWeight((RutString.nullToStr(((BigDecimal)outParameters.get("p_avg_mt_teu_weight")).toString())));
                    aOutputMod.setImbalanceWarningPercentage((RutString.nullToStr(((BigDecimal)outParameters.get("p_imbalance_warning_perc")).toString())));
                    aOutputMod.setCalculatedDuration((RutString.nullToStr(((BigDecimal)outParameters.get("p_calc_duration")).toString())));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                    aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                }
            }
            return isSuccess;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_BASE_ALLOCATION_MODEL.PRR_UPD_BSA_MODEL";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_model_name", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_simulation_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_valid_from", Types.DATE));
            declareParameter(new SqlInOutParameter("p_valid_to", Types.DATE));
            declareParameter(new SqlInOutParameter("p_avg_mt_teu_weight", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_imbalance_warning_perc", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_calc_duration", Types.NUMERIC)); 
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean update(RrcStandardMod mod) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if((inputMod instanceof BsaBsaModelMod)&&(outputMod instanceof BsaBsaModelMod)){
                BsaBsaModelMod aInputMod = (BsaBsaModelMod)inputMod;
                BsaBsaModelMod aOutputMod = (BsaBsaModelMod)outputMod;
                Map inParameters = new HashMap(11);
            ////
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_bsa_model_id:"+aInputMod.getBsaModelId());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_model_name:"+aInputMod.getModelName());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_simulation_flag:"+aInputMod.getSimulationFlag());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_valid_from:"+aInputMod.getValidFrom());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_valid_to:"+aInputMod.getValidTo());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_avg_mt_teu_weight:"+aInputMod.getAverageMtTeuWeight());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_imbalance_warning_perc:"+aInputMod.getImbalanceWarningPercentage());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_calc_duration:"+aInputMod.getCalculatedDuration());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_record_status:"+aInputMod.getRecordStatus());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[BsaBsaModelJdbcDao][UpdateStoreProcedure][update]:p_record_change_date:"+aInputMod.getRecordChangeDate());
            ////
                inParameters.put("p_bsa_model_id", new Integer((RutString.nullToStr(aInputMod.getBsaModelId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaModelId())));
                inParameters.put("p_model_name", aInputMod.getModelName());
                inParameters.put("p_simulation_flag", aInputMod.getSimulationFlag());
                inParameters.put("p_valid_from", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidFrom()));
                inParameters.put("p_valid_to", RutDate.getJdbcDateStringFromDefaultDateString(aInputMod.getValidTo()));
                inParameters.put("p_avg_mt_teu_weight", new BigDecimal(aInputMod.getAverageMtTeuWeight()));
                inParameters.put("p_imbalance_warning_perc", new BigDecimal(aInputMod.getImbalanceWarningPercentage()));
                inParameters.put("p_calc_duration", new BigDecimal(aInputMod.getCalculatedDuration()));
                inParameters.put("p_record_status", aInputMod.getRecordStatus());
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                    aOutputMod.setBsaModelId((RutString.nullToStr(((Integer)outParameters.get("p_bsa_model_id")).toString())));
                    aOutputMod.setModelName((RutString.nullToStr((String)outParameters.get("p_model_name"))));
                    aOutputMod.setSimulationFlag((RutString.nullToStr((String)outParameters.get("p_simulation_flag"))));
                    aOutputMod.setValidFrom(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_from")))));
                    aOutputMod.setValidTo(((RutDate.getDefaultDateStringFromJdbcDate((Date)outParameters.get("p_valid_to")))));
                    aOutputMod.setAverageMtTeuWeight((RutString.nullToStr(((BigDecimal)outParameters.get("p_avg_mt_teu_weight")).toString())));
                    aOutputMod.setImbalanceWarningPercentage((RutString.nullToStr(((BigDecimal)outParameters.get("p_imbalance_warning_perc")).toString())));
                    aOutputMod.setCalculatedDuration((RutString.nullToStr(((BigDecimal)outParameters.get("p_calc_duration")).toString())));
                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                } 
            }
            return isSuccess;
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_BASE_ALLOCATION_MODEL.PRR_DEL_BSA_MODEL";

        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
            compile();
        }
        
        protected boolean delete(final RrcStandardMod inputMod) {
            boolean isSuccess = false;
            if(inputMod instanceof BsaBsaModelMod){
                BsaBsaModelMod aInputMod = (BsaBsaModelMod)inputMod;
                Map inParameters = new HashMap(3);
    ////
                System.out.println("[BsaBsaModelJdbcDao][DeleteStoreProcedure][delete]:p_bsa_model_id:"+aInputMod.getBsaModelId());
                System.out.println("[BsaBsaModelJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                System.out.println("[BsaBsaModelJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
    ////
                inParameters.put("p_bsa_model_id", new Integer((RutString.nullToStr(aInputMod.getBsaModelId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaModelId())));
                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                execute(inParameters);
                isSuccess = true;
            }
            return isSuccess;
        }
    }
    
    protected class CopyStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_BASE_ALLOCATION_MODEL.PRR_COP_BSA_MODEL";

        protected CopyStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_copy_bsa_model_id", Types.INTEGER));
            compile();
        }
        
        protected boolean copy(final RrcStandardMod inMod, RrcStandardMod outMod, int copyBsaModelId, Timestamp copyRecordChangeDate) {
            boolean isSuccess = false;
            if (inMod instanceof BsaBsaModelMod && outMod instanceof BsaBsaModelMod && copyBsaModelId != 0) {
                BsaBsaModelMod inBean = (BsaBsaModelMod) inMod;
                BsaBsaModelMod outBean = (BsaBsaModelMod) outMod;
                Map param = new HashMap(4);
                
                param.put("p_bsa_model_id", new Integer(RutString.toInteger(inBean.getBsaModelId())));
                param.put("p_record_add_user", inBean.getRecordAddUser());
                param.put("p_record_change_user", inBean.getRecordChangeUser());
                param.put("p_copy_bsa_model_id", new Integer(copyBsaModelId));
                
                System.out.println("[BsaBsaModelJdbcDao][CopyStoreProcedure][copy]: p_bsa_model_id = "+param.get("p_bsa_model_id"));
                System.out.println("[BsaBsaModelJdbcDao][CopyStoreProcedure][copy]: p_record_add_user = "+param.get("p_record_add_user"));
                System.out.println("[BsaBsaModelJdbcDao][CopyStoreProcedure][copy]: p_record_change_user = "+param.get("p_record_change_user"));
                System.out.println("[BsaBsaModelJdbcDao][CopyStoreProcedure][copy]: p_copy_bsa_model_id = "+param.get("p_copy_bsa_model_id"));
                
                Map retParam = execute(param);
                if (retParam.size() > 0) {
                    isSuccess = true;
                    outBean.setBsaModelId((RutString.nullToStr(((Integer)retParam.get("p_bsa_model_id")).toString())));
                    outBean.setRecordAddUser((RutString.nullToStr((String)retParam.get("p_record_add_user"))));
                    outBean.setRecordChangeUser((RutString.nullToStr((String)retParam.get("p_record_change_user"))));
                }
            }
            return isSuccess;
        }
    }
    
    protected class ChangePortGroupStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_BASE_ALLOCATION_MODEL.PRR_ALLOCATION_BY_CHANGE_PG";
        
        protected ChangePortGroupStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port_group_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port_group_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_group_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_action", Types.VARCHAR));
            compile();
        }
        
        protected boolean allocationByPortGroup(String bsaModelId, String portGroupId, String portGroupCode, String portGroupService, String recordChangeUser, String action) {
            boolean isSuccess = false; 
            if (!RutString.isEmptyString(bsaModelId) && !RutString.isEmptyString(recordChangeUser) && !RutString.isEmptyString(action)) {
                Map inParameters = new HashMap();
                inParameters.put("p_bsa_model_id", RutDatabase.integerToDb(bsaModelId));
                inParameters.put("p_port_group_id", RutDatabase.integerToDb(portGroupId));
                inParameters.put("p_port_group_code", RutDatabase.stringToDb(portGroupCode));
                inParameters.put("p_port_group_service", RutDatabase.stringToDb(portGroupService));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(recordChangeUser));
                inParameters.put("p_action", RutDatabase.stringToDb(action));
                
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][allocationByPortGroup]: p_bsa_model_id = "+inParameters.get("p_bsa_model_id"));
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][allocationByPortGroup]: p_port_group_id:"+inParameters.get("p_port_group_id"));
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][allocationByPortGroup]: p_port_group_code:"+inParameters.get("p_port_group_code"));
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][allocationByPortGroup]: p_port_group_service:"+inParameters.get("p_port_group_service"));
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][allocationByPortGroup]: p_record_change_user:"+inParameters.get("p_record_change_user"));
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][allocationByPortGroup]: p_action:"+inParameters.get("p_action"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }
    }
    
    protected class VerifyStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_BASE_ALLOCATION_MODEL.PRR_ALLOCATION_VERIFY";
        
        protected VerifyStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            compile();
        }
        
        protected boolean verify(String bsaModelId, String recordChangeUser) {
            boolean isSuccess = false; 
            if (!RutString.isEmptyString(bsaModelId) && !RutString.isEmptyString(recordChangeUser)) {
                Map inParameters = new HashMap();
                inParameters.put("p_bsa_model_id", RutDatabase.integerToDb(bsaModelId));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(recordChangeUser));
                
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][verify]: p_bsa_model_id = "+inParameters.get("p_bsa_model_id"));
                System.out.println("[BsaBsaModelJdbcDao][ChangePortGroupStoreProcedure][verify]: p_record_change_user:"+inParameters.get("p_record_change_user"));
                
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;
                }
            }
            return isSuccess;
        }
    }
    
    public String getModelId(String bsaModelName) {
        System.out.println("[BsaBsaModelJdbcDao][getModelId]: Started");
        
        String bsaModelId = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select BSA_MODEL_ID ");
        sql.append("from VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append("where MODEL_NAME = :bsaModelName ");
        sql.append("    and RECORD_STATUS = 'A' ");
        sql.append("    and rownum = 1 ");
        
        System.out.println("[BsaBsaModelJdbcDao][getModelId]: sql = "+sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(), Collections.singletonMap("bsaModelName", bsaModelName));
        if (rs.next()) {
            bsaModelId = RutString.nullToStr(String.valueOf(rs.getInt("BSA_MODEL_ID")).toString());
        }
        
        System.out.println("[BsaBsaModelJdbcDao][getModelId]: bsaModelName = "+bsaModelName);
        System.out.println("[BsaBsaModelJdbcDao][getModelId]: bsaModelId = "+bsaModelId);
        System.out.println("[BsaBsaModelJdbcDao][getModelId]: Finished");
        return bsaModelId;
    }
    
    // ##01 BEGIN
    public String getCalDuration(String bsaModelId) {
        System.out.println("[BsaBsaModelJdbcDao][getCalDuration]: Started");
        
        String calDuration = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select to_char(CALC_DURATION) as CALC_DURATION ");
        sql.append("from VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append("where BSA_MODEL_ID = :bsaModelId ");
        sql.append("    and rownum = 1 ");
        
        System.out.println("[BsaBsaModelJdbcDao][getCalDuration]: sql = "+sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("bsaModelId", bsaModelId ));
        if (rs.next()) {
            calDuration = rs.getString("CALC_DURATION");
        }
        
        System.out.println("[BsaBsaModelJdbcDao][getCalDuration]: bsaModelId = "+bsaModelId);
        System.out.println("[BsaBsaModelJdbcDao][getCalDuration]: calDuration = "+calDuration);
        System.out.println("[BsaBsaModelJdbcDao][getCalDuration]: Finished");
        return calDuration;
    }
    // ##01 END
    
    
     public boolean generateVsaAllocation(String bsaModelId)throws DataAccessException{
         return generateVsaAllocation.generateVsaAllocation(bsaModelId, this.getUserId());
     }
     
     protected class GenerateVsaAllocation extends StoredProcedure {
         private static final String STORED_PROCEDURE_NAME = "PCR_BSA_VSA.PRR_VSA_ALLOCATION";
              
         protected GenerateVsaAllocation(JdbcTemplate jdbcTemplate) {
                  super(jdbcTemplate, STORED_PROCEDURE_NAME);
                  declareParameter(new SqlParameter("P_BSA_MODEL_ID", Types.INTEGER));
                  declareParameter(new SqlParameter("P_USER", Types.VARCHAR));
                  
                  compile();
          }
                          
          protected boolean generateVsaAllocation(String bsaModelId, String user) {
             boolean isSuccess = false; 
                  if (!RutString.isEmptyString(bsaModelId) && !RutString.isEmptyString(user)) {
                      Map inParameters = new HashMap();
                      inParameters.put("P_BSA_MODEL_ID", RutDatabase.integerToDb(bsaModelId));
                      inParameters.put("P_USER", RutDatabase.stringToDb(user));
                      
                      
                      System.out.println("[BsaBsaModelJdbcDao][GenerateVsaAllocation]: P_BSA_MODEL_ID = "+inParameters.get("p_bsa_model_id"));
                      System.out.println("[BsaBsaModelJdbcDao][GenerateVsaAllocation]: P_USER:"+inParameters.get("P_USER"));
                      
                      Map outParameters = execute(inParameters);
                      if (outParameters.size() > 0) {
                          isSuccess = true;
                      }
                  }
                  return isSuccess;
              }
          }
}
