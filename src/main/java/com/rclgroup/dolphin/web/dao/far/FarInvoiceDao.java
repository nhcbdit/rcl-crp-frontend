package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.model.far.FarBillingStatementGenerateMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface FarInvoiceDao {
    /**
     * check valid of Invoice#
     * @param invNo     
     * @return valid of invNo
     * @throws DataAccessException
     */
    public boolean isValid(String invNo) throws DataAccessException;
    
    /**
     * check valid of Invoice#
     * @param invNo  
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return valid of bookingNo
     * @throws DataAccessException
     */
    public boolean isValidByFsc(String invNo, String line, String trade, String agent, String fsc) throws DataAccessException;
    
    /**
     * list Invoice records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
    /**
     * list Invoice records for help screen
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForHelpScreenByFsc(String find,String search,String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
     
    /**
     * list Invoice records for help screen
     * @param rowAt
     * @param rowTo
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForNewHelpScreenByFsc(int rowAt,int rowTo,String find,String search,String wild, String line, String trade, String agent, String fsc) throws DataAccessException;
    
    /**
     * Count number of Invoice records for help screen
     * @param find
     * @param search
     * @param wild
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of Invoice
     * @throws DataAccessException
     */
    public int countListForNewHelpScreenByFsc(String find, String search, String wild,String line, String trade, String agent, String fsc) throws DataAccessException;
    /**
     * list Report template for invoice no.
     * @param invNo     
     * @return List of Report template
     * @throws DataAccessException
     */
    public List listTemplateByInvNo(String invNo) throws DataAccessException;
    
    /**
     * list all Invoice records for search screen
     * @param invDateFrom,invDateTo,invNumFrom,invNumTo,remark,line,region,agent,fsc,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForSearchScreenByInvHeader(String invDateFrom,String invDateTo,String invNumFrom,String invNumTo,String remark,String status,String line,String region,String agent,String fsc,String sortBy,String sortByIn) throws DataAccessException;  
    /**
     * list all Invoice & Po records for search screen
     * @param rowAt,rowTo,invDateFrom,invDateTo,invNumFrom,invNumTo,remark,line,region,agent,fsc,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listV02ForSearchScreenByInvHeader(int rowAt,int rowTo,String invDateFrom,String invDateTo,String invNumFrom,String invNumTo,String remark,String status,String line,String region,String agent,String fsc,String sortBy,String sortByIn) throws DataAccessException;  
    /**
     * list all Invoice & Po records for search screen
     * @param invDateFrom,invDateTo,invNumFrom,invNumTo,remark,line,region,agent,fsc,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public int countListForSearchScreenByInvHeader(String invDateFrom,String invDateTo,String invNumFrom,String invNumTo,String remark,String status,String line,String region,String agent,String fsc,String sortBy,String sortByIn) throws DataAccessException;  

    /**
     * list all Invoice records for search screen 
     * @param billToParty,dateType,fromDate,toDate,service,vessel,voyage,oprt,inOutXT,blNo,bkgNo,equipNo,line,region,agent,fsc,status,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForSearchScreenByInvDetail(String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String inOutXT,String blNo,String bkgNo,String equipNo,String line,String region,String agent,String fsc,String status, String sortBy,String sortByIn) throws DataAccessException;  
    /**
     * list all Invoice & Po records for search screen
     * @param rowAt,rowTo,billToParty,dateType,fromDate,toDate,service,vessel,voyage,oprt,inOutXT,blNo,bkgNo,equipNo,line,region,agent,fsc,status,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listV02ForSearchScreenByInvDetail(int rowAt,int rowTo,String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String inOutXT,String blNo,String bkgNo,String equipNo,String line,String region,String agent,String fsc,String status, String sortBy,String sortByIn) throws DataAccessException;  
    /**
     * list all Invoice & Po records for search screen
     * @param billToParty,dateType,fromDate,toDate,service,vessel,voyage,oprt,inOutXT,blNo,bkgNo,equipNo,line,region,agent,fsc,status,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public int countListForSearchScreenByInvDetail(String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String inOutXT,String blNo,String bkgNo,String equipNo,String line,String region,String agent,String fsc,String status, String sortBy,String sortByIn) throws DataAccessException;  
    
    /**
     * list all Invoice & Po records for search screen
     * @param invDateFrom,invDateTo,invNoFrom,invNoTo,remark,port,repTemplate,line,region,agent,fsc,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForSearchScreenInvItemByInvHeader(String invDateFrom,String invDateTo,String invNoFrom,String invNoTo,String remark,String status, String inbOubXT,String port,String repTemplate,String line,String region,String agent,String fsc,String sortBy,String sortByIn) throws DataAccessException;  
    
    /**
     * list all Invoice & Po records for search screen
     * @param rowAt,rowTo,invDateFrom,invDateTo,invNoFrom,invNoTo,remark,port,repTemplate,line,region,agent,fsc,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForSearchScreenInvItemByInvHeader(int rowAt,int rowTo,String invDateFrom,String invDateTo,String invNoFrom,String invNoTo,String remark,String status, String inbOubXT,String port,String repTemplate,String line,String region,String agent,String fsc,String sortBy,String sortByIn) throws DataAccessException;  
    
    /**
     * list all Invoice & Po records for search screen
     * @param invDateFrom,invDateTo,invNoFrom,invNoTo,remark,port,repTemplate,line,region,agent,fsc,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public int countListForSearchScreenInvItemByInvHeader(String invDateFrom,String invDateTo,String invNoFrom,String invNoTo,String remark,String status, String inbOubXT,String port,String repTemplate,String line,String region,String agent,String fsc,String sortBy,String sortByIn) throws DataAccessException;  
        
    /**
     * list all Invoice & Po records for search screen 
     * @param billToParty,dateType,fromDate,toDate,service,vessel,voyage,port,inOutXt,blNo,bkgNo,equipNo,line,region,agent,fsc,repTemplate,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForSearchScreenInvItemByInvDetail(String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String blNo,String bkgNo,String equipNo,String status, String inbOubXT,String line,String region,String agent,String fsc,String repTemplate,String sortBy,String sortByIn) throws DataAccessException;  
    
    /**
     * list all Invoice & Po records for search screen 
     * @param rowAt,rowTo,billToParty,dateType,fromDate,toDate,service,vessel,voyage,port,inOutXt,blNo,bkgNo,equipNo,line,region,agent,fsc,repTemplate,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public List listForSearchScreenInvItemByInvDetail(int rowAt,int rowTo,String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String blNo,String bkgNo,String equipNo,String status, String inbOubXT,String line,String region,String agent,String fsc,String repTemplate,String sortBy,String sortByIn) throws DataAccessException;  
    
    /**
     * list all Invoice & Po records for search screen 
     * @param billToParty,dateType,fromDate,toDate,service,vessel,voyage,port,inOutXt,blNo,bkgNo,equipNo,line,region,agent,fsc,repTemplate,sortBy,sortByIn  search criterias 
     * @return list of Invoice
     * @throws DataAccessException
     */
    public int countListForSearchScreenInvItemByInvDetail(String billToParty,String dateType,String fromDate,String toDate,String service,String vessel,String voyage,String port,String blNo,String bkgNo,String equipNo,String status, String inbOubXT,String line,String region,String agent,String fsc,String repTemplate,String sortBy,String sortByIn) throws DataAccessException;  
    
    /**
     * list all Template records 
     * @param type
     * @return list of Template
     * @throws DataAccessException
     */
    public List listTemplate(String type) throws DataAccessException;  
    
    /**
     * find report file name by template code
     * @param templateCode
     * @return report name
     * @throws DataAccessException
     */
    public String findReportNameByTemplateCode(String templateCode) throws DataAccessException;
   
    /**
     * generate next bill number
     * @param template
     * @param inbOubXT
     * @return next Bill No.
     * @throws DataAccessException
     */
    public FarBillingStatementGenerateMod generateNextBillNumber(String template, String inbOubXT) throws DataAccessException;
    
    /**
     * generate billing statement then return FarBillingStatementGenerateMod. 
     * @param batchNumber
     * @param template
     * @param port
     * @param billToParty
     * @param billNo
     * @param billVersion
     * @param billDate
     * @param billDateFrom
     * @param billDateTo
     * @param attn
     * @param description
     * @param fsc
     * @param invoiceSelectedStr
     * @param recordAddUser
     * @param ibObXt
     * @param currentNumber
     * @return batch number
     * @throws DataAccessException
     */
    public FarBillingStatementGenerateMod generateBillingStatement(String batchNumber, String template, String port, String billToParty, String billNo, String billVersion, String billDate, String billDateFrom, String billDateTo, String attn, String description, String fsc, String invoiceSelectedStr, String recordAddUser, String ibObXt, int currentNumber) throws DataAccessException;
    
    /**
     * return a list of existing invoice records 
     * @param billToParty
     * @param billNo
     * @param billVersion
     * @param templateCode
     * @param inbOubXT
     * @return list of existing invoice records
     * @throws DataAccessException
     */
    public List listForExistingInvoiceItemByBillNo(String billToParty, String billNo, int billVersion, String templateCode, String inbOubXT) throws DataAccessException;  
    /**
     * list Booking records for help screen
     * @param batchNo
     * @param billNo
     * @param version
     * @param flag
     * @throws DataAccessException
     */
     
     public List listPhilips(int batchNo,String billNo,int version,int flag) throws DataAccessException;
}
