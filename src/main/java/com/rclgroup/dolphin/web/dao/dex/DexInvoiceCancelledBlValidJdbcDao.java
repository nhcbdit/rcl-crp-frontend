/*--------------------------------------------------------
DexInvoiceCancelledBlValidJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Nipun Sutes 26/04/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.dex.DexBlMod;
import com.rclgroup.dolphin.web.model.dex.DexInvoiceCancelledBlValidMod;
import com.rclgroup.dolphin.web.model.dnd.DndDndSummaryStatMod;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class DexInvoiceCancelledBlValidJdbcDao extends RrcStandardDao implements DexInvoiceCancelledBlValidDao{
    private GenerateDexInvoiceCancelledBlValidProcedure generateDexInvoiceCancelledBlValidProcedure;
    public DexInvoiceCancelledBlValidJdbcDao()throws Exception {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        generateDexInvoiceCancelledBlValidProcedure = new GenerateDexInvoiceCancelledBlValidProcedure(getJdbcTemplate());
    }
    
    public boolean generateDexInvoiceCancelledBlValid(RrcStandardMod mod) throws DataAccessException {
        return generateDexInvoiceCancelledBlValidProcedure.generate(mod);
    }
    
    protected class GenerateDexInvoiceCancelledBlValidProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_DEX_RPT.PRR_DEX_RPT123";
        
        protected GenerateDexInvoiceCancelledBlValidProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_sessionid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_userid", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ser", Types.VARCHAR));
            declareParameter(new SqlParameter("p_ves", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voy", Types.VARCHAR));
            declareParameter(new SqlParameter("p_port", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_datefrom", Types.VARCHAR));
            declareParameter(new SqlParameter("p_dateto", Types.VARCHAR));
            declareParameter(new SqlParameter("p_blstatus", Types.VARCHAR));
            compile();
        }
        
        protected boolean generate(RrcStandardMod mod) {
            return generate(mod, mod);
        }
        
        protected boolean generate(final RrcStandardMod inputMod, RrcStandardMod outputMod) {
            boolean isSuccess = false;
            if ((inputMod instanceof DexInvoiceCancelledBlValidMod) && (outputMod instanceof DexInvoiceCancelledBlValidMod)) {
                Map inParameters = new HashMap();
                DexInvoiceCancelledBlValidMod aInputMod = (DexInvoiceCancelledBlValidMod) inputMod;                
                inParameters.put("p_sessionid", aInputMod.getSessionId());
                inParameters.put("p_userid", aInputMod.getUserId());
                inParameters.put("p_ser", aInputMod.getService());
                inParameters.put("p_ves", aInputMod.getVessel());
                inParameters.put("p_voy", aInputMod.getVoyage());
                inParameters.put("p_port", aInputMod.getPort());
                inParameters.put("p_pod", aInputMod.getPod());
                inParameters.put("p_datefrom", aInputMod.getFromDateFormat());// yyyymmdd
                inParameters.put("p_dateto", aInputMod.getToDateFormat());// yyyymmdd
                inParameters.put("p_blstatus", aInputMod.getBlStatus());
                
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_session_id = "+inParameters.get("p_sessionid"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_username = "+inParameters.get("p_userid"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_ser = "+inParameters.get("p_ser"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_ves = "+inParameters.get("p_ves"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_voy = "+inParameters.get("p_voy"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_port = "+inParameters.get("p_port"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_pod = "+inParameters.get("p_pod"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_datefrom = "+inParameters.get("p_datefrom"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_dateto = "+inParameters.get("p_dateto"));
                System.out.println("[DndDemurrageAndDetetionJdbcDao][GenerateSummaryStatStoreProcedure]: p_blstatus = "+inParameters.get("p_blstatus"));
                
                Map outParameters = execute(inParameters);
//                if (outParameters.size()>0) {
                    isSuccess = true;
//                }
            }
            return isSuccess;
        }
    }
}
