/*-----------------------------------------------------------------------------------------------------------  
CamOrganizationalRegionJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 24/04/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamOrganizationalRegionMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class CamOrganizationalRegionJdbcDao extends RrcStandardDao implements CamOrganizationalRegionDao  {
    
    public CamOrganizationalRegionJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String regionCode, String status) throws DataAccessException {
        System.out.println("[CamOrganizationalRegionJdbcDao][isValid]: With status: Started");
        boolean isValid = false;            
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT LINE_CODE ");
        sql.append("      ,REGION_CODE ");
        sql.append("FROM VR_CAM_ORG_REGION ");
        sql.append("WHERE 1 = 1 ");
        if((regionCode!=null)&&(!regionCode.trim().equals(""))){
            sql.append("  AND REGION_CODE = :regionCode ");
        }
        sql.append("  AND STATUS = :status "); 
        System.out.println("[CamOrganizationalRegionDao][isValid]: SQL: " + sql.toString());
        
        HashMap map = new HashMap();
        map.put("regionCode",regionCode);
        map.put("status",status);
        System.out.println("[CamOrganizationalRegionDao][isValid]: regionCode: " + regionCode);
        System.out.println("[CamOrganizationalRegionDao][isValid]: status: " + status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[CamOrganizationalRegionDao][isValid]: isValid: " + isValid);
        System.out.println("[CamOrganizationalRegionJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException {
        System.out.println("[CamOrganizationalRegionJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild); 
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT LINE_CODE ");
        sql.append("      ,REGION_CODE ");
        sql.append("      ,REGION_NAME ");
        sql.append("      ,STATUS ");
        sql.append("FROM VR_CAM_ORG_REGION ");
        sql.append(sqlCriteria);            
        sql.append("ORDER BY LINE_CODE ");
        sql.append("        ,REGION_CODE ");
        System.out.println("[CamOrganizationalRegionJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[CamOrganizationalRegionJdbcDao][listForHelpScreen]: Finished");      
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("R")) {
                sqlCriteria = "WHERE REGION_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("N")) {
                sqlCriteria = "WHERE REGION_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "WHERE STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }    
    
    public List listForHelpScreenWithUserLevel(String find,String search,String wild,String lineCode,String status) throws DataAccessException {
        System.out.println("[CamOrganizationalRegionDao][listForHelpScreenWithUserLevel]: Started");
        String sqlCriteria = createSqlCriteriaForHelpScreenWithUserLevel(find,search,wild);
        StringBuffer sql = new StringBuffer();     
        sql.append("SELECT LINE_CODE ");
        sql.append("      ,REGION_CODE ");
        sql.append("      ,REGION_NAME ");
        sql.append("      ,STATUS ");
        sql.append("FROM VR_CAM_ORG_REGION ");
        sql.append("WHERE STATUS = :status "); 
        if((lineCode!=null)&&(!lineCode.trim().equals(""))){
            sql.append("  AND LINE_CODE = :lineCode "); 
        }
        sql.append(sqlCriteria);            
        sql.append("ORDER BY LINE_CODE ");
        sql.append("        ,REGION_CODE ");
        System.out.println("[CamOrganizationalRegionDao][listForHelpScreenWithUserLevel]: SQL: " + sql.toString());
        
        HashMap map = new HashMap();
        map.put("status",status);
        map.put("lineCode",lineCode);
        System.out.println("[CamOrganizationalRegionDao][listForHelpScreenWithUserLevel]: status: " + status);
        System.out.println("[CamOrganizationalRegionDao][listForHelpScreenWithUserLevel]: lineCode: " + lineCode);
        System.out.println("[CamOrganizationalRegionJdbcDao][listForHelpScreenWithUserLevel]: Finished"); 
        return getNamedParameterJdbcTemplate().query(
                       sql.toString(),
                       map,
                       new RowModMapper());      

    }
    
    private String createSqlCriteriaForHelpScreenWithUserLevel(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";
        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("R")) {
                sqlCriteria = "AND REGION_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("N")) {
                sqlCriteria = "AND REGION_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "AND STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }    
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private CamOrganizationalRegionMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        CamOrganizationalRegionMod region = new CamOrganizationalRegionMod();
        region.setLineCode(RutString.nullToStr(rs.getString("LINE_CODE")));
        region.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));
        region.setRegionName(RutString.nullToStr(rs.getString("REGION_NAME")));
        region.setStatus(RutString.nullToStr(rs.getString("STATUS")));
        return region;
    }    
}

