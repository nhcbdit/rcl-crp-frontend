/*------------------------------------------------------
QtnPortSurchargeDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
 Author Sopon Dee-udomvongsa 03/09/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.qtn;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface QtnPortSurchargeDao {

    /**
     * @param surchargeCode
     * @return
     * @throws DataAccessException
     */
    public boolean isValid(String surchargeCode) throws DataAccessException;

    /**
     * @param surchargeCode
     * @param status
     * @return
     * @throws DataAccessException
     */
    public boolean isValid(String surchargeCode, String status) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;
    
}
