package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamGroupMandatoryMod;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Date;
import java.util.HashMap;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class CamGroupMandatoryHelperJdbcDao {
    public CamGroupMandatoryHelperJdbcDao() {
    }
    
        
    public List<CamGroupMandatoryMod> getListGroupCode(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String groupMandatoryCode,String mandatorySet,String parameterName,String status,String sortBy,String sortIn) throws DataAccessException{
        StringBuffer sb = new StringBuffer();  
        sb.append("select GROUP_MANDATORY_CODE  ");
        sb.append(" ,PCR_CAM_SERVICE_PARAM.GET_STR_GROUP_MANDATORY_SET ( GROUP_MANDATORY_CODE ) MANDATORY_SET  ");
        sb.append(" from CAM_GROUP_MANDATORY ");
        sb.append(" where 1=1 ");
        
        if(!RutString.isEmptyString(groupMandatoryCode)){
            sb.append(" AND UPPER(GROUP_MANDATORY_CODE) LIKE '%"+groupMandatoryCode.toUpperCase()+"%' ");
        }
        
        if(!RutString.isEmptyString(mandatorySet)){
            sb.append(" AND UPPER(MANDATORY_SET) LIKE '%"+mandatorySet.toUpperCase()+"%' ");
        }
        
        if(!RutString.isEmptyString(parameterName)){
            sb.append(" AND UPPER(PARAMETER_NAME) LIKE '%"+parameterName.toUpperCase()+"%' ");
        }
        
        if(!RutString.isEmptyString(status)){
            sb.append(" AND RECORD_STATUS = '"+status+"' ");
        }
        
        if(RutString.isEmptyString(sortBy)){
            sortBy = "GROUP_MANDATORY_CODE";
        }
       
        sb.append(" GROUP BY GROUP_MANDATORY_CODE ");
        sb.append(" order by "+sortBy+" "+sortIn);
        
        System.out.println("[CamGroupMandatoryHelperJdbcDao][getList]: sql = " + sb.toString());
        
      
        return namedParameterJdbcTemplate.query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public CamGroupMandatoryMod mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamGroupMandatoryMod bean = new CamGroupMandatoryMod();
                           
                           bean.setGroupMandatoryCode(RutString.nullToStr(rs.getString("GROUP_MANDATORY_CODE")));
                           bean.setMandatorySet(RutString.nullToStr(rs.getString("MANDATORY_SET")));
                         
                           return bean;
                       }
                   });
    }
    
    public List<CamGroupMandatoryMod> getList(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String groupMandatoryCode) throws DataAccessException{
        StringBuffer sb = new StringBuffer(); 
        sb.append("select  PK_CAM_GROUP_MANDATORY_ID  ");
        sb.append(",GROUP_MANDATORY_CODE  ");
        sb.append(",MANDATORY_SET  ");
        sb.append(",PARAMETER_NAME  ");
        sb.append(",SELECT_STATE  ");
       // sb.append(",IS_SELECT_MANDATORY  ");
        sb.append(",RECORD_STATUS  ");
        sb.append(",RECORD_CHANGE_USER  ");
        sb.append(",RECORD_CHANGE_DATE  ");
        sb.append(" from CAM_GROUP_MANDATORY ");
        sb.append(" WHERE UPPER(GROUP_MANDATORY_CODE) = '"+groupMandatoryCode.toUpperCase()+"'");
        sb.append(" order by MANDATORY_SET ");
      
        System.out.println("[CamGroupMandatoryHelperJdbcDao][getList]: sql = " + sb.toString());
        
        return namedParameterJdbcTemplate.query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public CamGroupMandatoryMod mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamGroupMandatoryMod bean = new CamGroupMandatoryMod();
                           
                           bean.setPkCamGroupMandatoryId(rs.getLong("PK_CAM_GROUP_MANDATORY_ID"));
                           bean.setGroupMandatoryCode(RutString.nullToStr(rs.getString("GROUP_MANDATORY_CODE")));
                           bean.setMandatorySet(RutString.nullToStr(rs.getString("MANDATORY_SET")));
                           bean.setParameterName(RutString.nullToStr(rs.getString("PARAMETER_NAME")));
                           bean.setSelectState(RutString.nullToStr(rs.getString("SELECT_STATE")));
                           //bean.setIsUnselDisable(RutString.nullToStr(rs.getString("IS_UNSELECT_DISABLE")));
                          // bean.setIsSelMandatory(RutString.nullToStr(rs.getString("IS_SELECT_MANDATORY")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecodeChangeDateStr(RutDate.dateToString(new Date(rs.getTimestamp("RECORD_CHANGE_DATE").getTime()),"dd/MM/yyyy HH:mm"));
                                           
                           return bean;
                       }
                   });
    }
    
    public StringBuffer save(JdbcTemplate jdbcTemplate,NamedParameterJdbcTemplate namedParameterJdbcTemplate,boolean isInsert,List<CamGroupMandatoryMod> newDataList, List<CamGroupMandatoryMod> updateList,String delIds) throws Exception {
    
        System.out.println ("[CamGroupMandatoryHelperJdbcDao][save]:isInsert "+isInsert);
        String errorMsg = "";
        StringBuffer msg = new StringBuffer();
       
        try{
            if(!RutString.isEmptyString(delIds)){
                deleteManyData(jdbcTemplate,delIds);
            }
        }catch(Exception e){
            errorMsg = "\n   Error in delete data: "+e.getMessage();
        }
        
        try{
             if(isInsert){
                 if(newDataList !=null && newDataList.size()>0){
                    if(checkGroupMandatoryCodeExist(namedParameterJdbcTemplate,newDataList.get(0).getGroupMandatoryCode())){
                       msg.append("* Duplicate Group Mandatory Code");
                    }else{
                        insertData(jdbcTemplate,namedParameterJdbcTemplate,newDataList);
                    }
                    
                 }
             }else{
                 insertData(jdbcTemplate,namedParameterJdbcTemplate,newDataList);
             }

         }catch(Exception e){
            errorMsg = errorMsg +"\n  Error in insert data: "+e.getMessage();
        }  
        
        try{
            updateData(jdbcTemplate,updateList) ;          
        }catch(Exception e){
           errorMsg = errorMsg +"\n    Error in update data: "+e.getMessage();
        }      
        
        
        if(!errorMsg.equals("")){
            
            throw new Exception(errorMsg);
        }
        
        return msg;
      
    }
    
    
    public String[] deleteByGroupCodes(JdbcTemplate jdbcTemplate,NamedParameterJdbcTemplate namedParameterJdbcTemplate,String[] groupCodeArray) throws Exception{
        System.out.println ("[CamGroupMandatoryHelperJdbcDao][deleteByGroupCodes] ");
        StringBuffer errorMsg = new StringBuffer();
        StringBuffer cannotDeleteGroupCodeMsg  = new StringBuffer();
        if(groupCodeArray !=null && groupCodeArray.length>0){
            for(int i = 0 ; i < groupCodeArray.length; i++){
            
                try{
                    System.out.println ("[CamGroupMandatoryHelperJdbcDao][deleteByGroupCodes]: "+groupCodeArray[i]);
                    if(checkGroupMandatoryExistService(namedParameterJdbcTemplate,groupCodeArray[i])){
                        System.out.println ("[CamGroupMandatoryHelperJdbcDao][deleteByGroupCodes]: "+groupCodeArray[i]);
                        cannotDeleteGroupCodeMsg.append(","+groupCodeArray[i]);
                    }else{
                        try{
                                deleteData(jdbcTemplate,groupCodeArray[i]);
                        }catch(Exception e){
                             e.printStackTrace();;
                            errorMsg.append(","+groupCodeArray[i]);
                        }
                    }
                }catch(Exception e){
                    errorMsg.append(","+groupCodeArray[i]);
                }
            }
        }
        
        String[] rs = new String[2];
        rs[0] = "";
        rs[1] = "";
        
        if(!RutString.isEmptyString(cannotDeleteGroupCodeMsg.toString())){
            rs[0] = cannotDeleteGroupCodeMsg.toString().substring(1);
        }
        
        if(!RutString.isEmptyString(errorMsg.toString())){
            rs[1] = errorMsg.toString().substring(1);
        }
        
        return rs;
    }
    
    public void insertData(JdbcTemplate jdbcTemplate,NamedParameterJdbcTemplate namedParameterJdbcTemplate,List<CamGroupMandatoryMod> newDataList) throws DataAccessException{
       StringBuffer insertSql =new StringBuffer(" INSERT INTO cam_group_mandatory ");
                    insertSql.append(" ( pk_cam_group_mandatory_id ");
                    insertSql.append(", group_mandatory_code");
                    insertSql.append(", mandatory_set");
                    insertSql.append(", parameter_name");
                    insertSql.append(", SELECT_STATE");
                   // insertSql.append(", is_select_mandatory");
                    insertSql.append(", record_status");
                    insertSql.append(", record_add_user");
                    insertSql.append(", record_add_date");
                    insertSql.append(", record_change_user");
                    insertSql.append(", record_change_date");
                    insertSql.append(" ) ");
                    insertSql.append(" VALUES ( ?,?,?,?,?,?,?,SYSDATE,?,SYSDATE )");
                    
       CamGroupMandatoryMod newBean = null;
       if(newDataList !=null && newDataList.size()>0){
           for(int i = 0 ; i < newDataList.size(); i++){
               
               newBean = newDataList.get(i);
               newBean.setPkCamGroupMandatoryId(getPkId(namedParameterJdbcTemplate));
               
               final CamGroupMandatoryMod updateData=newBean;
               
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getPkCamGroupMandatoryId() : "+updateData.getPkCamGroupMandatoryId());
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getGroupMandatoryCode() : "+updateData.getGroupMandatoryCode());
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getMandatorySet() : "+updateData.getMandatorySet());
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getParameterName() : "+updateData.getParameterName());
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getSelectState : "+updateData.getSelectState());
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getRecordStatus() : "+updateData.getRecordStatus());
               System.out.println ("[CamGroupMandatoryHelperJdbcDao][insertData]: updateData.getRecordChangeUser() : "+updateData.getRecordChangeUser());
               
               jdbcTemplate.update(insertSql.toString(),new PreparedStatementSetter() {
                                                                                     public void setValues(PreparedStatement ps) throws SQLException {
                                                                                     
                                                                                       ps.setLong(1,  updateData.getPkCamGroupMandatoryId());
                                                                                       ps.setString(2, updateData.getGroupMandatoryCode());
                                                                                       ps.setString(3, updateData.getMandatorySet());
                                                                                       ps.setString(4, updateData.getParameterName());
                                                                                       ps.setString(5, updateData.getSelectState());
                                                                                    //   ps.setString(6, updateData.getIsSelMandatory());
                                                                                       ps.setString(6, updateData.getRecordStatus());
                                                                                       ps.setString(7, updateData.getRecordChangeUser());
                                                                                       ps.setString(8, updateData.getRecordChangeUser());
                                                                                     
                                                                                     }
               });
            
           }
       }
    }
    
    
    public void updateData(JdbcTemplate jdbcTemplate, List<CamGroupMandatoryMod> updateList) throws DataAccessException{
        StringBuffer updateSql =new StringBuffer(" UPDATE cam_group_mandatory ");
                     updateSql.append(" SET ");
                     updateSql.append("  mandatory_set =? ");
                     updateSql.append(", parameter_name=? ");
                     updateSql.append(", SELECT_STATE=? ");
                   //  updateSql.append(", is_unselect_disable=? ");
                   //  updateSql.append(", is_select_mandatory=? ");
                     updateSql.append(", record_status=? ");
                     updateSql.append(", record_change_user=? ");
                     updateSql.append(", record_change_date=SYSDATE ");
                     updateSql.append(" WHERE pk_cam_group_mandatory_id =? ");
                     
        CamGroupMandatoryMod updataBean = null;
        if(updateList !=null && updateList.size()>0){
            for(int i = 0 ; i < updateList.size(); i++){
                
                updataBean = updateList.get(i);
                
                final CamGroupMandatoryMod updateData=updataBean;
                
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getPkCamGroupMandatoryId() : "+updateData.getPkCamGroupMandatoryId());
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getGroupMandatoryCode() : "+updateData.getGroupMandatoryCode());
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getMandatorySet() : "+updateData.getMandatorySet());
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getParameterName() : "+updateData.getParameterName());
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getSelectState() : "+updateData.getSelectState());
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getRecordStatus() : "+updateData.getRecordStatus());
                System.out.println ("[CamGroupMandatoryHelperJdbcDao][updateData]: updateData.getRecordChangeUser() : "+updateData.getRecordChangeUser());
                
                jdbcTemplate.update(updateSql.toString(),new PreparedStatementSetter() {
                                                                                      public void setValues(PreparedStatement ps) throws SQLException {
                                                                                      
                                                                                        ps.setString(1, updateData.getMandatorySet());
                                                                                        ps.setString(2, updateData.getParameterName());
                                                                                        ps.setString(3, updateData.getSelectState());
                                                                                     //   ps.setString(4, updateData.getIsSelMandatory());
                                                                                        ps.setString(4, updateData.getRecordStatus());
                                                                                        ps.setString(5, updateData.getRecordChangeUser());
                                                                                        ps.setLong(6,  updateData.getPkCamGroupMandatoryId());
                                                                                      
                                                                                      }
                });
             
            }
        }
    }

    public void deleteManyData(JdbcTemplate jdbcTemplate,final String idsForIn) throws DataAccessException{
            System.out.println ("[CamGroupMandatoryHelperJdbcDao][deleteManyData]: idsForIn : "+idsForIn);
            StringBuffer deleteSql =new StringBuffer(" DELETE cam_group_mandatory ");
                     deleteSql.append(" WHERE pk_cam_group_mandatory_id in ( "+idsForIn+" ) ");
            
                jdbcTemplate.update(deleteSql.toString());

    }
    
    public void deleteData(JdbcTemplate jdbcTemplate,final String id) throws DataAccessException{
            System.out.println ("[CamGroupMandatoryHelperJdbcDao][deleteData]: deleteData : "+id);
            StringBuffer deleteSql =new StringBuffer(" DELETE cam_group_mandatory ");
                     deleteSql.append(" WHERE GROUP_MANDATORY_CODE =? ");
            
                jdbcTemplate.update(deleteSql.toString(),new PreparedStatementSetter() {
                                                                                      public void setValues(PreparedStatement ps) throws SQLException {
                                                                                      
                                                                                        ps.setString(1, id);
                                                                                       
                                                                                        }
                });
                
       
        
            

    
        
    }

    private long getPkId(NamedParameterJdbcTemplate namedParameterJdbcTemplate){
    	Long result = (Long) namedParameterJdbcTemplate.queryForObject("SELECT SR_CAM_GMA01.nextVal FROM dual",new HashMap(), Long.class);
        return result;
    }
    
    private boolean checkGroupMandatoryExistService(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String groupMandatoryCode){
    
    	Integer countObj = (Integer) namedParameterJdbcTemplate.queryForObject("SELECT count(GROUP_MANDATORY_CODE) FROM CAM_SERVICE_PARAMETER WHERE GROUP_MANDATORY_CODE ='"+groupMandatoryCode+"'",new HashMap(), Integer.class);

      if(countObj != null && countObj.intValue() > 0){
          return true;
      }
      
      return false;
      
    }
    
    private boolean checkGroupMandatoryCodeExist(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String groupMandatoryCode){
    
    	Integer countObj = (Integer) namedParameterJdbcTemplate.queryForObject("select count(GROUP_MANDATORY_CODE) from CAM_GROUP_MANDATORY WHERE GROUP_MANDATORY_CODE ='"+groupMandatoryCode+"'",new HashMap(), Integer.class);
        
    	 if(countObj != null && countObj.intValue() > 0){
            return true;
        }
        
        return false;
        
    }
}
