/*-----------------------------------------------------------------------------------------------------------  
EmsActivityJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 10/06/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsActivityMod;
import com.rclgroup.dolphin.web.util.RutString;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class EmsActivityJdbcDao extends RrcStandardDao implements EmsActivityDao {

    public EmsActivityJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String activityCode) throws DataAccessException {
        System.out.println("[EmsActivityJdbcDao][isValid]: Started");
        boolean isValid = false;    
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ACTIVITY_CODE ");
        sql.append("FROM VR_EMS_ACTIVITY ");
        sql.append("WHERE ACTIVITY_CODE = :activityCode");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("activityCode", activityCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[EmsActivityJdbcDao][isValid]: Finished");
        return isValid;
    }
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[EmsActivityJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ACTIVITY_CODE ");
        sql.append("      ,DESCRIPTION ");
        sql.append("      ,STATUS "); 
        sql.append("FROM VR_EMS_ACTIVITY ");
        sql.append("WHERE STATUS = 'Active'");
        sql.append(sqlCriteria);            
        System.out.println("[EmsActivityJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[EmsActivityJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND ACTIVITY_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND DESCRIPTION " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }     
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private EmsActivityMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        EmsActivityMod activityCode = new EmsActivityMod();
        activityCode.setActivityCode(RutString.nullToStr(rs.getString("ACTIVITY_CODE")));
        activityCode.setActivityName(RutString.nullToStr(rs.getString("DESCRIPTION")));
        activityCode.setActivityStatus(RutString.nullToStr(rs.getString("STATUS")));

        return activityCode;
    }

}
