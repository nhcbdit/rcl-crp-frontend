package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RriStandardDao;
import java.util.List;
import org.springframework.dao.DataAccessException;
public interface DexBillToPartyCollFscDao extends RriStandardDao{
    public List getBillToParty(String billToParty,String responseFsc,String bypassFsc,String status) throws DataAccessException;
    public boolean insertBillToParty(String billToParty,
                                         String bypassFsc,
                                         String status,
                                         String reason,
                                         String userId) throws DataAccessException;
    public List getConfigureValue(String value) throws DataAccessException;
    public int  validateBillToParty(String billToParty,String bypassFsc) throws DataAccessException;    
    public boolean updateBillToParty(String seqNo,
                                     String status,
                                     String userId) throws DataAccessException;
    public List getCustBillParty(String billToParty) throws DataAccessException;
    public List getFscDesc(String fscCode) throws DataAccessException;
}
