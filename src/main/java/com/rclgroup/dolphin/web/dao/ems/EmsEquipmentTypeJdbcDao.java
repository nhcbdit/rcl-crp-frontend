/*------------------------------------------------------
EmsEquipmentTypeJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 10/10/07  
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
01 18/10/07 MNW              Added listForHelpScreen(), moveDbToModelForHelp()
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsEquipmentTypeMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsEquipmentTypeJdbcDao extends RrcStandardDao implements EmsEquipmentTypeDao {

    public EmsEquipmentTypeJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String equipmentType) throws DataAccessException {
        System.out.println("[EmsEquipmentTypeJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT EQUIPMENT_TYPE ");
        sql.append("FROM VR_EMS_EQUIPMENT_TYPE ");
        sql.append("WHERE EQUIPMENT_TYPE = :equipmentType");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("equipmentType", equipmentType));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         
         System.out.println("[EmsEquipmentTypeJdbcDao][isValid]: Finished");
         return isValid;
    }
    
    public boolean isValid(String equipmentType, String status) throws DataAccessException {
        System.out.println("[EmsEquipmentTypeJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT EQUIPMENT_TYPE ");
        sql.append("FROM VR_EMS_EQUIPMENT_TYPE ");
        sql.append("WHERE EQUIPMENT_TYPE = :equipmentType ");
        sql.append("AND EQUIPMENT_TYPE_STATUS = :status ");
        
        HashMap map = new HashMap();
        map.put("equipmentType",equipmentType);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[EmsEquipmentTypeJdbcDao][isValid]: Finished");
        return isValid;
    }  
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[EmsEquipmentTypeJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT EQUIPMENT_TYPE ");
        sql.append("      ,EQUIPMENT_TYPE_NAME ");    
        sql.append("      ,CATEGORY_CODE ");            
        sql.append("      ,EQUIPMENT_TYPE_STATUS "); 
        sql.append("FROM VR_EMS_EQUIPMENT_TYPE ");
        sql.append(sqlCriteria); 
        System.out.println("[EmsEquipmentTypeJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[EmsEquipmentTypeJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("T")){
                sqlCriteria = "WHERE EQUIPMENT_TYPE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "WHERE EQUIPMENT_TYPE_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE EQUIPMENT_TYPE_STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }    
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException {
        System.out.println("[EmsEquipmentTypeJdbcDao][listForHelpScreen]: With status: Started");
        String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT EQUIPMENT_TYPE ");
        sql.append("      ,EQUIPMENT_TYPE_NAME ");    
        sql.append("      ,CATEGORY_CODE ");            
        sql.append("      ,EQUIPMENT_TYPE_STATUS "); 
        sql.append("FROM VR_EMS_EQUIPMENT_TYPE ");
        sql.append("WHERE EQUIPMENT_TYPE_STATUS = :status ");
        sql.append(sqlCriteria);
        System.out.println("[EmsEquipmentTypeJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
        System.out.println("[EmsEquipmentTypeJdbcDao][listForHelpScreen]: With status: Finished");
        HashMap map = new HashMap();
        map.put("status",status);
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowModMapper());        
        
    }    
    
    private String createSqlCriteriaWithStatus(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("T")){
                sqlCriteria = "AND EQUIPMENT_TYPE " + sqlWild;
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND EQUIPMENT_TYPE_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "AND EQUIPMENT_TYPE_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private EmsEquipmentTypeMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        EmsEquipmentTypeMod equipmentType = new EmsEquipmentTypeMod();
        equipmentType.setEquipmentType(RutString.nullToStr(rs.getString("EQUIPMENT_TYPE")));   
        equipmentType.setEquipmentTypeName(RutString.nullToStr(rs.getString("EQUIPMENT_TYPE_NAME")));     
        equipmentType.setCategoryCode(RutString.nullToStr(rs.getString("CATEGORY_CODE")));         
        equipmentType.setEquipmentTypeStatus(RutString.nullToStr(rs.getString("EQUIPMENT_TYPE_STATUS")));

        return equipmentType;
    }     
}