/*-----------------------------------------------------------------------------------------------------------  
CamFscPortMasterJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 19/08/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamFscPortMasterMod;
import com.rclgroup.dolphin.web.util.RutString;

public class CamFscPortMasterJdbcDao extends RrcStandardDao implements CamFscPortMasterDao {
   
    public CamFscPortMasterJdbcDao() {
    }
   
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public CamFscPortMasterMod findFscPortMasterModByFscCode(String fscCode) throws DataAccessException {
        System.out.println("[CamFscPortMasterJdbcDao][findFscPortMasterModByFscCode]: Started");
        
        CamFscPortMasterMod bean = null;
        StringBuffer sql = new StringBuffer();
        sql.append("select LINE ");
        sql.append("    ,TRADE ");
        sql.append("    ,AGENT ");
        sql.append("    ,PORT_CODE ");
        sql.append("    ,COUNTRY_CODE ");
        sql.append("    ,PORT_POINT_FLAG ");
        sql.append("    ,PORT_NAME ");
        sql.append("    ,PORT_TYPE ");
        sql.append("    ,FSC_CODE ");
        sql.append("from VR_CAM_FSC_PORT_MASTER ");
        sql.append("where FSC_CODE = :fscCode ");
        
        System.out.println("[CamFscPortMasterJdbcDao][findFscPortMasterModByFscCode]: SQL = " + sql.toString());
        try {
            bean = (CamFscPortMasterMod) getNamedParameterJdbcTemplate().queryForObject(
                sql.toString(),
                Collections.singletonMap("fscCode", fscCode),
                new RowMapper(){
                    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                        CamFscPortMasterMod tmp = new CamFscPortMasterMod();
                        tmp.setLineCode(RutString.nullToStr(rs.getString("LINE")));
                        tmp.setRegionCode(RutString.nullToStr(rs.getString("TRADE")));
                        tmp.setAgentCode(RutString.nullToStr(rs.getString("AGENT")));
                        tmp.setPointCode(RutString.nullToStr(rs.getString("PORT_CODE")));
                        tmp.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
                        tmp.setPortPointFlag(RutString.nullToStr(rs.getString("PORT_POINT_FLAG")));
                        tmp.setPortName(RutString.nullToStr(rs.getString("PORT_NAME")));
                        tmp.setPortType(RutString.nullToStr(rs.getString("PORT_TYPE")));
                        tmp.setFscCode(RutString.nullToStr(rs.getString("FSC_CODE")));
                        return tmp; 
                    }
                });
        } catch (Exception e) {
            bean = null;
        }
        System.out.println("[CamFscPortMasterJdbcDao][findFscPortMasterModByFscCode]: Finished");
        return bean;
    }    

}


