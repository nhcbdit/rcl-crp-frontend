/*-----------------------------------------------------------------------------------------------------------  
DexEquipmentDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 20/05/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface DexEquipmentDao extends RriStandardDao {

    /**
     * list equipment no records for help screen
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of equipment system validate
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;
    /**
     * check valid of Equipment#
     * @param equipmentNo     
     * @return valid of equipmentNo
     * @throws DataAccessException
     */
    public boolean isValid(String equipmentNo) throws DataAccessException;
    /**
     * list equipment no records for help screen
     * @param find
     * @param search
     * @param wild
     * @param status
     * @param line
     * @param trade
     * @param agent
     * @param fsc
     * @return list of equipment system validate
     * @throws DataAccessException
     */
    public List listForHelpScreenByFsc(String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException;
    /**
     * list equipment# records for help screen
     * @param rowAt
     * @param rowTo
     * @param find
     * @param search
     * @param wild
     * @return list of bl
     * @throws DataAccessException
     */
    public List listForNewHelpScreen(int rowAt,int rowTo,String find,String search,String wild, String status) throws DataAccessException;
    
    /**
     * Count number of equipment# records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of bl
     * @throws DataAccessException
     */
    public int countListForNewHelpScreen(String find, String search, String wild, String status) throws DataAccessException;

}
