 /*-----------------------------------------------------------------------------------------------------------  
 EmsControlZoneDao.java
 ------------------------------------------------------------------------------------------------------------- 
 Copyright RCL Public Co., Ltd. 2007 
 -------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 29/04/08 
 - Change Log ------------------------------------------------------------------------------------------------  
 ## DD/MM/YY -User-     -TaskRef-      -Short Description 
 01 29/04/08  SPD                       Change to new framework
 02 04/07/13  NIP                       fix bug ControlZone
 -----------------------------------------------------------------------------------------------------------*/  

  package com.rclgroup.dolphin.web.dao.ems;

  import java.util.List;
  import org.springframework.dao.DataAccessException;

  public interface EmsControlZoneDao {

     /**
      * check valid of controlZone
      * @param controlZone
      * @return valid of controlZone
      * @throws DataAccessException
      */
     public boolean isValid(String controlZone) throws DataAccessException;

     /**
      * check valid of controlZone with status
      * @param controlZone
      * @param status
      * @return valid of controlZone with status
      * @throws DataAccessException
      */
     public boolean isValid(String controlZone, String status) throws DataAccessException;

     /**
      * list controlZone records for help screen
      * @param find
      * @param search
      * @param wild
      * @return list of controlZone
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

     /**
      * list controlZone records for help screen with status
      * @param find
      * @param search
      * @param wild
      * @param regionCode
      * @param areaCode
      * @param status
      * @return list of controlZone with status
      * @throws DataAccessException
      */
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String status) throws DataAccessException;


    /**
     * @param fsc
     * @param countryCode
     * @return
     * @throws DataAccessException
     */
    public String getControlZoneView(String fsc,String point,String terminal) throws DataAccessException;//##02
  }
