/*-----------------------------------------------------------------------------------------------------------  
VsaVsaServiceVariantVolumePortCallJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.bsa.VsaAdditionPortCallMod;
import com.rclgroup.dolphin.web.model.bsa.VsaPortCallMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class BsaVsaServiceVariantVolumePortCallJdbcDao extends RrcStandardDao implements BsaVsaServiceVariantVolumePortCallDao {
   
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private VsaSupportedPortGroupDao vsaSupportedPortGroupDao;
    
    private BsaVsaServiceVariantVolumeAddPortCallDao vsaVsaServiceVariantVolumeAddPortCallDao;
    
    
    
    public BsaVsaServiceVariantVolumePortCallJdbcDao() {
    }
    
    public void setVsaAddPortCallDao(BsaVsaServiceVariantVolumeAddPortCallDao vsaVsaServiceVariantVolumeAddPortCallDao) {
        this.vsaVsaServiceVariantVolumeAddPortCallDao = vsaVsaServiceVariantVolumeAddPortCallDao;
    }
    
    
    
    public void setVsaSupportedPortGroupDao(VsaSupportedPortGroupDao vsaSupportedPortGroupDao) {
        this.vsaSupportedPortGroupDao = vsaSupportedPortGroupDao;
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
    }
    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_UPD_BSA_PORT_CALL";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            declareParameter(new SqlInOutParameter("p_vsa_port_call_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_vsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_ref_no", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port",Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_seq_no_from", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_direction_from", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_supported_port_group_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port_group_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_seq_no_to", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_direction_to", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_load_discharge_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_transhipment_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_wayport_trunk_indicator", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_call_level", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_slot_teu", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_slot_tons", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_slot_reefer_plugs", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_avg_coc_teu_weight", Types.NUMERIC)); 
            declareParameter(new SqlInOutParameter("p_avg_soc_teu_weight", Types.NUMERIC)); 
            declareParameter(new SqlInOutParameter("p_min_teu", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_tolelant", Types.NUMERIC));   // ADD TOLELANT
           
            compile();
        }
        
        protected boolean update(RrcStandardMod mod ) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod ) {
            boolean isSuccess = false;
            if((inputMod instanceof VsaPortCallMod)&&(outputMod instanceof VsaPortCallMod)){
                VsaPortCallMod aInputMod = (VsaPortCallMod)inputMod;
                VsaPortCallMod aOutputMod = (VsaPortCallMod)outputMod;
                Map inParameters = new HashMap();
     
                  
        
                inParameters.put("p_vsa_port_call_id", RutDatabase.integerToDb(aInputMod.getPortCallID()));
                inParameters.put("p_vsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVariantID()));
                inParameters.put("p_service", RutDatabase.stringToDb(aInputMod.getService()));
                inParameters.put("p_proforma_ref_no", RutDatabase.stringToDb(aInputMod.getProformaRefNo()));
                inParameters.put("p_port", RutDatabase.stringToDb(aInputMod.getPort()));
                inParameters.put("p_port_seq_no_from",RutDatabase.integerToDb(aInputMod.getPortSeqFrom()));
                inParameters.put("p_direction_from", RutDatabase.stringToDb(aInputMod.getDirFrom()));            
                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis().substring(0,1)) ;
//                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis()) ;
                System.out.println("Load Dis " + loadDis);               
                String tranship = RutDatabase.stringToDb(aInputMod.getTranShip().substring(0,1));
//                    String tranship = RutDatabase.stringToDb(aInputMod.getTranShip());
                System.out.println("tranship Flag " + tranship);  
                    if(aInputMod.getSupportPortGrpId().equals("")){
                        inParameters.put("p_supported_port_group_id",null);
                    }else{
                        inParameters.put("p_supported_port_group_id",RutDatabase.integerToDb(aInputMod.getSupportPortGrpId()));
                    }
  
//                inParameters.put("p_supported_port_group_id",RutDatabase.integerToDb(aInputMod.getSupportPortGrpId()));
                inParameters.put("p_port_group_code",RutDatabase.stringToDb(aInputMod.getPortGrp()));
                inParameters.put("p_port_seq_no_to",RutDatabase.integerToDb(aInputMod.getPortSeqTo()));
                inParameters.put("p_direction_to",RutDatabase.stringToDb(aInputMod.getDirTo())) ;   
                inParameters.put("p_load_discharge_flag", loadDis) ;
                inParameters.put("p_transhipment_flag",tranship);
                inParameters.put("p_wayport_trunk_indicator", RutDatabase.stringToDb(aInputMod.getTrunkInd()));
                inParameters.put("p_port_call_level", RutDatabase.stringToDb(aInputMod.getSubCode()));
//                inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getTolelant()));
               
                inParameters.put("p_slot_teu", RutDatabase.integerToDb(aInputMod.getSlotTeu()));
                inParameters.put("p_slot_tons", RutDatabase.integerToDb(aInputMod.getSlotTon()));
                inParameters.put("p_slot_reefer_plugs",RutDatabase.integerToDb(aInputMod.getSlotRef()));
                inParameters.put("p_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getAvgCoc()));
                inParameters.put("p_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getAvgSoc()));
                inParameters.put("p_min_teu",RutDatabase.integerToDb(aInputMod.getMinTeu()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getTolelant()));
                
                
                    System.out.println("[Tolelant] " +RutDatabase.bigDecimalToDb(aInputMod.getTolelant())); 
                   
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_vsa_port_call_id = "+inParameters.get("p_vsa_port_call_id"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_vsa_service_variant_id = "+inParameters.get("p_vsa_service_variant_id"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_service = "+inParameters.get("p_service"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_proforma_ref_no = "+inParameters.get("p_proforma_ref_no"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port = "+inParameters.get("p_port"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_seq_no_from = "+inParameters.get("p_port_seq_no_from"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_direction_from = "+inParameters.get("p_direction_from"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_supported_port_group_id = "+inParameters.get("p_supported_port_group_id"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_group_code = "+inParameters.get("p_port_group_code"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_seq_no_to = "+inParameters.get("p_port_seq_no_to"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_direction_to = "+inParameters.get("p_direction_to"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_load_discharge_flag = "+inParameters.get("p_load_discharge_flag"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_transhipment_flag = "+inParameters.get("p_transhipment_flag"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_wayport_trunk_indicator = "+inParameters.get("p_wayport_trunk_indicator"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_call_level = "+inParameters.get("p_port_call_level"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_tolelant = "+inParameters.get("p_tolelant")); 
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_teu = "+inParameters.get("p_slot_teu")); 
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_tons = "+inParameters.get("p_slot_tons"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_reefer_plugs = "+inParameters.get("p_slot_reefer_plugs"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_avg_coc_teu_weight = "+inParameters.get("p_avg_coc_teu_weight"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_avg_soc_teu_weight = "+inParameters.get("p_avg_soc_teu_weight"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_min_teu = "+inParameters.get("p_min_teu"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                    System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
               
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;      
                    aOutputMod.setPortCallID(RutDatabase.dbToStrInteger(outParameters, "p_vsa_port_call_id"));
                    aOutputMod.setVariantID(RutDatabase.dbToStrInteger(outParameters, "p_vsa_service_variant_id"));
                    aOutputMod.setService(RutDatabase.dbToString(outParameters, "p_service"));
                    aOutputMod.setProformaRefNo(RutDatabase.dbToString(outParameters, "p_proforma_ref_no"));
                    aOutputMod.setPort(RutDatabase.dbToString(outParameters, "p_port"));
                    aOutputMod.setPortSeqFrom(RutDatabase.dbToStrInteger(outParameters, "p_port_seq_no_from"));
                    aOutputMod.setDirFrom(RutDatabase.dbToString(outParameters, "p_direction_from"));               
                    aOutputMod.setSupportPortGrpId(RutDatabase.dbToStrInteger(outParameters, "p_supported_port_group_id"));
                    aOutputMod.setPortGrp(RutDatabase.dbToString(outParameters, "p_port_group_code"));
                    aOutputMod.setPortSeqTo(RutDatabase.dbToStrInteger(outParameters, "p_port_seq_no_to"));
                    aOutputMod.setDirTo(RutDatabase.dbToString(outParameters, "p_direction_to"));                  
                    aOutputMod.setLoadDis(RutDatabase.dbToString(outParameters, "p_load_discharge_flag"));
                    aOutputMod.setTranShip(RutDatabase.dbToString(outParameters, "p_transhipment_flag"));
                    aOutputMod.setTrunkInd(RutDatabase.dbToString(outParameters, "p_wayport_trunk_indicator"));
                    aOutputMod.setSubCode(RutDatabase.dbToString(outParameters, "p_port_call_level"));         
                    aOutputMod.setSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_slot_teu"));
                    aOutputMod.setSlotTon(RutDatabase.dbToStrInteger(outParameters, "p_slot_tons"));
                    aOutputMod.setSlotRef(RutDatabase.dbToStrInteger(outParameters, "p_slot_reefer_plugs"));
                    aOutputMod.setAvgCoc(RutDatabase.dbToStrBigDecimal(outParameters, "p_avg_coc_teu_weight"));
                    aOutputMod.setAvgSoc(RutDatabase.dbToStrBigDecimal(outParameters, "p_avg_soc_teu_weight"));
                    aOutputMod.setMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_min_teu"));               
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    aOutputMod.setTolelant(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolelant"));
                    
                } 
                }
                return isSuccess;
                      
        }
    }
    
    public String getModelId(String vsaModelName) {
        System.out.println("[VsaVsaServiceVariantJdbcDao][getModelId]: Started");
        String vsaModelId = "";
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT BSA_MODEL_ID ");
        sql.append("FROM VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append("WHERE MODEL_NAME = :vsaModelName");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("vsaModelName",vsaModelName ));
        if(rs.next()) {
            vsaModelId = RutString.nullToStr(rs.getString("BSA_MODEL_ID"));
        }
        System.out.println("modelname :"+ vsaModelName);
        System.out.println("vsaModelId"+ vsaModelId);
        System.out.println("[EmsEquipmentSysValidateJdbcDao][getModelId]: Finished");
        return vsaModelId;
}
    public List listPortCallFindByKeyVsaServiceVariantID(String vsaServiceVariantId) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_CALL.PK_BSA_PORT_CALL_ID ");
        sql.append("      ,PORT_CALL.BSA_SERVICE_VARIANT_ID ");
        /* */
        sql.append("      ,PORT_CALL.SERVICE ");
        sql.append("      ,PORT_CALL.PROFORMA_REF_NO ");
        sql.append("      ,PORT_CALL.PORT_SEQ_NO_FROM ");
        sql.append("      ,PORT_CALL.DIRECTION_FROM ");
        sql.append("      ,PORT_CALL.PORT_SEQ_NO_TO ");
        sql.append("      ,PORT_CALL.DIRECTION_TO ");
        sql.append("      ,PORT_CALL.SUPPORTED_PORT_GROUP_ID ");
        /* */
        sql.append("      ,PORT_CALL.DN_PORT ");
        sql.append("      ,PORT_CALL.DN_PORT_GROUP ");
        sql.append("      ,CASE WHEN NVL(TRIM(PORT_CALL.PORT_CALL_LEVEL),' ') = 'S' THEN 'sub' ELSE ' ' END  PORT_CALL_LEVEL_DES ");
        sql.append("      ,PORT_CALL.PORT_CALL_LEVEL PORT_CALL_LEVEL ");
        sql.append("      ,DECODE (PORT_CALL.DIRECTION , 'N','North'" + 
        "                                          , 'S','South'" + 
        "                                          ,'E' ,'East'" + 
        "                                          ,'W' ,'West'" + 
        "                                          ,'NE','North East'" + 
        "                                          ,'NW','North West'" + 
        "                                          ,'SE','South East'" + 
        "                                          ,'SW','South West'" + 
        "                                          ,'R' ,'Round'" + 
        "                                          ,' ') DIRECTION ");        
        sql.append("      ,DECODE (PORT_CALL.DN_LOAD_DISCHARGE_FLAG , 'L','Load'" + 
        "                                          , 'D','Discharge'" + 
        "                                          ,'B' ,'Both'" + 
        "                                          ,'T' ,'Transit'" + 
        "                                          ,' ') DN_LOAD_DISCHARGE_FLAG ");        
        sql.append("      ,DECODE (PORT_CALL.DN_TRANSHIPMENT_FLAG , 'Y','Yes'" + 
        "                                          , 'N','No'" + 
        "                                          ,' ') DN_TRANSHIPMENT_FLAG ");      
        sql.append("      ,PORT_CALL.DN_TRUNK_IND ");        
        sql.append("      ,PORT_CALL.SLOT_TEU ");
        sql.append("      ,PORT_CALL.SLOT_TONS ");
        sql.append("      ,PORT_CALL.SLOT_REEFER_PLUGS ");
        sql.append("      ,PORT_CALL.AVG_COC_TEU_WEIGHT ");
        sql.append("      ,PORT_CALL.AVG_SOC_TEU_WEIGHT ");
        sql.append("      ,PORT_CALL.MIN_TEU ");
        sql.append("      ,PORT_CALL.RECORD_STATUS ");
        sql.append("      ,PORT_CALL.RECORD_ADD_USER ");
        sql.append("      ,PORT_CALL.RECORD_ADD_DATE ");
        sql.append("      ,PORT_CALL.RECORD_CHANGE_USER ");
        sql.append("      ,PORT_CALL.RECORD_CHANGE_DATE ");
        sql.append("      ,PORT_CALL.TOLELANT ");
        sql.append("FROM VR_BSA_ALLOCATION_PORT_CALLS PORT_CALL ");
        sql.append("WHERE PORT_CALL.BSA_SERVICE_VARIANT_ID = :vsaServiceVariantId ");        
        sql.append("ORDER BY PORT_SEQ_NO_TO,DN_PORT ");
        HashMap map = new HashMap();
        map.put("vsaServiceVariantId",vsaServiceVariantId);
        
        System.out.println("[VsaVsaServiceVariantVolumePortCallJdbcDao][listPortCallFindByKeyVsaServiceVariantID]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           VsaPortCallMod vsaPortCallMod = new VsaPortCallMod();
                           vsaPortCallMod.setPortCallID(RutString.nullToStr(rs.getString("PK_BSA_PORT_CALL_ID")));
                           vsaPortCallMod.setVariantID(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                           vsaPortCallMod.setService(RutString.nullToStr(rs.getString("SERVICE")));//System.out.println(">>>vsaPortCallMod.getService() "+vsaPortCallMod.getService());
                           vsaPortCallMod.setProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));//System.out.println(">>>vsaPortCallMod.getProformaRefNo() "+vsaPortCallMod.getProformaRefNo());
                           vsaPortCallMod.setPort(RutString.nullToStr(rs.getString("DN_PORT")));System.out.println(">>>vsaPortCallMod.getPort() "+vsaPortCallMod.getPort());
                           vsaPortCallMod.setPortSeqFrom(RutString.nullToStr(rs.getString("PORT_SEQ_NO_FROM")));//System.out.println(">>>vsaPortCallMod.getPortSeqFrom "+vsaPortCallMod.getPortSeqFrom());
                           vsaPortCallMod.setDirFrom(RutString.nullToStr(rs.getString("DIRECTION_FROM")));//System.out.println(">>>vsaPortCallMod.getDirFrom() "+vsaPortCallMod.getDirFrom());
                           vsaPortCallMod.setSupportPortGrpId(RutString.nullToStr(rs.getString("SUPPORTED_PORT_GROUP_ID")));System.out.println(">>>vsaPortCallMod.getSupportPortGrpId() "+vsaPortCallMod.getSupportPortGrpId());                            
                           vsaPortCallMod.setPortGrp(RutString.nullToStr(rs.getString("DN_PORT_GROUP")));System.out.println(">>>vsaPortCallMod.getPortGrp() "+vsaPortCallMod.getPortGrp());
                           vsaPortCallMod.setPortSeqTo(RutString.nullToStr(rs.getString("PORT_SEQ_NO_TO")));//System.out.println(">>>vsaPortCallMod.getPortSeqTo() "+vsaPortCallMod.getPortSeqTo());
                           vsaPortCallMod.setDirTo(RutString.nullToStr(rs.getString("DIRECTION_TO")));//System.out.println(">>>vsaPortCallMod.getDirTo() "+vsaPortCallMod.getDirTo());
                           vsaPortCallMod.setSub(RutString.nullToStr(rs.getString("PORT_CALL_LEVEL_DES"))); //System.out.println(">>>vsaPortCallMod.getSub() "+vsaPortCallMod.getSub());
                           vsaPortCallMod.setLoadDis(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE_FLAG")));//System.out.println(">>>vsaPortCallMod.getLoadDis() "+vsaPortCallMod.getLoadDis());
                           vsaPortCallMod.setTranShip(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT_FLAG")));//System.out.println(">>>vsaPortCallMod.getTranShip() "+vsaPortCallMod.getTranShip());
                           vsaPortCallMod.setTrunkInd(RutString.nullToStr(rs.getString("DN_TRUNK_IND")));//System.out.println(">>>vsaPortCallMod.getTrunkInd() "+vsaPortCallMod.getTrunkInd());
                           vsaPortCallMod.setSubCode(RutString.nullToStr(rs.getString("PORT_CALL_LEVEL")));//System.out.println(">>>vsaPortCallMod.getSubCode() "+vsaPortCallMod.getSubCode());
                           vsaPortCallMod.setDir(RutString.nullToStr(rs.getString("DIRECTION")));//System.out.println(">>>vsaPortCallMod.getDir() "+vsaPortCallMod.getDir());
                           vsaPortCallMod.setSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));//System.out.println(">>>vsaPortCallMod.getSlotTeu() "+vsaPortCallMod.getSlotTeu());
                           vsaPortCallMod.setSlotTon(RutString.nullToStr(rs.getString("SLOT_TONS")));//System.out.println(">>>vsaPortCallMod.getSlotTon() "+vsaPortCallMod.getSlotTon());
                           vsaPortCallMod.setSlotRef(RutString.nullToStr(rs.getString("SLOT_REEFER_PLUGS")));// System.out.println(">>>vsaPortCallMod.getSlotRef() "+vsaPortCallMod.getSlotRef());
                           vsaPortCallMod.setAvgCoc(RutString.nullToStr(rs.getString("AVG_COC_TEU_WEIGHT")));//System.out.println(">>>vsaPortCallMod.getAvgCoc() "+vsaPortCallMod.getAvgCoc());
                           vsaPortCallMod.setAvgSoc(RutString.nullToStr(rs.getString("AVG_SOC_TEU_WEIGHT")));//System.out.println(">>>vsaPortCallMod.getAvgSoc() "+vsaPortCallMod.getAvgSoc());
                           vsaPortCallMod.setMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));//System.out.println(">>>vsaPortCallMod.getMinTeu() "+vsaPortCallMod.getMinTeu());
                           vsaPortCallMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));//System.out.println(">>>vsaPortCallMod.getRecordStatus() "+vsaPortCallMod.getRecordStatus());
                           vsaPortCallMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));//System.out.println(">>>vsaPortCallMod.getRecordAddUser() "+vsaPortCallMod.getRecordAddUser());
                           vsaPortCallMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));//System.out.println(">>>vsaPortCallMod.getRecordAddDate() "+vsaPortCallMod.getRecordAddDate());
                           vsaPortCallMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));//System.out.println(">>>vsaPortCallMod.getRecordChangeUser() "+vsaPortCallMod.getRecordChangeUser());
                           vsaPortCallMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));//System.out.println(">>>vsaPortCallMod.getRecordChangeDate() "+vsaPortCallMod.getRecordChangeDate());
                           vsaPortCallMod.setTolelant(RutString.nullToStr(rs.getString("TOLELANT")));
                           return vsaPortCallMod;
                        }
                   });
    }
    
    public List listAddPortCallFindByKeyVsaServiceVariantID(String vsaServiceVariantId) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ADD_PORT_CALL.PK_BSA_PORT_CALL_ID ");
        sql.append("      ,ADD_PORT_CALL.BSA_SERVICE_VARIANT_ID ");
        sql.append("      ,ADD_PORT_CALL.DN_PORT ");
        sql.append("      ,ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG ");    
        sql.append("      ,DECODE (ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG , 'L','Load'" + 
        "                                          , 'D','Discharge'" + 
        "                                          ,'B' ,'Both'" + 
        "                                          ,'T' ,'Transit'" + 
        "                                          ,' ') DN_LOAD_DISCHARGE ");  
        sql.append("      ,ADD_PORT_CALL.DN_TRANSHIPMENT_FLAG ");        
        sql.append("      ,DECODE (ADD_PORT_CALL.DN_TRANSHIPMENT_FLAG , 'Y','Yes'" + 
        "                                          , 'N','No'" + 
        "                                          ,' ') DN_TRANSHIPMENT ");       
        sql.append("      ,ADD_PORT_CALL.TOLELANT ");
        sql.append("      ,ADD_PORT_CALL.SLOT_TEU ");
        sql.append("      ,ADD_PORT_CALL.SLOT_TONS ");
        sql.append("      ,ADD_PORT_CALL.MIN_TEU ");
        sql.append("      ,ADD_PORT_CALL.RECORD_STATUS ");
        sql.append("      ,ADD_PORT_CALL.RECORD_ADD_USER ");
        sql.append("      ,ADD_PORT_CALL.RECORD_ADD_DATE ");
        sql.append("      ,ADD_PORT_CALL.RECORD_CHANGE_USER ");
        sql.append("      ,ADD_PORT_CALL.RECORD_CHANGE_DATE ");
        sql.append("FROM BSA_ADD_PORT_CALL ADD_PORT_CALL ");
        sql.append("WHERE ADD_PORT_CALL.BSA_SERVICE_VARIANT_ID = :vsaServiceVariantId ");        
        sql.append("ORDER BY DN_PORT ");
        HashMap map = new HashMap();
        map.put("vsaServiceVariantId",vsaServiceVariantId);
        
        System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][listAddPortCallFindByKeyVsaServiceVariantID]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           VsaAdditionPortCallMod vsaAddPortCallMod = new VsaAdditionPortCallMod();
                           vsaAddPortCallMod.setPortCallID(RutString.nullToStr(rs.getString("PK_BSA_PORT_CALL_ID")));
                           vsaAddPortCallMod.setVariantID(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                           vsaAddPortCallMod.setAddPort(RutString.nullToStr(rs.getString("DN_PORT")));System.out.println(">>>vsaPortCallMod.getPort() "+vsaAddPortCallMod.getAddPort());
//                           vsaAddPortCallMod.setAddLoadDis(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE_FLAG")));//System.out.println(">>>vsaPortCallMod.getLoadDis() "+vsaPortCallMod.getLoadDis());
                           vsaAddPortCallMod.setAddTranShipFlag(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT_FLAG")));//System.out.println(">>>vsaPortCallMod.getTranShip() "+vsaPortCallMod.getTranShip());
                           vsaAddPortCallMod.setAddLoadDisFlag(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE")));//System.out.println(">>>vsaPortCallMod.getLoadDis() "+vsaPortCallMod.getLoadDis());
//                           vsaAddPortCallMod.setAddTranShip(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT")));//System.out.println(">>>vsaPortCallMod.getTranShip() "+vsaPortCallMod.getTranShip());
                           vsaAddPortCallMod.setAddTolelant(RutString.nullToStr(rs.getString("TOLELANT")));//System.out.println(">>>vsaPortCallMod.getAvgSoc() "+vsaPortCallMod.getAvgSoc());
                           vsaAddPortCallMod.setAddSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));//System.out.println(">>>vsaPortCallMod.getSlotTeu() "+vsaPortCallMod.getSlotTeu());
                           vsaAddPortCallMod.setAddSlotTon(RutString.nullToStr(rs.getString("SLOT_TONS")));//System.out.println(">>>vsaPortCallMod.getSlotTon() "+vsaPortCallMod.getSlotTon());   
                           vsaAddPortCallMod.setAddMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));//System.out.println(">>>vsaPortCallMod.getMinTeu() "+vsaPortCallMod.getMinTeu());
                           vsaAddPortCallMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));//System.out.println(">>>vsaPortCallMod.getRecordStatus() "+vsaPortCallMod.getRecordStatus());
                           vsaAddPortCallMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));//System.out.println(">>>vsaPortCallMod.getRecordAddUser() "+vsaPortCallMod.getRecordAddUser());
                           vsaAddPortCallMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));//System.out.println(">>>vsaPortCallMod.getRecordAddDate() "+vsaPortCallMod.getRecordAddDate());
                           vsaAddPortCallMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));//System.out.println(">>>vsaPortCallMod.getRecordChangeUser() "+vsaPortCallMod.getRecordChangeUser());
                           vsaAddPortCallMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));//System.out.println(">>>vsaPortCallMod.getRecordChangeDate() "+vsaPortCallMod.getRecordChangeDate());
                          
                           return vsaAddPortCallMod;
                        }
                   });
    }
    
    public boolean insertAddPortCall(RrcStandardMod mod) throws DataAccessException {
            mod.setRecordAddUser(this.getUserId());
            mod.setRecordChangeUser(this.getUserId());
            return insertStoreProcedure.insertAddPortCall(mod);
        }
    protected class InsertStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_INS_BSA_ADD_PORT_CALL";
    
            protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                
                declareParameter(new SqlInOutParameter("p_vsa_port_call_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_vsa_service_variant_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_port",Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_load_discharge_flag", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_transhipment_flag", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_tolelant", Types.NUMERIC));   // ADD TOLELANT
                declareParameter(new SqlInOutParameter("p_slot_teu", Types.INTEGER)); 
                declareParameter(new SqlInOutParameter("p_slot_tons", Types.INTEGER)); 
                declareParameter(new SqlInOutParameter("p_min_teu", Types.INTEGER)); 
                declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
                compile();
            }
    
            protected boolean insertAddPortCall(RrcStandardMod mod) {
                return insertAddPortCall(mod,mod);
            }
    
            protected boolean insertAddPortCall(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
                boolean isSuccess = false;
               
                if((inputMod instanceof VsaAdditionPortCallMod)&&(outputMod instanceof VsaAdditionPortCallMod)){
                    VsaAdditionPortCallMod aInputMod = (VsaAdditionPortCallMod)inputMod;
                    VsaAdditionPortCallMod aOutputMod = (VsaAdditionPortCallMod)outputMod;
                    Map inParameters = new HashMap(10);
                    
                    
                    
                    inParameters.put("p_vsa_port_call_id", RutDatabase.integerToDb(aInputMod.getPortCallID()));
                    inParameters.put("p_vsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVariantID()));
                    inParameters.put("p_port", RutDatabase.stringToDb(aInputMod.getAddPort()));      
                    String loadDis = RutDatabase.stringToDb(aInputMod.getAddLoadDisFlag().substring(0,1)) ;
                    //                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis()) ;
                    System.out.println("Load Dis " + loadDis);               
                    String tranship = RutDatabase.stringToDb(aInputMod.getAddTranShipFlag().substring(0,1));
                    //                    String tranship = RutDatabase.stringToDb(aInputMod.getTranShip());
                    System.out.println("tranship Flag " + tranship);  
                      
                    inParameters.put("p_load_discharge_flag", RutDatabase.stringToDb(aInputMod.getAddLoadDisFlag()));      
                    inParameters.put("p_transhipment_flag",RutDatabase.stringToDb(aInputMod.getAddTranShipFlag()));   
            
                    inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getAddTolelant()));
                    inParameters.put("p_slot_teu", RutDatabase.integerToDb(aInputMod.getAddSlotTeu()));
                    inParameters.put("p_slot_tons", RutDatabase.integerToDb(aInputMod.getAddSlotTon()));
                    inParameters.put("p_min_teu",RutDatabase.integerToDb(aInputMod.getAddMinTeu()));
                    inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                    inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());

//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_vsa_port_group_id:"+aInputMod.getVsaSupportedPortGroupId());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_vsa_model_id:"+aInputMod.getVsaModelId());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_port_group_code:"+aInputMod.getPortGroupCode());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_port_grp_soc_coc:"+aInputMod.getPortGroupSocCoc());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_service:"+aInputMod.getService());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
//                    System.out.println("[VsaVsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
   
 
                    Map outParameters = execute(inParameters);
                    if (outParameters.size() > 0) {
                        isSuccess = true;
                        
                        aOutputMod.setPortCallID(RutDatabase.dbToStrInteger(outParameters, "p_vsa_port_call_id"));
                        aOutputMod.setVariantID(RutDatabase.dbToStrInteger(outParameters, "p_vsa_service_variant_id"));
                        aOutputMod.setAddPort(RutDatabase.dbToString(outParameters, "p_port"));
                        aOutputMod.setAddLoadDisFlag(RutDatabase.dbToString(outParameters, "p_load_discharge_flag"));
                        aOutputMod.setAddTranShipFlag(RutDatabase.dbToString(outParameters, "p_transhipment_flag"));
                        aOutputMod.setAddTolelant(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolelant"));
                        aOutputMod.setAddSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_slot_teu"));
                        aOutputMod.setAddSlotTon(RutDatabase.dbToStrInteger(outParameters, "p_slot_tons"));
                        aOutputMod.setAddMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_min_teu"));               
                        aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                        aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                        aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));   
                    }
                }
               
                return isSuccess;
            }
        }
    
    
}
