package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.tos.TosApprovalInternalPSSearchDao;

import com.rclgroup.dolphin.web.model.tos.TosApprovalInternalPSSearchMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;


/*-----------------------------------------------------------------------------------------------------------
TosApprovalInternalPSSearchJdbcDao.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Tanaphol Lhimmanee 03/10/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY       -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/


public class TosApprovalInternalPSSearchJdbcDao extends RrcStandardDao implements TosApprovalInternalPSSearchDao{
    public int intCurrPage = 0;
    private GetApprovalHeaderListProcedure getApprovalHeaderListProcedure;
    
    public void initDao() throws Exception {
        super.initDao();
        getApprovalHeaderListProcedure = new GetApprovalHeaderListProcedure(getJdbcTemplate(), new TosApprovalListMapper());
    }
    
    public TosApprovalInternalPSSearchJdbcDao(){
        
    }

        protected class GetApprovalHeaderListProcedure extends StoredProcedure{
        private static final String SQL_TOS_APPROVAL_HDR = "PKG_TOS_RCL_APPROVAL.PRC_TOS_GET_APPROVAL_LIST";
        
        protected GetApprovalHeaderListProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_TOS_APPROVAL_HDR);
                            
            declareParameter(new SqlOutParameter("P_O_V_DATA", OracleTypes.CURSOR, rowMapper));
            declareParameter(new SqlInOutParameter("P_PORT",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_TERMINAL",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_SERVICE",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_VESSEL",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_VOYAGE",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_ADT_FROM",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_ADT_TO",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_TOS_PRO_REF",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_COC_SOC",OracleTypes.VARCHAR,rowMapper));
            declareParameter(new SqlInOutParameter("P_SORT",OracleTypes.VARCHAR,rowMapper));
            
            compile();
        }
        
        protected List getApprovalList(Map mapParams){
            Map outMap = new HashMap();
            
            System.out.println("p_i_v_port: " + (String)mapParams.get("P_PORT"));
            System.out.println("p_i_v_terminal: "+ (String)mapParams.get("P_TERMINAL"));
            System.out.println("p_i_v_sort: "+ (String)mapParams.get("P_SORT"));
            System.out.println("p_coc_soc: "+ (String)mapParams.get("P_COC_SOC"));
            System.out.println("p_voyage: "+ (String)mapParams.get("P_VOYAGE"));
            System.out.println("p_tos_pro_ref: "+ (String)mapParams.get("P_TOS_PRO_REF"));
            
            System.out.println("p_adt_from: "+ (String)mapParams.get("P_ADT_FROM"));
             
            System.out.println("p_adt_to: "+ (String)mapParams.get("P_ADT_TO"));
            System.out.println("p_service: "+ (String)mapParams.get("P_SERVICE"));
             
            System.out.println("p_vessel: "+ (String)mapParams.get("P_VESSEL"));
            
            List<TosApprovalInternalPSSearchMod> returnList = new ArrayList<TosApprovalInternalPSSearchMod>();
            
            try{
                outMap = execute(mapParams);
                returnList = (List<TosApprovalInternalPSSearchMod>) outMap.get("P_O_V_DATA");
                
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            
            return returnList;
         }
        }

    private class TosApprovalListMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
            TosApprovalInternalPSSearchMod bean = new TosApprovalInternalPSSearchMod();
            bean.setPort(RutString.nullToStr(rs.getString("PORT")));
            bean.setTerminal(RutString.nullToStr(rs.getString("TERMINAL")));
            bean.setService(RutString.nullToStr(rs.getString("SERVICE")));
            bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
            bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
            bean.setPcsq(RutString.nullToStr(rs.getString("ETA_PCSQ")));
            bean.setEtaDate(RutString.nullToStr(rs.getString("ETA_DATE")));
            bean.setModuleType(RutString.nullToStr(rs.getString("MODULE_TYPE")));
            bean.setItems(RutString.nullToStr(rs.getString("WAITLIST")));
            

            return bean;
        }
    }

    public List getApprovalSearchList(String port,String terminal,String service,String vessel,String voyage,String adtFrom, String adtTo, String tosProRef, String cocSoc, String sort) throws DataAccessException{        

        Map map = new HashMap();
        map.put("P_PORT",port);
        map.put("P_TERMINAL",terminal);
        map.put("P_SERVICE",service);
        map.put("P_VESSEL",vessel);
        map.put("P_VOYAGE",voyage);
        map.put("P_ADT_FROM",adtFrom);
        map.put("P_ADT_TO",adtTo);
        map.put("P_TOS_PRO_REF",tosProRef);
        map.put("P_COC_SOC",cocSoc);
        map.put("P_SORT",sort);

        
        List resultList = getApprovalHeaderListProcedure.getApprovalList(map);     
        
//        System.out.println("resultList  :  "+resultList.size());
        
        return resultList;
    }

}
