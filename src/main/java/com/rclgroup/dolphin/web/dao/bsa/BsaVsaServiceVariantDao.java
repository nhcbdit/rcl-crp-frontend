/*-----------------------------------------------------------------------------------------------------------  
BsaVsaServiceVariantDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.bsa.BsaVsaServiceVariantMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;

import java.util.HashMap;
import java.util.List;

import java.util.Map;

import org.springframework.dao.DataAccessException;


public interface BsaVsaServiceVariantDao extends RriStandardDao {

    /**
     * list all BSA model records for search screen
     * @param searchMod all search criterias in one model
     * @return list of BSA model models(VsaVsaModelMod)
     * @throws DataAccessException
     */
    //public List listForSearchScreen(RcmSearchMod searchMod, String modelName) throws DataAccessException;
     
    public Map listForSearchScreen(RcmSearchMod searchMod, String modelName, String effectiveDate, String expiredDate) throws DataAccessException;
    
     /**
      * save a vsa service variant record with its details in BSA Service Variant
      * @param masterMod a BSA service variant model
      * @param masterOperationFlag a operation flag for a BSA service variant model
      * @param isDeletes detail models as a hashmap of BSA service variant models
      */ 
    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException;
    
    /**
     * delete a vsa service variant record
     * @param mod a BSA service variant model
     * @return whether deletion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
   
    /**
     * insert a BSA Service Variant record
     * @param mod a BSA Service Variant model as input and output
     * @return whether insertion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:                             
     *                             error message: ORA-XXXXX (un-exceptional oracle error)   
     */ 
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * update a BSA Service Variant record
     * @param mod a BSA Service Variant model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean update(RrcStandardMod mod) throws DataAccessException;
    
    /**
     * find BSA Service Variant by using key as BSA Service Variant ID 
     * @param vsaServiceVariantId BSA Service Variant ID 
     * @return BSA Service Variant model
     * @throws DataAccessException
     */
    public BsaVsaServiceVariantMod findByKeyVsaServiceVariantID(String vsaServiceVariantId) throws DataAccessException;

    /**
     * find number of vessels by Proforma ID and BSA vessel type
     * @param proformaId
     * @param vsaVesselType
     * @return
     * @throws DataAccessException
     */
    public int findNoOfVesselsByProformaIdVsaVesselType(int proformaId, String vsaVesselType) throws DataAccessException;

    /**
     * find voyage days by Service code and Proforma no.
     * @param serviceCode
     * @param proformaNo
     * @return
     * @throws DataAccessException
     */
    public int findVoyageDaysByServiceProformaNo(String serviceCode, String proformaNo) throws DataAccessException;

    /**
     * @param number
     * @return
     * @throws DataAccessException
     */
    public long calculateDurationByArrayNumber(long[] number) throws DataAccessException;
    
    /**
     * @param number
     * @return
     * @throws DataAccessException
     */
    public long calculateFrequencyByArrayNumber(long[] number) throws DataAccessException;

}
