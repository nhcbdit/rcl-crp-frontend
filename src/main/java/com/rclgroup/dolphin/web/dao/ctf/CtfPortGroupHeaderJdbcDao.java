/*-----------------------------------------------------------------------------------------------------------  
CtfPortGroupHeaderJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 25/03/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.ctf;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ctf.CtfPortGroupHeaderMod;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;


public class CtfPortGroupHeaderJdbcDao extends RrcStandardDao implements CtfPortGroupHeaderDao {
    public static final String SOC = "S";
    public static final String COC = "C";
    public static final String RECORD_STATUS_ACTIVE = "A";
    
    public CtfPortGroupHeaderJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    } 

    public String getDescription(String portGroupCode) throws DataAccessException{
        String portGroupName = null;
        CtfPortGroupHeaderMod mod = findByKeyPortGroupCodeSocCoc(portGroupCode,SOC,RECORD_STATUS_ACTIVE);
        if(mod == null){
            portGroupName = null;
        }else{
            portGroupName = mod.getDescription();
        }
        if ((portGroupName == null) || (portGroupName.trim().equals(""))){
            mod = findByKeyPortGroupCodeSocCoc(portGroupCode,COC,RECORD_STATUS_ACTIVE);
            if(mod == null){
                portGroupName = null;
            }else{
                portGroupName = mod.getDescription();
            }
        }
        
        return portGroupName;
    }
    
   public CtfPortGroupHeaderMod findByKeyPortGroupCodeSocCoc(String portGroupCode,String socCoc,String recordStatus) throws DataAccessException{
        System.out.println("[CtfPortGroupHeaderJdbcDao][findByKeyPortGroupCodeSocCoc]: Started");
        CtfPortGroupHeaderMod ctfPortGroupHeaderMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_GROUP_CODE ");
        sql.append("      ,DESCRIPTION ");
        sql.append("      ,SOC_COC ");
        sql.append("      ,COUNTRY_CODE ");
        sql.append("      ,UPDATE_TIME ");
        sql.append("      ,LINE ");
        sql.append("      ,TRADE ");
        sql.append("      ,AGENT ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_CTF_PORT_GROUP_HEADER ");
        sql.append("WHERE PORT_GROUP_CODE = :portGroupCode ");  
        sql.append("  AND SOC_COC = :socCoc ");
        sql.append("  AND RECORD_STATUS = :recordStatus ");
        
        HashMap map = new HashMap();
        map.put("portGroupCode",portGroupCode);
        map.put("socCoc",socCoc);
        map.put("recordStatus",recordStatus);
        System.out.println("[CtfPortGroupHeaderJdbcDao][findByKeyPortGroupCodeSocCoc]: sql: [" + sql.toString()+"]");
        System.out.println("[CtfPortGroupHeaderJdbcDao][findByKeyPortGroupCodeSocCoc]: portGroupCode: " + portGroupCode);
        System.out.println("[CtfPortGroupHeaderJdbcDao][findByKeyPortGroupCodeSocCoc]: socCoc: " + socCoc);
        System.out.println("[CtfPortGroupHeaderJdbcDao][findByKeyPortGroupCodeSocCoc]: recordStatus: " + recordStatus);
        System.out.println("[CtfPortGroupHeaderJdbcDao][findByKeyPortGroupCodeSocCoc]: Finished");
        try{
            ctfPortGroupHeaderMod = (CtfPortGroupHeaderMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CtfPortGroupHeaderMod ctfPortGroupHeaderMod = new CtfPortGroupHeaderMod();
                           ctfPortGroupHeaderMod.setPortGroupCode(RutString.nullToStr(rs.getString("PORT_GROUP_CODE")));   
                           ctfPortGroupHeaderMod.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));        
                           ctfPortGroupHeaderMod.setSocCoc(RutString.nullToStr(rs.getString("SOC_COC")));
                           ctfPortGroupHeaderMod.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
                           ctfPortGroupHeaderMod.setUpdateTime(RutString.nullToStr(rs.getString("UPDATE_TIME")));   
                           ctfPortGroupHeaderMod.setLine(RutString.nullToStr(rs.getString("LINE")));        
                           ctfPortGroupHeaderMod.setTrade(RutString.nullToStr(rs.getString("TRADE")));
                           ctfPortGroupHeaderMod.setAgent(RutString.nullToStr(rs.getString("AGENT")));
                           ctfPortGroupHeaderMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));   
                           ctfPortGroupHeaderMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));        
                           ctfPortGroupHeaderMod.setRecordAddDate(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("RECORD_ADD_DATE")));
                           ctfPortGroupHeaderMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           ctfPortGroupHeaderMod.setRecordChangeDate(RutDate.getDefaultDateStringFromJdbcDate(rs.getDate("RECORD_CHANGE_DATE")));  
                           return ctfPortGroupHeaderMod; 
                       }
                   });
        }catch (EmptyResultDataAccessException e) {
            ctfPortGroupHeaderMod = null;
        }
        return ctfPortGroupHeaderMod;
    }

}


