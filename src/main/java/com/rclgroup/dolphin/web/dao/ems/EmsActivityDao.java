/*-----------------------------------------------------------------------------------------------------------  
EmsActivityDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
 Author Sopon Dee-udomvongsa 10/06/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.ems;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface EmsActivityDao {

    /**
     * check valid of activity code
     * @param activityCode
     * @return valid of activity code
     * @throws DataAccessException
     */
    public boolean isValid(String activityCode) throws DataAccessException;

    /**
     * list activity records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of activity
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;
}
