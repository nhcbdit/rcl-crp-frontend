 /*------------------------------------------------------
 VmsPerformaAgencyJdbcDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2010
 --------------------------------------------------------
 Author Sopon Dee-udomvongsa 02/08/2010    
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.dao.vms;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.vms.VmsPerformaAgencyMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class VmsPerformaAgencyJdbcDao extends RrcStandardDao implements VmsPerformaAgencyDao {
     
     public VmsPerformaAgencyJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String performaNo) throws DataAccessException {
        System.out.println("[VmsPerformaAgencyJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        
        sql.append("SELECT PRFNO ");
        sql.append("FROM VR_PRF_COM_FRE ");
        sql.append("WHERE PRFNO = :performaNo");        
        
         HashMap map = new HashMap();
         map.put("performaNo",performaNo);
         
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map );
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         
         System.out.println("[VmsPerformaAgencyJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("P")){
                 sqlCriteria = "AND PRFNO " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }    
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[VmsPerformaAgencyJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         VmsPerformaAgencyMod performaMod = null;
         List listPerforma = new ArrayList ();
         sql.append("SELECT PRFNO ");
         sql.append("      ,STATUS ");
         sql.append("FROM VR_PRF_COM_FRE ");
         sql.append("WHERE 1 = 1 ");
//         if((fsc!=null)&&(!fsc.trim().equals(""))){
//             sql.append("  AND FSC = :fsc "); 
//         } 
         sql.append(sqlCriteria);
         System.out.println("[VmsPerformaAgencyJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         System.out.println("[VmsPerformaAgencyJdbcDao][listForHelpScreen]: With status: Finished");
         HashMap map = new HashMap();
//         map.put("fsc",fsc);
         SqlRowSet rs =  getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);         
         while (rs.next()){
             performaMod = new VmsPerformaAgencyMod();
             performaMod.setPerformaNo(RutString.nullToStr(rs.getString("PRFNO")));
             performaMod.setStatus(RutString.nullToStr(rs.getString("STATUS")));
             listPerforma.add(performaMod);
         }
         return listPerforma;
     }
       
 }
