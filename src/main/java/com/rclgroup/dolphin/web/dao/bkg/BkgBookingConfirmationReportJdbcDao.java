/*--------------------------------------------------------
BkgBookingConfirmationReportJdbcDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
--------------------------------------------------------
Author Leena Babu 29/04/2011
- Change Log--------------------------------------------
## DD/MM/YY -User- -TaskRef- -ShortDescription
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.bkg;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.dao.dex.DexManifestBLPrintingJdbcDao;

import com.rclgroup.dolphin.web.util.RutDatabase;

import com.rclgroup.dolphin.web.util.RutDate;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class BkgBookingConfirmationReportJdbcDao extends RrcStandardDao implements BkgBookingConfirmationReportDao {
    private InsertStoreProcedure insertStoreProcedure;
    public BkgBookingConfirmationReportJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
    }
    /**
     * insert the report data into temporary tables for the given search criteria
     * @param service
     * @param vessel
     * @param voyage
     * @param direction
     * @param bookingNumber
     * @param mtPickupDepot
     * @param mtDate
     * @param pol
     * @param pod
     * @param showRoutingDetails
     * @param showFreightCharges
     * @param showCommodityDetails
     * @param showMTPickupInfo
     * @param showPrintRemarks
     * @param userCode
     * @param sessionId
     * @return void
     * */
    public String insertDataIntoTempTables(String service, String vessel, 
                                         String voyage, String direction, 
                                         String bookingNumber, String mtDepot, 
                                         String mtDate, String pol, String pod, 
                                         String showRoutDetails, 
                                         String showFreightCharges, 
                                         String showCommodityDetails, 
                                         String showMTPickupInfo, 
                                         String printRemarks, 
                                         String userCode,
                                         String sessionId) {
        return insertStoreProcedure.insert(   service,  vessel, voyage,  direction, 
                                          bookingNumber,  mtDepot, mtDate,  pol,
                                          pod, showRoutDetails, showFreightCharges, 
                                          showCommodityDetails, showMTPickupInfo, 
                                          printRemarks,userCode, sessionId);                          
    }

    protected class InsertStoreProcedure extends StoredProcedure {
    private static final String STORED_PROCEDURE_NAME = "PCR_BKG_CONFIRMATION_RPT.PRR_INS_BKG_118_RPT_DATA";
    protected  InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            System.out.println("BkgBookingConfirmationReportJdbcDao: InsertStoreProcedure declare parameter begin");
            declareParameter(new SqlParameter("p_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlParameter("p_vessel", Types.VARCHAR));
            declareParameter(new SqlParameter("p_voyage", Types.VARCHAR));
            declareParameter(new SqlParameter("p_direction", Types.VARCHAR));
            declareParameter(new SqlParameter("p_booking_number", Types.VARCHAR));
            declareParameter(new SqlParameter("p_mt_terminal", Types.VARCHAR));
            declareParameter(new SqlParameter("p_mt_date", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pol", Types.VARCHAR));
            declareParameter(new SqlParameter("p_pod", Types.VARCHAR));
            declareParameter(new SqlParameter("p_show_rout", Types.VARCHAR));
            declareParameter(new SqlParameter("p_show_freight_charges", Types.VARCHAR));
            declareParameter(new SqlParameter("p_show_commodity", Types.VARCHAR));
            declareParameter(new SqlParameter("p_show_mt_pickup", Types.VARCHAR));
            declareParameter(new SqlParameter("p_show_print", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_session_id", Types.VARCHAR));
            
            compile();
            System.out.println("BkgBookingConfirmationReportJdbcDao InsertStoreProcedure declare parameter end");
        }

    protected String insert(String service, String vessel, 
                                         String voyage, String direction, 
                                         String bookingNumber, String mtDepot, 
                                         String mtDate, String pol, String pod, 
                                         String showRoutDetails, 
                                         String showFreightCharges, 
                                         String showCommodityDetails, 
                                         String showMTPickupInfo, 
                                         String printRemarks, 
                                         String userCode,
                                         String sessionId) {
        return insertPro(  service,  vessel, voyage,  direction, 
                                          bookingNumber,  mtDepot, mtDate,  pol,
                                          pod, showRoutDetails, showFreightCharges, 
                                          showCommodityDetails, showMTPickupInfo, 
                                          printRemarks, userCode, sessionId  );
    }
    protected String insertPro(String service, String vessel, 
                                         String voyage, String direction, 
                                         String bookingNumber, String mtDepot, 
                                         String mtDate, String pol, String pod, 
                                         String showRoutDetails, 
                                         String showFreightCharges, 
                                         String showCommodityDetails, 
                                         String showMTPickupInfo, 
                                         String printRemarks, 
                                         String userCode,
                                         String sessionId) {
            System.out.println("BkgBookingConfirmationReportJdbcDao InsertStoreProcedure assign value to  parameter begin");
            
            Map inParameters = new HashMap();
            inParameters.put("p_user",RutDatabase.stringToDb(userCode));
            inParameters.put("p_service",RutDatabase.stringToDb(service));
            inParameters.put("p_vessel",RutDatabase.stringToDb(vessel));
            inParameters.put("p_voyage",RutDatabase.stringToDb(voyage));
            inParameters.put("p_direction",RutDatabase.stringToDb(direction));
            inParameters.put("p_booking_number",RutDatabase.stringToDb(bookingNumber));
            inParameters.put("p_mt_terminal",RutDatabase.stringToDb(mtDepot));
            inParameters.put("p_mt_date",RutDatabase.stringToDb(RutDate.dateToStr(mtDate)));
            inParameters.put("p_pol",RutDatabase.stringToDb(pol));
            inParameters.put("p_pod",RutDatabase.stringToDb(pod));
            inParameters.put("p_show_rout",RutDatabase.stringToDb(showRoutDetails));             
            inParameters.put("p_show_freight_charges",RutDatabase.stringToDb(showFreightCharges));      
            inParameters.put("p_show_commodity",RutDatabase.stringToDb(showCommodityDetails));
            inParameters.put("p_show_mt_pickup",RutDatabase.stringToDb(showMTPickupInfo));
            inParameters.put("p_show_print",RutDatabase.stringToDb(printRemarks));
            inParameters.put("p_session_id",RutDatabase.stringToDb(sessionId));
            
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_user = "+inParameters.get("p_user"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_service = "+inParameters.get("p_service"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_vessel = "+inParameters.get("p_vessel"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_voyage = "+inParameters.get("p_voyage"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_direction = "+inParameters.get("p_direction"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_booking_number = "+inParameters.get("p_booking_number"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_mt_terminal = "+inParameters.get("p_mt_terminal"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_mt_date = "+inParameters.get("p_mt_date"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_pol = "+inParameters.get("p_pol"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_pod = "+inParameters.get("p_pod"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_show_rout = "+inParameters.get("p_show_rout"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_show_freight_charges = "+inParameters.get("p_show_freight_charges"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_show_commodity = "+inParameters.get("p_show_commodity"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_show_mt_pickup = "+inParameters.get("p_show_mt_pickup"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_show_print = "+inParameters.get("p_show_print"));
            System.out.println("[BkgBookingListJdbcDao][generateTempTable]: p_session_id = "+inParameters.get("p_session_id"));
            
            System.out.println();
            
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
               sessionId = RutDatabase.dbToString(outParameters, "p_session_id");
            }
            return sessionId;
        }          
    } 
}
