/*-----------------------------------------------------------------------------------------------------------  
EmsControlZoneJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sopon Dee-udomvongsa 29/04/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 04/07/13  NIP                       fix bug ControlZone
-----------------------------------------------------------------------------------------------------------*/

 package com.rclgroup.dolphin.web.dao.ems;


import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.ems.EmsControlZoneMod;
import com.rclgroup.dolphin.web.util.RutString;

 import java.sql.ResultSet;
 import java.sql.SQLException;


import java.util.HashMap;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class EmsControlZoneJdbcDao extends RrcStandardDao implements EmsControlZoneDao {
     
     public EmsControlZoneJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String controlZone) throws DataAccessException {
         System.out.println("[EmsControlZoneJdbcDao][isValid]: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT CONTROL_ZONE_CODE ");
         sql.append("FROM VR_EMS_CONTROL_ZONE ");
         sql.append("WHERE CONTROL_ZONE_CODE = :controlZone ");
         HashMap map = new HashMap();
         map.put("controlZone",controlZone);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }         
         System.out.println("[EmsControlZoneJdbcDao][isValid]: Finished");
         return isValid;
     }
     
     public boolean isValid(String controlZone, String status) throws DataAccessException {
         System.out.println("[EmsControlZoneJdbcDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT CONTROL_ZONE_CODE ");
         sql.append("FROM VR_EMS_CONTROL_ZONE ");
         sql.append("WHERE CONTROL_ZONE_CODE = :controlZone ");
         sql.append("AND CONTROL_ZONE_STATUS = :status ");
         HashMap map = new HashMap();
         map.put("controlZone",controlZone);
         map.put("status",status);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }         
         System.out.println("[EmsControlZoneJdbcDao][isValid]: With status: Finished");
         return isValid;
     }     
     
     public String getControlZoneView(String fsc,String point,String terminal) throws DataAccessException {//##01
         String controlZoneView = null;
         StringBuffer sql = new StringBuffer();
         sql.append("select distinct CONTROLZONE_CODE ");
         sql.append("from rclapps.VR_SYS_GEO ");
         sql.append("WHERE 1=1 ");
         
         HashMap map = new HashMap();
         if(fsc != null && !fsc.equals("")){
            map.put("fsc",fsc.toUpperCase());
            sql.append("and FSC_CODE = :fsc ");
         }   
         if(point != null && !point.equals("")){
            map.put("point",point.toUpperCase());
            sql.append("and POINT_CODE = :point ");
         } 
         if(terminal != null && !terminal.equals("")){
            map.put("terminal",terminal.toUpperCase());
            sql.append("and TERMINAL_CODE = :terminal ");
         } 
         
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             controlZoneView=RutString.nullToStr(rs.getString("CONTROLZONE_CODE"));
         } 
         return controlZoneView;
     }  
          
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT CONTROL_ZONE_CODE ");
         sql.append("      ,CONTROL_ZONE_DESCRIPTION ");
         sql.append("      ,AREA_CODE ");             
         sql.append("      ,CONTROL_ZONE_STATUS "); 
         sql.append("FROM VR_EMS_CONTROL_ZONE ");
         sql.append(sqlCriteria);
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowModMapper());         
     }
     
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "WHERE CONTROL_ZONE_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "WHERE CONTROL_ZONE_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("A")){
                 sqlCriteria = "WHERE AREA_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "WHERE CONTROL_ZONE_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }    
     
     public List listForHelpScreen(String find, String search, String wild, String regionCode, String areaCode, String status) throws DataAccessException {
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT DISTINCT CONTROL_ZONE_CODE ");
         sql.append("      ,CONTROL_ZONE_DESCRIPTION ");
         sql.append("      ,REGION_CODE ");                          
         sql.append("      ,AREA_CODE ");             
         sql.append("      ,CONTROL_ZONE_STATUS "); 
         sql.append("FROM VR_EMS_ALL_GEO ");
         sql.append("WHERE CONTROL_ZONE_STATUS = :status ");
         if((regionCode!=null)&&(!regionCode.trim().equals(""))){
             sql.append("  AND REGION_CODE = :regionCode "); 
         }
         if((areaCode!=null)&&(!areaCode.trim().equals(""))){
             sql.append("  AND AREA_CODE = :areaCode "); 
         }             
         sql.append(sqlCriteria);
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         
         HashMap map = new HashMap();
         map.put("regionCode",regionCode);
         map.put("areaCode",areaCode);
         map.put("status",status);
         System.out.println("[EmsControlZoneJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowModMapper());
     }
     
     private String createSqlCriteriaWithStatus(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND CONTROL_ZONE_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "AND CONTROL_ZONE_DESCRIPTION " + sqlWild;
             }else if(search.equalsIgnoreCase("A")){
                 sqlCriteria = "AND AREA_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND CONTROL_ZONE_STATUS " + sqlWild;
             }
         }
         return sqlCriteria;
     }     
     
     protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModelForHelp(rs);
         }
     }
     
     private EmsControlZoneMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
         EmsControlZoneMod controlZone = new EmsControlZoneMod();
         controlZone.setControlZoneCode(RutString.nullToStr(rs.getString("CONTROL_ZONE_CODE")));
         controlZone.setControlZoneName(RutString.nullToStr(rs.getString("CONTROL_ZONE_DESCRIPTION")));
         controlZone.setRegionCode(RutString.nullToStr(rs.getString("REGION_CODE")));
         controlZone.setAreaCode(RutString.nullToStr(rs.getString("AREA_CODE")));
         controlZone.setControlZoneStatus(RutString.nullToStr(rs.getString("CONTROL_ZONE_STATUS")));

         return controlZone;
     }     
 }
