/*-----------------------------------------------------------------------------------------------------------  
BsaBsaServiceVariantVolumePortCallJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 12/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;


import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaAdditionPortCallMod;
import com.rclgroup.dolphin.web.model.bsa.BsaPortCallMod;
import com.rclgroup.dolphin.web.model.rcm.RcmModifiedObjectMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.text.DecimalFormat;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class BsaBsaServiceVariantVolumePortCallJdbcDao extends RrcStandardDao implements BsaBsaServiceVariantVolumePortCallDao {

//    private DeleteStoreProcedure deleteStoreProcedure;
//    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
//    private UpdateStoreProcedureAddPortCall updateStoreProcedureAddPortCall;
//    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;
    
    private BsaBsaServiceVariantVolumePortCallDao bsaAddPortCallDao;
   
    public BsaBsaServiceVariantVolumePortCallJdbcDao() {
    }
   
    protected void initDao() throws Exception {
        super.initDao();
//        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
//        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
//        updateStoreProcedureAddPortCall = new UpdateStoreProcedureAddPortCall(getJdbcTemplate());       
//        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
    }
    
    
    public void setBsaBsaServiceVariantVolumePortCallDao(BsaBsaServiceVariantVolumePortCallDao bsaAddPortCallDao) {
        this.bsaAddPortCallDao = bsaAddPortCallDao;
    }
    

    public boolean update(RrcStandardMod mod) throws DataAccessException{
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedure.update(mod);
    }

   /* For Addition Port Call */
   /*
    public boolean insertAddPortCall(RrcStandardMod mod) throws DataAccessException {
            mod.setRecordAddUser(this.getUserId());
            mod.setRecordChangeUser(this.getUserId());
            return insertStoreProcedure.insertAddPortCall(mod);
        }
           
        
    public boolean updateAddPortCall(RrcStandardMod mod) throws DataAccessException {
        mod.setRecordChangeUser(this.getUserId());
        return updateStoreProcedureAddPortCall.updateAddPortCall(mod);
    }

    public boolean delete(RrcStandardMod mod) throws DataAccessException{
        return deleteStoreProcedure.delete(mod);
    }
    */
  
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_UPD_BSA_PORT_CALL";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            declareParameter(new SqlInOutParameter("p_bsa_port_call_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_proforma_ref_no", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port",Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_seq_no_from", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_direction_from", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_supported_port_group_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port_group_code", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_seq_no_to", Types.NUMERIC));
            declareParameter(new SqlInOutParameter("p_direction_to", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_load_discharge_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_transhipment_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_wayport_trunk_indicator", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_port_call_level", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_slot_teu", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_slot_tons", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_slot_reefer_plugs", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_avg_coc_teu_weight", Types.NUMERIC)); 
            declareParameter(new SqlInOutParameter("p_avg_soc_teu_weight", Types.NUMERIC)); 
            declareParameter(new SqlInOutParameter("p_min_teu", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
            declareParameter(new SqlInOutParameter("p_tolelant", Types.NUMERIC));   // ADD TOLELANT
           
            compile();
        }
        
        protected boolean update(RrcStandardMod mod ) {
            return update(mod,mod);
        }
        
        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod ) {
            boolean isSuccess = false;
            if((inputMod instanceof BsaPortCallMod)&&(outputMod instanceof BsaPortCallMod)){
                BsaPortCallMod aInputMod = (BsaPortCallMod)inputMod;
                BsaPortCallMod aOutputMod = (BsaPortCallMod)outputMod;
                Map inParameters = new HashMap();
     
                  
        
                inParameters.put("p_bsa_port_call_id", RutDatabase.integerToDb(aInputMod.getPortCallID()));
                inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVariantID()));
                inParameters.put("p_service", RutDatabase.stringToDb(aInputMod.getService()));
                inParameters.put("p_proforma_ref_no", RutDatabase.stringToDb(aInputMod.getProformaRefNo()));
                inParameters.put("p_port", RutDatabase.stringToDb(aInputMod.getPort()));
                inParameters.put("p_port_seq_no_from",RutDatabase.integerToDb(aInputMod.getPortSeqFrom()));
                inParameters.put("p_direction_from", RutDatabase.stringToDb(aInputMod.getDirFrom()));            
                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis().substring(0,1)) ;
//                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis()) ;
                System.out.println("Load Dis " + loadDis);               
                String tranship = RutDatabase.stringToDb(aInputMod.getTranShip().substring(0,1));
//                    String tranship = RutDatabase.stringToDb(aInputMod.getTranShip());
                System.out.println("tranship Flag " + tranship);  
                    if(aInputMod.getSupportPortGrpId().equals("")){
                        inParameters.put("p_supported_port_group_id",null);
                    }else{
                        inParameters.put("p_supported_port_group_id",RutDatabase.integerToDb(aInputMod.getSupportPortGrpId()));
                    }
  
//                inParameters.put("p_supported_port_group_id",RutDatabase.integerToDb(aInputMod.getSupportPortGrpId()));
                inParameters.put("p_port_group_code",RutDatabase.stringToDb(aInputMod.getPortGrp()));
                inParameters.put("p_port_seq_no_to",RutDatabase.integerToDb(aInputMod.getPortSeqTo()));
                inParameters.put("p_direction_to",RutDatabase.stringToDb(aInputMod.getDirTo())) ;   
                inParameters.put("p_load_discharge_flag", loadDis) ;
                inParameters.put("p_transhipment_flag",tranship);
                inParameters.put("p_wayport_trunk_indicator", RutDatabase.stringToDb(aInputMod.getTrunkInd()));
                inParameters.put("p_port_call_level", RutDatabase.stringToDb(aInputMod.getSubCode()));
//                inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getTolelant()));
               
                inParameters.put("p_slot_teu", RutDatabase.integerToDb(aInputMod.getSlotTeu()));
                inParameters.put("p_slot_tons", RutDatabase.integerToDb(aInputMod.getSlotTon()));
                inParameters.put("p_slot_reefer_plugs",RutDatabase.integerToDb(aInputMod.getSlotRef()));
                inParameters.put("p_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getAvgCoc()));
                inParameters.put("p_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getAvgSoc()));
                inParameters.put("p_min_teu",RutDatabase.integerToDb(aInputMod.getMinTeu()));
                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getTolelant()));
                
                
                    System.out.println("[Tolelant] " +RutDatabase.bigDecimalToDb(aInputMod.getTolelant())); 
                   
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_bsa_port_call_id = "+inParameters.get("p_bsa_port_call_id"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_service = "+inParameters.get("p_service"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_proforma_ref_no = "+inParameters.get("p_proforma_ref_no"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port = "+inParameters.get("p_port"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_seq_no_from = "+inParameters.get("p_port_seq_no_from"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_direction_from = "+inParameters.get("p_direction_from"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_supported_port_group_id = "+inParameters.get("p_supported_port_group_id"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_group_code = "+inParameters.get("p_port_group_code"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_seq_no_to = "+inParameters.get("p_port_seq_no_to"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_direction_to = "+inParameters.get("p_direction_to"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_load_discharge_flag = "+inParameters.get("p_load_discharge_flag"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_transhipment_flag = "+inParameters.get("p_transhipment_flag"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_wayport_trunk_indicator = "+inParameters.get("p_wayport_trunk_indicator"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_call_level = "+inParameters.get("p_port_call_level"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_tolelant = "+inParameters.get("p_tolelant")); 
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_teu = "+inParameters.get("p_slot_teu")); 
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_tons = "+inParameters.get("p_slot_tons"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_reefer_plugs = "+inParameters.get("p_slot_reefer_plugs"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_avg_coc_teu_weight = "+inParameters.get("p_avg_coc_teu_weight"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_avg_soc_teu_weight = "+inParameters.get("p_avg_soc_teu_weight"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_min_teu = "+inParameters.get("p_min_teu"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
               
                Map outParameters = execute(inParameters);
                if (outParameters.size() > 0) {
                    isSuccess = true;      
                    aOutputMod.setPortCallID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_port_call_id"));
                    aOutputMod.setVariantID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
                    aOutputMod.setService(RutDatabase.dbToString(outParameters, "p_service"));
                    aOutputMod.setProformaRefNo(RutDatabase.dbToString(outParameters, "p_proforma_ref_no"));
                    aOutputMod.setPort(RutDatabase.dbToString(outParameters, "p_port"));
                    aOutputMod.setPortSeqFrom(RutDatabase.dbToStrInteger(outParameters, "p_port_seq_no_from"));
                    aOutputMod.setDirFrom(RutDatabase.dbToString(outParameters, "p_direction_from"));               
                    aOutputMod.setSupportPortGrpId(RutDatabase.dbToStrInteger(outParameters, "p_supported_port_group_id"));
                    aOutputMod.setPortGrp(RutDatabase.dbToString(outParameters, "p_port_group_code"));
                    aOutputMod.setPortSeqTo(RutDatabase.dbToStrInteger(outParameters, "p_port_seq_no_to"));
                    aOutputMod.setDirTo(RutDatabase.dbToString(outParameters, "p_direction_to"));                  
                    aOutputMod.setLoadDis(RutDatabase.dbToString(outParameters, "p_load_discharge_flag"));
                    aOutputMod.setTranShip(RutDatabase.dbToString(outParameters, "p_transhipment_flag"));
                    aOutputMod.setTrunkInd(RutDatabase.dbToString(outParameters, "p_wayport_trunk_indicator"));
                    aOutputMod.setSubCode(RutDatabase.dbToString(outParameters, "p_port_call_level"));         
                    aOutputMod.setSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_slot_teu"));
                    aOutputMod.setSlotTon(RutDatabase.dbToStrInteger(outParameters, "p_slot_tons"));
                    aOutputMod.setSlotRef(RutDatabase.dbToStrInteger(outParameters, "p_slot_reefer_plugs"));
                    aOutputMod.setAvgCoc(RutDatabase.dbToStrBigDecimal(outParameters, "p_avg_coc_teu_weight"));
                    aOutputMod.setAvgSoc(RutDatabase.dbToStrBigDecimal(outParameters, "p_avg_soc_teu_weight"));
                    aOutputMod.setMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_min_teu"));               
                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
                    aOutputMod.setTolelant(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolelant"));
                    
                } 
                }
                return isSuccess;
                      
        }
    }
    
    public String getModelId(String bsaModelName) {
        System.out.println("[BsaBsaServiceVariantJdbcDao][getModelId]: Started");
        String bsaModelId = "";
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT BSA_MODEL_ID ");
        sql.append("FROM VR_BSA_BASE_ALLOCATION_MODEL ");
        sql.append("WHERE MODEL_NAME = :bsaModelName");
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("bsaModelName",bsaModelName ));
        if(rs.next()) {
            bsaModelId = RutString.nullToStr(rs.getString("BSA_MODEL_ID"));
        }
        System.out.println("modelname :"+ bsaModelName);
        System.out.println("bsaModelId"+ bsaModelId);
        System.out.println("[EmsEquipmentSysValidateJdbcDao][getModelId]: Finished");
        return bsaModelId;
}
    public List listPortCallFindByKeyBsaServiceVariantID(String bsaServiceVariantId) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT PORT_CALL.PK_BSA_PORT_CALL_ID ");
        sql.append("      ,PORT_CALL.BSA_SERVICE_VARIANT_ID ");
        /* */
        sql.append("      ,PORT_CALL.SERVICE ");
        sql.append("      ,PORT_CALL.PROFORMA_REF_NO ");
        sql.append("      ,PORT_CALL.PORT_SEQ_NO_FROM ");
        sql.append("      ,PORT_CALL.DIRECTION_FROM ");
        sql.append("      ,PORT_CALL.PORT_SEQ_NO_TO ");
        sql.append("      ,PORT_CALL.DIRECTION_TO ");
        sql.append("      ,PORT_CALL.SUPPORTED_PORT_GROUP_ID ");
        /* */
        sql.append("      ,PORT_CALL.DN_PORT ");
        sql.append("      ,PORT_CALL.DN_PORT_GROUP ");
        sql.append("      ,CASE WHEN NVL(TRIM(PORT_CALL.PORT_CALL_LEVEL),' ') = 'S' THEN 'sub' ELSE ' ' END  PORT_CALL_LEVEL_DES ");
        sql.append("      ,PORT_CALL.PORT_CALL_LEVEL PORT_CALL_LEVEL ");
        sql.append("      ,DECODE (PORT_CALL.DIRECTION , 'N','North'" + 
        "                                          ,'S','South'" + 
        "                                          ,'E' ,'East'" + 
        "                                          ,'W' ,'West'" + 
        "                                          ,'NE','North East'" + 
        "                                          ,'NW','North West'" + 
        "                                          ,'SE','South East'" + 
        "                                          ,'SW','South West'" + 
        "                                          ,'R' ,'Round'" + 
        "                                          ,' ') DIRECTION ");        
        sql.append("      ,DECODE (PORT_CALL.DN_LOAD_DISCHARGE_FLAG , 'L','Load'" + 
        "                                          ,'D','Discharge'" + 
        "                                          ,'B' ,'Both'" + 
        "                                          ,'T' ,'Transit'" + 
        "                                          ,' ') DN_LOAD_DISCHARGE_FLAG ");        
        sql.append("      ,DECODE (PORT_CALL.DN_TRANSHIPMENT_FLAG , 'Y','Yes'" + 
        "                                          ,'N','No'" + 
        "                                          ,' ') DN_TRANSHIPMENT_FLAG ");      
        sql.append("      ,PORT_CALL.DN_TRUNK_IND ");        
        sql.append("      ,PORT_CALL.SLOT_TEU ");
        sql.append("      ,PORT_CALL.SLOT_TONS ");
        sql.append("      ,PORT_CALL.SLOT_REEFER_PLUGS ");
        sql.append("      ,PORT_CALL.AVG_COC_TEU_WEIGHT ");
        sql.append("      ,PORT_CALL.AVG_SOC_TEU_WEIGHT ");
        sql.append("      ,PORT_CALL.MIN_TEU ");
        sql.append("      ,PORT_CALL.RECORD_STATUS ");
        sql.append("      ,PORT_CALL.RECORD_ADD_USER ");
        sql.append("      ,PORT_CALL.RECORD_ADD_DATE ");
        sql.append("      ,PORT_CALL.RECORD_CHANGE_USER ");
        sql.append("      ,PORT_CALL.RECORD_CHANGE_DATE ");
        sql.append("      ,PORT_CALL.TOLERANT ");
        sql.append("FROM VR_BSA_ALLOCATION_PORT_CALLS PORT_CALL ");
        sql.append("WHERE PORT_CALL.BSA_SERVICE_VARIANT_ID = :bsaServiceVariantId ");        
        sql.append("ORDER BY PORT_SEQ_NO_TO,DN_PORT ");
        HashMap map = new HashMap();
        map.put("bsaServiceVariantId",bsaServiceVariantId);
        
        System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][listPortCallFindByKeyBsaServiceVariantID]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaPortCallMod bsaPortCallMod = new BsaPortCallMod();
                           bsaPortCallMod.setPortCallID(RutString.nullToStr(rs.getString("PK_BSA_PORT_CALL_ID")));
                           bsaPortCallMod.setVariantID(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                           bsaPortCallMod.setService(RutString.nullToStr(rs.getString("SERVICE")));//System.out.println(">>>bsaPortCallMod.getService() "+bsaPortCallMod.getService());
                           bsaPortCallMod.setProformaRefNo(RutString.nullToStr(rs.getString("PROFORMA_REF_NO")));//System.out.println(">>>bsaPortCallMod.getProformaRefNo() "+bsaPortCallMod.getProformaRefNo());
                           bsaPortCallMod.setPort(RutString.nullToStr(rs.getString("DN_PORT")));System.out.println(">>>bsaPortCallMod.getPort() "+bsaPortCallMod.getPort());
                           bsaPortCallMod.setPortSeqFrom(RutString.nullToStr(rs.getString("PORT_SEQ_NO_FROM")));//System.out.println(">>>bsaPortCallMod.getPortSeqFrom "+bsaPortCallMod.getPortSeqFrom());
                           bsaPortCallMod.setDirFrom(RutString.nullToStr(rs.getString("DIRECTION_FROM")));//System.out.println(">>>bsaPortCallMod.getDirFrom() "+bsaPortCallMod.getDirFrom());
                           bsaPortCallMod.setSupportPortGrpId(RutString.nullToStr(rs.getString("SUPPORTED_PORT_GROUP_ID")));System.out.println(">>>bsaPortCallMod.getSupportPortGrpId() "+bsaPortCallMod.getSupportPortGrpId());                            
                           bsaPortCallMod.setPortGrp(RutString.nullToStr(rs.getString("DN_PORT_GROUP")));System.out.println(">>>bsaPortCallMod.getPortGrp() "+bsaPortCallMod.getPortGrp());
                           bsaPortCallMod.setPortSeqTo(RutString.nullToStr(rs.getString("PORT_SEQ_NO_TO")));//System.out.println(">>>bsaPortCallMod.getPortSeqTo() "+bsaPortCallMod.getPortSeqTo());
                           bsaPortCallMod.setDirTo(RutString.nullToStr(rs.getString("DIRECTION_TO")));//System.out.println(">>>bsaPortCallMod.getDirTo() "+bsaPortCallMod.getDirTo());
                           bsaPortCallMod.setSub(RutString.nullToStr(rs.getString("PORT_CALL_LEVEL_DES"))); //System.out.println(">>>bsaPortCallMod.getSub() "+bsaPortCallMod.getSub());
                           bsaPortCallMod.setLoadDis(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE_FLAG")));//System.out.println(">>>bsaPortCallMod.getLoadDis() "+bsaPortCallMod.getLoadDis());
                           bsaPortCallMod.setTranShip(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT_FLAG")));//System.out.println(">>>bsaPortCallMod.getTranShip() "+bsaPortCallMod.getTranShip());
                           bsaPortCallMod.setTrunkInd(RutString.nullToStr(rs.getString("DN_TRUNK_IND")));//System.out.println(">>>bsaPortCallMod.getTrunkInd() "+bsaPortCallMod.getTrunkInd());
                           bsaPortCallMod.setSubCode(RutString.nullToStr(rs.getString("PORT_CALL_LEVEL")));//System.out.println(">>>bsaPortCallMod.getSubCode() "+bsaPortCallMod.getSubCode());
                           bsaPortCallMod.setDir(RutString.nullToStr(rs.getString("DIRECTION")));//System.out.println(">>>bsaPortCallMod.getDir() "+bsaPortCallMod.getDir());
                           bsaPortCallMod.setSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));//System.out.println(">>>bsaPortCallMod.getSlotTeu() "+bsaPortCallMod.getSlotTeu());
                           bsaPortCallMod.setSlotTon(RutString.nullToStr(rs.getString("SLOT_TONS")));//System.out.println(">>>bsaPortCallMod.getSlotTon() "+bsaPortCallMod.getSlotTon());
                           bsaPortCallMod.setSlotRef(RutString.nullToStr(rs.getString("SLOT_REEFER_PLUGS")));// System.out.println(">>>bsaPortCallMod.getSlotRef() "+bsaPortCallMod.getSlotRef());
                           bsaPortCallMod.setAvgCoc(RutString.nullToStr(rs.getString("AVG_COC_TEU_WEIGHT")));//System.out.println(">>>bsaPortCallMod.getAvgCoc() "+bsaPortCallMod.getAvgCoc());
                           bsaPortCallMod.setAvgSoc(RutString.nullToStr(rs.getString("AVG_SOC_TEU_WEIGHT")));//System.out.println(">>>bsaPortCallMod.getAvgSoc() "+bsaPortCallMod.getAvgSoc());
                           bsaPortCallMod.setMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));//System.out.println(">>>bsaPortCallMod.getMinTeu() "+bsaPortCallMod.getMinTeu());
                           bsaPortCallMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));//System.out.println(">>>bsaPortCallMod.getRecordStatus() "+bsaPortCallMod.getRecordStatus());
                           bsaPortCallMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));//System.out.println(">>>bsaPortCallMod.getRecordAddUser() "+bsaPortCallMod.getRecordAddUser());
                           bsaPortCallMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));//System.out.println(">>>bsaPortCallMod.getRecordAddDate() "+bsaPortCallMod.getRecordAddDate());
                           bsaPortCallMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));//System.out.println(">>>bsaPortCallMod.getRecordChangeUser() "+bsaPortCallMod.getRecordChangeUser());
                           bsaPortCallMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));//System.out.println(">>>bsaPortCallMod.getRecordChangeDate() "+bsaPortCallMod.getRecordChangeDate());
                          
                            if(RutString.nullToStr(rs.getString("TOLERANT")).equals("")||rs.getString("TOLERANT").equals("0")){
                               bsaPortCallMod.setTolelant("0.00");
                            }else{
                                DecimalFormat dc=new DecimalFormat();
                                dc.applyPattern("###.00");
                               bsaPortCallMod.setTolelant(dc.format(rs.getDouble("TOLERANT")));
                            }  
                         // bsaPortCallMod.setTolelant(RutString.nullToStr(rs.getString("TOLERANT")));
                           return bsaPortCallMod;
                        }
                   });
    }
    
    
   /* 
    
    public List listAddPortCallFindByKeyBsaServiceVariantID(String bsaServiceVariantId) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ADD_PORT_CALL.PK_BSA_PORT_CALL_ID ");
        sql.append("      ,ADD_PORT_CALL.FK_BSA_SERVICE_VARIANT_ID ");
        sql.append("      ,ADD_PORT_CALL.DN_PORT ");
//        sql.append("      ,ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG ");    
//        sql.append("      ,DECODE (ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG , 'L','Load'" + 
//        "                                          , 'D','Discharge'" + 
//        "                                          ,'B' ,'Both'" + 
//        "                                          ,'T' ,'Transit'" + 
//        "                                          ,' ') DN_LOAD_DISCHARGE ");  
//        sql.append("      ,ADD_PORT_CALL.DN_TRANSHIPMENT_FLAG ");        
//        sql.append("      ,DECODE (ADD_PORT_CALL.DN_TRANSHIPMENT_FLAG , 'Y','Yes'" + 
//        "                                          , 'N','No'" + 
//        "                                          ,' ') DN_TRANSHIPMENT ");       
        sql.append("      ,ADD_PORT_CALL.TOLELANT ");
        sql.append("      ,ADD_PORT_CALL.SLOT_TEU ");
        sql.append("      ,ADD_PORT_CALL.SLOT_TONS ");
        sql.append("      ,ADD_PORT_CALL.MIN_TEU ");
        sql.append("      ,ADD_PORT_CALL.RECORD_STATUS ");
        sql.append("      ,ADD_PORT_CALL.RECORD_ADD_USER ");
        sql.append("      ,ADD_PORT_CALL.RECORD_ADD_DATE ");
        sql.append("      ,ADD_PORT_CALL.RECORD_CHANGE_USER ");
        sql.append("      ,ADD_PORT_CALL.RECORD_CHANGE_DATE ");
        sql.append("FROM BSA_ADD_PORT_CALL ADD_PORT_CALL ");
        sql.append("WHERE ADD_PORT_CALL.FK_BSA_SERVICE_VARIANT_ID = :bsaServiceVariantId ");        
        sql.append("ORDER BY DN_PORT ");
        HashMap map = new HashMap();
        map.put("bsaServiceVariantId",bsaServiceVariantId);
        
        System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][listAddPortCallFindByKeyBsaServiceVariantID]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaAdditionPortCallMod bsaAddPortCallMod = new BsaAdditionPortCallMod();
                           bsaAddPortCallMod.setPortCallID(RutString.nullToStr(rs.getString("PK_BSA_PORT_CALL_ID")));
                           bsaAddPortCallMod.setVariantID(RutString.nullToStr(rs.getString("FK_BSA_SERVICE_VARIANT_ID")));
                           bsaAddPortCallMod.setAddPort(RutString.nullToStr(rs.getString("DN_PORT")));System.out.println(">>>bsaPortCallMod.getPort() "+bsaAddPortCallMod.getAddPort());
//                           bsaAddPortCallMod.setAddLoadDis(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE_FLAG")));//System.out.println(">>>bsaPortCallMod.getLoadDis() "+bsaPortCallMod.getLoadDis());
//                           bsaAddPortCallMod.setAddTranShipFlag(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT_FLAG")));//System.out.println(">>>bsaPortCallMod.getTranShip() "+bsaPortCallMod.getTranShip());
//                           bsaAddPortCallMod.setAddLoadDisFlag(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE")));//System.out.println(">>>bsaPortCallMod.getLoadDis() "+bsaPortCallMod.getLoadDis());
//                           bsaAddPortCallMod.setAddTranShip(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT")));//System.out.println(">>>bsaPortCallMod.getTranShip() "+bsaPortCallMod.getTranShip());
                           bsaAddPortCallMod.setAddTolelant(RutString.nullToStr(rs.getString("TOLELANT")));//System.out.println(">>>bsaPortCallMod.getAvgSoc() "+bsaPortCallMod.getAvgSoc());
                           bsaAddPortCallMod.setAddSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));//System.out.println(">>>bsaPortCallMod.getSlotTeu() "+bsaPortCallMod.getSlotTeu());
                           bsaAddPortCallMod.setAddSlotTon(RutString.nullToStr(rs.getString("SLOT_TONS")));//System.out.println(">>>bsaPortCallMod.getSlotTon() "+bsaPortCallMod.getSlotTon());   
                           bsaAddPortCallMod.setAddMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));//System.out.println(">>>bsaPortCallMod.getMinTeu() "+bsaPortCallMod.getMinTeu());
                           bsaAddPortCallMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));//System.out.println(">>>bsaPortCallMod.getRecordStatus() "+bsaPortCallMod.getRecordStatus());
                           bsaAddPortCallMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));//System.out.println(">>>bsaPortCallMod.getRecordAddUser() "+bsaPortCallMod.getRecordAddUser());
                           bsaAddPortCallMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));//System.out.println(">>>bsaPortCallMod.getRecordAddDate() "+bsaPortCallMod.getRecordAddDate());
                           bsaAddPortCallMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));//System.out.println(">>>bsaPortCallMod.getRecordChangeUser() "+bsaPortCallMod.getRecordChangeUser());
                           bsaAddPortCallMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));//System.out.println(">>>bsaPortCallMod.getRecordChangeDate() "+bsaPortCallMod.getRecordChangeDate());
                          
                           return bsaAddPortCallMod;
                        }
                   });
    }
    
    */
    // ##03 BEGIN
    /*
    public void saveMasterDetails( String masterOperationFlag, List dtlModifiedMods, Timestamp copyRecordChangeDate) throws DataAccessException {
//        BsaPortCallMod mod = (BsaPortCallMod) masterMod;
        BsaAdditionPortCallMod detailMod = null;
        RcmModifiedObjectMod modifiedMod = null;
//        BsaAdditionPortCallMod modAddProt 
        
        // save record change date
//        Timestamp masterRecChangeDate = mod.getRecordChangeDate();
//        String variedId = "";
//        
//        variedId = detailMod.getVariantID();
//        
//        detailMod.setVariantID(variedId);
//        
//        //begin: save master
        StringBuffer errorMsgBuffer = new StringBuffer();
//        try {
//            if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(masterOperationFlag)) {
//                insertAddPortCall(masterMod);
//            } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(masterOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_MODIFY.equals(masterOperationFlag)) {
//                update(masterMod);
//            }
//        } catch (CustomDataAccessException e) {
//            errorMsgBuffer.append(e.getMessages()+"&");
//        } catch (DataAccessException e) {
//            e.printStackTrace();
//        }
//        //end: save master
        
//        boolean isChangeAddPort = false;
//        String strOperationFlag = null;
//        System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][saveMasterDetails] : errorMsgBuffer = "+errorMsgBuffer.toString());
//        if (!errorMsgBuffer.toString().equals("")) {
//            isChangeAddPort = false;
//            mod.setRecordChangeDate(masterRecChangeDate);
//            throw new CustomDataAccessException(errorMsgBuffer.toString());
//        }
        
        //begin: save detail in master
        int index = 0;
        boolean isChangeAddPort = false;
        String strOperationFlag = null;
        for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
            modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
            if (modifiedMod != null && modifiedMod.getObjectMod() instanceof BsaAdditionPortCallMod) { //if 1
                
                try {
                    strOperationFlag = modifiedMod.getOperationFlag();
                    detailMod = (BsaAdditionPortCallMod) modifiedMod.getObjectMod();
                    detailMod.setPortCallID(detailMod.getPortCallID());
                    detailMod.setVariantID(detailMod.getVariantID());
                        
                    if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag) || detailMod.getPortCallID().equals("")) {
                        isChangeAddPort = true;
                        insertAddPortCall(detailMod);
                       
                    } else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_MODIFY.equals(strOperationFlag)) {
                        
//                    else if (RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)) {
                        isChangeAddPort = true;
                        updateAddPortCall(detailMod);
                    } else if (RutOperationFlagManager.OPERATION_FLAG_DELETE.equals(strOperationFlag)|| detailMod.getFlag().equalsIgnoreCase("Y")) {   
                        isChangeAddPort = true;
                        delete(detailMod);
                    }
       
//                } catch (CustomDataAccessException e) {
//                    index = modifiedMod.getSeqNo() + 1;
//                    errorMsgBuffer.append(e.getMessages()+"%"+index+"&");    
                } catch (DataAccessException e) {
                    e.printStackTrace();
                }
                
            } //end if 1
        } //end for 1
        //end: save detail in master  
        
        if (!errorMsgBuffer.toString().equals("")) {
//            mod.setRecordChangeDate(masterRecChangeDate);
            throw new CustomDataAccessException(errorMsgBuffer.toString());
        }  
        try {
            // check constraint
            this.checkConstraints(detailMod, dtlModifiedMods);
            
//            // ##02 BEGIN
//            if (isChangeAddPort) {
//                // change supported port group
//                this.changeAddPort(dtlModifiedMods);
//                
//            }
            // ##02 END
            
            // verify all service variant model in BSA model id
//            this.verify(mod.getBsaModelId());
            
        } catch(CustomDataAccessException e) {
//            mod.setRecordChangeDate(masterRecChangeDate);
            throw e;
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    // ##03 END    
  
     
     private void checkConstraints(RrcStandardMod masterMod, List dtlModifiedMods) {
         StringBuffer errorMsgBuffer = new StringBuffer();
         
         BsaPortCallMod mod = (BsaPortCallMod) masterMod;
         BsaAdditionPortCallMod detailMod = null;
         RcmModifiedObjectMod modifiedMod = null;
         String strOperationFlag = null;
         
         for (int i=0;i<dtlModifiedMods.size();i++) { //for 1
             modifiedMod = (RcmModifiedObjectMod) dtlModifiedMods.get(i);
             if (modifiedMod != null && modifiedMod.getObjectMod() instanceof BsaAdditionPortCallMod) { //if 1
             
                 try {
                     strOperationFlag = modifiedMod.getOperationFlag();
                     detailMod = (BsaAdditionPortCallMod) modifiedMod.getObjectMod();
//                     detailMod.setBsaModelId(mod.getBsaModelId());
                     
                     if (RutOperationFlagManager.OPERATION_FLAG_INSERT.equals(strOperationFlag) || RutOperationFlagManager.OPERATION_FLAG_UPDATE.equals(strOperationFlag)) {
                        checkConstraint(detailMod);
                     } else {
                         //don't do anythings in checking constraints.
                     }
                     
                 } catch (CustomDataAccessException e) {
                     if (errorMsgBuffer.indexOf(e.getMessages()) == -1) {
                         errorMsgBuffer.append(e.getMessages()+"&");
                     }
                 } catch (DataAccessException e) {
                     e.printStackTrace();
                 }
             
             } //end if 1
         } //end for 1
         
         System.out.println("[BsaBsaModelJdbcDao][checkConstraints] : errorMsgBuffer = "+errorMsgBuffer.toString());
         if (!errorMsgBuffer.toString().equals("")) {
             throw new CustomDataAccessException(errorMsgBuffer.toString());
         }
     }
     */
    
   /*
    protected class InsertStoreProcedure extends StoredProcedure {
            private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_INS_BSA_ADD_PORT_CALL";
    
            protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
                super(jdbcTemplate, STORED_PROCEDURE_NAME);
                
                declareParameter(new SqlInOutParameter("p_bsa_port_call_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_bsa_service_variant_id", Types.INTEGER));
                declareParameter(new SqlInOutParameter("p_port",Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_load_discharge_flag", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_transhipment_flag", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_tolelant", Types.NUMERIC));   // ADD TOLELANT
                declareParameter(new SqlInOutParameter("p_slot_teu", Types.INTEGER)); 
                declareParameter(new SqlInOutParameter("p_slot_tons", Types.INTEGER)); 
                declareParameter(new SqlInOutParameter("p_min_teu", Types.INTEGER)); 
                declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
                declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
                declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));         
                compile();
            }
    
        
            protected boolean insertAddPortCall(RrcStandardMod mod) {
                return insertAddPortCall(mod,mod);
            }

            protected boolean insertAddPortCall(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
                boolean isSuccess = false;
               
                if((inputMod instanceof BsaAdditionPortCallMod)&&(outputMod instanceof BsaAdditionPortCallMod)){
                    BsaAdditionPortCallMod aInputMod = (BsaAdditionPortCallMod)inputMod;
                    BsaAdditionPortCallMod aOutputMod = (BsaAdditionPortCallMod)outputMod;
                    Map inParameters = new HashMap(14);
                    
                    
                    
                    inParameters.put("p_bsa_port_call_id", RutDatabase.integerToDb(aInputMod.getPortCallID()));
                    inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVariantID()));
                    inParameters.put("p_port", RutDatabase.stringToDb(aInputMod.getAddPort()));      
//                    String loadDis = RutDatabase.stringToDb(aInputMod.getAddLoadDisFlag().substring(0,1)) ;
//                    //                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis()) ;
//                    System.out.println("Load Dis " + loadDis);               
//                    String tranship = RutDatabase.stringToDb(aInputMod.getAddTranShipFlag().substring(0,1));
//                    //                    String tranship = RutDatabase.stringToDb(aInputMod.getTranShip());
//                    System.out.println("tranship Flag " + tranship);  
                      
                    inParameters.put("p_load_discharge_flag"," ");
                    inParameters.put("p_transhipment_flag"," ");
            
                    inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getAddTolelant()));
                    inParameters.put("p_slot_teu", RutDatabase.integerToDb(aInputMod.getAddSlotTeu()));
                    inParameters.put("p_slot_tons", RutDatabase.integerToDb(aInputMod.getAddSlotTon()));
                    inParameters.put("p_min_teu",RutDatabase.integerToDb(aInputMod.getAddMinTeu()));
                    inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
                    inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
                    inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
                    inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
//                    inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
//                    inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());

                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_bsa_port_call_id:"+aInputMod.getPortCallID());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_bsa_service_variant_id:"+aInputMod.getVariantID());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_port:"+aInputMod.getAddPort());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][UpdateStoreProcedure][update]: p_load_discharge_flag = "+inParameters.get("p_load_discharge_flag"));
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][UpdateStoreProcedure][update]: p_transhipment_flag = "+inParameters.get("p_transhipment_flag"));
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_tolelant:"+aInputMod.getAddTolelant());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_slot_teu:"+aInputMod.getAddSlotTeu());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_slot_tons:"+aInputMod.getAddSlotTon());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_min_teu:"+aInputMod.getAddMinTeu());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                    System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
   
 
                    Map outParameters = execute(inParameters);
                    if (outParameters.size() > 0) {
                        isSuccess = true;
                        
                        aOutputMod.setPortCallID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_port_call_id"));
                        aOutputMod.setVariantID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
                        aOutputMod.setAddPort(RutDatabase.dbToString(outParameters, "p_port"));
                        aOutputMod.setAddLoadDisFlag(RutDatabase.dbToString(outParameters, "p_load_discharge_flag"));
                        aOutputMod.setAddTranShipFlag(RutDatabase.dbToString(outParameters, "p_transhipment_flag"));
                        aOutputMod.setAddTolelant(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolelant"));
                        aOutputMod.setAddSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_slot_teu"));
                        aOutputMod.setAddSlotTon(RutDatabase.dbToStrInteger(outParameters, "p_slot_tons"));
                        aOutputMod.setAddMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_min_teu"));               
                        aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                        aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
                        aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
                        aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
                        aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
                    }
                }
               
                return isSuccess;
            }
        }
   
  
    protected class UpdateStoreProcedureAddPortCall extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_UPD_BSA_ADD_PORT_CALL";
        
        protected UpdateStoreProcedureAddPortCall(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            
            declareParameter(new SqlInOutParameter("p_bsa_port_call_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_bsa_service_variant_id", Types.INTEGER));
            declareParameter(new SqlInOutParameter("p_port",Types.VARCHAR));    
            declareParameter(new SqlInOutParameter("p_load_discharge_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_transhipment_flag", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_tolelant", Types.NUMERIC));   // ADD TOLELANT
            declareParameter(new SqlInOutParameter("p_slot_teu", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_slot_tons", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_min_teu", Types.INTEGER)); 
            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
         
           
            compile();
        }
      
    protected boolean updateAddPortCall(RrcStandardMod mod) {
        return updateAddPortCall(mod,mod);
    } 
        
    
    protected boolean updateAddPortCall(final RrcStandardMod inputMod,RrcStandardMod outputMod ) {
        boolean isSuccess = false;
        if((inputMod instanceof BsaAdditionPortCallMod)&&(outputMod instanceof BsaAdditionPortCallMod)){
            BsaAdditionPortCallMod aInputMod = (BsaAdditionPortCallMod)inputMod;
            BsaAdditionPortCallMod aOutputMod = (BsaAdditionPortCallMod)outputMod;
            Map inParameters = new HashMap();
    
              
    
            inParameters.put("p_bsa_port_call_id", RutDatabase.integerToDb(aInputMod.getPortCallID()));
            inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVariantID()));
            inParameters.put("p_port", RutDatabase.stringToDb(aInputMod.getAddPort())); 
//            String loadDis = RutDatabase.stringToDb(aInputMod.getAddLoadDisFlag().substring(0,1)) ;
    //                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis()) ;
//            System.out.println("Load Dis " + loadDis);               
//            String tranship = RutDatabase.stringToDb(aInputMod.getAddTranShipFlag().substring(0,1));
    //                    String tranship = RutDatabase.stringToDb(aInputMod.getTranShip());
//            System.out.println("tranship Flag " + tranship);  
            inParameters.put("p_load_discharge_flag"," ");
            inParameters.put("p_transhipment_flag"," ");
            inParameters.put("p_tolelant", RutDatabase.bigDecimalToDb(aInputMod.getAddTolelant()));
            inParameters.put("p_slot_teu", RutDatabase.integerToDb(aInputMod.getAddSlotTeu()));
            inParameters.put("p_slot_tons", RutDatabase.integerToDb(aInputMod.getAddSlotTon()));
            inParameters.put("p_min_teu",RutDatabase.integerToDb(aInputMod.getAddMinTeu()));
            inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
            inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
            inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
  
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_bsa_port_call_id = "+inParameters.get("p_bsa_port_call_id"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port = "+inParameters.get("p_port"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_load_discharge_flag = "+inParameters.get("p_load_discharge_flag"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_transhipment_flag = "+inParameters.get("p_transhipment_flag"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_tolelant = "+inParameters.get("p_tolelant")); 
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_teu = "+inParameters.get("p_slot_teu")); 
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_tons = "+inParameters.get("p_slot_tons"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_min_teu = "+inParameters.get("p_min_teu"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
           
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
                isSuccess = true;      
                aOutputMod.setPortCallID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_port_call_id"));
                aOutputMod.setVariantID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
                aOutputMod.setAddPort(RutDatabase.dbToString(outParameters, "p_port"));
                aOutputMod.setAddLoadDisFlag(RutDatabase.dbToString(outParameters, "p_load_discharge_flag"));
                aOutputMod.setAddTranShipFlag(RutDatabase.dbToString(outParameters, "p_transhipment_flag"));
                aOutputMod.setAddTolelant(RutDatabase.dbToStrBigDecimal(outParameters, "p_tolelant"));
                aOutputMod.setAddSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_slot_teu"));
                aOutputMod.setAddSlotTon(RutDatabase.dbToStrInteger(outParameters, "p_slot_tons"));
                aOutputMod.setAddMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_min_teu"));               
                aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
                aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
                aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
               
                
            } 
            }
            return isSuccess;
                  
    }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
         private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_DEL_BSA_ADD_PORT_CALL";

         protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
             super(jdbcTemplate, STORED_PROCEDURE_NAME);
             declareParameter(new SqlParameter("p_bsa_port_call_id", Types.INTEGER));
             declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
             declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
             compile();
         }
         
         protected boolean delete(final RrcStandardMod inputMod) {
             boolean isSuccess = false;
             if (inputMod instanceof BsaAdditionPortCallMod) {
                 BsaAdditionPortCallMod aInputMod = (BsaAdditionPortCallMod) inputMod;
                 
                 Map inParameters = new HashMap(3);
                 inParameters.put("p_bsa_port_call_id", new Integer((RutString.nullToStr(aInputMod.getPortCallID())).equals("")?"0":RutString.nullToStr(aInputMod.getPortCallID())));
                 inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
                 inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
                 
                 System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][DeleteStoreProcedure][delete]:p_bsa_port_call_id:"+aInputMod.getPortCallID());
                 System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
                 System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
                 execute(inParameters);
                 isSuccess = true;
             }
             return isSuccess;
         }
     }
   
    public void checkConstraint(RrcStandardMod mod) throws DataAccessException{
        checkConstraintStoreProcedure.checkConstraint(mod);
    }
        
    
    protected class CheckConstraintStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_CHK_CONSTRAINT_RECORDS";
     
        protected CheckConstraintStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);            
            declareParameter(new SqlInOutParameter("p_bsa_port_call_id", Types.INTEGER));            
            compile();
        }

        protected void checkConstraint(RrcStandardMod mod) {
            checkConstraint(mod,mod);
        }
        
        protected void checkConstraint(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
            if((inputMod instanceof BsaAdditionPortCallMod)||(outputMod instanceof BsaAdditionPortCallMod)){
                BsaAdditionPortCallMod aInputMod = (BsaAdditionPortCallMod)inputMod;
                Map inParameters = new HashMap(1);
    ////
                System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][CheckConstraintStoreProcedure][checkConstraint]:p_bsa_port_call_id:"+aInputMod.getPortCallID());
    ////
                inParameters.put("p_bsa_port_call_id", aInputMod.getPortCallID());
                execute(inParameters);
            }
        } 
    }
    */

//    public List findBsaAddLoadDischByPortCallId(String portCallId, String recordStatus) throws DataAccessException {
//        StringBuffer sql = new StringBuffer();
//        sql.append("SELECT distinct upper(apc.bsa_vessel_type) as BSA_VESSEL_TYPE ");
//        sql.append("      ,DECODE (ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG , 'L','Load'" + 
//        "                                          , 'D','Discharge'" + 
//        "                                          ,'B' ,'Both'" + 
//        "                                          ,'T' ,'Transit'" + 
//        "                                          ,' ') DN_LOAD_DISCHARGE ");  
//        sql.append("FROM BSA_ADD_PORT_CALL ADD_PORT_CALL ");
//        sql.append("WHERE ADD_PORT_CALL.PK_BSA_PORT_CALL_ID = :portCallId ");        
//        sql.append("  AND ADD_PORT_CALL.RECORD_STATUS = :recordStatus ");   
//        HashMap map = new HashMap();
//        map.put("vssProformaId", portCallId);
//        map.put("recordStatus", recordStatus);
//        
//        System.out.println("[VssProformaVesselAssignmentJdbcDao][findBsaVesselTypeByVssProformaId]: vssProformaId = "+vssProformaId);
//        System.out.println("[VssProformaVesselAssignmentJdbcDao][findBsaVesselTypeByVssProformaId]: recordStatus = "+recordStatus);
//        
//        return getNamedParameterJdbcTemplate().query(
//                   sql.toString(),
//                   map,
//                   new RowMapper() {
//                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
//                           String bsaVesselType = RutString.nullToStr(rs.getString("BSA_VESSEL_TYPE"));
//                           return bsaVesselType;
//                       }
//                   });
//    }


}
