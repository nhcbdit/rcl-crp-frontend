package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.model.dex.DexBillToPartyMod;
import com.rclgroup.dolphin.web.model.dex.DexBlFscMod;
import com.rclgroup.dolphin.web.model.dex.DexBypassFsc;
import com.rclgroup.dolphin.web.model.dex.DexZccValueMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class DexBillToPartyCollFscJdbcDao extends RrcStandardDao implements DexBillToPartyCollFscDao{
    private GetBillToPartyProcedure getBillToPartyProcedure;
    private InsertBillToPartyProcedure insertBillToPartyProcedure;
    private GetConfigureProcedure getConfigureProcedure;
    private CheckBillToPartyProcedure checkBillToPartyProcedure;
    private UpdateBillToPartyProcedure updateBillToPartyProcedure;
    private GetCustBillPartyProcedure getCustBillPartyProcedure;
    private GetFscDescProcedure getFscDescProcedure;
    
    public void initDao() throws Exception {
        super.initDao(); 
        getBillToPartyProcedure = new GetBillToPartyProcedure(getJdbcTemplate(), new DexBillToPartyMapper());
        insertBillToPartyProcedure = new InsertBillToPartyProcedure(getJdbcTemplate());
        getConfigureProcedure = new GetConfigureProcedure(getJdbcTemplate(), new DexZccValueMapper());
        checkBillToPartyProcedure = new CheckBillToPartyProcedure(getJdbcTemplate());
        updateBillToPartyProcedure = new UpdateBillToPartyProcedure(getJdbcTemplate());
        getCustBillPartyProcedure = new GetCustBillPartyProcedure(getJdbcTemplate(), new DexCustBillToPartyMapper());
        getFscDescProcedure  = new GetFscDescProcedure(getJdbcTemplate(), new DexFscMapper());
    }
    public List getBillToParty(String billToParty,String responseFsc,String bypassFsc,String status) {
        Map map = new HashMap();
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getBillToParty] begin");
        map.put("P_BILL_TO_PARTY",billToParty);
        map.put("P_RESPONSE_FSC",responseFsc);
        map.put("P_BY_PASS_FSC",bypassFsc);
        map.put("P_STATUS",status);
        List resultList = getBillToPartyProcedure.getBillToParty(map);    
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getBillToParty] : end");
        return resultList;
    }
    protected class GetBillToPartyProcedure extends StoredProcedure{
        private static final String SQL_DEX_BL_TO_PARTY = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_GET_BILL_TO_PARTY";
        protected GetBillToPartyProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_DEX_BL_TO_PARTY);
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetBillToPartyProcedure] begin");  
            try{
                declareParameter(new SqlInOutParameter("P_BILL_TO_PARTY", OracleTypes.VARCHAR));
                declareParameter(new SqlInOutParameter("P_RESPONSE_FSC", OracleTypes.VARCHAR));
                declareParameter(new SqlInOutParameter("P_BY_PASS_FSC", OracleTypes.VARCHAR));
                declareParameter(new SqlInOutParameter("P_STATUS", OracleTypes.VARCHAR));
                declareParameter(new SqlOutParameter("P_BILL_TO_PARTY_OUT", OracleTypes.CURSOR, rowMapper));
                compile();
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [GetBillToPartyProcedure]" + ex.getMessage());
            }
            System.out.println("[DexBillToPartyCollFscJdbcDao] [getScreenHeaderProcedure] end"); 
        }
    
    protected List getBillToParty(Map mapParams){
            
            System.out.println("[DexBillToPartyCollFscJdbcDao] [getScreenHeader] begin");
            Map outMap = new HashMap();           
            List<DexBillToPartyMod> returnList = new ArrayList<DexBillToPartyMod>();
            try{
                outMap = execute(mapParams);
                returnList = (List<DexBillToPartyMod>) outMap.get("P_BILL_TO_PARTY_OUT");  
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [getScreenHeader]" + ex.getMessage());
                ex.printStackTrace();
            }          
            return returnList;
         }
    }    
    private class DexBillToPartyMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
             DexBillToPartyMod bean = new DexBillToPartyMod();
             bean.setSeqNo(RutString.nullToStr(rs.getString("SEQ")));
             bean.setPartyCode(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));
             bean.setPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
             bean.setResponseFscCode(RutString.nullToStr(rs.getString("BILL_TO_PARTY_FSC")));
             bean.setResponseFscCountry(RutString.nullToStr(rs.getString("BILL_TO_PARTY_FSC_CNTR")));
             bean.setBypassFscCode(RutString.nullToStr(rs.getString("BY_PASS_FSC")));
             bean.setBypassFscCountry(RutString.nullToStr(rs.getString("BY_PASS_FSC_CNTR")));
             bean.setStatus(RutString.nullToStr(rs.getString("STATUS")));
             bean.setReason(RutString.nullToStr(rs.getString("REASON")));
             return bean;
        }
     } 
    public boolean insertBillToParty(String billToParty,
                                         String bypassFsc,
                                         String status,
                                         String reason,
                                         String userId){
        boolean isSuccess = false;
        System.out.println("[insertBillToParty] billToParty: " + billToParty); 
        System.out.println("[insertBillToParty] bypassFsc: " + bypassFsc); 
        System.out.println("[insertBillToParty] status: " + status); 

        Map map = new HashMap();
        map.put("P_BILL_TO_PARTY",billToParty);
        map.put("P_BY_PASS_FSC", bypassFsc);
        map.put("P_STATUS", status);
        map.put("P_REASON",reason);
        map.put("P_USER_ID", userId);
             
        isSuccess = insertBillToPartyProcedure.insertBillToParty(map);
        System.out.println("[insertBillToParty] isSuccess:" + isSuccess);
        System.out.println("[insertBillToParty] end"); 
        return isSuccess;
    }
    private class InsertBillToPartyProcedure extends StoredProcedure{
          private static final String SQL_DEX_INS_BILL_TO_PARTY = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_INS_BILL_TO_PARTY";  
          protected InsertBillToPartyProcedure (JdbcTemplate jdbcTemplate){
              super(jdbcTemplate, SQL_DEX_INS_BILL_TO_PARTY);
               declareParameter(new SqlParameter("P_BILL_TO_PARTY", OracleTypes.VARCHAR));
               declareParameter(new SqlParameter("P_BY_PASS_FSC", OracleTypes.VARCHAR));
               declareParameter(new SqlParameter("P_STATUS",OracleTypes.VARCHAR));
               declareParameter(new SqlParameter("P_REASON",OracleTypes.VARCHAR));
               declareParameter(new SqlParameter("P_USER_ID",OracleTypes.VARCHAR));
               compile();
               System.out.println("[InsertBillToPartyProcedure] end"); 
          }
              
          protected boolean insertBillToParty(Map mapParams){
              boolean isSuccess = false;
              System.out.println(" InsertBillToPartyProcedure P_BILL_TO_PARTY >>>>>>>>>>>>>> "+mapParams.get("P_BILL_TO_PARTY"));
              System.out.println(" InsertBillToPartyProcedure P_BY_PASS_FSC   >>>>>>>>>>>>>> "+mapParams.get("P_BY_PASS_FSC"));
              System.out.println(" InsertBillToPartyProcedure P_STATUS    >>>>>>>>>>>>>> "+mapParams.get("P_STATUS"));
              System.out.println(" InsertBillToPartyProcedure P_REASON   >>>>>>>>>>>>>> "+mapParams.get("P_REASON"));
              System.out.println(" InsertBillToPartyProcedure P_USER_ID    >>>>>>>>>>>>>> "+mapParams.get("P_USER_ID"));
              
              Map outParameters = execute(mapParams);
//              if(outParameters.size() > 0){
                  isSuccess = true;
//              }
              return isSuccess;
          }
      }
    public List getConfigureValue(String value){
        Map map = new HashMap();
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getConfigureValue] begin");
        map.put("P_TABLE_FIELD",value);
        List resultList = getConfigureProcedure.getConfigureValue(map);    
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getConfigureValue] : end");
        return resultList;
    }
    protected class GetConfigureProcedure extends StoredProcedure{
        private static final String SQL_DEX_BL_TO_PARTY = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_GET_ZCC_CODE_VALUE";
        protected GetConfigureProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_DEX_BL_TO_PARTY);
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetConfigureProcedure] begin");  
            try{
                declareParameter(new SqlInOutParameter("P_TABLE_FIELD", OracleTypes.VARCHAR));
                declareParameter(new SqlOutParameter("P_ZCC_VALUE_OUT", OracleTypes.CURSOR, rowMapper));
                compile();
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [GetConfigureProcedure]" + ex.getMessage());
            }
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetConfigureProcedure] end"); 
        }
    
    protected List getConfigureValue(Map mapParams){
            
            System.out.println("[DexBillToPartyCollFscJdbcDao] [getConfigureValue] begin");
            Map outMap = new HashMap();           
            List<DexZccValueMod> returnList = new ArrayList<DexZccValueMod>();
            try{
                outMap = execute(mapParams);
                returnList = (List<DexZccValueMod>) outMap.get("P_ZCC_VALUE_OUT");  
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [getConfigureValue]" + ex.getMessage());
                ex.printStackTrace();
            }          
            return returnList;
         }
    }    
    private class DexZccValueMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
             DexZccValueMod bean = new DexZccValueMod();
             bean.setZccCode(RutString.nullToStr(rs.getString("CODE_VALUE")));
             bean.setZccDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));
             return bean;
        }
     }
     
    public int  validateBillToParty(String billToParty,String bypassFsc){
        System.out.println("[getNumberCompleteDate] validateBillToParty: >>>"); 
        Map map = new HashMap();
        map.put("P_BILL_TO_PARTY",billToParty); 
        map.put("P_BY_PASS_FSC",bypassFsc);    
        System.out.println("[validateBillToParty] after"); 
        int recAct = checkBillToPartyProcedure.getNumberBypass(map);
        System.out.println("[validateBillToParty] end numBypass = " + recAct); 
        return recAct;
    }
    private class CheckBillToPartyProcedure extends StoredProcedure{
          private static final String SQL_DEX_CHK_BILL_TO_PARTY = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_CHK_BILL_TO_PARTY";  
          protected CheckBillToPartyProcedure (JdbcTemplate jdbcTemplate){

              super(jdbcTemplate, SQL_DEX_CHK_BILL_TO_PARTY);
              declareParameter(new SqlInOutParameter("P_BILL_TO_PARTY", OracleTypes.VARCHAR));
              declareParameter(new SqlInOutParameter("P_BY_PASS_FSC", OracleTypes.VARCHAR));
              declareParameter(new SqlOutParameter("P_CNT_BILL_BY_PASS", OracleTypes.INTEGER));
              compile();
              System.out.println("[GetNumberCompleteDateProcedure] end"); 
          }
              
          protected int getNumberBypass(Map mapParams){
              System.out.println("getNumberBypass() >>>> ");
              String recAct = "";
              int total = 0;
              Map outParameters = execute(mapParams);
              System.out.println("getNumberBypass() >>>> size " + outParameters.size());
              if(outParameters.size() > 0){
                     try {
                      recAct= outParameters.get("P_CNT_BILL_BY_PASS").toString();
                      System.out.println("eqrecAct getNumberBypass >>>> "+ recAct);
                      total = Integer.valueOf(recAct);
                     } catch (Exception e) {
                         System.out.println("getNumberBypass() Exception :getPostPicture");
                     }
              }
              System.out.println("getNumberBypass() total >>>> "+ total);
              return total;
          }
      }
      
    public boolean updateBillToParty(String seqNo,
                                     String status,
                                     String userId){
        boolean isSuccess = false;
        System.out.println("[insertBillToParty] seqNo: " + seqNo); 
        System.out.println("[insertBillToParty] status: " + status); 
        System.out.println("[insertBillToParty] userId: " + userId);
        Map map = new HashMap();
        map.put("P_SEQ",seqNo);
        map.put("P_STATUS", status);
        map.put("P_USER_ID", userId);
        
        isSuccess = updateBillToPartyProcedure.updateBillToParty(map);
        System.out.println("[updateBillToParty] isSuccess:" + isSuccess);
        System.out.println("[updateBillToParty] end"); 
        return isSuccess;                                    
                                     
    }      
    private class UpdateBillToPartyProcedure extends StoredProcedure{
          private static final String SQL_DEX_UPD_BILL_TO_PARTY = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_UPD_BILL_TO_PARTY";  
          protected UpdateBillToPartyProcedure (JdbcTemplate jdbcTemplate){
              super(jdbcTemplate, SQL_DEX_UPD_BILL_TO_PARTY);
               declareParameter(new SqlParameter("P_SEQ", OracleTypes.VARCHAR));
               declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
               declareParameter(new SqlParameter("P_STATUS",OracleTypes.VARCHAR));
               compile();
               System.out.println("[UpdateBillToPartyProcedure] end"); 
          }
          protected boolean updateBillToParty(Map mapParams){
              boolean isSuccess = false;
              System.out.println(" updateBillToParty P_SEQ >>>>>>>>>>>>>> "+mapParams.get("P_SEQ"));
              System.out.println(" updateBillToParty P_STATUS   >>>>>>>>>>>>>> "+mapParams.get("P_STATUS"));
              
              Map outParameters = execute(mapParams);
//              if(outParameters.size() > 0){
                  isSuccess = true;
//              }
              return isSuccess;
          }
      }

    public List getCustBillParty(String billToParty){
        Map map = new HashMap();
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getCustBillParty] begin");
        map.put("P_BILL_TO_PARTY",billToParty);
        List resultList = getCustBillPartyProcedure.getCustBillParty(map);    
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getCustBillParty] : end");
        return resultList;
    }
    protected class GetCustBillPartyProcedure extends StoredProcedure{
        private static final String SQL_DEX_GET_CUST_BILL = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_GET_CUST_BILL";
        protected GetCustBillPartyProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_DEX_GET_CUST_BILL);
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetCustBillPartyProcedure] begin");  
            try{
                declareParameter(new SqlInOutParameter("P_BILL_TO_PARTY", OracleTypes.VARCHAR));
                declareParameter(new SqlOutParameter("P_CUST_BILL_OUT", OracleTypes.CURSOR, rowMapper));
                compile();
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [GetCustBillPartyProcedure]" + ex.getMessage());
            }
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetCustBillPartyProcedure] end"); 
        }
    
    protected List getCustBillParty(Map mapParams){
            
            System.out.println("[DexBillToPartyCollFscJdbcDao] [getCustBillParty] begin");
            Map outMap = new HashMap();           
            List<DexBlFscMod> returnList = new ArrayList<DexBlFscMod>();
            try{
                outMap = execute(mapParams);
                returnList = (List<DexBlFscMod>) outMap.get("P_CUST_BILL_OUT");  
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [getCustBillParty]" + ex.getMessage());
                ex.printStackTrace();
            }          
            return returnList;
         }
    }    
    private class DexCustBillToPartyMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
             DexBlFscMod bean = new DexBlFscMod();
             bean.setBlCompany(RutString.nullToStr(rs.getString("DESCRIPTION")));
             bean.setResponseFsc(RutString.nullToStr(rs.getString("FSC")));
             bean.setBlCountry(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
             return bean;
        }
     }
     
    public List getFscDesc(String fscCode){
        Map map = new HashMap();
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getFscDesc] begin");
        map.put("P_BY_PASS_FSC",fscCode);
        List resultList = getFscDescProcedure.getFscDesc(map);    
        System.out.println("[DexBillToPartyCollFscJdbcDao] [getFscDesc] : end");
        return resultList;
    }
    protected class GetFscDescProcedure extends StoredProcedure{
        private static final String SQL_DEX_GET_FSC = "VASAPPS.PCR_DEX_BILL_TO_PARTY_FSC.PRR_DEX_GET_FSC";
        protected GetFscDescProcedure(JdbcTemplate jdbcTemplate, RowMapper rowMapper){
            super(jdbcTemplate, SQL_DEX_GET_FSC);
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetFscDescProcedure] begin");  
            try{
                declareParameter(new SqlInOutParameter("P_BY_PASS_FSC", OracleTypes.VARCHAR));
                declareParameter(new SqlOutParameter("P_FSC_OUT", OracleTypes.CURSOR, rowMapper));
                compile();
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [GetFscDescProcedure]" + ex.getMessage());
            }
            System.out.println("[DexBillToPartyCollFscJdbcDao] [GetFscDescProcedure] end"); 
        }
    
    protected List getFscDesc(Map mapParams){
            
            System.out.println("[DexBillToPartyCollFscJdbcDao] [getFscDesc] begin");
            Map outMap = new HashMap();           
            List<DexBypassFsc> returnList = new ArrayList<DexBypassFsc>();
            try{
                outMap = execute(mapParams);
                returnList = (List<DexBypassFsc>) outMap.get("P_FSC_OUT");  
            }catch(Exception ex){
                System.out.println("[DexBillToPartyCollFscJdbcDao] [getFscDesc]" + ex.getMessage());
                ex.printStackTrace();
            }          
            return returnList;
         }
    }    
    private class DexFscMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int row) throws SQLException {
             DexBypassFsc bean = new DexBypassFsc();
             bean.setBypassFscName(RutString.nullToStr(rs.getString("FSC_NAME")));
             bean.setBypassFscCountry(RutString.nullToStr(rs.getString("FSC_COUNTRY")));
             return bean;
        }
     }
}
