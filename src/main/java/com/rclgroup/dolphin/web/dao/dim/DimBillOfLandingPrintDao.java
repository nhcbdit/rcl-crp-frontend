/*------------------------------------------------------
DimBillOfLandingPrintDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
 Author Kitti Pongsirisakun 12/01/2011
- Change Log -------------------------------------------
## DD/MM/YY �User-        -TaskRef-     -ShortDescription-
02 03/10/16 Sarawut A.                  Add new method for generate excel
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.model.dim.DimBillOfLandingPrintExcelMod;

import java.util.List;
import java.util.Vector;
import org.springframework.dao.DataAccessException;

public interface DimBillOfLandingPrintDao {

    
    /**
     * @param session
     * @param userName
     * @param columnName
     * @param ascDesc
     * @return
     * @throws DataAccessException
     */
    public Vector getListBLPrint(String session, String userName,String columnName , String ascDesc) throws DataAccessException;
    
    /**
     * @param sessionIDPro
     * @param userName
     * @param blList
     * @return
     * @throws DataAccessException
     */
    public String getListBLSelPrint(String sessionIDPro, String userName,String[] blList) throws DataAccessException;
    
    /**
     *  @param quotationNo
     * @param sessionId   
     * @return username
     * @throws DataAccessException
     */
    public String getSessionId(String service , String vessel, String voyage,  String direction, 
                                                      String bl,String cocsoc,
                                                      String pol_sts,String dateFrom,String dateTo,String third,
                                                      String fsc,String userCode,String sessionid,String line,
                                                      String region,String agent,String fscCode, String p_option,
                                                      String p_session_inv,String p_port_inv,String chkFwdBl,String print
                                                     ) throws DataAccessException;
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException;//02
    public List<DimBillOfLandingPrintExcelMod> generateExcelHeader(String userCode,String sessionid) throws DataAccessException;//02
    public List<DimBillOfLandingPrintExcelMod> generateExcelDetail(String userCode,String sessionid) throws DataAccessException;//02
}

