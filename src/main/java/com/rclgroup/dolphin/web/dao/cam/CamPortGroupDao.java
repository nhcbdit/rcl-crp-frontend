/*------------------------------------------------------
CamPortGroupDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
 Author Sopon Dee-udomvongsa 02/09/2008
- Change Log -------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
   25/03/09 PIE              Add listSupportedPortGroupForHelpScreen
--------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.cam;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamPortGroupDao {

    /**
     * @param portGroupCode
     * @return
     * @throws DataAccessException
     */
    public boolean isValid(String portGroupCode) throws DataAccessException;

    /**
     * @param portGroupCode
     * @param status
     * @return
     * @throws DataAccessException
     */
    public boolean isValid(String portGroupCode, String status) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException;

    /**
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;
    
    /**
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return
     * @throws DataAccessException
     */
    public List listSupportedPortGroupForHelpScreen(String find, String search, String wild, String status) throws DataAccessException;
    
}
