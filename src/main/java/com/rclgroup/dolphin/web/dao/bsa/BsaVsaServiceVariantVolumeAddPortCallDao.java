/*-----------------------------------------------------------------------------------------------------------
VsaVsaServiceVariantVolumeAddPortCallDao.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Dhruv Parekh 21/03/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY -User-     -TaskRef-      -Short Description
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.HashMap;
import java.util.List;


import org.springframework.dao.DataAccessException;


public interface BsaVsaServiceVariantVolumeAddPortCallDao extends RriStandardDao {


//    public boolean insert(RrcStandardMod mod) throws DataAccessException;


    /**
     * update a BSA model record
     * @param mod a BSA model model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: BSA_PCZ01_BSA_PORT_CALL_ID_REQ
     *                             error message: BSA_PCZ01_BSA_SERVICE_VARIANT_ID_REQ
     *                             error message: BSA_PCZ01_SERVICE_REQ
     *                             error message: BSA_PCZ01_PROFORMA_REF_NO_REQ
     *                             error message: BSA_PCZ01_PORT_REQ
     *                             error message: BSA_PCZ01_PORT_SEQ_NO_FROM_REQ
     *                             error message: BSA_PCZ01_DIRECTION_FROM_REQ
     *                             error message: BSA_PCZ01_PORT_SEQ_NO_TO_REQ 
     *                             error message: BSA_PCZ01_DIRECTION_TO_REQ
     *                             error message: BSA_PCZ01_LOAD_DISCHARGE_FLAG_REQ
     *                             error message: BSA_PCZ01_TRANSHIPMENT_FLAG_REQ
     *                             error message: BSA_PCZ01_WAYPORT_TRUNK_INDICATOR_REQ
     *                             error message: BSA_PCZ01_PORT_CALL_LEVEL_REQ  
     *                             error message: BSA_PCZ01_SLOT_TEU_REQ
     *                             error message: BSA_PCZ01_SLOT_TONS_REQ
     *                             error message: BSA_PCZ01_SLOT_REEFER_PLUGS_REQ
     *                             error message: BSA_PCZ01_AVG_COC_TEU_WEIGHT_REQ
     *                             error message: BSA_PCZ01_AVG_SOC_TEU_WEIGHT_REQ
     *                             error message: BSA_PCZ01_MIN_TEU_REQ
     *                             error message: BSA_PCZ01_RECORD_STATUS_REQ
     *                             error message: BSA_PCZ01_STATUS_NOT_IN_RANGE
     *                             error message: BSA_PCZ01_RECORD_CHANGE_USER_REQ
     *                             error message: BSA_PCZ01_RECORD_CHANGE_DATE_REQ
     *                             error message: BSA_PCZ01_UPDATE_CON
     *                             error message: BSA_PCZ01_BSA_PORT_CALL_NOT_FOUND
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */
//    public boolean update(RrcStandardMod mod) throws DataAccessException;


//    public boolean delete(RrcStandardMod mod) throws DataAccessException;


    public List listAddPortCallFindByKeyVsaServiceVariantID(String vsaServiceVariantId) throws DataAccessException;

    /**
     * find BSA Port Call of service variant ID
     * @param vsaServiceVariantId , record status 
     * @return
     * @throws DataAccessException
     */
     
//    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException;
     
//    public void checkConstraint(RrcStandardMod mod) throws DataAccessException;

}


