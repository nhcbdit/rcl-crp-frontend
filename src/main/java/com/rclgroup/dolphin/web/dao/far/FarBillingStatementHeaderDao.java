/*-----------------------------------------------------------------------------------------------------------  
FarBillingStatementHeaderDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 30/07/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.model.far.FarBillingStatementHeaderMod;

import org.springframework.dao.DataAccessException;


public interface FarBillingStatementHeaderDao {
   
    /**
     * @param billNo
     * @param billVersion
     * @return FarBillingStatementHeaderMod
     * @throws DataAccessException
     */
    public FarBillingStatementHeaderMod findByKey(String billNo, int billVersion) throws DataAccessException;
    
}
