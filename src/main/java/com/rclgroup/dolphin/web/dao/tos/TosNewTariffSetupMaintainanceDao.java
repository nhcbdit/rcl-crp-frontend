/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupMaintainanceDao.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Dhruv Parekh 28/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupCustomersMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupNonRclPartnerMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupPortTariffMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupServiceMod;

import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupSlotPartnerMod;
import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupVendorsMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

/**
 * DAO Interface for Database Related Operations
 * @author Dhruv Parekh
 * @see com.rclgroup.dolphin.web.common.RriStandardDao
 */
public interface TosNewTariffSetupMaintainanceDao extends RriStandardDao{

    /**
     * Returns List of Tariff Services based on Port and Terminal
     * @param port
     * @param terminal    
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupServiceMod> getTariffServiceList(String port, String terminal) throws DataAccessException;
     
    
    /**
     * Returns List of Port Tariff based on Port and Terminal
     * @param port
     * @param terminal    
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupPortTariffMod> getPortTariffList(String port, String terminal) throws DataAccessException;
     
     
    /**
     * Returns List of Vendors based on Port, Terminal and Service Parameters
     * @param port
     * @param terminal    
     * @param Service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupVendorsMod> getTariffVednorsList(String port, String terminal, String Service) throws DataAccessException;
     
    /**
     * Returns List of Customers based on Port, Terminal and Service Parameters
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupCustomersMod> getTariffCustomerList(String port, String terminal, String service) throws DataAccessException;
     
    /**
     * Returns List of RCL Slot Partners based on Port, Terminal and Service Parameters
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupSlotPartnerMod> getTariffSlotPartnerList(String port, String terminal, String service) throws DataAccessException;
     
    /**
     * Returns List of Non RCL Slot Partners based on Port, Terminal and Service Parameters
     * @param port
     * @param terminal    
     * @param service
     * @return List
     * @throws DataAccessException
     */
     public List<TosNewTariffSetupNonRclPartnerMod> getTariffNonSlotPartnerList(String port, String terminal, String service) throws DataAccessException;
     
    /**     
     * Returns Service Name based on Service Code Parameter
     * @param serviceCd
     * @return String
     * @throws DataAccessException
     */
     public String getServiceDesc(String serviceCd) throws DataAccessException;
     
    /**     
     * Returns Operation Type Description and Operation Type Code based on Rate Reference parameter
     * @param rateRef
     * @return String
     * @throws DataAccessException     
     */
     public String getOprDetail(String port, String terminal, String rateRef) throws DataAccessException;
     
    /**     
     * Returns Customer Name based on Customer Code parameter
     * @param custCd
     * @return String
     * @throws DataAccessException     
     */
     public String getCustDesc(String custCd) throws DataAccessException;
     
    /**     
     * Returns Vendor Name based on Vendor Code parameter
     * @param vendorCd
     * @return String
     * @throws DataAccessException     
     */
     public String getVendorDesc(String vendorCd) throws DataAccessException;
     
    /**     
     * Saves Tariff Setup Details into Database.
     * @param port
     * @param terminal
     * @param currency
     * @param serviceList
     * @param portList
     * @param slotPartnerList
     * @param custList
     * @param vendorsList
     * @param nonSlotPartnerList     
     * @return 
     * @throws DataAccessException
     */
     public void saveTariffDetail(String line,
                                  String trade,
                                  String agent,
                                  String user,
                                  String port, 
                                  String terminal, 
                                  String currency, 
                                  List<TosNewTariffSetupServiceMod> serviceList,
                                  List<TosNewTariffSetupPortTariffMod> portList,
                                  List<TosNewTariffSetupSlotPartnerMod> slotPartnerList, 
                                  List<TosNewTariffSetupCustomersMod> custList,
                                  List<TosNewTariffSetupVendorsMod> vendorsList,
                                  List<TosNewTariffSetupNonRclPartnerMod> nonSlotPartnerList,
                                  int selectServiceIndx)throws DataAccessException;
}
