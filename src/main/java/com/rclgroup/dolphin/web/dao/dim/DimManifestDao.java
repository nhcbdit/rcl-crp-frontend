/*-----------------------------------------------------------------------------------------------------------  
DimManifestDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 19/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
01 22/06/11 NIP                       BUG 505
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RrcStandardMod;

import com.rclgroup.dolphin.web.model.dim.DimCargoManifestMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface DimManifestDao {
    
    /**
     * 
     * @param serviceCode
     * @param vesselCode
     * @param voyageCode
     * @param direction
     * @param pol
     * @param pot
     * @param pod
     * @param del
     * @param blNo
     * @param hbl
     * @param blStatus
     * @param blId
     * @param consigneeCode
     * @param fsc
     * @return list of DimManifestMod
     * @throws DataAccessException
     */
    public List listForCargoManifest(String serviceCode, String vesselCode, String voyageCode, String direction, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) throws DataAccessException;
    
    /**
     * 
     * @param recFirst
     * @param recLast
     * @param serviceCode
     * @param vesselCode
     * @param voyageCode
     * @param direction
     * @param pol
     * @param pot
     * @param pod
     * @param del
     * @param blNo
     * @param hbl
     * @param blStatus
     * @param blId
     * @param consigneeCode
     * @param fsc
     * @return list of DimManifestMod
     * @throws DataAccessException
     */
    public List listForCargoManifest(int recFirst, int recLast, String serviceCode, String vesselCode, String voyageCode, String direction, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) throws DataAccessException;
    
    /**
     * 
     * @param recFirst
     * @param recLast
     * @param invoyagePort
     * @param sessionId
     * @param pol
     * @param pot
     * @param pod
     * @param del
     * @param blNo
     * @param hbl
     * @param blStatus
     * @param blId
     * @param consigneeCode
     * @param fsc
     * @return list of DimManifestMod
     * @throws DataAccessException
     */
    public List listForCargoManifestByInVoyageList(int recFirst, int recLast, String invoyagePort, String sessionId, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc,String vesselCode) throws DataAccessException;// ##01 add vesselCode
    
    /**
     * 
     * @param serviceCode
     * @param vesselCode
     * @param voyageCode
     * @param direction
     * @param pol
     * @param pot
     * @param pod
     * @param del
     * @param blNo
     * @param hbl
     * @param blStatus
     * @param blId
     * @param consigneeCode
     * @param fsc
     * @return number of lisForCargoManifest
     * @throws DataAccessException
     */
    public int countListForCargoManifest(String serviceCode, String vesselCode, String voyageCode, String direction, 
         String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
         String blStatus, String blId, String consigneeCode, String bookingNo, String fsc) throws DataAccessException;
    
    /**
     * 
     * @param invoyagePort
     * @param sessionId
     * @param pol
     * @param pot
     * @param pod
     * @param del
     * @param blNo
     * @param hbl
     * @param blStatus
     * @param blId
     * @param consigneeCode
     * @param fsc
     * @return number of lisForCargoManifest
     * @throws DataAccessException
     */
    public int countListForCargoManifestByInVoyageList(String invoyagePort, String sessionId, 
        String pol, String pot, String pod, String dischargeTerminal, String del, String blNo, String hbl, 
        String blStatus, String blId, String consigneeCode, String bookingNo, String fsc,String vesselCode) throws DataAccessException;// ##01 add vesselCode
    
    /**
     * 
     * @param service
     * @param vessel
     * @param voyage
     * @param direction
     * @param port
     * @param terminal
     * @param fromDate
     * @param toDate
     * @return list of TosTerminalStorageMod
     * @throws DataAccessException
     */
    public List listForTeminalStorage(String service,String vessel,String voyage,String direction,String port,String terminal,String fromDate,String toDate) throws DataAccessException;                       
    
    /**
     * @return list of DimCargoManifestMod
     * @throws DataAccessException
     */
    public boolean isValidWithSessionId(String sessionId) throws DataAccessException;
   
   /**
     * insert a DIM Cargo Manifest record
     * @param mod a Cargo Manifest model
     * @return whether insertion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:
     *                              error message: DIM_CARGO_MANIFEST_BL_NO_REQ
     *                              error message: DIM_CARGO_MANIFEST_RECORD_ADD_USER_REQ
     *                              error message: ORA-XXXXX (un_exceptional oracle error)
     */
    public boolean insert(RrcStandardMod mod) throws DataAccessException;
   
    /**
     * delete a DIM Cargo Manifest record
     * @param mod a Cargo Manifest model
     * @return wheter deletion is successful
     * @throws DataAccessException exception dwhich client has to catch all following error messages:
     *                              error message: DIM_CARGO_MANIFEST_SESSION_ID_REQ
     *                              error message: DIM_CARGO_MANIFEST_NOT_FOUND 
     *                              error message: ORA-XXXXX (un-exceptional oracle error)
     */
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
}
