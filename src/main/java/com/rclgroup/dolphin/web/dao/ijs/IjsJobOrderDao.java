package com.rclgroup.dolphin.web.dao.ijs;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface IjsJobOrderDao {
    /**
     * check valid of Job Order#
     * @param joNo     
     * @return valid of joNo
     * @throws DataAccessException
     */
    public boolean isValid(String joNo) throws DataAccessException;
        
    /**
     * list Booking records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Job Order
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild) throws DataAccessException;
    
}
