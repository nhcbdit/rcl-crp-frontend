/*-----------------------------------------------------------------------------------------------------------  
CtfPortGroupHeaderDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 25/03/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.ctf;

import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.ctf.CtfPortGroupHeaderMod;

import org.springframework.dao.DataAccessException;


public interface CtfPortGroupHeaderDao extends RriStandardDao {
    /**
     * find port group header model by using port group code,soc or coc, and record status
     * @param portGroupCode port group code
     * @param socCoc String as "S"(SOC) or "C"(COC) 
     * @param recordStatus record status of returned records
     * @return port group header model
     * @throws DataAccessException
     */    
    public CtfPortGroupHeaderMod findByKeyPortGroupCodeSocCoc(String portGroupCode,String socCoc,String recordStatus) throws DataAccessException;
    /**
     * find port group name for particular port group code 
     * @param portGroupCode port group code
     * @return Port group name for port group code 
     * @throws DataAccessException
     */
    public String getDescription(String portGroupCode) throws DataAccessException;
    
}




