/*-----------------------------------------------------------------------------------------------------------  
CamServiceJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/2008
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamServiceMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class CamServiceJdbcDao extends RrcStandardDao implements CamServiceDao {
   
    public CamServiceJdbcDao() {
    }
   
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException {
        System.out.println("[CamServiceDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find,search,wild);
        StringBuffer sql = new StringBuffer();
        sql.append("select SERVICE_CODE ");
        sql.append("    ,SERVICE_NAME ");
        sql.append("    ,RECORD_STATUS ");
        sql.append("from VR_CAM_SERVICE_MASTER ");
        sql.append("where RECORD_STATUS = :status ");
        sql.append(sqlCriteria);            
        sql.append("order by SERVICE_CODE ");
        System.out.println("[CamServiceDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[CamServiceDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   Collections.singletonMap("status", status),
                   new RowModMapper());
    }    
    
    public boolean isValid(String service, String status) throws DataAccessException {
        System.out.println("[CamServiceDao][isValid]: Started");
        StringBuffer sql = new StringBuffer();
        boolean isValid = false;
        sql.append("select SERVICE_CODE ");
        sql.append("from VR_CAM_SERVICE_MASTER ");
        sql.append("where RECORD_STATUS = :status "); 
        sql.append("    and SERVICE_CODE = :service ");
        System.out.println("[CamServiceDao][isValid]: SQL: " + sql.toString());
        System.out.println("[CamServiceDao][isValid]: Finished");
        HashMap map = new HashMap();
        map.put("service", service);
        map.put("status", status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(), map);
        if (rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        return isValid;
    }    

    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "like '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("C")) {
                sqlCriteria = "and SERVICE_CODE " + sqlWild;
            } else if (search.equalsIgnoreCase("N")) {
                sqlCriteria = "and SERVICE_NAME " + sqlWild;
            } else if (search.equalsIgnoreCase("S")) {
                sqlCriteria = "and RECORD_STATUS " + sqlWild;
            }
        }
        return sqlCriteria;
    }
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    private CamServiceMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        CamServiceMod mod = new CamServiceMod();
        mod.setServiceCode(RutString.nullToStr(rs.getString("SERVICE_CODE")));
        mod.setServiceName(RutString.nullToStr(rs.getString("SERVICE_NAME")));
        mod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
        return mod;
    }    
}



