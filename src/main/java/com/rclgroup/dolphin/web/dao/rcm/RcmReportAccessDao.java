package com.rclgroup.dolphin.web.dao.rcm;

import com.rclgroup.dolphin.web.model.rcm.RcmReportAccessMod;

import org.springframework.dao.DataAccessException;

public interface RcmReportAccessDao {

    public boolean updateReportAccess(RcmReportAccessMod mod) throws DataAccessException;

}
