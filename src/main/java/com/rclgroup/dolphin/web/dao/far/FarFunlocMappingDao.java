/*-----------------------------------------------------------------------------------------------------------  
FarFunlocMappingDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 01/07/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.model.far.FarFunlocMod;

import java.sql.Timestamp;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface FarFunlocMappingDao {
     /**
      * number of list of B/L
      * @param bl_no 
      * @param billToParty
      * @param inv_no
      * @param funloc      
      * @param status
      * @return list of B/L model(FarFunlocMod)
      * @throws DataAccessException
      */
     public int countListForStatement(String bl_no,String billToParty,String inv_no,String funloc,String status) throws DataAccessException;
     
     /**
      * list B/L for search screen
      * @param bl_no 
      * @param billToParty
      * @param inv_no
      * @param funloc      
      * @param status
      * @param sortBy
      * @param sortByIn
      * @return list of B/L model(invoiceBillStatementMod)
      * @throws DataAccessException
      */
     public List listForStatementByHeader(String bl_no,String billToParty,String inv_no,String funloc,String status,String sortBy, String sortByIn) throws DataAccessException;
     
     /**
      * list billing statement by header records for search screen
      * @param rowAt
      * @param rowTo
      * @param bl_no 
      * @param billToParty 
      * @param inv_no
      * @param funloc      
      * @param status
      * @param sortBy
      * @param sortByIn
      * @return list of billing statement model(invoiceBillStatementMod)
      * @throws DataAccessException
      */
     public List listForStatement(int rowAt, int rowTo, String bl_no,String billToParty,String inv_no,String funloc,String status,String sortBy, String sortByIn) throws DataAccessException;
        
     /**
      * update a funloc record 
      * @param mod a FunLoc model as input and output
      * @return whether updating is successful
      * @throws DataAccessException exceptin which client has to catch all following error messages:
      *                              error message: BILL_TO_PARTY_REQ
      *                              error message: BL_REQ
      *                              error message: INV_NO_REQ
      *                              error message: DATA_NOT_FOUND      
      *                              error message: ORA-XXXXX (un-exceptional oracle error)  
      */
     public boolean updateFunLoc (RrcStandardMod mod) throws DataAccessException;
     
    /**
     * delete a funloc record 
     * @param mod a FunLoc model as input and output
     * @return whether delete is successful
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: PK_DATA_MAP_ID_REQ     
     *                              error message: DATA_NOT_FOUND      
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public boolean deleteFunLoc (RrcStandardMod mod) throws DataAccessException;
     
     /**
      * insert a funloc record 
      * @param mod a FunLoc model as input and output
      * @return whether insert is successful
      * @throws DataAccessException exceptin which client has to catch all following error messages:
      *                              error message: BILL_TO_PARTY_REQ
      *                              error message: INV_NO_REQ
      *                              error message: DATA_NOT_FOUND      
      *                              error message: ORA-XXXXX (un-exceptional oracle error)  
      */
     public boolean insertFunLoc (RrcStandardMod mod) throws DataAccessException;
     
}
