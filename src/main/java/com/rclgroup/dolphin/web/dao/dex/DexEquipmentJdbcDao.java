/*-----------------------------------------------------------------------------------------------------------  
DexEquipmentJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 20/05/2009
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY   -User-     -TaskRef-      -Short Description  
#01 24/06/2009  WAC                       added isValid method,listForHelpScreenByFsc
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dex.DexEquipmentMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class DexEquipmentJdbcDao extends RrcStandardDao implements DexEquipmentDao {
    private DexEquipmentDao dexEquipmentDao;

    public DexEquipmentJdbcDao() {
        super();
    }

    public void setEmsEquipmentSysValidateDao(DexEquipmentDao dexEquipmentDao) {
        this.dexEquipmentDao = dexEquipmentDao;
    }

    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List listForHelpScreen(String find, String search, String wild, String status) throws DataAccessException{
        System.out.println("[DexEquipmentJdbcDao][listForHelpScreen]: Started");
        
        status = (status==null) ? "" : status.trim();
        String sqlCriteria = createSqlHelpCriteria(find, search, wild);
        
        StringBuffer sql = new StringBuffer();
        sql.append("select EQUIPMENT_NO ");    
        sql.append("    ,CATEGORY ");
        sql.append("    ,EQUIPMENT_SIZE ");
        sql.append("    ,EQUIPMENT_TYPE ");
        sql.append("    ,ACTIVITY ");
        sql.append("    ,CURRENT_ACTIVITY_DATE ");
        sql.append("    ,CURRENT_PORT ");
        sql.append("    ,CURRENT_STATUS ");
        sql.append("from VR_DEX_EQUIPMENT ");
        sql.append("where CURRENT_STATUS = :status ");
        sql.append(sqlCriteria);
        sql.append("order by EQUIPMENT_NO");
        
        HashMap map = new HashMap();
        map.put("status", status);
        
        System.out.println("[DexEquipmentJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[DexEquipmentJdbcDao][listForHelpScreen]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            DexEquipmentMod bean = new DexEquipmentMod();
                            bean.setEquipmentNo(RutString.nullToStr(rs.getString("EQUIPMENT_NO")));
                            bean.setCategory(RutString.nullToStr(rs.getString("CATEGORY")));
                            bean.setEquipmentSize(RutString.nullToStr(rs.getString("EQUIPMENT_SIZE")));
                            bean.setEquipmentType(RutString.nullToStr(rs.getString("EQUIPMENT_TYPE")));
                            bean.setActivity(RutString.nullToStr(rs.getString("ACTIVITY")));
                            bean.setActivityDate(RutString.nullToStr(rs.getString("CURRENT_ACTIVITY_DATE")));
                            bean.setPort(RutString.nullToStr(rs.getString("CURRENT_PORT")));
                            bean.setStatus(RutString.nullToStr(rs.getString("CURRENT_STATUS")));
                            return bean;
                        }
                    });
    }
    
    private String createSqlHelpCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if (wild.equalsIgnoreCase("ON")) {
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        } else {
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if (find.trim().length() == 0) {
        } else {
            if (search.equalsIgnoreCase("ENO")) {
                sqlCriteria = " and EQUIPMENT_NO " + sqlWild;
            } else if (search.equalsIgnoreCase("CAT")) {
                sqlCriteria = " and CATEGORY " + sqlWild;
            } else if (search.equalsIgnoreCase("ESZ")) {
                sqlCriteria = " and EQUIPMENT_SIZE " + sqlWild;
            } else if (search.equalsIgnoreCase("ETP")) {
                sqlCriteria = " and EQUIPMENT_TYPE " + sqlWild;
            } else if (search.equalsIgnoreCase("ACT")) {
                sqlCriteria = " and ACTIVITY " + sqlWild;
            } else if (search.equalsIgnoreCase("POR")) {
                sqlCriteria = " and CURRENT_PORT " + sqlWild;
            }
        }

        return sqlCriteria;
    }
    
// Begin #01    
    public boolean isValid(String equipmentNo) throws DataAccessException {
        System.out.println("[DexEquipmentJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
         
        StringBuffer sql = new StringBuffer();
        
        sql.append("SELECT EQUIPMENT_NO");        
        sql.append(" FROM VR_DEX_EQUIPMENT");
        sql.append(" WHERE  1=1       ");  
        if((equipmentNo!=null)&&(!equipmentNo.trim().equals(""))){
            sql.append("  AND UPPER(EQUIPMENT_NO) = :equipmentNo ");
        }        
        HashMap map = new HashMap();        
        map.put("equipmentNo",equipmentNo.toUpperCase());
        System.out.println("[DexEquipmentJdbcDao][isValid]: SQL: " + sql.toString());
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) { 
            isValid = true;
        } else { 
            isValid = false;
        }        
        System.out.println("[DexEquipmentJdbcDao][isValid]: With status: Finished");
        return isValid;
    }
    public List listForHelpScreenByFsc(String find, String search, String wild, String status,String line,String trade,String agent,String fsc) throws DataAccessException{
        System.out.println("[DexEquipmentJdbcDao][listForHelpScreen]: Started");
        
        status = (status==null) ? "" : status.trim();
        String sqlCriteria = createSqlHelpCriteria(find, search, wild);
        
        StringBuffer sql = new StringBuffer();
        sql.append("select EQUIPMENT_NO ");    
        sql.append("    ,CATEGORY ");
        sql.append("    ,EQUIPMENT_SIZE ");
        sql.append("    ,EQUIPMENT_TYPE ");
        sql.append("    ,ACTIVITY ");
        sql.append("    ,CURRENT_ACTIVITY_DATE ");
        sql.append("    ,CURRENT_PORT ");
        sql.append("    ,CURRENT_STATUS ");
        sql.append("from VR_DEX_EQUIPMENT ");
        sql.append("where CURRENT_STATUS = :status ");
        sql.append(" AND NVL(COLL_LINE,' ') = NVL(:line,NVL(COLL_LINE,' '))");
        sql.append(" AND NVL(COLL_TRADE,' ') = NVL(:trade,NVL(COLL_TRADE,' '))");
        sql.append(" AND NVL(COLL_AGENT,' ') = NVL(:agent,nvl(COLL_AGENT,' '))");
        
        sql.append(sqlCriteria);
        sql.append("order by EQUIPMENT_NO");
        
        HashMap map = new HashMap();
        map.put("status", status);
        
        System.out.println("[DexEquipmentJdbcDao][listForHelpScreen]: SQL: " + sql.toString());
        System.out.println("[DexEquipmentJdbcDao][listForHelpScreen]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                            DexEquipmentMod bean = new DexEquipmentMod();
                            bean.setEquipmentNo(RutString.nullToStr(rs.getString("EQUIPMENT_NO")));
                            bean.setCategory(RutString.nullToStr(rs.getString("CATEGORY")));
                            bean.setEquipmentSize(RutString.nullToStr(rs.getString("EQUIPMENT_SIZE")));
                            bean.setEquipmentType(RutString.nullToStr(rs.getString("EQUIPMENT_TYPE")));
                            bean.setActivity(RutString.nullToStr(rs.getString("ACTIVITY")));
                            bean.setActivityDate(RutString.nullToStr(rs.getString("CURRENT_ACTIVITY_DATE")));
                            bean.setPort(RutString.nullToStr(rs.getString("CURRENT_PORT")));
                            bean.setStatus(RutString.nullToStr(rs.getString("CURRENT_STATUS")));
                            return bean;
                        }
                    });
    }    
//End #01   
     public List listForNewHelpScreen(int rowAt,int rowTo,String find, String search, String wild, String status) throws DataAccessException{
         System.out.println("[DexEquipmentJdbcDao][listForNewHelpScreen]: Started");
         
         status = (status==null) ? "" : status.trim();
         String sqlCriteria = createSqlHelpCriteria(find, search, wild);
         
         StringBuffer sql = new StringBuffer();
         String selectColumn = "EQUIPMENT_NO , CATEGORY , EQUIPMENT_SIZE , EQUIPMENT_TYPE, ACTIVITY , CURRENT_ACTIVITY_DATE , CURRENT_PORT , CURRENT_STATUS ";
         sql.append("select "+selectColumn);
         sql.append("from VR_DEX_EQUIPMENT ");
         sql.append("where CURRENT_STATUS = :status ");
         sql.append(sqlCriteria);
         sql.append("order by EQUIPMENT_NO");
         sql = addSqlForNewHelp(sql,rowAt,rowTo,selectColumn);
         
         HashMap map = new HashMap();
         map.put("status", status);
         
         System.out.println("[DexEquipmentJdbcDao][listForNewHelpScreen]: SQL: " + sql.toString());
         System.out.println("[DexEquipmentJdbcDao][listForNewHelpScreen]: Finished");
         
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
                    new RowMapper(){
                         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                             DexEquipmentMod bean = new DexEquipmentMod();
                             bean.setEquipmentNo(RutString.nullToStr(rs.getString("EQUIPMENT_NO")));
                             bean.setCategory(RutString.nullToStr(rs.getString("CATEGORY")));
                             bean.setEquipmentSize(RutString.nullToStr(rs.getString("EQUIPMENT_SIZE")));
                             bean.setEquipmentType(RutString.nullToStr(rs.getString("EQUIPMENT_TYPE")));
                             bean.setActivity(RutString.nullToStr(rs.getString("ACTIVITY")));
                             bean.setActivityDate(RutString.nullToStr(rs.getString("CURRENT_ACTIVITY_DATE")));
                             bean.setPort(RutString.nullToStr(rs.getString("CURRENT_PORT")));
                             bean.setStatus(RutString.nullToStr(rs.getString("CURRENT_STATUS")));
                             return bean;
                         }
                     });
     }
    public int countListForNewHelpScreen(String find, String search, String wild , String status) throws DataAccessException{
        String sqlCriteria = createSqlHelpCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("select EQUIPMENT_NO ");    
        sql.append("    ,CATEGORY ");
        sql.append("    ,EQUIPMENT_SIZE ");
        sql.append("    ,EQUIPMENT_TYPE ");
        sql.append("    ,ACTIVITY ");
        sql.append("    ,CURRENT_ACTIVITY_DATE ");
        sql.append("    ,CURRENT_PORT ");
        sql.append("    ,CURRENT_STATUS ");
        sql.append("from VR_DEX_EQUIPMENT ");
        sql.append("where CURRENT_STATUS = :status ");
        sql.append(sqlCriteria);
        sql.append("order by EQUIPMENT_NO");

        sql = getNumberOfAllData(sql);

        HashMap map = new HashMap();                
        map.put("status", status);

        Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sql.toString(),map, Integer.class);
        return result;
    }     

}
