/*-----------------------------------------------------------------------------------------------------------  
DimArrivalNoticeJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Wuttitorn Wuttijiaranai 29/12/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.dim;

import com.rclgroup.dolphin.web.common.RcmConstant;
import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.dim.DimArrivalNoticeMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;


public class DimArrivalNoticeJdbcDao extends RrcStandardDao implements DimArrivalNoticeDao{
    
    public DimArrivalNoticeJdbcDao() {
        super();
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    //begin: Arrival Notice
    public String makeArrivalNoticeSqlStatment(String criteriaBy, String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) {
        StringBuffer sb = new StringBuffer();  
        
        if ("CRITERIA_BY_VSL".equals(criteriaBy)) {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");    
            sb.append("    ,POD_TERMINAL ");
        } else {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");
            sb.append("    ,POD_TERMINAL ");
        }
        sb.append("from VR_DIM_ARRIVAL_NOTICE vr ");
           
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(service)) {
            sbWhere.append(" and SERVICE = '"+service+"' ");
        }
        if (!RutString.isEmptyString(vessel)) {
            sbWhere.append(" and VESSEL = '"+vessel+"' ");
        }
        if (!RutString.isEmptyString(voyage)) {
            sbWhere.append(" and VOYAGE = '"+voyage+"' ");
        }
        if (!RutString.isEmptyString(direction)) {
            sbWhere.append(" and DIRECTION = '"+direction+"' ");
        }
        if (!RutString.isEmptyString(pol)) {
            sbWhere.append(" and POL = '"+pol+"' ");
        }
        if (!RutString.isEmptyString(pod)) {
            sbWhere.append(" and POD = '"+pod+"' ");
        }
        if (!RutString.isEmptyString(podTerminal)) {
            sbWhere.append(" and POD_TERMINAL = '"+podTerminal+"' ");
        }
        if (!RutString.isEmptyString(blId)) {
            sbWhere.append(" and BL_ID = '"+blId+"' ");
        }
        if (!RutString.isEmptyString(cocSoc)) {
            sbWhere.append(" and COC_SOC = '"+cocSoc+"' ");
        }
        if (!RutString.isEmptyString(permissionUser)) {
            sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
            sb.append(" [and :columnName :conditionWild :columnFind] ");
        } else {
            sb.append("[where :columnName :conditionWild :columnFind] ");
        }
        
        sb.append("[order by :sortBy :sortIn] ");
        return sb.toString();
    }
    
    public int findCountForArrivalNoticeByVesselVoyageList(String columnName, String conditionWild, String columnFind
        ,String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimArrivalNoticeJdbcDao][findCountForArrivalNoticeByVesselVoyageList]: Started");
        
        String sqlStatement = this.makeArrivalNoticeSqlStatment("CRITERIA_BY_VSL", service, vessel, voyage, direction, pol, pod, podTerminal, blId, cocSoc, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, "", "");
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[DimArrivalNoticeJdbcDao][findCountForArrivalNoticeByVesselVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimArrivalNoticeJdbcDao][findCountForArrivalNoticeByVesselVoyageList]: Finished");
        
		
		Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sqlStatement,new HashMap(), Integer.class);
		return result;
    }
    
    public String makeArrivalNoticeSqlStatment(String criteriaBy, String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException {
        StringBuffer sb = new StringBuffer();  
        
        if ("CRITERIA_BY_VSL".equals(criteriaBy)) {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");    
            sb.append("    ,POD_TERMINAL ");
        } else {
            sb.append("select distinct VESSEL ");
            sb.append("    ,VOYAGE ");
            sb.append("    ,POL ");
            sb.append("    ,POD ");
            sb.append("    ,POD_TERMINAL ");
        }
        sb.append("from VR_DIM_DELIVERY_ORDER_PRINT vr ");
           
        StringBuffer sbWhere = new StringBuffer();
        if (!RutString.isEmptyString(sessionId) && !RutString.isEmptyString(invoyagePort)) {
            sbWhere.append(" and exists ( select 1 ");
            sbWhere.append("    from IDP005 i05 ");
            sbWhere.append("        ,VR_RCM_INVOYAGE_BROWSER ib ");
            sbWhere.append("    where i05.SYBLNO = vr.BL_NO ");
            sbWhere.append("        and i05.SERVICE = ib.SERVICE ");
            sbWhere.append("        and i05.VESSEL = ib.VESSEL ");
            sbWhere.append("        and i05.VOYAGE = ib.VOYAGE ");
            sbWhere.append("        and i05.LOAD_PORT = ib.PORT ");
            sbWhere.append("        and i05.DISCHARGE_PORT = '"+invoyagePort+"' ");
            sbWhere.append("        and ib.SESSION_ID = '"+sessionId+"' ) ");
        }
        if (!RutString.isEmptyString(pol)) {
            sbWhere.append(" and POL = '"+pol+"' ");
        }
        if (!RutString.isEmptyString(pod)) {
            sbWhere.append(" and POD = '"+pod+"' ");
        }
        if (!RutString.isEmptyString(podTerminal)) {
            sbWhere.append(" and POD_TERMINAL = '"+podTerminal+"' ");
        }
        if (!RutString.isEmptyString(blId)) {
            sbWhere.append(" and BL_ID = '"+blId+"' ");
        }
        if (!RutString.isEmptyString(cocSoc)) {
            sbWhere.append(" and COC_SOC = '"+cocSoc+"' ");
        }
        if (!RutString.isEmptyString(permissionUser)) {
            sbWhere.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }
        
        if (sbWhere != null && sbWhere.length() > 0) {
            String sqlWhere = "where " + sbWhere.substring(4);
            sb.append(sqlWhere);
            sb.append(" [and :columnName :conditionWild :columnFind] ");
        } else {
            sb.append("[where :columnName :conditionWild :columnFind] ");
        }
        
        sb.append("[order by :sortBy :sortIn] ");
        return sb.toString();
    }
    
    public int findCountForArrivalNoticeByInVoyageList(String columnName, String conditionWild, String columnFind 
        ,String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimArrivalNoticeJdbcDao][findCountForArrivalNoticeByInVoyageList]: Started");
        
        String sqlStatement = this.makeArrivalNoticeSqlStatment("CRITERIA_BY_VSL", invoyagePort, sessionId, pol, pod, podTerminal, blId, cocSoc, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, "", "");
        sqlStatement = RutDatabase.optimizeCountRecordSQL(sqlStatement);
        
        System.out.println("[DimArrivalNoticeJdbcDao][findCountForArrivalNoticeByInVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimArrivalNoticeJdbcDao][findCountForArrivalNoticeByInVoyageList]: Finished");
        
		
		Integer result = (Integer) getNamedParameterJdbcTemplate().queryForObject(sqlStatement,new HashMap(), Integer.class);
		return result;
    }
    
    public List findArrivalNoticeByVesselVoyageList(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortIn
        ,String service, String vessel, String voyage, String direction, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByVesselVoyageList]: Started");
        
        String sqlStatement = this.makeArrivalNoticeSqlStatment("CRITERIA_BY_VSL", service, vessel, voyage, direction, pol, pod, podTerminal, blId, cocSoc, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, sortBy, sortIn);
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByVesselVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByVesselVoyageList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimArrivalNoticeMod bean = new DimArrivalNoticeMod();
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setPol(RutString.nullToStr(rs.getString("POL")));
                           bean.setPod(RutString.nullToStr(rs.getString("POD")));
                           bean.setPodTerminal(RutString.nullToStr(rs.getString("POD_TERMINAL")));
                           return bean;
                       }
                   });
    }
    
    public List findArrivalNoticeByInVoyageList(int recFirst, int recLast, String columnName, String conditionWild, String columnFind, String sortBy, String sortIn
        ,String invoyagePort, String sessionId, String pol, String pod, String podTerminal, String blId, String cocSoc, String permissionUser) throws DataAccessException 
    {
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByInVoyageList]: Started");
        
        String sqlStatement = this.makeArrivalNoticeSqlStatment("CRITERIA_BY_VSL", invoyagePort, sessionId, pol, pod, podTerminal, blId, cocSoc, permissionUser);
        sqlStatement = RutDatabase.makeSqlStatementByParameter(sqlStatement, columnName, columnFind, conditionWild, sortBy, sortIn);
        sqlStatement = RutDatabase.optimizeSQL(sqlStatement, recFirst, recLast);
        
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByInVoyageList]: sql = "+sqlStatement);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByInVoyageList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                new HashMap(),
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimArrivalNoticeMod bean = new DimArrivalNoticeMod();
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setPol(RutString.nullToStr(rs.getString("POL")));
                           bean.setPod(RutString.nullToStr(rs.getString("POD")));
                           bean.setPodTerminal(RutString.nullToStr(rs.getString("POD_TERMINAL")));
                           return bean;
                       }
                   });
    }
    
    public List findArrivalNoticeByBlList(String columnName, String conditionWild, String columnFind, String vessel, String voyage, String pol, String pod, String podTerminal, String permissionUser) {
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: Started");
        
        StringBuffer sb = new StringBuffer();  
        sb.append("select SERVICE ");
        sb.append(" ,VESSEL ");
        sb.append(" ,VOYAGE ");
        sb.append(" ,DIRECTION ");
        sb.append(" ,POL ");
        sb.append(" ,POT ");
        sb.append(" ,POD ");    
        sb.append(" ,POD_TERMINAL ");
        sb.append(" ,BL_ID ");
        sb.append(" ,BL_TYPE ");
        sb.append(" ,BL_NO ");
        sb.append(" ,HBL_NO ");
        sb.append(" ,BL_CREATE_BY ");
        sb.append(" ,COC_SOC ");
        sb.append(" ,IN_STATUS ");
        sb.append(" ,OUT_STATUS ");
        sb.append(" ,SHIP_TYPE ");
        sb.append(" ,REMARKS ");
        sb.append("from VR_DIM_ARRIVAL_NOTICE ");
        sb.append("where VESSEL = :vessel ");
        sb.append(" and VOYAGE = :voyage ");
        sb.append(" and POL = :pol ");
        sb.append(" and POD = :pod ");
        sb.append(" and POD_TERMINAL = :podTerminal ");
        if (!RutString.isEmptyString(permissionUser)) {
            sb.append(" and PCR_RUT_UTILITY.FR_CHK_USR_PERM_PORT('"+permissionUser+"', POD) = '"+RcmConstant.FLAG_YES+"' ");
        }
        if (!RutString.isEmptyString(columnName) && !RutString.isEmptyString(columnFind)) {
            if (RcmConstant.WILD_DEFAULT.equals(conditionWild)) {
                sb.append(" and "+columnName+" like '%"+columnFind+"%' ");
            } else {
                sb.append(" and "+columnName+" = '"+columnFind+"' ");
            }
        }
        
        sb.append("order by REMARKS desc ");
        sb.append(" ,BL_NO ");
        
        HashMap map = new HashMap();
        map.put("vessel", vessel);
        map.put("voyage", voyage);
        map.put("pol", pol);
        map.put("pod", pod);
        map.put("podTerminal", podTerminal);
        
        String sqlStatement = sb.toString();
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: sql = "+sqlStatement);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = vessel:"+vessel);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = voyage:"+voyage);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = pol:"+pol);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = pod:"+pod);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: parameter = podTerminal:"+podTerminal);
        System.out.println("[DimArrivalNoticeJdbcDao][findArrivalNoticeByBlList]: Finished");
        
        return getNamedParameterJdbcTemplate().query(
                sqlStatement,
                map,
                new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           DimArrivalNoticeMod bean = new DimArrivalNoticeMod();
                           bean.setService(RutString.nullToStr(rs.getString("SERVICE")));
                           bean.setVessel(RutString.nullToStr(rs.getString("VESSEL")));
                           bean.setVoyage(RutString.nullToStr(rs.getString("VOYAGE")));
                           bean.setDirection(RutString.nullToStr(rs.getString("DIRECTION")));
                           bean.setPol(RutString.nullToStr(rs.getString("POL")));
                           bean.setPot(RutString.nullToStr(rs.getString("POT")));
                           bean.setPod(RutString.nullToStr(rs.getString("POD")));
                           bean.setPodTerminal(RutString.nullToStr(rs.getString("POD_TERMINAL")));
                           bean.setBlId(RutString.nullToStr(rs.getString("BL_ID")));
                           bean.setBlType(RutString.nullToStr(rs.getString("BL_TYPE")));
                           bean.setBlNo(RutString.nullToStr(rs.getString("BL_NO")));
                           bean.setHblNo(RutString.nullToStr(rs.getString("HBL_NO")));
                           bean.setBlCreateBy(RutString.nullToStr(rs.getString("BL_CREATE_BY")));
                           bean.setCocSoc(RutString.nullToStr(rs.getString("COC_SOC")));
                           bean.setInStatus(RutString.nullToStr(rs.getString("IN_STATUS")));
                           bean.setOutStatus(RutString.nullToStr(rs.getString("OUT_STATUS")));
                           bean.setShipType(RutString.nullToStr(rs.getString("SHIP_TYPE")));
                           bean.setRemarks(RutString.nullToStr(rs.getString("REMARKS")));
                           return bean;
                       }
                   });
    }
    //end: Arrival Notice
    
}
