package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface BsaVsaPortCallDao extends RriStandardDao{

//    listPortCallFindByKeyBsaServiceVariantID
 /**
  * find VSA Port Call of voyage header ID
  * @param vsaVoyageHeaderID
  * @return
  * @throws DataAccessException
  */
    public List listPortCallByKeyVsaVoyageHeaderID(String vsaVoyageHeaderID, String startingVessel) throws DataAccessException;
 
 
    
    public boolean update(RrcStandardMod mod) throws DataAccessException;
 

 
  
    
}
