/*-----------------------------------------------------------------------------------------------------------  
VssProformaMasterDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 13/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.vss;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;
import com.rclgroup.dolphin.web.model.vss.VssProformaMasterMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface VssProformaMasterDao extends RriStandardDao {

    /**
     * list service proforma records for help screen with status
     * @param find
     * @param search
     * @param wild
     * @param status
     * @param serviceCode
     * @return list of service proformas with status
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild, String status, String serviceCode) throws DataAccessException;
    
    /**
     * list all voyage proforma records for search screen
     * @param searchMod all search criterias in one model
     * @return list of voyage proformas(VssProformaMasterMod)
     * @throws DataAccessException
     */
    public List listForSearchScreen(RcmSearchMod searchMod,String validAt) throws DataAccessException;

    /**
     * find voyage proforma by using key as VSS proforma ID 
     * @param vssProformaId VSS proforma ID 
     * @return VSS proforma master model
     * @throws DataAccessException
     */
    public VssProformaMasterMod findByKeyVssProformaId(String vssProformaId) throws DataAccessException;
    
    /**
     * find voyage proforma by using VSS proforma NO 
     * @param proformaNo VSS proforma NO 
     * @return VSS proforma master model
     * @throws DataAccessException
     */
    public VssProformaMasterMod findByProformaNo(String proformaNo) throws DataAccessException;

    /**
     * insert a proforma master record
     * @param mod a VSS proforma master model as input and output
     * @return whether insertion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: VSS_PMZ01_VSS_PROFORMA_ID_REQ
     *                             error message: VSS_PMZ01_SERVICE_REQ 
     *                             error message: VSS_PMZ01_SERVICE_NOT_EXIST with one argument: service code from input value  
     *                             error message: VSS_PMZ01_PROFORMA_REF_NO_REQ 
     *                             error message: VSS_PMZ01_PROFORMA_REF_NO_NOT_EXIST with two arguments: service and proforma reference no. from input value
     *                             error message: VSS_PMZ01_DESIGNED_NO_OF_VESSELS_REQ
     *                             error message: VSS_PMZ01_DESIGNED_NO_OF_VESSELS_INVALID
     *                             error message: VSS_PMZ01_VALID_FROM_REQ 
     *                             error message: VSS_PMZ01_VALID_TO_REQ 
     *                             error message: VSS_PMZ01_VALID_DATE_NOT_IN_RANGE   
     *                             error message: VSS_PMZ01_RECORD_STATUS_REQ 
     *                             error message: VSS_PMZ01_STATUS_NOT_IN_RANGE with one argument: record status from input value  
     *                             error message: VSS_PMZ01_RECORD_ADD_USER_REQ
     *                             error message: VSS_PMZ01_SERVICE_PROFORMA_EXIST
     *                             error message: VSS_PMZ01_VALUE_DUP with one argument: primary key from input value  
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
     public boolean insert(RrcStandardMod mod) throws DataAccessException;

    /**
     * update a proforma master record
     * @param mod a VSS proforma master model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: VSS_PMZ01_VSS_PROFORMA_ID_REQ
     *                             error message: VSS_PMZ01_SERVICE_REQ
     *                             error message: VSS_PMZ01_SERVICE_NOT_EXIST with one argument: service code from input value  
     *                             error message: VSS_PMZ01_PROFORMA_REF_NO_REQ
     *                             error message: VSS_PMZ01_PROFORMA_REF_NO_NOT_EXIST with two arguments: service and proforma reference no. from input value   
     *                             error message: VSS_PMZ01_DESIGNED_NO_OF_VESSELS_REQ
     *                             error message: VSS_PMZ01_DESIGNED_NO_OF_VESSELS_INVALID
     *                             error message: VSS_PMZ01_VALID_FROM_REQ
     *                             error message: VSS_PMZ01_VALID_TO_REQ
     *                             error message: VSS_PMZ01_VALID_DATE_NOT_IN_RANGE 
     *                             error message: VSS_PMZ01_RECORD_STATUS_REQ
     *                             error message: VSS_PMZ01_STATUS_NOT_IN_RANGE with one argument: record status from input value    
     *                             error message: VSS_PMZ01_RECORD_CHANGE_USER_REQ
     *                             error message: VSS_PMZ01_RECORD_CHANGE_DATE_REQ
     *                             error message: VSS_PMZ01_UPDATE_CON
     *                             error message: VSS_PMZ01_PROFORMA_MASTER_NOT_FOUND with primary key from input value 
     *                             error message: VSS_PMZ01_SERVICE_PROFORMA_EXIST 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean update(RrcStandardMod mod) throws DataAccessException;

    /**
     * delete a proforma master record
     * @param mod a VSS proforma master model
     * @return whether deletion is successful
     * @throws DataAccessException exception which client has to catch all following error messages:  
     *                             error message: VSS_PMZ01_RECORD_CHANGE_DATE_REQ 
     *                             error message: VSS_PMZ01_DELETE_CON  
     *                             error message: VSS_PMZ01_PROFORMA_MASTER_NOT_FOUND with primary key from input value 
     *                             error message: ORA-XXXXX (un-exceptional oracle error)
     */ 
    public boolean delete(RrcStandardMod mod) throws DataAccessException;
   
    /**
     * save a proforma master record with its details in VSS proforma vessel assignment
     * @param masterMod a VSS proforma master model
     * @param masterOperationFlag a operation flag for a VSS proforma master model
     * @param detailMods detail models as a vector of VSS proforma vessel assignment models
     * @param detailOperationFlags operation flags as operation flags for detail models 
     */ 
    public void saveMasterDetails(RrcStandardMod masterMod,String masterOperationFlag, List dtlModifiedMods) throws DataAccessException; 
    
}



