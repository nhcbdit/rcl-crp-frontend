package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamPeriodParameterMod;
import com.rclgroup.dolphin.web.model.cam.CamRseServiceParameterGroupMod;
import com.rclgroup.dolphin.web.util.RutDate;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class CamPeriodParameterHelperJdbcDao {
    public CamPeriodParameterHelperJdbcDao() {
    }
    
    public List<CamPeriodParameterMod> getList(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String periodParameterCode,String noOfDay,String status,String sortBy,String sortIn) throws DataAccessException{
        StringBuffer sb = new StringBuffer(" SELECT ");  
        sb.append(" PK_CAM_PERIOD_PARAMETER_ID  ");
        sb.append(" ,PERIOD_PARAMETER_CODE  ");
        sb.append(" ,NO_OF_DAY ");
        sb.append(" from CAM_PERIOD_PARAMETER ");
        sb.append(" where 1=1 ");
        
        if(!RutString.isEmptyString(periodParameterCode)){
            sb.append(" AND UPPER(PERIOD_PARAMETER_CODE) LIKE '%"+periodParameterCode.toUpperCase()+"%' ");
        }
        
        if(!RutString.isEmptyString(noOfDay)){
            sb.append(" AND NO_OF_DAY LIKE '"+noOfDay+"%' ");
        }
        
        if(!RutString.isEmptyString(status)){
            sb.append(" AND RECORD_STATUS = '"+status+"' ");
        }
        
        if(RutString.isEmptyString(sortBy)){
            sortBy = "PERIOD_PARAMETER_CODE";
        }
        
        sb.append(" ORDER BY "+sortBy+" "+sortIn);
        
        System.out.println("[CamPeriodParameterHelperJdbcDao][getList]: sql = " + sb.toString());
        
      
        return namedParameterJdbcTemplate.query(
                sb.toString(),
                new HashMap(),
                new RowMapper(){
                       public CamPeriodParameterMod mapRow(ResultSet rs, int rowNum) throws SQLException {
                           CamPeriodParameterMod bean = new CamPeriodParameterMod();
                           
                           bean.setPeriodParameterId(RutString.nullToStr(rs.getString("PK_CAM_PERIOD_PARAMETER_ID")));
                           bean.setPeriodParameterCode(RutString.nullToStr(rs.getString("PERIOD_PARAMETER_CODE")));
                           bean.setNoOfDay(RutString.nullToStr(rs.getString("NO_OF_DAY")));
                         
                           return bean;
                       }
                   });
    }
    
    public List<CamPeriodParameterMod> getItem(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String periodParameterId) throws DataAccessException{
        StringBuffer sb = new StringBuffer("  SELECT   "); 
                                sb.append("       PK_CAM_PERIOD_PARAMETER_ID     ");
                                sb.append("       , PERIOD_PARAMETER_CODE        ");
                                sb.append("       , NO_OF_DAY        ");
                                sb.append("       , RECORD_STATUS       ");
                                sb.append("       , RECORD_ADD_USER     ");
                                sb.append("       , RECORD_ADD_DATE     ");
                                sb.append("       , RECORD_CHANGE_USER  ");
                                sb.append("       , RECORD_CHANGE_DATE  ");
                                sb.append("  FROM CAM_PERIOD_PARAMETER         ");
                                sb.append("  WHERE PK_CAM_PERIOD_PARAMETER_ID = :periodParameterId  ");

        System.out.println("[CamPeriodParameterHelperJdbcDao][getList]: periodParameterCode = " + periodParameterId);
        System.out.println("[CamPeriodParameterHelperJdbcDao][getList]: sql = " + sb.toString());
        
        return namedParameterJdbcTemplate.query(
                sb.toString(),
                Collections.singletonMap("periodParameterId", periodParameterId),
                new RowMapper(){
                       public CamPeriodParameterMod mapRow(ResultSet rs, int rowNum) throws SQLException {
                       
                           CamPeriodParameterMod bean = new CamPeriodParameterMod();
                           
                           bean.setPeriodParameterId(rs.getString("PK_CAM_PERIOD_PARAMETER_ID"));
                           bean.setPeriodParameterCode(RutString.nullToStr(rs.getString("PERIOD_PARAMETER_CODE")));
                           bean.setNoOfDay(RutString.nullToStr(rs.getString("NO_OF_DAY")));
                           bean.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
                           bean.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
                           bean.setRecordChangeDateStr(RutDate.dateToString(new Date(rs.getTimestamp("RECORD_CHANGE_DATE").getTime()),"dd/MM/yyyy HH:mm"));
                                           
                           return bean;
                       }
                   });
    }
    
    public StringBuffer save(JdbcTemplate jdbcTemplate,NamedParameterJdbcTemplate namedParameterJdbcTemplate,boolean isInsert,CamPeriodParameterMod updateData) throws Exception {
    
        System.out.println ("[CamPeriodParameterHelperJdbcDao][save]:isInsert "+isInsert);
        String errorMsg = "";
        StringBuffer msg = new StringBuffer();
       
        if(updateData !=null ){
   
             if(isInsert){
                try{
                        
                            if(checkPeriodParameterCodeExist(namedParameterJdbcTemplate,updateData.getPeriodParameterCode())){
                               msg.append("* Duplicate period parameter code");
                            }else{
                                insertData(jdbcTemplate,namedParameterJdbcTemplate,updateData);
                            }

                 }catch(Exception e){
                    errorMsg = errorMsg +"Cannot insert data: "+e.getMessage();
                 }
             }else{
                 try{
                     updateData(jdbcTemplate,updateData) ;          
                 }catch(Exception e){
                    errorMsg = errorMsg +"Cannot update data: "+e.getMessage();
                 }      
             }

             } 
        
       
        
        
        if(!errorMsg.equals("")){
            
            throw new Exception(errorMsg);
        }
        
        return msg;
      
    }
    
    
    public String[] deleteByIds(JdbcTemplate jdbcTemplate,NamedParameterJdbcTemplate namedParameterJdbcTemplate,String[] idArray) throws Exception{
        System.out.println ("[CamPeriodParameterHelperJdbcDao][deleteByIds] ");
        StringBuffer errorMsg = new StringBuffer();
        StringBuffer cannotDeleteMsg  = new StringBuffer();
        if(idArray !=null && idArray.length>0){
            for(int i = 0 ; i < idArray.length; i++){
            
                try{
                    System.out.println ("[CamPeriodParameterHelperJdbcDao][deleteByIds]: "+idArray[i]);
                    if(checkPeriodParameterExistServiceParameter(namedParameterJdbcTemplate,idArray[i])){
                        cannotDeleteMsg.append(","+idArray[i]);
                    }else{
                        deleteData(jdbcTemplate,idArray[i]);
                    }
                }catch(Exception e){
                    errorMsg.append(","+idArray[i]);
                }
            }
        }
        
        String[] rs = new String[2];
        rs[0] = "";
        rs[1] = "";
        
        if(!RutString.isEmptyString(cannotDeleteMsg.toString())){
            rs[0] = cannotDeleteMsg.toString().substring(1);
        }
        
        if(!RutString.isEmptyString(errorMsg.toString())){
            rs[0] = errorMsg.toString().substring(1);
        }
        
        return rs;
    }
    
    public void insertData(JdbcTemplate jdbcTemplate,NamedParameterJdbcTemplate namedParameterJdbcTemplate,CamPeriodParameterMod newBean) throws DataAccessException{
       StringBuffer insertSql =new StringBuffer(" INSERT INTO CAM_PERIOD_PARAMETER ");
                    insertSql.append(" ( PK_CAM_PERIOD_PARAMETER_ID ");
                    insertSql.append(", PERIOD_PARAMETER_CODE");
                    insertSql.append(", NO_OF_DAY");
                    insertSql.append(", RECORD_STATUS");
                    insertSql.append(", RECORD_ADD_USER");
                    insertSql.append(", RECORD_ADD_DATE");
                    insertSql.append(", RECORD_CHANGE_USER");
                    insertSql.append(", RECORD_CHANGE_DATE");
                    insertSql.append(" ) ");
                    insertSql.append(" VALUES ( ?,?,?,?,?,SYSDATE,?,SYSDATE )");
                    
       if(newBean !=null ){
         

               newBean.setPeriodParameterId(String.valueOf(getPkId(namedParameterJdbcTemplate)));
               
                final CamPeriodParameterMod newData =newBean;
               System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: insertData.getPeriodParameterId() : "+newData.getPeriodParameterId());
               System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: insertData.getPeriodParameterCode() : "+newData.getPeriodParameterCode());
               System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: insertData.getNoOfDay() : "+newData.getNoOfDay());
               System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: insertData.getRecordStatus() : "+newData.getRecordStatus());
               System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: insertData.getRecordChangeUser() : "+newData.getRecordChangeUser());
               
               jdbcTemplate.update(insertSql.toString(),new PreparedStatementSetter() {
                                                                                     public void setValues(PreparedStatement ps) throws SQLException {
                                                                                     
                                                                                       ps.setLong(1,  Long.parseLong(newData.getPeriodParameterId()));
                                                                                       ps.setString(2, newData.getPeriodParameterCode());
                                                                                       ps.setString(3, newData.getNoOfDay());
                                                                                       ps.setString(4, newData.getRecordStatus());
                                                                                       ps.setString(5, newData.getRecordChangeUser());
                                                                                       ps.setString(6, newData.getRecordChangeUser());
                                                                                     
                                                                                     }
               });
   
       }
    }
    
    
    public void updateData(JdbcTemplate jdbcTemplate,final CamPeriodParameterMod updateData) throws DataAccessException{
        StringBuffer updateSql =new StringBuffer(" UPDATE CAM_PERIOD_PARAMETER ");
                     updateSql.append(" SET ");
                     updateSql.append("  PERIOD_PARAMETER_CODE =? ");
                     updateSql.append(", NO_OF_DAY=? ");
                     updateSql.append(", record_status=? ");
                     updateSql.append(", record_change_user=? ");
                     updateSql.append(", record_change_date=SYSDATE ");
                     updateSql.append(" WHERE PK_CAM_PERIOD_PARAMETER_ID =? ");
                     
        CamPeriodParameterMod updataBean = null;
        if(updateData !=null ){
        

                System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: updateData.getPeriodParameterId() : "+updateData.getPeriodParameterId());
                System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: updateData.getPeriodParameterCode() : "+updateData.getPeriodParameterCode());
                System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: updateData.getNoOfDay() : "+updateData.getNoOfDay());
                System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: updateData.getRecordStatus() : "+updateData.getRecordStatus());
                System.out.println ("[CamPeriodParameterHelperJdbcDao][insertData]: updateData.getRecordChangeUser() : "+updateData.getRecordChangeUser());
                
                jdbcTemplate.update(updateSql.toString(),new PreparedStatementSetter() {
                                                                                      public void setValues(PreparedStatement ps) throws SQLException {
                                                                                      
                                                                                          ps.setString(1, updateData.getPeriodParameterCode());
                                                                                          ps.setString(2, updateData.getNoOfDay());
                                                                                          ps.setString(3, updateData.getRecordStatus());
                                                                                          ps.setString(4, updateData.getRecordChangeUser());
                                                                                          ps.setLong(5,  Long.parseLong(updateData.getPeriodParameterId()));
                                                                                      
                                                                                      }
                });

        }
    }

  
    public void deleteData(JdbcTemplate jdbcTemplate,final String id) throws DataAccessException{
            System.out.println ("[CamPeriodParameterHelperJdbcDao][deleteData]: deleteData : "+id);
            StringBuffer deleteSql =new StringBuffer(" DELETE CAM_PERIOD_PARAMETER ");
                     deleteSql.append(" WHERE PERIOD_PARAMETER_CODE =? ");
            
                jdbcTemplate.update(deleteSql.toString(),new PreparedStatementSetter() {
                                                                                      public void setValues(PreparedStatement ps) throws SQLException {
                                                                                      
                                                                                        ps.setString(1, id);
                                                                                       
                                                                                        }
                });
                
   
    }

    private long getPkId(NamedParameterJdbcTemplate namedParameterJdbcTemplate){
    	Long result = (Long) namedParameterJdbcTemplate.queryForObject("SELECT SR_CAM_PPA01.nextVal FROM dual",new HashMap(), Long.class);
    	return result;
    }
    
    private boolean checkPeriodParameterExistServiceParameter(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String periodParameterCode){
    
    Integer countObj = (Integer) namedParameterJdbcTemplate.queryForObject("SELECT count(PERIOD_PARAMETER_CODE) FROM CAM_SERVICE_PARAMETER WHERE PERIOD_PARAMETER_CODE ='"+periodParameterCode+"'",new HashMap(), Integer.class);
      
      if(countObj != null && countObj.intValue() > 0){
          return true;
      }
      
      return false;
      
    }
    
    private boolean checkPeriodParameterCodeExist(NamedParameterJdbcTemplate namedParameterJdbcTemplate,String periodParameterCode){
    
    	Integer countObj = (Integer) namedParameterJdbcTemplate.queryForObject("SELECT count(PERIOD_PARAMETER_CODE) from CAM_PERIOD_PARAMETER  WHERE PERIOD_PARAMETER_CODE ='"+periodParameterCode+"'",new HashMap(), Integer.class);
        
        if(countObj != null && countObj.intValue() > 0){
            return true;
        }
        
        return false;
        
    }
}
