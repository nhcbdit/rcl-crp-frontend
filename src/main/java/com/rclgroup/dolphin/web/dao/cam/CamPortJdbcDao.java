 /*------------------------------------------------------
 CamPortJdbcDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Manop Wanngam 10/10/2007   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

 package com.rclgroup.dolphin.web.dao.cam;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamPortMod;
import com.rclgroup.dolphin.web.util.RutString;


public class CamPortJdbcDao extends RrcStandardDao implements CamPortDao {
     
     public CamPortJdbcDao() {
     }
     
     protected void initDao() throws Exception {
         super.initDao();
     }
     
     public boolean isValid(String portCode) throws DataAccessException {
        System.out.println("[CamPortJdbcDao][isValid]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT PORT_CODE ");
        sql.append("FROM VR_CAM_PORT ");
        sql.append("WHERE PORT_CODE = :portCode");        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("portCode", portCode));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         
         System.out.println("[CamPortJdbcDao][isValid]: Finished");
         return isValid;
     }
     public boolean isValidTerminal(String terminalCode) throws DataAccessException {
        System.out.println("[CamPortJdbcDao][isValidTerminal]: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT TQTERM ");
        sql.append("FROM VR_CAM_FSC_PORT_TERMINAL ");
        sql.append("WHERE TQTERM = :terminalCode");        
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
             Collections.singletonMap("terminalCode", terminalCode));
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         
         System.out.println("[CamPortJdbcDao][isValidTerminal]: Finished");
         return isValid;
     }
     
     public boolean isValidByFSC(String portCode, String fsc) throws DataAccessException {
         System.out.println("[CamPortJdbcDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT PORT_CODE ");
         sql.append("FROM VR_CAM_PORT ");
         sql.append("WHERE PORT_CODE = :portCode ");
         if((fsc!=null)&&(!fsc.trim().equals(""))){
            sql.append("AND FSC = :fsc ");
         }
         HashMap map = new HashMap();
         map.put("portCode",portCode);
         map.put("fsc",fsc);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[CamPortJdbcDao][isValidByFSC]: Finished");
         return isValid;
     }
     
     public boolean isValid(String portCode, String status) throws DataAccessException {
         System.out.println("[CamPortJdbcDao][isValid]: With status: Started");
         boolean isValid = false;
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT PORT_CODE ");
         sql.append("FROM VR_CAM_PORT ");
         sql.append("WHERE PORT_CODE = :portCode ");
         sql.append("AND PORT_STATUS = :status ");
         
         HashMap map = new HashMap();
         map.put("portCode",portCode);
         map.put("status",status);
         SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
         if(rs.next()) {
             isValid = true;
         } else { 
             isValid = false;
         }
         System.out.println("[CamPortJdbcDao][isValid]: Finished");
         return isValid;
     }   
     
     public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
         System.out.println("[CamPortJdbcDao][listForHelpScreen]: Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT PORT_CODE ");
         sql.append("      ,PORT_NAME ");
         sql.append("      ,COUNTRY_CODE ");   
         sql.append("      ,PORT_STATE ");
         sql.append("      ,PORT_ZONE ");
         sql.append("      ,PORT_TYPE ");            
         sql.append("      ,PORT_STATUS "); 
         sql.append("      ,AREA ");
         sql.append("      ,REGION ");
         sql.append("FROM VR_CAM_PORT ");         
         sql.append(sqlCriteria);
         sql.append("GROUP BY PORT_CODE ");
         sql.append("      ,PORT_NAME ");
         sql.append("      ,COUNTRY_CODE ");   
         sql.append("      ,PORT_STATE ");
         sql.append("      ,PORT_ZONE ");
         sql.append("      ,PORT_TYPE ");            
         sql.append("      ,PORT_STATUS "); 
         sql.append("      ,AREA ");
         sql.append("      ,REGION ");
         System.out.println("[CamPortJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
         System.out.println("[CamPortJdbcDao][listForHelpScreen]: Finished");
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    new HashMap(),
                    new RowModMapper());
     }
     public List listForHelpScreenDisplayTerminal(String find, String search, String wild, String line, String trade, String agent, String fsc) throws DataAccessException {
         System.out.println("[CamPortJdbcDao][listForHelpScreenDisplayTerminal]:With Terminal : Started");
         String sqlCriteria = createSqlCriteria(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT PORT_CODE ");
         sql.append("      ,PORT_NAME ");
         sql.append("      ,TQTERM ");   
         sql.append("      ,TQTRNM ");
         sql.append("FROM VR_CAM_FSC_PORT_TERMINAL ");

         if(sqlCriteria.trim().equals("")){
             sql.append("WHERE");
         }else{
             sql.append(sqlCriteria);
             sql.append(" AND");
         }
         
         HashMap map = new HashMap();
         if(!line.equals("") ){
             sql.append(" LINE = :line");
             map.put("line",line);
         }
         if(!trade.equals("") ){
             sql.append(" AND TRADE = :trade");            
             map.put("trade",trade);
         }
         if(!agent.equals("") ){
             sql.append(" AND AGENT = :agent");
             map.put("agent",agent);
         }
         if(!fsc.equals("") ){
             sql.append(" AND FSC = :fsc");
             map.put("fsc",fsc);
         }
         
         System.out.println("[CamPortJdbcDao][listForHelpScreenDisplayTerminal]:With Terminal : SQL = " + sql.toString());
         System.out.println("[CamPortJdbcDao][listForHelpScreenDisplayTerminal]:With Terminal : Finished");
         
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 CamPortMod port = new CamPortMod();
                 port.setPortCode(RutString.nullToStr(rs.getString("PORT_CODE")));   
                 port.setPortName(RutString.nullToStr(rs.getString("PORT_NAME")));    
                 port.setTerminalCode(RutString.nullToStr(rs.getString("TQTERM")));   
                 port.setTerminalName(RutString.nullToStr(rs.getString("TQTRNM")));
                 return port;
             }
         });

     }
     private String createSqlCriteria(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "WHERE PORT_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "WHERE PORT_NAME " + sqlWild;
             }else if(search.equalsIgnoreCase("CN")){
                 sqlCriteria = "WHERE COUNTRY_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("ST")){
                 sqlCriteria = "WHERE PORT_STATE " + sqlWild;
             }else if(search.equalsIgnoreCase("Z")){
                 sqlCriteria = "WHERE PORT_ZONE " + sqlWild;
             }else if(search.equalsIgnoreCase("T")){
                 sqlCriteria = "WHERE PORT_TYPE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "WHERE PORT_STATUS " + sqlWild;
             }        
         }
         return sqlCriteria;
     }    
     
     public List listForHelpScreen(String find, String search, String wild, String zone, String fsc, String status) throws DataAccessException {
         System.out.println("[CamPortJdbcDao][listForHelpScreen]: With status: Started");
         String sqlCriteria = createSqlCriteriaWithStatus(find, search, wild);
         StringBuffer sql = new StringBuffer();
         sql.append("SELECT PORT_CODE ");
         sql.append("      ,PORT_NAME ");
         sql.append("      ,COUNTRY_CODE ");   
         sql.append("      ,PORT_STATE ");
         sql.append("      ,PORT_ZONE ");
         sql.append("      ,PORT_TYPE ");            
         sql.append("      ,PORT_STATUS "); 
         sql.append("      ,AREA ");
         sql.append("      ,REGION ");
         sql.append("      ,TQTERM ");
         sql.append("      ,TQTRNM ");
         
         sql.append("FROM VR_CAM_PORT ");
         sql.append("WHERE PORT_STATUS = :status ");
         if((zone!=null)&&(!zone.trim().equals(""))){
             sql.append("  AND PORT_ZONE = :zone "); 
         } 
         if((fsc!=null)&&(!fsc.trim().equals(""))){
             sql.append("  AND FSC = :fsc "); 
         }
         System.out.println(""+zone+fsc);
         sql.append(sqlCriteria);
         System.out.println("[CamPortJdbcDao][listForHelpScreen]: With status: SQL = " + sql.toString());
         System.out.println("[CamPortJdbcDao][listForHelpScreen]: With status: Finished");
         HashMap map = new HashMap();
         map.put("status",status);
         map.put("zone",zone);
         map.put("fsc",fsc);
         return getNamedParameterJdbcTemplate().query(
                    sql.toString(),
                    map,
         new RowMapper(){
             public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                 CamPortMod port = new CamPortMod();
                 port.setPortCode(RutString.nullToStr(rs.getString("PORT_CODE")));   
                 port.setPortName(RutString.nullToStr(rs.getString("PORT_NAME")));        
                 port.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
                 port.setPortState(RutString.nullToStr(rs.getString("PORT_STATE")));
                 port.setPortZone(RutString.nullToStr(rs.getString("PORT_ZONE"))); 
                 port.setPortType(RutString.nullToStr(rs.getString("PORT_TYPE")));        
                 port.setPortStatus(RutString.nullToStr(rs.getString("PORT_STATUS")));
                 port.setPortArea(RutString.nullToStr(rs.getString("AREA")));
                 port.setPortRegion(RutString.nullToStr(rs.getString("REGION")));
                 port.setTerminalCode(RutString.nullToStr(rs.getString("TQTERM")));
                 port.setTerminalName(RutString.nullToStr(rs.getString("TQTRNM")));
                 
                 return port;
             }
         });         
     }
     
     private String createSqlCriteriaWithStatus(String find, String search, String wild) {
         String sqlCriteria = "";
         String sqlWild = "";

         if(wild.equalsIgnoreCase("ON")){
             sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
         }else{
             sqlWild = "= '" + find.toUpperCase() + "' ";
         }
         if(find.trim().length() == 0){
         }else{
             if(search.equalsIgnoreCase("C")){
                 sqlCriteria = "AND PORT_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("N")){
                 sqlCriteria = "AND PORT_NAME " + sqlWild;
             }else if(search.equalsIgnoreCase("CN")){
                 sqlCriteria = "AND COUNTRY_CODE " + sqlWild;
             }else if(search.equalsIgnoreCase("ST")){
                 sqlCriteria = "AND PORT_STATE " + sqlWild;
             }else if(search.equalsIgnoreCase("Z")){
                 sqlCriteria = "AND PORT_ZONE " + sqlWild;
             }else if(search.equalsIgnoreCase("T")){
                 sqlCriteria = "AND PORT_TYPE " + sqlWild;
             }else if(search.equalsIgnoreCase("S")){
                 sqlCriteria = "AND PORT_STATUS " + sqlWild;
             }        
         }
         return sqlCriteria;
     }

    protected class RowModMapper implements RowMapper{
         public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
             return moveDbToModelForHelp(rs);
         }
     } 
     
    
     private CamPortMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
         CamPortMod port = new CamPortMod();
         port.setPortCode(RutString.nullToStr(rs.getString("PORT_CODE")));   
         port.setPortName(RutString.nullToStr(rs.getString("PORT_NAME")));        
         port.setCountryCode(RutString.nullToStr(rs.getString("COUNTRY_CODE")));
         port.setPortState(RutString.nullToStr(rs.getString("PORT_STATE")));
         port.setPortZone(RutString.nullToStr(rs.getString("PORT_ZONE"))); 
         port.setPortType(RutString.nullToStr(rs.getString("PORT_TYPE")));        
         port.setPortStatus(RutString.nullToStr(rs.getString("PORT_STATUS")));
         port.setPortArea(RutString.nullToStr(rs.getString("AREA")));
         port.setPortRegion(RutString.nullToStr(rs.getString("REGION")));
         
         return port;
     }     
       
 }
