/*------------------------------------------------------
DexBillOfLandingPrintDao.java
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2010
--------------------------------------------------------
 Author Kitti Pongsirisakun 05/01/2011
- Change Log -------------------------------------------
## DD/MM/YY �User-      -TaskRef-       -ShortDescription-
01 03/10/16 Sarawut A.                  Add new method for generate excel
--------------------------------------------------------
*/
package com.rclgroup.dolphin.web.dao.dex;

import com.rclgroup.dolphin.web.model.dex.DexBillOfLandingPrintExcelMod;

import java.util.List;
import java.util.Vector;
import org.springframework.dao.DataAccessException;

public interface DexBillOfLandingPrintDao {

    
    /**
     * @param quotationNo
     * @return
     * @throws DataAccessException
     */
    public Vector getListBLPrint(String session, String userName) throws DataAccessException;
    
    /**
     *  @param quotationNo
     * @param sessionId   
     * @return username
     * @throws DataAccessException
     */
    public String getSessionId(String service , String vessel, String voyage,  String direction, 
                                                      String offCreateBL ,String bl,String cocsoc, String print,
                                                      String pol_sts,String dateFrom,String dateTo,String third,
                                                      String fsc,String userCode,String sessionid,String line,
                                                      String region,String agent,String fscCode
                                                     ) throws DataAccessException;
    
    public String showHideGenExcelButton(String fsc) throws DataAccessException; //01
    public List<DexBillOfLandingPrintExcelMod> generateExcelHeader(String userCode,String sessionid) throws DataAccessException; //01
    public List<DexBillOfLandingPrintExcelMod> generateExcelDetail(String userCode,String sessionid) throws DataAccessException; //01
}

