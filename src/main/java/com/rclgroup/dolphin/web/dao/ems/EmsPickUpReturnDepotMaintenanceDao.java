/*-----------------------------------------------------------------------------------------------------------  
EmsPickUpReturnDepotMaintenanceDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Sukit Narinsakchai 15/11/2010
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/
package com.rclgroup.dolphin.web.dao.ems;

import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;
import org.springframework.dao.DataAccessException;

public interface EmsPickUpReturnDepotMaintenanceDao extends RriStandardDao {
     /**
      * number of list of Multi Depot Detail data
      * @param fk_ems_multi_depot_hdr_id       
      * @param status
      * @return number of Multi Depot Detail's record
      * @throws DataAccessException
      */
//     public int countListDetailForStatement(String fk_ems_multi_depot_hdr_id,String status) throws DataAccessException;
     
     /**
      * list of Size/Type for search and maintenance screen
      * @param fk_ems_multi_depot_hdr_id          
      * @param status      
      * @return list of Size/Type(EmsPickUpReturnDepotMaintenanceDetailMod)
      * @throws DataAccessException
      */
     public List listDetailForStatement(String fk_ems_multi_depot_hdr_id,String status) throws DataAccessException;
      
     /**
      * update a size/type record 
      * @param mod a Multi Depot Detail model as input and output
      * @return whether updating is successful
      * @throws DataAccessException exceptin which client has to catch all following error messages:
      *                              error message: BILL_TO_PARTY_REQ
      *                              error message: BL_REQ
      *                              error message: INV_NO_REQ
      *                              error message: DATA_NOT_FOUND      
      *                              error message: ORA-XXXXX (un-exceptional oracle error)  
      */
     public boolean updateSizeType (RrcStandardMod mod) throws DataAccessException;
     
     /**
     * delete a size/type record 
     * @param mod a Multi Depot Detail model as input and output
     * @return whether delete is successful
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: PK_DATA_MAP_ID_REQ     
     *                              error message: DATA_NOT_FOUND      
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
     public boolean deleteSizeType (RrcStandardMod mod) throws DataAccessException;
     
     /**
      * insert a size/type record 
      * @param mod a Multi Depot Detail model as input and output
      * @return whether insert is successful
      * @throws DataAccessException exceptin which client has to catch all following error messages:
      *                              error message: BILL_TO_PARTY_REQ
      *                              error message: INV_NO_REQ
      *                              error message: DATA_NOT_FOUND      
      *                              error message: ORA-XXXXX (un-exceptional oracle error)  
      */
     public boolean insertSizeType (RrcStandardMod mod) throws DataAccessException;
     
    /**
     * update a multi depot header record 
     * @param mod a Multi Depot Header model as input and output
     * @return whether updating is successful
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: BILL_TO_PARTY_REQ
     *                              error message: BL_REQ
     *                              error message: INV_NO_REQ
     *                              error message: DATA_NOT_FOUND      
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public boolean updateHdr (RrcStandardMod mod) throws DataAccessException;
    
    /**
    * delete a multi depot header record 
    * @param mod a Multi Depot Header model as input and output
    * @return whether delete is successful
    * @throws DataAccessException exceptin which client has to catch all following error messages:
    *                              error message: PK_DATA_MAP_ID_REQ     
    *                              error message: DATA_NOT_FOUND      
    *                              error message: ORA-XXXXX (un-exceptional oracle error)  
    */
    public boolean deleteHdr (RrcStandardMod mod) throws DataAccessException;
    
    /**
    * delete a multi depot header record 
    * @param mod a Multi Depot Header model as input and output
    * @return whether delete is successful
    * @throws DataAccessException exceptin which client has to catch all following error messages:
    *                              error message: PK_DATA_MAP_ID_REQ     
    *                              error message: DATA_NOT_FOUND      
    *                              error message: ORA-XXXXX (un-exceptional oracle error)  
    */
    public boolean deleteDtlByHdr (RrcStandardMod mod) throws DataAccessException;
    
    /**
     * insert a multi depot header record 
     * @param mod a Multi Depot Header model as input and output
     * @return whether insert is successful
     * @throws DataAccessException exceptin which client has to catch all following error messages:
     *                              error message: BILL_TO_PARTY_REQ
     *                              error message: INV_NO_REQ
     *                              error message: DATA_NOT_FOUND      
     *                              error message: ORA-XXXXX (un-exceptional oracle error)  
     */
    public boolean insertHdr (RrcStandardMod mod) throws DataAccessException;
     
}


