/*-----------------------------------------------------------------------------------------------------------  
TosRateHeaderJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 06/06/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RrcStandardDao;

import com.rclgroup.dolphin.web.model.tos.TosRateHeaderMod;
import com.rclgroup.dolphin.web.model.tos.TosServiceMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

public class TosRateHeaderJdbcDao  extends RrcStandardDao implements TosRateHeaderDao{
    public TosRateHeaderJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public List listForHelpScreen(String find, String search, String wild, String port, String termianl) throws DataAccessException {
        System.out.println("[TosRateHeaderJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT OPERATION_TYPE ");
        sql.append("       ,(SELECT DESCR FROM TOS_OPR_CODE_MASTER OPR_MST WHERE OPR_MST.OPR_CODE = TOS_RATE_HEADER.OPERATION_TYPE) DESCR ");
        sql.append("       ,TOS_RATE_REF ");   
        sql.append(" FROM TOS_RATE_HEADER ");                 
        sql.append(" WHERE PORT = '"+port+"' ");
        sql.append(" AND TERMINAL = '"+termianl+"' ");
        sql.append(sqlCriteria);
        System.out.println("[TosRateHeaderJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[TosRateHeaderJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("OPT")){
                sqlCriteria = "AND OPERATION_TYPE " + sqlWild;            
            }else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND UPPER(DESCR) " + sqlWild;                        
            }else if(search.equalsIgnoreCase("R")){
                sqlCriteria = "AND TOS_RATE_REF " + sqlWild;      
            }        
        }
        return sqlCriteria;
    }
    
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    
    private TosRateHeaderMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        TosRateHeaderMod rate = new TosRateHeaderMod();
        
        rate.setOprType(RutString.nullToStr(rs.getString("OPERATION_TYPE")));
        rate.setDescription(RutString.nullToStr(rs.getString("DESCR")));
        rate.setRateRef(RutString.nullToStr(rs.getString("TOS_RATE_REF")));
     
        return rate;
    } 

}
