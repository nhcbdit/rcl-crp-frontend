/*-----------------------------------------------------------------------------------------------------------  
VssServiceProformaDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 29/08/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/ 

package com.rclgroup.dolphin.web.dao.vss;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface VssServiceProformaDao {

    /**
     * list service proforma records for help screen with status
     * @param find
     * @param search
     * @param wild
     * @param status
     * @return list of service proformas with status
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild, String status) throws DataAccessException;

    /**
     * list service proforma records for help screen with status
     * @param performaNo
     * @param status

     */
    public boolean  isValid(String performaNo,String status) throws DataAccessException;
    
}


