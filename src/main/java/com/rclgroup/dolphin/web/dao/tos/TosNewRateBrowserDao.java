/*-----------------------------------------------------------------------------------------------------------  
TosNewRateBrowserDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 06/06/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/


package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TosNewRateBrowserDao extends RriStandardDao{

    public List getRateDtlList(String rate_seq, String port,String terminal,String operation) throws DataAccessException;
    
    public List getCategoryList(String port,String terminal,String operation) throws DataAccessException;
        
    public List getActivityList(String port, String terminal, String operation) throws DataAccessException;
    
}
