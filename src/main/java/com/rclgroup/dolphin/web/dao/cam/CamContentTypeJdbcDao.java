/*-----------------------------------------------------------------------------------------------------------  
CamContentTypeJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Piyapong Ieumwananonthachai 03/04/08
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.cam.CamContentTypeMod;
import com.rclgroup.dolphin.web.model.cam.CamContentTypeModuleNameDescriptionMod;
import com.rclgroup.dolphin.web.model.rcm.RcmSearchMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;


public class CamContentTypeJdbcDao extends RrcStandardDao implements CamContentTypeDao {
    private InsertStoreProcedure insertStoreProcedure;
    private UpdateStoreProcedure updateStoreProcedure;
    private DeleteStoreProcedure deleteStoreProcedure;
    private GetModuleNameDescriptionMapStoreProcedure getModuleNameDescriptionMapStoreProcedure;

    public CamContentTypeJdbcDao() {
    }
    
    protected void initDao() throws Exception {
        super.initDao();
        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
        getModuleNameDescriptionMapStoreProcedure = new GetModuleNameDescriptionMapStoreProcedure(getJdbcTemplate());
    }    

    public List listForSearchScreen(RcmSearchMod searchMod) throws DataAccessException{
        Map columnMap = new HashMap();
        columnMap.put("C","CONTENT_CODE");
        columnMap.put("T","CONTENT_TYPE");
        columnMap.put("D","DESCRIPTION");
        columnMap.put("M","MODULE_NAME");
        String sqlSearchCriteria = createSqlSearchCriteria(searchMod,columnMap);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT CONTENT_CODE ");
        sql.append("      ,CONTENT_TYPE ");
        sql.append("      ,DESCRIPTION ");
        sql.append("      ,MODULE_NAME ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_CAM_CONTENT_TYPE ");
        sql.append(sqlSearchCriteria);
        System.out.println("[CamContentTypeJdbcDao][listForSearchScreen]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }

    public CamContentTypeMod findByKeyCode(String code) throws DataAccessException{
        CamContentTypeMod contentTypeMod = null;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT CONTENT_CODE ");
        sql.append("      ,CONTENT_TYPE ");
        sql.append("      ,DESCRIPTION ");
        sql.append("      ,MODULE_NAME ");
        sql.append("      ,RECORD_STATUS ");
        sql.append("      ,RECORD_ADD_USER ");
        sql.append("      ,RECORD_ADD_DATE ");
        sql.append("      ,RECORD_CHANGE_USER ");
        sql.append("      ,RECORD_CHANGE_DATE ");
        sql.append("FROM VR_CAM_CONTENT_TYPE ");
        sql.append("WHERE CONTENT_CODE = :code ");        
        sql.append("ORDER BY CONTENT_CODE ");
        try{
            contentTypeMod = (CamContentTypeMod)getNamedParameterJdbcTemplate().queryForObject(
                   sql.toString(),
                   Collections.singletonMap("code", code),
                   new RowModMapper());
        }catch (EmptyResultDataAccessException e) {
            contentTypeMod = null;
        }
        return contentTypeMod;
    }

    private String insertWithoutTransaction(CamContentTypeMod contentTypeMod, Date recordChangeDate) throws DataAccessException{
        return insertStoreProcedure.insert(contentTypeMod,recordChangeDate);
    }
    
    private Date updateWithoutTransaction(CamContentTypeMod contentTypeMod) throws DataAccessException{
        return updateStoreProcedure.update(contentTypeMod);
    }
    
    private void deleteWithoutTransaction(CamContentTypeMod contentTypeMod) throws DataAccessException{
        deleteStoreProcedure.delete(contentTypeMod);
    }
    
    public String insert(CamContentTypeMod contentTypeMod, Date recordChangeDate) throws DataAccessException{
        return insertWithoutTransaction(contentTypeMod,recordChangeDate);
    }
    
    public Date update(CamContentTypeMod contentTypeMod) throws DataAccessException{
        return updateWithoutTransaction(contentTypeMod);
    }
    
    public void delete(CamContentTypeMod contentTypeMod) throws DataAccessException{
        deleteWithoutTransaction(contentTypeMod);
    }
    
    public List getContentTypeModuleNameDescriptionList() throws DataAccessException{
        return getModuleNameDescriptionMapStoreProcedure.getContentTypeModuleNameDescriptionModList();
    }

    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModel(rs);
        }
    }    
    
    private CamContentTypeMod moveDbToModel(ResultSet rs) throws SQLException {
        CamContentTypeMod contentTypeMod = new CamContentTypeMod();
        contentTypeMod.setCode(RutString.nullToStr(rs.getString("CONTENT_CODE")));
        contentTypeMod.setType(RutString.nullToStr(rs.getString("CONTENT_TYPE")));
        contentTypeMod.setDescription(RutString.nullToStr(rs.getString("DESCRIPTION")));
        contentTypeMod.setModuleName(RutString.nullToStr(rs.getString("MODULE_NAME")));
        contentTypeMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));
        contentTypeMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));
        contentTypeMod.setRecordAddDate(rs.getDate("RECORD_ADD_DATE")); 
        contentTypeMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));
        contentTypeMod.setRecordChangeDate(rs.getDate("RECORD_CHANGE_DATE"));
        return contentTypeMod;
    }

    protected class InsertStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_CONTENT_TYPE.PRR_INS_CONTENT_TYPE";
        
        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlInOutParameter("p_content_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_content_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_description", Types.VARCHAR));
            declareParameter(new SqlParameter("p_module_name", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_add_user", Types.VARCHAR));
            declareParameter(new SqlOutParameter("p_record_change_date", Types.DATE));
            compile();
        }
        
        protected String insert(CamContentTypeMod contentTypeMod, Date recordChangeDate) {
            String contentTypeCode = null; 
            Map inParameters = new HashMap(6);
            inParameters.put("p_content_code", contentTypeMod.getCode());
            inParameters.put("p_content_type", contentTypeMod.getType());
            inParameters.put("p_description", contentTypeMod.getDescription());
            inParameters.put("p_module_name", contentTypeMod.getModuleName());
            inParameters.put("p_record_status", contentTypeMod.getRecordStatus());
            inParameters.put("p_record_add_user", contentTypeMod.getRecordAddUser());
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
                contentTypeCode = (String)outParameters.get("p_content_code");
                recordChangeDate = (Date)outParameters.get("p_record_change_date");
            } else {
                contentTypeCode = null;
                recordChangeDate = null;
            }
            return contentTypeCode;
        }
    }
    
    protected class UpdateStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_CONTENT_TYPE.PRR_UPD_CONTENT_TYPE";
        
        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_content_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_content_type", Types.VARCHAR));
            declareParameter(new SqlParameter("p_description", Types.VARCHAR));
            declareParameter(new SqlParameter("p_module_name", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_status", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlInOutParameter("p_record_change_date", Types.DATE));
            compile();
        }
        
        protected Date update(CamContentTypeMod contentTypeMod) {
            Map inParameters = new HashMap(7);
            inParameters.put("p_content_code", contentTypeMod.getCode());
            inParameters.put("p_content_type", contentTypeMod.getType());
            inParameters.put("p_description", contentTypeMod.getDescription());
            inParameters.put("p_module_name", contentTypeMod.getModuleName());
            inParameters.put("p_record_status", contentTypeMod.getRecordStatus());
            inParameters.put("p_record_change_user", contentTypeMod.getRecordChangeUser());
            inParameters.put("p_record_change_date", contentTypeMod.getRecordChangeDate());
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
                return (Date)outParameters.get("p_record_change_date");
            } else {
                return null;
            }
        }
    }
    
    protected class DeleteStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_CONTENT_TYPE.PRR_DEL_CONTENT_TYPE";
        
        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlParameter("p_content_code", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
            declareParameter(new SqlParameter("p_record_change_date", Types.DATE));
            compile();
        }
        
        protected void delete(CamContentTypeMod contentTypeMod) {
            Map inParameters = new HashMap(3);
            inParameters.put("p_content_code", contentTypeMod.getCode());
            inParameters.put("p_record_change_user", contentTypeMod.getRecordChangeUser());
            inParameters.put("p_record_change_date", contentTypeMod.getRecordChangeDate());
            execute(inParameters);
        }
    }
    
    protected class GetModuleNameDescriptionMapStoreProcedure extends StoredProcedure {
        private static final String STORED_PROCEDURE_NAME = "PCR_CAM_CONTENT_TYPE.PRR_SEL_MODULE_NAME_DESC";
        
        protected GetModuleNameDescriptionMapStoreProcedure(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, STORED_PROCEDURE_NAME);
            declareParameter(new SqlOutParameter("p_module_name", Types.ARRAY,"TT_CAM_CONTENT_TYPE_MOD_NAME"));
            declareParameter(new SqlOutParameter("p_module_description", Types.ARRAY,"TT_CAM_CONTENT_TYPE_MOD_DESC"));
            compile();
        }
        
        protected List getContentTypeModuleNameDescriptionModList() {
            List list = new ArrayList();
            String[] moduleNameArray = null;
            String[] moduleDescArray = null;
            Map inParameters = new HashMap();
            Map outParameters = execute(inParameters);
            if (outParameters.size() > 0) {
                Array moduleNameValue = (Array)outParameters.get("p_module_name");
                Array moduleDescValue = (Array)outParameters.get("p_module_description");
                try {
                    moduleNameArray = (String[]) moduleNameValue.getArray();
                    moduleDescArray = (String[]) moduleDescValue.getArray();
                    for(int i=0;i<moduleNameArray.length;i++){
                        CamContentTypeModuleNameDescriptionMod moduleMod = new CamContentTypeModuleNameDescriptionMod();
                        moduleMod.setModuleName(moduleNameArray[i]);
                        moduleMod.setModuleDescription(moduleDescArray[i]);
                        list.add(moduleMod);
                    }
                }catch (SQLException se) {
                    list = new ArrayList();
                }
            } else {
                list = new ArrayList();
            }
            return list;
        }
    }
}
