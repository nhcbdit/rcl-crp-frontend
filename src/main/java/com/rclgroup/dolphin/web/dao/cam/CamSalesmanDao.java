/*-----------------------------------------------------------------------------------------------------------  
CamSalesmanDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2009
-------------------------------------------------------------------------------------------------------------
 Author Leena Babu 22/08/2011
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description 
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.cam;

import com.rclgroup.dolphin.web.model.cam.CamSalesmanMod;

import java.util.List;

import org.springframework.dao.DataAccessException;


public interface CamSalesmanDao {

   /**
     * Check whether the salesman code is valid for the given status
     * @param salesmanCode
     * @param status
     * @return true if salesmancode is valid else false
     * @throws DataAccessException
     */
    public boolean isValid(String salesmanCode, String status) throws DataAccessException;  
    
    /**
     * Get the salesman details for the given user id
     * @param userId
     * @return CamSalesmanMod
     * @throws DataAccessException
     */
    public CamSalesmanMod findSalesmanForUserId(String userId) throws DataAccessException;
    
    /**
     * Checks whether the given salesman comes under the given manager
     * @param salesmanCode
     * @param mgrSalesmanCode
     * @return true if salesman comes under the manager else false
     * @throws DataAccessException
     */
    public boolean isSalesManUnderManager(String salesmanCode, String mgrSalesmanCode) throws DataAccessException;
    
    /**
     * Checks whether the given salesman code is a salesmanager
     * 
     * @param salesmanCode
     * @return boolean true if salesmancode is manager else false
     * @throws DataAccessException
     */
    public boolean isSalesManager(String salesmanCode) throws DataAccessException ;
}
