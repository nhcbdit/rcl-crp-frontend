/*
-------------------------------------------------------------------------------------------------------------
TosNewTariffSetupSearchDao.java
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Dhruv Parekh 25/05/2012
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY �User- -TaskRef- -ShortDescription-
-------------------------------------------------------------------------------------------------------------
*/

package com.rclgroup.dolphin.web.dao.tos;

import com.rclgroup.dolphin.web.common.RriStandardDao;

import com.rclgroup.dolphin.web.model.tos.TosNewTariffSetupSearchMod;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TosNewTariffSetupSearchDao extends RriStandardDao{
    
    /**
     * @param port
     * @param terminal    
     * @return List
     * @throws DataAccessException
     */
     
     public List getPortTermList(String port, String Terminal, String sortBy, String sortIn) throws DataAccessException;
}
