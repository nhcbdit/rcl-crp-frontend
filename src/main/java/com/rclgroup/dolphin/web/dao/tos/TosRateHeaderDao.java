/*-----------------------------------------------------------------------------------------------------------  
TosRateHeaderDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Panadda P. 06/06/2012
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.tos;

import java.util.List;

import org.springframework.dao.DataAccessException;

public interface TosRateHeaderDao {
    /**
     * list Service records for help screen
     * @param find
     * @param search
     * @param wild
     * @return list of Service
     * @throws DataAccessException
     */
    public List listForHelpScreen(String find,String search,String wild, String port, String terminal) throws DataAccessException;
}
