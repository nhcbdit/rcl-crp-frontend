/*-----------------------------------------------------------------------------------------------------------  
CtfGrcTrackingHeaderDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Porntip Aramrattana 05/11/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  
package com.rclgroup.dolphin.web.dao.ctf;

import java.util.List;
import org.springframework.dao.DataAccessException;

public interface CtfGrcTrackingHeaderDao {
    
    /**
     * 
     * @param grcTrackingNo
     * @return valid of GRCTrackingNo
     * @throws DataAccessException
     */
    public boolean isValid(String grcTrackingNo) throws DataAccessException;
    
    /**
     * 
     * @param rowAt
     * @param rowTo
     * @param cocSoc
     * @param rateChgFrm
     * @param rateChgType
     * @param grcNo
     * @param fromDate
     * @param toDate
     * @param status
     * @param sortBy
     * @param sortIn
     * @return List's CtfGrcTrackingHeaderMod
     * @throws DataAccessException
     */
    public List listForGrcTrackingReport(int rowAt,int rowTo,String cocSoc,String rateChgFrm,String rateChgType,String grcNo,String fromDate,String toDate,String status,String sortBy,String sortIn) throws DataAccessException;
    
    /**
     * 
     * @param cocSoc
     * @param rateChgFrm
     * @param rateChgType
     * @param grcNo
     * @param fromDate
     * @param toDate
     * @param status
     * @param sortBy
     * @param sortIn
     * @return
     * @throws DataAccessException
     */
    public int countListForGrcTrackingReport(String cocSoc,String rateChgFrm,String rateChgType,String grcNo,String fromDate,String toDate,String status,String sortBy,String sortIn) throws DataAccessException;

}
