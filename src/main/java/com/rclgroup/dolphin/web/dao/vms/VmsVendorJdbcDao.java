 /*------------------------------------------------------
 VmsVendorJdbcDao.java
 --------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 --------------------------------------------------------
 Author Sukit Narinsakchai 10/10/2007   
 - Change Log -------------------------------------------
 ## DD/MM/YY �User- -TaskRef- -ShortDescription-
 --------------------------------------------------------
 */

package com.rclgroup.dolphin.web.dao.vms;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.vms.VmsVendorMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class VmsVendorJdbcDao extends RrcStandardDao implements VmsVendorDao{
    public VmsVendorJdbcDao() {
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    
    public boolean isValid(String vendorCode) throws DataAccessException {
       System.out.println("[VmsVendorJdbcDao][isValid]: Started");
       boolean isValid = false;
       StringBuffer sql = new StringBuffer();
       sql.append("SELECT VENDOR_CODE ");
       sql.append("FROM VR_VMS_VENDOR_MASTER ");
       sql.append("WHERE VENDOR_CODE = :vendorCode");        
       SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
            Collections.singletonMap("vendorCode", vendorCode));
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        
        System.out.println("[VmsVendorJdbcDao][isValid]: Finished");
        return isValid;
    }
      
    public boolean isValid(String vendorCode, String status) throws DataAccessException {
        System.out.println("[VmsVendorJdbcDao][isValid]: With status: Started");
        boolean isValid = false;
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VENDOR_CODE ");
        sql.append("FROM VR_VMS_VENDOR_MASTER ");
        sql.append("WHERE VENDOR_CODE = :vendorCode");   
        sql.append("AND STATUS = :status ");
        
        HashMap map = new HashMap();
        map.put("vendorCode",vendorCode);
        map.put("status",status);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[VmsVendorJdbcDao][isValid]: Finished");
        return isValid;
    }   
    
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException {
        System.out.println("[VmsVendorJdbcDao][listForHelpScreen]: Started");
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT VENDOR_CODE ");
        sql.append("       ,VENDOR_NAME ");
        sql.append("       ,CITY ");
        sql.append("       ,VCSTAT "); 
        sql.append("       ,COUNTRY ");
        sql.append("       ,ZIPCODE ");
        sql.append("       ,PHONENO ");
        sql.append("       ,FAXNO ");
        sql.append("       ,EMAIL ");
        sql.append("       ,CONTACT_NAME ");
        sql.append("       ,TITLE ");
        sql.append("       ,STATUS ");
        sql.append(" FROM VR_VMS_VENDOR_MASTER ");         
        sql.append(sqlCriteria);        
        System.out.println("[VmsVendorJdbcDao][listForHelpScreen]: SQL = " + sql.toString());
        System.out.println("[VmsVendorJdbcDao][listForHelpScreen]: Finished");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   new HashMap(),
                   new RowModMapper());
    }
    
    private String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("C")){
                sqlCriteria = "WHERE VENDOR_CODE " + sqlWild;
            }else if(search.equalsIgnoreCase("N")){
                sqlCriteria = "WHERE VENDOR_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("CT")){
                sqlCriteria = "WHERE CITY " + sqlWild;
            }else if(search.equalsIgnoreCase("ST")){
                sqlCriteria = "WHERE VCSTAT " + sqlWild;
            }else if(search.equalsIgnoreCase("CC")){
                    sqlCriteria = "WHERE COUNTRY " + sqlWild;
            }else if(search.equalsIgnoreCase("Z")){
                sqlCriteria = "WHERE ZIPCODE " + sqlWild;
            }else if(search.equalsIgnoreCase("P")){
                sqlCriteria = "WHERE PHONENO " + sqlWild;
            }else if(search.equalsIgnoreCase("F")){
                sqlCriteria = "WHERE FAXNO " + sqlWild;
            }else if(search.equalsIgnoreCase("E")){
                sqlCriteria = "WHERE EMAIL " + sqlWild;
            }else if(search.equalsIgnoreCase("CN")){
                sqlCriteria = "WHERE CONTACT_NAME " + sqlWild;
            }else if(search.equalsIgnoreCase("T")){
                sqlCriteria = "WHERE TITLE " + sqlWild;
            }else if(search.equalsIgnoreCase("S")){
                sqlCriteria = "WHERE STATUS " + sqlWild;
            }        
        }
        return sqlCriteria;
    }        
   
    protected class RowModMapper implements RowMapper{
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return moveDbToModelForHelp(rs);
        }
    } 
    
    
    private VmsVendorMod moveDbToModelForHelp(ResultSet rs) throws SQLException {
        VmsVendorMod vendor = new VmsVendorMod();
        vendor.setVendorCode(RutString.nullToStr(rs.getString("VENDOR_CODE")));   
        vendor.setVendorName(RutString.nullToStr(rs.getString("VENDOR_NAME")));        
        vendor.setCity(RutString.nullToStr(rs.getString("CITY")));
        vendor.setState(RutString.nullToStr(rs.getString("VCSTAT")));
        vendor.setCountry(RutString.nullToStr(rs.getString("COUNTRY"))); 
        vendor.setZipCode(RutString.nullToStr(rs.getString("ZIPCODE")));        
        vendor.setPhone(RutString.nullToStr(rs.getString("PHONENO")));
        vendor.setFax(RutString.nullToStr(rs.getString("FAXNO")));
        vendor.setEmail(RutString.nullToStr(rs.getString("EMAIL")));
        vendor.setContactName(RutString.nullToStr(rs.getString("CONTACT_NAME")));
        vendor.setTitle(RutString.nullToStr(rs.getString("TITLE")));
        vendor.setStatus(RutString.nullToStr(rs.getString("STATUS")));
        
        return vendor;
    }  
}
