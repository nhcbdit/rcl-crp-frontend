/*-----------------------------------------------------------------------------------------------------------  
FarBillingStatementAndInvoiceLinkageJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsiisakun 17/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/  

package com.rclgroup.dolphin.web.dao.far;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.model.far.FarBillingStatementAndInvoiceLiknkageMod;
import com.rclgroup.dolphin.web.util.RutString;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class FarBillingStatementAndInvoiceLinkageJdbcDao extends RrcStandardDao implements FarBillingStatementAndInvoiceLinkageDao{
    public FarBillingStatementAndInvoiceLinkageJdbcDao() {
    
    }
    protected void initDao() throws Exception {
        super.initDao();
    }
    public List listForHelpScreen(String find, String search, String wild) throws DataAccessException{
        String sqlCriteria = createSqlCriteria(find, search, wild);
        StringBuffer sql = new StringBuffer();
         
        sql.append(" SELECT BILL_NUM ");
        sql.append(" ,BILL_VERSION ");
        sql.append(" ,BILL_TO_PARTY ");
        sql.append(" ,BILL_TO_PARTY_NAME ");
        sql.append(" ,DESCRIPTION ");
        sql.append(" ,BILL_STATUS ");
        sql.append(" ,FSC ");

        sql.append(" FROM VR_FAR_STATEMENT_BY_HEADER");        
        sql.append(" WHERE 1 = 1 ");  
        
        sql.append(sqlCriteria);
        HashMap map = new HashMap();                
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
        new RowMapper(){
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                FarBillingStatementAndInvoiceLiknkageMod mod = new FarBillingStatementAndInvoiceLiknkageMod();
                mod.setInvLinkNum(RutString.nullToStr(rs.getString("BILL_NUM")));
                mod.setInvLinkParty(RutString.nullToStr(rs.getString("BILL_TO_PARTY")));                      
                mod.setInvLinkPartyName(RutString.nullToStr(rs.getString("BILL_TO_PARTY_NAME")));
                mod.setInvLinkDesc(RutString.nullToStr(rs.getString("DESCRIPTION")));
                mod.setInvLinkFsc(RutString.nullToStr(rs.getString("FSC")));                      
                mod.setInvLinkStatus(RutString.nullToStr(rs.getString("BILL_STATUS")));
                mod.setInvLinkBillVersion((rs.getInt("BILL_VERSION")));
                return mod;
            }
        });
    }
    public String createSqlCriteria(String find, String search, String wild) {
        String sqlCriteria = "";
        String sqlWild = "";

        if(wild.equalsIgnoreCase("ON")){
            sqlWild = "LIKE '%" + find.toUpperCase() + "%' ";
        }else{
            sqlWild = "= '" + find.toUpperCase() + "' ";
        }
        if(find.trim().length() == 0){
        }else{
            if(search.equalsIgnoreCase("A")){
                sqlCriteria = "AND UPPER(BILL_NUM) " + sqlWild;
            }else if(search.equalsIgnoreCase("B")){
                sqlCriteria = "AND BILL_VERSION " + sqlWild;            
            }else if(search.equalsIgnoreCase("C")){
                sqlCriteria = "AND UPPER(BILL_TO_PARTY) " + sqlWild;
            } else if(search.equalsIgnoreCase("D")){
                sqlCriteria = "AND UPPER(BILL_TO_PARTY_NAME) " + sqlWild;            
            }else if(search.equalsIgnoreCase("E")){
                sqlCriteria = "AND UPPER(DESCRIPTION) " + sqlWild;
            }else if(search.equalsIgnoreCase("F")){
                sqlCriteria = "AND UPPER(BILL_STATUS) " + sqlWild;            
            }else if(search.equalsIgnoreCase("G")){
                sqlCriteria = "AND UPPER(FSC) " + sqlWild;
            }       
        }
        return sqlCriteria;
    }
    public boolean isValidBillNo(String billNo) throws DataAccessException{
        System.out.println("[FarBillingStatementAndInvoiceLinkageJdbcDao][isValid]: Started");
        StringBuffer sql = new StringBuffer();
        boolean isValid = false;
        sql.append(" SELECT BILL_NUM ");

        sql.append(" FROM VR_FAR_STATEMENT_BY_HEADER");        
        sql.append(" WHERE BILL_NUM = :billNo ");  
    
        HashMap map = new HashMap();
        map.put("billNo",billNo);
        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),map);
        if(rs.next()) {
            isValid = true;
        } else { 
            isValid = false;
        }
        System.out.println("[FarBillingStatementAndInvoiceLinkageJdbcDao][isValid]: Finished");
        return isValid;
        }
}
  
