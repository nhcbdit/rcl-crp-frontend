/*-----------------------------------------------------------------------------------------------------------  
BsaBsaServiceVariantVolumePortCallJdbcDao.java
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2007 
-------------------------------------------------------------------------------------------------------------
Author Kitti Pongsirisakun 12/06/09
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-      -Short Description  
-----------------------------------------------------------------------------------------------------------*/

package com.rclgroup.dolphin.web.dao.bsa;

import com.rclgroup.dolphin.web.common.RrcStandardDao;
import com.rclgroup.dolphin.web.common.RrcStandardMod;
import com.rclgroup.dolphin.web.exception.CustomDataAccessException;
import com.rclgroup.dolphin.web.model.bsa.BsaAdditionPortCallMod;
import com.rclgroup.dolphin.web.util.RutDatabase;
import com.rclgroup.dolphin.web.util.RutOperationFlagManager;
import com.rclgroup.dolphin.web.util.RutString;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class BsaBsaServiceVariantVolumeAddPortCallJdbcDao extends RrcStandardDao implements BsaBsaServiceVariantVolumeAddPortCallDao {
    
//    private InsertStoreProcedure insertStoreProcedure;
//    private UpdateStoreProcedure updateStoreProcedure;
//    private DeleteStoreProcedure deleteStoreProcedure;
//    private CheckConstraintStoreProcedure checkConstraintStoreProcedure;
//    private BsaSupportedPortGroupDao bsaSupportedPortGroupDao;
    
    public BsaBsaServiceVariantVolumeAddPortCallJdbcDao() {
    }
//    
//    public void setBsaSupportedPortGroupDao(BsaSupportedPortGroupDao bsaSupportedPortGroupDao) {
//        this.bsaSupportedPortGroupDao = bsaSupportedPortGroupDao;
//    }
    
    protected void initDao() throws Exception {
        super.initDao();
//        insertStoreProcedure = new InsertStoreProcedure(getJdbcTemplate());
//        updateStoreProcedure = new UpdateStoreProcedure(getJdbcTemplate());
//        deleteStoreProcedure = new DeleteStoreProcedure(getJdbcTemplate());
//        checkConstraintStoreProcedure = new CheckConstraintStoreProcedure(getJdbcTemplate());
    }
    
//    public boolean insert(RrcStandardMod mod) throws DataAccessException {
//        mod.setRecordAddUser(this.getUserId());
//        mod.setRecordChangeUser(this.getUserId());
//        return insertStoreProcedure.insert(mod);
//    }

//    public boolean update(RrcStandardMod mod) throws DataAccessException {
//        mod.setRecordChangeUser(this.getUserId());
//        return updateStoreProcedure.update(mod);
//    }

//    public boolean delete(RrcStandardMod mod) throws DataAccessException {
//        return deleteStoreProcedure.delete(mod);
//    }
//    
//    public void checkConstraint(RrcStandardMod mod) throws DataAccessException{
////        checkConstraintStoreProcedure.checkConstraint(mod);
//    }
    
//    protected class InsertStoreProcedure extends StoredProcedure {
//        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_INS_BSA_ADD_PORT_CALL";
//     
//        protected InsertStoreProcedure(JdbcTemplate jdbcTemplate) {
//            super(jdbcTemplate, STORED_PROCEDURE_NAME);
//            declareParameter(new SqlInOutParameter("p_bsa_port_group_id", Types.INTEGER));
//            declareParameter(new SqlInOutParameter("p_bsa_model_id", Types.INTEGER));
//            declareParameter(new SqlInOutParameter("p_port_group_code", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_port_grp_soc_coc", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_service", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_record_add_user", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_record_add_date", Types.TIMESTAMP));
//            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
//            compile();
//        }
//
//        protected boolean insert(RrcStandardMod mod) {
//            return insert(mod,mod);
//        }
// 
//        protected boolean insert(final RrcStandardMod inputMod,RrcStandardMod outputMod) {
//            boolean isSuccess = false; 
//            /*
//            if((inputMod instanceof BsaAdditionPortCallMod)&&(outputMod instanceof BsaAdditionPortCallMod)){
//                BsaAdditionPortCallMod aInputMod = (BsaAdditionPortCallMod)inputMod;
//                BsaAdditionPortCallMod aOutputMod = (BsaAdditionPortCallMod)outputMod;
//                Map inParameters = new HashMap(10);
//    ////
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_bsa_port_group_id:"+aInputMod.getBsaSupportedPortGroupId());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_bsa_model_id:"+aInputMod.getBsaModelId());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_port_group_code:"+aInputMod.getPortGroupCode());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_port_grp_soc_coc:"+aInputMod.getPortGroupSocCoc());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_service:"+aInputMod.getService());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_status:"+aInputMod.getRecordStatus());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_add_user:"+aInputMod.getRecordAddUser());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_add_date:"+aInputMod.getRecordAddDate());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_change_user:"+aInputMod.getRecordChangeUser());
//                System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][InsertStoreProcedure][insert]:p_record_change_date:"+aInputMod.getRecordChangeDate());
//    ////
//
//                inParameters.put("p_bsa_port_group_id", new Integer((RutString.nullToStr(aInputMod.getBsaSupportedPortGroupId())).equals("")?"0":RutString.nullToStr(aInputMod.getBsaSupportedPortGroupId())));
//                inParameters.put("p_bsa_model_id", aInputMod.getBsaModelId());
//                inParameters.put("p_port_group_code", aInputMod.getPortGroupCode());
//                inParameters.put("p_port_grp_soc_coc", aInputMod.getPortGroupSocCoc());
//                inParameters.put("p_service", aInputMod.getService());
//                inParameters.put("p_record_status", aInputMod.getRecordStatus());
//                inParameters.put("p_record_add_user", aInputMod.getRecordAddUser());
//                inParameters.put("p_record_add_date", aInputMod.getRecordAddDate());
//                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
//                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
//                Map outParameters = execute(inParameters);
//                if (outParameters.size() > 0) {
//                    isSuccess = true;
//                    aOutputMod.setBsaSupportedPortGroupId((RutString.nullToStr(((Integer)outParameters.get("p_bsa_port_group_id")).toString())));
//                    aOutputMod.setBsaModelId((RutString.nullToStr(((Integer)outParameters.get("p_bsa_model_id")).toString())));
//                    aOutputMod.setPortGroupCode((RutString.nullToStr((String)outParameters.get("p_port_group_code"))));
//                    aOutputMod.setPortGroupSocCoc((RutString.nullToStr((String)outParameters.get("p_port_grp_soc_coc"))));
//                    aOutputMod.setService((RutString.nullToStr((String)outParameters.get("p_service"))));
//                    aOutputMod.setRecordStatus((RutString.nullToStr((String)outParameters.get("p_record_status"))));
//                    aOutputMod.setRecordAddUser((RutString.nullToStr((String)outParameters.get("p_record_add_user"))));
//                    aOutputMod.setRecordAddDate((((Timestamp)outParameters.get("p_record_add_date"))));
//                    aOutputMod.setRecordChangeUser((RutString.nullToStr((String)outParameters.get("p_record_change_user"))));
//                    aOutputMod.setRecordChangeDate((((Timestamp)outParameters.get("p_record_change_date"))));
//                }
//            }
//            */
//            return isSuccess;
//        }
//    }
//
//    protected class UpdateStoreProcedure extends StoredProcedure {
//        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_UPD_BSA_ADD_PORT_CALL";
//        
//        protected UpdateStoreProcedure(JdbcTemplate jdbcTemplate) {
//            super(jdbcTemplate, STORED_PROCEDURE_NAME);
//            
//            declareParameter(new SqlInOutParameter("p_bsa_port_call_id", Types.INTEGER));
//            declareParameter(new SqlInOutParameter("p_bsa_service_variant_id", Types.INTEGER));
//            declareParameter(new SqlInOutParameter("p_port",Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_load_discharge_flag", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_transhipment_flag", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_slot_teu", Types.INTEGER)); 
//            declareParameter(new SqlInOutParameter("p_slot_tons", Types.INTEGER)); 
//            declareParameter(new SqlInOutParameter("p_min_teu", Types.INTEGER)); 
//            declareParameter(new SqlInOutParameter("p_record_status", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_record_change_user", Types.VARCHAR));
//            declareParameter(new SqlInOutParameter("p_record_change_date", Types.TIMESTAMP));
//            compile();
//        }
//        
//        protected boolean update(RrcStandardMod mod ) {
//            return update(mod,mod);
//        }
//        
//        protected boolean update(final RrcStandardMod inputMod,RrcStandardMod outputMod ) {
//            boolean isSuccess = false;
////            if((inputMod instanceof BsaPortCallMod)&&(outputMod instanceof BsaPortCallMod)){
////                BsaPortCallMod aInputMod = (BsaPortCallMod)inputMod;
////                BsaPortCallMod aOutputMod = (BsaPortCallMod)outputMod;
////                Map inParameters = new HashMap();
////     
////                  
////        
////                inParameters.put("p_bsa_port_call_id", RutDatabase.integerToDb(aInputMod.getPortCallID()));
////                inParameters.put("p_bsa_service_variant_id", RutDatabase.integerToDb(aInputMod.getVariantID()));
////                inParameters.put("p_service", RutDatabase.stringToDb(aInputMod.getService()));
////                inParameters.put("p_proforma_ref_no", RutDatabase.stringToDb(aInputMod.getProformaRefNo()));
////                inParameters.put("p_port", RutDatabase.stringToDb(aInputMod.getPort()));
////                inParameters.put("p_port_seq_no_from",RutDatabase.integerToDb(aInputMod.getPortSeqFrom()));
////                inParameters.put("p_direction_from", RutDatabase.stringToDb(aInputMod.getDirFrom()));
////               
////              
////                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis().substring(0,1)) ;
//////                String loadDis = RutDatabase.stringToDb(aInputMod.getLoadDis()) ;
////                System.out.println("Load Dis " + loadDis);
////                
////                String tranship = RutDatabase.stringToDb(aInputMod.getTranShip().substring(0,1));
//////                    String tranship = RutDatabase.stringToDb(aInputMod.getTranShip());
////                System.out.println("tranship Flag " + tranship);
////                
////                
////                   
////                    if(aInputMod.getSupportPortGrpId().equals("")){
////                        inParameters.put("p_supported_port_group_id",null);
////                    }else{
////                        inParameters.put("p_supported_port_group_id",RutDatabase.integerToDb(aInputMod.getSupportPortGrpId()));
////                    }
////                   
////                
////                
//////                inParameters.put("p_supported_port_group_id",RutDatabase.integerToDb(aInputMod.getSupportPortGrpId()));
////                inParameters.put("p_port_group_code",RutDatabase.stringToDb(aInputMod.getPortGrp()));
////                inParameters.put("p_port_seq_no_to",RutDatabase.integerToDb(aInputMod.getPortSeqTo()));
////                inParameters.put("p_direction_to",RutDatabase.stringToDb(aInputMod.getDirTo())) ;
////                
////                inParameters.put("p_load_discharge_flag", loadDis) ;
////                inParameters.put("p_transhipment_flag",tranship);
////                inParameters.put("p_wayport_trunk_indicator", RutDatabase.stringToDb(aInputMod.getTrunkInd()));
////                inParameters.put("p_port_call_level", RutDatabase.stringToDb(aInputMod.getSubCode()));
////                
////                inParameters.put("p_slot_teu", RutDatabase.integerToDb(aInputMod.getSlotTeu()));
////                inParameters.put("p_slot_tons", RutDatabase.integerToDb(aInputMod.getSlotTon()));
////                inParameters.put("p_slot_reefer_plugs",RutDatabase.integerToDb(aInputMod.getSlotRef()));
////                inParameters.put("p_avg_coc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getAvgCoc()));
////                inParameters.put("p_avg_soc_teu_weight", RutDatabase.bigDecimalToDb(aInputMod.getAvgSoc()));
////                inParameters.put("p_min_teu",RutDatabase.integerToDb(aInputMod.getMinTeu()));
////                
////                
////                inParameters.put("p_record_status", RutDatabase.stringToDb(aInputMod.getRecordStatus()));
////                inParameters.put("p_record_change_user", RutDatabase.stringToDb(aInputMod.getRecordChangeUser()));
////                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
////                
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_bsa_port_call_id = "+inParameters.get("p_bsa_port_call_id"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_bsa_service_variant_id = "+inParameters.get("p_bsa_service_variant_id"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_service = "+inParameters.get("p_service"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_proforma_ref_no = "+inParameters.get("p_proforma_ref_no"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port = "+inParameters.get("p_port"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_seq_no_from = "+inParameters.get("p_port_seq_no_from"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_direction_from = "+inParameters.get("p_direction_from"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_supported_port_group_id = "+inParameters.get("p_supported_port_group_id"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_group_code = "+inParameters.get("p_port_group_code"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_seq_no_to = "+inParameters.get("p_port_seq_no_to"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_direction_to = "+inParameters.get("p_direction_to"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_load_discharge_flag = "+inParameters.get("p_load_discharge_flag"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_transhipment_flag = "+inParameters.get("p_transhipment_flag"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_wayport_trunk_indicator = "+inParameters.get("p_wayport_trunk_indicator"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_port_call_level = "+inParameters.get("p_port_call_level"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_teu = "+inParameters.get("p_slot_teu")); 
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_tons = "+inParameters.get("p_slot_tons"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_slot_reefer_plugs = "+inParameters.get("p_slot_reefer_plugs"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_avg_coc_teu_weight = "+inParameters.get("p_avg_coc_teu_weight"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_avg_soc_teu_weight = "+inParameters.get("p_avg_soc_teu_weight"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_min_teu = "+inParameters.get("p_min_teu"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_status = "+inParameters.get("p_record_status"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_user = "+inParameters.get("p_record_change_user"));
////                    System.out.println("[BsaBsaServiceVariantVolumePortCallJdbcDao][UpdateStoreProcedure][update]: p_record_change_date = "+inParameters.get("p_record_change_date"));
////                
////                
////                
////                
////                
////                
////
////                Map outParameters = execute(inParameters);
////                if (outParameters.size() > 0) {
////                    isSuccess = true;
////                    
////                    aOutputMod.setPortCallID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_port_call_id"));
////                    aOutputMod.setVariantID(RutDatabase.dbToStrInteger(outParameters, "p_bsa_service_variant_id"));
////                    aOutputMod.setService(RutDatabase.dbToString(outParameters, "p_service"));
////                    aOutputMod.setProformaRefNo(RutDatabase.dbToString(outParameters, "p_proforma_ref_no"));
////                    aOutputMod.setPort(RutDatabase.dbToString(outParameters, "p_port"));
////                    aOutputMod.setPortSeqFrom(RutDatabase.dbToStrInteger(outParameters, "p_port_seq_no_from"));
////                    aOutputMod.setDirFrom(RutDatabase.dbToString(outParameters, "p_direction_from"));
////                    
////                    aOutputMod.setSupportPortGrpId(RutDatabase.dbToStrInteger(outParameters, "p_supported_port_group_id"));
////                    aOutputMod.setPortGrp(RutDatabase.dbToString(outParameters, "p_port_group_code"));
////                    aOutputMod.setPortSeqTo(RutDatabase.dbToStrInteger(outParameters, "p_port_seq_no_to"));
////                    aOutputMod.setDirTo(RutDatabase.dbToString(outParameters, "p_direction_to"));
////                    
////                    aOutputMod.setLoadDis(RutDatabase.dbToString(outParameters, "p_load_discharge_flag"));
////                    aOutputMod.setTranShip(RutDatabase.dbToString(outParameters, "p_transhipment_flag"));
////                    aOutputMod.setTrunkInd(RutDatabase.dbToString(outParameters, "p_wayport_trunk_indicator"));
////                    aOutputMod.setSubCode(RutDatabase.dbToString(outParameters, "p_port_call_level"));
////                    
////                    aOutputMod.setSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_slot_teu"));
////                    aOutputMod.setSlotTon(RutDatabase.dbToStrInteger(outParameters, "p_slot_tons"));
////                    aOutputMod.setSlotRef(RutDatabase.dbToStrInteger(outParameters, "p_slot_reefer_plugs"));
////                    aOutputMod.setAvgCoc(RutDatabase.dbToStrInteger(outParameters, "p_avg_coc_teu_weight"));
////                    aOutputMod.setAvgSoc(RutDatabase.dbToStrInteger(outParameters, "p_avg_soc_teu_weight"));
////                    aOutputMod.setMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_min_teu"));
////                    
////                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
////                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
////                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
////                    
////                    /*
////                    aOutputMod.setCalcFrequency(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_frequency"));
////                    aOutputMod.setCalcDuration(RutDatabase.dbToStrBigDecimal(outParameters, "p_calc_duration"));
////                    aOutputMod.setDefSlotTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_teu"));
////                    aOutputMod.setDefSlotTons(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_tons"));
////                    aOutputMod.setDefSlotReefer(RutDatabase.dbToStrInteger(outParameters, "p_def_slot_reefer_plugs"));
////                    aOutputMod.setDefAvgCocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_coc_teu_weight"));
////                    aOutputMod.setDefAvgSocTeuWeight(RutDatabase.dbToStrBigDecimal(outParameters, "p_def_avg_soc_teu_weight"));
////                    aOutputMod.setDefMinTeu(RutDatabase.dbToStrInteger(outParameters, "p_def_min_teu"));
////                    aOutputMod.setRecordStatus(RutDatabase.dbToString(outParameters, "p_record_status"));
////                    aOutputMod.setRecordChangeUser(RutDatabase.dbToString(outParameters, "p_record_change_user"));
////                    aOutputMod.setRecordChangeDate(RutDatabase.dbToTimestamp(outParameters, "p_record_change_date"));
////                    */
////                } 
////                }
//                return isSuccess;
//                      
//        }
//    }
//    
//    public void saveMasterDetails(RrcStandardMod masterMod, String masterOperationFlag, HashMap isDeletes) throws DataAccessException {
//        BsaAdditionPortCallMod mod = (BsaAdditionPortCallMod) masterMod;
//        
//        StringBuffer errorMsgBuffer = new StringBuffer();
//        try {
//            if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_INSERT)) {
//                //insert(masterMod);
//            } else if (masterOperationFlag.equals(RutOperationFlagManager.OPERATION_FLAG_UPDATE)) {
//                //update(masterMod);
//            }
//        } catch(CustomDataAccessException e) {
//            errorMsgBuffer.append(e.getMessages()+"&");
//        } catch(DataAccessException e) {
//            e.printStackTrace();
//        }
//        
//        //begin: delete record of bsa service variant
//        if (isDeletes != null && !isDeletes.isEmpty()) { //if 1
//            BsaAdditionPortCallMod bean = null;
//            ArrayList listDelete = new ArrayList(isDeletes.values());
//            for (int i=0;i<listDelete.size();i++) { //for 1
//                bean = (BsaAdditionPortCallMod) listDelete.get(i);
//                this.delete(bean);
//                
//            } //end for 1
//        } //end if 1
//        //end: delete record of bsa service variant
//    }
//    
//    
//    
//    
//    protected class DeleteStoreProcedure extends StoredProcedure {
//        private static final String STORED_PROCEDURE_NAME = "PCR_BSA_PORT_CALL.PRR_DEL_BSA_ADD_PORT_CALL";
//     
//        protected DeleteStoreProcedure(JdbcTemplate jdbcTemplate) {
//            super(jdbcTemplate, STORED_PROCEDURE_NAME);
//            declareParameter(new SqlParameter("p_bsa_port_call_id", Types.INTEGER));
//            declareParameter(new SqlParameter("p_record_change_user", Types.VARCHAR));
//            declareParameter(new SqlParameter("p_record_change_date", Types.TIMESTAMP));
//            compile();
//        }
//     
//        protected boolean delete(final RrcStandardMod inputMod) {
//            boolean isSuccess = false;
//            if(inputMod instanceof BsaAdditionPortCallMod){
//                BsaAdditionPortCallMod aInputMod = (BsaAdditionPortCallMod)inputMod;
//                Map inParameters = new HashMap(3);
//    ////
//                System.out.println("[BsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_bsa_port_group_id:"+aInputMod.getPortCallID());
//                System.out.println("[BsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_user:"+aInputMod.getRecordChangeUser());
//                System.out.println("[BsaSupportedPortGroupJdbcDao][DeleteStoreProcedure][delete]:p_record_change_date:"+aInputMod.getRecordChangeDate());
//    ////
//                inParameters.put("p_bsa_port_call_id", new Integer((RutString.nullToStr(aInputMod.getPortCallID())).equals("")?"0":RutString.nullToStr(aInputMod.getPortCallID())));
//                inParameters.put("p_record_change_user", aInputMod.getRecordChangeUser());
//                inParameters.put("p_record_change_date", aInputMod.getRecordChangeDate());
//                execute(inParameters);
//                isSuccess = true;
//            }
//            return isSuccess;
//        }
//    }
//    
//    
//    
//    
//    
//    public String getModelId(String bsaModelName) {
//        System.out.println("[BsaBsaServiceVariantJdbcDao][getModelId]: Started");
//        String bsaModelId = "";
//        StringBuffer sql = new StringBuffer();
//        sql.append("SELECT BSA_MODEL_ID ");
//        sql.append("FROM VR_BSA_BASE_ALLOCATION_MODEL ");
//        sql.append("WHERE MODEL_NAME = :bsaModelName");
//        SqlRowSet rs = getNamedParameterJdbcTemplate().queryForRowSet(sql.toString(),
//            Collections.singletonMap("bsaModelName",bsaModelName ));
//        if(rs.next()) {
//            bsaModelId = RutString.nullToStr(rs.getString("BSA_MODEL_ID"));
//        }
//        System.out.println("modelname :"+ bsaModelName);
//        System.out.println("bsaModelId"+ bsaModelId);
//        System.out.println("[EmsEquipmentSysValidateJdbcDao][getModelId]: Finished");
//        return bsaModelId;
//}
    public List listAddPortCallFindByKeyBsaServiceVariantID(String bsaServiceVariantId) throws DataAccessException{
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ADD_PORT_CALL.PK_BSA_PORT_CALL_ID ");
        sql.append("      ,ADD_PORT_CALL.BSA_SERVICE_VARIANT_ID ");
        sql.append("      ,ADD_PORT_CALL.DN_PORT ");
        sql.append("      ,ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG ");    
        sql.append("      ,DECODE (ADD_PORT_CALL.DN_LOAD_DISCHARGE_FLAG , 'L','Load'" + 
        "                                          , 'D','Discharge'" + 
        "                                          ,'B' ,'Both'" + 
        "                                          ,'T' ,'Transit'" + 
        "                                          ,' ') DN_LOAD_DISCHARGE ");  
        sql.append("      ,ADD_PORT_CALL.DN_TRANSHIPMENT_FLAG ");        
        sql.append("      ,DECODE (ADD_PORT_CALL.DN_TRANSHIPMENT_FLAG , 'Y','Yes'" + 
        "                                          , 'N','No'" + 
        "                                          ,' ') DN_TRANSHIPMENT ");       
        sql.append("      ,ADD_PORT_CALL.TOLELANT ");
        sql.append("      ,ADD_PORT_CALL.SLOT_TEU ");
        sql.append("      ,ADD_PORT_CALL.SLOT_TONS ");
        sql.append("      ,ADD_PORT_CALL.MIN_TEU ");
        sql.append("      ,ADD_PORT_CALL.RECORD_STATUS ");
        sql.append("      ,ADD_PORT_CALL.RECORD_ADD_USER ");
        sql.append("      ,ADD_PORT_CALL.RECORD_ADD_DATE ");
        sql.append("      ,ADD_PORT_CALL.RECORD_CHANGE_USER ");
        sql.append("      ,ADD_PORT_CALL.RECORD_CHANGE_DATE ");
        sql.append("FROM BSA_ADD_PORT_CALL ADD_PORT_CALL ");
        sql.append("WHERE ADD_PORT_CALL.BSA_SERVICE_VARIANT_ID = :bsaServiceVariantId ");        
        sql.append("ORDER BY DN_PORT ");
        HashMap map = new HashMap();
        map.put("bsaServiceVariantId",bsaServiceVariantId);
        
        System.out.println("[BsaBsaServiceVariantVolumeAddPortCallJdbcDao][listAddPortCallFindByKeyBsaServiceVariantID]: sql: ["+sql.toString()+"]");
        return getNamedParameterJdbcTemplate().query(
                   sql.toString(),
                   map,
                   new RowMapper(){
                       public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                           BsaAdditionPortCallMod bsaAddPortCallMod = new BsaAdditionPortCallMod();
                           bsaAddPortCallMod.setPortCallID(RutString.nullToStr(rs.getString("PK_BSA_PORT_CALL_ID")));
                           bsaAddPortCallMod.setVariantID(RutString.nullToStr(rs.getString("BSA_SERVICE_VARIANT_ID")));
                           bsaAddPortCallMod.setAddPort(RutString.nullToStr(rs.getString("DN_PORT")));System.out.println(">>>bsaPortCallMod.getPort() "+bsaAddPortCallMod.getAddPort());
//                           bsaAddPortCallMod.setAddLoadDis(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE_FLAG")));//System.out.println(">>>bsaPortCallMod.getLoadDis() "+bsaPortCallMod.getLoadDis());
                           bsaAddPortCallMod.setAddTranShipFlag(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT_FLAG")));//System.out.println(">>>bsaPortCallMod.getTranShip() "+bsaPortCallMod.getTranShip());
                           bsaAddPortCallMod.setAddLoadDisFlag(RutString.nullToStr(rs.getString("DN_LOAD_DISCHARGE")));//System.out.println(">>>bsaPortCallMod.getLoadDis() "+bsaPortCallMod.getLoadDis());
//                           bsaAddPortCallMod.setAddTranShip(RutString.nullToStr(rs.getString("DN_TRANSHIPMENT")));//System.out.println(">>>bsaPortCallMod.getTranShip() "+bsaPortCallMod.getTranShip());
                           bsaAddPortCallMod.setAddTolelant(RutString.nullToStr(rs.getString("TOLELANT")));//System.out.println(">>>bsaPortCallMod.getAvgSoc() "+bsaPortCallMod.getAvgSoc());
                           bsaAddPortCallMod.setAddSlotTeu(RutString.nullToStr(rs.getString("SLOT_TEU")));//System.out.println(">>>bsaPortCallMod.getSlotTeu() "+bsaPortCallMod.getSlotTeu());
                           bsaAddPortCallMod.setAddSlotTon(RutString.nullToStr(rs.getString("SLOT_TONS")));//System.out.println(">>>bsaPortCallMod.getSlotTon() "+bsaPortCallMod.getSlotTon());   
                           bsaAddPortCallMod.setAddMinTeu(RutString.nullToStr(rs.getString("MIN_TEU")));//System.out.println(">>>bsaPortCallMod.getMinTeu() "+bsaPortCallMod.getMinTeu());
                           bsaAddPortCallMod.setRecordStatus(RutString.nullToStr(rs.getString("RECORD_STATUS")));//System.out.println(">>>bsaPortCallMod.getRecordStatus() "+bsaPortCallMod.getRecordStatus());
                           bsaAddPortCallMod.setRecordAddUser(RutString.nullToStr(rs.getString("RECORD_ADD_USER")));//System.out.println(">>>bsaPortCallMod.getRecordAddUser() "+bsaPortCallMod.getRecordAddUser());
                           bsaAddPortCallMod.setRecordAddDate(rs.getTimestamp("RECORD_ADD_DATE"));//System.out.println(">>>bsaPortCallMod.getRecordAddDate() "+bsaPortCallMod.getRecordAddDate());
                           bsaAddPortCallMod.setRecordChangeUser(RutString.nullToStr(rs.getString("RECORD_CHANGE_USER")));//System.out.println(">>>bsaPortCallMod.getRecordChangeUser() "+bsaPortCallMod.getRecordChangeUser());
                           bsaAddPortCallMod.setRecordChangeDate(rs.getTimestamp("RECORD_CHANGE_DATE"));//System.out.println(">>>bsaPortCallMod.getRecordChangeDate() "+bsaPortCallMod.getRecordChangeDate());
                          
                           return bsaAddPortCallMod;
                        }
                   });
    }
}
