// JavaScript source code

function showLoader(containerId) {

    hideLoader(containerId);

    var dialogHtml = '<div class="loader"></div>'

    $("#" + containerId).append(dialogHtml);
    
}

function hideLoader(containerId) {

    $("#" + containerId)
        .find('.loader')
        .remove();

}


