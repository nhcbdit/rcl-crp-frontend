//equipments: Equipment[], cargoAvailaDate: isoDateString, mtPickupDate: isoDateString
//return isoDateString
function getLatestPositioningDate(equipments, cargoAvailDate, mtPickupDate){
	if(cargoAvailDate){
		cargoAvailDate = isoToDate(cargoAvailDate);
	}
	if(mtPickupDate){
		mtPickupDate = isoToDate(mtPickupDate);
	}
	if(!equipments || equipments.length <= 0){
		if(cargoAvailDate && mtPickupDate){
			return dateToIso(cargoAvailDate < mtPickupDate ? mtPickupDate : cargoAvailDate);
		}
		if(cargoAvailDate){
			return dateToIso(cargoAvailDate);
		}
		if(mtPickupDate){
			return dateToIso(mtPickupDate);
		}
		return getCurrentServerDate();
	}
	
	var latestEquipmentDate = null;
	
	for(var i=0; i<equipments.length; i++){
		if(!equipments[i].supplier){
			continue;
		}
		for(var j=0; j<equipments[i].supplier.length; j++){
			var positioningDate = equipments[i].supplier[j].mtDate;
			if(!positioningDate){
				continue;
			}
			positioningDate = isoToDate(positioningDate);
			if(!latestEquipmentDate || latestEquipmentDate < positioningDate){
				latestEquipmentDate = positioningDate;
			}
		}
	}
	
	if(cargoAvailDate && latestEquipmentDate){
		return dateToIso(cargoAvailDate > latestEquipmentDate ? cargoAvailDate : latestEquipmentDate);
	}
	if(cargoAvailDate){
		return dateToIso(cargoAvailDate);
	}
	if(latestEquipmentDate){
		return dateToIso(latestEquipmentDate);
	}
	return getCurrentServerDate();
}