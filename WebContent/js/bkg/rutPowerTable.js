    /*
    concept sub tables
    ========================
    tagging as usual t6-blabla
    tableInitrequires that parent table is displayed
    => As many table Inits as there are rows

    tableInit
    ------------
    will take as row of id t7-row-12 as input
    will look, whether containerId has another tblArea as parent and will create and entry
    in settings saying, that this is a subtable of e.g. t0-container
    will shift suffix -12 to prefix so that all ids in the template now read
        t7-12-row, t7-12-container
    The prefix length of subtable set to 6 (t7-12-) and prefix being t7-12-

    */
    //See below marked Javadoc (/**) comments on what it does and how to apply
    /*-----------------------------------------------------------------------------------------------------------  
    rutPowerTable.js
    ------------------------------------------------------------------------------------------------------------- 
    Copyright RCL Public Co., Ltd. 2018 
    -------------------------------------------------------------------------------------------------------------
    Author Ascan Heydorn 13/08/2018
    - Change Log ------------------------------------------------------------------------------------------------  
    ## DD/MM/YY   -User-     -TaskRef-      -Short Description
    01 13/08/2018 aschey1                   Initial PoC
    02 02/10/2018 aschey1                   Inserted paging
    03 04/10/2018 aschey1                   Insert rows in between + change event handler with insertAndChanges list
    04 09/10/2018 aschey1                   Bugfix, when lookup table is used  and closed without select, also searhc without search input in lookup table
    05 09/10/2018 aschey1                   Suppress contentMenue SearchAndSort option for lookup tables 
    06 10/10/2018 aschey1                   Improve styling of buttons
    07 11/10/2018 aschey1                   Allow rptGetDataFromSingleArea in any case
    08 18/10/2018 aschey1                   Expanded addData to completely reset existing map and sortAndsearchDefinition
    09 18/10/2018 aschey1                   introduced nextRowIndex to avoid rowId isseus after inserting, deleting and inserting again data, remowed rowId bug in new
    10 18/10/2018 aschey1                   introduced change log for remove row
    11 18/10/2018 aschey1                   When inserting a new row all occurences of an id are replaced with the new id. This will also address "label for" topic
    12 19/10/2018 aschey1                   Change moveDatatoDisplay to use rptDisplayDetailArea
    13 20/10/2018 aschey1                   Added rptSaveAsCsv and added to sort display 
    14 20/10/2018 aschey1                   Added rutInitArea after insertion of a new row
    15 21/10/2018 aschey1                   Improved display of sort control
    16 24/10/2018 aschey1                   Fix duplicate id issues for insertRowBefore
    17 31/10/2018 aschey1                   Fix issue with varying row-id's, add row Id to map
    18 01/11/2018 aschey1                   Insert new function rptGetModelDataOfRowId
    19 03/11/2018 aschey1                   Changed in seach value.search with value.indexOf since '$' is a problem
    20 09/11/2018 aschey1                   Fix issues with the new types tel, email, url
    21 11/11/2018 aschey1                   Introduce automatic sync between tag and data
    22 12/12/2018 aschey1                   Fixed issues in display of paging buttons
    */

    /**
     * RCL Power Table - ConceptionalOverview<br>
     * This Javascript provides a generic facility for creating lists of records in an html page and fill it with data.
     * The tool requires a &lt;div&gt; (or any other suitable tag) of class "tblArea" (Bootstrap also need "container") to contain the whole list.
     * It must have an html ID and this id must have a 3-character prefix and all id's within the container shall have same prefix.<br>

     * Inside this containers it requires  at some place one &lt;div&gt; (or any other suitable tag) of class "tblRow" 
     * (Bootstrap also requires "row" or "form-row") to keep the template of one row of data. The row must also have an html ID with same prefix.
     * Within this template row each tag with data must have class "tblField" and an id of this format "&lt;prefix&gt;&lt;attributeNameOfDataInClass&gt;".<br>

     * After intitializing the tool with the function tableInit one can add the data to it with addData.
     * The data would consist of an array of classes. The attribute names within the class must correspond to the IDs of the tblField's (minus prefix). 
     * So if there is a tblField with ID "t0-booking" there should be an attribute named "booking".<br>

     * The tool supports &lt;input&gt;,&lt;p&gt;,&lt;span&gt;,&lt;td&gt;,&lt;textarea&gt;,&lt;select&gt; and any tag which contains text. For select the data value must be in the html attribute value of each option.
     * So when data is added it compares the it looks at &lt;option value=CN"&gt; China &lt;option&gt; it would select this option only, if the value in
     * the data is "CN".<br>

     * The PowerTable tool uses a global variable named rutPowerTableIndex<br>
     * It also uses a temporary global variable rutPowerTableInActiveSortSettings to store the settings of the PowerTable, which at this very moment is being sorted.
     * The tool will suppress display of the template row and will create copies of it for each row in the data array. Any html ID in the
     * template will be replaced in the copy with same ID-index, so ID="t0-booking" would become ID="t0-booking-0" ...
     * The new rows are inserted before the template row. So all elements of the container other than the template row will remain untouched.<br>
     * A special topic are id's. Since these must be unique the id's in the template receive a suffix with a running number per row. So id="t0-container" becomes 
     * id="t0-container-4". RCL Power table creates an inventory of all Id's in a row and adds number suffixes. Since these id's can be used in other attributes of the row as well, it will also replace 
     * any other ocurrence of thes id's with the suffixes version, provided taht this occurence is isolated by double quotes. Thereby a prefix of '#' is considered<br>
     * Example: for="d0-container" becomes for="d0-container-4", data-toggle="#d0-fsc" becomes data-toggle="#d0-fsc-4". But data-id-list="d0-customer d0-fsc" is not touched.
     * Also the special pattern #-RowId-# is replaced by the present row id.
     * 
     * The below function is a dummy to keep this documentation. Do not use it.
    * @author Ascan Heydorn
    */
    function aRCLPowerTable_ConceptionalOverview(){}
    /**RCL PowerTable - Paging Concept<br>
     * In order to have pagiing support, there must be a divider (or other area) of id="t0-paging" (in general <prefix>paging)
     * PowerTable will put into it another divider of flex-layout which itself contains 3 areas:<b>
     * The paging indicator on the left side, which shows "Page n of m"<br>
     * A Goto button with an input field in the middle, where users can enter a pager number and press the button to see the data<br>
     * A button area on the right side with 9 buttons.<br>
     * These buttons comprise<br>
     * 2 buttons which allow to move to the next or previos page<br>
     * 2 buttons showing the first and last page number<br>
     * 5 buttons showing page numbers in between.<br>
     * The format of these controls uses the following classes, whose styling is left to the developer:<br>
     * rptPgIndicator<br>
     * rptPgGotoLabel<br>
     * rptPgGotoField<br>
     * rptPgBtn<br>
     * 
     * @author Ascan Heydorn
     */
    function aRCLPowerTable_PagingFacility(){}
    
    var rutPowerTableIndex = []; 
    /**Keeps the settings of the sort which is just running. Required, since we cannot pass this as a parameter to the compare
     * functions and there could be morethan one table on the page.
     * After the sort has finished it is reset to null
     * @private
     */
    var _rutPowerTableInActiveSortSettings = null; //set to settings once a sort is started. 
    /** Initializes the settings of a table based on the HTML. The functions scans the children of containerId and looks for 
    tags of the classes tblRow, tblField and their id's. From that it creates a table of columns and related tags.
     * @author Ascan Heydorn
     * @param {string} containerId The ID attribute of the container in the html, which comprises the table
     * @return the settings
     * @private
    */
    function rptTableInit(containerId){
        return tableInit(containerId);
    }
    /**
     * @private
     */
    function tableInit(containerId){
        //Check whether containerId is unique
        var l= $('#'+containerId).length;
        if (l>1){
            alert('tableInit: containerId '+containerId+' exists '+l+' times');
            return;                
        }
        var parents=rptGetParentIds(containerId);
        if (parents.elementId==null){
            alert('tableInit: containerId '+containerId+' does not exists');
            return;
        }
        //Safety TO DO: we should check, that we have this id only once
        var s=getTableSettings(containerId);
        if (s!==null) {
            alert('tableInit: Settings for table with id "'+containerId+'" are already set');
            return null;
        }

        var settings = {
            containerId : containerId,
            //fields describing the template of the row header
            rowId : rowId, //this is the id of the row template, new rows get indexed ids rowId-0, rowId-1 ...
            idPrefix: "", //the prefix of the id of each field, after which the column name is show
            rowOuterHtml :"", //this is the html of one row                        
            columns: [],  //name, tag, type, potential values of tag "input", //select, p, td, textarea 
                        // potential values of type: radio, text, checkbox, number, ""=not applicable
                        //fields are name, tag, type, conversionTable = am 2dim string array of name=date/value=display
                        //type=10 input other, 11=input text 12 input numeric 13 input checkbox, 14 input/radio
                        //type=50 SELECT
                        //99 anything other than the above. These will be treated like text
                        //0 undefined, will not be used
                        //conversionTable: [],  // optional feature table of {dataValue, displayValue}. Can translate data to display and vice versa  
            idList:[], //a list of all ids in a row. these Ids we must replace with indexed Ids before inersting into html
            data: [], // an array of classes, each class provides data for one row. The field names of the class must match the columns names
            insertsAndChanges: [], //an array of row ids, which have been updated or inserted since las delete of this list
                      //in case of update the prefix i 'u', otherwise 'i'. Example 'ut2-row-33', it0-row-12'
            // ###09 this gives a safe new row-id index number. It start with map.lenght but when rows are deleted it stays
            //without that inserting, deleting and inserting again might create duplicates
            nextRowIndex: 0, //
            requireChangeEvent: false, //if true it indicates, that we must and have registered a chane event on the area
            configuration: {
                prefixLength: 3,   //can be adjusted atRuntime by subtables
                classTableHeader: "tblHeader", //the class indicating a table header
                classTableArea: "tblArea",
                classTableRow: "tblRow",
                classTableField: "tblField",
                rowIdReplacementTag: "#-RowId-#", //when this is present in the tblRow, it will be replaced with the id of the row 
                                            //Example: onclick='put("#-ID-#")' is replaced by onclick='put("t0-container-3")'
                sortDialogSuffix: "-dlg",
                sortDialogBtnAscSuffix: "-dlgAsc-",
                sortDialogBtnDescSuffix: "-dlgDes-",
                sortDialogSearchFieldSuffix: "-dlgSearch",
                sortDialogSearchFieldCountSuffix: "-dlgSearchCount",
            },  // configuration              
            sortAndSearch: { 
                map:[],   // an array of classes {htmlRowId, originalSortKey, wasFound=true if search has found it, row-id} //###17
                            //map[0,0] = data[0] 
                            //map[0,1] = original sortKey is the index of the data. Inserted data become a decimal between the predecessor and successor
                            //map[0,2] = true/false depending on whether search has found it (true) or not (false)
                            //map[0,3] = page number, default 0 ###02 Paging
                sortDefinition: [], //an array of classes {sortColumnName, ascDesc}     asc=1, desc=-1 
//                  search: "", //a text field with the actual search    
//                  foundHits: null, //when search was done thiscontainers the number of hits
                dialogOpen: false, //whether the sortAndSearchDialog is opened or not
            }, //end sortAndSearch
            // paging introduced with ###02 Paging
            paging: {
                hasPaging: false, //set to true is sarch area has a paging area with id prefix-paging (e.g. "t0-paging")
                activePage: 1, //The present page being displayed
                lastPageNumber: 1, //the las page number
                pageSize: 5, //the number of rows on a page
                buttonClass: "rptPgBtn", //the css class of each page button
                gotoLabelClass: "rptPgGotoLabel",
                gotoFieldClass: "rptPgGotoField",
                indicatorClass: "rptPgIndicator", 
            }, //end insert ###02 Paging
            displayConfiguration:{
                sortBtnColorClass: "rptSortBtnColor",
                sortBtnHighlightClass: "rptSortBtnHighlight",
                oddClassList: null,    //a space separated list of classes to be attached to any visible odd entry, to remove from the others.
                evenClassList: "bg-light",    // presently on one class is supported TO DO bg-light
            }, // end displayConfiguration
        };   
        var rowId=null;
        //We need to find the innermost tblRow
        $("#"+containerId).find("."+settings.configuration.classTableRow).each(function(){
            parents=rptGetParentIds(this);
            if(containerId!=parents.parentContainerId){
                return; //This was the wrong one, we are seeking another tblRow
            }
            rowId=this.id;
        });
        settings.rowId=rowId;
        if (!rowId) {
            alert('tableInit: Table container of id "'+containerId+'" does not have a row template of class "'+settings.configuration.classTableRow+'".');
                return null;
        }    
        settings.rowOuterHtml=document.getElementById(rowId).outerHTML;
        //Find column definitions
        var columnName="", tag="",type="";
        var prefixLength=settings.configuration.prefixLength;
        settings.idPrefix = containerId.substr(0,prefixLength);    
        $("#"+settings.rowId).find("."+settings.configuration.classTableField).each(function(){
            //Check whether tblField belongs has this rowId as innermost tblRow
            parents=rptGetParentIds(this);
            if (settings.rowId!=parents.parentRowId){
                return; //this tblField does not belong to our row, move on with the next
            }
            columnName=this.id.substring(prefixLength);
            tag=this.nodeName;
            type=(tag=== "INPUT") ? this.type : "";

            if (tag==="INPUT"){
                if (!this.readonly){ settings.requireChangeEvent=true;} //we need a change event
                if      (this.type==="checkbox") { type=13;}
                else if (this.type==="radio")    { type=14;}
                else if (this.type==="text")     { type=11;} 
                else if (this.type==="number")   { type=11;} //assign to value;
                else if (this.type==="usernamer"){ type=11;} //assign to value;
                else if (this.type==="password") { type=11;} //assign to value;
                //###20 BEGIN
                else if (this.type=="date")    { type=11;} //assign to value;
                else if (this.type=="time")    { type=11;} //assign to value;
                else if (this.type=="tel")     { type=11;} //assign to value;
                else if (this.type=="email")   { type=11;} //assign to value;
                else if (this.type=="url")     { type=11;} //assign to value;
                //###20 END
                else {type=0;}  //undefined, do not assign
            } 
            else if (tag==="SELECT") { 
                if (!this.readonly){ settings.requireChangeEvent=true;} //we need a change event
                type=50;} //compare with values in options
            else if (tag==="TEXTAREA") { 
                if (!this.readonly){ settings.requireChangeEvent=true;}//we need a change event
                type=99;} //assign to text
            else if (tag==="SPAN") { type=99;}
            else if (tag==="P") { type=99;}
            else if (tag==="TD") { type=99;}
            else { type=0;}
            
            settings.columns.push ({ 
                "name": columnName,
                "tag" : tag.toLowerCase(),
                "type": type
                }
            );
        });
        //Create a list of all ids of the row, so that we can replace them with indexed ids, once we create new entries
        var id="";
        settings.idList.push ( settings.rowId); // for easy programming we put the row id on top of the list
        $("#"+rowId).find("*").each(function(){
            if (this.id !=="") {
                settings.idList.push ( this.id );
            }
        });
        //Check whether we need a change event and register it
        if( settings.requireChangeEvent){
            document.getElementById(containerId).addEventListener("change",
                function(event){_rptHandleRowChangeEvent(settings, event);});
        }
        //Check whether we have a paging area and inserts the tags ###02 paging
        if (document.getElementById(settings.idPrefix+"paging")!=null){
            settings.paging.hasPaging=true;
        }
        //Add settings to tableIndex
        rutPowerTableIndex.push({containerId: containerId, settings: settings});
        //Make the template invisible
        $('#'+rowId).hide();

        //add rightClickEventListener
        //but only if it is not a lookup dialog
        if (containerId!="t99Lookup") { //###05
            document.getElementById(containerId).addEventListener(
                    'contextmenu', 
                    function(e) {
                        showSortControl(containerId);
                        e.preventDefault();
                    }, 
                    false); 
        }
        //build the paging area ###02, only now the table is in rutPowerTableIndex
        if (settings.paging.hasPaging==true){
            rptInsertPagingTags(containerId);
        }
        return settings;
    } //end function tableInit
    /**Finds the inner most tblRow and tblArea of an element. The element does not need to have an idea or belong to a table.
     * @param {string | object} source must be event object or a HTML Id
     * @return {object} An object with this properties: elementId, parentRowId, parentContainerId
     * @author Ascan Heydorn
     */
    function rptGetParentIds(source){ 
        var node=null;
        var elementId=null;
        if (typeof source =="string"){
            node=document.getElementById(source);
            elementId=source;
        }
        else if (source instanceof Event) { //Source is an event
            node=source.currentTarget; 
            elementId=node.id;
        }
        else if (source instanceof Element){
            node=source;
            elementId=source.id;
        }

        else {
            alert('Function rptGetParentIds was called with '+JSON.stringify(source)+" It should be Element, Event or a string")
        }
        var parentRow=null;
        var parentContainer=null;
        while(true){
            node=node.parentNode;
            tag=node.tagName;
            if (tag=='BODY') break;
            
            if (node.classList.contains("tblRow")&&(!parentRow)){
                parentRow=node.id;
                hasParentRow=true;
            }
            if (node.classList.contains("tblArea")&&(!parentContainer)){
                parentContainer=node.id;
                hasParentArea=true;
            }
        }
        return {elementId:elementId, parentRowId: parentRow, parentContainerId: parentContainer};
    }//end function rptGetParentIds
    /** Finds settings for a table in the rutPowerTableIndex. Requires that tableInit was run before.
     * @author Ascan Heydorn
     * @param containerId The html id of the container comprising the table
     * @return the settings or null if nothing found
    */
    function rptGetTableSettings(containerId){
        return getTableSettings(containerId);
    }
    /**
     * 
     * @private 
     */
    function getTableSettings(containerId){
        var settings=null;
        for (var i=0;i<rutPowerTableIndex.length;i++){
            if (rutPowerTableIndex[i].containerId === containerId) {settings=rutPowerTableIndex[i].settings;}
        }
        return settings;
    } //end function getTableSettings
    /** Adds the data to a table. Requires that tableInit was run before.
     * @author Ascan Heydorn
     * @param containerId The html id of the container comprising the table
     * @param data An array of classes. The match between data and html is through the id of tags of class "tblField". 
     * This id must be after the prefix identical to a attribute name of the class.
     * If the tag has id t0-containerNumber then the class should have an attribute containerNumber. 
    */
    function rptAddData(containerId, data){
        return addData(containerId, data);
    }
    /**
     * @private 
     */
    function addData(containerId, data){
        var settings=getTableSettings(containerId) || null;
        var pageNumber=(settings.paging.hasPaging)? 0:1; //###02 paging
        if (!settings) {
            alert( 'adData: Settings for table with id "'+containerId+'" are not created. Run initTableSettings first');
            return null;
        }
        settings.data = data;//###to do should be removed
        //###09
        settings.nextRowIndex=data.length;
        //###08 BEGIN
        settings.sortAndSearch.map=[];
        settings.sortAndSearch.sortDefinition=[];
        settings.sortAndSearch.search="";    
        settings.sortAndSearch.foundHits=null; 
        if (settings.sortAndSearch.dialogOpen) {
            rptCloseSortControl(containerId);
        }
        //###08 END
        //###02 Paging, added colmnn with index 3
        for (var i=0;i<data.length;i++){
            //increment page number
            if ( (settings.paging.hasPaging) && ( ( i% settings.paging.pageSize)==0) ){
                pageNumber++;
            }
            //settings.sortAndSearch.map.push([data[i],i,true,pageNumber]); //dataRecord, originalSort, withinSearch, page# ###17
            settings.sortAndSearch.map.push([data[i],i,true,pageNumber,settings.rowId+'-'+i]); //dataRecord, originalSort, withinSearch, page#, rowId ###17
            //#####we need to have the row-id in map as well, should become map[4]
        }
        if (settings.paging.hasPaging){
            settings.paging.activePage=1;
            settings.paging.lastPageNumber=pageNumber;
            rptResetPagingValues(containerId);
        }
        //###02 paging end
        return null;
    } //end function addData
    /** Builds html and display it after settings and data are ready. Requires tableInit and addData to be run before.
     * @param {string} containerId The html id of the container comprising the table
    */
    function rptDisplayTable(containerId){
        return displayTable(containerId);
    }
    /**
     * @private
     */
    function displayTable(containerId){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'displayTable: Settings for table with id "'+containerId+'" are not created. Run initTableSettings first');
            return null;
        }
        for (var i=0;i<settings.sortAndSearch.map.length;i++) {
            createNewTableRow(settings,i);
            moveDataToDisplay(settings,i);
            //rptHideAndMarkDataOnDisplay(containerId);
        }
        rptHideAndMarkDataOnDisplay(containerId);
        //testOutput(containerId);
    } //end function displayTable
    /**
     * @private
     */
    function testOutput(containerId){
        var settings=getTableSettings(containerId) || null;
        var column=settings.columns[0].name;
        for (var i=0;i<settings.sortAndSearch.map.length;i++){
            console.log('testOutput map:'+i+":"+eval('settings.sortAndSearch.map['+i+'][0].'+column)+
            "/"+settings.sortAndSearch.map[i][1]+
            ":"+settings.sortAndSearch.map[i][2]+
            ":"+settings.sortAndSearch.map[i][3]+
            ":"+settings.sortAndSearch.map[i][4]);
        }
        $(".tblRow").each(function(){
            console.log("testOutput rowId:"+this.id);
        });
        for (var i=0;i<settings.data.length;i++){
            console.log('testOutput data :'+i+":"+eval('settings.data['+i+'].'+column));
        }
    }
    //Replaces all id's in the inner html of a row with new index ids
    /**Creates html for a new row and the given index and returns it. 
     * It is used to initially build up the display of the table, after
     * the data has been added. 
     * 
     * @param {object} settings The settings object of the table
     * @param {integer} index The index of the data for which to create a row.
     * @returns {string} The html code of the new row to insert nito the DOM
     * @private
     * @author Ascan Heydorn
     */
    function rptCreateNewTableRow(settings,index){
        return createNewTableRow(settings, index);
    }
    /**
     * @private
     */
    function createNewTableRow(settings, index){ 
        var html=settings.rowOuterHtml;
        var toBeReplaced=null; //###11
        var replaceWith=null; //###11
        //add index to all ids
        //we need to use the index of the row Id
        var newIndex=settings.sortAndSearch.map[index][4].substring(settings.rowId.length); //###17
        for (var i=0;i<settings.idList.length;i++){
            //###11 BEGIN  Ids can show up more than one time in a row, e.g. as <label for="s0-container"> 
            toBeReplaced='"'+settings.idList[i]+'"';
            //replaceWith=('"'+settings.idList[i]+"-"+index+'"');//###17
            replaceWith=('"'+settings.idList[i]+newIndex+'"');//###17
            //html=html.replace( 
            //    '"'+settings.idList[i]+'"',('"'+settings.idList[i]+"-"+index+'"') );
            while (-1<html.search(toBeReplaced)){
                html=html.replace(toBeReplaced,replaceWith);
            }
            // They can also show up with prefix # like in data-toggle="#so-container"
            toBeReplaced='"#'+settings.idList[i]+'"';
            //replaceWith=('"#'+settings.idList[i]+"-"+index+'"');//###17
            replaceWith=('"#'+settings.idList[i]+newIndex+'"');//###17
            while (-1<html.search(toBeReplaced)){
                html=html.replace(toBeReplaced,replaceWith);
            }
            //###11 END
        }
        //replace the idReplacementTag with the true rowIdReplacementTag
        //var rowId=settings.rowId+'-'+index; //###17
        var rowId=settings.sortAndSearch.map[index][4]; //###17
        while (-1<html.search(settings.configuration.rowIdReplacementTag)){
            html=html.replace(settings.configuration.rowIdReplacementTag,rowId);
        }
        //we insert the row before the template row. By this any additional controls within the container, which shall not repeat are preserved.
        
        $("#"+settings.rowId).before(html);
        rutInitArea(rowId); //###14 initialize event handlers
        return html;
    }
    //####03 
    /**Inserts a row and an empty data record before the given place.<br>
     * If index is a number, the new row is inserted before the row of that index.
     * If it is 'end', the new row is inserted as last row
     * Otherwise index is considered to be a rowId
     * 
     * @param {String} containerId Html id of the table area
     * @param {String|Number|constant} index Either index number or rowId or 'end'
     * @author Ascan Heydorn
     */
    function rptInsertNewRowBefore(containerId, index){  //index=numeric index, rowId or end 
        /* This is the strategy
        1) The new index and rowId is always nextTowIndex, this value becomes bigger and bigger because id's of deleted rows are not reused.
        2) Sort numbers
        2.1 if the row is inserted at the beginning, the sort number is -map.length
        2.2 if it is at the end then always +map.length
        2.3 in between and we have a sort active: -map.length => with sort reset it jumps to the start
        2.4 in between but original sort (sortNumber[predecessor]+sortNumber[successor])/2
        */
        var idType=(isNaN(index))?"htmlId":"index";
        idType = (index=='end')?"end":"htmlId";
        var settings=rptGetTableSettings(containerId);
        //####09 BEGIN
        //var newIndexNumber=settings.sortAndSearch.map.length;
        var newIndexNumber=settings.nextRowIndex;
        settings.nextRowIndex++;
        var newRowId=settings.rowId+"-"+newIndexNumber;
        //####09 END
        var html=settings.rowOuterHtml;
        //###16 BEGIN
        var toBeReplaced=null; 
        var replaceWith=null; 
        //add index to all ids
        for (var i=0;i<settings.idList.length;i++){
            //Ids can show up more than one time in a row, e.g. as <label for="s0-container"> 
            toBeReplaced='"'+settings.idList[i]+'"';
            replaceWith=('"'+settings.idList[i]+"-"+newIndexNumber+'"');
            while (-1<html.search(toBeReplaced)){
                html=html.replace(toBeReplaced,replaceWith);
            }
            // They can also show up with prefix # like in data-toggle="#so-container"
            toBeReplaced='"#'+settings.idList[i]+'"';
            replaceWith=('"#'+settings.idList[i]+"-"+newIndexNumber+'"');
            while (-1<html.search(toBeReplaced)){
                html=html.replace(toBeReplaced,replaceWith);
            }
        }//end for settings.idList
        //add index to all ids
        //for (var i=0;i<settings.idList.length;i++){
        //    html=html.replace( 
        //        '"'+settings.idList[i]+'"',('"'+settings.idList[i]+"-"+newIndexNumber+'"') );
        //}
        //replace the idReplacementTag with the true rowIdReplacementTag
        //###16 END
        //var rowId=settings.rowId+'-'+newIndexNumber; //###17
        var newRowId=settings.rowId+'-'+newIndexNumber; //###17
        while (-1<html.search(settings.configuration.rowIdReplacementTag)){
            //html=html.replace(settings.configuration.rowIdReplacementTag,rowId);//###17
            html=html.replace(settings.configuration.rowIdReplacementTag,newRowId);//###17
        }
        //Now we need to find the row id where to insert the row.
        var data={};
        var rowId=""; 
        var count=-1;
        var sortNumber=0;
        if (index=='end') {
            rowId=settings.rowId;
            //we insert an empty data with sort index lenght of arry and considered as found and last page number
            //settings.sortAndSearch.map.push([data,settings.sortAndSearch.map.length,true,settings.paging.lastPageNumber] );//###17
            settings.sortAndSearch.map.push([data,settings.sortAndSearch.map.length,true,settings.paging.lastPageNumber,newRowId] );//###17 
            settings.data.push(data); //###17  The new data are inserted into data at the place of newRowId, so that they can be retrieved later even after a deletion
            $("#"+rowId).before(html);
        } //end 'atEnd'
        else { //Now we need to step through rows of the table and count
            $("#"+containerId).find(".tblRow").each(function(){
                rowId=this.id;
                if (rowId==settings.rowId) { //This is the template row, it does not count
                    return true;
                }
                count++; //we increment the counter and compare with index
                if  ( ((idType=='index')&&(count<index)) ||
                    ((idType=='htmlId')&&(rowId!=index))
                    ) {  //next row please
                    return true;
                } 
                else if ( ((idType=='index')&&(count==index)) ||
                          ((idType=='htmlId')&&(rowId==index))
                        ) { //This is now the hit
                    if ( (count==0) ||//when you insert at beginning or special sort, we put the record at the start
                         (settings.sortAndSearch.sortDefinition.length>0) ) {
                             sortNumber=-1*settings.sortAndSearch.map.length;
                    }
                    else { //now we are in between and still have the original sort. So we divide sort numbers by half
                        sortNumber= (settings.sortAndSearch.map[count  ][1]+
                                     settings.sortAndSearch.map[count-1][1] ) / 2;
                    }
                    //settings.sortAndSearch.map.splice(count, 0,[data,sortNumber,true,settings.sortAndSearch.map[count][3]] );//###17
                    settings.sortAndSearch.map.splice(count, 0,[data,sortNumber,true,settings.sortAndSearch.map[count][3],newRowId] );//###17
                    settings.data.push(data); //###17  The new data are inserted into data at the place of newRowId, so that they can be retrieved later even after a deletion
                    $("#"+rowId).before(html);
                    rutInitArea(newRowId); //###14 initialize event handlers
                    return false;
                }
                else {return false;} //too much advanced
            });
        }
        //add row to list of inserts and changes marked as an insert.
        settings.insertsAndChanges.push('i-'+newRowId);//###09 changed from rowId to newRowId
        return html;
    }//end function rptInsertNewRow
    /**Moves the data for a row to the screen
     * 
     * @param {object} settings The settings of the table 
     * @param {integer} index The index in the array of data of the record to move.
     * @author Ascan Heydorn 
     */
    function rptMoveDataToDisplay(settings, index){
        return moveDataToDisplay(settings, index);
    }
    /**
     * @private 
     */
    function moveDataToDisplay(settings, index ){
        //###12 BEGIN
        /*
        var j=0;
        var fieldValue="";
        var value="";  
        var column=null;
        */
       //###12 END
        //make display visible or not depending on whether it is in search result or not
        /* ###02 paging
        if (settings.sortAndSearch.map[index][2]){ //record is a search hit
            $("#"+settings.rowId+"-"+index).show();
        }
        */
        if  ( settings.sortAndSearch.map[index][2]&&
             (settings.sortAndSearch.map[index][3]==settings.paging.activePage)){ //record is a search hit
            $("#"+settings.rowId+"-"+index).show();
        } // end ###02 Paging
        else {
            $("#"+settings.rowId+"-"+index).hide();
            //this.style.display='none';        
        }   
        //Fill the display fields with the data    
        //###12 BEGIN
        //rptDisplayDetailArea(containerId, data)
        //rptDisplayDetailArea(settings.rowId+"-"+index, settings.sortAndSearch.map[index][0]); //###17
        rptDisplayDetailArea(settings.sortAndSearch.map[index][4], settings.sortAndSearch.map[index][0]); //###17
        /*        
        $("#"+settings.rowId+"-"+index).find("."+settings.configuration.classTableField).each(function(){
            //for the time being we believe that the sequence is same as in columns array
            column=settings.columns[j];
            fieldValue=eval("settings.sortAndSearch.map["+index+"][0]."+column.name);
            ///####here we should replace with setElementValue
            switch (column.type){
                case 11: $(this).val(fieldValue); break;
                case 13:
                case 14: 
                    if (fieldValue==="Y") { $(this).attr("checked","");}
                    else {$(this).removeAttr("checked");} 
                    break;
                case 50: //<select> here we need to compare the values in the options with the data
                    $(this).find("option").each(function(){
                        if (fieldValue===$(this).val()){
                            $(this).attr("selected","");
                        }
                        else {
                            $(this).removeAttr("selected");
                        }
                    });
                    break;
                case 99: 
                    $(this).text(fieldValue);
                    break;
            }  //endswitch            
            j++;
        });
        */     
       //###12 END   
    }
    /**Adds a conversion table to a column in the settings
     * @author Ascan Heydorn   
     * @private     
    */
    function rptAddConversionTable(containerId,fieldId,table){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'addConversionTable: Settings for table with id "'+containerId+'" are not created. Run initTableSettings first');
            return null;
        }
        var found=false;
        for (var i=0;i<settings.columns.length;i++){
            if ( (settings.idPrefix+settings.columns[i].name)===fieldId) {
                settings.columns[i].conversionTable=table;
                found=true;
                break;
            }
        }
        if (!found) {
            alert('addConversionTable: fieldId "'+fieldId+'" not found in settings');
        }
        return null;
    } //end function addConversionTable
    /**Removes the display of a table (but not the data).
     * @private
     */
    function clearDisplay(containerId){
        return rptClearDisplay(containerId);
    }
    /**Removes the display of all records (but not the data).
     * @param {string} containerId The id of the table area wose diplay shall be removed
     * @author Ascan Heydorn
    */
    function rptClearDisplay(containerId){
        var settings=getTableSettings(containerId) || null;
        rptSyncTag2Data(containerId); //###20 before we remove the display we sync it
        $('#'+containerId).find('.tblRow').each(function(){
            if (this.getAttribute('id')!==settings.rowId){
                $(this).remove();
            }
        });
        return null;
    } //end function clearDisplay
    /**Returns the data class for the indicated display row as the user has edited it.
     * @param {string} containerId The html Id of the table area
     * @param {integer | object} id considered to be an index if numeric, otherwise cnsidered to be a rowId
     * @return the data object which was initially given to the tool with the changed data.
     * @author Ascan Heydorn
     */
    function rptGetDataFromDisplay(containerId, id){
    	
        rptSyncTag2Data(containerId); //###20
        idType="index";
        if (isNaN(id) ){ idType="htmlId";}
        //###20 BEGIN
        if (idType=="htmlId"){
        	
            return rptGetModelDataOfRowId(containerId,id);
        }
        else {
        	
            var settings=getTableSettings(containerId); //issues should have been occured during init or display
            return settings.sortAndSearch.map[id][0];
        }
        /*
        var settings=getTableSettings(containerId); //issues should have been occured during init or display
        index=-1;
        $("#"+containerId).find("."+settings.configuration.classTableRow).each(function(){
            //if (this.getAttribute('id')!=settings.rowId){
            if (this.id!=settings.rowId){ //don't study the template
                index++;
                if ( ((idType=="index")&&(index==id)) || ((idType=="htmlId")&&(this.id==id)) ){
                    //Now we move the data from display to the data store
                    var fieldValue=""; 
                    var j=0;
                    var column=null;
                    var statement=null;
                    $("#"+this.id).find("."+settings.configuration.classTableField).each(function(){
                        //for the time being we believe that the sequence is same as in columns array
                        column=settings.columns[j];
                        switch (column.type){
                            case 11: fieldValue=this.value; break;
                            case 13:
                            case 14: 
                                fieldValue=(this.checked) ? 'Y' : '';
                                break;
                            case 50: 
                                fieldValue=this.options[this.selectedIndex].value;
                                break;
                            case 99: fieldValue=$(this).val(fieldValue); break;
                        }  //endswitch  
                        if (typeof fieldValue == "number"){
                            statement="settings.sortAndSearch.map["+index+"][0]."+column.name+"="+fieldValue;
                        }
                        else {
                            statement="settings.sortAndSearch.map["+index+"][0]."+column.name+"="+'"'+fieldValue+'"';                                
                        }
                        eval(statement);      
                        j++;
                    });
                    return false;
                } //endif this row fits the index resp. htmlId
            } //endif this row is not the template row
        });    //endloop overall rows of this container
        return settings.sortAndSearch.map[index][0];   
        */ 
       //###20 END      
    } // end function rptGetDataFromDisplay
    /**Removes a row from display and data. The row is identified by the index in the display (not considering any search) or the row id
     * @param {string} containerId The html Id of the table area
     * @param {integer | object} id considered to be an index if numeric, otherwise cnsidered to be a rowId
     * @author Ascan Heydorn
     */
    function rptRemoveRow(containerId, id){
        idType="index";
        if (isNaN(id) ){ idType="htmlId";}
        var settings=getTableSettings(containerId); //issues should have been occured during init or display
        var deletedRowId; //###10
        index=-1;
        $("#"+containerId).find("."+settings.configuration.classTableRow).each(function(){
            if (this.getAttribute('id')!==settings.rowId){
                index++;
                if ( ((idType=="index")&&(index==id)) || ((idType=="htmlId")&&(this.id==id)) ){
                    deletedRowId=this.id; //###10
                    rptSyncTag2Data(containerId);//###20 before deleting the row we sync all data
                    $(this).remove();
                    if(index>settings.sortAndSearch.map.length) {
                        alert( 'rptRemoveRow: sortAndSearch.map was not initialized'); 
                    }
                    else {
                        settings.sortAndSearch.map.splice(index,1); //remove row from sortAndsearchMap as well
                    }
                    return false;
                }
            }
        });     
        settings.insertsAndChanges.push('d-'+deletedRowId);  //###10
    }//end function rptRemoveRow
    /**Is applied in sorting data by the serach criteria. It will swap settings.sortAndSearch.map accordingly
     * It requires settings.sortAndSearch.sortDefinition being set for at least one column
     * It requires the global variable rutPowerTableInActiveSortSettings to keep the settings of the PowerTable, which at this time shall be sorted.
     * @author Ascan Heydorn
     * @private
    */
    function _rptSortCompare(data1, data2){
        var settings=_rutPowerTableInActiveSortSettings;
        var sortDef=null;

        var1=null; var2=null;
        result=0;
        for(var i=0;i<settings.sortAndSearch.sortDefinition.length;i++){
            sortDef=settings.sortAndSearch.sortDefinition[i];
            //get the column value
            var1=eval('data1[0].'+sortDef.sortColumn);
            var2=eval('data2[0].'+sortDef.sortColumn);

            if (var1<var2) { return (-1*sortDef.ascDesc);}
            else if (var1>var2) { return sortDef.ascDesc;}
        }
        return result;
    }//end functions sortCompare
    /**Is applied in reset the sorting to the original state. It will swap settings.sortAndSearch.map accordingly
     * It requires settings.sortAndSearch.sortDefinition being set for at least one column
     * It requires the global variable rutPowerTableInActiveSortSettings to keep the settings of the PowerTable, which at this time shall be sorted.
     * @author Ascan Heydorn
     * @private
    */
    function _rptResetSortCompare(data1, data2){
        return (data1[1]-data2[1]);
    }//end functions _rptResetSortCompare
    /**Adds a single sort Definition to the settings
     * @param {string} containerId The html Id of the PowerTable to be used.
     * @param {string} sortColumn  The columnName of the column which shall be sorted
     * @param {string} ascDesc     Whether this column shall be sorted ascending (1) or descending (-1)
     * @author Ascan Heydorn
     */
    function rptAddSingleSortDefinition(containerId, columnName, ascDesc){
        return addSingleSortDefinition(containerId, columnName, ascDesc);
    }
    /**
     * @private
     */
    function addSingleSortDefinition(containerId, columnName, ascDesc){
        var settings=getTableSettings(containerId); //If this is a problem it should have occurred at time of addData already
        settings.sortAndSearch.sortDefinition.push({sortColumn: columnName, ascDesc: ascDesc});    
    } //end function addSingleSortDefinition

    /**Sorts the PowerTable according to the sort definitions which have been one by one added with addSingleSortDefinition
     * The sequence of these call determines the sequence of sorts.
     * @param {string} containerId The html Id of the container comprising the table to be sorted.
     * @param {boolean} isReset
     * @author Ascan Heydorn
     * */
    function rptSortPowerTable(containerId,isReset){ 
        _rutPowerTableInActiveSortSettings=getTableSettings(containerId); //If this is a problem it should have occurred at time of addData already
        settings=_rutPowerTableInActiveSortSettings;
        if(!isReset){
            settings.sortAndSearch.map.sort(_rptSortCompare);
        }
        else {
            settings.sortAndSearch.map.sort(_rptResetSortCompare);
        }
        rptClearDisplay(containerId); //###20 just to clean
        displayTable(containerId);            
        _rutPowerTableInActiveSortSettings=null;
    }//end function sortPowerTable
    /**Finds the index of a column by name in the columns array
     * @param settings The settings to use
     * @param columnName The name of the column to find
     * @return The index of the column or null
     * @author Ascan Heydorn
     * @private
     */
    function _rptGetIndexOfColumn(settings, columnName){
        var index=null;
        for (var i=0; i<settings.columns.length;i++){
            if (columnName===settings.columns[i].name) {
                index=i;
                break;
            }
        }
        return index;
    } //end function _rptGetIndexOfColumn
    /**Function used internally by the sort and search control to set the search definitions and puts colors 
     * and markings to the dialog.
     * @param {string} containerId The html Id of the container comprising the table
     * @param {string} buttonId The html of the button which was clicked to set this sort 
     * @param {string} columnName The name of the column which was asked to be sorted
     * @param {int} ascDesc Whene the sort shall be ascending(1) or descending (-1)
     * @author Ascan Heydorn
     */
    function rptInjectSortDef(containerId, buttonId, columnName, ascDesc){
        var settings=getTableSettings(containerId);//if this is not present, then we would have found out much earlier
        addSingleSortDefinition(containerId, columnName,ascDesc);
        //and now paint the button
        var button=document.getElementById(buttonId);
        button.classList.remove(settings.displayConfiguration.sortBtnColorClass);
        button.classList.add(settings.displayConfiguration.sortBtnHighlightClass);
        button.textContent=settings.sortAndSearch.sortDefinition.length;
        rptSortPowerTable(containerId);
    }//end function rutInjectSortDef
    /**Resets the sortDefinitions and also the display of the dialog
     * @param {string} containerId
     * @author Ascan Heydorn
     */
    function rptResetSortDefinitions(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier
        var element=document.getElementById(containerId+"-dlg")||null;
        var id=null;
        if (element!==null){
            for (var i=0;i<settings.sortAndSearch.sortDefinition.length;i++){
                id=containerId+'-dlgAsc-'+ _rptGetIndexOfColumn(settings, settings.sortAndSearch.sortDefinition[i].sortColumn);
                element=document.getElementById(id);
                if (element!==null){
                    element.classList.remove(settings.displayConfiguration.sortBtnHighlightClass);
                    element.classList.add(settings.displayConfiguration.sortBtnColorClass);
                    element.innerHTML="&and;"; //##06
                }
                id=containerId+'-dlgDes-'+ _rptGetIndexOfColumn(settings, settings.sortAndSearch.sortDefinition[i].sortColumn);
                element=document.getElementById(id);
                if (element!==null){
                    element.classList.remove(settings.displayConfiguration.sortBtnHighlightClass);
                    element.classList.add(settings.displayConfiguration.sortBtnColorClass);
                    element.innerHTML="&or;"; //##02
                }
            }
        }
        settings.sortAndSearch.sortDefinition=[];
        rptSortPowerTable(containerId,true); 
    } //end function rptResetSortDefinitions
    /**Sets the search string for a table. This string is used for searching in the function call of rptSearch
     * 
     * @param {string} containerId 
     * @param {string} searchString 
     */
    function rptSetSearchString(containerId, searchString){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier  
        settings.sortAndSearch.search=searchString;
        return null;
    }
    /**Searches the data for the given string
     * 
     * @author Ascan Heydorn
     * @private
     */
    function _rptCallSearch(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier  
        var searchString=document.getElementById(containerId+settings.configuration.sortDialogSearchFieldSuffix).value;
        settings.sortAndSearch.search=searchString;
        //###04 empty search string causes abort
        if ((!searchString) || (searchString=="")){
            return;
        }
        rptSearch(containerId);
        //###05 bug fix, the lookup table does not display the count, such a field is not required
        var element=document.getElementById(containerId+settings.configuration.sortDialogSearchFieldCountSuffix);
        if (element!=null){
            element.textContent='found '+
                settings.sortAndSearch.foundHits+
                ' of '+
                settings.sortAndSearch.map.length;
        }
    } //end function _rptCallSearch
    /**Searches the data for the search string, which was set by rptSetSearchString
     * 
     * @param {string} containerId 
     * @author Ascan Heydorn
     */
    function rptSearch(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier  
        searchString=settings.sortAndSearch.search;
        var value=null;
        settings.sortAndSearch.foundHits=0;
        settings.paging.activePage=1;
        var hasPaging=settings.paging.hasPaging;
        var page=(hasPaging)?0:1;
        var pageSize=settings.paging.pageSize;
        var hitNumber=0;

        for (var i=0;i<settings.sortAndSearch.map.length;i++){
            for (var j=0;j<settings.columns.length;j++){
                value=eval('settings.sortAndSearch.map['+i+'][0].'+settings.columns[j].name);
                if ((typeof value)==="number") { //convert numbers to strings
                    value=String(value);
                }
                if ((typeof value)==="string") {  //TO DO check against conversion table diplay values.
                    //if (value.search(searchString)>-1){ //###19
                    if (value.indexOf(searchString)>-1){ //###19
                        settings.sortAndSearch.map[i][2]=true;
                        settings.sortAndSearch.foundHits++;   
                        break; //once we found a hit no more search on further fields
                    }
                    else {
                        settings.sortAndSearch.map[i][2]=false;
                    }//end else
                }//end if typeof string
            } //endfor j columns
            //#### we count and set pages only if they are a hit.
            hitNumber=settings.sortAndSearch.foundHits-1;
            if ( (hasPaging) && (settings.sortAndSearch.map[i][2]==true )){
                if ((hitNumber%pageSize)==0) { 
                    page++;
                } 
                settings.sortAndSearch.map[i][3]=page;
            }
        }  //endfor i map
        //###02 Paging
        //reset the page number to 1
        if (hasPaging){
            settings.paging.activePage=1;
            settings.paging.lastPageNumber = page;
            rptResetPagingValues(containerId);
        }
        rptHideAndMarkDataOnDisplay(containerId);  
    }//end function rptSearch
    /**Opens a sort and search control. this control displays buttons for sort and searching
     * @param {string} containerId The html Id of the container comprising the table
     * @author Ascan Heydorn
     */
    function rptShowSortControl(containerId){
        return showSortControl(containerId);
    } 
    /**
     * 
     * @param {string} containerId
     * @private
     * @author Ascan Heydorn 
     */
    function showSortControl(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier

        if (settings.sortAndSearch.dialogOpen){
            alert("Sort and Search dialog already open");}
        else {settings.sortAndSearch.dialogOpen=true;}
        var dialogHtml= '<div id="'+containerId+settings.configuration.sortDialogSuffix+'" style="font-size:10px;">'
            +'<table>'
            +'<tr>';
        var buttonIdAsc=null;
        var buttonIdDes=null;
        var buttonColorClass=settings.displayConfiguration.sortBtnColorClass;
        var displayName=null;
        for (var i=0;i<settings.columns.length;i++){ 
            //We do not show columns, which are not displayed
            if (null==document.getElementById(settings.idPrefix+settings.columns[i].name)){
                continue;
            }
            //if the field has a name attribute, then we take that, otherwise we take the column name 
            displayName=document.getElementById(settings.idPrefix+settings.columns[i].name).getAttribute('name')
                    ||settings.columns[i].name;
            buttonIdAsc='"'+containerId+settings.configuration.sortDialogBtnAscSuffix+i+'"';
            buttonIdDes='"'+containerId+settings.configuration.sortDialogBtnDescSuffix+i+'"';
            dialogHtml+='<td><span class="col-sm-1">'+displayName+'</span></td>'
                +'<td><button id='+buttonIdAsc+' class="btn btn-sm '+buttonColorClass+'" style="font-size:9px;width:30px;text-align:center;margin:0;"' //###06
                +' data-toggle="tooltip" data-placement="top" title="Sorts by ascending values of '+displayName+'. Existing sorts are expanded."'
                +' onclick=\'rptInjectSortDef("'+containerId+'", '+buttonIdAsc+', "'+settings.columns[i].name+'", 1); \'>&and;</button></td>'

                +'<td><button id='+buttonIdDes+' class="btn btn-sm '+buttonColorClass+'" style="font-size:9px;width:30px;text-align:center;margin:0;"' //###06
                +' data-toggle="tooltip" data-placement="top" title="Sorts descending values of '+displayName+'. Existing sorts are expanded."'
                +' onclick=\'rptInjectSortDef("'+containerId+'", '+buttonIdDes+', "'+settings.columns[i].name+'", -1); \'>&or;</button></td>'
                +'</tr>';              
        }
        dialogHtml+='</table>';
        //Display search area and count
        //Display number of hits only it there was a search text
        var hitsText =((settings.sortAndSearch.search==null)||(settings.sortAndSearch.search=='')) ? '' :  //###15 added ==''
                        ('found '+settings.sortAndSearch.foundHits+' of '+settings.sortAndSearch.map.length);
        dialogHtml+='<table class="mt-1">'+  //###15 added mt-1
                        '<tr>'+
                            '<td><span class="col-sm1 bt-1">Search</span></td>'+
                            '<td colspan=2> <input class="col-sm2" type="text" id="'+containerId+settings.configuration.sortDialogSearchFieldSuffix+'" '+                                
                            'value="'+(settings.sortAndSearch.search||'')+'"></input></td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td colspan=3><span id="'+containerId+settings.configuration.sortDialogSearchFieldCountSuffix+'">'+
                            hitsText+'</span></td>'+
                        '</tr>'+
                    '</table>';
        dialogHtml+='</div>';

        $("#"+containerId).append(dialogHtml);

        //if the container has a name it becomes part of the title <name> - Sort and Search
        displayName=document.getElementById(settings.containerId).getAttribute('name')||null;
        if (!displayName) { 
            displayName="Sort and Search";
        }
        else {
            displayName+=" - Sort and Search";
        }
        $("#"+containerId+settings.configuration.sortDialogSuffix).dialog({
            autoOpen: false,
            draggable: true,
            title: displayName, //####TO DO parameterisieren
            buttons: [    
                        {
                            text: "Search",     //####TO DO parameterisieren
                            click: function() {
                                _rptCallSearch(containerId); 
                            },
                            class: "rclLookupDlgBtn"
                        },
                        {
                            text: "Reset Search",     //####TO DO parameterisieren
                            click: function() {
                                rptResetSearch(containerId); 
                            },
                            class: "rclLookupDlgBtn"

                        },                            
                        {
                            text: "Reset Sort",
                            click: function() {
                                rptResetSortDefinitions(containerId);  
                            },
                            class: "rclLookupDlgBtn"
                        },
                        { //###13 BEGIN
                            text: "Export to csv",
                            click: function() {
                                rptSaveAsCsv(containerId);  
                            },
                            class: "rclLookupDlgBtn"
                        },//###13 END
                    ],
            close: function( event, ui ) {rptCloseSortControl(containerId);},
        });
        //We make Search the default once you press enter
        document.getElementById(containerId+settings.configuration.sortDialogSearchFieldSuffix).addEventListener('keypress', function (e) {
            if (e.key === 'Enter') {
                _rptCallSearch(containerId);
            }
        });
        $("#"+containerId+settings.configuration.sortDialogSuffix).dialog("open");
        // If sort definitions exist (the dialog was closed without reset) then
        // 1) paint them again
        // 2) sort again, because someone may af inserted something in between
        if (0<settings.sortAndSearch.sortDefinition.length){
            var searchColumnIndex=null;
            var buttonId=null;
            var button=null;
            for (var i=0;i<settings.sortAndSearch.sortDefinition.length;i++){
                searchColumnIndex=_rptGetIndexOfColumn(settings, settings.sortAndSearch.sortDefinition[i].sortColumn);
                if (1===settings.sortAndSearch.sortDefinition[i].ascDesc){ //ascending
                    buttonId=containerId+settings.configuration.sortDialogBtnAscSuffix+searchColumnIndex;
                }
                else {
                    buttonId=containerId+settings.configuration.sortDialogBtnDescSuffix+searchColumnIndex;
                }
                button=document.getElementById(buttonId);
                button.classList.remove(settings.displayConfiguration.sortBtnColorClass);
                button.classList.add(settings.displayConfiguration.sortBtnHighlightClass);
                button.textContent=(i+1); //show the number of the sort definiton
            }
        }
        // sort again
        rptSortPowerTable(containerId);
    }
    /**Close the sort dialog
     * @param {string} containerId The html id of the tble area
     * @author Ascan Heydorn
     */
    function rptCloseSortControl(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier
        settings.sortAndSearch.dialogOpen=false;
        $("#"+containerId+settings.configuration.sortDialogSuffix).remove();
    }
    /**Reset Search. Will set the search value to null, set all data to visible and displays all data again
     * @param containerId The html id of the tble area
     * @author Ascan Heydorn
     */
    function rptResetSearch(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier
        settings.sortAndSearch.search=null;
        settings.sortAndSearch.foundHits=null;
        //make everything visible
        //###02 Paging
        settings.paging.activePage=1;
        var hasPaging=settings.paging.hasPaging;
        var page=(hasPaging)?0:1;
        var pageSize=settings.paging.pageSize;
        for (var i=0; i<settings.sortAndSearch.map.length;i++){
            settings.sortAndSearch.map[i][2]=true;
            if ( (hasPaging) && ((i%pageSize)==0) ){
                page++;
            } 
            settings.sortAndSearch.map[i][3]=page;
        }
        if (hasPaging){
            settings.paging.activePage=1;
            settings.paging.lastPageNumber=page;
            rptResetPagingValues(containerId)    
        }
        rptHideAndMarkDataOnDisplay(containerId); 
        //now display empty search screen on the dialog, if the dialog is open
        var element=null;
        element=document.getElementById(containerId+settings.configuration.sortDialogSearchFieldSuffix);
        if (element){
            element.value='';
        }
        element=document.getElementById(containerId+settings.configuration.sortDialogSearchFieldCountSuffix);
        if (element){
            element.textContent='';
        }  
    } //end function rptResetSearch
    /**Adjust the display of the table in the following ways:
     * Hides/displays rows which are or 'are not' part of the search result or active page
     * Adjust the css classList depending on whether the row to display is even/odd numbered
     * @param {string} containerId The html id of the tble area
     * @author Ascan Heydorn
     */
    function rptHideAndMarkDataOnDisplay(containerId){
        var settings=getTableSettings(containerId); //if this is not present, then we would have found out much earlier
        var i=-1; //the counter of rows
        var displayedRow=0;
        var evenOddClassList
        $("#"+containerId).find('.'+settings.configuration.classTableRow).each(function(){
            if(this.id==settings.rowId){ //do not display the template
                return true;

            }
            i++;
            // ###02 paging BEGIN
            if ( settings.sortAndSearch.map[i][2] &&
                (settings.sortAndSearch.map[i][3]==settings.paging.activePage)
                ) { //record is a search hit
                displayedRow++;
                $(this).show();
            }
            else {
                this.style.display='none';        
            }
            //Now apply even/odd class
            if(displayedRow & 1){ //odd
                _rptSwapCssClasses(this,
                    settings.displayConfiguration.evenClassList||null,
                    settings.displayConfiguration.oddClassList||null);
            }
            else  { //even
                _rptSwapCssClasses(this,
                    settings.displayConfiguration.oddClassList||null,
                    settings.displayConfiguration.evenClassList||null);
            }
        });
    } //end function rptHideAndMarkDataOnDisplay
    /**Removes a list of classes from an HTML element and attaches a different list
     * @param element The HTML element where styles should be swapped
     * @param oldClassList List of classes to remove
     * @param newClassList List of classes to add
     * @author Ascan Heydorn
     * @private
     */
    function _rptSwapCssClasses(element,oldClassList, newClassList){
        var classList=null;
        if (oldClassList!=null){
            classList=oldClassList.split(" ");
            for (var i=0;i<classList.length;i++){
                element.classList.remove(classList[i]);
            }
        }
        if (newClassList!=null){
            classList=newClassList.split(" ");
            for (var i=0;i<classList.length;i++){
                element.classList.add(classList[i]);
            }
        }
    }// end function _rptSwapCssClasse
    /**Display data in the html tags of the area of the indicated html id.<br>
     * Does the same as rptDisplayDetailArea
     * 
     * @param {string} containerId HTML id of the search or detail area, where the data shall be displayed
     * @param {object} data
     * @author Ascan Heydorn 
     */
    function rptSetSingleAreaValues(containerId, data){
        rptDisplayDetailArea(containerId, data);
    }
    /**Displays the properties of the data in the tags within the container given by containerId.
     * The first 3 characters of containerId are considered the prefix.
     * The property of name <property1> is displayed int the tag of id <prefix><property1>
     * Example: containerID=d0-DetailArea, so prefix is d0-
     * then the property data.amount is shown in the tag of id d0-amount
     * Does the same as rptSetSingleAreaValue
     * 
     * @param {*} containerId The id oof the area containing the fields to be displayed.
     * @param {*} data An object having all properties (and potentially more), 
     * which shall be displayed. The tags must have class dtlField
     * @author Ascan Heydorn
     */
    function rptDisplayDetailArea(containerId, data){
        if (containerId==null) {
            alert("rptDisplayDetailArea: containerId is missing");
            return;    
        }
        if (data==null){
            alert("rptDisplayDetailArea: data is missing");
            return;             
        }
        var prefix=containerId.substr(0,2);
        if (prefix==null){
            alert("rptDisplayDetailArea: prefix is missing");
            return;
        }
        //####12 BEGIN reuse this function also for power table and search
        var fieldClass="dtlField";
        if (prefix.charAt(0)=='s'){
                fieldClass='SearchField';
        }
        else if (prefix.charAt(0)=='t'){
                fieldClass='tblField'; //should come from settings.configuration.classTableField but we may not have a table here
        }
        // ###12 END
        //Now save data for reuse
        if (prefix.charAt(0)!='t') { //###12 save only if not table
            if (typeof _rutPowerDetails== "undefined"){
                _rutPowerDetails = []; 
            }
            _rutPowerDetails.push({id:containerId, object: data});
        } //###12
        //check whether containerId is searchArea or detailArea and set the subsequent each accordingly
        //get list of porperties
//        var propertyList=Object.getOwnPropertyNames(data);
        var element=null;
        var tag=null;
        var value=null;
        var id=null;
        var property=null;
//        var index=-1;
        $("#"+containerId).find('.'+fieldClass).each(function(){ //####12 
        //$("#"+containerId).find('.dtlField').each(function(){ //####12 settings for details
            element=this;
            tag=this.nodeName;
            id=this.id;
            if (id==null) {return true;} //continue
            if (id.substr(0,2)!=prefix) {return true;}
            property=id.substr(3);
            //12 END BEGIN
            if (prefix.charAt(0)=='t') {
                property=property.substring(0,property.lastIndexOf('-'));
            }
            //###12 END
//            index=propertyList.indexOf(property);
//            if (index<0) {return true;} //continue, prperty not in object
            value=eval('data.'+property); //get the data value
            rutSetElementValue(id, value); //###12
            /*
            if (tag=='INPUT'){
                if (    (this.type=="text")         ||
                        (this.type=="number")      ||
                        (this.type=="usernamer")   ||   
                        (this.type=="password")    ||    
                        (this.type=="date")        ||    
                        (this.type=="time")            
                    )                          {
                    element.value=value;                    
                }
                else if (this.type=="radio"){
                    if (value=="Y") { element.setAttribute("checked","");}   
                    else {element.removeAttribute("checked");}                  
                }
                else if (this.type=="checkbox"){
                    if (value=="Y") { element.setAttribute("checked","");}   
                    else {element.removeAttribute("checked");}                  
                }
            }//end if input
            else if (tag=="TEXTAREA"){
                element.value=value;
            }
            else if (tag=="SELECT"){
                $(this).find("option").each(function(){
                    if (value==$(this).val()){
                        $(this).attr("selected","");
                    }
                    else {
                        $(this).removeAttr("selected");
                    }
                });
            }
            else { //all fields like td, span, p
                this.textContent=value;
            }
            return true;
            */
        });
    } //end function rptDisplayDetailArea
    /**Returns data from a search- or detail area to an object.<br>
     * The search or detail area must have received data before by rptDisplayDetailArea or rptSetSingleAreaValue
     * Any property in the data, which is not displayed is not touched but again returned.<br>
     * Does the same as rptGetDataFromDetailArea.
     * 
     * @param {string} containerId The html id of the area where we want the data from 
     * @returns {object} the data from the area
     * @author Ascan Heydorn
     * @private
     */
    function getDataFromSingleArea(containerId){
        return rptGetDataFromSingleArea(containerId);
    }
    /**Returns data from the tags within the container given by containerId.
     * The first 3 characters of containerId are considered the prefix.
     * The property of name [property1] is displayed int the tag of id [prefix][property1]
     * Example: containerID=d0-DetailArea, so prefix is d0-
     * then the property data.amount is shown in the tag of id d0-amount.<
     * 
     * @param {string} containerId The id oof the area containing the fields to be displayed.
     * @return {object} data An object having all properties which were displayed in a tag including
     * those properties, which have been in data at time if rptDisplayDetailArea.
     * Only tags with class dtlField are considered.
     * @author Ascan Heydorn
     */
    function rptGetDataFromSingleArea(containerId){
        if (containerId==null) {
            alert("rptGetDataFromDetailArea: containerId is missing");
            return;    
        }
        var prefix=containerId.substr(0,2);
        if (prefix==null){
            alert("rptGetDataFromDetailArea: prefix is missing");
            return;
        }
        var foundIndex=-1; //the index of data in _rutPowerDetails
        // ###07 empty object
        //var data=null;
        var data={};
        if ((typeof _rutPowerDetails)== "undefined"){
            _rutPowerDetails=[];
        }
        if (_rutPowerDetails!=null){
            if (typeof _rutPowerDetails!= "undefined"){
                for (var i=0;i<_rutPowerDetails.length;i++){
                    if (_rutPowerDetails[i].id==containerId){
                        data=_rutPowerDetails[i].object;
                        foundIndex=i;
                        break;
                    }
                }
            }
        }
        else {
            _rutPowerDetails=[];
        }
        // ###07 decommented
        /*
        if (foundIndex==-1){
            alert('rptGetDataFromSingleArea can run only after data was set by rptSetSingleAreaValues');
            return null;
        }
        */
        //Now save data for reuse
        _rutPowerDetails.push({id:containerId, object:data});
        //check whether containerId is searchArea or detailArea and set the subsequent each accordingly
        //get list of porperties
        var element=null;
        var tag=null;
        var prf=null;
        var value=null;
        var id=null;
        var property=null;
        var index=-1; 
        var radioButtonGroups=[]; //an array of all radio button groups in the area
                // it is build on the fly whenever we find a new group
                //each element has 4 properties property, id, name, value
                //The name is the name attribute from html and all elemenets with same property should have same name
                //Property is the key to the array.
                //When the first button is found, the entry is created with the value of the button
                //Only non '' values will overwrite the value in the array
        $("#"+containerId).find('.dtlField').each(function(){ //#### settings for details
            element=this;
            tag=this.nodeName;
            id=this.id;
            var foundBtnGrp=false; //indicator used in the find for radio button groups
            if (id==null) {return true;} //continue
            if (id.substr(0,2)!=prefix) {return true;}
            property=id.substr(3).split('#')[0];
            // 02 support for radio button groups
            value=rutGetElementValue(id); //element); //in case of radio button this is a single button
            // plan if radio button, put the found value to a list by group
            // then at the end we go through this list
            if (tag=='INPUT' && this.type == 'radio'){
                foundBtnGrp=false; 
                for (var i=0;i<radioButtonGroups.length;i++){
                    if (radioButtonGroups[i].property==property){
                        foundBtnGrp=true;
                        //This radio button group is already present in the group
                        if (radioButtonGroups[i].property[i].name!= this.name){
                            alert ("rptGetDataFromSingleArea: property:"+
                                property+' is used with name:'+
                                radioButtonGroups[i].property[i].name+' but id:'+
                                id+' has name:'+this.name);
                                return null;
                        }
                        if (radioButtonGroups[i].value!=null){
                            radioButtonGroups[i].value=value;
                        }
                    }
                }
                if (foundBtnGrp=false){
                    radioButtonGroups.push({'property': property, 
                                            'id:': this.id,
                                            'name': this.name,
                                            'value': value });    
                }
                return true; //In this case we just store the valuse and set them later
            }
            else{
                if (data!=null){
                    eval('data.'+property+'=\"'+value+"\"");
                }
                else {
                    alert()
                }
                return true;
            }

        });
        //No we have set all values except radio button groups
        for (var i=0;i<radioButtonGroups.length;i++){
            eval('data.'+radioButtonGroups[i].property+'=\"'+radioButtonGroups[i].value+"\"");
        }
        //finally we check, wehter we need to push the data to the storage
        if (foundIndex>-1){
            _rutPowerDetails[foundIndex].object=data;
        }
        else {
            _rutPowerDetails.push({id:containerId, object:data});
        }
        return data;
    } //end function rptGetDataFromDetailArea
    /**Displays the given page of the table. 
     * 
     * @param {String} containerId The id of the area of the table
     * @param {String|Number} pageNumber The page number to display if numeric or first, last, goto, previous, next
     * @author Ascan Heydorn
     */
    function rptGotoPage(containerId, pageNumber){
        var settings=rptGetTableSettings(containerId);
        var previousActivePage=settings.paging.activePage; //###27
        var newPageNumber=pageNumber;
        var prefix=settings.idPrefix;
        if (pageNumber=='first'){
            newPageNumber=1;
        }
        else if (pageNumber=='last'){
            newPageNumber=settings.paging.lastPageNumber;
        }
        else if (pageNumber=='previous'){
            newPageNumber=settings.paging.activePage-1;
        }
        else if (pageNumber=='next'){
            newPageNumber=settings.paging.activePage+1;
        }
        else if (pageNumber == 'goto'){
            newPageNumber=Number(rutGetElementValue(prefix+'pgGotoField'));
            if (newPageNumber==NaN) {
                newPageNumber=1;}
        }
        else {
            //newPageNumber=1+Number(pageNumber);//###22
            newPageNumber=Number(rutGetElementValue(prefix+'pg'+pageNumber));//###22
            if (newPageNumber<1) {return;} //###22   disabled button pressed
        }
        var lastPage=settings.paging.lastPageNumber;
        var btnValue;
        var btnReadOnly;
        if (newPageNumber<1) {
            newPageNumber=1;
        }
        if (newPageNumber>lastPage){
            newPageNumber=lastPage;
        }
        settings.paging.activePage=newPageNumber;
        rutSetElementValue(prefix+'pgFirst',1);
        rutSetElementValue(prefix+'pgLast',lastPage);
        //###22 BEGIN
        //Shift the numbers pn the pgBtn buttons
        //Usually the number ing starts with the highest number disible by 4 which is lower than the (activePage - 2)
        //Clicking 3-6 gives 2 3 4 5 6, 7-9 gives 6 7 8 9 10, 11-13 gives 10 11 12 13 14
        //however if we come down from a higher 
        var baseOffset=2; 
        if ('goto;first;last'.indexOf(pageNumber)>=0) {
            baseOffset=2;
        }
        else if (previousActivePage>settings.paging.activePage){
            baseOffset=3;
        }
        //Renumber the page buttons
        var basePage=(Math.floor((settings.paging.activePage-baseOffset) / 4)* 4)+1 ;
        if ((pageNumber=='first')||(settings.paging.activePage==1)){
            basePage=1;
        }
        else if ((settings.paging.activePage==2)&&(previousActivePage>2)){
            basePage=1;                
        }
        for (var i=1;i<=5;i++){
            btnValue=basePage+i;
            rutSetElementValue(prefix+'pg'+i,(btnValue<lastPage)?btnValue:' ');
            if (btnValue<lastPage){document.getElementById(prefix+'pg'+i).classList.remove("noPointer");}
            else {document.getElementById(prefix+'pg'+i).classList.add("noPointer");}
        }  
        //Now set the focus
        if (settings.paging.activePage==Number(rutGetElementValue(prefix+'pgFirst'))){
            document.getElementById(prefix+'pgFirst').focus(); 
        }
        else if (settings.paging.activePage==Number(rutGetElementValue(prefix+'pgLast'))){
            document.getElementById(prefix+'pgLast').focus(); 
        }
        else {
            for (var i=1;i<=5;i++){
                if (settings.paging.activePage==Number(rutGetElementValue(prefix+'pg'+i))){
                    document.getElementById(prefix+'pg'+i).focus();   
                    break;
                }
            }
        }
        //###22 END
        rptHideAndMarkDataOnDisplay(containerId);
        //now we need to change the button numbers
        rutSetElementValue(prefix+'pgIndicator','Page '+settings.paging.activePage+' of '+lastPage);
        return true;
    } //end function rptGotoPage
    /**Sets the values of the paging buttons and the paging indicator to their initial state
     * 
     * @param {String} containerId Id of table area
     * @author Ascan Heydorn 
     */
    function rptResetPagingValues(containerId){
        var settings=rptGetTableSettings(containerId);
        var prefix=settings.idPrefix;
        var lastPage=settings.paging.lastPageNumber;
        rutSetElementValue(prefix+'pgFirst',1);
        rutSetElementValue(prefix+'pgLast',lastPage);
        var btnValue=1;
        //var btnReadOnly; //###27 
        for (var i=0;i<5;i++){
            btnValue++;  
            rutSetElementValue(prefix+'pg'+(1+i),(btnValue<lastPage)?btnValue:' ');
            //btnReadOnly=(btnValue<lastPage)? true: false; //###27
            if (btnValue<lastPage){document.getElementById(prefix+'pg'+(1+i)).classList.remove("noPointer");} //###27
            else {document.getElementById(prefix+'pg'+(1+i)).classList.add("noPointer");} //###27
            //document.getElementById(prefix+'pg'+(1+i)).readonly=btnReadOnly;//###27
        }
        document.getElementById(prefix+'pgFirst').focus(); //###27
        rutSetElementValue(prefix+'pgIndicator','Page '+settings.paging.activePage+' of '+lastPage);
    } //end function rptResetPagingValues
    /** Inserts the paging tags into the paging area
     *  @param {String} containerId The html Id of the table area
     *  @author Ascan Heydorn
     */
    function rptInsertPagingTags(containerId){
        var settings=rptGetTableSettings(containerId);
        var prefix=settings.idPrefix;
        var html='<div style="display:flex;justify-content:space-between;align-items:center;">' +
            '<input id="'+prefix+'pgIndicator" class="'+settings.paging.indicatorClass+'" type="text" readonly="true">' +
            '<div style="display:flex;justify-content:flex-start;align-items:center;">' +
            '<a id="'+prefix+'pgGotoLabel" href="#" class="'+settings.paging.gotoLabelClass+'" onclick="rptGotoPage(\''+containerId+'\',\'goto\');">Goto</a>' +
            '<input id="'+prefix+'pgGotoField" class="'+settings.paging.gotoFieldClass+'" type="number" size="2"'
                +' data-check="dec(3,0)" min="0" onkeypress="rutCheckNumberKeypress(event);">' +
            '</div>' +
            '<div style="display:flex;justify-content:flex-end">' +
            '<a id="'+prefix+'pgPrevious" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',\'previous\');">&lt;</a>' +                
            '<a id="'+prefix+'pgFirst" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',\'first\');">1</a>'  + 
            '<a id="'+prefix+'pg1" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',1);">2</a>' +    
            '<a id="'+prefix+'pg2" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',2);">3</a>' +   
            '<a id="'+prefix+'pg3" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',3);">4</a>' +   
            '<a id="'+prefix+'pg4" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',4);">5</a>' +   
            '<a id="'+prefix+'pg5" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',5);">6</a>' +   
            '<a id="'+prefix+'pgLast" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',\'last\');">7</a>' +   
            '<a id="'+prefix+'pgNext" class="'+settings.paging.buttonClass+'" href="#" onclick="rptGotoPage(\''+containerId+'\',\'next\');">&gt;</a>' +   
            '</div>' +
            '</div>';
        $("#"+prefix+"paging").append(html);
    }//end function rptInsertPagingTags
    /**Returns a list of actions and row id's. If action is true the existing list is 
     * cleaned after returning it.
     * 
     * @param {String} containerId The html id of the table area.
     * @param {Boolean} action True or false depending on whether the list shall be cleared or not
     * @returns {Array} An array of objects, each having two properties action, which is 'u','d' or 'i'
     * depending on whether this was an update, delete or insert and rowId, which is the inserted/changed id of
     * the row.
     * @author Ascan Heydorn
     */
    function rptGetInsertsAndChanges(containerId, action){
        var settings=rptGetTableSettings(containerId);

        var changeRows=[];
        changeRows.length=settings.insertsAndChanges.length;
        for (var i=0;i<settings.insertsAndChanges.length;i++){
            changeRows[i]={'action': settings.insertsAndChanges[i].charAt(0),
                            'rowId': settings.insertsAndChanges[i].substring(2)
                        }
        }//end for
        if (action){
            rptSyncTag2Data(containerId); //###20 before we remove the log we sync the data
            settings.insertsAndChanges.length=0;
        }
        return changeRows;
    }//end function rptGetInsertsAndChanges
    /**Change event handler on the area to put updated rows into the list settings.insertsAndChanges
     * @param {Object} settings The settings object of the table area
     * @param {Event} event The change event
     * @author Ascan Heydorn
     * @private
     */
    function _rptHandleRowChangeEvent(settings, event){
        var element=event.target;
        var parents=rptGetParentIds(element.id);
        var found=false; //whether we found the row already
        //array.find is not supported in IE11 and earlier
        for (var i=0;i<settings.insertsAndChanges.length;i++){
            if (settings.insertsAndChanges[i]==('u-'+parents.parentRowId)){
                found=true;
                break;
            }
            else if (settings.insertsAndChanges[i]==('i-'+parents.parentRowId)){
                found=true;
                break;
            }
        }
        if (!found){
            settings.insertsAndChanges.push('u-'+parents.parentRowId);
        }
    }//end function rptHandleRowChangeEvent
    /** Saves a table as csv file. The searched data are stored as csv in the order of the sort. 
     *  The filename is RCL-PowerTableExport.csv
     *  @param {String} containerId The html id of the table
     *  @author Ascan Heydorn
     * */
    function rptSaveAsCsv(containerId, fileName){
        var settings=rptGetTableSettings(containerId);
        var rows=[];
        var columns=[];
        var colSep=',';
        var rowSep='\r\n';
        var column="";
        var displayName="";
        var file= fileName || "RCL-PowerTableExport.csv";

        if (0<=navigator.language.indexOf("de")){ colSep=';'} //German separator is ;
        for (var j=0; j<settings.columns.length;j++){
                column=settings.columns[j];
                displayName=document.getElementById(settings.idPrefix+settings.columns[j].name).getAttribute('name')
                            ||settings.columns[j].name;
                columns.push(displayName);
        }
        rows.push(columns.join(colSep));
        columns=[];
        for (var i=0; i<settings.sortAndSearch.map.length;i++){
            if (!settings.sortAndSearch.map[i][2]){ //this is not part of search
                continue;
            }
            for (var j=0; j<settings.columns.length;j++){
                column=settings.columns[j];
                columns.push(eval("settings.sortAndSearch.map["+i+"][0]."+column.name));
            }//endfor j
            rows.push(columns.join(colSep));
            columns=[];
        }//endfor i
        rutSaveAsFile(rows.join(rowSep),file,"text/plain;charset=utf-8");
    }//end function rptSaveAsCsv
    //###18 BEGIN
    /**Returns the data which have been displayed in a row of given rowId.
     * Deleted rows have still their model data. The datta are uniquely identified by rowId.
     * Note that if users change the data o the screen, it does not update the model data automatically.
     * Model data for a row are update only by the function rptGetDataFromDisplay
     * If the rowId does not exist, null is returned
     * 
     * @param {String} containerId 
     * @param {String} rowId 
     * @author Ascan Heydorn
     */
    function rptGetModelDataOfRowId(containerId,rowId){
        var settings=rptGetTableSettings(containerId);
        var dataIndex=rowId.substring(rowId.lastIndexOf('-')+1);
        if (dataIndex<0) {return;}
        if (dataIndex>=settings.data.length){return;}
        if (isNaN(dataIndex)){return;}
        return settings.data[dataIndex];
    }//end function rptGetModelDataOfRowId
    //###18 END
    //###20 BEGIN
    /**Synchronizes the inserts, updates and deletions from the tags to data. All rows which have been inserted, updated
     * or deleted since the last sync where synchronized.
     * 
     * @param {*} containerId 
     * @author Ascan Heydorn
     */
    function rptSyncTag2Data(containerId){
        var settings=rptGetTableSettings(containerId);
        //we step through the list of changes and update
        //The list of changes has every changed record only once. 
        //Therefore we need to step through it always from the very beginning
        //Sync point would be useless
        var changes=settings.insertsAndChanges;
        for (var i=0;i<changes.length;i++){
            rptSyncOneRow2Data(containerId, changes[i].substring(2));
        }
    } //end function rptSyncTag2Data
    /**Synchronizes one row of screen from tags to data
     * 
     * @param {*} containerId 
     * @param {*} rowId The htmlId of the row to synchronize
     * @author Ascan Heydorn
     */
    function rptSyncOneRow2Data(containerId, rowId){
        var settings=getTableSettings(containerId); //issues should have been occured during init or display
        var dataIndex=rowId.substring(rowId.lastIndexOf('-')+1);
        //Now we move the data from display to the data store
        var fieldValue=""; 
        var j=0;
        var column=null;
        var statement=null;
        $("#"+rowId).find("."+settings.configuration.classTableField).each(function(){
            //for the time being we believe that the sequence is same as in columns array
            column=settings.columns[j];
            switch (column.type){
                case 11: fieldValue=this.value; break;
                case 13:
                case 14: 
                    fieldValue=(this.checked) ? 'Y' : '';
                    break;
                case 50: 
                    fieldValue=this.options[this.selectedIndex].value;
                    break;
                case 99: fieldValue=$(this).val(fieldValue); break;
            }  //endswitch  
            if (typeof fieldValue == "number"){
                statement="settings.data["+dataIndex+"]."+column.name+"="+fieldValue;
            }
            else {
                statement="settings.data["+dataIndex+"]."+column.name+"="+'"'+fieldValue+'"';                                
            }
            eval(statement);      
            j++;
        });
        return settings.data[dataIndex];           
    } // end function rptSyncOneRow2Data
    //###20 END


    
