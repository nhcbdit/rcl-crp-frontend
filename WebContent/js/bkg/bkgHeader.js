//Index Button
//0: Back
//1: Confirm
//2: Save
//3: Help
//4: Close

var url;

$(document).ready(function() {
	setShowHideButton(["none", "none", "none", "none", "none"]);
	initOptions();
});

function initOptions() {

	url = window.location.href.toString().split('/')[window.location.href.toString().split('/').length - 1];

	switch (url) {
	case "bkgBookingMaintenanceScn.jsp" : 
		setShowHideButton(["", "", "", "", ""]);
		break;
	default : console.log(url);
	break;
	}
}

function setShowHideButton(listButtonShow){
	$("#h1-pageheader button").each(function(index) {
		this.style.display = listButtonShow[index];
	});
}

function save(){
	switch (url) {
	case "bkgBookingMaintenanceScn.jsp" : 
		saveBookingMaintenance();
		break;
	default : console.log(url);
	break;
	}
}

function help(){

}

function confirm(){
	let activeTab = $("#p0-tabs").find("a.active").text();
	switch(activeTab){
	case "General": activeTab = "GeneralTab"; break;
	case "Customers": activeTab = "Customers"; break;
	case "Equipments": activeTab = "Equipments"; break;
	case "Routing": activeTab = "Routing"; break;
	case "Charges": activeTab = "Charges"; break;
	case "Remarks": activeTab = "Remarks"; break;
	case "Print_Clause": activeTab = "PrintClause"; break;
	}

	var data = { 	
			GeneralTab : getGeneralTabOutput(),
			Customers: getCustomerTabOutput(),
			Equipments: getEquipmentTabOutput(),
			Charges: getChargeTabOutput(),
			PrintClause: getPrintClauseTabOutput(),
			Remarks: getRemarksTabOutput(),
			tab: activeTab
	};

	//console.log(getRemarksTabOutput());
//	console.log(data);
	//console.log(JSON.stringify(data));

	getResultAjax("WS_BKG_CONFIRM_BOOKING", data).done(handleSaveDataBooking);

	function handleSaveDataBooking(response){
		console.log(response);
	}
}

function saveBookingMaintenance(){
	
	var activeTab = $("#p0-tabs").find("a.active").text();
	switch(activeTab){
	case "General": activeTab = "GeneralTab"; break;
	case "Customers": activeTab = "Customers"; break;
	case "Equipments": activeTab = "Equipments"; break;
	case "Routing": activeTab = "Routing"; break;
	case "Charges": activeTab = "Charges"; break;
	case "Remarks": activeTab = "Remarks"; break;
	case "Print_Clause": activeTab = "PrintClause"; break;
	}

	var data = { 	
			GeneralTab : getGeneralTabOutput(),
			Customers: getCustomerTabOutput(),
			Equipments: "",//getEquipmentTabOutput(),
			Charges: getChargeTabOutput(),
			PrintClause: getPrintClauseTabOutput(),
			Remarks: getRemarksTabOutput(),
			tab: activeTab
	};

	//console.log(getRemarksTabOutput());
	console.log(data);
	//console.log(JSON.stringify(data));

	//getResultAjax("WS_BKG_SAVE_BOOKING", data).done(handleSaveDataBooking);

	function handleSaveDataBooking(response){
		console.log(response);
	}

}