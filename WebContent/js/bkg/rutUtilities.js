/*-----------------------------------------------------------------------------------------------------------  
rutUtilities.js
------------------------------------------------------------------------------------------------------------- 
Copyright RCL Public Co., Ltd. 2018 
-------------------------------------------------------------------------------------------------------------
Author Ascan Heydorn 13/08/2018
- Change Log ------------------------------------------------------------------------------------------------  
## DD/MM/YY -User-     -TaskRef-  -Short Description
01 29/08/2018 aschey1             Initial PoC
02 19/09/2018 aschey1  #02        Added support for radio button groups
03 02/10/2018 aschey1  #03        Added rutCheckArea
04 04/10/2018 aschey1  #04        Added fieldcheck and initializiation
05 09/10/2018 aschey1  #05        In lookup search field pressing the enter key starts search
06 09/10/2018 aschey1  #06        input type date not recognized in this.type, requires this.getAttribute('type')
07 10/10/2018 aschey1  #07        improved styling of buttons
08 12/10/2018 aschey1             fixed setting text-transform style during initArea
09 13/10/2018 aschey1             added rutOpen/CloseDialog
10 20/10/2018 aschey1             added rptSaveAsCsv to rutOpenLookup, improved rutLookup
11 20/10/2018 aschey1             create rutCheckCodeQuality   
12 24/10/2018 aschey1             fixed typo tele in rut getElementValue, fixed undefined display, 
13 24/10/2018 aschey1             initArea does not handle tel fields, which had been number fields in Chrome  
14 26/10/2018 aschey1             implemented number format setting also for setElementValue   
15 29/10/2018 aschey1             added keypress checker for tel and time, simplified event registration   
16 30/10/2018 aschey1             Change defaults dates on IE when value is ISO.
17 30/10/2018 aschey1             Format and/or convert numeric, date and time fields on output also for tags other than input
18 31/10/2018 aschey1             added interim ctrl key for send email and check code
19 02/11/2018 aschey1             added rutOpenMessageDialog, change element.remove() due to bug in IE
20 03/11/2018 aschey1             lookup and popover, check for typeof return value, fixed bug in number format
21 06/11/2018 aschey1             add browser & locale details to email, add rutGetDate
22 11/11/2018 aschey1             fix bug for null values rutFormatNumberForElement and datePicker
23 16/11/2018 aschey1             Finalized code checker
24 18/11/2018 aschey1             added code to openMessageBox
25 23/11/2018 aschey1             added code to access Office 365
26 07/12/2018 aschey1             created rutToolMenue()
27 12/12/2018 aschey1             improvements to code check, drop down, openTicket and fixes in initArea
28 14/12/2018 aschey1             lookup, added that params are not transferred, if input is empty
29 18/12/2018 aschey1             fixed Firefox issues in input type tel and number keypress event handler
*/
//### 04 field check and initializiation BEGIN

$(document).ready(function(){
    // The below is to circumvent the padding settings on the <span> in RCL.css
    // Ideally it is polaced there.
    var style=document.createElement("style");
    style.appendChild(document.createTextNode(""));
    document.head.appendChild(style);
    style.sheet.insertRule(".ui-datepicker span { padding:0pc 0px 0px 0px;}",0);
    rutInitPage();
});
/** Release notes for version from 06/11/2018
 * 1) new function rutOpenMessageDialog to display error messages etc. without need to create html
 * 2) New function rutGetDate to allow date claculations similar to rcl:date custom tag (today, first|last of previous|this|next month) and
 *    to allow output in numeric formt (20181106) to provide values, which can be compared with DB old date columns
 * 
 */
function a_ReleaseNotes181106(){} //This is a dummy
/**Relase notes for version from 31/10/2018
 * 1) JQuery date picker is only added if browser is neither Chrome nor Firefox
 * 2) rutGetElementValue will for date fields always return ISO fomrat yyyy-mm-dd
 * 3) rutSetElementValue will accept ISO date format and will ensure proper format of the DOM value
 * 4) rutSetElementValue will set value to proper numeric format as defined in dec(p,s)
 * 5) Same applies to any tag like p, td, span when data-type="number" or "date" and data-check="dev(p,s)"<br>
 *    Example &lt;p id="d0-amount" data-type="number" data-check="dec(7,2)" &gt;&lt;/p&gt;<br>
 *    After setElementValue("d0-amount","743") the result would be &lt;p ... &gt;743.00&lt;/p&gt;
 * 6) Added keypress event on type=time fields in case of IE. It enforces time 24h format ISO time input and automates :
 * 7) Added keypress event on type=tel fields for all browsers. Input need to (optionally) start wit a + followed by any sequence of digits
 *    The digits can be separated by one character of " -." like +49 675 876-655433 76
 * 8) Converts on IE any date default to dd/mm/yyyy format
 *
 * Work in progress
 * a) rutSendSupportEmail(). Would run after pressing Ctrl+1
 * b) rutCheckCodeQuality(). Would run after pressing Ctrl+2
 */
function a_ReleaseNotes181031(){} //This is a dummy
/**Initializes event listeners and date pickers for the whole page
 * Insert styles if required
 * @author Ascan Heydorn
 */
function rutInitPage(){
    //$('[data-toggle="popover"]').popover(); //###15
    $('[data-toggle="tooltip"]').tooltip();
    rutInitArea("body");
} //end function rut initPage

/**Registers for an area or the complete body keypress, change and datepicker events and also sets
 * the style for uppercase. The following is done:<br/>
 * 0) Creates (when applied to the body tag) a global variable _rutBrowser giving features of the Browser
 * 1) Register keypress events for all text fields (non readonly) which have maxLength set. Purpose: Check on maxLength<br/>
 * 2) Register style uppercase for all tags (non readonly) with upc<br/>
 * 3) Register change event for all text input (non readonly) to convert value to uppercase<br/>
 * 4) Register keypress events for all number fields (non readonly) to check format<br/>
 * 5) Change type of number fields to "tel" for Chrome and Firefox (see below)<br/>
 * 6) Register datepicker for all date fields if browser is not Chrome or firefox
 * 7) Register keypress events for all time fields if browser is not Chrome or firefox
 * 8) Register keypress events for all tel fields, which are not originally numbers (see 5)
 * 
 * In case of number fields and Chrome it changes the field type to "tel", since in Chrome "number" does not
 * return the position of the caret. Caret position is required for the format check.
 * 
 * @param {String} area Either "body" or the id of an area whose elements shall receive event listeners
 * @author Ascan Heydorn 
 */
function rutInitArea(area){
    var selector=area;
    //var isChrome = !!window.chrome && !!window.chrome.webstore; //###15
    //###27 BEGIN
    if ((typeof _rutBrowser)=="undefined"){
        var isChrome = (!!window.chrome && !!window.chrome.webstore)?true:false;
        var isFirefox = (typeof InstallTrigger !== 'undefined')?true:false;
        var isModern = (isChrome || isFirefox)? true:false;

        _rutBrowser = { hasOwnDatePicker : isModern,       //Yes=> suppress JQuery datepicker --> Chrome and Firefox
                       usesIsoDate : isModern,             //NO=> convert iso date to british for defaults --> Chrome and Firefox
                       typeConversionNumToTel : isModern,  //Yes=> convert type=number into type=tel to allow keypress support --> Chrome and Firefox
                       checkNumberKeypress : true,         //Yes=> enable keypress check for numbers --> IE and Chrome and Firefox
                       checkTimeKeypress : !isModern,      //Yes=> enable keypress event for type=time --> only IE
                       checkTelKeypress: true,             //Yes=> enable kexpress event for type=tel --> IE and Chrome and Firefox
                       isModern : isModern,                //information only, no practical usage
                       isChrome : isChrome,                //information only, no practical usage
                       isFirefox : isFirefox,              //information only, no practical usage
        }
    }
    //###27 END
    if (area!="body"){
        selector='#'+area;
    }
    else {
        selector="body";
        //###15BEGIN
        // Firefox 1.0+
        //###27BEGIN
        /*
        var isChrome = (!!window.chrome && !!window.chrome.webstore)?true:false;
        var isFirefox = (typeof InstallTrigger !== 'undefined')?true:false;
        var isModern = (isChrome || isFirefox)? true:false;

        _rutBrowser = { hasOwnDatePicker : isModern,       //Yes=> suppress JQuery datepicker --> Chrome and Firefox
                       usesIsoDate : isModern,             //NO=> convert iso date to british for defaults --> Chrome and Firefox
                       typeConversionNumToTel : isModern,  //Yes=> convert type=number into type=tel to allow keypress support --> Chrome and Firefox
                       checkNumberKeypress : true,         //Yes=> enable keypress check for numbers --> IE and Chrome and Firefox
                       checkTimeKeypress : !isModern,      //Yes=> enable keypress event for type=time --> only IE
                       checkTelKeypress: true,             //Yes=> enable kexpress event for type=tel --> IE and Chrome and Firefox
                       //isModern : isModern,                //information only, no practical usage
                       //isChrome : isChrome,                //information only, no practical usage
                       //isFirefox : isFirefox,              //information only, no practical usage
                    }
        */
        //###27 END
        //###15END
    } //endelse body
    var type="";
    var check=""; //###13
    var oldType=""; //###13
    document.getElementsByTagName("body")[0].addEventListener("keydown",rutCheckKeydown); //###18
    $(selector).find("input").each(function(){
        oldType=this.getAttribute('data-type'); //###13
        //##06 input type date not recognized in "this.type", requires "this.getAttribute('type')"
        type=this.getAttribute('type');
        if (this.getAttribute('readonly')==true){ //###15 changed from this.readonly to this.getAttribute...
            return false;
        }
        //###15BEGIN
        if (this.disabled){
            return false;
        }
        //###15END
        //if (type=="text"){ //###15
        if ((type=="text")&&(oldType!="time")){ //###15 we need to allow for type conversions time=> text
            if (this.maxLength>0){
                //this.addEventListener("keypress",function(event){rutCheckTextKeypress(event);}); //###15
                this.addEventListener("keypress",rutCheckTextKeypress); //###15
            }
            check=this.getAttribute('data-check');//###13 type decl moved up
            if ((check!=null)&&(check.indexOf("upc")>=0)){
                this.classList.add('text-uppercase'); //###08
                //this.addEventListener("change",function(event){rutChangeText(event);}); //###15
                this.addEventListener("change",rutChangeText); //###15
            }
        } //endif text
        //else if (type=="number"){ //###13
        else if ((type=="number")||(oldType=="number")){ //###13
            //if (isChrome){ //Chrome does not support selectionStart/End on type number //###15
//            if (_rutBrowser.typeConversionNumToTel){ //Chrome does not support selectionStart/End on type number //###15 ###27
            if (isModern){ //Chrome does not support selectionStart/End on type number //###15 //###27
                this.type="tel";
                this.setAttribute("data-type","number"); //keep the original data type
            }            
            //this.addEventListener("keypress",function(event){rutCheckNumberKeypress(event);}); //###15
            this.addEventListener("keypress",rutCheckNumberKeypress); //###15
            //Now we insert a "change" event handlr to format the decimals
            //var check=this.getAttribute('data-check'); //###13 type declaration moved up
            check=this.getAttribute('data-check'); //###13 
            if ((check!=null)&&(check.indexOf("dec(")>=0)){
                check=check.substring(p+4);
                var p=check.indexOf(")");
                var b=check.indexOf(",");
                if (p<=b) {return true;} // incorrect format definition
                scale=Number(check.substring(b+1,p)); //becomes NaN when not existing
                if (scale == NaN) {return true;} //incorrect format
                if (scale<=0) {return true;} //no decimals 
                this.addEventListener("change",function(event){rutChangeDecimal(event);});
            }
        } //end if number
        else if(type=="date"){
            //datepicker JQuery
            //the below is not required, since we do not register events with readonly tags
            //$.datepicker.setDefaults({ beforeShow: function (i) { if ($(i).attr('readonly')) { return false; } } });
            //if (!isChrome){ //###15
            //###16 BEGIN
            if (!_rutBrowser.usesIsoDate){ //This is IE and we need to convert all defaults to British formats
                if (this.defaultValue!=null){
                    this.value=rutConvertDate(this.value, false);
                    this.defaultValue=this.value;
                }
            }
            //###16 END
            //if (_rutBrowser.hasOwnDatePicker){ //###15
            if (!_rutBrowser.hasOwnDatePicker){ //###22
                $( "#"+this.id).datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    dateFormat: "dd/mm/yy"
                });
            }
        } //end if date
        //###15 BEGIN
        else if ((type=="tel")&&(oldType!="number")){
            if(_rutBrowser.checkTelKeypress){
                this.addEventListener("keypress",rutCheckTelKeypress); //###15
            }
        }
        else if ((type=="time")||(oldType=="time")){
            if (_rutBrowser.checkTimeKeypress){ //Chrome has own time picker
                this.addEventListener("keypress",rutCheckTimeKeypress); //###15
            }            
        }
        //###15 END
    }); //end $(selector).find("input").each"
} // rutInitArea
/**Change event handler to convert to uppercase
 * @param {ChangeEvent} event
 * @author Ascan Heydorn 
 */ 
function rutChangeText(event){
    var element=event.target;
    element.value=element.value.toUpperCase();

}
/**Change event handler to format the right number of decimals
 * @param {ChangeEvent} event
 * @author Ascan Heydorn 
 */ 
function rutChangeDecimal(event){
    //formats to the given number of decimals

    var element=event.target;
    //###14 BEGIN
    element.value=rutFormatNumberForElement(element, element.value)||'';
    //###14 END
} //end function rutCheckDecimalChange
/**Returns an object giving precision and scale of the dec(p,s) settings of an rcl number tag in html
 * @param {element} element The html element whose number settings should be derived
 * @returns {object} with property precision and scale. Returns null, if dec is not set or ill formatted
 * @author Ascan Heydorn
 */
function rutGetNumberSettings(element){ //###14 function new
    var check=element.getAttribute("data-check");
    var scale=0;
    var precision=0;
    if (check==null){ return null;} // no format details
    var p=check.indexOf("dec(");
    if (p<0){ return null;} //no decimal format
    check=check.substring(p+4);
    p=check.indexOf(")");
    var b=check.indexOf(",");
    if (p<=b) {return null;} // incorrect format definition
    scale=Number(check.substring(b+1,p)); //becomes NaN when not existing
    precision=Number(check.substring(0,b));
    if (scale == NaN) {return null;} //incorrect format
    if (scale<=0) {return null;} //no decimals 
    return {precision: precision, scale: scale}
}//end function rutGetNumberSettings
function rutFormatNumberForElement(element, initialValue){  //###14 function new
    //if (!initialValue){return "";} //###21 //###29
    if ((typeof initialValue == 'undefined')||(initialValue==null)){return "";} //###21 //###29
    var value=initialValue.toString();//###20
    var numberSettings=rutGetNumberSettings(element);
    var formattedValue="";
    if (!numberSettings) {
        //console.log("not number settings");
        return value;
    }
    var p=value.indexOf(".");
    if (p>=0){ //we have a dot
        //var decimals=value.substring(p+1,element.value.length)+"00000000000";//###20
        var decimals=value.substring(p+1,value.length)+"00000000000";//###20
        decimals=decimals.substring(0,numberSettings.scale);
        formattedValue=value.substring(0,p)+"."+decimals;
    }
    else { //no dot
        formattedValue=value+".000000000".substring(0,numberSettings.scale+1);
    }
    return formattedValue;
}//end function rutFormatNumberForElement
/**Checks after keypress whether the input to a text field dos not exceeds the maximum length
 * 
 * @param {KeyEvent} event
 * @author Ascan Heydorn 
 */
function rutCheckTextKeypress(event){
    //checks maxlength only
    var maxLength=event.target.maxLength;
    var value=event.target.value;
    var element=event.target;
    var startPos = element.selectionStart;
    var endPos = element.selectionEnd;
    if ((value.length+startPos-endPos)>= maxLength) {
        event.preventDefault();
        return false;
    }
}//end function rutCheckTextInput
/**Checks number input after a keypress event. Checks whether the input
 * leaves a number with the intended decimals, and scale.
 * 
 * @param {Event} event The keypress event object
 * @author Ascan Heydorn 
 */
function rutCheckNumberKeypress(event){
    var element=event.target;
    var check=element.getAttribute("data-check");
    var precision=20;
    var scale=0;
    var value=element.value;
    var min=element.min;
    var max=element.max;
    var valueLength=(!value)?0:value.length;
    var startPos = element.selectionStart;
    var endPos = element.selectionEnd;
    /*
    Chrome does not give selectionStart, selectionEnd for input type number,
    therefore we change to "tel" during initilalization with rutInitArea */
    if (check!=null){
        var p=check.indexOf("dec(");
        if (p>=0){
            check=check.substring(p+4);
            check=check.split(")")[0];
            check=check.split(",");
            precision=Number(check[0]); //becomes NaN when not existing
            scale=Number(check[1]); //becomes NaN when not existing
            if (precision == NaN){ precision=20;}
            if (scale == NaN) {scale=0;} 
        }
    }
    var positionDot=value.indexOf("."); //position of decimal dot, -1 = no dot
    var hasMinus = ((value.indexOf("-")>=0)||(value=="-"))? true: false; //whether has minus or not
    var decimals=  (positionDot<0) ? 0 : value.length-positionDot-1; //number of decimals
    var integers=  (positionDot<0) ? value.length : positionDot; //number of digits before the dot
    integers = (hasMinus) ? integers-1 : integers;
    //console.log("keypress num: value:"+element.value+" pos:"+startPos+"/"+endPos+" positionDot:"+positionDot+" integers:"+integers+
    //    " decimals:"+decimals+" key:"+event.key+" keyCode:"+event.keyCode+"/"+event.which+"/"+event.charCode);
    //numpad key !!
    //if ( (event.keyCode<48) && (event.key!='-') && (event.key!='.') ){ //###29
    if ( (event.charCode<48) && (event.key!='-') && (event.key!='.') ){ //###29
        console.log("control key");
        event.preventDefault();
        return false;
    }
    //else if (  (event.keyCode>57) ){ //###29
    else if (  (event.charCode>57) ){ //###29
        //        console.log("character other than - .");
        event.preventDefault();
        return false;
    }
    //- pressed but after position 1
    else if ( (event.key=="-") && (startPos>0) ) {
    //        console.log("- startPos>0");
        event.preventDefault();
        return false;
    }
//        //- pressed but min is >=0 and defined
    if ( (event.key=="-") && (min >=0) && (element.getAttribute('min')!=null)) {
        //console.log("- min>=0");
        event.preventDefault();
        return false;
    }    
    // .dot pressed but scale is 0
    if ( (event.key==".") &&(scale=0) ){
//            console.log(". pressed, but scale is 0");
        event.preventDefault(); 
        return false;
    }
    //integer exceeds length
    if ( (scale==0) && ( (precision+(hasMinus?1:0)) < (valueLength+1+startPos-endPos) ) ){
//            console.log("integer exceeds lenght:"+(precision+(hasMinus?1:0))+"<"+(valueLength+1+startPos-endPos) );
        event.preventDefault(); 
        return false;
    }
    // . pressed but cursor is before the dot
    if ( (event.key==".") && (endPos<positionDot) ){
//            console.log(". but cursor before dot")
        event.preventDefault(); 
        return false;
    }
    // . pressed but cursor is after the dot and there is already a dot
    if ( (event.key==".") && (startPos>positionDot) && (positionDot>=0) ){
//            console.log(". but cursor after dot")
        event.preventDefault(); 
        return false;
    }
    // too many decimals
    if ( (positionDot>0) && (startPos>positionDot) && (scale < ( startPos-positionDot+startPos-endPos) ) ){
//            console.log("too many decimals:"+scale+"/"+( startPos-positionDot+startPos-endPos));
        event.preventDefault(); 
        return false;
    }
    // integer part exceeds precision and there is a dot
    if ( (positionDot>=startPos) && ( (precision-scale+(hasMinus?1:0)) < (1+positionDot+startPos-endPos)) ){
//            console.log("too many integers and dot:"+(precision-scale+(hasMinus?1:0))+"/"+(1+positionDot+startPos-endPos));
        event.preventDefault(); 
        return false;
    }
    // integer part exceeds precision and there is no dot
    if ( (positionDot==-1) && ( (precision-scale+(hasMinus?1:0)) < (1+valueLength+startPos-endPos)) ){
        if ((!hasMinus)&&(event.key=="-")&&(startPos==0)){
            //inserting a minus at start position is still ok
        }
        else {
//                console.log("too many integers and no dot:"+(precision-scale+(hasMinus?1:0))+"/"+(1+valueLength+startPos-endPos));
            event.preventDefault(); 
            return false;
        }
    }
    // break precision by replacing - with a digit
    if ( hasMinus &&                     // we have '-'
        (startPos==0) && (endPos==1) && //'-' is selected
        ((precision-scale) == integers)
        ){
//            console.log("trying to break precision by replacing '-' with digit");
        event.preventDefault(); 
        return false;
    } 
} //end function rutCheckNumberKeypress
/**Checks time input after a keypress event. Checks whether the input is hh:mm.
 * 
 * @param {Event} event The keypress event object
 * @author Ascan Heydorn 
 */
function rutCheckTimeKeypress(event){
    var element=event.target;
    var value=element.value;
    var startPos = element.selectionStart;
    if (startPos==0){
        if (0>'012'.indexOf(event.key)){
            //console.log("first digit not 012");
            event.preventDefault();
            return false;
        }
    }//end startpos==0
    else if (startPos==1){
        if ((0<='01'.indexOf(value.charAt(0))) &&
            (0>'0123456789'.indexOf(event.key))
           ){
            //console.log("hours can be 00-19");
            event.preventDefault();
            return false;
        }//endif 00-19h
        else if ((value.charAt(0)=='2') &&
                 (0>'0123'.indexOf(event.key))
                ){
            //console.log("hours can be 20-23");
            event.preventDefault();
            return false;
        } //endif 20-23h
        //now :
        element.value=value.charAt(0)+event.key+':'+value.substring(3);
        event.preventDefault();
        return false;
    }//end startpos==1    
    else if (startPos==2){
        if (event.key!=':'){
            //console.log("3 key must be :");
            event.preventDefault();
            return false;
        }//endif :
    }//end startpos==2  
    else if (startPos==3){
        if (0>'012345'.indexOf(event.key)){
            //console.log("minute can be 00-59");
            event.preventDefault();
            return false;
        }//endif :
    }//end startpos==3  
    else if (startPos==4){
        if (0>'0123456789'.indexOf(event.key)){
            //console.log("2 digit of miute must be a number");
            event.preventDefault();
            return false;
        }//endif 
    }//end startpos==4  
    else {
        //console.log("too long");
        event.preventDefault();
        return false;        
    }
}//end function rutCheckTimeKeypress
/**Checks after keypress whether the input to a tel field broadly matches the pattern of phone numbers
 * Tel tags allowonly "0123456789 -." and a leading +, " -." must not be duplicated
 *  * 
 * @param {KeyEvent} event
 * @author Ascan Heydorn 
 */
function rutCheckTelKeypress(event){
    //###29 BEGIN This is only required for Firefox, Chrome and IE process the backspace directly on the sreen before.
    if (event.which==8){
        return true;
    }
    //###29 END
    var value=event.target.value;
    var element=event.target;
    var startPos = element.selectionStart;
    var key=event.key;
    //var glue='+ -.Spacebar'; //This is not supported in IE:  const glue = (v)=>(0<='+ -.'.indexOf(v)); //###29
    var glue='+ -'; //This is not supported in IE:  const glue = (v)=>(0<='+ -.'.indexOf(v)); //###29
    //the characters + -. are also allowed in between, + in front only
    //IE still returns Spacebar instead of ' '
    if ( (key=="+") && (startPos==0) ) {
        //+ only as first character is fine
        return true;
    }
    //else if (0>'0123456789 -.Spacebar'.indexOf(key)){ //###29
    else if (0>'0123456789 -'.indexOf(key)){ //###29
        //other character than digits and space,-,.
        //console.log(key+"/"+event.key+" only 0123456789 -. allowed");
        event.preventDefault();
        return false;
    }
    //Tried to enter + or glue after same  (0<=glue.indexOf(value.charAt(startPos-1)))
    else if ( (0<=glue.indexOf(key)) && (startPos>0) && (0<=glue.indexOf(value.charAt(startPos-1))) ){
        //console.log(event.key+' at pos '+startPos+' and '+value.charAt(startPos-1)+ " at position "+(startPos-1));
        event.preventDefault();
        return false;
    }
    //Tried to enter + or glue before same
    else if ( (0<=glue.indexOf(key)) && (startPos<(value.length-1)) && (0<=glue.indexOf(value.charAt(startPos+1))) ){
        //console.log(event.key+' at pos '+startPos+' and '+value.charAt(startPos+1)+ " at position "+(startPos+1));
        event.preventDefault();
        return false;
    }
}//end function rutCheckTelKeypress
//### 04 field check and initializiation END
/**Checks the input tags of an area for required, min, max
 * 
 * @param {String} containerId The html id of the area whose input elements to check 
 * @returns {array} An array of errors with columns htmmlId of field in error, errorNumber, errorDescription 
 * @author Ascan Heydorn
 */
function rutCheckArea(containerId){
    var errors = [];
    $("#"+containerId).find("input").each(function(){
        if ( (this.required) &&
             ((this.value==null) || (this.value==""))
            ){ 
            //errors.push( [this.id , 1 , 'missing value'] ); //###27
            errors.push( [this.id , 'rut001-001' , 'missing value'] ); //###27
        }
    });//end input tag
    return errors;
} //end function rutCheckInput
/**Resets all subtags of the tag with the given Id, which have an Id of same 3-character prefix with 
the default value.<br>
It supports all input tags where the value is stored in value or checked attribute and 
also select and textarea tags.<br>
Example: <br>&lt;div id=s0-area<br>
            &lt;input id="s0-account" ...<br>
            &lt;input id="name"<br>
            &lt;select id="s0-country"<br>
        &lt;/div&gt;<br>
In this example the input with id="s0-account" and the select would be reset to their defaults.
@param {string} containerId The html id of a tag (usually a div tag) whose conaining elements are reset.
@author Ascan Heydorn 
*/
function rutResetToDefault(containerId){
    $("#"+containerId).find("input[id^="+containerId.substr(0,2)+"]").each(function(){
        if      (this.type==="checkbox") { 
            this.checked=this.defaultChecked;
        }
        else if (this.type==="radio")    { 
            this.checked=this.defaultChecked;
        }
        else {
            this.value=this.defaultValue;
        }  
    });//end input tag
    $("#"+containerId).find("select[id^="+containerId.substr(0,2)+"]").each(function(){
        $(this).find("option").each(function(){
            this.selected=this.defaultSelected;
        });
    });// end select tag
    $("#"+containerId).find("textarea[id^="+containerId.substr(0,2)+"]").each(function(){
            this.value=this.defaultValue;
    });// end textarea tag
} //end function rutResetToDefault
function _rutPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators){
    var sc=(selectCols)?selectCols.split(' '):[];
    var sv=(selectValues)?selectValues.split(' '):[];
    var so=(selectOperators)?selectOperators.split(' '):[];
    var select=[];
    var element=null;
    var value=null;
    var operator=null;
    var dataType=null;
    var check=null;
    var index=null;
    var tag=null;
    for (var i=0;i<sv.length;i++){
        if (sc[i]==''){ continue;}
        if (sv[i]==''){ continue;}
        element=document.getElementById(sv[i]);
        value=(element==null)?sv[i]: rutGetElementValue(element);
        dataType=null; //###28
        if (element!=null){
            if ((value==null)||(value=='')){ continue;} //###28 if values come from a tag and the tag is empty, then the operator is not transferred
            tag=element.nodeName;
            if (tag=='INPUT'){
                dataType=element.type;
                //we may haved changed the type during init like for Chrome number to tel, then the original type is in data-type
                dataType=(element.getAttribute('data-type')==null)?element.type:element.getAttribute('data-type'); //###28
                if (dataType=='number'){
                    check=element.getAttribute('data-check');
                    if (check!=null){
                        index=check.search('dec(');
                        if (index>=0) {
                            check=check.substring(index);
                            dataType=check.split(')')[0];
                        }//endif data-check has dec
                    }//endif data-check exists
                }// endif type number   
                else {
                    value="'"+value+"'";                    
                }             
            }//endif input
            else {
                dataType='unknown';
                value="'"+value+"'";
            }
        }
        operator=(i>=so.length)?'=': so[i];
        if (dataType) { //###28
            select.push({column: sc[i], value:value, operator: operator, dataType : dataType})            
        //###28 BEGIN
        } 
        else { 
            select.push({column: sc[i], value:value, operator: operator})
        }
        //###28 END
    }
    /* This is the JSON structure for the webservice
    {   userToken : <userToken>,
        table: <tbl>, 
        {
            select: [{ column: <selectColumn>, value: <selectValue>, operator: <selectOperator>}]
        },
    }
    */
    var wsInput = { table: table, 
                    select: select
                  };
    return wsInput;
}
/**Opens a modal dialog box and displays a list of records for selection
 * After a record is selected by the user it fills certain fields, given bei their Ids., with values 
 * from the selected data
 * @param {string} table The name of the table to lookup. This shall be a view.
 * @param {string} returnCols A space separated list of columns which are returned from the selected record
 * @param {string} returnIds A space separated list of element Ids, which receive the value of the corresponfing column
 * @param {string} selectCols  A space separated list of columns which restrict the list
 * @param {string} selectValues A space separated list of values or element Ids, which must match the respective column. I values are empty, then the respective column is suppressedd
 * @param {string} selectOperators A space separated list of comparison operators. may include IN. If this list is not provided, than = is he default
 * @param {string} displayTitle The title of the dialog box. The default title is <table>.substring(3) + " Lookup" converted to titlecase
 * @author Ascan Heydorn
 * 
 */
function rutOpenLookupTable(table, returnCols, returnIds, selectCols, selectValues, selectOperators, displayTitle){
    //1 call web service to get the data with table, selectedCols, selectedVals
    //2 when data array is returned analyse first record in array to bild the html template
    //3 assign data to settings
    //4 Display dialog box
    //5 Close dialog box, assign return values
    var title=(displayTitle)?displayTitle: rutToTitleCase(table.substring(4)+' Lookup'); //remove the VRL_ from the table name
    //###20 BEGIN
    var wsInput = _rutPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators);
    _wsInput = wsInput //############## ONLY FOR DEMONSTRATION
    //console.log(JSON.stringify(wsInput));
    var lookupResult=rutGetLookupData(wsInput);
    var data=lookupResult.data;
    var metaData=lookupResult.metaData;
    //###20 END
    /*
    return: 
    {
        data: [{}],
        metaData : [{columnName: String, columnType: Integer, displaySize:Integer, precision: Integer, scale: Integer}]
    }
ARRAY 2003          BIGINT -5       BINARY -2       BIT -7          BLOB 2004       BOOLEAN 16  CHAR 1      CLOB 2005
DATALINK 70         DATE 91         DECIMAL 3       DISTINCT 2001   DOUBLE 8        FLOAT 6     INTEGER 4   JAVA_OBJECT 2000 
LONGVARBINARY -4    LONGVARCHAR -1  NULL 0          NUMERIC 2       OTHER 1111      REAL 7      REF 2006    SMALLINT 5 
STRUCT 2002         TIME 92         TIMESTAMP 93    TINYINT -6      VARBINARY -3    VARCHAR 12
*/
    //Step 2 
    //###19 BEGIN
    var element=document.getElementById('t99Lookup-dlg');
    if (element!=null){ //another lookup table was not clossed properly but through x button
        element.parentNode.removeChild(element); //element.remove does not work in IE
    }
    //###19 END
    var record=data[0];
    var listOfColumns=Object.getOwnPropertyNames(record);
    //var bodyElement = document.getElementsByTagName("body")[0];
    var htmlString='';
    var cr='\r\n';
    var dlgStyle='style="font-size: 10px;  border: #6A7896 1px solid;"';
    var tdStyleString='style="border: #6A7896 1px solid;padding: 3px;"';
    var tdStyleNumber='style="border: #6A7896 1px solid;padding: 3px; text-align:right;"';
    var tdHideClass="d-none"; //###20
    var tdClass='';

    htmlString+='<div id="t99Lookup-dlg" '+dlgStyle+'>';
    htmlString+='<div></div>'+cr; //This should be a bar
    var containerId="t99Lookup";
    var buttonIdAsc=null;
    var buttonIdDes=null;
    var buttonColorClass="rclDlgSortDirectionBtn"; //settings are not ready, so we cannot get it from there
    //Display search area
    htmlString+='<div style="padding:2px;"><table>'+
                '<tr>'+
                    '<td><label class="col-sm1 bt-1" for="'+containerId+'-dlgSearch'+'">Search</label></td>'+ //###07
                    '<td colspan=2> <input class="col-sm2" type="text" id="'+containerId+'-dlgSearch'+'" '+                                
                    '></input></td>'+
                '</tr>'+
            '</table>';
    htmlString+='</div>';


    htmlString+= '<table id="t99Lookup" class="tblArea">'+cr;
    htmlString+='<thead><tr>'+cr;
    htmlString+='<th '+tdStyleString+'>Select</th>'+cr;
    var colHeader=null;//###20
    for (var i=0;i<listOfColumns.length;i++){
        //###20 BEGIN
        if (listOfColumns[i].substring(0,5).toUpperCase()=='HIDE_'){
            tdClass='class="'+tdHideClass+'"';
        }
        else {
            tdClass='';
        }
        colHeader=rutToTitleCase(listOfColumns[i]);
        //htmlString+=('<th '+tdStyleString+'>'+listOfColumns[i]+'</th>'+cr);
        //htmlString+=('<th '+tdClass+tdStyleString+'>'+listOfColumns[i]+'</th>'+cr);//###20
        htmlString+=('<th '+tdClass+tdStyleString+'>'+colHeader+'</th>'+cr);//###20
        //###20 END
    }
    htmlString+='</tr>'+cr; 
    htmlString+='<tr>'+cr+'<th '+tdStyleString+'></th>'+cr; 
    for (var i=0;i<listOfColumns.length;i++){
        //###20 BEGIN
        colHeader=rutToTitleCase(listOfColumns[i]);
        if (listOfColumns[i].substring(0,5).toUpperCase()=='HIDE_'){
            tdClass='class="'+tdHideClass+'" ';
        }
        else {
            tdClass='';
        }
        //###20 END
        //#?#here we can suppress HIDE_ columns if (listPfColumns[i].substring(0,5).equalsIgnoreCase("HIDE_")){continue;}
        buttonIdAsc='"'+containerId+"-dlgAsc-"+i+'"';  //settings are not ready, so we cannot get it from there
        buttonIdDes='"'+containerId+"-dlgDes-"+i+'"';  //settings are not ready, so we cannot get it from there
        //htmlString+=('<th '+tdStyleString+'> '   //###20                          //text-dark ml-2 pl-2
        htmlString+=('<th '+tdClass+tdStyleString+'> ' //###20                      //text-dark ml-2 pl-2
            + '<button id='+buttonIdAsc+' class="'+buttonColorClass 
            + '" style="font-size:10px;width:30px;text-align:center;margin:0;"'
            //+' data-toggle="tooltip" data-placement="top" title="Sorts by ascending values of '+listOfColumns[i]+'. Existing sorts are expanded."' //###20
            +' data-toggle="tooltip" data-placement="top" title="Sorts by ascending values of '+colHeader+'. Existing sorts are expanded."' //###20
            +' onclick=\'rptInjectSortDef("'+containerId+'", '+buttonIdAsc+', "'+listOfColumns[i]+'", 1); \'>&and;</button></td>'
            +cr); //###07
        htmlString+=('<button id='+buttonIdDes+' class="'+buttonColorClass 
            + '" style="font-size:10px;width:30px;text-align:center;margin:0;"' //###07
            //+' data-toggle="tooltip" data-placement="top" title="Sorts descending values of '+listOfColumns[i]+'. Existing sorts are expanded."'  //###20
            +' data-toggle="tooltip" data-placement="top" title="Sorts descending values of '+colHeader+'. Existing sorts are expanded."' //###20
            +' onclick=\'rptInjectSortDef("'+containerId+'", '+buttonIdDes+', "'+listOfColumns[i]+'", -1); \'>&or;</button></td>'
            +'</th>'+cr);
    }// end for listOfColumns
    htmlString+='</tr>'+cr;
    htmlString+='</thead>'+cr;
    htmlString+='<tbody>'+cr;       
    htmlString+='<tr id="t99Row" class="tblRow">'+cr;
    htmlString+='<td '+tdStyleString+'><button id="t99SelectButton" class="'+buttonColorClass+'" onclick="rutSelectLookupEntry(\'t99Lookup-dlg\',\''+
                returnCols+'\',\''+returnIds+'\')">select</button></td>'+cr; //###07
    /*#?#
    var value=null;
    */
    for (var i=0;i<listOfColumns.length;i++){
        /*#?#
        here we can suppress HIDE_ columns 
        if (listOfColumns[i].substring(0,5).equalsIgnoreCase("HIDE_")){continue;}
        BIGINT -5 INTEGER 4 SMALLINT 5 TINYINT -6 
        BOOLEAN 16  CHAR 1 VARCHAR 12 DOUBLE 8   FLOAT 6 NUMERIC 2
        */
       //###20 BEGIN
        if (0<='-5,4,5,-6,2,6,8'.indexOf(metaData[i].columnType)){  //integer or decimal
            style='data-type="number" data-check="dec('+metaData[i].precision+','+metaData[i].scale+')" '+tdStyleNumber;
        }
        else if (metaData[i].precision==10){ //check for date
            for (var j=0; j<Math.min(10,data.length);j++){ //check for data
                value=eval ('data[j].'+listOfColumns[i]);
                if ((value.length==10)&&(value.charAt(4)=='-') && (value.charAt(7)=='-')){
                    style='data-type="date" '+tdStyleString;
                    break;
                }
            }
        }
        else {
            style=tdStyleString;
        }
        //style=(  (typeof eval ('data[0].'+listOfColumns[i]))=='number')? tdStyleNumber: tdStyleString;
        if (listOfColumns[i].substring(0,5).toUpperCase()=='HIDE_'){
            tdClass=' '+tdHideClass;
        }
        else {
            tdClass='';
        }
        //htmlString+=('<td id="t99'+listOfColumns[i]+'" class="tblField" '+style+'></td>'+cr); 
        htmlString+=('<td id="t99'+listOfColumns[i]+'" class="tblField'+tdClass+'" '+style+'></td>'+cr); 
        //###20 END
    }// end for listOfColumns
    htmlString+='</tr></tbody></table>'+cr;
    htmlString+='</div>'+cr; // end modal-body
    
    $("body").append( htmlString);
    //The below makes RCL PowerTable study the template in the html and build a model
    tableSettings=tableInit("t99Lookup"); //Id of the container, we may have more than one table on the same page
    //We make Search the default once you press enter ###05
    document.getElementById("t99Lookup"+tableSettings.configuration.sortDialogSearchFieldSuffix).addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            _rptCallSearch("t99Lookup");
        }
    }); 
    //attaches data to RCL PowerTable
    rptAddData("t99Lookup",data);
    //diplays the data by copying the template as many times as the data require and fill it from the data.
    rptDisplayTable("t99Lookup");
    $("#t99Lookup-dlg").dialog({
        autoOpen: false, 
        modal: true, 
        draggable: true,
        width: "auto", //500,
        height:"auto", // 400,
        title: title,
        buttons: [    
                    {
                        text: "Search",     //####TO DO parameterisieren
                        //icon: "ui-icon-heart", //####TO DO "ui-icon-heart" parameterisieren
                        click: function() {
                            _rptCallSearch(containerId); 
                        },
                        class: "rclLookupDlgBtn"
                    },
                    {
                        text: "Reset Search",     //####TO DO parameterisieren
                        //icon: "ui-icon-heart", //####TO DO "ui-icon-heart" parameterisieren
                        click: function() {
                            rptResetSearch(containerId); 
                        },
                        class: "rclLookupDlgBtn"
                    },                            
                    {
                        text: "Reset Sort",
                        //icon: "ui-icon-heart",  //####TO DO parameterisieren
                        click: function() {
                            rptResetSortDefinitions(containerId);  
                        },
                        class: "rclLookupDlgBtn"
                    },
                    { //###10 BEGIN
                        text: "Export to csv",
                        click: function() {
                            rptSaveAsCsv("t99Lookup");  
                        },
                        class: "rclLookupDlgBtn"
                    },//###10 END
                    {
                        text: "Cancel",  
                        //icon: "ui-icon-heart", //####TO DO "ui-icon-heart" parameterisieren
                        click: function() {
                            rutCloseDialog("t99Lookup-dlg"); 
                        },
                        class: "rclLookupDlgBtn"
                    },
                ],
        close: function( event, ui ) {rutCloseLookupPopoverDialog('t99Lookup-dlg','t99Lookup');},//###04 changed from t99Lookup-Dlg
    });
    $("#t99Lookup-dlg").dialog('open');
}//end function rutOpenLookupTable
/** Removes a closed lookup or popover dialog from the DOM and also removes the table settings of a lookup
 * from rutPowerTableIndex
 * 
 * @param {string} dialogId The html id of the complete dialog, which needs to be removed from the DOM 
 * @param {string} tableContainerId The containerId of the table, if the dialog is a lookup, otherwise null
 * @author Ascan Heydorn
 */
function rutCloseLookupPopoverDialog(dialogId, tableContainerId){ 
    rutCloseDialog(dialogId);//###04
    $("#"+dialogId).remove();//###19
    //###19 BEGIN
    var element=document.getElementById(dialogId);
    if (element!=null){
        console.log("Lookup "+dialogId+" is still existing after closing");
        element.parentNode.removeChild(element); //element.remove does not work in IE
    }
    //###19 END
    //remove the table from the table index
    if (tableContainerId!=null){
        for (var i=0;i<rutPowerTableIndex.length;i++){
            if (rutPowerTableIndex[i].containerId==tableContainerId){
                rutPowerTableIndex.splice(i,1);
                break;
            }
        }
    }//endfor
    //rutCloseDialog(dialogId);//###04
} //enfunction rutClseLookupPopoverDialog
/**
 * 
 * @param {*} dialogId
 * @private
 * @author Ascan Heydorn 
 */
function rutCloseDialog(dialogId){
    $("#"+dialogId).dialog('close');
}

/**Returns the value or text of an html element depending on its tag.
 * Supported tags are input, textarea, select and everyhting contaiing text. Null is returned in any other case
 * @param { string | DomElement} elementId A html element object or the html id of an element
 * @returns {string} The value or text of the element depending on its tag. 
 * @author Ascan Heydorn 
 */
function rutGetElementValue(elementId){
    var element=null;
    var value=null;
    // #02 begin
    var foundRadioButton=false; //this indicates, whether we found at least one radio button, if the id contains an #
    if ((typeof elementId)=="string"){ //the elementId is an id
        element=document.getElementById(elementId);
        if (element==null) {
            //It could still be a radio button with an id like d0-emptyFull#E
            //JQuery has issues with "." and "#" in id's, so we need to work around
            var str=elementId.split('#')[0].split('.')[0];
            $("input").each(function(){
                if (this.type!="radio") {return true;} //not a radio button, continue with next
                if (elementId!=this.id.split('#')[0]) {return true;} //does not belong to this attribute
                foundRadioButton=true; //we found a radio button, so this will not create an error
                if (this.checked) { 
                    value=this.value;
                    return false;
                }
            });
            if (foundRadioButton==true) {
                return value;
            }
            else {
                alert('rutGetElementValue: elementId "'+elementId+'" does not exist/does not represents a group of radio buttons');
                return null;
            }
        } //endif element==null
    }
    // #02 end
    else { //elementId is an element
        element=elementId;
    } //end if else elementId==string
    if (element==null){
        tag=null;
    }
    else {
        tag=element.nodeName||null;
    }
    if (tag==null){
        alert('rutSetElementValue: element '+JSON.stringify(element)+' of elementId:'+elementId+' is not an html element');
        return null;
    }
    if (tag=='INPUT'){
        if (    (element.type=="text")        ||
                (element.type=="number")      ||
                (element.type=="usernamer")   ||   
                (element.type=="password")    ||    
                (element.type=="date")        ||    
                //(element.type=="tele")        ||  //###12        
                (element.type=="tel")         ||  //###12   
                (element.type=="email")       ||  //###12  
                (element.type=="url")         ||    
                (element.type=="time")            
            )                          {
            value=element.value;                    
        }
        else if (element.type=="radio"){
            value=(element.checked) ? element.value : '';
        }
        else if (element.type=="checkbox"){
            value=(element.checked) ? 'Y' : '';
        }
    }
    else if (tag=="TEXTAREA"){
        value=element.value;
    }
    else if (tag=="SELECT"){
        value=element.options[element.selectedIndex].value;
    }
    else {
        value=element.textContent; //just strip to bare text
    }
    return value;
}//end function rutGetElementValue

/**Sets the value of an html element. Supports input, select, textarea and all elements, which can contain text.
 * For date and time fields the value must be in ISO format
 * for number field the output is fromatted according to the dec(p,s) specification
 * If the element is not an input tag, but something else like a p, span, td tag, this function can still be used.
 * The tag requires an id. If dates or times shall be displayed, the tag requires a data_type attribute.
 * If numbers shall be displayed the tag requires a data-type="number" attraibute and a data-check="dec(p,s)" attribute.
 * Example &lt;p id="d0-num" data-type="number" data-check="dec(7,2)" &gt; with a value of 543 would display as 543.00
 * 
 * @param {string | DomElement} elementId The element itself or the id of the element whose value needs to be set.
 * @param {string} value The value to be set.
 * @author Ascan Heydorn
 */
function rutSetElementValue(elementId, value){
    var element=null;
    var tag=null;
    var foundRadioButton=false;
    if ((typeof elementId)=="string"){ //the elementId is an id
        element=document.getElementById(elementId);
        if (element==null) {
            //It could still be a radio button with an id like d0-emptyFull#E
            //JQuery has issues with "." and "#" in id's, so we need to work around
            //var str=elementId.split('#')[0].split('.')[0];
            $("input").each(function(){
                if (this.type!="radio") {return true;} //not a radio button, continue with next
                if (elementId!=this.id.split('#')[0]) {return true;} //does not belong to this attribute
                foundRadioButton=true; //we found a radio button, so this will not create an error
                if (this.value==value) { 
                    this.checked=true; 
                }
                else {
                    this.checked=false; 
                }
            });
            if (foundRadioButton==false) {
                alert('rutSetElementValue: elementId "'+elementId+'" does not exist');
                return null;
            }
            return null;
        }
    }
    else { //elementId is an element
        element=elementId;
    }
    tag=element.nodeName||null;
    var dataType=element.getAttribute('data-type')||' '; //###14
    if (tag==null){
        alert('rutSetElementValue: elementId "'+elementId+'" does not have a tag');
        return null;        
    }
    if (tag=='INPUT'){
        if (    (element.type=="text")        ||
                //(element.type=="number")      || //###14
                (element.type=="usernamer")   ||   
                (element.type=="password")    ||    
                (element.type=="date")        ||    
                (element.type=="datetime-local")        ||  
                (element.type=="email")       ||  
                //(element.type=="tel")       ||  //###14
                ((element.type=="tel") && (dataType!='number'))        ||  //###14
                (element.type=="url")         ||  
                (element.type=="time")           
            )                          {
                //element.value=value;  //###12    
                element.value=value||'';  //###12              
        }
        //###14 BEGIN{
        else if ((element.type=='number') || (dataType=='number')){
            element.value=rutFormatNumberForElement(element, value)||'';
        }
        //###14 END
        else if (element.type=="radio"){
            if (value==element.value){ 
                element.checked=true; 
            }
            else {
                element.checked=false; 
            }
            
        }
        else if (element.type=="checkbox"){
            if (value=='Y'){ 
                element.checked=true; 
            }
            else {
                element.checked=false; 
            }
        }
    }
    else if (tag=="TEXTAREA"){
        //element.value=value; //###12        
        element.value=value||''; //###12
    }
    else if (tag=="SELECT"){
        $(element).find("option").each(function(){
            if (value===$(this).val()){
                $(this).attr("selected","");
            }
            else {
                $(this).removeAttr("selected");
            }
        });
    }
    else { //all other tags are treated like td, p, div, span etc.
        //element.textContent=value; //###12
        //###17 BEGIN
        //element.textContent=value||'';//###12
        if (dataType=='number'){
            element.textContent=rutFormatNumberForElement(element, value)||'';
        }
        else if (dataType=='date'){
            element.textContent=rutConvertDate(value,false)||''; //we need always convert to British format, since this is not a date field 
        }
        else if (dataType=='time'){
            element.textContent=rutConvertTime(value,true)||'';
        }
        else {
            element.textContent=value||'';
        }
        //###17 END
    }
} //end function rutSetElementValue

/**
 * 
 * @param {*} dialogId 
 * @param {*} returnCols 
 * @param {*} returnIds 
 * @author Ascan Heydorn
 * @private
 */
function rutSelectLookupEntry(dialogId, returnCols, returnIds ){
    var element=document.activeElement;
    var parents=rptGetParentIds(element);
    var settings=rptGetTableSettings(parents.parentContainerId);
    var prefix=settings.idPrefix;
    var r=parents.parentRowId.split('-');
    var rowSuffix='-'+r[r.length-1];
    // now we have prefix and suffix and are capable to build the elementIds.
    var columns=returnCols.split(' ');
    var returnIdList=returnIds.split(' ');
    var el=null;
    for (var i=0;i<columns.length;i++){
        if (columns[i]!=''){
            el=document.getElementById(prefix+columns[i]+rowSuffix);
            fid=prefix+columns[i]+rowSuffix;
            el=document.getElementById(fid);
            rutSetElementValue(returnIdList[i],rutGetElementValue(el) );
        }
    }
    rutCloseLookupPopoverDialog(dialogId, parents.parentContainerId);
}//end function rutSelectLookupEntry

/**rutToggle 
 * hides or Unhides an element and changes the control which has asked for this message to show 'expand', if the element
 * is hidden  or 'collapse, if it is visible.
 * 
 * @param {string} sourceElementId The element which need to change its display from expand to collapse
 * @param {string} targetElementId The element which shall be hidden or displayed
 * @author Ascan Heydorn
 */
function rutToggle(sourceElementId, targetElementId) {
    var isVisible = $('#' + targetElementId).is(":visible");
    $('#' + targetElementId).slideToggle('slow'); //rollup
    if (isVisible === true) {
        $('#'+sourceElementId).text("Expand");
    }
    else {
        $('#'+sourceElementId).text('Collapse');
    }
} 

/**Opens a nn modal dialog box and displays one records for review
 * Requires input of all required keys
 * @param table The name of the table for popover. This shall be a view.
 * @param selectCols  A list of columns which restrict the list
 * @param selectValues A list of values or element Ids, which must match the respective column
 * @author Ascan Heydorn
 * @private
 */
function rutOpenPopoverDialog(table, selectCols, selectValues, selectOperators, displayTitle){
    //1 call web service to get the data with table, selectedCols, selectedVals
    //2 when data array is returned analyse first record in array to bild the html template
    //3 assign data to settings
    //4 Display dialog box
    //5 Close dialog box, assign return values
    var title=(displayTitle)?displayTitle: rutToTitleCase(table.substring(4)+' Details'); //remove the VRL_ from the table name
    var wsInput = _rutPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators);
    
    var lookupResult=rutGetLookupData(wsInput);
    if (!lookupResult){
        alert("Field has no value. No data found");
        return null;
    }
    var data=lookupResult.data;
    if (data.length==0){
        alert("Field has no value. No data found");
        return null;
    }
    var metaData=lookupResult.metaData;
    /*
    {
        data: [{}],
        metaData : [{columnName: String, columnType: Integer, displaySize:Integer, precision: Integer, scale: Integer}]
    }
    */

    //Step 2 
    data=(data.length>0)?data[0]:data;
    var listOfColumns=Object.getOwnPropertyNames(data);
    var bodyElement = document.getElementsByTagName("body")[0];
    var htmlArray=[];
    var cr='\r\n';
    var dlgStyle='style="font-size: 10px;  border: #6A7896 1px solid;"';
    var tdStyleString='style="border: #6A7896 1px solid;padding: 6px;"';
    var tdStyleNumber='style="border: #6A7896 1px solid;padding: 6px; text-align:right;"';
    htmlArray.push('<div id="d99Popover-dlg" class="dtlHeader" '+dlgStyle+'>');
    htmlArray.push('<div></div>'); //This should be a bar
    var buttonColorClass="btn-light"; //settings are not ready, so we cannot get it from there
    var dataType=null;
    var style=null;

    htmlArray.push('<table id="d99Detail" class="detailArea">');
    htmlArray.push('<thead><tr>');
    htmlArray.push('<th>Field</th><th>Value</th>');
    htmlArray.push('</tr>');
    htmlArray.push('</thead>');
    htmlArray.push('<tbody>');
    for (var i=0;i<listOfColumns.length;i++){
        if (listOfColumns[i].substring(0,5).toUpperCase()=='HIDE_'){ //hidden columns are not displayed
            continue;
        }
        //This piece is now for preparing proper formatting of data depending on the sql data type
        if (0<='-5,4,5,-6,2,6,8'.indexOf(metaData[i].columnType)){  //integer or decimal
            dataType='data-type="number" data-check="dec('+metaData[i].precision+','+metaData[i].scale+')" ';
            style=tdStyleNumber;
        }
        else if (metaData[i].precision==10){ //check for date
            value=eval ('data.'+listOfColumns[i]);
            if ((value.length==10)&&(value.charAt(4)=='-') && (value.charAt(7)=='-')){
                dataType='data-type="date" ';
            }
            style=tdStyleString;
        }
        else {
            dataType='';
            style=tdStyleString;
        }
        htmlArray.push('<tr><td '+tdStyleString+' >'+rutToTitleCase(listOfColumns[i])+
                '</td><td id="d99'+listOfColumns[i]+'" class="dtlField" '+dataType+style+' >'
               +'</td></tr>');
    }// endfor listOfColumns
    htmlArray.push('</tr></tbody></table>');
    htmlArray.push('</div>'); // end modal-body


    bodyElement.insertAdjacentHTML('afterbegin', htmlArray.join(cr));
    //now display the data
    rptDisplayDetailArea('d99Detail', data);
    //JQUERY ONLY START
    var displayName=title+' Details';
    $("#d99Popover-dlg").dialog({
        autoOpen: false, 
        draggable: true,
        minWidth: 50,
        minHeight: 50,
        title: displayName, //####TO DO parameterisieren
        buttons: [    
                    {
                        text: "Cancel",  
                        icon: "ui-icon-heart", //####TO DO "ui-icon-heart" parameterisieren
                        click: function() {
                            rutCloseLookupPopoverDialog('d99Popover-dlg'); 
                        }
                    },
                ],
        close: function( event, ui ) {rutCloseLookupPopoverDialog('d99Popover-dlg');},
    });
    $("#d99Popover-dlg").dialog('open');

}//end function rutOpenPopoverDialog
/**Opens a dialog
 * @param {String} dialogId HTML id of the dialog
 * @author Ascan Heydorn
 */
function rutOpenDialog(dialogId){
    $("#"+dialogId).dialog("open");
}
/**Closes a dialog
 * @param {String} dialogId HTML id of the dialog
 * @author Ascan Heydorn
 */
function rutCloseDialog(dialogId){
    $("#"+dialogId).dialog("close");
}
//###10 Added this function
/** Converts a string to title case. Underscore is thereby replaced by spaces. Simple implementation.
 * For an elaborate implementation go for the JavaScript port by John Resig - http://ejohn.org/ - 21 May 2008
 * Original by John Gruber - http://daringfireball.net/ - 10 May 2008
 * License: http://www.opensource.org/licenses/mit-license.php
 * 
 * @param {String} title The string to convert
 * @author Ascan Heydorn
 */
function rutToTitleCase(title){
	//var small = "(a|an|and|as|at|but|by|en|for|if|in|of|on|or|the|to|v[.]?|via|vs[.]?)";
	//var punct = "([!\"#$%&'()*+,./:;<=>?@[\\\\\\]^_`{|}~-]*)";
    var workingTitle=title.toLowerCase();
    while (workingTitle.search('_')>=0){
        workingTitle=workingTitle.replace('_',' ');
    }
    var wt=workingTitle.split(' ');
    for (var i=0; i<wt.length; i++){
        wt[i]=wt[i].substr(0,1).toUpperCase() + wt[i].substr(1);
    }
    return wt.join(' ');
} //end function rutToTitleCase
/**Prepares an email for service desk to open a ticket and includes details about the page in the email
 * 
 * @param {Boolean} toServiceDesk Whether the email shall go to service desk
 * @author Ascan Heydorn 
 */
function rutSendSupportEmail(toServiceDesk){
    //###27 BEGIN
    var receiver="itservicedesk@rclgroup.com";
    var screenId=document.getElementsByTagName("body")[0].id; 
    var errorCode = null;
    if (typeof _rutLastError !== 'undefined'){
        errorCode = _rutLastError.code;
    }
    var subject="DCS Issue:"+screenId+" - "+rutGetElementValue("h0-title") + (errorCode?" Last Error Code: "+errorCode : "");//###27
    //###27 END
    var screenUrl=window.location.pathname; //window.location.href;
    if (screenUrl.indexOf('#')>=0){
        screenUrl=screenUrl.substring(0,screenUrl.indexOf('#'));
    }
    var cr='\r\n'; //'%0D%0A'; 
    var body=[];
    var settings=null;
    var changes=null;
    var sort=null;;
    var action=null;
    //###21 BEGIN
    //browser detection
    var browser=null;
    if (false || !!document.documentMode) browser='IE 6-11';
    else if (!!window.chrome && !!window.chrome.webstore){ browser="Chrome 1+";}
    else if (typeof InstallTrigger !== 'undefined'){browser="Firefox 1.0+";}
    else if (!!window.StyleMedia){Brwoser="Edge 20+";} //end not IE
    else if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0){browser="Opera 8.0+";}
    else if (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))){browser="Safari 3.0+";}
    else {browser="other";}
    //###21 END
    body.push("TO:Service Desk"); //###25
    body.push("I have the following issue with DCS and would kindly ask for help:");//###25 ###27
    body.push(".......................");//###25
    body.push(".......................");//###25
    //###24 BEGIN
    //if (!!_rutLastError){ //###25
    if (typeof _rutLastError !== 'undefined'){ //###25
        body.push("Last error: "+_rutLastError.code+" - "+_rutLastError.message);
    }
    //###24 END
    body.push("");
    body.push("BROWSER AND LOCALISATION DETAILS");
    body.push("-----------------------------------");
    body.push("URL: "+screenUrl);
    body.push("Browser: "+browser);
    //###21 BEGIN
    var n=1000.10;
    body.push("Language: "+navigator.language+
            ", Decimal format: "+n.toLocaleString()+
            ", Offset to UTC time zone: "+(new Date()).getTimezoneOffset());
    body.push("");
    //###21 END
    body.push("SCREEN DETAILS:");
    body.push("-----------------------------------");
    body.push("User:"+rutGetElementValue("h0-userData")); //###27
    body.push("Screen: "+screenId+" - "+rutGetElementValue("h0-title"));//###27
    body.push("");
    body.push("Field Values:");
    body.push("-----------------------");
    $(".searchField,.dtlField").each(function(){
        body.push("id: "+this.id+", value: "+rutGetElementValue(this.id));
    });
    if (rutPowerTableIndex){
        body.push("");
        body.push("Table Settings:");
        body.push("-----------------------");
        for (var i=0;i<rutPowerTableIndex.length;i++){
            settings=rutPowerTableIndex[i].settings;
            body.push("Table: "+rutPowerTableIndex[i].containerId+" has "+settings.sortAndSearch.map.length+" rows, sort dialog "+
                ((settings.sortAndSearch.dialogOpen)?'is':'is not')+" open");
            if (settings.paging.hasPaging){
//                body.push("...... Paging with page "+settings.paging.lastPageNumber+" of "+settings.paging.lastPageNumber+" pages shown");//###27
                body.push("...... Paging with page "+settings.paging.activePage+" of "+settings.paging.lastPageNumber+" pages shown"); //###27
            }//endif hasPaging
            else {
                body.push("...... No paging");
            }//endelse no paging
            body.push("...... Search is "+((settings.sortAndSearch.search)?settings.sortAndSearch.search:'empty'));
            //now find the sorts
            if (settings.sortAndSearch.sortDefinition.length>0){
                body.push("...... Table sort:");                
                for (var j=0;j<settings.sortAndSearch.sortDefinition.length;j++){
                    sort=(settings.sortAndSearch.sortDefinition[j].ascDesc==1)?'ascending':'descending';
                    body.push("...... Sort by column "+settings.sortAndSearch.sortDefinition[j].sortColumn+" "+sort);
                }
            } //endif there is sort
            else {
                body.push("...... No table sorts");
            }   //end else no sort    
            //now find the changes
            changes=rptGetInsertsAndChanges(rutPowerTableIndex[i].containerId);
            if (changes.length>0){
                body.push("...... Table changes:");
                for (var j=0;j<changes.length;j++){
                    action=(changes[j].action=='i')?'insert': ( (changes[j].action=='u')?'update':'delete');
                    body.push("...... "+action+" of row id:"+changes[j].rowId);
                }
            } //endif there are changes
            else {
                body.push("...... No table changes");
            }
        }// en for each PowerTable

    }//end if there are PowerTables
    var bodyText=encodeURIComponent(body.join(cr));
    //window.open("mailto:"+receiver+"?subject="+subject+"&body="+bodyText); //###25
    window.open("https://outlook.office.com/?path=/mail/action/compose&to="+receiver+"&subject="+subject+"&body="+bodyText); //###25
    //window.location.href='mailto:mail@example.org';">
    //cc, bcc, references
}//end function rutSendSupportEmail
/** Saves a text file. 
 *  @param {String} text The text to place into the file
 *  @param {String} fileName The name of the file
 *  @param {String} mimeType The mimeType of the file

    *  @author Ascan Heydorn
    *  @private
    * */
function rutSaveAsFile(text,fileName,mimeType) {
    try {
        var blob = new Blob([text],{type:mimeType});
        saveAs(blob, fileName);
    } catch (exc) {
        window.open("data:"+mimeType+"," + encodeURIComponent(text), '_blank','');
    }
}//end function rutSaveAsFile

/**Does an automatic code review and saves results into a csv file.
 * 
 * @author Ascan Heydorn
 */
function rutCheckCodeQuality(){
    var colSep=',';
    var cr='\r\n';
    var element=null;
    var id=null;
    var tag=null;
    var fieldClass=null;
    var areaClass=null;
    var parent=null;
    var found=null;
    var check=null; // content of data-check
    var type=null; // type attribute
    var customTag=null; //content of data-ct
    var originalType=null; //the original tag type as stored in data-type
    var dataField=null; //id.substring(3)
    var fieldLength=0;
    if (0<=navigator.language.indexOf("de")){ colSep=';'} //German separator is ; //###23
    //if (0<=rutGetLanguage().indexOf('de')){colSep=';'} //German column separator is ; ###18 need to improve
    /*Checks and errors
    100 base errors id's missing, classes missing, hierarchy of elements, incorrect types, comments missing
    101 body id missing
    102 id on input,select, textarea missing
    103 id first char must be from sdthf
    111 classlist empty on input,select,textarea, missing field class
    112 missing field class but classlist not empty (input, select, textarea)
    121 field should be in a parent area
    122 parent area should have id
    123 field and parent area have same id prefix
    180ff comment issues
    200 missing attributes like dec, min
    201 text should have len
    202 number should have dec
    203 number should have min(0)
    300 missing stdSelectTables
    400 missing std custom tags
    401 text but no rcl:text
    411ff missing specialised custom tags
    421ff missing base custom tags
    */
    report=[];
    report.push("Code Quality Issue Report");
    report.push("");
    report.push("Issue#"+colSep+'Id'+colSep+'Description');

    var children=document.childNodes; //###23
    var commentCount=0;
    //###23 BEGIN
    var commentHasDate = false;
    var commentHasTaskRef=false;
    var commentHasUserRef=false;   
    var commentHasShortDesc=false;  
    var commentHasCopyright=false;  
    var commentHasAuthor=false;  
    var commentHasChangeLog=false;  
    // Check code
    if (true){ //###27 just to have ability to collapse the code in the editor
        for (var i=0; i<Math.min(children.length,5); i++) {
            if (children[i].nodeType == Node.COMMENT_NODE) {
                commentCount++;   
                if (children[i].textContent.indexOf('## DD/MM/YY')>=0) { commentHasDate=true;}
                if (children[i].textContent.indexOf('User')>=0){commentHasUserRef=true;}  
                if (children[i].textContent.indexOf('TaskRef')>=0){commentHasTaskRef=true;} 
                if (children[i].textContent.indexOf('Short Description')>=0){commentHasShortDesc=true;} 
                if (children[i].textContent.indexOf('Copyright RCL Public Co.')>=0){commentHasCopyright=true;} 
                if (children[i].textContent.indexOf('Author')>=0){commentHasAuthor=true;} 
                if (children[i].textContent.indexOf('Change Log')>=0){commentHasChangeLog=true;} 
            }
        }
        if (commentCount==0){
            report.push('180'+colSep+colSep+'Initial comment with changelog and copyright missing');
        }
        if (!commentHasCopyright){
            report.push('181'+colSep+colSep+'Initial comment has no copyright remark (Copyright RCL Public Co.)');
        } 
        if (!commentHasAuthor){
            report.push('182'+colSep+colSep+'Initial comment has no author (Author)');
        } 
        if (!commentHasChangeLog){
            report.push('183'+colSep+colSep+'Initial comment has no change log (Change Log)');
        } 
        if (!commentHasDate){
            report.push('184'+colSep+colSep+'Initial comment has no date column in change log (## DD/MM/YY)');
        }    
        if (!commentHasUserRef){
            report.push('185'+colSep+colSep+'Initial comment has no user column in change log (User)');
        } 
        if (!commentHasTaskRef){
            report.push('186'+colSep+colSep+'Initial comment has no TaskRef column in change log (TaskRef)');
        }
        if (!commentHasShortDesc){
            report.push('187'+colSep+colSep+'Initial comment has no description column in change log (Short Description)');
        }
    }//###27 just to have ability to collapse the code in the editor
    
    //###23 END
    //1) Check whether body as id    
    element=document.getElementsByTagName("body");
    if (!this.id){
        report.push('101'+colSep+colSep+'<body> has no id. Should be screen id');
    }
    //2) Step through all input, select, textarea
    var classList=null; //the classlist of a tag
    var excemptedField=false; //###27 indicates, whether a field is excempted from certain rules
    $('div').each(function(){
        element=this;
        customTag=element.getAttribute('data-ct')||' ';
        if (classList){
            if ( classList.contains('rcl-standard-navigation-bar') &&
                 (customTag.indexOf('tabGroup')<0)
               ){
                if (!this.id){ id='<id is missing>';}
                else id=this.id;
                report.push('429'+colSep+id+colSep+'<div> Tab groups should use custom tag <rcl:tabGroup>');                     
            }
            else if ( classList.contains('tab-pane') &&
                      (customTag.indexOf('tabContentPane')<0)
               ){
                if (!this.id){ id='<id is missing>';}
                else id=this.id;
                report.push('430'+colSep+id+colSep+'<div> Tab panes should use custom tag <rcl:tabContentPane>');                     
            }
        }
    }); //end $('div').each.function
    $('input, select, textarea').each(function(index){
        element=this;
        tag=this.nodeName.toLowerCase();
        excemptedField=false; //###27
        //id missing?
        if(!this.id){
            report.push('102'+colSep+this.nodeName+colSep+'Id missing. Every field with data should have an id');        
        }
        id=this.id;
        //incorrect 1st char of id?
        if ('hsdt'.indexOf(id.charAt(0))<0){
            report.push('103'+colSep+id+colSep+'Id should start with s, d, t or h');        
        }
        //field class missing?
        if (id.charAt(0)=='s') {fieldClass='searchField';areaClass='searchArea';}
        else if (id.charAt(0)=='d') {fieldClass='dtlField';areaClass='dtlArea';}
        else if (id.charAt(0)=='t') {fieldClass='tblField';areaClass='tblArea';}
        else if (id.charAt(0)=='h') {fieldClass='hdrField';areaClass='hdrArea';}
        classList=this.classList||null;
        //###27 BEGIN defining an excempted field
        if (((id.substring(3)=='pgIndicator') && (id.charAt(0)=='t') ) || //this is from PowerTable paging
            ((id.substring(3)=='pgGotoField') && (id.charAt(0)=='t')) ||  //this is from PowerTable paging 
            (id.charAt(0)=='h')     //we allow the header without class        
            ){
                excemptedField=true;
        }
        //###27 END
        if (!classList){
            report.push('111'+colSep+id+colSep+'<'+tag+'> has empty classlist. Should have class '+fieldClass); 
        }
        //###27 BEGIN adding a few more exceptions
        //else if (classList.contains('searchField') ||
        //         classList.contains('dtlField') ||
        //         classList.contains('hdrField') ||
        //         classList.contains('tblField') ||){ /* all ok */ 
        //}
        else if (classList.contains('searchField') ||
                 classList.contains('dtlField') ||
                 classList.contains('hdrField') ||
                 classList.contains('tblField') ||
                 (excemptedField)             
                 ){ /* all ok */ 
        }
        //###27 END
        else {
            report.push('112'+colSep+id+colSep+'<'+tag+'> should have class '+fieldClass); 
        }
        //field is in an area?
        parent=element.parentNode;
        found=false;
        while (parent!=null){
            if (parent.classList!=null){
                if (parent.classList.contains(areaClass)) {
                    found=true;
                    break;
                }
            }
            parent=parent.parentNode;
        }
        if (!found){
            if (!excemptedField) {  //###27
                report.push('121'+colSep+id+colSep+'<'+tag+'> should have parent of class '+areaClass);   
            }//###27          
        }
        else { //now parent is the area, both should have same prefix
            if (!parent.id){
                report.push('122'+colSep+id+colSep+'<'+tag+'> has parent of class '+areaClass+ " but this parent has no id");             
            }
            else if (id.substring(0,3)!=parent.id.substring(0,3)){
                report.push('123'+colSep+id+colSep+'<'+tag+'> has parent of class '+areaClass+ " but this parent has id "+
                    parent.id+" of different prefix");             
            }
        }
        check=element.getAttribute('data-check')||' ';
        fieldLength=0;
        var n1=check.indexOf('len(');
        if (n1>=0){
            var n2=check.substring(n1+4).indexOf(')');
            if (n2>=0){
                fieldLength=check.substring(n1+4,n1+4+n2);
            }
        }
        customTag=element.getAttribute('data-ct')||' ';
        originalType=element.getAttribute('data-type')||' ';
        dataField=id.substring(3).toLowerCase();
        displayList =[]; //An array of display options
        valueList=[]; //An array of select values
        valueString=""; //the valueList joined
        displayString="";//the displayList joined
        if (tag=='input'){
            type=element.getAttribute('type');
            if (type=='text' || originalType=='text'){
                //text must have len
                //###27 BEGIN adding some exceptions
                //if (check.indexOf('len(')<0){ 
                if ((check.indexOf('len(')>=0) ||
                    (excemptedField)    
                    ){ /* all ok */ 
                }
                else {
                //###27 END
                    report.push('201'+colSep+id+colSep+'<'+tag+'> of type text should have len(n) defined');             
                }
                //text must come from customTag text
                //if (customTag==' '){ //###27
                if ((customTag==' ')&&(!excemptedField)){ //###27
                    report.push('401'+colSep+id+colSep+'<'+tag+'> of type text should use custom tag rcl:text or more specialised');                     
                }
                //else if (customTag.indexOf('text')<0){ //###27
                else if ((customTag.indexOf('text')<0)&&(!excemptedField)){ //###27
                    report.push('401'+colSep+id+colSep+'<'+tag+'> of type text should use custom tag rcl:text or more specialised');             
                }
                //no stdTag was used, check for stdTags and incorect base tags (better time, url, date, email)
                if (customTag.indexOf('std')<0){
                    if ((dataField.indexOf('pot')>=0) &&  //pot +len5 + !terminal
                        (fieldLength==5) &&
                        (dataField.indexOf('terminal')<0)
                       ) {
                        report.push('411'+colSep+id+colSep+'<'+tag+'> of type text should use special custom tag stdPort');             
                    }
                    else if ((dataField.indexOf('pol')>=0) &&
                             (fieldLength==5) &&
                             (dataField.indexOf('terminal')<0)                    
                            ){
                        report.push('412'+colSep+id+colSep+'<'+tag+'> of type text should use special custom tag stdPort');             
                    }
                    else if ((dataField.indexOf('pod')>=0) && 
                             (fieldLength==5) &&
                             (dataField.indexOf('terminal')<0)
                            ){
                        report.push('413'+colSep+id+colSep+'<'+tag+'> of type text should use special custom tag stdPort');             
                    }
                    else if ((dataField.indexOf('vessel')>=0) && 
                             (fieldLength==5) 
                            ){
                        report.push('414'+colSep+id+colSep+'<'+tag+'> of type text should use special custom tag stdVessel');             
                    }
                    else if ((dataField.indexOf('service')>=0) && 
                             (fieldLength==5)                     
                            ) {
                        report.push('415'+colSep+id+colSep+'<'+tag+'> of type text should use special custom tag stdService');             
                    }
                    else if ((dataField.indexOf('fsc')>=0) && 
                             (fieldLength==3)                       
                            ){
                        report.push('416'+colSep+id+colSep+'<'+tag+'> of type text should use special custom tag stdFSC');             
                    }              
                    //should have used tel, email, time or url
                    else if ((0<=dataField.indexOf('email')) && 
                             (fieldLength==80)                     
                            ){
                        report.push('421'+colSep+id+colSep+'<'+tag+'> of type text should be custom tag <rcl:email>');                          
                    }
                    else if (0<=dataField.indexOf('time')){
                        report.push('422'+colSep+id+colSep+'<'+tag+'> of type text should be custom tag <rcl:time>');                          
                    }
                    else if (0<=dataField.indexOf('web')){
                        report.push('423'+colSep+id+colSep+'<'+tag+'> of type text should be custom tag <rcl:url>');                          
                    }
                    else if (0<=dataField.indexOf('date')){
                        report.push('424'+colSep+id+colSep+'<'+tag+'> of type text should be custom tag <rcl:date>');                          
                    }
                    else if ((0<=dataField.indexOf('fax')) && 
                             (fieldLength==17)                        
                            ){
                        report.push('425'+colSep+id+colSep+'<'+tag+'> of type text should be custom tag <rcl:tel>');                          
                    }
                    else if ((0<=dataField.indexOf('tel')) && 
                             (fieldLength==17)                      
                            ){
                        report.push('426'+colSep+id+colSep+'<'+tag+'> of type text should be custom tag <rcl:tel>');                          
                    }
                    else if ((0<=dataField.indexOf('voyage')) && 
                             (fieldLength==10)                      
                            ){
                        report.push('417'+colSep+id+colSep+'<'+tag+'> of type text special custom tag stdVoyage');                          
                    }
                    else if ((0<=dataField.indexOf('point')) && 
                             (fieldLength==5)                      
                            ){
                        report.push('418'+colSep+id+colSep+'<'+tag+'> of type text special custom tag stdPoint');
                    }                          
                    else if ((0<=dataField.indexOf('del')) && 
                             (fieldLength==5) &&
                             (dataField.indexOf('haul_loc')<0)                     
                       ){
                       report.push('419'+colSep+id+colSep+'<'+tag+'> of type text special custom tag stdPoint');       
                    }
                }//endif no stdTag used
            }//endif type=text
            else if (type=='number' || originalType=='number'){
                //number must have dec, even disabled fields for proper display.
                if (check.indexOf('dec(')<0){
                    report.push('202'+colSep+id+colSep+'<'+tag+'> of type number should have dec(n,m) defined');             
                }
                //number must come from customTag number
                //if (customTag.indexOf('number')<0){ //###27
                if ((customTag.indexOf('number')<0)&&(!excemptedField)){ //###27
                    report.push('427'+colSep+id+colSep+'<'+tag+'> of type number should use custom tag number or more specialised');             
                }
                //Check nonnegative numeric fields, but only if not disbled or readonly
                if ( (0>=check.indexOf("min(0)")) && (true!=this.getAttribute('readonly')) && (!this.disabled)){
                    if ((dataField.indexOf("width")>=0)  ||
                        (dataField.indexOf("height")>=0) ||
                        (dataField.indexOf("weight")>=0) ||
                        (dataField.indexOf("length")>=0) ||
                        (dataField.indexOf("measurement")>=0) ||
                        (dataField.indexOf("pkgs")>=0)      ||
                        (dataField.indexOf("breadth")>=0)   ||
                        (dataField.indexOf("bundle")>=0)    ||
                        (dataField.indexOf("co2")>=0)       ||
                        (dataField.indexOf("days")>=0)      ||    
                        (dataField.indexOf("diameter")>=0)  ||                        
                        (dataField.indexOf("equip_vgm")>=0) ||                        
                        (dataField.indexOf("humidity")>=0)  ||
                        (dataField.indexOf("nitrogen")>=0)  ||
                        (dataField.indexOf("oxygen")>=0)    ||
                        (dataField.indexOf("percent")>=0)   ||
                        (dataField.indexOf("ton")>=0)       ||
                        (dataField.indexOf("qty")>=0)       ||
                        (dataField.indexOf("age")>=0)       ||
                        (dataField.indexOf("axles")>=0)     ||
                        (dataField.indexOf("extra_back")>=0)    ||                        
                        (dataField.indexOf("extra_front")>=0)   ||
                        (dataField.indexOf("void_slot")>=0)     ||
                        (dataField.indexOf("gross")>=0)    
                    )
                    {
                        report.push('203'+colSep+id+colSep+'<'+tag+'> of type number is likely not negative (min(0)). Please verify');       
                    }
                }//endif number fields which might be nonnegative
            }//endif type number
            else if (type=='date' || originalType=='date'){
                //date must come from customTag date
                if (customTag.indexOf('date')<0){
                    report.push('428'+colSep+id+colSep+'<'+tag+'> of type date should use custom tag date or more specialised');             
                }
            }//endif type date
        }//end if <input>
        else if (tag=='select'){
            displayList=[];
            if (0>customTag.indexOf("tb-")){
                $('#'+id).find("option").each(function(){
                    displayList.push(this.text);
                    valueList.push(this.value);
                });
                displayString=';'+displayList.join(';')+';';
                valueString=';'+valueList.join(';')+';'
                if (0<=displayString.indexOf(';BKK;')){
                    report.push('301'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable ShipmentType');             
                }    
                else if (0<=displayString.indexOf(';COC;')){
                    report.push('311'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable SocCoc');             
                }    
                else if (0<=displayString.indexOf(';Farenheit;')){
                    report.push('312'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable TemperatureMeasure');             
                }    
                else if (0<=displayString.indexOf(';Collect;')){
                    report.push('313'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable PrepaidCollect');             
                }    
                else if (0<=displayString.indexOf(';local;')){
                    report.push('314'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable PortStatus');             
                }    
                else if (0<=displayString.indexOf(';Canvas;')){
                    report.push('315'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable PackageMaterial');             
                }    
                else if (0<=displayString.indexOf(';Origin;')){
                    report.push('316'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable OriginDestination');             
                }    
                else if (0<=displayString.indexOf(';Truck;')){
                    report.push('317'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable ModeOfTransport');             
                }    
                else if (0<=displayString.indexOf(';Revenue Ton;')){
                    report.push('318'+colSep+id+colSep+'<'+tag+'> LclRateBasis');          
                }    
                else if (0<=displayString.indexOf(';Laden;')){
                    report.push('319'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable LadenEmpty');             
                }    
                else if (0<=displayString.indexOf(';Haul.Loc;')){
                    report.push('302'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable InlandLocationType or InlandLocationTypeWithTruck');             
                }    
                else if (0<=displayString.indexOf(';Imperial;')){
                    report.push('303'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable ImperialMetric');             
                }    
                else if (0<=displayString.indexOf(';Merchant;')){
                    report.push('304'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable Haulage');             
                }    
                else if (0<=displayString.indexOf(';Under Deck;')){
                    report.push('305'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable Handling Instructions');              
                }    
                else if (0<=valueString.indexOf(';UD;')){
                    report.push('305'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable Handling Instructions');              
                } 
                else if (0<=displayString.indexOf(';Percent;')){
                    report.push('306'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable FreightSurchargeBasis');             
                }    
                else if (0<=valueString.indexOf(';%;')){
                    report.push('306'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable FreightSurchargeBasis');             
                }   
                else if (0<=displayString.indexOf(';40FT;')){
                    report.push('307'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable EqSize');             
                }    
                else if (0<=valueString.indexOf(';40;')){
                    report.push('307'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable EqSize');             
                }
                else if (0<=displayString.indexOf(';Grade A;')){
                    report.push('308'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable EquipmentGrade');             
                }    
                else if (0<=displayString.indexOf(';North East;')){
                    report.push('309'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable Direction');             
                } 
                else if (0<=valueString.indexOf(';NE;')){
                    report.push('309'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable Direction');             
                }    
                else if (0<=displayString.indexOf(';Contract Party;')){
                    report.push('321'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable CustomerFunction');             
                }    
                else if (0<=displayString.indexOf(';Consignee;')){
                    report.push('322'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable CustomerType');             
                }    
                else if (0<=displayString.indexOf(';Fax;')){
                    report.push('323'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable Communication');             
                }   
                else if (0<=valueString.indexOf(';DA;')){
                    report.push('324'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable RateType');             
                }   
                else if (0<=valueString.indexOf(';WBHFax;')){
                    report.push('325'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable SpecialCargo');             
                }   
                else if (0<=valueString.indexOf(';FN;')){
                    report.push('326'+colSep+id+colSep+'<'+tag+'> should use stdSelecTable ContainerLoadingRemarks');             
                } 
            }//endif select without stdSelectTable
        }//endif <select>
        //Check on dataField
    });//end $("input"....) each
    rutSaveAsFile(report.join(cr),"Code-Review-Report.csv","text/plain;charset=utf-8");
}//end function rutCheckCodeQuality
    /*TODO 
    * Check buttons and href
    2) Check header
    4) Check search area data-ct=searchArea
    5) label should have for

/** Converts a date to ISO (yyyy-mm-ddd) or british (dd/mm/yyyy) date format
 * Format of input date can be both
 * @param {string} input The date string to be converted
 * @param {boolean} toIsoDate true if input to be converted to ISO
 * @author Ascan Heydorn
 */
function rutConvertDate(input,toIsoDate){ //from any to ISO or british
    if (input.length<10){return null;}//###17
    if (toIsoDate){
        if (input.charAt(2)=='/'){
            return input.substring(6)+'-'+input.substring(3,5)+'-'+input.substring(0,2);
        }
        else if (input.charAt(4)=='-'){
            return input;
        }
        else {
            return null;
        }
    }// endif to iso date format
    else {
        if (input.charAt(2)=='/'){
            return input;
        }
        else if (input.charAt(4)=='-'){
            return input.substring(8)+'/'+input.substring(5,7)+'/'+input.substring(0,4);
        }
        else {
            return null;
        }                
    } //end else to british date format
}//end function rutConvertDate
/** Converts a time to ISO (hh:mm) or numeric (hhmm) time format
 * Format of input date can be both
 * @param {string} input The time string to be converted
 * @param {boolean} toIsoTime true if input to be converted to ISO
 * @author Ascan Heydorn
 */
function rutConvertTime(input,toIsoTime){ //from any to ISO or numeric
    if (input.length<3){return null;}//###17
    if (toIsoTime){
        if (input.charAt(2)==':'){
            return input;
        }
        else if (input.indexOf(':')<0){
            var txt='0000'+input; //01234567
            var l=txt.length;
            txt=txt.substring(l-4);
            return txt.substring(0,2)+':'+txt.substring(2,4);
        }
        else {
            return null;
        }
    }//endif usesIsoTime
    else {//to numeric
        if (input.charAt(2)==':'){
            return input.substring(0,2)+input.substring(3,5);
        }
        else if (input.indexOf(':')<0){
            return input
        }
        else {
            return null;
        }                
    }//end else usesIsoTime
} // end function rutConvertTime    
//function rutOpenDropdownMenue(id, itemList, functionList){  //###27
function rutOpenDropdownMenue(id, itemList, functionList, connectedElement){
    //###19 BEGIN
    var element=document.getElementById(id);
    if (element!=null){
        element.parentNode.removeChild(element); //The previous message was closed by cross button
    }
    //###19 END
    itemArray=itemList.split(' ');
    functionArray=functionList.split(' ');
    html=[];
    boxWidth=0; //###27
    cr='\r\n';
    html.push('\t<div id="'+id+'" class="rcl-dropdown-content">');
    for (var i=0;i<itemArray.length;i++){
        html.push('\t\t<a href="javascript:'+functionArray[i]+';">'+rutToTitleCase(itemArray[i])+'</a>');
        boxWidth= (itemArray[i].length>boxWidth)?itemArray[i].length:boxWidth; //###27
    }
    html.push('\t</div> <!-- end dropdown-content -->');
    $("body").append( html.join(cr));
    element=document.getElementById(id);
    //###27 BEGIN position it better
    if (connectedElement!=null){
        //Say our longest entry is 20 and button is 5
        //The entry has lenght 5*button
        // so I need to check rect.right - (5*) 
        var rectParent=connectedElement.getBoundingClientRect();
        var factor=boxWidth / (rutGetElementValue(connectedElement).trim().length +5); //5 chars for a potential icon
        var n=( rectParent.right -  (factor * (rectParent.right-rectParent.left)));
        if (n<0) {
            n=rectParent.left;
        }
        element.style.left = n+'px';
        element.style.top = (rectParent.bottom+2)+'px';

    }
    //###27 END
    element.classList.add("d-block");
}   
function rutCloseDropdownMenue(){
    var dropdowns = document.getElementsByClassName("rcl-dropdown-content");
    for (var i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('d-block')) {
            openDropdown.classList.remove('d-block');
            //document.getElementById(openDropdown.id).remove();//###19
            openDropdown.parentNode.removeChild(openDropdown);//###19
        }
    }    
}//end function rutCloseDropdownMenue
//###18 BEGIN
function rutCheckKeydown(event){
    if (event.ctrlKey&&(event.key=='1')){
        rutSendSupportEmail(true);
        event.preventDefault();
        return false;
    }
    else if (event.ctrlKey&&(event.key=='2')){
        rutCheckCodeQuality();
        event.preventDefault();
        return false;
    }
    else if (event.ctrlKey&&(event.key=='3')){
        rutOpenDropdownMenue('m0-area', 'open_ticket inspect_code cancel', 'rutSendSupportEmail(true) rutCheckCodeQuality() rutCloseDropdownMenue()');
        event.preventDefault();
        return false;
    }
    return true;
}//end function rutCheckKeydown
//###18 END
//###19 BEGIN
/**Opens a simple message box and displays a message. Depending on input an ok and/or cancel button 
 * are displayed and upon pressing these buttons certain functions are called and the box is closed.
 * When messageCode is present it is assumed to be an error and message and messageCode are placed into the global
 * variable _rutLastError={ code:messageCode, message:message}
 * 
 * @param {String} title The title of the message box
 * @param {String} message The message being displayed. 
 * @param {String} messageCode Optional code for display and emailing
 * @param {String} cancelFunction If present a cancel button is displayed and this function is executed before the box is closed
 * @param {String} okFunction If present an ok button is displayed and this function is executet before the box is closed
 * @author Ascan Heydorn
 * @private
 */
//function rutOpenMessageBox(title, message, cancelFunction, okFunction){ 
function rutOpenMessageBox(title, message, messageCode, cancelFunction, okFunction){ //###24
    var html=[];
    var cr='\r\n';
    var element=document.getElementById('by-message');
    if (element!=null){
        element.parentNode.removeChild(element); //The previous message was closed by cross button
    }
    html.push('<div id="by-area" title="'+title+'" style="display:none;">');
    //###24 BEGIN
    if ((messageCode!=null)&&(messageCode!='')){ 
        html.push('<p id="by-messageCode" class="text-center bg-info lead pb-0 mb-0 text-white" >'+messageCode+'</p>'); //mark
        _rutLastError = {code:messageCode, message:message};           
    }
    else {
        _rutLastError=null;              
    }
    //###24 END
    html.push('<p id="by-message" class="text-center bg-info pt-0 mt-0 pb-0 text-white" >'+message+'</p>');  //mark
    html.push('</div><!-- end of message dialog b9zarea -->');
    var buttons=[];
    if (okFunction!=null){
        buttons.push( { text: "ok",
                         click: function(){
                                eval(okFunction);
                                $(this).dialog("close");
                                this.parentNode.removeChild(this);
                        }
                    });
    } //endif okFunction
    if (cancelFunction!=null){
        buttons.push({  text:"cancel",
                        click: function(){
                                eval(cancelFunction);
                                $(this).dialog("close");
                                this.parentNode.removeChild(this);
                        }
                    });
    } //endif cancel
    $("body").append( html.join(cr));
	$("#by-area").dialog({
		autoOpen: true,
		modal: true,
		draggable: false,
		resizable: false,
		position: {my: "center", at: "center", of: window } ,
		minWidth: 300,
        minHeight: 50,
        buttons: buttons,
    });//end $("#by-area").dialog
}//end function rutOpenMessageBox
//###19 END
function rutGetLookupData(lookupRequest){
    /*
    {userToken:"xxx",
     table:"VRL_PORT",
     select:
      [
          {column:"Country",value:"'TH'",operator:"=",dataType:null},
          {column:"Port",value:"''",operator:"="}
      ]
    }
    */
    //console.log("rutOpenLookupTable:"+JSON.stringify(lookupRequest));
    /* returns
    {
        data: [{}],
        metaData : [{columnName: String, columnType: Integer, displaySize:Integer, precision: Integer, scale: Integer}]
    }
    */
    var lookupResult=null;
    var data1=[
            {Customer:"ABC Company", Code:"THBKK",Port:"THBKK",HIDE_Code:"$",Container_no:"HTMU3456234",Amount:657,    ETA:"2018-07-01"},
            {Customer:"Longroon Ltd",Code:"INNSA",Port:"INNSA",HIDE_Code:"B",Container_no:"KLMU8942645",Amount:1565.33,ETA:"2018-07-01"},
            {Customer:"Longroon Ltd",Code:"THBKK",Port:"THBKK",HIDE_Code:"C",Container_no:"KLMU8942647",Amount:2165.33,ETA:"2018-08-13"},
            {Customer:"Longroon Ltd",Code:"SGSIN",Port:"INNSA",HIDE_Code:"D",Container_no:"KLMU8942649",Amount:2065.33,ETA:"2018-07-12"},
            {Customer:"Kiu San",     Code:"PHMNL",Port:"INNSA",HIDE_Code:"A",Container_no:"ZPNU9352173",Amount:1234.21,ETA:"2018-07-07"},
            {Customer:"ABC Company", Code:"CNSHA",Port:"THBKK",HIDE_Code:"B",Container_no:"ZKMU3456256",Amount:337,    ETA:"2018-07-01"},
            {Customer:"Longroon Ltd",Code:"INNSA",Port:"THBKK",HIDE_Code:"A",Container_no:"MLRU8989145",Amount:6165.27,ETA:"2018-07-01"},
            {Customer:"Longroon Ltd",Code:"CNSHA",Port:"SGSIN",HIDE_Code:"A",Container_no:"KMUU8942767",Amount:2343.00,ETA:"2018-09-17"},
            {Customer:"Longroon Ltd",Code:"HKHKG",Port:"SGSIN",HIDE_Code:"A",Container_no:"ZKFU8942649",Amount:65.11,  ETA:"2018-07-01"},
            {Customer:"Kiu San",     Code:"SGSIN",Port:"SGSIN",HIDE_Code:"X",Container_no:"SKLU9354392",Amount:1674,   ETA:"2018-07-01"}
        ];
        //columnType 12 String, 5: 4:
    var metaData1=[
        {columnName: "Customer", columnType: 12, displaySize: 15, precision: 15, scale:0 },
        {columnName: "Code", columnType: 12, displaySize: 5, precision: 5, scale:0 },
        {columnName: "Port", columnType: 12, displaySize: 5, precision: 5, scale:0 },
        {columnName: "HIDE_CODE", columnType: 1, displaySize: 1, precision: 1, scale:0 },
        {columnName: "Container_no", columnType: 12, displaySize: 12, precision: 12, scale:0 },
        {columnName: "Amount", columnType: 6, displaySize: 15, precision: 15, scale:2 },
        {columnName: "ETA", columnType: 12, displaySize: 10, precision: 10, scale:2 }
        ];
    var data2=[
            {Customer:"ABC Company", Container_no:"HTMU34562345", Amount:657.87},
            {Customer:"Longroon Ltd", Container_no:"KLMU8942645", Amount:1565.33},
        ];
    var metaData2=[
        {columnName: "Customer", columnType: 12, displaySize: 15, precision: 15, scale:0 },
        {columnName: "Code", columnType: 12, displaySize: 5, precision: 5, scale:0 },
        {columnName: "Port", columnType: 12, displaySize: 5, precision: 5, scale:0 },
        {columnName: "HIDE_CODE", columnType: 1, displaySize: 1, precision: 1, scale:0 },
        {columnName: "Container_no", columnType: 12, displaySize: 12, precision: 12, scale:0 },
        {columnName: "Amount", columnType: 6, displaySize: 15, precision: 15, scale:2 },
        {columnName: "ETA", columnType: 12, displaySize: 10, precision: 10, scale:2 }
        ];

    lookupResult={data:data1, metaData: metaData1};

    return lookupResult;
}//end function rutGetLookupData
/** Returns a date in ISO or numeric format after an optional calculation.
 * 
 * @param {String||ISODateString} expectedDate An ISO Date | "today" | "first|last of previous|this|next month" 
 * @param {integer} offset Number of days to add to expected date. Could be negative
 * @param {String} format  "ISO" (2018-11-06" | "num" (20181106)
 * @author Ascan Heydorn
 */
function rutGetDate(expectedDate, offset, format){
    var result=null;
    var date=null;
    if (expectedDate=="today"){
        date=new Date();
        result=date.toISOString();
    }
    else if (expectedDate.charAt(4)=='-' && expectedDate.charAt(7)=='-'){
        date=new Date(expectedDate);
    }
    else {
        //firsT/last of previous/this/next month
        date=new Date();
        var dayOffset=0;
        var monthOffset = null;
        if (expectedDate.indexOf("first")>=0){monthOffset=0;}  //0
        else if (expectedDate.indexOf("last")>=0){
            monthOffset=1;
            dayOffset=-1;}
        else {return null;}
        if (expectedDate.indexOf("previous")>=0) { 
            monthOffset=monthOffset-1;
        }
        else if (expectedDate.indexOf("next")>=0) { 
            monthOffset=monthOffset+1; 
        }
        else if (expectedDate.indexOf("this")>=0) {}
        else {return null;}
        date.setMonth(date.getMonth()+monthOffset); 
        date.setDate(1+dayOffset);
    }
    date.setDate(date.getDate()+(offset||0));
    if ((!format)||(format=='ISO')) {return date.toISOString().substring(0,10);}
    else if (format="num") {return ( (date.getFullYear() * 10000) + ((1+date.getMonth())*100) + date.getDate() );}
}//end function rutGetDate
//###26 BEGIN
/** Opens the tool menue for a page. This menue allows to create a ticket or run the self service code review
 *  @author Ascan Heydorn
 */
function rutToolMenue(){
    //###27 BEGIN attach drop down menue to the tool button
    var toolBtn=document.getElementById('h1-tool');
    //rutOpenDropdownMenue('m0-area', 'Open_ticket Self_service_code_review Cancel', 'rutSendSupportEmail(true) rutCheckCodeQuality() rutCloseDropdownMenue()');//###27
    rutOpenDropdownMenue('m0-area', 'Open_ticket Self_service_code_review Cancel', 
                        'rutSendSupportEmail(true) rutCheckCodeQuality() rutCloseDropdownMenue()',
                        toolBtn );
    //###27 END
}
//###26 END