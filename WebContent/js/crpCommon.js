Number.prototype.padLeft = function(base,chr){
   var  len = (String(base || 10).length - String(this).length)+1;
   return len > 0? new Array(len).join(chr || '0')+this : this;
}

function formatDateWithSepartor(dateString, separator) {
	var year        = dateString.substring(0,4);
	var month       = dateString.substring(4,6);
	var day         = dateString.substring(6,8);

	return day + separator + month + separator + year;
}

function formatDate(dateString) {
	return formatDateWithSepartor(dateString, '/');
}


function formatTime(timeString) {
	if( timeString.length > 0) {
		timeString = padLeft(timeString, 4);
	}
	
	var hour        = timeString.substring(0,2);
	var min       = timeString.substring(2,4);

	return hour + ':' + min;
}

function padLeft(str, max) {
	str = str.toString();
	return str.length < max ? padLeft("0" + str, max) : str;
}


function getTargetServer(){
	var baseurl = "http://" + location.hostname + (window.location.port != "" ? ":" + window.location.port : "") + "/";

	var baseurl2 = (location.hostname.indexOf('rclgroup') >= 0) ?
	('http://' + location.hostname + (window.location.port != '' ? ':' + window.location.port : '') + '/') :
	'http://marlin-ce.rclgroup.com' + (window.location.port != '' ? ':' + window.location.port : '') + '/';
	
	return baseurl;
}

//
//function replaceNull(someObj, replaceValue = "***") {
//  const replacer = (key, value) => 
//    String(value) === "null" || String(value) === "undefined" ? replaceValue : value;
//  //^ because you seem to want to replace (strings) "null" or "undefined" too
//  
//  return JSON.parse( JSON.stringify(someObj, replacer));
//}

// -------------------------------------------------------------------------------------------------------------------

var serviceContextPath = getTargetServer() + "CRPWebService/rclws/crp";

function getAuthorization(){
	/*
	var userData = 	sessionStorage.getItem("userData");
	// Encode the String
	var encodedString = btoa(userData);
	*/
	
	var encodedString = sessionStorage.getItem("userData");
	return encodedString;
}

function downloadFile(urlToSend) {

	var encodedString = getAuthorization();
	
     var req = new XMLHttpRequest();
     req.open("GET", baseurl + urlToSend);
	 req.setRequestHeader("Authorization", encodedString);


     req.responseType = "blob";
     req.onload = function (event) {
         var blob = req.response;
         var fileName = req.getResponseHeader("fileName") //if you have the fileName header available
//         var link=document.createElement('a');
//         link.href=window.URL.createObjectURL(blob);
//         link.download=fileName;
//         link.click();

		   var link = document.createElement("a");
		   link.setAttribute("href", baseurl + urlToSend);
		   link.innerHTML = fileName;
		   link.click();


		 $("body").find('.loading').remove();
     };


	 $("body").append('<div class="loading"></div>');
     req.send();


 }


$( document ).ajaxStop(function() {
	$("body").find('.loading').remove();
});

function ajaxRequest(uri, method, dataParam){
	var encodedString = getAuthorization();

	//console.log(">>> " + encodedString);
	
	var ajaxCall = null;
	
		/**
     * A function to be called if the request fails. 
     */
    var errorHandlerFunc = function(jqXHR, textStatus, errorThrown) {
	/*
        alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

        $('#result').html('<p>status code: '+jqXHR.status+'</p><p>errorThrown: ' + errorThrown + '</p><p>jqXHR.responseText:</p><div>'+jqXHR.responseText + '</div>');
        console.log('jqXHR:');
        console.log(jqXHR);
        console.log('textStatus:');
        console.log(textStatus);
        console.log('errorThrown:');
        console.log(errorThrown);
	*/
		if( jqXHR.responseJSON ) {
			var bodyResp = jqXHR.responseJSON;
			if( bodyResp.resultContent ) {
				dialogGenericv2("Error",  bodyResp.resultContent , "Ok");
			}
		}
    }

	if( method == 'GET') {
		ajaxCall = $.ajax({
			//param: getAsUriParameters(dataParam),
		    data: dataParam,
		    dataType: "json",
			contentType: 'application/json',
		    method : method,
		    async : true,
			cache: false,
			headers: {
			    "Authorization": encodedString
			},
		    url: serviceContextPath + uri,
			error: errorHandlerFunc
		});
		
	}else if (method == 'POST' || method == 'DELETE' ){
		
		ajaxCall = $.ajax({
			data: JSON.stringify(dataParam),
		    //data: dataParam,
		    dataType: "json",
			contentType: 'application/json',
		    method : method,
		    async : true,
			cache: false,
			headers: {
			    "Authorization": encodedString
			},
		    url: serviceContextPath + uri,
			error: errorHandlerFunc
		});
	
	}


	return ajaxCall;
}


function crpGetResultAjax(name, data, autoHandleResult) {
	if (autoHandleResult === undefined) {
		autoHandleResult = true;
	}
	
	var uri = "";
	var method = "";
	var Param = "";
	switch (name) {
		case "WS_CRP_STATUS":
			uri = "/voyage-status";
			method = "GET";
			break;
		
		case "WS_CRP_SEARCH":
			uri = "/voyage-list";
			method = "POST";
			break;
			
		case "WS_CRP_SCHEDULE_REPORT":
			uri = "/schedule-report";
			method = "POST";
			break;
		
		case "WS_CRP_GET_CURRENT_PORT":
			uri = "/current-port";
			method = "POST";
			break;
		
		case "WS_CRP_GET_DATA_PREDICTION":
			uri = "/data-prediction";
			method = "POST";
			break;
			
		case "WS_CRP_GET_CALL_REPORT_HIS":
			uri ="/call-report-history";
			method = "POST";
			break;
			
		case "WS_CRP_GET_CALL_REPORT_HIS_PREDICTION":
			uri = "/call-report-history-prediction";
			method = "POST";
			break;
			
		case "WS_CRP_SAVE_CALL_REPORT":
			uri = "/save-call-report";
			method = "POST";
			break;;
			
		case "WS_CRP_SEND_CALL_REPORT":
			uri = "/send-call-report";
			method = "POST";
			break;
			
		default:
			console.error("Error not found " + name);
			uri = "";
			break;
	}


	$("body").append('<div class="loading"></div>');

	return ajaxRequest(uri, method, data)
	.done(autoHandleResult)
	.always(function(jqXHROrData, textStatus, jqXHROrErrorThrown) { 
		//$("body").find('.loading').remove();
		
		if( jqXHROrErrorThrown.status != 200 ){
//			if( data ){
//				var resultCode = data.resultCode;
//				var resultMsg = data.resultMsg;
//				
//				dialogGenericv2("Error", resultMsg , "Ok");
//			}else{
//				dialogGenericv2("Error", "Your transaction cannot be completed" , "Ok");
//			}
		}
	});
	
//	if (!autoHandleResult) {
//		return AjaxPost(url, data, Param);
//	} else {
//		return AjaxPost(url, data, Param).done(handleAjaxResponse).fail(handleAjaxError).always(
//					function () {
//						$("body").find('.loading').remove();
//					});
//		
//	}
}


function drawPaging(containerId, paging){
	var settings=rptGetTableSettings(containerId);
    var prefix=settings.idPrefix;
//    var html='<div style="display:flex;justify-content:space-between;align-items:center;">' +
//        '<input id="'+prefix+'pgIndicator" class="'+settings.paging.indicatorClass+'" type="text" readonly="true">' +
//        '<div style="display:flex;justify-content:flex-start;align-items:center;">' +
//        '<a id="'+prefix+'pgGotoLabel" href="#" class="'+settings.paging.gotoLabelClass+'" onclick="rptGotoPage(\''+containerId+'\',\'goto\');">Goto</a>' +
//        '<input id="'+prefix+'pgGotoField" class="'+settings.paging.gotoFieldClass+'" type="number" size="2"'
//            +' data-check="dec(3,0)" min="0" onkeypress="rutCheckNumberKeypress(event);">' +
//        '</div>' +
//        '<div style="display:flex;justify-content:flex-end">' +
//        '<a id="'+prefix+'pgPrevious" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',\'previous\');">&lt;</a>' +                
//        '<a id="'+prefix+'pgFirst" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',\'first\');">1</a>'  + 
//        '<a id="'+prefix+'pg1" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',1);">2</a>' +    
//        '<a id="'+prefix+'pg2" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',2);">3</a>' +   
//        '<a id="'+prefix+'pg3" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',3);">4</a>' +   
//        '<a id="'+prefix+'pg4" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',4);">5</a>' +   
//        '<a id="'+prefix+'pg5" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',5);">6</a>' +   
//        '<a id="'+prefix+'pgLast" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',\'last\');">7</a>' +   
//        '<a id="'+prefix+'pgNext" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',\'next\');">&gt;</a>' +   
//        '</div>' +
//        '</div>';
	var pagingEasyFormatFlag = ( paging.totalPage < 5 ? true : false );   
	var styleHidden = "";
	if( pagingEasyFormatFlag ) {
		styleHidden = "display:none;";
	}
	
    var html='<div style="display:flex;justify-content:space-between;align-items:center;">' +
        '<input id="'+prefix+'pgIndicator" class="'+settings.paging.indicatorClass+'" type="text" readonly="true">' +
        '<div style="display:flex;justify-content:flex-start;align-items:center;">' +
        '<a id="'+prefix+'pgGotoLabel" href="#" class="'+settings.paging.gotoLabelClass+'" onclick="gotoPage(\''+containerId+'\',\'goto\');">Goto</a>' +
        '<input id="'+prefix+'pgGotoField" class="'+settings.paging.gotoFieldClass+'" type="number" size="2"'
            +' data-check="dec(3,0)" min="0" onkeypress="rutCheckNumberKeypress(event);">' +
        '</div>' +
        '<div style="display:flex;justify-content:flex-end">' +
        '<a id="'+prefix+'pgPrevious" class="'+settings.paging.buttonClass+'" style="' + styleHidden + '" href="#" onclick="gotoPage(\''+containerId+'\',\'previous\');">&lt;</a>' +                
        '<a id="'+prefix+'pgFirst" class="'+settings.paging.buttonClass+'" style="' + styleHidden + '" href="#" onclick="gotoPage(\''+containerId+'\',\'first\');">1</a>';

	
	if( paging.totalPage < 5 ){
		for(var i = 0; i < paging.totalPage; i++ ){
			var pageNo = i + 1;
			html += '<a id="'+prefix+'pg1" class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',' + pageNo + ');">' + pageNo + '</a>';
		}
		
	}else{
		
		var currentPageNo = paging.pageNo;
		var overTotalPageFlag = ( currentPageNo + 5 ) > paging.totalPage ? true : false;
		
		if( overTotalPageFlag == false ){
			for(var i = 0; i < 5; i++ ){
				var pageNo = null;
				if( currentPageNo == 1 ){
					pageNo = currentPageNo + i + 1;
				}else{
					pageNo = currentPageNo + i;
				}
				html += '<a class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',' + pageNo + ');">' + pageNo + '</a>';	
			}
			
		}else if( overTotalPageFlag == true ){
			for(var i = 5; i > 0; i-- ){
				var pageNo = currentPageNo - i;
				html += '<a class="'+settings.paging.buttonClass+'" href="#" onclick="gotoPage(\''+containerId+'\',' + pageNo + ');">' + pageNo + '</a>';	
			}
		}	
	}
	
	
	html += 
	    	'<a id="'+prefix+'pgLast" class="'+settings.paging.buttonClass+'" style="' + styleHidden + '" href="#" onclick="gotoPage(\''+containerId+'\',\'last\');">' + paging.totalPage + '</a>' +   
        	'<a id="'+prefix+'pgNext" class="'+settings.paging.buttonClass+'" style="' + styleHidden + '" href="#" onclick="gotoPage(\''+containerId+'\',\'next\');">&gt;</a>' +   
		'</div>'
        '</div>';

    $("#"+prefix+"paging").empty();
    $("#"+prefix+"paging").append(html);
    
}

/**Sets the values of the paging buttons and the paging indicator to their initial state
 * 
 * @param {String} containerId Id of table area
 * @author Ascan Heydorn 
 */
function crpResetPagingValues(containerId, paging){
	console.log("Call crpResetPagingValues");
	
	var settings = rptGetTableSettings(containerId);
    settings.paging.lastPageNumber = paging.totalPage;
	settings.paging.activePage = paging.pageNo;
	
	var prefix=settings.idPrefix;
    var lastPage = settings.paging.lastPageNumber;

	drawPaging(containerId, paging);

	if( paging == null ){
		return null;
	}    
    
/*
    if( paging.pageNo == 1 ) {
    	document.getElementById(prefix+'pgPrevious').classList.add("noPointer");
    }else{
    	document.getElementById(prefix+'pgPrevious').classList.remove("noPointer");
    }
    
    if( paging.pageNo == paging.totalPage ) {
    	document.getElementById(prefix+'pgNext').classList.add("noPointer");
    }else{
    	document.getElementById(prefix+'pgNext').classList.remove("noPointer");
    }
    
    rutSetElementValue(prefix+'pgFirst',1);
    rutSetElementValue(prefix+'pgLast',lastPage);
    var btnValue=1;
    //var btnReadOnly; //###27 
    for (var i=0;i<5;i++){
        btnValue++;  
        rutSetElementValue(prefix+'pg'+(1+i),(btnValue<lastPage)?btnValue:' ');
        //btnReadOnly=(btnValue<lastPage)? true: false; //###27

        if (btnValue < lastPage){
			document.getElementById(prefix+'pg'+(1+i)).classList.remove("noPointer");
		} //###27
        else {
			document.getElementById(prefix+'pg'+(1+i)).classList.add("noPointer");
		} //###27
        //document.getElementById(prefix+'pg'+(1+i)).readonly=btnReadOnly;//###27
    }


    document.getElementById(prefix+'pgFirst').focus(); //###27

*/
    rutSetElementValue(prefix+'pgIndicator','Page '+settings.paging.activePage+' of '+lastPage);


} //end function rptResetPagingValues



function crp_GetDataAreaToObject(area) {
	var obj = {};
	var div = document.getElementById(area);

	$(div).find('input:text, input:password, input:file, input[type="date"], input[data-type="date"], input:radio, input:checkbox, select, textarea')
		.each(function () {
			switch ($(this).attr("type")) {
				case 'checkbox':
					obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).is(":checked") ? "Y" : "N";
					break;
				case 'radio':
					obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).is(":checked");
					break;
				case 'date':
					obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = qtn_ConvertDateFormat($(this).val());
					break;
				default:
					if ($(this).attr("data-type") == 'date') {
						obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = qtn_ConvertDateFormat($(this).val());
					} else {
						obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).val();
					}
					break;
			}

		});
	return obj;
}

function crpGetData(divId){
	var result = {};
	
	$(divId).find(':input').each(function(){
		var inputId = $(this).attr('id');
		
		var id = inputId.replace(/.*-/g, '');
		
		//console.log(id + " >> " + $(this).val() );
		result[id] = $(this).val();
	})
	
	
	return result;

}

function crpGetUserDataFromHeader(){
	var divTarget = "#h3-hidden";
	var data = crpGetData(divTarget);

	return data;
}

