/*-----------------------------------------------------------------------------------------------------------
esnUtilities.js
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Thanapong Tienniem 10/10/2018
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY 	-User-     		-TaskRef-       -Short Description
01 10/10/18 	Thanapong                       Added function for using Ajax with jQuery
02 29/10/18		Pichit							Add
-----------------------------------------------------------------------------------------------------------*/

/*function getDataFormServerToTable(service, data, table, onDataDisplayed){

	rptClearDisplay(table);
	//showLoader(table);


//	alert("JSON.stringify(data) :"+JSON.stringify(data));

	getResultAjax(service, data)
		.done(handleTable);

	function handleTable(response){
		//hideLoader(table);
		//alert("response.Content :"+response.Content);
		//alert("response.Content :"+response);
        rptAddData(table, response);
		rptDisplayTable(table);
		if(onDataDisplayed){
			onDataDisplayed();
		}
	}
}*/

function getDataFormServerToTable(service, data, table, onDataDisplayed, onDataReceived){
	rptClearDisplay(table);
//	showLoader(table);

	getResultAjax(service, data)
		.done(handleTable);

	function handleTable(response){
//		hideLoader(table);

		if(onDataReceived){

			onDataReceived(response.resultContent);
		}
		console.log(JSON.stringify(response.resultContent));
        rptAddData(table, response.resultContent);
        rptDisplayTable(table);
		if(onDataDisplayed){
			onDataDisplayed();
		}
	}
}

function dialogConfirm(msg) {
    return dialogGeneric("Modal", msg, "Yes", "No");
}

function dialogGeneric(title, msg, yesLabel, noLabel){
	var buttons = {};
	if(yesLabel){
		buttons[yesLabel] = function(){
			$(this).dialog('close');
            $('body').find('#dialog-confirm').remove();
            deferred.resolve(true);
		}
	}
	if(noLabel){
		buttons[noLabel] = function () {
            $(this).dialog('close');
            $('body').find('#dialog-confirm').remove();
            deferred.resolve(false);
        }
	}

	var deferred = new $.Deferred();
    $('body').append('<div id="dialog-confirm"></div>')
    $("#dialog-confirm").html(msg);

    // Define the Dialog and its properties.
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: title,
        width: 300,
        buttons: buttons
    });

    // stuff -- resolve deferred once async method is complete
    return deferred.promise();
}

function dialogFadeout(msg, title, errorCode){
	return dialogGeneric(title ? title : "Warning", msg, "Ok", null, true, errorCode);
	/*
	if($('body').find('#dialog').length <= 0)
		$('body').append('<div id="dialog" style="display:none;"> ' + msg + ' </div>');
	else
		$('#dialog').html(msg);
		
	$('#dialog').dialog({
	    autoOpen: true,
	    show: "blind",
	    hide: "clip",
	    modal: true,
	    position: { at: "center top" },
	    open: function(event, ui) {
	    	//setTimeout(function() {
	        //	$('#dialog').dialog('close');
	        //	$('body').find('#dialog').remove();  
	    	//}, 12000);
	    }
	});
	*/
		
	//setTimeout(function() {
    //	$('body').find('#dialog').remove();  
	//}, 6000);
}

function setDisableEnableElement(id, disabled){
	$("#" + id).prop('disabled', disabled)
}

function hideParentElemnt(id){
	$("#" + id).parent().remove();
}

function removeElementWithIcon(id){
	$("#" + id).parent().parent().parent().remove();
}

function checkAction(oldobj, newobj){

	var action = 'n';

	if(!oldobj)
		action = 'i';

	else if (JSON.stringify(oldobj) !== JSON.stringify(newobj))
		action = 'u';

	return action;
}

function getDataAreaToObject(area){

	var obj = { };
	var div = document.getElementById(area);

	$(div).find('input:text, input:password, input:file, input[type="date"], input:radio, input:checkbox, select, textarea')
	      .each(function() {
		        	switch($(this).attr("type")) {
			            case 'checkbox':
			            	obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).is(":checked") ? "Y" : "N";
		                	break;
			            case 'radio':
		            		obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).is(":checked");
		                	break;
			            case 'date':
			            	obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = convertDateFormat($(this).val());
			            	break;
			            default:
			            	obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).val();
		            	    break;
	        			}
	      		});
	return obj;
}

function convertDateFormat(date){

      if (!date)
          return "";

      else {
    	  var d = date.split('-')[2];
          var m = date.split('-')[1];
          var y = date.split('-')[0];
          return y + m + d;
      }
}

function addIndexTable(list){
	for (i = 0; i < list.length; i++) {
		list[i].index = i + 1;
	}
	return list;
}

/**
 * Invoke callback for each row in specified Powertable. Callback will be provided with the following parameters
 * rowId : HTML id of the row
 * idNum : Numeric part extracted from rowId
 * index : Actual index of the row in current collection of rows
 * data: Display data. Provided only if useData = true
 * @param powerTableId Powertable to loop through
 * @param callback Callback to be invoked
 * @param useData Whether callback should be provided with display data or not
 * @param criteria Additional row criteria for filtering before invoking callback
 */
function rptForeachRow(powerTableId, callback, useData, criteria){
	var settings = rptGetTableSettings(powerTableId);
	var rowIndexStart = settings.rowId.length + 1;
	var jqueryResult = $("#" + powerTableId).find(".tblRow").not(":last");
	var index = 0;
	if(criteria){
		jqueryResult = jqueryResult.filter(criteria);
	}
	jqueryResult.each(function(){
		var rowId = $(this).attr("id");
		var idNum = parseInt(rowId.substring(rowIndexStart), 10);
		if(criteria){
			//Ignore index increment and search for actual index
			index = $(this).index();
		}
		if(useData){
			var rowData = rptGetDataFromDisplay(powerTableId, rowId);
			callback(rowId, idNum, index, rowData);
		}else{
			callback(rowId, idNum, index);
		}
		index++;
	});
}

function rptGetDataFromDisplayAll(containerId){

 	var listData = [];
 	$("#" + containerId).find(".tblRow").not(':last').each(function(){

   		var rowData = rptGetDataFromDisplay(containerId, $(this)[0].id);

   		rowData.rowId = $(this)[0].id;

   		listData.push(rowData);
   	});

   	return listData;
}

/**
 * Generate a map consisting of { rowId -> { Display data } }
 * @param containerId Powertable to extract data from
 * @returns A map
 */
function rptGetDataMapFromDisplay(containerId){
	var map = {};

	$("#" + containerId).find(".tblRow").not(':last').each(function(){
		var rowId = $(this).attr("id");
		var rowData = rptGetDataFromDisplay(containerId, rowId);
   		map[rowId] = rowData;
   	});

	return map;
}

function rptAddDataWithCache(containerId, data){
	var settings = rptGetTableSettings(containerId);
	var original = JSON.parse(JSON.stringify(data));
	settings.originalData = original;
	rptAddData(containerId, data);
}

/**
 * Return summarized version of history log of Powertable retrieved with rptGetInsertsAndChanges
 *
 * @returns {Array} An array of summarized log that has these characteristics:
 * - Each log element will contain key "index", specifying which data (in cache) is associated with this log
 * - rowId in history will be unique
 * - Action for existing row will be either 'u' or 'd'. Action for newly inserted row will always be 'i'
 */
function getSummarizedPtLog(containerId, action){
	var settings = rptGetTableSettings(containerId);
	var rowIndexStart = settings.rowId.length + 1; //Index to extract row index from element id

	//History log. Action for existing row will be either 'u' or 'd'. Action for new row will be either 'i' or 'd'
	var historyLog = rptGetInsertsAndChanges(containerId, action);

	var historyCache = {}; //rowId -> action
	for(var i=0; i<historyLog.length; i++){
		var h = historyLog[i];
		if(h.action === 'd' && historyCache[h.rowId] && historyCache[h.rowId].action === 'i'){
			//Any newly inserted row that gets deleted should be removed from log
			delete historyCache[h.rowId];
		}else{
			var rowIndex = parseInt(h.rowId.substring(rowIndexStart), 10);
			historyCache[h.rowId] = {action:h.action, index:rowIndex};
		}
	}

	historyLog = [];
	for(var rowId in historyCache){
		historyLog.push({ rowId:rowId, index:historyCache[rowId].index, action:historyCache[rowId].action });
	}

	return historyLog;
}

function getCurrentPtDataWithAction(containerId, clearLog, addRowId){
	var settings = rptGetTableSettings(containerId);
	var prefixLength = settings.configuration.prefixLength;
	var prefix = containerId.substring(0,prefixLength);
	var prechangeData = settings.originalData;

	//Extract pk field from template row
	var pks = []; //A list of pk field name (prefix removed)
	$("#"+settings.rowId).find(".pk").each(function(){
		var str = this.id.substring(prefixLength);
		pks.push(str);
		});

	//Get current displayed data map, tag all of them as no-change
	var dataMap = rptGetDataMapFromDisplay(containerId);
	for(var k in dataMap){
		if(dataMap.hasOwnProperty(k)){
			dataMap[k].action = 'n';
		}
	}

	//Get summarized history log
	var historyLog = getSummarizedPtLog(containerId, clearLog);

	/*
	 * Loop through history log and check for pk(s) change. The update action will be changed to
	 * - The pre-update row is deleted
	 * - The post-update row is inserted
	 * History log will now contain keys:
	 * - "pk" is object containing pks
	 */
	var historyLength = historyLog.length; //Save length so that the loop does not iterate pass initial log
	for(var i=0; i<historyLength; i++){
		var log = historyLog[i];
		if(log.action === 'u'){
			//Check for pk change first
			var pkChanged = false;
			var rowData = prechangeData[log.index];
			var rowDisplayData = dataMap[log.rowId];
			var dataPks = {};
			var displayPks = {};
			for(var j=0; j<pks.length; j++){
				var pk = pks[j];
				if(rowData[pk] != rowDisplayData[pk]){ //Use == as number input becomes string ##pp
					pkChanged = true;
				}
				dataPks[pk] = rowData[pk];
				displayPks[pk] = rowDisplayData[pk];
    		}
			if(pkChanged){
				//Pre-update becomes delete action. Switch update log to delete
				log.action = 'd';
				log.pk = dataPks;

				//Post-update becomes insert action. Add insert log
				historyLog.push({ rowId:log.rowId, index:log.index, action:'i', pk:displayPks });
			}else{
				//If pk(s) is not changed, takes data from display
				log.pk = displayPks;
			}
		}else{
			//If insert action -> take pks from display
			//If delete action -> take pks from data
			var rowData = log.action === 'i' ? rptGetDataFromDisplay(containerId, log.rowId) : prechangeData[log.index];
			log.pk = {};
			for(var j=0; j<pks.length; j++){
				var pk = pks[j];
				log.pk[pk] = rowData[pk];
    		}
		}
	}

	/*
	 * At this state, all "insert" and "update" are guaranteed to be written to db.
	 * Any "delete" with repeated pks as "insert"/"update" should be removed.
	 * "insert"/"update" should be changed to "update" with rowId changed to the deleted one.
	 */
	//Sort "delete" to the end
	historyLog.sort(function(a,b){
		return a.action === 'd' ? 1 : (b.action === 'd' ? -1 : 0);
	});
	//Loop through history, inspect "delete"
	var deleteStartIndex = -1;
	for(var i=0; i<historyLog.length; i++){
		if(historyLog[i].action === 'd' && deleteStartIndex === -1){
			deleteStartIndex = i;
		}
		//"delete" log
		if(i >= deleteStartIndex){
			//Check for pks match between "delete" and "insert"/"update"
			for(var j=0; j<deleteStartIndex; j++){
				var pkMatched = true;
				for(var pkIdx = 0; pkIdx < pks.length; pkIdx++){
					var pkName = pks[pkIdx];
					if(historyLog[i].pk[pkName] != historyLog[j].pk[pkName]){ //Use == as number input becomes string ##pp
						pkMatched = false;
						break;
					}
				}
				if(!pkMatched) continue;
				//Match found
				historyLog[j].action = 'u';
				historyLog[i] = null;
				break;
			}
		}
	}

	//Gather data for each changed row
	var result = [];

	for(var i = 0; i < historyLog.length; i++ ){
		var log = historyLog[i];
		if(!log) continue;
		if(log.action === 'd'){
			var obj = { action: 'd' };
			for(var pkName in log.pk){
				obj[pkName] = log.pk[pkName];
			}
			result.push(obj);
		}else{
			//Take data from map and add it to result
			var obj = dataMap[log.rowId];
			delete dataMap[log.rowId];

			obj.action = log.action;
			if (addRowId)
				obj.rowId = log.rowId;

			result.push(obj);
		}
	}

	//Take remaining data from dataMap
	for(var k in dataMap){
		if(dataMap.hasOwnProperty(k)){
			result.push(dataMap[k]);
		}
	}

	return result;
}

/**
 * Returns an array of data of all rows that are either inserted or changed based on history log
 * (the list used by rptGetInsertsAndChanges).
 * Each row of data will have additional key "action" with 3 possible values: i/u/d (insert/update/delete)
 * For a row with action = d, only fields with class pk will be included.
 * This function will try to summarize change if multiple actions occurs on the same data row.
 * The function also checks for actual action on existing data if it is actually update or delete.
 * If action is true the existing history log is cleaned.
 * IMPORTANT:
 * - Before calling this function, all current pk fields must be unique.
 * - Data must be added with rptAddDataWithCache
 *
 * @param {String} containerId The html id of the table area.
 * @param {Boolean} action True or false depending on whether the list shall be cleared or not.
 * @returns {Array} An array of rows data extracted from the table, each row have "action" key included.
 */
function getChangedData(containerId, action, rowId){
	var settings = rptGetTableSettings(containerId);
	var prefixLength = settings.configuration.prefixLength;
	var prefix = containerId.substring(0,prefixLength);
	var prechangeData = settings.originalData;

	//Extract pk field from template row
	var pks = []; //A list of pk field name (prefix removed)
	$("#"+settings.rowId).find(".pk").each(function(){
		var str = this.id.substring(prefixLength);
		pks.push(str);
		});

	//Get summarized history log
	var historyLog = getSummarizedPtLog(containerId, action);

	/*
	 * Loop through history log and check for pk(s) change. The update action will be changed to
	 * - The pre-update row is deleted
	 * - The post-update row is inserted
	 * History log will now contain keys:
	 * - "pk" is object containing pks
	 */
	var historyLength = historyLog.length; //Save length so that the loop does not iterate pass initial log
	for(var i=0; i<historyLength; i++){
		var log = historyLog[i];
		if(log.action === 'u'){
			//Check for pk change first
			var pkChanged = false;
			var rowData = prechangeData[log.index];
			var rowDisplayData = rptGetDataFromDisplay(containerId, log.rowId);
			var dataPks = {};
			var displayPks = {};
			for(var j=0; j<pks.length; j++){
				var pk = pks[j];
				if(rowData[pk] != rowDisplayData[pk]){ //Use == as number input becomes string ##pp
					pkChanged = true;
				}
				dataPks[pk] = rowData[pk];
				displayPks[pk] = rowDisplayData[pk];
    		}
			if(pkChanged){
				//Pre-update becomes delete action. Switch update log to delete
				log.action = 'd';
				log.pk = dataPks;

				//Post-update becomes insert action. Add insert log
				historyLog.push({ rowId:log.rowId, index:log.index, action:'i', pk:displayPks });
			}else{
				//If pk(s) is not changed, takes data from display
				log.pk = displayPks;
			}
		}else{
			//If insert action -> take pks from display
			//If delete action -> take pks from data
			var rowData = log.action === 'i' ? rptGetDataFromDisplay(containerId, log.rowId) : prechangeData[log.index];
			log.pk = {};
			for(var j=0; j<pks.length; j++){
				var pk = pks[j];
				log.pk[pk] = rowData[pk];
    		}
		}
	}

	/*
	 * At this state, all "insert" and "update" are guaranteed to be written to db.
	 * Any "delete" with repeated pks as "insert"/"update" should be removed.
	 * "insert"/"update" should be changed to "update" with rowId changed to the deleted one.
	 */
	//Sort "delete" to the end
	historyLog.sort(function(a,b){
		return a.action === 'd' ? 1 : (b.action === 'd' ? -1 : 0);
	});
	//Loop through history, inspect "delete"
	var deleteStartIndex = -1;
	for(var i=0; i<historyLog.length; i++){
		if(historyLog[i].action === 'd' && deleteStartIndex === -1){
			deleteStartIndex = i;
		}
		//"delete" log
		if(i >= deleteStartIndex){
			//Check for pks match between "delete" and "insert"/"update"
			for(var j=0; j<deleteStartIndex; j++){
				var pkMatched = true;
				for(var pkIdx = 0; pkIdx < pks.length; pkIdx++){
					var pkName = pks[pkIdx];
					if(historyLog[i].pk[pkName] != historyLog[j].pk[pkName]){ //Use == as number input becomes string ##pp
						pkMatched = false;
						break;
					}
				}
				if(!pkMatched) continue;
				//Match found
				historyLog[j].action = 'u';
				historyLog[i] = null;
				break;
			}
		}
	}

	//Gather data for each changed row
	var result = [];
	for(var i = 0; i < historyLog.length; i++ ){
		var log = historyLog[i];
		if(!log) continue;
		if(log.action === 'd'){
			var obj = { action: 'd' };
			for(var pkName in log.pk){
				obj[pkName] = log.pk[pkName];
			}
			result.push(obj);
		}else{
			var obj = rptGetDataFromDisplay(containerId, log.rowId);
			obj.action = log.action;

			if (rowId)
				obj.rowId = log.rowId;

			result.push(obj);
		}
	}

	return result;
}

/**
 * Try to delete all rows marked for deletion from specified table. A row is marked using
 * an element with id specified in checkboxId. The element must return value as either 'Y' or 'N'.
 * Any row with the element with value 'Y' will be deleted. Delete confirmation and warning is optional
 * @param tableId Powertable to delete rows
 * @param checkboxId Row element ID to check for deletion. The element must return value 'Y' or 'N'
 * @param showDialog If true, dialog is shown if no row to delete and when confirmation needed
 * @param onDeleteCallback Callback function with (rowId) as parameter. Called before the row is deleted
 */
function rptTryToDeleteSelectedRows(tableId, checkboxIdWithoutPrefix, showDialog, onDeleteCallback){
	var allData = rptGetDataFromDisplayAll(tableId);
	var deleting = [];
	allData.forEach(function(row){
		if(row[checkboxIdWithoutPrefix] === 'Y'){
			deleting.push(row.rowId);
		}
	});
	if(deleting.length == 0){
		if(showDialog){
			dialogGeneric("Warning", "At least one delete checkbox should be checked", "Ok");
		}
	}else{
		for(var i=0; i<deleting.length; i++){
			if(onDeleteCallback){
				onDeleteCallback(deleting[i]);
			}
			rptRemoveRow(tableId, deleting[i]);
		}
	}
}

function _rptHandleRowChildChange(settings, rowid){
    var found=false; //whether we found the row already
    //array.find is not supported in IE11 and earlier
    for (var i=0;i<settings.insertsAndChanges.length;i++){
        if (settings.insertsAndChanges[i]==('u-'+ rowid)){
            found=true;
            break;
        }
        else if (settings.insertsAndChanges[i]==('i-'+ rowid)){
            found=true;
            break;
        }
    }
    if (!found){
        settings.insertsAndChanges.push('u-'+ rowid);
    }
}

function esnParseNumber(data, defaultValue){
	data = parseInt(data, 10);
	if(isNaN(data)){
		data = defaultValue;
	}
	return data;
}

function rutSetDialogTitle(dialogId, title){
	$("[aria-describedBy='" + dialogId + "']").find("span.ui-dialog-title").text(title);
}

//====================================================

function openEdiHistory(bookingNo){
	var baseUrl = ediBaseUrl + "fromModule=BOOKING&servaction=find&txt_booking_no=" + bookingNo;
	var w = window.open(baseUrl, 'PopupWindow', 'width=650,height=650,resizable=no,scrollbars=yes,toolbar=no,,left=100,top=100');
	w.focus();
}

//overide fucntion ascan
//change get date to number format
function rutGetElementValue(elementId){
	var element=null;
    var value=null;
    // #02 begin
    var foundRadioButton=false; //this indicates, whether we found at least one radio button, if the id contains an #
    if ((typeof elementId)=="string"){ //the elementId is an id
        element=document.getElementById(elementId);
        if (element==null) {
            //It could still be a radio button with an id like d0-emptyFull#E
            //JQuery has issues with "." and "#" in id's, so we need to work around
            var str=elementId.split('#')[0].split('.')[0];
            $("input").each(function(){
                if (this.type!="radio") {return true;} //not a radio button, continue with next
                if (elementId!=this.id.split('#')[0]) {return true;} //does not belong to this attribute
                foundRadioButton=true; //we found a radio button, so this will not create an error
                if (this.checked) {
                    value=this.value;
                    return false;
                }
            });
            if (foundRadioButton==true) {
                return value;
            }
            else {
                alert('rutGetElementValue: elementId "'+elementId+'" does not exist/does not represents a group of radio buttons');
                return null;
            }
        } //endif element==null
    }
    // #02 end
    else { //elementId is an element
        element=elementId;
    } //end if else elementId==string
    if (element==null){
        tag=null;
    }
    else {
        tag=element.nodeName||null;
    }
    if (tag==null){
        alert('rutSetElementValue: element '+JSON.stringify(element)+' of elementId:'+elementId+' is not an html element');
        return null;
    }
    if (tag=='INPUT'){
        if (    (element.type=="text")        ||
                (element.type=="number")      ||
                (element.type=="usernamer")   ||
                (element.type=="password")    ||
                //(element.type=="date")        ||
                //(element.type=="tele")        ||  //###12
                (element.type=="tel")         ||  //###12
                (element.type=="email")       ||  //###12
                (element.type=="url")         //||
                //(element.type=="time")
            )                          {
        	value = element.value;
        }
        else if (element.type=="date"){
        	value =  convertDateFormat(element.value);
        }
        else if (element.type=="time"){
        	value = element.value.replace(':','');
        }
        else if (element.type=="radio"){
            value=(element.checked) ? element.value : '';
        }
        else if (element.type=="checkbox"){
            value=(element.checked) ? 'Y' : '';
        }
    }
    else if (tag=="TEXTAREA"){
        value=element.value;
    }
    else if (tag=="SELECT"){
		if (element.options && element.options[element.selectedIndex]) {
			value=element.options[element.selectedIndex].value;
		} else {
			value = '';
		}
    } else {
        value=element.textContent; //just strip to bare text
    }
    return value;
}//end function rutGetElementValue

/**
 * Toggle standard checkbox (checkbox input wrapped in div and has label as its preceding sibling)
 * to view mode, which is <rcl:select>
 */
function toggleCheckboxToViewMode(checkboxId, wrapperClasses){
	var checkbox = $("#" + checkboxId);
	var wrapper = checkbox.parent();
	if(wrapperClasses){
		wrapper.attr("class", wrapperClasses);
	}
	var label = checkbox.prev();
	var customTagClass = "";
	var checkboxClass = checkbox.attr("class");
	if(checkboxClass){
		var ctClasses = ["dtlField", "searchField", "tblField"];
		for(var i=0; i<ctClasses.length; i++){
			if(checkboxClass.indexOf(ctClasses[i]) !== -1){
				customTagClass = ctClasses[i];

			}
		}
	}

	var html = '<label class="rcl-standard-font" for="' + checkboxId + '">' + label.html() + "</label>\n" +
				'<select id="' + checkboxId + '" class="rcl-standard-form-control rcl-standard-component ' +
						customTagClass + '" data-rdo data-ct="bt-select tb-YesNo">\n' +
				'	<option value></option>\n' +
				'	<option value="Y">Yes</option>\n' +
				'	<option value="N">No</option>\n' +
				'</select>\n';

	wrapper.html(html);
}

//====================================================

