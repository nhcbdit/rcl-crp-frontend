/*-----------------------------------------------------------------------------------------------------------
 quotation.js
 -------------------------------------------------------------------------------------------------------------
 Copyright RCL Public Co., Ltd. 2007
 -------------------------------------------------------------------------------------------------------------
 Author Sarawut Anuckwattana 02/07/2019
 - Change Log ------------------------------------------------------------------------------------------------
 ## DD/MM/YY       -User-           -TaskRef-      -Short Description
 01	02/07/19		Sarawut A.						Add method saveAs
 02 18/09/19		nawrat1							disabled or readonly icon must be disabled
 03 20/09/19		nawrat1							add transform date format for quotation search data
 -----------------------------------------------------------------------------------------------------------*/
//var baseurl = "http://localhost:8080/";
//var baseurl = "http://10.2.6.61:8080/QTNWSWebApp/rclws/";
//var baseurl = "http://" + location.hostname + ":8080/";	//Edit by Nitikorn 13/09/2019 - fix cross domain call.
// var baseurl = "http://marlin-ce.rclgroup.com:8080/";
//var baseurl2 = "http://" + location.hostname + ":8080/";	//Edit by Nitikorn 13/09/2019 - fix cross domain call.
// var baseurl2 = "http://marlin-ce.rclgroup.com:8080/";

var baseurl = "http://" + location.hostname + (window.location.port != "" ? ":" + window.location.port : "") + "/";		//Edit by Nitikorn 13/09/2019 - fix cross domain call.
//var baseurl2 = "http://marlin-ce.rclgroup.com" + (window.location.port != "" ? ":"+window.location.port : "") +"/";	//Edit by Nitikorn 13/09/2019 - fix cross domain call.
//var baseurl2 = "http://" + location.hostname + (window.location.port != "" ? ":"+window.location.port : "") +"/";

var baseurl2 = (location.hostname.indexOf('rclgroup') >= 0) ?
	('http://' + location.hostname + (window.location.port != '' ? ':' + window.location.port : '') + '/') :
	'http://marlin-ce.rclgroup.com' + (window.location.port != '' ? ':' + window.location.port : '') + '/';

var qtn = "QTNWSWebApp/rclws/";  
var pcl = "BKGWSWebApp/rclws/";
var crp = "CRPWebService/rclws/crp";
//var upfile = "MSTWSWebApp/rclws/";
var upfile = "QTNWSWebApp/rclwsupload/";
// ##DEV Server
var IP_ADDRESS = "http://10.0.3.47";
var PATH_UPLOADFILE = "";



var equipmentAvvalidateValuequoV2lBaseUrl = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/EMSMoveCorrectionServ?servaction=EquipmentAvailabilityInquiryFind";
var productCatalogueBaseUrl = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/MiProdCatalogServ?";
var ediBaseUrl = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/EDITransactionEnquiryServ?";
var sealinerUrl = "http://zebra-cl.rclgroup.com/SealinerRCL/rest/webServiceSealiner/";

function getResultAjaxUploadFile(name, data, autoHandleResult) {
	if (autoHandleResult === undefined) {
		autoHandleResult = true;
	}

	var url = "";
	var Iscase = "";
	var Param = "";
	switch (name) {
		case "upload":
			url = upfile + "Quotationupload/upload";
			Iscase = "upload";
			break;
		case "delete":
			url = upfile + "Quotationupload/delete";
			Iscase = "1";
			break;
		case "WS_QTN_GET_PATHATTACH":
			url = qtn + "Quotation/WS_QTN_GET_PATHATTACH";
			break;
		case "WS_QTN_GET_ATTACHMENT":
			url = qtn + "Quotation/WS_QTN_GET_ATTACHMENT";
			break;
		case "WS_QTN_GET_CONTENTYPE":
			url = qtn + "Quotation/WS_QTN_GET_CONTENTYPE";
			break;
		case "WS_QTN_GET_DUPFILMNAME":
			url = qtn + "Quotation/WS_QTN_GET_DUPFILMNAME";
			break;
		case "WS_QTN_SAVE_ATTACHMENT":
			url = qtn + "Quotation/WS_QTN_SAVE_ATTACHMENT";
			break;
		case "WS_QTN_DELETE_SESSION":
			url = upfile + "Quotationupload/WS_QTN_DELETE_SESSION";
			break;
		default:
			url = "";
			break;
	}
	
	console.log(url)

	$("body").append('<div class="loading"></div>');

	if (!autoHandleResult) {
		return AjaxPost(url, data, Iscase);
	} else {
		return AjaxPost(url, data, Iscase).done(handleUploadAjaxResponse).fail(handleAjaxError).always(
			function () {
				$("body").find('.loading').remove();
			});
	}
}

function getResultAjax(name, data, autoHandleResult) {
	if (autoHandleResult === undefined) {
		autoHandleResult = true;
	}
	
	var url = "";
	var Param = "";
	switch (name) {
		case "WS_CRP_SEARCH":
			url = crp + "/voyage-list";
		break;
		
		case "WS_QTN_SEARCH":
			url = qtn + "Quotation/WS_QTN_SEARCH";
			break;
		case "WS_QTN_SAVE":
			url = qtn + "Quotation/WS_QTN_SAVE";
			break;
		case "WS_GET_QTN_DETAIL":
			url = qtn + "Quotation/WS_GET_QTN_DETAIL";
			break;
		case "WS_BKG_PC":
			url = pcl + "Booking/WS_BKG_PC";
			Param = "1";
			break;
		case "WS_BKG_PROCESS_ADDED_ROUTE":
			url = pcl + "Booking/WS_BKG_PROCESS_ADDED_ROUTE";
			Param = "1";
			break;
		case "WS_QTN_GET_DND":
			url = qtn + "Quotation/WS_QTN_GET_DND";
			break;
		case "WS_QTN_GET_RATE":
			url = qtn + "Quotation/WS_QTN_GET_RATE";
			break;
		case "WS_QTN_ACTIVITY_DTL":
			url = qtn + "Quotation/WS_QTN_ACTIVITY_DTL";
			break;
		case "WS_QTN_COPY_QUOTATION":
			url = qtn + "Quotation/WS_QTN_COPY_QUOTATION";
			break;
		case "WS_QTN_CREATE_INTERIM":
			url = qtn + "Quotation/WS_QTN_CREATE_INTERIM";
			break;
		case "WS_BKG_GET_PRINT_CLAUSE_DETAIL":
			url = pcl + "Booking/WS_BKG_GET_PRINT_CLAUSE_DETAIL";
			Param = "1";
			break;
		case "WS_QTN_COPY_OOG_BBK":
			url = qtn + "Quotation/WS_QTN_COPY_OOG_BBK";
			break;
		case "BookingService/WS_KEEP_ALIVE":
			url = pcl + "BookingService/WS_KEEP_ALIVE";
			Param = "1";
			break;
		case "WS_QTN_GET_PREFER_DEPOT":
			url = qtn + "Quotation/WS_QTN_GET_PREFER_DEPOT";
			break;
		case "WS_QTN_GET_APPROVAL_OPTION":
			url = qtn + "Quotation/WS_QTN_GET_APPROVAL_OPTION";
			break;
		case "WS_QTN_APPROVAL":
			url = qtn + "Quotation/WS_QTN_APPROVAL";
			break;
		case "WS_QTN_GET_FSC_PORT":
			url = qtn + "Quotation/WS_QTN_GET_FSC_PORT";
			break;
		case "WS_GET_REEFER_TYPE":
			url = qtn + "Quotation/WS_GET_REEFER_TYPE";
			break;
		case "WS_NEW_PCL":
			url = "PCLWS/api/schedule/routing";
			//Param = "1";
			break;
		case "WS_GET_DEFAULT_MOT":
			url = qtn + "Quotation/WS_GET_DEFAULT_MOT";
			break;
		default:
			url = "";
			break;
	}

	$("body").append('<div class="loading"></div>');

	if (!autoHandleResult) {
		return AjaxPost(url, data, Param);
	} else {
		return AjaxPost(url, data, Param).done(handleAjaxResponse).fail(handleAjaxError).always(
					function () {
						$("body").find('.loading').remove();
					});
		
	}


}

function handleAjaxError(error){
	console.log()
	if(error.statusText =="Internal Server Error"){
		dialogFadeout("Internal Server Error");
	}
	else
	{
		//$(".nav-tabs a[href='#p0-" + data.tab + "']").tab("show");
		if(error.responseText){
			dialogFadeout(error.responseText);
		}
	}
    return { $_success: false };
}

function handleAjaxPclResponse(response){
	if(!response){
		response = { $_success: false };
	}
	else if (!response.Success){
		response.$_success = false;
		if(response.Success == false){
			if(response.Type == "expire"){
				location.href = expiryUrl;
			}else if(response.Content){
				//Parse oracle error
				if(response.Content.search(/^ORA\-\d{1,5}/) >= 0){
					//response.Content = response.Content.replace(/^(ORA-\d{1,5}\:)(.+)(ORA-\d{1,5}\:.*)*/, "$2").replace(/~t/g, " ").trim();
					response.Content = response.Content.replace(/ORA-\d{1,5}\:/g, "~~~").split("~~~")[1]
						.replace(/~t/g, " ").trim();
				}else if(response.Content.search(/ORA\-\d{0,5}\:/) > -1){
					//cut response text to ora-xxxxx : *[message] ora-xxxxx : at [line] 
					response.Content = Content = response.Content.substring(response.Content.search(/ORA\-\d{0,5}\:/));
					//trim response text to ora-xxxxx : *[message]
					response.Content = response.Content.substring( 0,response.Content.search(/ORA\-\d{0,5}\:\s*\at/));
					//trim ora-xxxxx : response text  
					response.Content = response.Content.replace(/ORA\-\d{0,5}\:/, '');
					
					//remove possible leftover ~t
					response.Content = response.Content.replace(/~tNULL~t/i, "").trim();
					response.Content = response.Content.replace(/~t/i, "").trim();
				}
				dialogFadeout(response.Content);
			}
			else{
				dialogFadeout("Success = false");
			}
		}
	}else{
		response.$_success = true;
		//if ( (typeof response.Content === 'string' || response.Content instanceof String) && response.Content ){
		//	dialogFadeout("Content\n"+"=================\n"+response.Content);
		//}
	}
    return response;
}

function AjaxPost(url, data, Param) {
	var setbaseurl = "";
	if (Param == "1") {
		setbaseurl = baseurl2;
	} else {
		setbaseurl = baseurl;
	}
	if (Param == 'upload') {
		return $.ajax({
			url: baseurl2 + url,
			type: 'POST',
			data: data,
			async: false,
			cache: false,
			contentType: false,
			enctype: 'multipart/form-data',
			processData: false

		});
	} else {
		return $.ajax({
			url: setbaseurl + url,
			method: "POST",
			cache: false,
			contentType: "application/json",
			data: JSON.stringify(data),
			dataType: "json"
		});

	}

}


function handleAjaxResponse(response) {
	if(response.resultCode == '401'){
		dialogGeneric("Warning", response.resultContent, "Ok");
	}
}

function handleUploadAjaxResponse(response) {
	if (!response) {
		response = { Success: false };
	} else {
		if(response.resultStatus == '200' || response.resultCode == '200' || response.responseCode == '200'){
             response.Success = true;
		}else{
			response.Success = false;
		}

	}

	//console.log("handleUploadAjaxResponse :: "+JSON.stringify(response));
	return response;
}




function dialogFadeout_N(msg, title, errorCode) {
	return dialogGeneric(title ? title : "Warning", msg, "Ok", null, true, errorCode);
}

function setCustomTagProperty(id, properties) {
	setCustomTagPropertyParam(id, properties.dis, properties.rdo, properties.req, properties.iconClick);
}

function setCustomTagPropertyParam(id, disabled, readonly, required, keepIconClick) {
	var setDis = typeof disabled === 'boolean';
	var setRdo = typeof readonly === 'boolean';
	var control = $("#" + id);
	if (keepIconClick === undefined) {
		keepIconClick = false;
	}

	var isSelect = control.prop("tagName") === "SELECT";
	if (isSelect) {
		if (setDis) {
			control.attr('data-dis', disabled ? '' : null);
		}
		if (setRdo) {
			control.attr('data-rdo', readonly ? '' : null);
		}
	} else {
		if (setDis) {
			control.prop('disabled', disabled);
		}
		if (setRdo) {
			control.prop('readonly', readonly);
		}
	}
	if (typeof required === 'boolean') {
		control.prop('required', required);
	}

	var iconControl = control.parent().next();
	var hasIcon = iconControl.prop("tagName") === "I";

	// original code
	// if(hasIcon && !keepIconClick){
	// 	//Try to cache onclick to data-onclick first if not yet
	// 	if(iconControl.attr('data-onclick') === undefined){
	// 		var onclickContent = iconControl.attr('onclick');
	// 		if(onclickContent === undefined){
	// 			onclickContent = "";
	// 		}
	// 		iconControl.attr('data-onclick', onclickContent);
	// 	}

	// 	//Check if the control is becoming interactable or not
	// 	var interactable = true;
	// 	if(isSelect){
	// 		interactable = control.attr('data-dis') === undefined && control.attr('data-rdo') === undefined;
	// 	}else{
	// 		interactable = !control.prop('disabled') && !control.prop('readonly');
	// 	}

	// 	//Adjust icon accordingly
	// 	if(interactable){
	// 		//If possible, move data-onclick to onclick
	// 		var onclickContent = iconControl.attr('data-onclick');
	// 		if(onclickContent){
	// 			iconControl.attr('onclick', onclickContent);
	// 		}else{
	// 			iconControl.attr('onclick', null);
	// 		}
	// 	}else{
	// 		//clear onclick
	// 		iconControl.attr('onclick', null);
	// 	}
	// 	iconControl.prop("disabled", true);
	// }

	// custom code
	function isDefined(fn) {
		return fn && fn.length && fn !== 'null';
	}
	if (hasIcon) {
		var ctrlDisabled = control.prop('disabled');
		var ctrlReadonly = control.prop('readonly');
		var attrDataOnclick = iconControl.attr('data-onclick');
		var attrOnclick = iconControl.attr('onclick');
		if ((ctrlDisabled || ctrlReadonly) && isDefined(attrOnclick)) {
			iconControl.attr('data-onclick', attrOnclick);
			iconControl.attr('onclick', 'null');
		} else if (!ctrlDisabled && !ctrlReadonly && !isDefined(attrOnclick)) {
			iconControl.attr('onclick', attrDataOnclick);
			iconControl.attr('data-onclick', 'null');
		}
	}
}


function validateValuequo() {
	var elementRequired = $(".searchArea ,.dtlArea, .tblArea")
	var errorMsg = "";
	var error = "";
	var result = true;
	for (i = 0; i < elementRequired.length; i++) {
		var chkError = rutCheckAreaquo(elementRequired[i].id);

		if (chkError.length > 0) {

			var firsterrorElement = chkError[0][0];

			//find lebel error
			error = $('#' + chkError[0][0]).prev().html();
			if (!error)
				error = $('#' + firsterrorElement).parent().parent().parent()[0].firstElementChild.innerText;
			if (!error.trim())
				error = $('#' + firsterrorElement).prop('name');

			//active tab error
			var tabs = $('.tab-pane');
			for (j = 0; j < tabs.length; j++) {
				var tabError = $('#' + tabs[j].id).find('#' + firsterrorElement);
				if (tabError.length > 0)
					$(".rcl-standard-navigation-bar-tab")[j].click();
			}

			//focus element error
			$('#' + firsterrorElement).focus();

			result = false;
			errorMsg = error;
			break;
		}

	}

	dialogGeneric("Warning", errorMsg + ' is required.', "Ok");
	return result
}

function validateDataTab() {
	var result = true;
	if (!validateDataTaball()) {
		dialogGeneric("Warning", "*Incomplete information found , please select & save it to proceed", "Ok");
		result = false;
	}
	return result;
}


function validateDataTaball() {
	return (isObjectEmpty(rptGetDataFromDisplayAll("t31containerArea"))
		&& isObjectEmpty(rptGetDataFromDisplayAll("t40rountingDetailArea"))
		&& isObjectEmpty(rptGetDataFromDisplayAll("t52ChargesDetailsArea"))
		&& checkEmptyDND());
}

function checkEmptyDND() {
	return (isObjectEmpty(rptGetDataFromDisplayAll("t66AdditionalFreeDays_D"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t67AdditionalFreeDays_E"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t68AdditionalFreeDays_I"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t69AdditionalFreeDays_M"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t81Charge_D"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t82Charge_E"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t83Charge_I"))
		|| isObjectEmpty(rptGetDataFromDisplayAll("t84Charge_M"))
	)
}



function isEmptyObject(obj) {
	return JSON.stringify(obj) === '{}';
}


function isObjectEmpty(obj) {
	return obj.length >= 1

}


function validateValuequoV2(area, isOnlyVisible, reportAllError) {
	console.log("isOnlyVisible :: " + isOnlyVisible)
	if (area) {
		var elementRequired = [];
		if (Array.isArray(area)) {
			$.each(area, function (index, value) {
				$.each($("#" + value), function (index2, value2) {
					elementRequired.push(value2);
				});
			});
		}
		else {
			elementRequired.push($("#" + area));
		}
	}
	else {
		var elementRequired = $(".searchArea ,.dtlArea, .tblArea");
	}

	var errorMsg = "";
	var error = "";
	var result = true;
	for (var i = 0; i < elementRequired.length; i++) {
		var chkError = rutCheckArea(elementRequired[i].id, isOnlyVisible);
		if (chkError.length > 0) {
			var firsterrorElement = chkError[0][0];

			//find lebel error
			error = $('#' + chkError[0][0]).prev().html();
			if (!error) {
				error = $('#' + firsterrorElement).prop('name');
			}
			if (!error) {
				error = $('#' + firsterrorElement).parent().parent().parent()[0].firstElementChild.innerText;
			}


			//active tab error
			var tabs = $('.tab-pane');
			for (var j = 0; j < tabs.length; j++) {
				var tabError = $('#' + tabs[j].id).find('#' + firsterrorElement);
				if (tabError.length > 0) {
					activeTabWithOutChangeTrigger($(".rcl-standard-navigation-bar-tab")[j].innerHTML.replace(/[&!/.*+?^${}()|[\]\\]/g, ''));
					//$(".rcl-standard-navigation-bar-tab")[j].click();
				}
			}

			//focus element error
			$('#' + firsterrorElement).focus();

			result = false;
			if (!reportAllError) {
				errorMsg = error;
				break;
			} else {
				errorMsg += (errorMsg.length === 0 ? "" : ", ") + error;
			}
		}
	}

	if (!result) {
		dialogGenericv2("Warning", errorMsg + (reportAllError ? ' required.' : ' is required.'), "Ok");
	}

	console.log("result :: " + result);

	return result
}


function activeTabWithOutChangeTrigger(tab) {
	if (!tab)
		return;
	var tabs = $(".rcl-standard-navigation-bar-tab");

	var tabpane = $(".tab-pane");
	tab = tab.replace(/\s+/g, '');

	for (var i = 0; i < tabs.length; i++) {
		var tabName = tabs[i].innerHTML.replace(/\s+/g, '');
		var tabName = tabs[i].innerHTML.classList;


		if (tabName == tab) {

			tabs[i].classList.add("active");
		}
		else {
			tabs[i].classList.remove("active");
		}
	}

	for (var i = 0; i < tabpane.length; i++) {
		if (tabpane[i].id.split('-')[1] == tab) {
			tabpane[i].classList.add("active");
		}
		else {
			tabpane[i].classList.remove("active");
		}
	}
}

function dialogGenericv2(title, msg, yesLabel, noLabel, visibleClose, errorCode) {
	if (visibleClose == undefined) {
		visibleClose = true;
	}

	var deferred = new $.Deferred();

	rutOpenMessageBox(title, msg, errorCode,
		noLabel ? function () {
			deferred.resolve(false);
		} : null,
		yesLabel ? function () {
			deferred.resolve(true);
		} : null,
		noLabel, yesLabel, visibleClose);

	return deferred.promise();

}

function dialogGenericv3(nametab, title, msg, yesLabel, noLabel, visibleClose, errorCode) {

	activeTabWithOutChangeTrigger(nametab);

	if (visibleClose == undefined) {
		visibleClose = true;
	}

	var deferred = new $.Deferred();

	rutOpenMessageBox(title, msg, errorCode,
		noLabel ? function () {
			deferred.resolve(false);
		} : null,
		yesLabel ? function () {
			deferred.resolve(true);
		} : null,
		noLabel, yesLabel, visibleClose);

	return deferred.promise();

}


function rutCheckAreaquo(containerId, onlyCheckVisible) { //##56b
	var errors = [];
	//###30a BEGIN
	var container = $("#" + containerId);
	var checkArray = container.attr("class").indexOf("tblArea") > -1;
	if (checkArray) {
		container = container.children(":not(.tblRow)").add(container.children(".tblRow:not(:last)"));
	}

	//container = container.find("input"); //###36
	//container = container.find("input, select"); //###36 ##56b
	//##56b BEGIN
	if (onlyCheckVisible) {
		container = container.find("input:visible, select:visible");
	} else {
		container = container.find("input, select");
	}
	//##56b END
	container.each(function () {
    	/*if(checkArray){
    		if(this.id.indexOf("-") > -1){
    			if (this.required){
            		if((this.value==null) || (this.value.trim()=="")){
            			 errors.push( [this.id , 'rut001-001' , 'missing value'] );
            		}

            	}
    		}
    	}else{*/
		//###30a END
		//$("#"+containerId).find("input").each(function(){ //###30a
		if ((this.required) &&
			//((this.value==null) || (this.value=="")) //###36
			((this.value == null) || (this.value.trim() == "")) //###36
		) {
			errors.push([this.id, 'rut001-001', 'missing value']);
		}
		// }

	});//end input tag
	return errors;
} //end function rutCheckArea


function rutOpenLookupTableWithReturnAction(table, returnCols, returnIds, selectCols, selectValues, selectOperators, displayTitle) {
	//1 call web service to get the data with table, selectedCols, selectedVals
	//2 when data array is returned analyse first record in array to build the html template
	//3 assign data to settings
	//4 Display dialog box
	//5 Close dialog box, assign return values
	var title = (displayTitle) ? displayTitle : rutToTitleCase(table.substring(4) + ' Lookup'); //remove the VRL_ from the table name
	//##60 BEGIN
	//var wsInput = _rutPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators);
	var wsInput = _rutPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators, returnCols);
	returnCols = returnCols.replace(/\*\w*/g, "");
	//##60 END
	rutGetLookupData(wsInput, showLookupDialog); //###42
	//showLookupDialog(getTestData()); //only for dry test without server
	return; //###42

	//Inner Function
	function showLookupDialog(lookupResult) {
		//###55 BEGIN
		var data = lookupResult.data;
		var metaData = lookupResult.metadata;
		if (data.length <= 0) {
			//No data found display error message
			let errorMessage = (wsInput.select.length == 0) ? 'No data found' : 'No data found for ';
			for (var i = 0; i < wsInput.select.length; i++) {
				errorMessage += wsInput.select[i].column + wsInput.select[i].operator + wsInput.select[i].value + ' ';
			}
			rutOpenMessageBox('Error on lookup of ' + table.substring(4), errorMessage, 'rut003-001', null, '');
			return;
		}
		var element = document.getElementById('t99Lookup-dlg');
		if (element != null) { //another lookup table was not clossed properly but through x button
			element.parentNode.removeChild(element); //element.remove does not work in IE
		}
		var htmlString = '';
		var cr = '\r\n';
		var tdStyleString = '';
		var tdStyleNumber = '';
		var tdHideClass = "d-none";
		var tdClass = '';

		var minDialogWidth = 550; //This is wide enough for paging/dialog buttons //##56e

		var colWidths = []; //##56f
		var selectColSize = 50; //##56f
		var colSizes = [];
		//var totalColSize=50; //we have already the select button //##56f
		var totalColSize = selectColSize; //##56f
		var numCols = 0;
		var colSize = 0;
		for (var i = 0; i < metaData.length; i++) { //Here we compute the size of the columns
			colSize = Math.min(Math.max(metaData[i].precision, metaData[i].columnName.length) * 9, 200); //1 char ~ 9px
			colSize = Math.max(colSize, 72); //Need at least space for sort buttons //##56f
			colWidths.push(colSize); //##56f
			colSizes.push(colSize + 'px;');
			if (metaData[i].columnName.substring(0, 5).toUpperCase() != 'HIDE_') {
				numCols++;
				totalColSize += colSize;
			}
		}
		//var totalAreaWidth=((1+numCols)*8)+totalColSize+1; //each column has 2 x 3px padding and 1px border, 17px for scrollbar //##56f
		var totalAreaWidth = ((1 + numCols) * 8) + totalColSize + 17;

		//##56f BEGIN
		if (totalAreaWidth < minDialogWidth) {
			//Redistribute column width using width ratio
			var oldTotalColSize = totalColSize;
			totalColSize = 0;
			//Fix select to 50
			//var expectedTotalColSize = minDialogWidth - 17 - ((1+numCols)*8);
			//selectColSize = (50.0/oldTotalColSize) * expectedTotalColSize;
			var expectedTotalColSize = minDialogWidth - 17 - ((1 + numCols) * 8) - 50;
			oldTotalColSize -= 50;
			for (var i = 0; i < colSizes.length; i++) {
				colWidths[i] = (colWidths[i] * 1.0 / oldTotalColSize) * expectedTotalColSize;
				colSizes[i] = colWidths[i] + 'px;';
				if (metaData[i].columnName.substring(0, 5).toUpperCase() != 'HIDE_') {
					totalColSize += colWidths[i];
				}
			}
			totalAreaWidth = minDialogWidth;
		}
		//##56f END

		htmlString += '<div id="t99Lookup-dlg">';
		var containerId = "t99Lookup";
		var buttonIdAsc = null;
		var buttonIdDes = null;
		var buttonColorClass = "rclDlgSortDirectionBtn"; //settings are not ready, so we cannot get it from there
		//Display search area
		htmlString += '<div style="padding:2px;"><table>' +
			'<tr>' +
			'<td><label class="col-sm1 bt-1" for="' + containerId + '-dlgSearch' + '">Search</label></td>' +
			'<td colspan=2> <input class="col-sm2" type="text" id="' + containerId + '-dlgSearch' + '" ' +
			'></input></td>' +
			'</tr>' +
			'</table>';
		htmlString += '</div>';

		//htmlString+= '<div>'; //##56f
		htmlString += '<div style="overflow-x:auto;">'; //##56f
		htmlString += '<div id="t99HeaderRow" class="rcl-lkHeader">';
		htmlString += '<p class="rcl-lkHeaderRow">';
		//htmlString+='<span class="rcl-lkHeaderCell" style="flex-basis:50px;max-width:50px;min-width:50px;">Select</span>'; //##56f
		htmlString += '<span class="rcl-lkHeaderCell" style="flex-basis:' + selectColSize + 'px;max-width:' + //##56f
			selectColSize + 'px;min-width:' + selectColSize + 'px;">Select</span>'; //##56f
		var colHeader = null;
		for (var i = 0; i < metaData.length; i++) {
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {
				tdClass = 'class="rcl-lkHeaderCell ' + tdHideClass + '"';
			}
			else {
				tdClass = 'class="rcl-lkHeaderCell"';
			}
			colHeader = rutToTitleCase(metaData[i].columnName);
			htmlString += ('<span ' + tdClass + ' style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '">' + colHeader + '</span>');
		}
		htmlString += '</p>';

		//htmlString+='<p class="rcl-lkHeaderRow">'+cr+'<span class="rcl-lkHeaderCell" style="flex-basis:50px;max-width:50px;min-width:50px;"></span>'; //##56f
		htmlString += '<p class="rcl-lkHeaderRow">' + cr + '<span class="rcl-lkHeaderCell" style="flex-basis:' + //##56f
			selectColSize + 'px;max-width:' + selectColSize + 'px;min-width:' + selectColSize + 'px;"></span>'; //##56f
		for (var i = 0; i < metaData.length; i++) {
			colHeader = rutToTitleCase(metaData[i].columnName);//###42x
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {//###42x
				tdClass = 'class="rcl-lkHeaderCell ' + tdHideClass + '" ';
			}
			else {
				tdClass = 'class="rcl-lkHeaderCell"';;
			}
			buttonIdAsc = '"' + containerId + "-dlgAsc-" + i + '"';
			buttonIdDes = '"' + containerId + "-dlgDes-" + i + '"';
			htmlString += ('<span ' + tdClass + ' style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '"> '
				+ '<button id=' + buttonIdAsc + ' class="' + buttonColorClass
				+ '" style="font-size:10px;width:30px;text-align:center;margin:0;"'
				+ ' data-toggle="tooltip" data-placement="top" title="Sorts by ascending values of ' + colHeader + '. Existing sorts are expanded."'
				+ ' onclick=\'rptInjectSortDef("' + containerId + '", ' + buttonIdAsc + ', "' + metaData[i].columnName + '", 1); \'>&and;</button>');
			htmlString += ('<button id=' + buttonIdDes + ' class="' + buttonColorClass
				+ '" style="font-size:10px;width:30px;text-align:center;margin:0;"'
				+ ' data-toggle="tooltip" data-placement="top" title="Sorts by descending values of ' + colHeader + '. Existing sorts are expanded."'
				+ ' onclick=\'rptInjectSortDef("' + containerId + '", ' + buttonIdDes + ', "' + metaData[i].columnName + '", -1); \'>&or;</button>'
				+ '</span>');
		}// end for listOfColumns
		htmlString += '</p>';

		htmlString += '</div>' + cr;
		htmlString += '<div id="t99Lookup" class="tblArea rcl-lkArea" style="min-width:' + totalAreaWidth + 'px;width:' + totalAreaWidth + 'px;">';
		htmlString += '<p id="t99Row" class="tblRow rcl-lkAreaRow" >';
		//##56f BEGIN
        /*
        htmlString+='<span class="rcl-lkAreaCell" style="flex-basis:50px;min-width:50px;max-width:50px;"><button id="t99SelectButton" class="'+buttonColorClass+'" onclick="rutSelectLookupEntry(\'t99Lookup-dlg\',\''+
                    returnCols+'\',\''+returnIds+'\')">select</button></span>';
        */
		htmlString += '<span class="rcl-lkAreaCell" style="flex-basis:' + selectColSize + 'px;min-width:' +
			selectColSize + 'px;max-width:' + selectColSize + 'px;"><button id="t99SelectButton" class="' + buttonColorClass + '" onclick="rutSelectLookupEntryWithReturnAction(\'t99Lookup-dlg\',\'' +
			returnCols + '\',\'' + returnIds + '\')">select</button></span>';
		//##56f END
		for (var i = 0; i < metaData.length; i++) {
			tdStyleString = style = 'style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '"';
			if (0 <= '-5,4,5,-6,2,6,8'.indexOf(metaData[i].columnType)) {  //integer or decimal
				style = 'data-type="number" data-check="dec(' + metaData[i].precision + ',' + metaData[i].scale + ')" ' + tdStyleNumber;
			}
			else if (metaData[i].precision == 10) { //check for date
				for (var j = 0; j < Math.min(10, data.length); j++) { //check for data
					value = data[j][metaData[i].columnName];
					//if (value==null){ //##56a
					if (!value) { //##56a
					}
					else if ((value.length == 10) && (value.charAt(4) == '-') && (value.charAt(7) == '-')) {
						style = 'data-type="date" ' + tdStyleString;
						break;
					}
				}
			}
			else {
				style = tdStyleString;
			}
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {
				tdClass = 'class="tblField rcl-lkAreaCell ' + tdHideClass;
			}
			else {
				tdClass = 'class="tblField rcl-lkAreaCell"';
			}
			htmlString += ('<span id="t99' + metaData[i].columnName + '" ' + tdClass + ' ' + tdStyleString + '></span>');
		}// end for listOfColumns
		//htmlString+='</p></div>'; //##56f
		htmlString += '</p></div></div>'; //end table area //##56f
		if (data.length > 50) {
			htmlString += '<div id="t99paging" class="container-fluid" data-pageSize="50"></div>';
		}

		//htmlString+='</div></div>'; // end modal-body //##56f
		htmlString += '</div>'; //##56f
		$("body").append(htmlString);
		//The below makes RCL PowerTable study the template in the html and build a model
		tableSettings = tableInit("t99Lookup"); //Id of the container, we may have more than one table on the same page
		document.getElementById("t99Lookup" + tableSettings.configuration.sortDialogSearchFieldSuffix).addEventListener('keypress', function (e) {
			if (e.key === 'Enter') {
				_rptCallSearch("t99Lookup");
			}
		});
		//attaches data to RCL PowerTable
		// rptAddData("t99Lookup",data);
		//diplays the data by copying the template as many times as the data require and fill it from the data.
		// rptDisplayTable("t99Lookup");

		//BEGIN ###56e
		var maxDialogWidth = $(window).width() * 0.8;
		var dialogWidth = totalAreaWidth + 52;
		dialogWidth = Math.min(maxDialogWidth, Math.max(minDialogWidth, dialogWidth));
		//END ###56e

		$("#t99Lookup-dlg").dialog({
			autoOpen: false,
			modal: true,
			draggable: true,
			resizable: false,
			//width: "auto", //###56e
			width: dialogWidth, //###56e
			height: "auto",
			title: title,
			buttons: [
				{
					text: "Search",
					click: function () {
						_rptCallSearch(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Reset Search",     //####TO DO parameterisieren
					click: function () {
						rptResetSearch(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Reset Sort",
					click: function () {
						rptResetSortDefinitions(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Export to csv",
					click: function () {
						rptExportAsCsv("t99Lookup");  //#01
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Cancel",
					click: function () {
						rutCloseDialog("t99Lookup-dlg");
					},
					class: "rclLookupDlgBtn"
				},
			],
			open: function (event, ui) {
				rptAddData("t99Lookup", data);//Change to avoid focus on firstpage without the dialog open
				rptDisplayTable("t99Lookup");//Change to avoid focus on firstpage without the dialog open

				$(this).css("overflow", "hidden");
			}, //##56f
			close: function (event, ui) { rutCloseLookupPopoverDialog('t99Lookup-dlg', 't99Lookup'); },// changed from t99Lookup-Dlg
		});
		$("#t99Lookup-dlg").dialog('open');
		//###55 END
	}//end inner function showLookupDialog //###42
}//end function rutOpenLookupTable

function rutSelectLookupEntryWithReturnAction(dialogId, returnCols, returnIds) {
	var element = document.activeElement;
	var parents = rptGetParentIds(element);
	var settings = rptGetTableSettings(parents.parentContainerId);
	var prefix = settings.idPrefix;
	var r = parents.parentRowId.split('-');
	var rowSuffix = '-' + r[r.length - 1];
	// now we have prefix and suffix and are capable to build the elementIds.
	var columns = returnCols.split(' ');
	var returnIdList = returnIds.split(' ');
	var el = null;
	var triggeringElements = []; //###61
	for (var i = 0; i < columns.length; i++) {
		if (columns[i] != '') {
			el = document.getElementById(prefix + columns[i] + rowSuffix);
			fid = prefix + columns[i] + rowSuffix;
			el = document.getElementById(fid);
			//rutSetElementValue(returnIdList[i],rutGetElementValue(el) ); //###56c
			//###61 BEGIN
			//rutSetElementValue(rutSplitRowID(returnIdList[i]),rutGetElementValue(el) ); //###56c
			var targetId = rutSplitRowID(returnIdList[i]);
			rutSetElementValue(targetId, rutGetElementValue(el));

			var idAll = returnIdList[i].split(returnIdList[i].substring(0, 3));
			var rowId = returnIdList[i].substring(0, 3) + idAll[2];
			var datauser = {
				"userToken": "QTNPCTEST",
				"userId": "DEV_TEAM",
				"line": "R",
				"trade": "*",
				"agent": "***",
				"fscCode": "R"
			};

			var data = {};
			data.userData = datauser;
			data.seqNo = rutGetElementValue(document.getElementById(prefix + 'SEQ_NO' + rowSuffix));
			console.log("data :: " + JSON.stringify(data));

			getResultAjax("WS_BKG_GET_PRINT_CLAUSE_DETAIL", data).done(function (response) {

				//        		var newData = {};
				//        		newData.printClauseHdrSeqNo = data.seqNo;
				//        		newData.clauseTitle = rutGetElementValue(el);
				//        		newData.Detail = response;

				var detailPrintClause = getCurrentPtDataWithAction("t73PrintClausesArea", false);
				var newData = detailPrintClause[detailPrintClause.length - 1];
				newData.printClauseHdrSeqNo = data.seqNo;
				newData.clauseTitle = rutGetElementValue(el);
				newData.Detail = response;

				console.log("detailPrintClause :: " + JSON.stringify(detailPrintClause));
				rptClearDisplay("t73PrintClausesArea");
				rptAddData("t73PrintClausesArea", detailPrintClause);
				rptDisplayTable("t73PrintClausesArea");

				var clicklastrow = document.getElementById('t73selectclauseTitle-' + (detailPrintClause.length - 1));

				setDisableisMandatory();

				if (clicklastrow)
					clicklastrow.click();

				//        		rptSetSingleAreaValues(targetId, newData);
			});

			triggeringElements.push($("#" + targetId)[0]);
			//###61 END
		}
	}
	//###61 BEGIN
	for (var i = 0; i < triggeringElements.length; i++) {
		//$("#" + targetId).change() //This does not propagate event to parent
		var event = document.createEvent('Event');
		event.initEvent('change', true, true);
		triggeringElements[i].dispatchEvent(event);
	}
	//###61 END

	rutCloseLookupPopoverDialog(dialogId, parents.parentContainerId);

	function handleSetPrintClauseDetail(data) {
		rptSetSingleAreaValues("d70printClauseArea", data.PrintClauseHeader);
	}
}//end function rutSelectLookupEntry

function setDeleteBtnPowerTable(table, button) {
	var sizeTypeRows = $("#" + table).find(".tblRow").not(":last");
	if (sizeTypeRows.length > 0) {
		if (!viewMode) {
			$("#" + button).show();
		}
	} else {
		$("#" + button).hide();
	}
}

function toggleCheckboxToViewModeQuo(checkboxId, wrapperClasses, value) {
	var checkbox = $("#" + checkboxId);
	var wrapper = checkbox.parent();
	if (wrapperClasses) {
		wrapper.attr("class", wrapperClasses);
	}
	var label = checkbox.prev();
	var customTagClass = "";
	var checkboxClass = checkbox.attr("class");
	if (checkboxClass) {
		var ctClasses = ["dtlField", "searchField", "tblField"];
		for (var i = 0; i < ctClasses.length; i++) {
			if (checkboxClass.indexOf(ctClasses[i]) !== -1) {
				customTagClass = ctClasses[i];

			}
		}
	}

	var html = '<label class="rcl-standard-font" for="' + checkboxId + '">' + label.html() + "</label>\n" +
		'<select id="' + checkboxId + '" class="rcl-standard-form-control rcl-standard-component ' +
		customTagClass + '" data-rdo data-ct="bt-select tb-YesNo">\n' +
		'	<option value></option>\n' +
		'	<option value="Y">Yes</option>\n' +
		'	<option value="N">No</option>\n' +
		'</select>\n';
	wrapper.html(html);
	if (value != "") {
		$("#" + checkboxId).val('Y');
	} else {
		$("#" + checkboxId).val('N');
	}

}

function qtn_getDataFormServerToTable(service, data, table, onDataDisplayed, onDataReceived) {
	rptClearDisplay(table);
	//	showLoader(table);

	getResultAjax(service, data)
		.done(handleTable);

	function handleTable(response) {
		//		hideLoader(table);

		if (onDataReceived) {

			onDataReceived(response.resultContent);
		}

		if (response.resultCode == "200") {

			resultContent = response.resultContent;

		} else if (response.resultCode == '401') {

			location.href = expiryUrl;

		} else {
			resultContent = [];
		}
		//console.log(JSON.stringify(resultContent));

		//console.log(JSON.stringify(setbuttoncopy(table,resultContent)));
		transformDates(resultContent);
		setbuttoncopy(table, resultContent);
		rptAddData(table, resultContent);
		rptDisplayTable(table);

		if (onDataDisplayed) {
			onDataDisplayed();
		}
	}
}

function rutOpenLookupTable2(table, returnCols, returnIds, selectCols, selectValues, selectOperators, displayTitle) {
	console.log("rutOpenLookupTable2 ::");

	var title = (displayTitle) ? displayTitle : rutToTitleCase(table.substring(4) + ' Lookup'); //remove the VRL_ from the table name
	//##60 BEGIN
	var wsInput = null;
	try {
		wsInput = _rutPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators, returnCols, false);
	} catch (e) {
		rutOpenMessageBox("Warning", e.message, e.code, null, '');
		return;
	}
	//###64 END
	returnCols = returnCols.replace(/\*\w*/g, "");
	//##60 END
	$("body").append('<div class="loading"></div>'); //###65
	rutGetLookupData(wsInput, showLookupDialog); //###42
	//showLookupDialog(getTestData()); //only for dry test without server
	return; //###42

	//Inner Function
	function showLookupDialog(lookupResult) {
		//###55 BEGIN
		var data = lookupResult.data;
		var metaData = lookupResult.metadata;
		if (data.length <= 0) {
			//No data found display error message
			let errorMessage = (wsInput.select.length == 0) ? 'No data found' : 'No data found for ';
			for (var i = 0; i < wsInput.select.length; i++) {
				errorMessage += wsInput.select[i].column + wsInput.select[i].operator + wsInput.select[i].value + ' ';
			}
			rutOpenMessageBox('Error on lookup of ' + table.substring(4), errorMessage, 'rut003-001', null, '');
			return;
		}
		var element = document.getElementById('t99Lookup-dlg');
		if (element != null) { //another lookup table was not clossed properly but through x button
			element.parentNode.removeChild(element); //element.remove does not work in IE
		}
		var htmlString = '';
		var cr = '\r\n';
		var tdStyleString = '';
		var tdStyleNumber = '';
		var tdHideClass = "d-none";
		var tdClass = '';

		var minDialogWidth = 550; //This is wide enough for paging/dialog buttons //##56e

		var colWidths = []; //##56f
		var selectColSize = 50; //##56f
		var colSizes = [];
		//var totalColSize=50; //we have already the select button //##56f
		var totalColSize = selectColSize; //##56f
		var numCols = 0;
		var colSize = 0;
		for (var i = 0; i < metaData.length; i++) { //Here we compute the size of the columns
			colSize = Math.min(Math.max(metaData[i].precision, metaData[i].columnName.length) * 9, 200); //1 char ~ 9px
			colSize = Math.max(colSize, 72); //Need at least space for sort buttons //##56f
			colWidths.push(colSize); //##56f
			colSizes.push(colSize + 'px;');
			if (metaData[i].columnName.substring(0, 5).toUpperCase() != 'HIDE_') {
				numCols++;
				totalColSize += colSize;
			}
		}
		//var totalAreaWidth=((1+numCols)*8)+totalColSize+1; //each column has 2 x 3px padding and 1px border, 17px for scrollbar //##56f
		var totalAreaWidth = ((1 + numCols) * 8) + totalColSize + 17;

		//##56f BEGIN
		if (totalAreaWidth < minDialogWidth) {
			//Redistribute column width using width ratio
			var oldTotalColSize = totalColSize;
			totalColSize = 0;
			//Fix select to 50
			//var expectedTotalColSize = minDialogWidth - 17 - ((1+numCols)*8);
			//selectColSize = (50.0/oldTotalColSize) * expectedTotalColSize;
			var expectedTotalColSize = minDialogWidth - 17 - ((1 + numCols) * 8) - 50;
			oldTotalColSize -= 50;
			for (var i = 0; i < colSizes.length; i++) {
				colWidths[i] = (colWidths[i] * 1.0 / oldTotalColSize) * expectedTotalColSize;
				colSizes[i] = colWidths[i] + 'px;';
				if (metaData[i].columnName.substring(0, 5).toUpperCase() != 'HIDE_') {
					totalColSize += colWidths[i];
				}
			}
			totalAreaWidth = minDialogWidth;
		}
		//##56f END

		htmlString += '<div id="t99Lookup-dlg">';
		var containerId = "t99Lookup";
		var buttonIdAsc = null;
		var buttonIdDes = null;
		var buttonColorClass = "rclDlgSortDirectionBtn"; //settings are not ready, so we cannot get it from there
		//Display search area
		htmlString += '<div style="padding:2px;"><table>' +
			'<tr>' +
			'<td><label class="col-sm1 bt-1" for="' + containerId + '-dlgSearch' + '">Search</label></td>' +
			'<td colspan=2> <input class="col-sm2" type="text" id="' + containerId + '-dlgSearch' + '" ' +
			'></input></td>' +
			'</tr>' +
			'</table>';
		htmlString += '</div>';

		//htmlString+= '<div>'; //##56f
		htmlString += '<div style="overflow-x:auto;">'; //##56f
		htmlString += '<div id="t99HeaderRow" class="rcl-lkHeader">';
		htmlString += '<p class="rcl-lkHeaderRow">';
		//htmlString+='<span class="rcl-lkHeaderCell" style="flex-basis:50px;max-width:50px;min-width:50px;">Select</span>'; //##56f
		htmlString += '<span class="rcl-lkHeaderCell" style="flex-basis:' + selectColSize + 'px;max-width:' + //##56f
			selectColSize + 'px;min-width:' + selectColSize + 'px;">Select</span>'; //##56f
		var colHeader = null;
		for (var i = 0; i < metaData.length; i++) {
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {
				tdClass = 'class="rcl-lkHeaderCell ' + tdHideClass + '"';
			}
			else {
				tdClass = 'class="rcl-lkHeaderCell"';
			}
			colHeader = rutToTitleCase(metaData[i].columnName);
			htmlString += ('<span ' + tdClass + ' style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '">' + colHeader + '</span>');
		}
		htmlString += '</p>';


		htmlString += '</div>' + cr;
		htmlString += '<div id="t99Lookup" class="tblArea rcl-lkArea" style="min-width:' + totalAreaWidth + 'px;width:' + totalAreaWidth + 'px;">';
		htmlString += '<p id="t99Row" class="tblRow rcl-lkAreaRow" >';
		//##56f BEGIN


		//htmlString+='</p></div>'; //##56f
		htmlString += '</p></div></div>'; //end table area //##56f
		if (data.length > 50) {
			htmlString += '<div id="t99paging" class="container-fluid" data-pageSize="50"></div>';
		}

		//htmlString+='</div></div>'; // end modal-body //##56f
		htmlString += '</div>'; //##56f
		$("body").append(htmlString);


		//BEGIN ###56e
		var maxDialogWidth = $(window).width() * 0.8;
		var dialogWidth = totalAreaWidth + 52;
		dialogWidth = Math.min(maxDialogWidth, Math.max(minDialogWidth, dialogWidth));
		//END ###56e

		$("#t99Lookup-dlg").dialog({
			autoOpen: false,
			modal: true,
			draggable: true,
			resizable: false,
			//width: "auto", //###56e
			width: dialogWidth, //###56e
			height: "auto",
			title: title,
			buttons: [
				{
					text: "Search",
					click: function () {
						_rptCallSearch(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Reset Search",     //####TO DO parameterisieren
					click: function () {
						rptResetSearch(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Reset Sort",
					click: function () {
						rptResetSortDefinitions(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Export to csv",
					click: function () {
						rptExportAsCsv("t99Lookup");  //#01
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Cancel",
					click: function () {
						rutCloseDialog("t99Lookup-dlg");
					},
					class: "rclLookupDlgBtn"
				},
			],
			open: function (event, ui) {
				rptAddData("t99Lookup", data);//Change to avoid focus on firstpage without the dialog open
				rptDisplayTable("t99Lookup");//Change to avoid focus on firstpage without the dialog open

				$(this).css("overflow", "hidden");
			}, //##56f
			close: function (event, ui) { rutCloseLookupPopoverDialog('t99Lookup-dlg', 't99Lookup'); },// changed from t99Lookup-Dlg
		});
		$("#t99Lookup-dlg").dialog('open');
		//###55 END
	}//end inner function showLookupDialog //###42
}//end function rutOpenLookupTable


function saveAs(blob, fileName) {
	try {

		var _global = typeof window === 'object' && window.window === window
			? window : typeof self === 'object' && self.self === self
				? self : typeof global === 'object' && global.global === global
					? global
					: this
		var URL = _global.URL || _global.webkitURL
		var a = document.createElement('a')
		a.download = fileName
		a.rel = 'noopener' // tabnabbing

		// TODO: detect chrome extensions & packaged apps
		// a.target = '_blank'
		if (typeof blob === 'string') {
			// Support regular links

			a.href = blob
			if (a.origin !== location.origin) {
				corsEnabled(a.href)
					? download(blob, fileName, opts)
					: click(a, a.target = '_blank')
			} else {
				click(a)
			}
		} else if (window.navigator.msSaveOrOpenBlob) {
			// IE11
			window.navigator.msSaveOrOpenBlob(blob, fileName);
		} else {
			a.href = URL.createObjectURL(blob)
			setTimeout(function () { URL.revokeObjectURL(a.href) }, 4E4) // 40s
			setTimeout(function () { click(a) }, 0)
		}

	} catch (exc) {
		window.open("data:" + mimeType + "," + encodeURIComponent(text), '_blank', '');
	}
}

//`a.click()` doesn't work for all browsers (#465)
function click(node) {
	try {
		node.dispatchEvent(new MouseEvent('click'))
	} catch (e) {
		var evt = document.createEvent('MouseEvents')
		evt.initMouseEvent('click', true, true, window, 0, 0, 0, 80,
			20, false, false, false, false, 0, null)
		node.dispatchEvent(evt)
	}
}

function bom(blob, opts) {
	if (typeof opts === 'undefined') opts = { autoBom: false }
	else if (typeof opts !== 'object') {
		console.warn('Deprecated: Expected third argument to be a object')
		opts = { autoBom: !opts }
	}

	// prepend BOM for UTF-8 XML and text/* types (including HTML)
	// note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
	if (opts.autoBom && /^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
		return new Blob([String.fromCharCode(0xFEFF), blob], { type: blob.type })
	}
	return blob
}

function download(url, name, opts) {
	var xhr = new XMLHttpRequest()
	xhr.open('GET', url)
	xhr.responseType = 'blob'
	xhr.onload = function () {
		saveAs(xhr.response, name, opts)
	}
	xhr.onerror = function () {
		console.error('could not download file')
	}
	xhr.send()
}

function corsEnabled(url) {
	var xhr = new XMLHttpRequest()
	// use sync to avoid popup blocker
	xhr.open('HEAD', url, false)
	try {
		xhr.send()
	} catch (e) { }
	return xhr.status >= 200 && xhr.status <= 299
}

function exportToCsv(filename, rows, isblob) {
	console.log("exportToCsv ::" + rows);
	var processRow = function (row) {
		var finalVal = '';
		for (var j = 0; j < row.length; j++) {
			var innerValue = row[j] === null ? '' : row[j].toString();
			if (row[j] instanceof Date) {
				innerValue = row[j].toLocaleString();
			};
			var result = innerValue.replace(/"/g, '""');
			if (result.search(/("|,|\n)/g) >= 0)
				result = '"' + result + '"';
			if (j > 0)
				finalVal += ',';
			finalVal += result;
		}
		return finalVal + '\n';
	};

	var csvFile = '';

	//  for (var i = 0; i < rows.length; i++) {

	// csvFile += processRow(rows[i]);
	// }

	// var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
	var blob = isblob;
	//console.log("blob ::"+URL.createObjectURL(blob));
	/* if (navigator.msSaveBlob) { // IE 10+
		 navigator.msSaveBlob(blob, filename);
	 } else {
		 var link = document.createElement("a");
		 if (link.download !== undefined) { // feature detection
			 // Browsers that support HTML5 download attribute
			 var url = URL.createObjectURL(blob);
			 link.setAttribute("href", url);
			 link.setAttribute("download", filename);
			 link.style.visibility = 'hidden';
			 document.body.appendChild(link);
			 link.click();
			 document.body.removeChild(link);
		 }
	 }*/
}



/*exportToCsv('export.csv', [
['name','description'],
['david','123'],
['jona','""'],
['a','b'],

])*/


function PopupCenter(url, title, w, h) {
	// Fixes dual-screen position                         Most browsers      Firefox
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
	var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

	var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	var systemZoom = width / window.screen.availWidth;
	var left = (width - w) / 2 / systemZoom + dualScreenLeft
	var top = (height - h) / 2 / systemZoom + dualScreenTop
	var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

	// Puts focus on the newWindow
	if (window.focus) newWindow.focus();
}

function conCatDate(getDate) {
	var NewDate = "";
	var reg = /[.,\/ -]/;

	if (getDate != undefined) {

		if (getDate.charAt(2) == '/') {

			NewDate = getDate.split(reg).reverse().join('-');

		} else if (getDate.charAt(4) == '-') {

			NewDate = getDate;

		} else {
			NewDate = getDate.substring(0, 4) + "-" + getDate.substring(4, 6) + "-" + getDate.substring(6, 8);
		}

		/*var NewString = getDate.split("/([^\d]*)(\d*)([^\w]*)/");
		if(NewString.length == 3){
			NewDate = getDate;
		}else{
			NewDate = getDate.substring(0, 4)+"-"+getDate.substring(4, 6)+"-"+getDate.substring(6, 8);
		}*/
	}
	return NewDate;
}


function splitDate(getDate) {

	var NewDate = "";
	var reg = /[.,\/ -]/;

	if (getDate != "") {
		if (getDate != undefined) {

			if (getDate.charAt(2) == '/') {

				NewDate = getDate.split(reg).reverse().join('');

			} else if (getDate.charAt(4) == '-') {

				NewDate = NewDate = getDate.split(reg).join('');

			} else {
				NewDate = getDate.substring(0, 4) + "-" + getDate.substring(4, 6) + "-" + getDate.substring(6, 8);
			}

		}
	}
	return NewDate;
}

function convertDateCheckDiff(getDate) {
	var NewDate = "";
	var reg = /[.,\/ -]/;

	if (getDate != "") {
		if (getDate != undefined) {

			if (getDate.charAt(2) == '/') {

				NewDate = getDate.split(reg).reverse().join('');

			} else if (getDate.charAt(4) == '-') {

				NewDate = NewDate = getDate.split(reg).join('');

			} else {
				NewDate = getDate;
			}

		}
	}
	return NewDate; //20181231
}

function convertDatevalidate(getDate) {

	var NewDate = "";
	var reg = /[.,\/ -]/;

	if (getDate != undefined) {
		if (getDate.charAt(2) == '/') {

			NewDate = getDate.split(reg).reverse().join('/');

		}
	}
	return NewDate;

}

function convertformatDatereg(getDate) {
	var NewDate = "";
	var reg = /[.,\/ -]/;
	if (getDate != undefined) {
		if (getDate.charAt(2) == '/') {

			NewDate = getDate.split(reg).reverse().join('-');

		} else {

			NewDate = getDate;

		}
	}
	return NewDate;//2019-06-11
}

function setformatedate_IscaseError(ISdate) {
	var date = ISdate.replace(/[^0-9]/g, '');
	if (date.length != 8) {

	}
	return date;
	//if(date.length != 8){}
}

/*function IssetDate_powerTable(containerId,typedate){
	var i = 0;
	$("#"+containerId).find('.hasDatepicker').each(function(){ //#### settings for details //###32
			id=$(this)[0].id;
	        varstr=$(this)[0].value;
	        if(varstr != ""){
	        	if(id.split("-").length > 1){
	        		rutSetElementValueqt(id,varstr,typedate);
	        	}

	         }

	        i ++;

    });
}

function IssetDate(containerId,typedate){
	var newdata = {};
	$("#"+containerId).find('.hasDatepicker').each(function(){ //#### settings for details //###32
        id=this.id;
        varstr=this.value;
        if(varstr != ""){

        	rutSetElementValueqt(id,varstr,typedate);
        }

    });

}*/


function rutSetElementValueqt(elementId, value, typedate) {
	var newvalue = "";
	var element = null;
	if ((typeof elementId) == "string") { //the elementId is an id
		element = document.getElementById(elementId);

	} else { //elementId is an element
		element = elementId;
	}
	var dataType = element.getAttribute('data-type') || ' ';
	if (dataType == 'date') {
		if (typedate) { //want sent to database
			element.value = splitDate(value);
		} else {
			element.value = conCatDate(value);
		}

	}

} //end function rutSetElementValue

function qtnGetCurrentDateTime() {
	var today = new Date();
	var month = today.getMonth() + 1;
	var day = today.getDate();
	var date = (day < 10 ? '0' : '') + day +
		'/' + (month < 10 ? '0' : '') + month + '/' +
		today.getFullYear();


	//var date = today.getDate() + "/" + (today.getMonth()+1) + '/' + today.getFullYear();
	var time = today.getHours() + ":" + today.getMinutes();
	var dateTime = date + ' ' + time;

	return dateTime;
}

function calcTime(city, offset) {

	d = new Date();

	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	today = new Date(utc + (3600000 * offset));

	var month = today.getMonth() + 1;
	var day = today.getDate();
	var date = (day < 10 ? '0' : '') + day +
		'/' + (month < 10 ? '0' : '') + month + '/' +
		today.getFullYear();
	var hour = (today.getHours() < 10 ? '0' : '') + today.getHours();
	var minute = (today.getMinutes() < 10 ? '0' : '') + today.getMinutes();
	var time = hour + ":" + minute;
	var dateTime = date + ' ' + time;

	return dateTime;

}

function setDateDay(isday) {
	var date = new Date();
	date.setDate(date.getDate() + isday);

	return date;
}

function addDays(dateObj, numDays) {
	var res = dateObj.setDate(dateObj.getDate() + numDays);

	var d = new Date(res);
	var month = d.getMonth() + 1;
	var day = d.getDate();

	var output = (day < 10 ? '0' : '') + day +
		'/' + (month < 10 ? '0' : '') + month + '/' +
		d.getFullYear();

	return output;
}

function deleteDays(dateObj, numDays) {
	var res = dateObj.setDate(dateObj.getDate() - numDays);

	var d = new Date(res);
	var month = d.getMonth() + 1;
	var day = d.getDate();

	var output = (day < 10 ? '0' : '') + day +
		'/' + (month < 10 ? '0' : '') + month + '/' +
		d.getFullYear();

	return output;
}


function fnonfocus(isid, rowid) {

	var rowId = rowid.split("-");

	$("#" + isid + "-" + rowId[1]).focus();

}

function isEmpty(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key))
			return false;
	}
	return true;
}

function onfocusUpdate(area, rowId) {
	$('#t10weight-0').addClass('BG_updateFiled');

}

function getCurrentServerDateTH(useDBFormat) {
	if (useDBFormat === undefined) {
		useDBFormat = false;
	}
	var date = new Date();

	console.log("getCurrentServerDate " + date)
	if (useDBFormat) {
		return "" + date.getUTCFullYear() + (date.getUTCMonth() + 1) + date.getDate();
	} else {
		return date.getUTCFullYear() + "-" + (date.getUTCMonth() + 1) + "-" + date.getDate();
	}
}

function Isempty(data) {
	if (typeof (data) == 'number' || typeof (data) == 'boolean') {
		return false;
	}
	if (typeof (data) == 'undefined' || data === null) {
		return true;
	}
	if (typeof (data.length) != 'undefined') {
		return data.length == 0;
	}
	var count = 0;
	for (var i in data) {
		if (data.hasOwnProperty(i)) {
			count++;
		}
	}
	return count == 0;
}

//start check Diff json//

/* NEW CODE*/
function jsonEqual(a, b) {
	a = JSON.parse(a);

	//	JSON.stringify(replaceNull(replaceNumber(a), "")) === JSON.stringify(replaceNull(replaceNumber(b), "")); // Changed
	return objectEquals(replaceNull(replaceNumber(a), ""), replaceNull(replaceNumber(b), ""));
}

function objectEquals(x, y) {
	'use strict';
	if (x === null || x === undefined || y === null || y === undefined) { return x === y; }
	if (x.constructor !== y.constructor) { return false; }// after this just checking type of one would be enough
	if (x instanceof Function) { return x === y; } // if they are functions, they should exactly refer to same one (because of closures)
	if (x instanceof RegExp) { return x === y; }// if they are regexps, they should exactly refer to same one (it is hard to better equality check on current ES)
	if (x === y || x.valueOf() === y.valueOf()) { return true; }
	if (Array.isArray(x) && x.length !== y.length) { return false; }
	if (x instanceof Date) { return false; }// if they are dates, they must had equal valueOf

	// if they are strictly equal, they both need to be object at least
	if (!(x instanceof Object)) { return false; }
	if (!(y instanceof Object)) { return false; }

	// recursive object equality check
	var p = Object.keys(x);
	return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) &&
		p.every(function (i) { return objectEquals(x[i], y[i]); });
}
function replaceNull(someObj, replaceValue) {
	if (replaceValue == null || replaceValue == "" || replaceValue === "undefined") {
		replaceValue = "***";
	}
	var data = JSON.stringify(someObj, function (key, value) {
		return (String(value) === "null" || String(value) === "undefined") ? replaceValue : value
	});
	return JSON.parse(data);
}
function replaceNulltoEmpty(someObj, replaceValue) {
	if (replaceValue == null || replaceValue === "undefined") {
		replaceValue = "";
	}
	var data = JSON.stringify(someObj, function (key, value) {
		return (String(value) === "null" || String(value) === "undefined") ? replaceValue : value
	});
	return JSON.parse(data);
}
function replaceNumber(someObj) {
	var data = JSON.stringify(someObj, function (key, value) {
		return (isNumber(value) || String(value) === "undefined") ? String(value) : value
	});
	return JSON.parse(data);
}
function isNumber(value) {
	return typeof value === 'number' && isFinite(value);
}

//		/* OLD CODE*/
//		function jsonEqual(a,b) {
//			a = JSON.parse(a);
//			//console.log("orgi :"+JSON.stringify(replaceNull(replaceNumber(a), "")));
//			//console.log("new :"+JSON.stringify(replaceNull(replaceNumber(b), "")));
//			//console.log("orgi :"+JSON.stringify(replaceNumber(a)));
//			 return JSON.stringify(replaceNull(replaceNumber(a), "")) === JSON.stringify(replaceNull(replaceNumber(b), ""));
//		}
//
//			//--check value null change Empty--//
//		function replaceNull(someObj, replaceValue = "***") {
//			  const replacer = (key, value) =>
//
//			  String(value) === "null" || String(value) === "undefined" ? replaceValue : value;
//
//			  return JSON.parse( JSON.stringify(someObj, replacer));
//		}
//
//		function replaceNulltoEmpty(someObj, replaceValue = "") {
//			  const replacer = (key, value) =>
//
//			  String(value) === "null" || String(value) === "undefined" ? replaceValue : value;
//
//			  return JSON.parse( JSON.stringify(someObj, replacer));
//		}
//
//			//--start check value typeof number change string--//
//		function replaceNumber(someObj) {
//			  const replacer = (key, value) =>
//
//			  isNumber(value) || String(value) === "undefined" ? String(value) : value;
//
//			  return JSON.parse( JSON.stringify(someObj, replacer));
//		}
//
//		function isNumber(value) {
//			return typeof value === 'number' && isFinite(value);
//		}
//			//--end check value typeof number change string--//

//end check Diff json//

function upc() {

	var id = document.activeElement.id;

	var x = document.getElementById(id);
	x.value = x.value.toUpperCase();

}

function removeEmtryOptions(elementId) {
	var x = document.getElementById(elementId);
	x.remove(0);
}



function fngetcheck(filedId) {
	var gtaFlag = "";
	if ($('#' + filedId).is(":checked")) {
		gtaFlag = "Y";
	} else {
		gtaFlag = "N";
	}

	return gtaFlag;
}

function fngetcheckfield(param) {
	var gtaFlag = "";
	if (param == "Y") {
		gtaFlag = "Y";
	} else {
		gtaFlag = "N";
	}

	return gtaFlag;
}

function fngetcheckfieldJquery(param) {
	var gtaFlag = "";
	if (param) {
		gtaFlag = "Y";
	} else {
		gtaFlag = "N";
	}

	return gtaFlag;
}

function fnsetcheckbox(isvalue, filedId) {
	if (isvalue == 'Y') {
		$("#" + filedId).prop("checked", true);
	}

}

function removeComma(value) {
	return parseFloat(value.replace(/,/g, ''));
}

function round(value, decimals) {
	return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function trim(s) {
	var l = 0; var r = s.length - 1;
	while (l < s.length && s[l] == ' ') { l++; }
	while (r > l && s[r] == ' ') { r -= 1; }
	return s.substring(l, r + 1);
}

function validatedateFormate(inputid, inputText) {
	var getdate = conValidatedateFormate(inputText);
	var date = new Date(getdate);
	if (date == 'Invalid Date') {
		dialogGeneric("Warning", "Invalid date format!", "OK").then(function (result) {
			$('#' + inputid).val("");
			$('#' + inputid).focus();
		});
	}

}

function conValidatedateFormate(getDate) {
	console.log("conValidatedateFormate :: " + getDate)
	var NewDate = "";
	var reg = /[.,\/ -]/;

	if (getDate != undefined) {

		NewDate = getDate.split(reg);
		NewDate = NewDate[1] + "/" + NewDate[0] + "/" + NewDate[2];


	}

	return NewDate;
}

function splitId(id, prm) {
	var getrow = id.split(prm);
	return getrow[1];
}

function checkIfDuplicateExists(sorted_arr) {
	var data = sorted_arr;
	var filter = function (value, index) { return this.indexOf(value) == index };
	var results = data.filter(filter, data);

	return results;
}

function settime_qnt(valuetime) {
	var num;
	if (valuetime == undefined) {
		num = "0000";
	} else if (valuetime == "0") {
		num = "0000";
	}

	return num;

}

function SetPathUpload() {
	var data = {"userData" : GetuserData};
 
	getResultAjaxUploadFile("WS_QTN_GET_PATHATTACH",data).done(handleDisplay);

	function handleDisplay(response){
		if(response.resultCode == '200'){
			_getpath(response.resultContent);
		 }else{
			 dialogGeneric("Warning", "invalid URL, Path Upload File " , "Ok");
		 }
	}

	var  Gurl =  "";
	function _getpath(Geturl){
			Gurl = Geturl;
	}

        return {
		"host": IP_ADDRESS,
		"path": Gurl,
		"baseurl": baseurl,
		"baseurl2": baseurl2,
		"urlUpfile": upfile
	   }
	
}

function qtn_LookupByKey(table, returnCols, returnIds, selectCols, selectValues, selectOperators, selectOperatorsParam, selectColsOr, tagId) {

	if (typeof tagId != 'undefined' && tagId != null && tagId != '') {
		tagId = rutSplitRowID(tagId); //###56c
		let value = rutGetElementValue(tagId);
		if (value == null || value.trim() == '') {

			let returnIdArray = returnIds.split(' ');
			for (var i = 0; i < returnIdArray.length; i++) {
				var elemId = rutSplitRowID(returnIdArray[i]);
				if (elemId == tagId) {
					continue;
				}
				rutSetElementValue(elemId, '');
			}
			return;
		}
	}
	var wsInput = null;
	try {
		wsInput = _QtnPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators, returnCols, true, selectOperatorsParam, selectColsOr);
	} catch (e) {
		return;
	}
	returnCols = returnCols.replace(/\*\w*/g, "");

	Qtn_GetLookupData(wsInput, processLookupByKey);//###42


	function processLookupByKey(lookupResult) {//###42
		var data = lookupResult.data;
		var metaData = lookupResult.metadata;//###42
		var columns = returnCols.split(' ');
		var returnIdList = returnIds.split(' ');
		if (data.length == 1) {
			var triggeringElements = [];
			for (var i = 0; i < columns.length; i++) {
				if (columns[i] != '') {
					for (var j = 0; j < metaData.length; j++) {
						if (metaData[j].columnName == columns[i] && returnIdList[i] && returnIdList[i] != '*') { //###62
							var targetId = rutSplitRowID(returnIdList[i]);
							rutSetElementValue(targetId, data[0][metaData[j].columnName]);
							triggeringElements.push($("#" + targetId)[0]);

							break;
						}
					}
				}
			}
			for (var i = 0; i < triggeringElements.length; i++) {
				var event = document.createEvent('Event');
				event.initEvent('change', true, true);
				triggeringElements[i].dispatchEvent(event);
			}
		}
		else {
			for (var i = 0; i < returnIdList.length; i++) {
				var elemId = rutSplitRowID(returnIdList[i]);
				if (tagId == elemId) {
					continue;
				}
				rutSetElementValue(elemId, '');
			}
		}
	}
}


function qtn_OpenLookupTable(table, returnCols, returnIds, selectCols, selectValues, selectOperators, displayTitle, selectOperatorsParam, selectColsOr) {

	var title = (displayTitle) ? displayTitle : rutToTitleCase(table.substring(4) + ' Lookup'); //remove the VRL_ from the table name

	var wsInput = null;
	try {
		wsInput = _QtnPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators, returnCols, false, selectOperatorsParam, selectColsOr);
	} catch (e) {
		rutOpenMessageBox("Warning", e.message, e.code, null, '');
		return;
	}
	//###64 END
	returnCols = returnCols.replace(/\*\w*/g, "");
	//##60 END
	$("body").append('<div class="loading"></div>'); //###65
	Qtn_GetLookupData(wsInput, showLookupDialog); //###42
	//showLookupDialog(getTestData()); //only for dry test without server
	return; //###42

	//Inner Function
	function showLookupDialog(lookupResult) {

		//###55 BEGIN
		var data = lookupResult.data;
		var metaData = lookupResult.metadata;

		if (data == '401') {
			console.log("data.length :: " + data);
			rutOpenMessageBox('Warning', 'Session Expired!', '');
			//###77 END
			return;
		} else if (data.length <= 0) {
			rutOpenMessageBox('Warning', 'No row(s) retrieved', '');
			//###77 END
			return;
		}
		var element = document.getElementById('t99Lookup-dlg');
		if (element != null) { //another lookup table was not clossed properly but through x button
			element.parentNode.removeChild(element); //element.remove does not work in IE
		}
		var htmlString = '';
		var cr = '\r\n';
		var tdStyleString = '';
		var tdStyleNumber = '';
		var tdHideClass = "d-none";
		var tdClass = '';

		var minDialogWidth = 550; //This is wide enough for paging/dialog buttons //##56e

		var colWidths = []; //##56f
		var selectColSize = 50; //##56f
		var colSizes = [];
		//var totalColSize=50; //we have already the select button //##56f
		var totalColSize = selectColSize; //##56f
		var numCols = 0;
		var colSize = 0;
		for (var i = 0; i < metaData.length; i++) { //Here we compute the size of the columns
			colSize = Math.min(Math.max(metaData[i].precision, metaData[i].columnName.length) * 9, 200); //1 char ~ 9px
			colSize = Math.max(colSize, 72); //Need at least space for sort buttons //##56f
			colWidths.push(colSize); //##56f
			colSizes.push(colSize + 'px;');
			if (metaData[i].columnName.substring(0, 5).toUpperCase() != 'HIDE_') {
				numCols++;
				totalColSize += colSize;
			}
		}
		//var totalAreaWidth=((1+numCols)*8)+totalColSize+1; //each column has 2 x 3px padding and 1px border, 17px for scrollbar //##56f
		var totalAreaWidth = ((1 + numCols) * 8) + totalColSize + 17;

		//##56f BEGIN
		if (totalAreaWidth < minDialogWidth) {
			//Redistribute column width using width ratio
			var oldTotalColSize = totalColSize;
			totalColSize = 0;
			//Fix select to 50
			//var expectedTotalColSize = minDialogWidth - 17 - ((1+numCols)*8);
			//selectColSize = (50.0/oldTotalColSize) * expectedTotalColSize;
			var expectedTotalColSize = minDialogWidth - 17 - ((1 + numCols) * 8) - 50;
			oldTotalColSize -= 50;
			for (var i = 0; i < colSizes.length; i++) {
				colWidths[i] = (colWidths[i] * 1.0 / oldTotalColSize) * expectedTotalColSize;
				colSizes[i] = colWidths[i] + 'px;';
				if (metaData[i].columnName.substring(0, 5).toUpperCase() != 'HIDE_') {
					totalColSize += colWidths[i];
				}
			}
			totalAreaWidth = minDialogWidth;
		}
		//##56f END

		htmlString += '<div id="t99Lookup-dlg">';
		var containerId = "t99Lookup";
		var buttonIdAsc = null;
		var buttonIdDes = null;
		var buttonColorClass = "rclDlgSortDirectionBtn"; //settings are not ready, so we cannot get it from there
		//Display search area
		htmlString += '<div style="padding:2px;"><table>' +
			'<tr>' +
			'<td><label class="col-sm1 bt-1" for="' + containerId + '-dlgSearch' + '">Search</label></td>' +
			'<td colspan=2> <input class="col-sm2" type="text" id="' + containerId + '-dlgSearch' + '" ' +
			'></input></td>' +
			'</tr>' +
			'</table>';
		htmlString += '</div>';

		//htmlString+= '<div>'; //##56f
		htmlString += '<div style="overflow-x:auto;">'; //##56f
		htmlString += '<div id="t99HeaderRow" class="rcl-lkHeader">';
		htmlString += '<p class="rcl-lkHeaderRow">';
		//htmlString+='<span class="rcl-lkHeaderCell" style="flex-basis:50px;max-width:50px;min-width:50px;">Select</span>'; //##56f
		htmlString += '<span class="rcl-lkHeaderCell" style="flex-basis:' + selectColSize + 'px;max-width:' + //##56f
			selectColSize + 'px;min-width:' + selectColSize + 'px;">Select</span>'; //##56f
		var colHeader = null;
		for (var i = 0; i < metaData.length; i++) {
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {
				tdClass = 'class="rcl-lkHeaderCell ' + tdHideClass + '"';
			}
			else {
				tdClass = 'class="rcl-lkHeaderCell"';
			}
			colHeader = rutToTitleCase(metaData[i].columnName);
			htmlString += ('<span ' + tdClass + ' style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '">' + colHeader + '</span>');
		}
		htmlString += '</p>';

		//htmlString+='<p class="rcl-lkHeaderRow">'+cr+'<span class="rcl-lkHeaderCell" style="flex-basis:50px;max-width:50px;min-width:50px;"></span>'; //##56f
		htmlString += '<p class="rcl-lkHeaderRow">' + cr + '<span class="rcl-lkHeaderCell" style="flex-basis:' + //##56f
			selectColSize + 'px;max-width:' + selectColSize + 'px;min-width:' + selectColSize + 'px;"></span>'; //##56f
		for (var i = 0; i < metaData.length; i++) {
			colHeader = rutToTitleCase(metaData[i].columnName);//###42x
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {//###42x
				tdClass = 'class="rcl-lkHeaderCell ' + tdHideClass + '" ';
			}
			else {
				tdClass = 'class="rcl-lkHeaderCell"';;
			}
			buttonIdAsc = '"' + containerId + "-dlgAsc-" + i + '"';
			buttonIdDes = '"' + containerId + "-dlgDes-" + i + '"';
			htmlString += ('<span ' + tdClass + ' style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '"> '
				+ '<button id=' + buttonIdAsc + ' class="' + buttonColorClass
				+ '" style="font-size:10px;width:30px;text-align:center;margin:0;"'
				+ ' data-toggle="tooltip" data-placement="top" title="Sorts by ascending values of ' + colHeader + '. Existing sorts are expanded."'
				+ ' onclick=\'rptInjectSortDef("' + containerId + '", ' + buttonIdAsc + ', "' + metaData[i].columnName + '", 1); \'>&and;</button>');
			htmlString += ('<button id=' + buttonIdDes + ' class="' + buttonColorClass
				+ '" style="font-size:10px;width:30px;text-align:center;margin:0;"'
				+ ' data-toggle="tooltip" data-placement="top" title="Sorts by descending values of ' + colHeader + '. Existing sorts are expanded."'
				+ ' onclick=\'rptInjectSortDef("' + containerId + '", ' + buttonIdDes + ', "' + metaData[i].columnName + '", -1); \'>&or;</button>'
				+ '</span>');
		}// end for listOfColumns
		htmlString += '</p>';

		htmlString += '</div>' + cr;
		htmlString += '<div id="t99Lookup" class="tblArea rcl-lkArea" style="min-width:' + totalAreaWidth + 'px;width:' + totalAreaWidth + 'px;">';
		htmlString += '<p id="t99Row" class="tblRow rcl-lkAreaRow" >';
		//##56f BEGIN
		/*
		htmlString+='<span class="rcl-lkAreaCell" style="flex-basis:50px;min-width:50px;max-width:50px;"><button id="t99SelectButton" class="'+buttonColorClass+'" onclick="rutSelectLookupEntry(\'t99Lookup-dlg\',\''+
					returnCols+'\',\''+returnIds+'\')">select</button></span>';
		*/
		htmlString += '<span class="rcl-lkAreaCell" style="flex-basis:' + selectColSize + 'px;min-width:' +
			selectColSize + 'px;max-width:' + selectColSize + 'px;"><button id="t99SelectButton" class="' + buttonColorClass + '" onclick="rutSelectLookupEntry(\'t99Lookup-dlg\',\'' +
			returnCols + '\',\'' + returnIds + '\')">select</button></span>';
		//##56f END
		for (var i = 0; i < metaData.length; i++) {
			tdStyleString = style = 'style="flex-basis:' + colSizes[i] + 'max-width:' + colSizes[i] + '"';
			if (0 <= '-5,4,5,-6,2,6,8'.indexOf(metaData[i].columnType)) {  //integer or decimal
				style = 'data-type="number" data-check="dec(' + metaData[i].precision + ',' + metaData[i].scale + ')" ' + tdStyleNumber;
			}
			else if (metaData[i].precision == 10) { //check for date
				for (var j = 0; j < Math.min(10, data.length); j++) { //check for data
					value = data[j][metaData[i].columnName];
					//if (value==null){ //##56a
					if (!value) { //##56a
					}
					else if ((value.length == 10) && (value.charAt(4) == '-') && (value.charAt(7) == '-')) {
						style = 'data-type="date" ' + tdStyleString;
						break;
					}
				}
			}
			else {
				style = tdStyleString;
			}
			if (metaData[i].columnName.substring(0, 5).toUpperCase() == 'HIDE_') {
				tdClass = 'class="tblField rcl-lkAreaCell ' + tdHideClass;
			}
			else {
				tdClass = 'class="tblField rcl-lkAreaCell"';
			}
			htmlString += ('<span id="t99' + metaData[i].columnName + '" ' + tdClass + ' ' + tdStyleString + '></span>');
		}// end for listOfColumns
		//htmlString+='</p></div>'; //##56f
		htmlString += '</p></div></div>'; //end table area //##56f
		if (data.length > 50) {
			htmlString += '<div id="t99paging" class="container-fluid" data-pageSize="50"></div>';
		}

		//htmlString+='</div></div>'; // end modal-body //##56f
		htmlString += '</div>'; //##56f
		$("body").append(htmlString);
		//The below makes RCL PowerTable study the template in the html and build a model
		tableSettings = tableInit("t99Lookup"); //Id of the container, we may have more than one table on the same page
		document.getElementById("t99Lookup" + tableSettings.configuration.sortDialogSearchFieldSuffix).addEventListener('keypress', function (e) {
			if (e.key === 'Enter') {
				_rptCallSearch("t99Lookup");
			}
		});
		//attaches data to RCL PowerTable
		//  rptAddData("t99Lookup",data);
		//diplays the data by copying the template as many times as the data require and fill it from the data.
		//  rptDisplayTable("t99Lookup");

		//BEGIN ###56e
		var maxDialogWidth = $(window).width() * 0.8;
		var dialogWidth = totalAreaWidth + 52;
		dialogWidth = Math.min(maxDialogWidth, Math.max(minDialogWidth, dialogWidth));
		//END ###56e

		$("#t99Lookup-dlg").dialog({
			autoOpen: false,
			modal: true,
			draggable: true,
			resizable: false,
			//width: "auto", //###56e
			width: dialogWidth, //###56e
			height: "auto",
			title: title,
			buttons: [
				{
					text: "Search",
					click: function () {
						_rptCallSearch(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Reset Search",     //####TO DO parameterisieren
					click: function () {
						rptResetSearch(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Reset Sort",
					click: function () {
						rptResetSortDefinitions(containerId);
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Export to csv",
					click: function () {
						rptSaveAsCsv("t99Lookup");
					},
					class: "rclLookupDlgBtn"
				},
				{
					text: "Cancel",
					click: function () {
						rutCloseDialog("t99Lookup-dlg");
					},
					class: "rclLookupDlgBtn"
				},
			],
			open: function (event, ui) {
				rptAddData("t99Lookup", data);//Change to avoid focus on firstpage without the dialog open
				rptDisplayTable("t99Lookup");//Change to avoid focus on firstpage without the dialog open

				$(this).css("overflow", "hidden");
			}, //##56f
			close: function (event, ui) { rutCloseLookupPopoverDialog('t99Lookup-dlg', 't99Lookup'); },// changed from t99Lookup-Dlg
		});
		$("#t99Lookup-dlg").dialog('open');
	}
}

function _QtnPrepareInputForGetLookupData(table, selectCols, selectValues, selectOperators, returnCols, forAutoLookup, selectOperatorsParam, selectColsOr) { //###64
	console.log("selectColsOr :: " + selectColsOr)
	//var sc=(selectCols)?selectCols.split(' '):[];
	//###64 BEGIN
	var selectProcessors = (selectCols) ? selectCols.split(' ') : []; //Store processors for each column
	var sc = [];
	for (var i = 0; i < selectProcessors.length; i++) {
		var processorIndex = selectProcessors[i].indexOf("*");
		if (processorIndex >= 0) {
			sc.push(selectProcessors[i].substring(0, processorIndex));
			selectProcessors[i] = _rutParseLookupSelectProcessor(selectProcessors[i].substring(processorIndex + 1));
		} else {
			sc.push(selectProcessors[i]);
			selectProcessors[i] = undefined;
		}
	}
	var scProcessor = null;
	//###64 END
	var sv = (selectValues) ? selectValues.split(' ') : [];
	var so = (selectOperators) ? selectOperators.split(' ') : [];
	var operatorparam = selectOperatorsParam;
	var columnor = selectColsOr;
	var select = [];
	var element = null;
	var value = null;
	var operator = null;
	var dataType = null;
	var check = null;
	var index = null;
	var tag = null;
	for (var i = 0; i < sv.length; i++) {
		if (sc[i] == '') { continue; }
		if (sv[i] == '') { continue; }
		//element=document.getElementById(sv[i]); //###56c
		element = document.getElementById(rutSplitRowID(sv[i])); //###56c
		value = (element == null) ? sv[i] : rutGetElementValue(element);
		dataType = null;
		//###64 BEGIN
		scProcessor = selectProcessors[i];
		if (scProcessor && scProcessor.min) {
			//Process min validation
			if (typeof value === "string" && value.length < scProcessor.min) {
				throw {
					code: "rut010-001",
					message: (element == null ? "Value" : _findElementLabel(element.id))
						+ " requires at least " + scProcessor.min + " character(s)"
				};
			}
		}
		//###64 END
		if (element != null) {
			if ((value == null) || (value == '')) { continue; }
			tag = element.nodeName;
			if (tag == 'INPUT') {
				dataType = element.type;
				//we may haved changed the type during init like for Chrome number to tel, then the original type is in data-type
				dataType = (element.getAttribute('data-type') == null) ? element.type : element.getAttribute('data-type');
				if (dataType == 'number') {
					check = element.getAttribute('data-check');
					if (check != null) {
						index = check.indexOf('dec(');
						if (index >= 0) {
							check = check.substring(index);
							dataType = check.split(')')[0];
						}//endif data-check has dec
					}//endif data-check exists
				}// endif type number
				//###60 BEGIN
				else if (dataType == 'date') {
					value = value.replace(/\-/g, "");
					value = "'" + value + "'";
				}
				//###60 END
				//###73 BEGIN
				else if (dataType == 'time') {
					value = rutConvertTime(value, false);
				}
				//###73 END
				else {
					value = "'" + value + "'";
				}
			}//endif input
			else {
				dataType = 'unknown';
				value = "'" + value + "'";
			}
		}
		//###64 BEGIN
		//operator=(i>=so.length)?'=': so[i];
		if (scProcessor && scProcessor.main) {
			//Set operator for main field
			operator = forAutoLookup ? "=" : "LIKE";
		} else {
			operator = (i >= so.length) ? '=' : so[i];
		}
		//###79 BEGIN
		if (operator.indexOf("*$p") >= 0) {
			operator = operator.replace(/\*\$p/g, " ");
		}
		//###79 END
		//###64 END
		if (dataType) {
			select.push({ column: sc[i], value: value, operator: operator, operatorparam: operatorparam, columnor: columnor, dataType: dataType })
		}
		else {
			select.push({ column: sc[i], value: value, operator: operator, operatorparam: operatorparam, columnor: columnor })
		}
		console.log("select :: " + JSON.stringify(select))
	}
	//###42 BEGIN
	var userData = {
		userToken: rutGetElementValue('h3-userToken')
		, userId: rutGetElementValue('h3-userId')
		, line: rutGetElementValue('h3-line')
		, trade: rutGetElementValue('h3-trade')
		, agent: rutGetElementValue('h3-agent')
		, fscCode: rutGetElementValue('h3-fscCode')
	}
	var userToken = userData.userToken; //temporary until Nuttapol has fixed it
	//###42 END
	/* This is the JSON structure for the webservice
	{   userData : {}},
		table: <tbl>,
		{
			select: [{ column: <selectColumn>, value: <selectValue>, operator: <selectOperator>}]
		},
	}
	*/
	//var wsInput = { table: table, //###42

	var wsInput = { userToken: userToken, userData: userData, table: table }; //###42

	if (select.length > 0) {
		wsInput.select = select;
	}
	//##60 BEGIN
	if (returnCols) {
		var processor = [];
		var rco = returnCols.split(' ');
		for (var i = 0; i < rco.length; i++) {
			//Processor exists
			var processorIndex = rco[i].indexOf("*");
			if (processorIndex >= 0) {
				var processorType = rco[i].substring(processorIndex + 1);
				processor.push({
					column: rco[i].substring(0, processorIndex),
					type: processorType
				});
			}
		}
		if (processor.length > 0) {
			wsInput.processor = processor;
		}
	}
	//##60 END
	return wsInput;
}

function Qtn_GetLookupData(lookupRequest, callbackFunction) {
	var url = baseurl + 'QTNWSWebApp/rclws/help/qntGenericHelp';

	//###60 BEGIN
	var processor = lookupRequest.processor;
	if (processor) {
		delete lookupRequest.processor;
	}

	//###60 END
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		data: JSON.stringify(lookupRequest),
		complete: function () { $("body").find('.loading').remove(); }, //###65
		//success: callbackFunction, //###60
		//###60 BEGIN
		success: function (response, status, jqxhr) {
			if (processor) {
				var data = response.data;
				var metadata = response.metadata;
				//Process each field in returned data
				if (data) {
					for (var i = 0; i < data.length; i++) {
						for (var j = 0; j < processor.length; j++) {
							if (data[i].hasOwnProperty(processor[j].column)) {
								//Process depends on type
								if (processor[j].type.indexOf('d') >= 0) {
									var col = processor[j].column;
									var dateStr = data[i][col] + "";
									data[i][col] = dateStr.substring(0, 4) + "-" +
										dateStr.substring(4, 6) + "-" + dateStr.substring(6, 8);
								}
							}
						}
					}
				}
				//Process metadata
				if (metadata) {
					for (var i = 0; i < metadata.length; i++) {
						for (var j = 0; j < processor.length; j++) {
							if (metadata[i].columnName == processor[j].column) {
								//Process depends on type
								if (processor[j].type.indexOf('d') >= 0) {
									//Framework check precision == 10 for date
									metadata[i].precision = 10;
								}
								//##63 BEGIN
								if (processor[j].type.indexOf('h') >= 0) {
									metadata[i].columnName = "HIDE_" + metadata[i].columnName;
								}
								//##63 END
							}
						}
					}
				}
			}
			callbackFunction(response, status, jqxhr);
		},
		//###60 END
		error: function (result) {
			rutOpenMessageBox("Lookup Error", result.responseText, 'rut004-' + result.readyState, null, ''); //###50
		}
	});
}

function qnt_GetDataAreaToObject(area) {
	var obj = {};
	var div = document.getElementById(area);

	$(div).find('input:text, input:password, input:file, input[type="date"], input[data-type="date"], input:radio, input:checkbox, select, textarea')
		.each(function () {
			switch ($(this).attr("type")) {
				case 'checkbox':
					obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).is(":checked") ? "Y" : "N";
					break;
				case 'radio':
					obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).is(":checked");
					break;
				case 'date':
					obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = qtn_ConvertDateFormat($(this).val());
					break;
				default:
					if ($(this).attr("data-type") == 'date') {
						obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = qtn_ConvertDateFormat($(this).val());
					} else {
						obj[$(this).attr('id').substring(3, $(this).attr('id').length)] = $(this).val();
					}
					break;
			}

		});
	return obj;
}

function qtn_ConvertDateFormat(date) {
	if (!date)
		return "";

	else {

		/* var d = date.split('/')[2];
		 var m = date.split('/')[1];
		 var y = date.split('/')[0];*/
		return date.split('/').reverse().join('');
	}
}

function qnt_GetDataSetToObject(data) {

}

function defaultField(id, value, valueset) {
	if (value == "") {
		$('#' + id).val(valueset);
	}


}

//-----------------------Start   Set  display PCL---------------------//

function displayProductCatalog_New(response) {
	response = ISSetDataRountig(response);
	console.log(JSON.stringify(response));
	productcatList = response;
	productcatListhil = response;
	productcatPointer = -1;

	var html = "";
	for (i = 0; i < response.length; i++) {


		var stackWaitingTime = 0;
		var stackTransitTime = 0;
		var stackTotalTime = 0;

		if(response[i].hilight){
				
				html += '<div class="tblRow border highlighted">';
			}else{
				html += '<div class="tblRow border">';
			}
			

			for (j = 0; j < response[i].length; j++) {

				var setclass = '';
				if (response[i].length > 1 && j != response[i].length - 1) {

					//setclass = 'border-bottom: 1px solid #dee2e6 !important;';

				}

				
					html += '<div class="row"  style="margin-left: 0px;">';



			switch (j) {
				case 0:
					html += '<div class="col-sm-1 col-md-1 pb-0 pl-1 pr-2" style="text-align: center;">';
					html += '<div class="rcl-standard-input-wrapper pt-1">'
					html += '<input name="SelectChangeRouting" type="radio" class="rcl-standard-form-control" placeholder="" onchange="onSelectChangeRouting(' + i + ')">';
					html += '</div>';
					html += '</div>';
					break;
				default:
					html += '<div class=" col-md-1">';
					html += '</div>';
					break;
			}
			html += writeProductCatalogueRouteList_New(response[i][j].fromLoc);

			html += writeProductCatalogueRouteList_New(response[i][j].fromTerminal);

			html += writeProductCatalogueRouteList_New(response[i][j].fromLocTp, "InlandLocationType");

			html += writeProductCatalogueRouteList_New(response[i][j].toLoc);

			html += writeProductCatalogueRouteList_New(response[i][j].toTerminal);

			html += writeProductCatalogueRouteList_New(response[i][j].toLocTp, "InlandLocationType");

			//html += writeProductCatalogueRouteList_New((response[i][j].transportationMode.toUpperCase()) == 'V' ? 'M' : response[i][j].transportationMode, "ModeOfTransport");
			html += writeProductCatalogueRouteList_New((response[i][j].transportationMode.toUpperCase()) == 'V' ? 'M' : response[i][j].transportationMode, "ModeOfTransport");
			
			var waitingTime = parseFloat(response[i][j].waitingTime);
			stackWaitingTime += waitingTime;

			html += writeProductCatalogueRouteList_New(waitingTime.toFixed(2));

			var transitTime = parseFloat(response[i][j].transitTime);
			stackTransitTime += transitTime;

			html += writeProductCatalogueRouteList_New(transitTime.toFixed(2));

			var totalTime = waitingTime + transitTime;
			stackTotalTime += totalTime;

			html += writeProductCatalogueRouteList_New(totalTime.toFixed(2));

			if (j == 0) {
				html += writeButtonPlusFunction_New();
			}
			else {
				html += writeDummyCol_New();
			}

			html += writeDummyCol_New();

			html += writeProductCatalogueRouteList_New(response[i][j].service);

			html += writeProductCatalogueRouteList_New(response[i][j].vessel);

			html += writeProductCatalogueRouteList_New(response[i][j].voyage);

			html += writeProductCatalogueRouteList_New(response[i][j].direction, "Direction");

			html += writeProductCatalogueRouteList_New(response[i][j].transportCarrierDrayage);
			
			html += writeProductCatalogueRouteList_New(response[i][j].etd);
			
			html += writeProductCatalogueRouteList_New(response[i][j].eta);

			html += writeButtonPlusFunction_New();

			html += writeButtonPlusFunction_New();

			/*	//Dummy Sapce Row 2
				for (k = 0; k <= 6; k++) {
					html += writeDummyCol();
				}

				html += writeProductCatalogueRouteList(response[i][j].eta);

				html += writeProductCatalogueRouteList(response[i][j].etd);

				html += writeButtonPlusFunction();

				html += writeButtonPlusFunction();*/

			//Dummy Sapce Row 3
		/*	for (k = 0; k <= 6; k++) {
				html += writeDummyCol_New();
			}*/
			html += '</div>';
		}

		//Total By Row At End of Row
			html += '<div class="row"  style="margin-left: 0px;">';
			html += '<div class="col-md-1 mt-1 mb-1  pl-2 pr-0 offset-md-7 font-weight-bold">';
			html += '<label class="rcl-std">Total</label>';
			html += '</div>';
			html += '<div class="col-md-1 mt-1 mb-1  pl-2 pr-0 font-weight-bold">';
			html += '<label class="rcl-std">' + stackWaitingTime.toFixed(2)
					+ '</label>';
			html += '</div>';
			html += '<div class="col-md-1 mt-1 mb-1  pl-2 pr-0 font-weight-bold">';
			html += '<label class="rcl-std">' + stackTransitTime.toFixed(2)
					+ '</label>';
			html += '</div>';
			html += '<div class="col-md-1 mt-1 mb-1  pl-2 pr-0 font-weight-bold">';
			html += '<label class="rcl-std">' + stackTotalTime.toFixed(2)
					+ '</label>';
			html += '</div>';
			html += '</div>';
			html += '</div>';

			//End Row
			html += '</div>';
	}

	$('#t42RoutingDetails').html(html);

	function writeButtonPlusFunction_New(funcname, pointer) {
		var output = "";
			output += '<div class="col-md-1" style="text-align: center;">';
			output += '<div style="margin-top: 5px;"><i class="fa fa-plus " aria-hidden="true"'
					+ (funcname ? 'onclick="' + funcname + '(""' + pointer
							+ '")"' : '') + '></i></div>';
			output += '</div>';
			return output;
	}

	function writeDummyCol_New() {
		return '<div class=" col-md-1"></div>'
	}

	function writeProductCatalogueRouteList_New(value, mapSelectTable) {
		   if(mapSelectTable){
				value = mapValueToSelectTable(value, mapSelectTable);
			}

			var setvalue = (value != null) ? value : "";
			var output = "";
			output += '<div class=" col-md-1 mt-1 mb-1  pl-2 pr-0 ">';
			
			output += '<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField classreadonly" value="' + setvalue + '" readonly="" data-ct="bt-text">';
			
			output += '</div>';

			return output;
	}

	function mapValueToSelectTable(value, mapSelectTable){
			if(value){
				if(mapSelectTable == "Direction"){
					switch(value){
					case 'W':
						value = 'West';
						break;
					case 'N':
						value = 'North';
						break;
					case 'S':
						value = 'South';
						break;
					case 'E':
						value = 'East';
						break;
					case 'NW':
						value = 'North West';
						break;
					case 'NE':
						value = 'North East';
						break;
					case 'SW':
						value = 'South West';
						break;
					case 'SE':
						value = 'South East';
						break;
					default:
						break;
					}
				}
				else if(mapSelectTable == "InlandLocationType"){
					switch(value){
					case 'O':
						value = 'Port';
						break;
					case 'Y':
						value = 'Depot';
						break;
					case 'H':
						value = 'Haul.Loc';
						break;
					case 'P':
						value = 'Terminal';
						break;
					case 'D':
						value = 'Door';
						break;
					}
				}
				else if(mapSelectTable == "ModeOfTransport"){
					switch(value){
					case 'B':
						value = 'Barge';
						break;
					case 'F':
						value = 'Feeder';
						break;
					case 'M':
						value = 'Veseel';
						break;
					case 'R':
						value = 'Rail';
						break;
					case 'T':
						value = 'Truck';
						break;
					default:
						break;
					}
				}
			}
			
			return value;
		}
}

//-----------------------End   Set  display PCL---------------------//



//------------Start Set Data Rounting---------------//

function ISSetDataRountig(response) {
	//New set array object array objct  of  array.[ [ {},{} ] ]
	var dataRetrun = [];
	var result = groupBy(response, function (item) {
		//return [item.lastname, item.age];
		return [item.routingId];
	});
	//New  set field rename key 
	var Newfield = ['transportationMode', 'fromLoc', 'toLoc', 'transportCarrierDrayage', 'fromLocTp', 'toLocTp'];
	var Oldfield = ['mot', 'fromLocation', 'toLocation', 'transportCarrier', 'fromLocationType', 'toLocationType'];
	for (i = 0; i < result.length; i++) {
		dataRetrun.push(rename(result[i], Newfield, Oldfield));
	}

	return dataRetrun;
}

function rename(dataIn, Newfield, Oldfield) {
	var output = [];
	output = dataIn.map(function (obj) {
		for (x = 0; x < Newfield.length; x++) {
			obj[Newfield[x]] = obj[Oldfield[x]]; // Assign new key 
			delete obj[Oldfield[x]]; // Delete old key 
		}
		return obj;
	});
	//setrowID
	HanderGetDataRowId(output);

	return output;
}

function groupBy(array, f) {
	var groups = {};
	array.forEach(function (o) {
		var group = JSON.stringify(f(o));
		groups[group] = groups[group] || [];
		groups[group].push(o);
	});
	return Object.keys(groups).map(function (group) {
		return groups[group];
	})
}

function HanderGetDataRowId(ArrayList) {
	var allDataRowid = ArrayList;
	var i = 1;
	allDataRowid.forEach(function (rowlist) {
		output = rowlist;
		output.rowNo = i;
		output.transportationMode = (output.transportationMode == 'V') ? 'M' : output.transportationMode;
		console.log(output.transportationMode)
		i++;
	});
}

//------------End Set Data Rounting---------------//

function conPclDate(getDate) {
	var NewDate = "";
	var reg = /[.,\/ -]/;
	var getstr = "";

	if (getDate != undefined) {
		
		if(getDate.length != undefined){
			if (getDate.charAt(2) == '/') {
				NewDate = getDate.split(reg).reverse().join('-');

			} else if (getDate.charAt(4) == '-') {
				NewDate = getDate;

			} else {
				NewDate = getDate.substring(0, 4) + "-" + getDate.substring(4, 6) + "-" + getDate.substring(6, 8);
			}
		}else{
			
			getstr = getDate.toString();
			NewDate = getstr.substring(0, 4) + "-" + getstr.substring(4, 6) + "-" + getstr.substring(6, 8);
		}
			

	}
	return NewDate;
}

