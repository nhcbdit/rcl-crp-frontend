//var baseurl = "http://localhost:8080/";
//var baseurl = "http://10.2.6.61:8080/QTNWSWebApp/rclws/";
//var baseurl = "http://marlin-ce.rclgroup.com:8080/";
var baseurl = "http://" + location.hostname + ":8080/";
//var baseurl = "http://marlin-ce.rclgroup.com:8080/";


var equipmentAvlBaseUrl = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/EMSMoveCorrectionServ?servaction=EquipmentAvailabilityInquiryFind";
var productCatalogueBaseUrl = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/MiProdCatalogServ?";
var ediBaseUrl = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/EDITransactionEnquiryServ?";
var sealinerUrl = "http://zebra-cl.rclgroup.com/SealinerRCL/rest/webServiceSealiner/";

/**
 * Call API using Ajax. Result will be handled automatically if flag is set.
 * If handled automatically,
 * If Ajax call error: return { $_success:false, error:jqXHR }
 * Otherwise: return response object with $_success added indicating if the API call is success or not    
 * @param name API name
 * @param data Data to pass to API
 * @param autoHandleResult Whether result should be handled here or not 
 * @returns Ajax object
 */
function getResultAjax(name, data, autoHandleResult) { 
	if(autoHandleResult === undefined){
		autoHandleResult = true;
	}
	
	var url = "";
	switch (name) {
		default:
			url = "";
			break;		
		}
	
		$( "body" ).append('<div class="loading"></div>');
		
		if(!autoHandleResult){
			return AjaxPost(url, data); 
		}else{
			return AjaxPost(url, data).done(handleAjaxResponse).fail(handleAjaxError).always(
					function (){
				    	$("body").find('.loading').remove();
				    });
		}
	
	
}

/**
 * Call multiple APIs using Ajax. Fail result will be handled automatically.
 * Response will be returned when all APIs return. Response will be evaluated in order.
 * If a single Ajax error occurs, the call terminates with response { $_success:false, error:jqXHR }.
 * If a single API error occurs, return { $_success:false, index:(index of failed call), result:(fail response) }.
 * If all pass, responses are returned { $_success:true, result:[response array in call order] }.
 * @param ajaxCalls Multiple ajax calls. Provide as arguments (not array) 
 * @returns Ajax object
 */
function getResultMultipleAjax(ajaxCalls){
	return $.when.apply($, arguments).then(function(ajaxResults){
		var responses = [];
		for(var i=0; i<arguments.length; i++){
			var resultArray = arguments[i];
			if(resultArray[2].status === 200){
				var responseResult = handleAjaxResponse(resultArray[0]);
				if(!responseResult.$_success){
					return { $_success:false, index:i, result:responseResult };
				}else{
					responses.push(responseResult);
				}
			}else{
				return handleAjaxError(resultArray[2]);
			}
		}
		return { $_success:true, result:responses };
	}).always(function (){
    	$("body").find('.loading').remove();
    });
}

function handleAjaxResponse(response){
	//alert("handleAjaxResponse : "+response);
	if(!response){
		response = { $_success: false };
	}
	else if (!response.Success){
		if(response.Success == false){
			if(response.Content =='d'){
				dialogFadeout('Save successful.');
			}else if(response.Method == 'Save'){
				dialogFadeout(response.Content);
			}else if(response.Content.indexOf("ORA-") >= 0){
				dialogFadeout(response.Content);
			}else{
				dialogFadeout("Success = false");
			}
		}
		response.$_success = false;
	}else{
		response.$_success = true;
	}
    return response;
}

function handleAjaxError(error){
	if(error.statusText =="Internal Server Error")
		dialogFadeout("Internal Server Error");
	else
	{
		//$(".nav-tabs a[href='#p0-" + data.tab + "']").tab("show");
		dialogFadeout(error.responseText);
	}
    return { $_success: false };
}

//Url for authenticating with WS, auth is required so that ws can access the same user object as in web app
//This is due to web and ws running within different context on local environment
var tmpAuthUrl = "http://localhost:8080/BKGWSWebApp/RrcGenericSrv?service=common.RcmLoginSvc&select=tmp.AuthLandingSvc";

/*
 * Temp function to retrieve data from ws. The function does the same as getResultAjax() but also automatically authenticate
 * with ws if session expire
 */
function getResultAjaxWithTmpAuth(name, data){	
	if(window.localStorage.getItem('auth')){
		//Do web service
		return getResultAjax(name, data).then(function(response){
			var authFailed = !response.Success && response.Auth; 
			if(authFailed){
				//Auth failed, authenticate first then call ws again
				window.localStorage.setItem('auth', false);
				return tmpAuthAndDoWS(name, data);
			}else{
				//Invoke registered callback
				
				return response;
			}
		}).fail(function(error){
			return error;
		});
	}else{
		//Auth then do web service
		return tmpAuthAndDoWS(name, data);
	}
}

function tmpAuthAndDoWS(name, data){
	//Auth
	return $.ajax({
		url: tmpAuthUrl,
		method: "GET",
		cache:false
	}).then(function(response, text, jqxhr){
		if(jqxhr.status != 200){
			//Auth fail
			window.localStorage.setItem('auth', false);
			return { success:false, message:"Session expired", auth:true };
		}else{
			//Auth success
			window.localStorage.setItem('auth', true);
			return getResultAjax(name, data);
		}
	}).fail(function(error){
		return { success:false, message:""+error, auth:true };
	});
}

function getResultAjaxSealiner(name, data){
	var url = "";
	switch (name) {
		case "ProductCatalogueRouteList":
			url = "getProductCatalogueRouteList";
			break;
		default:
			url = "";
			break;		
	}
	

	return AjaxGet(url, data).done(function (response) {

		if (response.errorCode){
			dialogFadeout(response.errorDescription);
		}
		
        return response;
        
    }).fail(handleAjaxError);
}

function AjaxPost(url, data){
	return  $.ajax({
    url:baseurl + url,
    method:"POST",
    cache:false,
    contentType: "application/json",
    data: JSON.stringify(data),
    dataType: "json"
	});
}

function AjaxGet(url, data){
	return  $.ajax({
    url: sealinerUrl + url + data,
    method:"GET",
	});
}
	
	
	
