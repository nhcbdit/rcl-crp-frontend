<!--
---------------------------------------------------------------------------------------------------------
inquiry.jsp
---------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
---------------------------------------------------------------------------------------------------------
Author Nuttapol Thanasrisatit 07/05/2013
- Change Log---------------------------------------------------------------------------------------------
## 		DD/MM/YY 		-User- 		-TaskRef-       -ShortDescription
01 		06/09/18 		Nuttapol               		Create this screen
---------------------------------------------------------------------------------------------------------
-->

<%@ taglib prefix = "rcl" uri = "/WEB-INF/custom.tld"%>

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="com.rclgroup.dolphin.web.common.RcmConstant"%>

<!DOCTYPE html>

<%@ page pageEncoding="UTF-8"%>

<html lang="en">
<head>

<title>Pilot Screen</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../include/tab_js.jsp" %>
<%@include file="../include/datatable_js.jsp" %>
<%@include file="../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../css/rcl.css">

<script type="text/javascript">
    $(function () {
        $('#invoice_start_date').datetimepicker({
			format: 'DD/MM/YYYY',
			todayHighlight: true,
			autoclose: true
        });
        $('#invoice_end_date').datetimepicker({
            format: 'DD/MM/YYYY',
			useCurrent: false,
			todayHighlight: true,
			autoclose: true
        });
        $("#invoice_start_date").on("change.datetimepicker", function (e) {
            $('#invoice_end_date').datetimepicker('minDate', e.date);
        });
        $("#invoice_end_date").on("change.datetimepicker", function (e) {
            $('#invoice_start_date').datetimepicker('maxDate', e.date);
        });
    });
</script>



<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>

<script>
function getPort(){
	openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmPortHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm&retName=txtPort');
}

function getVessel() {

	openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmVesselScheduleHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=txtVessel');
}
</script>

</head>
<body>
	<div class="rcl-standard-page-header">
		<label>Header 1</label>
	</div>

	<div class="rcl-standard-navigation-bar rcl-standard-font">
		<div class="navbar navbar-light bg-faded navbar-expand-md">
			<ul class="nav navbar-nav">

				<a class="nav-item nav-link active rcl-standard-navigation-bar-tab" data-toggle="tab" href="#tab1">Inquiry</a>

				<a class="nav-item nav-link rcl-standard-navigation-bar-tab" data-toggle="tab" href="#tab2">Maintenance</a>


				<!--
				<a class="nav-item nav-link rcl-standard-navigation-bar-tab" data-toggle="tab" href="#tab3">Tab 3</a>
				<a class="nav-item nav-link rcl-standard-navigation-bar-tab" data-toggle="tab" href="#tab4">Custom Tag from Ascan</a>
				 -->
			</ul>
	  	</div>

	  	<div class="tab-content">

			<div class="tab-pane active" id="tab1"><%@include file="tab1.jsp" %></div>
			<div class="tab-pane" id="tab2"><%@include file="tab2.jsp"%></div>

			<!--
			<div class="tab-pane" id="tab3"><!--%@include file="tab3.jsp" %></div>
			<div class="tab-pane" id="tab4"><!--%@include file="tab4.jsp" %></div>
			 -->
	  	</div>
	</div>
	<!-- navigation bar -->


	<div class="rcl-standard-page-footer rcl-standard-font" >
		<label>Footer</label>
	</div>



</body>
</html>