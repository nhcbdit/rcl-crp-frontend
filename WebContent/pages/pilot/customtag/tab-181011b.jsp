<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181011b</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<body>
<div class="Container">
	<rcl:tabGroup tabList="General Eq_Detail Charges" activeTab="Eq_Details">
		<div class="tab-pane" id="p0-General">
			12345 General
		</div> <!-- end content pane for General -->
		<!-- Content pane Eq_Detail Start -->
		<div class="tab-pane active " id="p0-Eq_Details">
			67890 Eq_Details
		</div> <!-- end content pane for Eq_Detail -->
	</rcl:tabGroup>
</div>

</body>
</html>