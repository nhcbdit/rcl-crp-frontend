<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181015d</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<body>



<div class="container">
    <div class="row">
        <rcl:stdVessel id="s0-vessel" classes="div(col-sm-2 pb-0)" check="req" defaultValue="MATU" options="onchange=&quot;alert('options issue');&quot;" />
        <rcl:stdShipmentType id="s0-shipmentType" classes="div(col-sm-2 pb-0)" />
        <rcl:stdDirection id="s0-direction" classes="div(col-sm-2 pb-0)"/>
        <rcl:stdVessel id="s0-vessel2" label="Mother Vessel" classes="div(col-sm-2 pb-0)" check="dis"/>
    </div> <!-- end row-->
</div><!-- end container -->


</body>
</html>
