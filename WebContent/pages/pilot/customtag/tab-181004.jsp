<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181004</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<script>

function show() {

	var param = { exp_imp: "E", vessel: "XTB", voyage: "1101W", port: null, coc_soc: "C", invoice_date_from: null, invoice_date_to: null, invoice_type: null, invoice: null, bl_no: "BKKCB18003597", transaction_type: null };

	var form = document.getElementById('frm');
	var isValidForm = form.checkValidity();

	if(!isValidForm) {

		/*
		var fields = document.getElementsByTagName('INPUT');

	    for(var i=0; i < fields.length; i++) {
	      if(fields[i].hasAttribute('placeholder')) {
	        fields[i].defaultValue = fields[i].getAttribute('placeholder');
	        fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
	        fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
	      }
	    }
		*/

		return false;
	}

	$(".hidden").show();

	$.ajax({
		async: true,
		url: 'http://marlin-ce.rclgroup.com:8080/TSTWSWebApp/rclws/wspilot/getDataPilot',
    	type: 'POST',
    	dataType: 'json',
    	data: param,
    	success: function (dataList) {

			var dataSet = [];
			for(var i=0;i<dataList.length;i++) {
				dataSet[i] = [dataList[i]['COM_CODE']
							,dataList[i]['PORT_CALL']
							,dataList[i]['COLL_FSC']
							,dataList[i]['INVOICE_NO']
							,dataList[i]['INVOICE_TYPE']
							,dataList[i]['INVOICE_STATUS']
							,dataList[i]['BILL_TO_PARTY_CODE']
							,dataList[i]['BILL_TO_PARTY_NAME']
							,dataList[i]['MAIN_CURRENCY']
							,dataList[i]['NET_AMT_MAIN']
							,dataList[i]['EXPORT_IMPORT']
							]
			}

			$(document).ready(function() {
				$('#list').DataTable( {
					fixedHeader: true,
					data: dataSet,
					columns: [
						{ title: "Com Code" },
						{ title: "Port Call" },
						{ title: "Collection FSC" },
						{ title: "Invoice No" },
						{ title: "Invoice Type" },
						{ title: "Invoice Status" },
						{ title: "Customer Code" },
						{ title: "Customer Names" },
						{ title: "Main Currency" },
						{ title: "Net Amount Main" },
						{ title: "Export Import" }
					],
					responsive: {
						details: {
							display: $.fn.dataTable.Responsive.display.modal( {
								header: function ( row ) {
									var data = row.data();
									return 'Invoice Detail for '+data[2];
								}
							} ),
							//renderer: $.fn.dataTable.Responsive.renderer.tableAll({tableClass: 'table'})
						}
					}
				} );
				//$('.close').click();

				$(".hidden").hide();
			} );

    	},
		error: function (result) {
			alert('error');

		},
		complete: function () {


		}


	});


}
//###ADDED BY ASCAN
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();
    });
	function rutOpenLookupTable(table, option, returnVariable){
		alert('Attempting to run: rutOpenLookupTable('+table+","+option+","+returnVariable+')');
	}
//###ADDED BY ASCAN
</script>

<body>
<form id="frm" name="frm" class="needs-validation" novalidate>
	<div class="container-fluid">
<!--###ADDED BY ASCAN   BEGIN-->
		<div class="row">
            <!-- options with onchange event and quotes -->
            <rcl:searchArea id="s1-area" title="Booking Search" classes="div(areaCls) btn(btnCls) row(rowCls)">
				<div class="container">
					<div class="row">
						<rcl:text id="s0-port" label="Port" classes="div(col-sm-2 pb-0) lbl(labelClass)" check="req upc len(5)" defaultValue="THBKK" options="onchange=&quot;alert('options issue');&quot;" />
			            <!-- Micro Help and Lookup -->
            			<rcl:text id="s0-Vessel" label="Vessel" classes="div(col-sm-2 pb-0)" microHelp="A ship of the line" lookup="tbl(Vessel) rco(Customer) rid(s0-Vessel)" />
						<rcl:date id="s0-acc" classes="div(col-sm-2 pb-0)" label="Acceptance" defaultValue="+4" />
						<rcl:date id="s0-eta" classes="div(col-sm-2 pb-0)" label="ETD" defaultValue="lastOf next month" />
						<rcl:number id="s0-Amount" label="Amount" classes="div(col-sm-2 pb-0)" check="req dec(7,2) min(0)" />
						<!-- Select optional -->
						<rcl:select id="s0-size" label="Customer" selectTable="CustomerType" classes="div(col-sm-2 pb-0)" options="novalidate"/>
						<!-- Select mandatory -->
						<rcl:select id="s0-type" label="Type" optionsList="20FT 40FT HC" valueList="20 40 45" defaultValue="40" classes="div(col-sm-2 pb-0)" check="req"/>
					</div>
				</div>
            </rcl:searchArea>
        </div>
<!--###ADDED BY ASCAN  END-->

	</div>

</form>

</body>
</html>

