<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181029-01</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<form id="frm" name="frm" class="needs-validation" novalidate>
	<rcl:area id="s1-area" title="Booking Search" classes="div(areaCls) btn(btnCls) row(rowCls)" collapsible="Y">
		<div class="container">
			<div class="row">
				<rcl:text id="s0-port" label="Port" classes="div(col-sm-2)" check="req upc len(5)" defaultValue="THBKK" options="onchange=&quot;alert('options issue');&quot;" />
				<!-- Micro Help and Lookup -->
				<rcl:text id="s0-Vessel" label="Vessel" check="dis" classes="div(col-sm-2 pb-0)" microHelp="A ship of the line" lookup="tbl(Vessel) rco(Customer) rid(s0-Vessel)" />
				<rcl:date id="t0-acc" classes="div(col-sm-2)" label="Acceptance" defaultValue="+4" />
				<rcl:date id="d0-eta" classes="div(col-sm-2)" label="ETD" defaultValue="lastOf next month" />
				<rcl:time id="d0-etaTime" classes="div(col-sm-2)" label="ETA Time" />
			</div> <!-- end row-->
			<div class="row">
				<rcl:number id="s0-Amount" label="Amount" classes="div(col-sm-2)" check="req dec(7,2) min(0)" />
				<!-- Select mandatory -->
				<rcl:select id="s0-size" label="Customer" selectTable="CustomerType" classes="div(col-sm-2)" check="req"/>
				<!-- Select optional emptyOption -->
				<rcl:select id="s0-type" label="Type" optionsList="20_FT 40_FT HC" valueList="20 40 45" emptyDisplay="Select one" classes="div(col-sm-2"/>
			</div><!-- end row-->
			<div class="row">
				<rcl:email id="s0-email" label="Email" classes="div(col-sm-2)" check="req" />
				<!-- Select mandatory -->
				<rcl:tel id="s0-fax" label="Fax" classes="div(col-sm-2)" check="req"/>
				<!-- Select optional emptyOption -->
				<rcl:url id="s0-webpage" label="Web" classes="div(col-sm-2"/>
			</div><!-- end row-->
		</div><!-- end container -->
	</rcl:area>
</form>

</body>
</html>