<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181026-02</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<body>


<form id="frm" name="frm" class="needs-validation" >
	<rcl:area id="s1-area" title="Booking Search" classes="div(areaCls) btn(btnCls) row(rowCls)" useSearch="Y" collapsible="Y" newBtn="Y">
		<div class="container">
			<div class="row">
				<rcl:stdService id="s0-service" classes="div(col-sm-2)" check="req" />
				<rcl:stdVessel id="s0-vessel" check="req" classes="div(col-sm-2)" />
				<rcl:stdVoyage id="t0-voyage" classes="div(col-sm-2)"  />
				<rcl:stdDirection id="d0-direction" classes="div(col-sm-2)"/>
				<rcl:stdPort id="d0-pol" classes="div(col-sm-2)" label="POL" />
			</div> <!-- end row-->
			<div class="row">
				<rcl:stdPoint id="s0-plr" label="Place of Receipt" classes="div(col-sm-2)"/>
			</div><!-- end row-->
		</div><!-- end container -->
	</rcl:area>
</form>

</body>
</html>
