<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181010</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<script>

	function show() {
	}

</script>

<body>
<form id="frm" name="frm" class="needs-validation" novalidate>
	<rcl:searchArea id="s1-area" title="Booking Search" classes="div(areaCls) btn(btnCls) row(rowCls)">
		<div class="container">
			<div class="row">
				<rcl:text id="s0-port" label="Port" classes="div(col-sm-2 pb-0) lbl(labelClass)" check="req upc len(5)" defaultValue="THBKK" options="onchange=&quot;alert('options issue');&quot;" />
				<!-- Micro Help and Lookup -->
				<rcl:text id="s0-Vessel" label="Vessel" classes="div(col-sm-2 pb-0)" microHelp="A ship of the line" lookup="tbl(Vessel) rco(Customer) rid(s0-Vessel)" />
				<rcl:date id="s0-acc" classes="div(col-sm-2 pb-0)" label="Acceptance" defaultValue="+4" />
				<rcl:date id="s0-eta" classes="div(col-sm-2 pb-0)" label="ETD" defaultValue="lastOf next month" />
			</div> <!-- end row-->
			<div class="row">
				<rcl:number id="s0-Amount" label="Amount" classes="div(col-sm-2 pb-0)" check="req dec(7,2) min(0)" />
				<!-- Select optional -->
				<rcl:select id="s0-size" label="Customer" selectTable="CustomerType" classes="div(col-sm-2 pb-0)" options="novalidate"/>
				<!-- Select mandatory -->
				<rcl:select id="s0-type" label="Type" optionsList="20FT 40FT HC" valueList="20 40 45" defaultValue="40" classes="div(col-sm-2 pb-0)" check="req"/>
			</div><!-- end row-->
		</div><!-- end container -->
	</rcl:searchArea>
</form>
</body>
</html>
