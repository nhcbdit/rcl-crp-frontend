<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>tab-181102-01</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<form id="frm" name="frm" class="needs-validation" >
<button id="b0-btn" type="button" onclick="rutOpenDialog('b0-area');">Open Dialog</button>
<rcl:dialog id="b0-area" draggable="true" resizable="false" width="400" minWidth="200" maxHeight="200"
            title="Test dialogs" buttonList="ok" onClickList="alert('ok pressed');">
	<div class="container">
		<div class="row dtlArea">
			<rcl:text id="d0-port" label="Port" classes="div(col-sm-2 pb-0) lbl(labelClass)" check="req upc len(5)" defaultValue="THBKK" options="onchange=&quot;alert('options issue');&quot;" />
			<!-- Micro Help and Lookup -->
			<rcl:tel id="d0-phone" label="Phone" classes="div(col-sm-2 pb-0)"  />
			<rcl:date id="d0-etd" classes="div(col-sm-2 pb-0)" label="ETD" defaultValue="+4" />
			<rcl:date id="d0-eta" classes="div(col-sm-2 pb-0)" label="ETA" defaultValue="lastOf next month" />
		</div> <!-- end row-->
	</div><!-- end container -->
</rcl:dialog>

</form>

</body>
</html>


