<%@ taglib prefix = "rcl" uri = "../../../WEB-INF/custom.tld"%>

<html lang="en">
<head>

<title>Tab 2</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../../include/tab_js.jsp" %>
<%@include file="../../include/datatable_js.jsp" %>
<%@include file="../../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../../css/rcl.css">

<script>

function show() {

	var param = { exp_imp: "E", vessel: "XTB", voyage: "1101W", port: null, coc_soc: "C", invoice_date_from: null, invoice_date_to: null, invoice_type: null, invoice: null, bl_no: "BKKCB18003597", transaction_type: null };

	var form = document.getElementById('frm');
	var isValidForm = form.checkValidity();

	if(!isValidForm) {

		/*
		var fields = document.getElementsByTagName('INPUT');

	    for(var i=0; i < fields.length; i++) {
	      if(fields[i].hasAttribute('placeholder')) {
	        fields[i].defaultValue = fields[i].getAttribute('placeholder');
	        fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
	        fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
	      }
	    }
		*/

		return false;
	}

	$(".hidden").show();

	$.ajax({
		async: true,
		url: 'http://marlin-ce.rclgroup.com:8080/TSTWSWebApp/rclws/wspilot/getDataPilot',
    	type: 'POST',
    	dataType: 'json',
    	data: param,
    	success: function (dataList) {

			var dataSet = [];
			for(var i=0;i<dataList.length;i++) {
				dataSet[i] = [dataList[i]['COM_CODE']
							,dataList[i]['PORT_CALL']
							,dataList[i]['COLL_FSC']
							,dataList[i]['INVOICE_NO']
							,dataList[i]['INVOICE_TYPE']
							,dataList[i]['INVOICE_STATUS']
							,dataList[i]['BILL_TO_PARTY_CODE']
							,dataList[i]['BILL_TO_PARTY_NAME']
							,dataList[i]['MAIN_CURRENCY']
							,dataList[i]['NET_AMT_MAIN']
							,dataList[i]['EXPORT_IMPORT']
							]
			}

			$(document).ready(function() {
				$('#list').DataTable( {
					fixedHeader: true,
					data: dataSet,
					columns: [
						{ title: "Com Code" },
						{ title: "Port Call" },
						{ title: "Collection FSC" },
						{ title: "Invoice No" },
						{ title: "Invoice Type" },
						{ title: "Invoice Status" },
						{ title: "Customer Code" },
						{ title: "Customer Names" },
						{ title: "Main Currency" },
						{ title: "Net Amount Main" },
						{ title: "Export Import" }
					],
					responsive: {
						details: {
							display: $.fn.dataTable.Responsive.display.modal( {
								header: function ( row ) {
									var data = row.data();
									return 'Invoice Detail for '+data[2];
								}
							} ),
							//renderer: $.fn.dataTable.Responsive.renderer.tableAll({tableClass: 'table'})
						}
					}
				} );
				//$('.close').click();

				$(".hidden").hide();
			} );

    	},
		error: function (result) {
			alert('error');

		},
		complete: function () {


		}


	});


}
//###ADDED BY ASCAN
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();
    });
	function rutOpenLookupTable(table, option, returnVariable){
		alert('Attempting to run: rutOpenLookupTable('+table+","+option+","+returnVariable+')');
	}
//###ADDED BY ASCAN
</script>

<body>

<form id="frm" name="frm" class="needs-validation" novalidate>
	<div class="rcl-standard-search-header rcl-standard-font">
<!--###ADDED BY ASCAN   BEGIN-->
		<div class="row">
            <!-- Date -->
			<rcl:date id="s0-eta" label="ETA" classes="div(col-sm-2 pb-0)" check="req" options="novalidate"/>
        </div>
<!--###ADDED BY ASCAN  END-->


		<div class="row">
			<div class="col-md-2">
				<label class="rcl-standard-font">Vessel</label>
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-textbox-required rcl-standard-font" autocomplete="on" id="txtVessel" required placeholder="">
					<label for="txtVessel" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px"></label>
				</div>
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">Voyage</label>
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-textbox-required rcl-standard-font" id="txtVoyage">
					<label for="txtVoyage" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" ></label>
				</div>
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">Port</label>
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font" name="txtPort">
					<label for="txtPort" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" onclick="getPort()"></label>
				</div>
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">Invoice Type</label>
				<select id="selectbox" data-selected="" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font">
					<option value="" selected="selected" disabled="disabled">Select Invoice Type</option>
					<option value="1">All</option>
					<option value="2">Invoice</option>
					<option value="3">Credit Note</option>

				</select>
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">Invoice Status</label>
				<select id="selectbox" data-selected="" class="rcl-standard-textbox rcl-standard-font">
					<option value="" selected="selected" disabled="disabled">Select Invoice Status</option>
					<option value="1">All</option>
					<option value="2">Printed</option>
					<option value="3">Cancelled</option>

				</select>
			</div>
		</div>

		<div class="row">

			<div class="col-md-2">
				<label class="rcl-standard-font">Invoice No</label>
				<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font" id="txtInvoiceNo">
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">BL No</label>
				<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font" id="txtBL">
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">Invoice Date From</label>
				<div class="form-group">
				<div class="input-group date" id="invoice_start_date" data-target-input="nearest">
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font " id="txtInvStartDate" data-target="#invoice_start_date"/>
							<div class="input-group-append" data-target="#invoice_start_date" data-toggle="datetimepicker">
								<label for="txtInvStartDate" class="fa fa-calendar-alt rcl-standard-input-icon" style="font-size:14px"></label>
							</div>
					</div>
				</div>
				</div>
			</div>

			<div class="col-md-2">
				<label class="rcl-standard-font">Invoice Date To</label>
				<div id="invoice_end_date" data-target-input="nearest">
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font" id="txtInvEndDate" data-target="#invoice_end_date"/>
							<div class="input-group-append" data-target="#invoice_end_date" data-toggle="datetimepicker">
								<label for="txtInvEndDate" class="fa fa-calendar-alt rcl-standard-input-icon" style="font-size:14px"></label>
							</div>
					</div>
				</div>
			</div>

		</div>

		<div class="row">


			<!--
			<div class="col-md">
			<label class="rcl-standard-font">Comment:</label>
	  			<textarea class="form-control rcl-standard-textbox rcl-standard-font" rows="5" id="comment"></textarea>
			</div>

			<div class="col-md">
				<label class="rcl-standard-font">Status</label>
				<div class="form-check form-check-inline">
					<input type="checkbox" id="inlineCheckbox1" value="option1">
					<label class="rcl-standard-font" for="inlineCheckbox1">Active</label>

					<input type="checkbox" id="inlineCheckbox2" value="option2">
					<label class="rcl-standard-font"for="inlineCheckbox2">Suspend</label>
				</div>
			</div>
			-->

		</div>
		<div class="row">
			<div class="col-md">
				<button type="button" class="rcl-standard-button rcl-standard-font" onclick="show()">Find</button>
				<button type="button" class="rcl-standard-button rcl-standard-font">Clear</button>
			</div>
		</div>
		<div class="ajax_loader hidden"><i class="fa fa-spinner fa-spin" style="font-size:40px"></i></div>


	</div>

</form>

<div class="rcl-standard-search-result rcl-standard-font">
	<table id="list" class="table table-bordered table-hover rcl-standard-font"/></table>
</div>

</body>
</html>