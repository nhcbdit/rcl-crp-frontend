<script>
	
function showInquiry() {
	
	var param = { exp_imp: "E"
		, vessel: "XTB"
		, voyage: "1101W"
		, port: null
		, coc_soc: "C"
		, invoice_date_from: null
		, invoice_date_to: null
		, invoice_type: null
		, invoice: null
		, bl_no: "BKKCB18003597"
		, transaction_type: null };

	var form = document.getElementById('frmInquiry');
	var isValidForm = form.checkValidity();	

	/*
	
	if(!isValidForm) {
		
		var fields = document.getElementsByTagName('INPUT');
		
	    for(var i=0; i < fields.length; i++) {
	      if(fields[i].hasAttribute('placeholder')) {
	        fields[i].defaultValue = fields[i].getAttribute('placeholder');
	        fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
	        fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
	      }
	    }
		
		
		return false;
	}	
	*/
	
	$(".hidden").show();
	
	$.ajax({
		async: true,
		url: 'http://marlin-ce.rclgroup.com:8080/TSTWSWebApp/rclws/wspilot/getDataPilot',
    	type: 'POST',
    	dataType: 'json',
    	data: param,
    	success: function (dataList) {	
			var dataSet = [];
			for(var i=0;i<dataList.length;i++) {
				dataSet[i] = [dataList[i]['COM_CODE']
							,dataList[i]['PORT_CALL']
							,dataList[i]['COLL_FSC']
							,dataList[i]['INVOICE_NO']
							,dataList[i]['INVOICE_TYPE']
							,dataList[i]['INVOICE_STATUS']
							,dataList[i]['BILL_TO_PARTY_CODE']
							,dataList[i]['BILL_TO_PARTY_NAME']
							,dataList[i]['MAIN_CURRENCY']
							,dataList[i]['NET_AMT_MAIN']
							,dataList[i]['EXPORT_IMPORT']
							]
			}
			
			$(document).ready(function() {
				$('#inquiry').DataTable( {	
					retrieve: true,
					fixedHeader: true,
					data: dataSet,
					columns: [
						{ title: "Com Code" },
						{ title: "Port Call" },
						{ title: "Collection FSC" },
						{ title: "Invoice No" },
						{ title: "Invoice Type" },
						{ title: "Invoice Status" },
						{ title: "Customer Code" },
						{ title: "Customer Names" },
						{ title: "Main Currency" },
						{ title: "Net Amount Main" },
						{ title: "Export Import" }
					]
				} );
				
				$(".hidden").hide();
			} );
			
    	}, 
		error: function(jqXHR, textStatus, errorThrown) {
			alert('Error: ' + textStatus);
			
			$(".hidden").hide();

		},
		complete: function () {

			$(".hidden").hide();
		}
    	
    	
	});

	
}

</script>

<form id="frmInquiry" name="frmInquiry" class="needs-validation" novalidate>
	<div class="rcl-standard-search-header">	
		<div class="row">
			<div class="col-md-2">
				<label>Vessel</label>			
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtVessel" required placeholder="">
					<label for="txtVessel" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" onclick="getVessel()"></label> 
				</div>
				 
			</div>
	
			<div class="col-md-2">
				<label>Voyage</label>			
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtVoyage" required>
					<label for="txtVoyage" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" ></label>
				</div>
			</div>
	
			<div class="col-md-2">
				<label>Port</label>
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" autocomplete="on" name="txtPort">
					<label for="txtPort" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" onclick="getPort()"></label>
				</div>
			</div>
	
			<div class="col-md-2">
				<label>Invoice Type</label>
				<select id="selectbox" data-selected="" class="rcl-standard-form-control rcl-standard-component">
					<option value="" selected="selected" disabled="disabled">Select Invoice Type</option>
					<option value="1">All</option>
					<option value="2">Invoice</option>
					<option value="3">Credit Note</option>
	
				</select>
			</div>
			
			<div class="col-md-2">
				<label>Invoice Status</label>
				<select id="selectbox" data-selected="" class="rcl-standard-component">
					<option value="" selected="selected" disabled="disabled">Select Invoice Status</option>
					<option value="1">All</option>
					<option value="2">Printed</option>
					<option value="3">Cancelled</option>
	
				</select>
			</div>
		</div>
	
		<div class="row">
			
			<div class="col-md-2">
				<label>Invoice No</label>
				<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtInvoiceNo">
			</div>
	
			<div class="col-md-2">				
				<label>BL No</label>
				<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtBL">
			</div>
			
			<div class="col-md-2">
				<label>Invoice Date From</label>				
				<div class="form-group">
				<div class="input-group date" id="invoice_start_date" data-target-input="nearest">
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtInvStartDate" data-target="#invoice_start_date"/>
							<div class="input-group-append" data-target="#invoice_start_date" data-toggle="datetimepicker">
								<label for="txtInvStartDate" class="fa fa-calendar-alt rcl-standard-input-icon" style="font-size:14px"></label>
							</div>
					</div>	
				</div>
				</div>
			</div>  
			
			<div class="col-md-2">
				<label>Invoice Date To</label>
				<div id="invoice_end_date" data-target-input="nearest">
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtInvEndDate" data-target="#invoice_end_date"/>
							<div class="input-group-append" data-target="#invoice_end_date" data-toggle="datetimepicker">
								<label for="txtInvEndDate" class="fa fa-calendar-alt rcl-standard-input-icon" style="font-size:14px"></label>
							</div>
					</div>
				</div>
			</div>  
	
		</div>
	
		<div class="row">
			<div class="col-md">
				<button type="button" class="rcl-standard-button" onclick="showInquiry()">Find</button>
				<button type="button" class="rcl-standard-button">Clear</button>
			</div>
		</div>			
		<div class="ajax_loader hidden"><i class="fa fa-spinner fa-spin" style="font-size:40px"></i></div>
		
	
	</div>

</form>

<div class="rcl-standard-search-result">
	<table id="inquiry" class="table table-bordered table-hover rcl-standard-search-font"/></table>
</div>