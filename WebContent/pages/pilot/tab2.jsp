<script>

function displayDataTable(param) {
	
	$(".hidden").show();	
	
	$.ajax({
		async: true,
		url: 'http://marlin-ce.rclgroup.com:8080/TSTWSWebApp/rclws/wspilot/maintainDataPilot',
		type: 'POST',
		dataType: 'json',
		data: param,
		success: function (dataList) {
			var dataSet = [];
			for(var i=0;i<dataList.length;i++) {								
				dataSet[i] = [dataList[i]['ID']
							,dataList[i]['APP_REFERENCE']
							,dataList[i]['INVOICE_NO']
							,dataList[i]['INVOICE_TYPE_STRING']
							,dataList[i]['INVOICE_PRINTING_DATE']
							,dataList[i]['SAIL_DATE']
							,dataList[i]['STATUS_STRING']
							,'<a href="javascript:editData(\'' + dataList[i]['ID'] + '\',\''  + dataList[i]['APP_REFERENCE'] + '\',\'' + dataList[i]['INVOICE_NO'] + '\',\'' + dataList[i]['INVOICE_TYPE'] + '\',\'' + dataList[i]['INVOICE_PRINTING_DATE'] + '\',\'' + dataList[i]['SAIL_DATE'] + '\',\'' + dataList[i]['STATUS'] + '\');"><i class="fas fa-edit"></i></a>'
							,'<a href="javascript:deleteData(\'' + dataList[i]['ID'] + '\')"><i class="fas fa-trash-alt"></i></a>'
							]
			}
	
			$(document).ready(function() {
				$('#maintenance').DataTable( {					
					fixedHeader: true,
					data: dataSet,
					columns: [
						{ title: "ID" },
						{ title: "App Reference" },
						{ title: "Invoice No" },
						{ title: "Invoice Type" },
						{ title: "Invoice Printing Date" },
						{ title: "Invoice Sail Date" },
						{ title: "Status" },
						{ title: "Edit" },
						{ title: "Delete" }
					],
					destroy: true
				} );
			
				$(".hidden").hide();
			} );
	
		}, 
		error: function (jqXHR, textStatus, errorThrown) {
			alert('Error: ' + textStatus);
			$(".hidden").hide();
	
		},
		complete: function () {
	
			$(".hidden").hide();
		}
	});
	
	//clear value
	$('#idEdit').val('');
	$('#txtBLEdit').val('');
	$('#txtInvoiceEdit').val('');
	$('#selectInvoiceTypeEdit').val('');
	$('#txtInvPrintDateEdit').val('');
	$('#selectInvStatusEdit').val('');
	
}

function editData(ID, APP_REFERENCE, INVOICE_NO, INVOICE_TYPE, INVOICE_PRINTING_DATE, SAIL_DATE, STATUS) {	
	
	$('#idEdit').val(ID);
	$('#txtBLEdit').val(APP_REFERENCE);
	$('#txtInvoiceEdit').val(INVOICE_NO);
	$('#selectInvoiceTypeEdit').val(INVOICE_TYPE);
	$('#txtInvPrintDateEdit').val(INVOICE_PRINTING_DATE);
	$('#selectInvStatusEdit').val(STATUS);
}

function addDetail() {
	
	var param = { 'maintain_type': 'I'
		, 'seq': $('#idEdit').val()
		, 'app_reference': $('#txtBLEdit').val()
		, 'invoice_no': $('#txtInvoiceEdit').val()
		, 'invoice_type': '-' 
		, 'invoice_printing_date': '-'
		, 'sail_date': '-'
		, 'user': '-' };	

	
	var form = document.getElementById('frmMaintenance');
	var isValidForm = form.checkValidity();	

	/*
	
	if(!isValidForm) {
	
		var fields = document.getElementsByTagName('INPUT');
		
		for(var i=0; i < fields.length; i++) {
		if(fields[i].hasAttribute('placeholder')) {
		fields[i].defaultValue = fields[i].getAttribute('placeholder');
		fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
		fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
		}
		}
		
		
		return false;
	}	
	*/
	
	displayDataTable(param);
}

function updateDetail() {	

	var param = { 'maintain_type': 'U'
		, 'seq': $('#idEdit').val()
		, 'app_reference': $('#txtBLEdit').val()
		, 'invoice_no': $('#txtInvoiceEdit').val()
		, 'invoice_type': '-' 
		, 'invoice_printing_date': '-'
		, 'sail_date': '-'
		, 'user': '-' };	

	
	var form = document.getElementById('frmMaintenance');
	var isValidForm = form.checkValidity();	

	/*
	
	if(!isValidForm) {
	
		var fields = document.getElementsByTagName('INPUT');
		
		for(var i=0; i < fields.length; i++) {
		if(fields[i].hasAttribute('placeholder')) {
		fields[i].defaultValue = fields[i].getAttribute('placeholder');
		fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
		fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
		}
		}
		
		
		return false;
	}	
	*/
	
	displayDataTable(param);
	
}

function deleteData(ID) {	
	
	var param = { 'maintain_type': 'D'
		, 'seq': ID
		, 'app_reference': $('#txtBL').val()
		, 'invoice_no': $('#txtInvoice').val()
		, 'invoice_type': '-' 
		, 'invoice_printing_date': '-'
		, 'sail_date': '-'
		, 'user': '-' };	

	var form = document.getElementById('frmMaintenance');
	var isValidForm = form.checkValidity();	
	
	/*
	
	if(!isValidForm) {
	
	var fields = document.getElementsByTagName('INPUT');
	
	for(var i=0; i < fields.length; i++) {
	if(fields[i].hasAttribute('placeholder')) {
	fields[i].defaultValue = fields[i].getAttribute('placeholder');
	fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
	fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
	}
	}
	
	
	return false;
	}	
	*/
	
	displayDataTable(param);
}
	
function showMaintenance() {	
	
	
	var param = { 'maintain_type': '-'
					, 'seq': '0'
					, 'app_reference': $('#txtBL').val()
					, 'invoice_no': $('#txtInvoice').val()
					, 'invoice_type': '-' 
					, 'invoice_printing_date': '-'
					, 'sail_date': '-'
					, 'user': '-' };	
	
	var form = document.getElementById('frmMaintenance');
	var isValidForm = form.checkValidity();	

	/*
	
	if(!isValidForm) {
		
		var fields = document.getElementsByTagName('INPUT');
		
	    for(var i=0; i < fields.length; i++) {
	      if(fields[i].hasAttribute('placeholder')) {
	        fields[i].defaultValue = fields[i].getAttribute('placeholder');
	        fields[i].onfocus = function() { if(this.value == this.defaultValue) this.value = ''; }
	        fields[i].onblur = function() { if(this.value == '') this.value = this.defaultValue; }
	      }
	    }
		
		
		return false;
	}	
	*/
	
	displayDataTable(param);

	
}

</script>

<form id="frmMaintenance" name="frmMaintenance" class="needs-validation" novalidate>
	<div class="rcl-standard-search-header">	
		<div class="row">		
	
			<div class="col-md-2">
				<label>BL</label>			
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" id="txtBL" required>
					<label for="txtBL" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" ></label>
				</div>
			</div>
			
			<div class="col-md-2">
				<label>Invoice No</label>			
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtInvoice" required placeholder="">
					<label for="txtInvoice" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px"></label> 
				</div>
			</div>
			
			<div class="col-md-2">
				<label>Invoice Type</label>
				<select id="selectInvoiceType" data-selected="" class="rcl-standard-component">
					<option value="" selected="selected" disabled="disabled">Select Invoice Type</option>
					<option value="N">All</option>
					<option value="I">Invoice</option>
					<option value="C">Credit Note</option>
	
				</select>
			</div>
			
			<div class="col-md-2">
				<label>Invoice Print Date From</label>				
				<div class="form-group">
				<div class="input-group date" id="invoice_print_start_date" data-target-input="nearest">
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control" id="txtInvPrintStartDate" data-target="#invoice_print_start_date"/>
							<div class="input-group-append" data-target="#invoice_print_start_date" data-toggle="datetimepicker">
								<label for="txtInvPrintStartDate" class="fa fa-calendar-alt rcl-standard-input-icon" style="font-size:14px"></label>
							</div>
					</div>	
				</div>
				</div>
			</div> 
			
			<div class="col-md-2">
				<label>Invoice Status</label>
				<select id="selectInvStatus" data-selected="" class="rcl-standard-component">
					<option value="" selected="selected" disabled="disabled">Select Invoice Status</option>
					<option value="N">All</option>
					<option value="A">Active</option>
					<option value="S">Suspended</option>	
				</select>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md">
				<button type="button" class="rcl-standard-button" onclick="showMaintenance()">Find</button>
				<button type="button" class="rcl-standard-button">Clear</button>
			</div>
		</div>			
		<div class="ajax_loader hidden"><i class="fa fa-spinner fa-spin" style="font-size:40px"></i></div>
		
	
	</div>

</form>

<a id="toggleDataList" style="color:white;right:0;" href="javascript:toggle('toggleDataList','t-container');"><label>Search</label></a>

<div id="t-container">
	<div class="rcl-standard-search-result">
		<table id="maintenance" class="table table-bordered table-hover rcl-standard-search-font"/></table>
	</div>
</div>



<a id="toggleEdit" style="color:white;right:0;" href="javascript:toggle('toggleEdit','t-containerEdit');"><label>Edit Detail</label></a>

	<div id="t-containerEdit">
		
		
		<form id="frmEdit" name="frmEdit" class="needs-validation" novalidate>
		<div class="rcl-standard-search-header">	
			<div class="row">
		
				<input type="hidden" id="idEdit" name>
				
				<div class="col-md-2">
					<label>BL</label>			
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control" id="txtBLEdit" required>
						<label for="txtBL" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" ></label>
					</div>
				</div>
				
				<div class="col-md-2">
					<label>Invoice No</label>			
					<div class="rcl-standard-input-wrapper">
						<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtInvoiceEdit" required placeholder="">
						<label for="txtInvoice" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px"></label> 
					</div>
				</div>
				
				<div class="col-md-2">
					<label>Invoice Type</label>
					<select id="selectInvoiceTypeEdit" data-selected="" class="rcl-standard-component">
						<option value="" selected="selected" disabled="disabled">Select Invoice Type</option>
						<option value="N">All</option>
						<option value="I">Invoice</option>
						<option value="C">Credit Note</option>	
					</select>
				</div>
				
				<div class="col-md-2">
					<label>Invoice Print Date</label>			
					<div class="form-group">
					<div class="input-group date" id="invoice_print_start_date" data-target-input="nearest">
						<div class="rcl-standard-input-wrapper">
							<input type="text" class="rcl-standard-form-control" id="txtInvPrintDateEdit" data-target="#invoice_print_date_edit"/>
								<div class="input-group-append" data-target="#invoice_print_date_edit" data-toggle="datetimepicker">
									<label for="txtInvPrintDateEdit" class="fa fa-calendar-alt rcl-standard-input-icon" style="font-size:14px"></label>
								</div>
						</div>	
					</div>
					</div>
				</div>  
				
				
				
				<div class="col-md-2">
					<label>Invoice Status</label>
					<select id="selectInvStatusEdit" data-selected="" class="rcl-standard-component">
						<option value="" selected="selected" disabled="disabled">Select Invoice Status</option>
						<option value="N">All</option>
						<option value="A">Active</option>
						<option value="S">Suspended</option>
		
					</select>
				</div>
				
			</div>
	
			<div class="row">
				<div class="col-md">
					<button type="button" class="rcl-standard-button" onclick="addDetail()">Add</button>
					<button type="button" class="rcl-standard-button" onclick="updateDetail()">Update</button>
					<button type="button" class="rcl-standard-button">Clear</button>
				</div>
			</div>			
			<div class="ajax_loader hidden"><i class="fa fa-spinner fa-spin" style="font-size:40px"></i></div>
			
		
		</div>
	
	</form>

</div>

<script language="javascript"> 
    function toggle(sourceElementId, targetElementId) {
        /*sourceElementId: The element which has called this function
          targetElementId: The element which shall be collapsed/expanded*/
        var isVisible = $('#' + targetElementId).is(":visible");
        //$('#'+elementId).toggle('slow'); //simply it is there or away
        $('#' + targetElementId).slideToggle('slow'); //rollup
        //$('#'+elementId).fadeToggle('slow');  //fade away
        if (isVisible === true) {
            $('#'+sourceElementId).text("Expand");
        }
        else {
            $('#'+sourceElementId).text('Collapse');
        }
    }    
</script>