<!--
-------------------------------------------------------------------------------------------------------------  
RcmJobOrderHelpScn.jsp
-------------------------------------------------------------------------------------------------------------  
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Sukit Narinsakchai 28/04/2010
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
01 28/04/10  SUK              Created
-------------------------------------------------------------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmJobOrderHelpUim, com.rclgroup.dolphin.web.model.ijs.IjsJobOrderMod"%>
<html>
<head>
<jsp:useBean id="rcmJobOrderHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmJobOrderHelpUim" scope="session"/>
<title>Job Order Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Job Order Help</td>
            </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="1%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="1%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmJobOrderHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="1%">&nbsp;in</td>
                <td class="TableLeftWhite" width="1%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." selected>Select one ...</option>
                    <option value="JN" <%if(rcmJobOrderHelpUim.getSearch().equals("JN")) { %>selected <%} %>>Job Order#</option>
                    <option value="JV" <%if(rcmJobOrderHelpUim.getSearch().equals("JV")) { %>selected <%} %>>Version#</option>
                    <option value="JT" <%if(rcmJobOrderHelpUim.getSearch().equals("JT")) { %>selected <%} %>>Job Order Type</option>    
                    <option value="V" <%if(rcmJobOrderHelpUim.getSearch().equals("V")) { %>selected <%} %>>Vendor Code</option>
                    <option value="JS" <%if(rcmJobOrderHelpUim.getSearch().equals("JS")) { %>selected <%} %>>Job Order Status</option>                   
                    <option value="S" <%if(rcmJobOrderHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>   
                </select>
                </td>
                <td class="TableLeftWhite" width="40%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');">&nbsp;&nbsp;<input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>                
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="5">
                    <%if(rcmJobOrderHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmJobOrderHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmJobOrderHelpUim.getFormName()%>','<%=rcmJobOrderHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Job Order List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="5%">Select</td>
                    <td class="TableLeftSub" height="20" width="10%">Job Order#</td>
                    <td class="TableLeftSub" height="20" width="25%">Version#</td>
                    <td class="TableLeftSub" height="20" width="10%">Job Order Type</td>
                    <td class="TableLeftSub" height="20" width="15%">Vendor Code</td>
                    <td class="TableLeftSub" height="20" width="15%">Job Order Status</td>                    
                    <td class="TableLeftSub" height="20" width="5%">Status</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      IjsJobOrderMod jo = new IjsJobOrderMod();
                      jo = (IjsJobOrderMod)listResult.get(i);
                      String type = rcmJobOrderHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = jo.getJoNo();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="5%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="10%"><%=jo.getJoNo()%></td>
                    <td height="20" width="25%"><%=jo.getJoVer()%></td>
                    <td height="20" width="10%"><%=jo.getJoType()%></td>
                    <td height="20" width="15%"><%=jo.getVendorCode()%></td>
                    <td height="20" width="15%"><%=jo.getJoStatus()%></td>
                    <td height="20" width="15%"><%=jo.getStatus()%></td>                    
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmJobOrderHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
