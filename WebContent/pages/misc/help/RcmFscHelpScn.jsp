<!--
--------------------------------------------------------
RcmFscHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 09/11/2007
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*,com.rclgroup.dolphin.web.ui.misc.help.RcmFscHelpUim,com.rclgroup.dolphin.web.model.cam.CamFscMod"%>
<html>
<head>
<jsp:useBean id="rcmFscHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmFscHelpUim" scope="session"/>
<title>FSC Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">FSC Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="1%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="1%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmFscHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="1%">&nbsp;in</td>
                <td class="TableLeftWhite" width="1%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmFscHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="F" <%if(rcmFscHelpUim.getSearch().equals("F")) { %>selected <%} %>>FSC Code</option>
                    <option value="N" <%if(rcmFscHelpUim.getSearch().equals("N")) { %>selected <%} %>>FSC Name</option>
                    <option value="L" <%if(rcmFscHelpUim.getSearch().equals("L")) { %>selected <%} %>>Line Code</option>
                    <option value="R" <%if(rcmFscHelpUim.getSearch().equals("R")) { %>selected <%} %>>Region Code</option>
                    <option value="A" <%if(rcmFscHelpUim.getSearch().equals("A")) { %>selected <%} %>>Agent Code</option>
                    <option value="S" <%if(rcmFscHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="40%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');">&nbsp;&nbsp;<input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
                
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="5">
                    <%if(rcmFscHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmFscHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmFscHelpUim.getFormName()%>','<%=rcmFscHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">FSC List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="5%">Select</td>
                    <td class="TableLeftSub" height="20" width="12%">FSC Code</td>
                    <td class="TableLeftSub" height="20" width="42%">FSC Name</td>
                    <td class="TableLeftSub" height="20" width="12%">Line Code</td>
                    <td class="TableLeftSub" height="20" width="12%">Region Code</td>
                    <td class="TableLeftSub" height="20" width="12%">Agent Code</td>
                    <td class="TableLeftSub" height="20" width="5%">Status</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      CamFscMod fsc = new CamFscMod();
                      fsc = (CamFscMod)listResult.get(i);
                      String type = rcmFscHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = fsc.getRegionCode()+"|"+fsc.getAgentCode()+"|"+fsc.getFscCode();
                      }else if(type.equals(RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS)) {
                          returnString = fsc.getRegionCode()+"|"+fsc.getAgentCode()+"|"+fsc.getFscCode();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="5%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="12%"><%=fsc.getFscCode()%></td>
                    <td height="20" width="42%"><%=fsc.getFscName()%></td>
                    <td height="20" width="12%"><%=fsc.getLineCode()%></td>
                    <td height="20" width="12%"><%=fsc.getRegionCode()%></td>
                    <td height="20" width="12%"><%=fsc.getAgentCode()%></td>
                    <td height="20" width="5%"><%=fsc.getStatus()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    System.out.println("Inside RcmFscHelpScn.jsp");
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmFscHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
