<!--
--------------------------------------------------------
RcmQuotationHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2008
--------------------------------------------------------
Author Kitti Pongsirisakun 01/12/2008
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="com.rclgroup.dolphin.web.ui.misc.help.RcmQtnExportToExcelHelpUim,com.rclgroup.dolphin.web.model.qtn.QtnExportToExcelMod"%>
<%@ page import="java.util.*" %>
<html>
<head>
<jsp:useBean id="rcmQtnExportToExcelHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmQtnExportToExcelHelpUim" scope="session"/>
<title>Quotation Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Quotation Help</td>
                <td class="PageTitle" width="2"%><a href="javascript:void(0)" onClick="javascript:window.parent.close()"><img border="0" valign="center" src="<%=imgURL%>/btnClose.gif" alt="Close" width="16" height="16"></td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="19%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmQtnExportToExcelHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="4%">&nbsp;&nbsp;in</td>
                <td class="TableLeftWhite" width="23%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="Q" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("Q")) { %>selected <%} %>>Quotation No</option>
                    <option value="RF" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("RF")) { %>selected <%} %>>Ref Quotation</option>
                    <option value="P" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("P")) { %>selected <%} %>>Contract Party Code</option>
                    <option value="PN" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("PN")) { %>selected <%} %>>Contract Party Name</option>
                    <option value="S" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>
                    <option value="DE" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("DE")) { %>selected <%} %>>Effective Date</option>
                    <option value="DX" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("DX")) { %>selected <%} %>>Expiry Date</option>
                    <option value="F" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("F")) { %>selected <%} %>>Follow Up Date</option>
                    <option value="T" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("T")) { %>selected <%} %>>Telephone</option>
                    <option value="E" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("E")) { %>selected <%} %>>Email</option>
                    <option value="R" <%if(rcmQtnExportToExcelHelpUim.getSearch().equals("R")) { %>selected <%} %>>Remarks</option>
                </select>
                </td>
                <td class="TableLeftWhite" colspan="2"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');">
                &nbsp;&nbsp;<input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();">
                </td>
              <!--  <td class="TableLeftWhite" width="3%"></td> -->
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmQtnExportToExcelHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" value="ON" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild" value="OFF">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmQtnExportToExcelHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmQtnExportToExcelHelpUim.getFormName()%>','<%=rcmQtnExportToExcelHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Quotation List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="5%">Select</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Quotation #</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Ref #</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Contract Party Code</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Contract Party Name</td>
                    <td class="TableLeftSub" height="20" width="5%" nowrap>Status</td>
                    <td class="TableLeftSub" height="20" width="5%" nowrap>Effective Date</td>
                    <td class="TableLeftSub" height="20" width="5%" nowrap>Expiry Date</td>
                    <td class="TableLeftSub" height="20" width="5%" nowrap>Follow Up Date</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Tel</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Email</td>
                    <td class="TableLeftSub" height="20" width="10%" nowrap>Remarks</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(), new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      QtnExportToExcelMod qtnExportToExcelMod = new QtnExportToExcelMod();
                      qtnExportToExcelMod = (QtnExportToExcelMod)listResult.get(i);  
                      String type = rcmQtnExportToExcelHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = qtnExportToExcelMod.getQuotationNo();
                      }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
                          returnString = qtnExportToExcelMod.getQuotationNo();
                      }else if(type.equals(RrcStandardUim.GET_HELP_V02__WITH_ACTIVE_STATUS)) {
                          returnString = qtnExportToExcelMod.getQuotationNo();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="5%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getQuotationNo()%></td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getRefQuotation()%></td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getPartyCode()%></td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getPartyName()%></td>
                    <td height="20" width="5%"><%=qtnExportToExcelMod.getStatus()%></td>
                    <td height="20" width="5%"><%=qtnExportToExcelMod.getEffDate()%>&nbsp;&nbsp;</td>
                    <td height="20" width="5%"><%=qtnExportToExcelMod.getExpDate()%>&nbsp;&nbsp;</td>
                    <td height="20" width="5%"><%=qtnExportToExcelMod.getFollowDate()%>&nbsp;&nbsp;</td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getTel()%></td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getEmail()%></td>
                    <td height="20" width="10%"><%=qtnExportToExcelMod.getRemarks()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmQtnExportToExcelHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
