<!--
--------------------------------------------------------
RcmOperatorHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sukit Narinsakchai 23/02/09
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmOperatorHelpUim, com.rclgroup.dolphin.web.model.cam.CamOperatorMod"%>
<html>
<head>
<jsp:useBean id="rcmOperatorHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmOperatorHelpUim" scope="session"/>
<title>Operator Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Operator/Slot Operator Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="1%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="1%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmOperatorHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="1%">&nbsp;in</td>
                <td class="TableLeftWhite" width="1%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmOperatorHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="C" <%if(rcmOperatorHelpUim.getSearch().equals("C")) { %>selected <%} %>>Operator Code</option>
                    <option value="D" <%if(rcmOperatorHelpUim.getSearch().equals("N")) { %>selected <%} %>>Description</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="40%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');">
                &nbsp;<input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmOperatorHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" value="ON" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild" value="OFF">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmOperatorHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmOperatorHelpUim.getFormName()%>','<%=rcmOperatorHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Operator List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    <td class="TableLeftSub" height="20" width="15%">Operator Code</td>
                    <td class="TableLeftSub" height="20" width="40%">Description</td>                    
                </tr>
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(), new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      CamOperatorMod Operator = new CamOperatorMod();
                      Operator = (CamOperatorMod)listResult.get(i);
                      String type = rcmOperatorHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = Operator.getOperatorCode();                      
                      }
                %>
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="15%"><%=Operator.getOperatorCode()%></td>
                    <td height="20" width="40%"><%=Operator.getDescription()%></td>                    
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmOperatorHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
