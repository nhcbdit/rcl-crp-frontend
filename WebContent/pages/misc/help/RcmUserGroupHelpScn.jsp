<!--
--------------------------------------------------------
RcmUserGroupHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Manop Wanngam 27/03/08
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
--> 

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmUserGroupHelpUim, com.rclgroup.dolphin.web.model.sys.SysUserGroupMod"%>
<html>
<head>
<jsp:useBean id="rcmUserGroupHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmUserGroupHelpUim" scope="session"/>
<title>User Group Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">User Group Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmUserGroupHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmUserGroupHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="C" <%if(rcmUserGroupHelpUim.getSearch().equals("C")) { %>selected <%} %>>User Group Code</option>
                    <option value="N" <%if(rcmUserGroupHelpUim.getSearch().equals("N")) { %>selected <%} %>>User Group Name</option>
                    <option value="S" <%if(rcmUserGroupHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmUserGroupHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" value="ON" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild" value="OFF">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmUserGroupHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmUserGroupHelpUim.getFormName()%>','<%=rcmUserGroupHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">User Group List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    <td class="TableLeftSub" height="20" width="20%">User Group Code</td>
                    <td class="TableLeftSub" height="20" width="60%">User Group Name</td>
                    <td class="TableLeftSub" height="20" width="10%">Status</td>
                </tr>
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(), new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      SysUserGroupMod userGroup = new SysUserGroupMod();
                      userGroup = (SysUserGroupMod)listResult.get(i);
                      String type = rcmUserGroupHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = userGroup.getUserGroupCode();
                      }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
                          returnString = userGroup.getUserGroupCode();
                      }
                %>
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="20%"><%=userGroup.getUserGroupCode()%></td>
                    <td height="20" width="60%"><%=userGroup.getUserGroupName()%></td>
                    <td height="20" width="10%"><%=userGroup.getUserGroupStatus()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmUserGroupHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
