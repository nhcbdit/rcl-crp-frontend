<!--
--------------------------------------------------------
RcmBsaPortPairTsHelpScn2.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Songkran Totiya 11/04/2012
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-test
## 11/05/2012 - Panadda - Fix Search criteria line:131
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmBsaPortPairTsHelpUim2, com.rclgroup.dolphin.web.model.bsa.BsaBsaPortPairTsMod2"%>
<html>
<head>
<jsp:useBean id="rcmBsaPortPairTsHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmBsaPortPairTsHelpUim2" scope="session"/>
<title>Port Pair Transhipment Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
<script>
function changeParent(name,frmName,retName,rowAt){    
    var cmb = eval("parent.opener.document.frm."+name);
    var vTsIndic = document.frmHelp.tsIndic.value;    
    if(cmb.length>1){
        if(vTsIndic == 'from' || vTsIndic == 'From' || vTsIndic == 'FROM'){    
            cmb.selectedIndex = 0;
        }else if(vTsIndic == 'to' || vTsIndic == 'To' || vTsIndic == 'TO'){
            cmb.selectedIndex = 1;
        }        
    }else{
        cmb.selectedIndex = 0;
    }
    //alert("  name "+name+"    frmName "+frmName+"    retName "+retName);
    //////////////////////
    
    var strVal = "";
    var selectRow = 0;
    if(document.frmHelp.optSelect.length==null){
        strVal = document.frmHelp.optSelect.value;
    }else{
        for(var i=0; i<document.frmHelp.optSelect.length; i++) {
            if(document.frmHelp.optSelect[i].checked) {
                strVal=document.frmHelp.optSelect[i].value;
                selectRow = i;
            }
        }
    }
    //alert(strVal);
    var strSplitVal = strVal.split("|");
    //var subGroupFlag = document.frmHelp.hidSubGroupFlag[selectRow].value;
    var subGroupFlag = "T";//mock data
    changeClassOwner(rowAt,frmName,subGroupFlag);    
    setValue(frmName,retName);
}

function changeClassOwner(rowAt,frmName,subGroupFlag){
    var vPolPortCall = (eval("parent.opener.document."+frmName+'.polPortCallLv'+rowAt)).value;
    var vPodPortCall = (eval("parent.opener.document."+frmName+'.podPortCallLv'+rowAt)).value;    
    if(subGroupFlag == "S" || vPolPortCall == "S" || vPodPortCall == "S"){
        (eval("parent.opener.document."+frmName+'.txtCocTeuFull'+rowAt)).className = "FormTextBoxARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtCocFullWeight'+rowAt)).className = "FormTextBoxARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtCocTeuMt'+rowAt)).className = "FormTextBoxARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtSocTeuFull'+rowAt)).className = "FormTextBoxARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtSocFullWeight'+rowAt)).className = "FormTextBoxARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtSocTeuMt'+rowAt)).className = "FormTextBoxARightFloatLeft";        
        (eval("parent.opener.document."+frmName+'.tsPortLv'+rowAt)).value = "S";       
        var onBoardWeight = eval("parent.opener.document."+frmName+'.txtOnBoardWeight'+rowAt);
        var onBoardTeu = eval("parent.opener.document."+frmName+'.txtOnBoardTeu'+rowAt);              
        if(typeof(onBoardWeight) != "undefined"){            
            onBoardWeight.style.visibility  = "hidden";
        }        
        if(typeof(onBoardTeu) != "undefined"){            
            onBoardTeu.style.visibility  = "hidden"; 
        }        
    }else{        
        (eval("parent.opener.document."+frmName+'.txtCocTeuFull'+rowAt)).className = "FormTextBoxReqARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtCocFullWeight'+rowAt)).className = "FormTextBoxReqARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtCocTeuMt'+rowAt)).className = "FormTextBoxReqARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtSocTeuFull'+rowAt)).className = "FormTextBoxReqARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtSocFullWeight'+rowAt)).className = "FormTextBoxReqARightFloatLeft";
        (eval("parent.opener.document."+frmName+'.txtSocTeuMt'+rowAt)).className = "FormTextBoxReqARightFloatLeft";             
        (eval("parent.opener.document."+frmName+'.tsPortLv'+rowAt)).value = "P";        
        var onBoardWeight = eval("parent.opener.document."+frmName+'.txtOnBoardWeight'+rowAt);
        var onBoardTeu = eval("parent.opener.document."+frmName+'.txtOnBoardTeu'+rowAt);                 
        if("undefined" != typeof(onBoardWeight)){            
            onBoardWeight.style.visibility  = "visible";
        }        
        if(typeof(onBoardTeu) != "undefined"){            
            onBoardTeu.style.visibility  = "visible"; 
        }        
    }
}

function setIndicValue(i)
{
    var obj = eval("document.frmHelp.hdTsIndic"+i);
    document.frmHelp.tsIndic.value = obj.value;    
    highlightRadioTD('row'+i);
}
</script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">
<input type="hidden" name="tsIndic" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Port Pair Transhipment Help</td>
            </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmBsaPortPairTsHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." selected>Select one ...</option>                
                    <option value="S" <%if(rcmBsaPortPairTsHelpUim.getSearch().equals("S")) { %>selected <%} %>>Service</option>
                    <option value="R" <%if(rcmBsaPortPairTsHelpUim.getSearch().equals("R")) { %>selected <%} %>>Proforma RefNo</option>
                    <option value="G" <%if(rcmBsaPortPairTsHelpUim.getSearch().equals("G")) { %>selected <%} %>>Port Proup</option>
                    <option value="P" <%if(rcmBsaPortPairTsHelpUim.getSearch().equals("P")) { %>selected <%} %>>Port</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmBsaPortPairTsHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmBsaPortPairTsHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="changeParent('<%=rcmBsaPortPairTsHelpUim.getTsIndicName()%>','<%=rcmBsaPortPairTsHelpUim.getFormName()%>','<%=rcmBsaPortPairTsHelpUim.getRetName()%>','<%=rcmBsaPortPairTsHelpUim.getRowAt()%>')"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Port List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    <td class="TableLeftSub" height="20" width="14%">Ts Indic</td>
                    <td class="TableLeftSub" height="20" width="14%">Service</td>
                    <td class="TableLeftSub" height="20" width="14%">Proforma RefNo</td>
                    <td class="TableLeftSub" height="20" width="14%">Variant</td>
                    <td class="TableLeftSub" height="20" width="14%">Port</td>
                    <td class="TableLeftSub" height="20" width="14%">Group</td>
                    <td class="TableLeftSub" height="20" width="14%">Status</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      BsaBsaPortPairTsMod2 bsaPortPairTs = new BsaBsaPortPairTsMod2();
                      bsaPortPairTs = (BsaBsaPortPairTsMod2)listResult.get(i); 
                      String type = rcmBsaPortPairTsHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_WITH_ACTIVE_STATUS)){                          
                          returnString = bsaPortPairTs.getBsaService()+"|"+bsaPortPairTs.getBsaProformaRefNo()+"|"+bsaPortPairTs.getBsaVariant()+"|"+bsaPortPairTs.getBsaPort()+"|"+bsaPortPairTs.getBsaGroup();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="setIndicValue(<%=i%>);"></td>
                    <td height="20" width="15%"><input type="hidden" name="hdTsIndic<%=i%>" value="<%=bsaPortPairTs.getTsIndic()%>"><%=bsaPortPairTs.getTsIndic()%></td>
                    <td height="20" width="15%"><%=bsaPortPairTs.getBsaService()%></td>
                    <td height="20" width="15%"><%=bsaPortPairTs.getBsaProformaRefNo()%></td>
                    <td height="20" width="15%"><%=bsaPortPairTs.getBsaVariant()%></td>
                    <td height="20" width="15%"><%=bsaPortPairTs.getBsaPort()%></td>
                     <td height="20" width="15%"><%=bsaPortPairTs.getBsaGroup()%></td>
                    <td height="20" width="15%"><%=bsaPortPairTs.getStatus()%></td>
                </tr>
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmBsaPortPairTsHelpSvc2";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
