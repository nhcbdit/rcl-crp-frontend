
<%--
--------------------------------------------------------
RcmTosRateReferanceHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Panadda P. 5/06/2010
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
-------------------------------------------------------- --%>

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*,com.rclgroup.dolphin.web.ui.misc.help.RcmTosRateReferanceHelpUim,com.rclgroup.dolphin.web.model.tos.TosRateHeaderMod"%>
<html>
<head>
<jsp:useBean id="rcmTosRateReferanceHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmTosRateReferanceHelpUim" scope="session"/>
<title>Rate Referance Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Rate Ref. Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmTosRateReferanceHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmTosRateReferanceHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="OPT" <%if(rcmTosRateReferanceHelpUim.getSearch().equals("OPT")) { %>selected <%} %>>Operation Type</option>
                    <option value="D" <%if(rcmTosRateReferanceHelpUim.getSearch().equals("D")) { %>selected <%} %>>Description</option>
                    <option value="R" <%if(rcmTosRateReferanceHelpUim.getSearch().equals("R")) { %>selected <%} %>>Rate Ref</option>
                    
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmTosRateReferanceHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmTosRateReferanceHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmTosRateReferanceHelpUim.getFormName()%>','<%=rcmTosRateReferanceHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Rate Ref. List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    
                    <td class="TableLeftSub" height="20" width="15%">Operation Type</td>
                    <td class="TableLeftSub" height="20" width="45%">Description</td>
                    <td class="TableLeftSub" height="20" width="20%">Rate Ref</td>
                    
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      TosRateHeaderMod rate = new TosRateHeaderMod();
                      rate = (TosRateHeaderMod)listResult.get(i);  
                      String type = rcmTosRateReferanceHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = rate.getOprType()+"|"+rate.getDescription()+"|"+rate.getRateRef();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                     <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="15%"><%=rate.getOprType()%></td>
                    <td height="20" width="45%"><%=rate.getDescription()%></td>
                    <td height="20" width="15%"><%=rate.getRateRef()%></td>
                    
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmTosRateReferanceHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
