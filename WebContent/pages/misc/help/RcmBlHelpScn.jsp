<!--
--------------------------------------------------------
RcmBlHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sukit Narinsakchai 10/10/2008
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
## 12/01/09  KIT    BUG.174   B/L Lookup problems
## 24/01/09  KIT    BUG.174 (Re-Opend)  B/L Lookup problems
03 03/06/09  POR    MODIFIED  ADD General B/L Lookup
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmBlHelpUim,com.rclgroup.dolphin.web.model.dex.DexBlMod"%>
<html>
<head>
<jsp:useBean id="rcmBlHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmBlHelpUim" scope="session"/>
<title>Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutValidation.js"></script>

<script>
function validate1(action){
    var strFind = document.frmHelp.txtFind.value;
    var strSearch = document.frmHelp.cmbSearch.value;
    var strErrMsg = "";
    //-- ## Begin 02
   /* if((strSearch == "OS" ||strSearch == "IS")  && strFind.length != 0) {        
        if(!checkOnlyNumber(strFind)){
         
            strErrMsg = "*Find criteria must be number";
        }
    }else*/ 
    //-- ## End 02
    if(strSearch == "ID" && strFind.length != 0){
        var c1 = strFind.substr(2,1);
        var c2 = strFind.substr(5,1)
        if(strFind.length!=10 || c1 != "/" || c2 != "/" ){                    
            strErrMsg = "*Find criteria should be in DD/MM/YYYY format";        
        } 
    }    
    if(strErrMsg == ""){ 
        search(action);
        //document.frmHelp.action = action;
        //document.frmHelp.submit();
    }else{
        showBarMessage('messagetd',strErrMsg,"")
    }
}
</script>
</head>

<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <tr>
                <td class="PageTitle">B/L Lookup</td> 
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmBlHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmBlHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="B" <%if(rcmBlHelpUim.getSearch().equals("B")) { %>selected <%} %>>Booking#</option>
                    <option value="BL" <%if(rcmBlHelpUim.getSearch().equals("BL")) { %>selected <%} %>>B/L#</option>
                    <%if (rcmBlHelpUim.getType().equals(RrcStandardUim.GET_HOUSEBL_GENERAL)){%> <% //--##03 %>
                        <option value="HBL" <%if(rcmBlHelpUim.getSearch().equals("HBL")) { %>selected <%} %>>HBL#</option>   <% //--##03 %>                                                                                    
                    <% } %>                                                                                                 <% //--##03 %>
                    <option value="OC" <%if(rcmBlHelpUim.getSearch().equals("OC")) { %>selected <%} %>>Op Code</option>
                    <option value="CS" <%if(rcmBlHelpUim.getSearch().equals("CS")) { %>selected <%} %>>Coc/Soc</option>
                    <option value="ID" <%if(rcmBlHelpUim.getSearch().equals("ID")) { %>selected <%} %>>Issue Date</option>
                    <option value="OS" <%if(rcmBlHelpUim.getSearch().equals("OS")) { %>selected <%} %>>Out Status</option>
                    <option value="IS" <%if(rcmBlHelpUim.getSearch().equals("IS")) { %>selected <%} %>>In Status</option>
                    <option value="VS" <%if(rcmBlHelpUim.getSearch().equals("VS")) { %>selected <%} %>>Vessel</option>
                    <option value="VY" <%if(rcmBlHelpUim.getSearch().equals("VY")) { %>selected <%} %>>Voyage</option>
                    <option value="POL" <%if(rcmBlHelpUim.getSearch().equals("POL")) { %>selected <%} %>>POL</option>
                    <option value="POD" <%if(rcmBlHelpUim.getSearch().equals("POD")) { %>selected <%} %>>POD</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="validate1('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmBlHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmBlHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmBlHelpUim.getFormName()%>','<%=rcmBlHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">B/L List</td> <% // --##01 %>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="5%">Select</td>
                    <td class="TableLeftSub" height="20" width="10%">Booking#</td> <% // --##01 %>
                    <td class="TableLeftSub" height="20" width="10%">B/L#</td> <% // --##01 %>
                    
                    <%if (rcmBlHelpUim.getType().equals(RrcStandardUim.GET_HOUSEBL_GENERAL)){%> <% //--##03 %>
                        <td class="TableLeftSub" height="20" width="5%" nowrap>House B/L#</td>      <% //--##03 %>
                    <% } %>                                                                         <% //--##03 %>
                    
                    <td class="TableLeftSub" height="20" width="5%" nowrap>Op Code</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">COC/SOC</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%" nowrap>Issue Date</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">Out Status</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">In Status</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">Vessel</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">Voyage</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">POL</td><% // --##01 %>
                    <td class="TableLeftSub" height="20" width="5%">POD</td><% // --##01 %>
                    
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                  String inStatus = "";
                  String outStatus = "";
                  String cocsoc = "";
                  
                  for(int i=0;i<listResult.size();i++){                      
                      DexBlMod bl = new DexBlMod();
                      bl = (DexBlMod)listResult.get(i); 
                      String type = rcmBlHelpUim.getType();
                      String returnString = "";
                      inStatus  = bl.getInStatus() ;
                      outStatus = bl.getOutStatus() ;
                      cocsoc = bl.getCocSoc();
                      
                      if(cocsoc != null && cocsoc.equals("C")){
                        cocsoc = "COC";
                      }else  if(cocsoc.equals("S")){
                        cocsoc = "SOC";
                      }else{
                        cocsoc = "";
                      }
                      
                      if (inStatus != null && inStatus.equals("1")  ){
                        inStatus = "Entry";
                      }else   if (inStatus.equals( "2") ){
                        inStatus = "Confirmed";
                      }else  if (inStatus.equals( "3") ){
                        inStatus = "Printed";
                      } else if (inStatus.equals( "4" )){
                        inStatus = "Manifested";
                      }else  if (inStatus.equals( "5" )){
                        inStatus = "Invoiced";
                      }else  if (inStatus.equals("6" )){
                        inStatus = "Waitlisted";
                      }else  if (inStatus.equals( "7" )){
                        inStatus = "Multipart(B/L)";
                      }else  if (inStatus.equals( "9") ){
                        inStatus = "Cancelled";
                      }
    
                    if (outStatus != null && outStatus.equals("1") ){
                        outStatus = "Entry";
                      }else   if (outStatus.equals( "2" )){
                        outStatus = "Confirmed";
                      } else if (outStatus.equals( "3" )){
                        outStatus = "Printed";
                      }else  if (outStatus.equals( "4") ){
                        outStatus = "Manifested";
                      }else  if (outStatus.equals( "5" )){
                        outStatus = "Invoiced";
                      } else if (outStatus.equals( "6") ){
                        outStatus = "Waitlisted";
                      }else  if (outStatus.equals( "7") ){
                        outStatus = "Multipart(B/L)";
                      } else if (outStatus.equals( "9") ){
                        outStatus = "Cancelled";
                      }

                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          //returnString = bl.getActivityCode();
                          returnString = bl.getBl()+"|"+bl.getPol()+"|"+bl.getPod();                          
                      }else if(type.equals(RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS)) {
                          //returnString = activity.getActivityCode();
                          returnString = bl.getBl()+"|"+bl.getPol()+"|"+bl.getPod();
                      }else if(type.equals(RrcStandardUim.GET_BL_FOR_DG_MANIFEST)) {
                          //returnString = activity.getActivityCode();
                          returnString = bl.getBl()+"|"+bl.getManifestDate();
                      }else if (type.equals(RrcStandardUim.GET_REEFER_GENERAL)){
                          returnString = bl.getBl();
                      }else if (type.equals(RrcStandardUim.GET_REEFER_IMP_GENERAL)){
                          returnString = bl.getBl();
                      }else if (type.equals(RrcStandardUim.GET_HOUSEBL_GENERAL)){    //--##03
                          returnString = bl.getHbl();                                //--##03
                      }else if (type.equals(RrcStandardUim.GET_BL_GENERAL)){         //--##03
                          returnString = bl.getBl();                                 //--##03
                      }else if (type.equals(RrcStandardUim.GET_WITH_FSC)){           //--##03
                          returnString = bl.getBl();                                 //--##03
                      }else if (type.equals(RrcStandardUim.GET_HELP_V02_GENERAL)){ 
                          returnString = bl.getBl();                               
                      }else if (type.equals(RrcStandardUim.GET_HELP_V02_WITH_FSC)){           
                          returnString = bl.getBl();                                 
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="5%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="10%"><%=bl.getBooking()%>&nbsp;&nbsp;</td><% // --##01 %>
                    <td height="20" width="10%"><%=bl.getBl()%>&nbsp;&nbsp;</td>    <% // --##01 %>
                    
                    <%if (rcmBlHelpUim.getType().equals(RrcStandardUim.GET_HOUSEBL_GENERAL)){%>  <% //--##03 %>
                        <td height="20" width="10%"><%=bl.getHbl()%>&nbsp;&nbsp;</td>                <% //--##03 %>
                    <% } %>                                                                          <% //--##03 %>
                    
                    <td height="20" width="5%"><%=bl.getOpCode()%></td><% // --##01 %>
                    <td height="20" width="5%"><%=cocsoc%></td><% // --##01 %>
                    <td height="20" width="5%"> <%=bl.getIssueDateFormat()%>&nbsp;&nbsp;</td><% // --##01 %>  
                    <td height="20" width="5%"><%=outStatus%>&nbsp;&nbsp;</td><% // --##01 %>
                    <td height="20" width="5%"><%=inStatus%>&nbsp;&nbsp;</td><% // --##01 %>
                    <td height="20" width="5%"><%=bl.getVessel()%></td><% // --##01 %>
                    <td height="20" width="5%"><%=bl.getVoyage()%>&nbsp;&nbsp;</td><% // --##01 %>
                    <td height="20" width="5%"><%=bl.getPol()%>&nbsp;&nbsp;</td><% // --##01 %>
                    <td height="20" width="5%"><%=bl.getPod()%>&nbsp;&nbsp;</td><% // --##01 %>
                   
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmBlHelpSvc";  // --##01
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
