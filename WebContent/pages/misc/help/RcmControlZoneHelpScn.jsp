<!--
--------------------------------------------------------
RcmControlZoneHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Manop Wanngam 05/11/07
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmControlZoneHelpUim, com.rclgroup.dolphin.web.model.ems.EmsControlZoneMod"%>
<html>
<head>
<jsp:useBean id="rcmControlZoneHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmControlZoneHelpUim" scope="session"/>
<title>Control Zone Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Control Zone Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmControlZoneHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmControlZoneHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="C" <%if(rcmControlZoneHelpUim.getSearch().equals("C")) { %>selected <%} %>>Control Zone Code</option>
                    <option value="N" <%if(rcmControlZoneHelpUim.getSearch().equals("N")) { %>selected <%} %>>Control Zone Name</option>
                    <option value="A" <%if(rcmControlZoneHelpUim.getSearch().equals("A")) { %>selected <%} %>>Area Code</option>
                    <option value="S" <%if(rcmControlZoneHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmControlZoneHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" value="ON" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild" value="OFF">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmControlZoneHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmControlZoneHelpUim.getFormName()%>','<%=rcmControlZoneHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Control Zone List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    <td class="TableLeftSub" height="20" width="25%">Control Zone Code</td>
                    <td class="TableLeftSub" height="20" width="40%">Control Zone Name</td>
                    <td class="TableLeftSub" height="20" width="15%">Area Code</td>                    
                    <td class="TableLeftSub" height="20" width="10%">Status</td>
                </tr>
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(), new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      EmsControlZoneMod controlZone = new EmsControlZoneMod();
                      controlZone = (EmsControlZoneMod)listResult.get(i);
                      String type = rcmControlZoneHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = controlZone.getRegionCode()+"|"+controlZone.getAreaCode()+"|"+controlZone.getControlZoneCode();
                      }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
                          returnString = controlZone.getRegionCode()+"|"+controlZone.getAreaCode()+"|"+controlZone.getControlZoneCode();
                      }
                %>
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="25%"><%=controlZone.getControlZoneCode()%></td>
                    <td height="20" width="40%"><%=controlZone.getControlZoneName()%></td>
                    <td height="20" width="15%"><%=controlZone.getAreaCode()%></td>
                    <td height="20" width="10%"><%=controlZone.getControlZoneStatus()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmControlZoneHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
