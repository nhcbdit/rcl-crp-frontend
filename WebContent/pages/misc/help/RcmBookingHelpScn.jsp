<!--
--------------------------------------------------------
RcmBookingHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 02/09/2008
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmBookingHelpUim, com.rclgroup.dolphin.web.model.bkg.BkgBookingMod"%>
<html>
<head>
<jsp:useBean id="RcmBookingHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmBookingHelpUim" scope="session"/>
<title>Booking Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutValidation.js"></script>
<script>
function validate1(action){
    var strFind = document.frmHelp.txtFind.value;
    var strSearch = document.frmHelp.cmbSearch.value;
    var strErrMsg = "";
    
    if(strSearch == "D" && strFind.length != 0){
        var c1 = strFind.substr(2,1);
        var c2 = strFind.substr(5,1)
        if(strFind.length!=10 || c1 != "/" || c2 != "/" ){                    
            strErrMsg = "*Find criteria should be in DD/MM/YYYY format";        
        } 
    }    
    if(strErrMsg == ""){ 
        search(action);
        //document.frmHelp.action = action;
        //document.frmHelp.submit();
    }else{
        showBarMessage('messagetd',strErrMsg,"")
    }
}
</script>
</head>

<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Booking Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="1%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="1%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="RcmBookingHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="1%">&nbsp;in</td>
                <td class="TableLeftWhite" width="1%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(RcmBookingHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="B" <%if(RcmBookingHelpUim.getSearch().equals("C")) { %>selected <%} %>>Booking#</option>
                    <option value="D" <%if(RcmBookingHelpUim.getSearch().equals("D")) { %>selected <%} %>>Booking Date</option>
                    <option value="S" <%if(RcmBookingHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="20%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="validate1('<%=servURL%>/RrcStandardSrv?pageAction=search');">
                &nbsp;<input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(RcmBookingHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" value="ON" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild" value="OFF">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = RcmBookingHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=RcmBookingHelpUim.getFormName()%>','<%=RcmBookingHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Term List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    <td class="TableLeftSub" height="20" width="20%">Booking#</td>
                    <td class="TableLeftSub" height="20" width="60%">Booking Date</td>
                    <td class="TableLeftSub" height="20" width="10%">Status</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(), new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      BkgBookingMod bkgMod = new BkgBookingMod();
                      bkgMod = (BkgBookingMod)listResult.get(i);  
                      String type = RcmBookingHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = bkgMod.getBookingNo();
                      }else if(type.equals(RrcStandardUim.GET_WITH_FSC)) {
                          returnString = bkgMod.getBookingNo();
                      }else if(type.equals(RrcStandardUim.GET_HELP_V02_WITH_FSC)) {
                          returnString = bkgMod.getBookingNo();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="20%"><%=bkgMod.getBookingNo()%></td>
                    <td height="20" width="60%"><%=bkgMod.getBookingDate()%></td>
                    <td height="20" width="10%"><%=bkgMod.getBookingStatus()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmBookingHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
