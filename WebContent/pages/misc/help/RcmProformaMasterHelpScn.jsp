<!--
--------------------------------------------------------
RcmProformaMasterHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Wuttitorn Wuttijiaranai 02/06/2009
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*,com.rclgroup.dolphin.web.model.vss.VssProformaMasterMod,com.rclgroup.dolphin.web.util.RutCodeDescription"%>
<html>
<head>
<jsp:useBean id="rcmProformaMasterHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmProformaMasterHelpUim" scope="session"/>
<title>Proforma Number Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Proforma Number Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmProformaMasterHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmProformaMasterHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="C" <%if(rcmProformaMasterHelpUim.getSearch().equals("C")) { %>selected <%} %>>Service Code</option>
                    <option value="D" <%if(rcmProformaMasterHelpUim.getSearch().equals("D")) { %>selected <%} %>>Service Description</option>
                    <option value="P" <%if(rcmProformaMasterHelpUim.getSearch().equals("P")) { %>selected <%} %>>Proforma No</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmProformaMasterHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmProformaMasterHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmProformaMasterHelpUim.getFormName()%>','<%=rcmProformaMasterHelpUim.getRetName()%>','<%=rcmProformaMasterHelpUim.getRetFunc()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Service Proforma List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="5%">Select</td>
                    <td class="TableLeftSub" height="20" width="15%">Service Code</td>
                    <td class="TableLeftSub" height="20" width="40%">Service Description</td>
                    <td class="TableLeftSub" height="20" width="15%">Proforma No</td>
                    <td class="TableLeftSub" height="20" width="10%">Status</td>
                </tr>   
                <%
                    String returnString = null;
                    String type = rcmProformaMasterHelpUim.getType();
                    VssProformaMasterMod bean = null;
                    
                    List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                    for (int i=0;i<listResult.size();i++) {
                        bean = (VssProformaMasterMod) listResult.get(i);  
                        if (type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)) {
                            returnString = bean.getVssProformaId()+"|"+bean.getServiceCode()+"|"+bean.getProformaReferenceNo()+"|"+bean.getDesignedNoOfVessels();
                        }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20"><%=bean.getServiceCode()%></td>
                    <td height="20"><%=bean.getServiceDescription()%></td>
                    <td height="20"><%=bean.getProformaReferenceNo()%></td>
                    <td height="20"><%=RutCodeDescription.getStatusDesc(bean.getRecordStatus())%></td>
                </tr>   
                <%        
                    }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmProformaMasterHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
