<!--
-------------------------------------------------------------------------------------------------------------  
RcmUserHelpScn.jsp
-------------------------------------------------------------------------------------------------------------  
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------  
Author Sopon Dee-udomvongsa 28/03/2008
- Change Log ------------------------------------------------------------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
01 28/03/08  SPD              Created
-------------------------------------------------------------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmUserHelpUim, com.rclgroup.dolphin.web.model.sys.SysUserMod"%>
<html>
<head>
<jsp:useBean id="rcmUserHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmUserHelpUim" scope="session"/>
<title>User Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">User Help</td>
            </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="1%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="1%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmUserHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="1%">&nbsp;in</td>
                <td class="TableLeftWhite" width="1%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." selected>Select one ...</option>
                    <option value="I" <%if(rcmUserHelpUim.getSearch().equals("I")) { %>selected <%} %>>User ID</option>
                    <option value="N" <%if(rcmUserHelpUim.getSearch().equals("N")) { %>selected <%} %>>Name</option>
                    <option value="F" <%if(rcmUserHelpUim.getSearch().equals("F")) { %>selected <%} %>>FSC</option>    
                    <option value="O" <%if(rcmUserHelpUim.getSearch().equals("O")) { %>selected <%} %>>Organization Type</option>
                    <option value="D" <%if(rcmUserHelpUim.getSearch().equals("D")) { %>selected <%} %>>Department</option>
                    <option value="E" <%if(rcmUserHelpUim.getSearch().equals("E")) { %>selected <%} %>>E-Mail</option>   
                    <option value="S" <%if(rcmUserHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>   
                </select>
                </td>
                <td class="TableLeftWhite" width="40%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');">&nbsp;&nbsp;<input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>                
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="5">
                    <%if(rcmUserHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card&nbsp;&nbsp;&nbsp;&nbsp;</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmUserHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmUserHelpUim.getFormName()%>','<%=rcmUserHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">User List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="5%">Select</td>
                    <td class="TableLeftSub" height="20" width="10%">User ID</td>
                    <td class="TableLeftSub" height="20" width="25%">Name</td>
                    <td class="TableLeftSub" height="20" width="10%">FSC</td>
                    <td class="TableLeftSub" height="20" width="15%">Organization Type</td>
                    <td class="TableLeftSub" height="20" width="15%">Department</td>
                    <td class="TableLeftSub" height="20" width="15%">E-Mail</td>
                    <td class="TableLeftSub" height="20" width="5%">Status</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(),new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      SysUserMod user = new SysUserMod();
                      user = (SysUserMod)listResult.get(i);
                      String type = rcmUserHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = user.getUserID()+"|"+user.getName()+"|"+user.getFsc()+"|"+user.getOrg_type()+"|"+user.getDepartment()+"|"+user.getEmail();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="5%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="10%"><%=user.getUserID()%></td>
                    <td height="20" width="25%"><%=user.getName()%></td>
                    <td height="20" width="10%"><%=user.getFsc()%></td>
                    <td height="20" width="15%"><%=user.getOrg_type()%></td>
                    <td height="20" width="15%"><%=user.getDepartment()%></td>
                    <td height="20" width="15%"><%=user.getEmail()%></td>
                    <td height="20" width="5%"><%=user.getStatus()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmUserHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>
