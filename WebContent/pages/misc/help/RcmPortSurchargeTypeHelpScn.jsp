<!--
--------------------------------------------------------
RcmPortSurchargeTypeHelpScn.jsp
--------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
--------------------------------------------------------
Author Sopon Dee-udomvongsa 02/09/2008
- Change Log -------------------------------------------
## DD/MM/YY –User- -TaskRef- -ShortDescription-
--------------------------------------------------------
-->

<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="java.util.*, com.rclgroup.dolphin.web.ui.misc.help.RcmPortSurchargeTypeHelpUim, com.rclgroup.dolphin.web.model.qtn.QtnPortSurchargeTypeMod"%>
<html>
<head>
<jsp:useBean id="rcmPortSurchargeTypeHelpUim" class="com.rclgroup.dolphin.web.ui.misc.help.RcmPortSurchargeTypeHelpUim" scope="session"/>
<title>Port Surcharge Type Help</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssURL%>/sealiner.css">
<script language="Javascript1.2" src="<%=jsURL%>/RutMessage.js"></script>
<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
</head>
<body onLoad="checkParent()" topmargin="0" leftmargin="0" bgcolor="white">
<form name="frmHelp" method="post" action= "<%=servURL%>/RrcStandardSrv">
<input type="hidden" name="service" value="<%=request.getParameter("service")%>">
<input type="hidden" name="errCode" value="">
<input type="hidden" name="errMsg" value="">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="400">
    <tr>
        <td height="10">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="5">
            <tr>
                <td class="PageTitle">Port Surcharge Type Help</td>
            </tr>
            </table>        
            <table border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td class="TableLeftMaint" width="5%">&nbsp;Find</td>
                <td class="TableLeftWhite" width="30%" height="24"><input type="text" name="txtFind" size="20" maxlength="60" value="<jsp:getProperty name="rcmPortSurchargeTypeHelpUim" property="find"/>" class="FormTextBox"></td>
                <td class="TableLeftMaint" width="5%">&nbsp;in</td>
                <td class="TableLeftWhite" width="25%" height="24">
                <p align="left">
                <select size="1" name="cmbSearch" class="FormTextBox">
                    <option value="Select one ..." <%if(rcmPortSurchargeTypeHelpUim.getSearch().equals("")) { %>selected <%} %>>Select one ...</option>
                    <option value="T" <%if(rcmPortSurchargeTypeHelpUim.getSearch().equals("T")) { %>selected <%} %>>Surcharge Type</option>
                    <option value="D" <%if(rcmPortSurchargeTypeHelpUim.getSearch().equals("D")) { %>selected <%} %>>Description</option>
                    <option value="S" <%if(rcmPortSurchargeTypeHelpUim.getSearch().equals("S")) { %>selected <%} %>>Status</option>
                </select>
                </td>
                <td class="TableLeftWhite" width="9%"><input type="button" class="FormBtnClr" value="Search" name="btnSearch" onClick="search('<%=servURL%>/RrcStandardSrv?pageAction=search');"></td>
                <td class="TableLeftWhite" width="10%"><input type="button" class="FormBtnClr"  value="Reset" name="btnReset" onClick="clickReset();"></td>
            </tr>
            <tr>
                <td class="TableLeftMaint" style="width:30px">&nbsp;</td>
                <td class="TableLeftWhite" colspan="6">
                    <%if(rcmPortSurchargeTypeHelpUim.getWild().equalsIgnoreCase("ON")){%>
                        <input type="checkbox" name="chkWild" value="ON" checked>
                    <%}else{%>
                        <input type="checkbox" name="chkWild" value="OFF">
                    <%}%>
                    <font face="Verdana" size="1">Wild Card</font>
                </td>
            </tr>
            </table>        
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" height="300">
                <%
                RutPage rutPage = rcmPortSurchargeTypeHelpUim.getRutPage();
                if((rutPage != null)&&(rutPage.getNumOfPages()!=0)){
                %>
                <table border="0" width="100%" cellspacing="0" cellpadding="0"  >
                <tr>
                    <td width="100%" class="TableLeftSub" ><input type="button" class="FormBtnClr" value="Select" name="btnSelect" onClick="setValue('<%=rcmPortSurchargeTypeHelpUim.getFormName()%>','<%=rcmPortSurchargeTypeHelpUim.getRetName()%>');"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1">
                <tr>
                    <td width="50%" class="TableLeftText" height="20">Port Surcharge Type List</td>
                    <td width="50%" class="TableLeftText" align="right"></td>
                </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>   
                    <td class="TableLeftSub" height="20" width="10%">Select</td>
                    <td class="TableLeftSub" height="20" width="20%">Surcharge Type</td>
                    <td class="TableLeftSub" height="20" width="60%">Description</td>
                    <td class="TableLeftSub" height="20" width="10%">Status</td>
                </tr>   
                <%
                  List listResult = rutPage.getItemList(rutPage.getPageNo(), new ArrayList()); 
                  for(int i=0;i<listResult.size();i++){
                      QtnPortSurchargeTypeMod portSurchargeType = new QtnPortSurchargeTypeMod();
                      portSurchargeType = (QtnPortSurchargeTypeMod)listResult.get(i); 
                      String type = rcmPortSurchargeTypeHelpUim.getType();
                      String returnString = "";
                      if(type == null || type.equals("") || type.equalsIgnoreCase(RrcStandardUim.GET_GENERAL)){
                          returnString = portSurchargeType.getSurchargeType();
                      }else if(type.equals(RrcStandardUim.GET_WITH_ACTIVE_STATUS)) {
                          returnString = portSurchargeType.getSurchargeType();
                      }
                %>        
                <tr class="TableLeftWhite" id="row<%=i%>">
                    <td height="20" width="10%"><input type="radio" value="<%=returnString%>" name="optSelect" onClick="highlightRadioTD('row<%=i%>');"></td>
                    <td height="20" width="20%"><%=portSurchargeType.getSurchargeType()%></td>
                    <td height="20" width="60%"><%=portSurchargeType.getSurchargeDescription()%></td>
                    <td height="20" width="10%"><%=portSurchargeType.getSurchargeTypeStatus()%></td>
                </tr>   
                <%        
                  }//end for
                %>    
                </table>
                <%//Begin:Page Selection Section%>
                <%
                    request.setAttribute("rutPage",rutPage);
                    String linkPage = servURL + "/RrcStandardSrv?service=ui.misc.help.RcmPortSurchargeTypeHelpSvc";
                %>
                <jsp:include page="<%=pageSelectionURL%>" flush="true">
                    <jsp:param name="linkPage" value="<%=linkPage%>"/>
                </jsp:include>
                <%//End:Page Selection Section%>
                <%    
                }//end if
                %>        
        </td>
    </tr>
</table>
</form>
</body>
<%//Begin:Footer Section%>
    <jsp:include page="<%=shortFooterURL%>" flush="true">
        <jsp:param name="errCode" value="<%=errCode%>"/>
        <jsp:param name="errMsg" value="<%=errMsg%>"/>
        <jsp:param name="msg" value="<%=msg%>"/>
    </jsp:include>
<%//End: Footer Section%>
</html>