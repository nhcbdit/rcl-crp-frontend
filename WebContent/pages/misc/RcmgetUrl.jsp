<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Sarawut Anuckwattana 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
1   04/07/19       Sarawut A.                 				Remove add FileSaver.js
2   11/09/19       nawrat1	                 				Remove usage of userData.coutnry
-------------------------------------------------------------------------------------------------------- --%>
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>

    <script type="text/javascript">
	    	   
	    	 
	function GetUrl(referrer){
		if(referrer != undefined){
		//	console.log(referrer)
			
	    	 var urlchildExpire;
	    	 //marlin-ce == jboss
	    	// referrer = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/SpLoginServ?servaction=doLogin";
	    	if(referrer != ""){
	    		 var path = referrer.split( '/' );
		    		var numser;
			    	 if(referrer.indexOf('-ce') < 0){
			    		for (i = 0; i < path.length; i++) {
			    			if(path[i].indexOf('rclgroup') > 0){
			    				 numser = i;
			    			}
						}
			    		//console.log("path[numser] :: "+path[numser]);
			    		urlchildExpire = path[numser];
			    		var UserData = JSON.stringify(rptGetDataFromSingleArea("h3-hidden"));
			    		console.log("userData :: " + UserData);
			    		sessionStorage.setItem("urlchildExpire", urlchildExpire);
			    		sessionStorage.setItem("userData", UserData);
					}
			    	
			    	
	    	}
		}
		
	}
    </script>
