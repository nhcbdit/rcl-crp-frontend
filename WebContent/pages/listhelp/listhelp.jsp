<%@ include file="/pages/misc/RcmInclude.jsp"%>
<%@ page import="com.rclgroup.dolphin.web.common.RcmConstant"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>List Help</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<%@include file="../include/tab_js.jsp" %>
<%@include file="../include/datatable_js.jsp" %>
<%@include file="../include/datetimepicker_js.jsp" %>


<link rel="stylesheet" type="text/css" href="../../css/rcl.css">

<script language="Javascript1.2" src="<%=jsURL%>/RutHelp.js"></script>
<script>
function getPort(){
	openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmPortHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm&retName=txtPort');
}

function getVessel() {

	openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmVesselScheduleHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=txtVessel');
}

function getAgent() {
	openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmAgentHelpSvc&type=<%=RrcStandardUim.GET_WITH_USER_LEVEL_ACTIVE_STATUS%>&pageAction=new&formName=frm0&retName=txtVessel');
}

</script>
</head>
<body>

<form id="frmHelp" name="frmHelp" >

	<div class="rcl-standard-search-header">
		<div class="row">
			<div class="col-md-2">
				<label>Vessel</label>
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtVessel" placeholder="">
					<label for="txtVessel" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" onclick="getVessel()"></label>
				</div>
			</div>


			<div class="col-md-2">
				<label>Agent</label>
				<div class="rcl-standard-input-wrapper">
					<input type="text" class="rcl-standard-form-control" autocomplete="on" id="txtVessel" placeholder="">
					<label for="txtVessel" class="fa fa-search-plus rcl-standard-input-icon" style="font-size:14px" onclick="getAgent()"></label>
				</div>
			</div>

		</div>
	</div>

</form>
</body>
</html>