<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Nutsu 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../include/tab_js.jsp" %>
	<%@include file="../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

	<script src="${context}/js/bkg/rutPowerTable.js"></script>
	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>

	<style>
	/* Nutsu */
    .label > .rcl-standard-form-control {
    	 border: 0px;
    }
    
    .rptEvenRow > .label > .rcl-standard-form-control {
	    background-color: #f8f9fa;
	}
	
	/* Nutsu */
	.text-center > .rcl-standard-form-control {
		text-align: center;
	}
	
	.text-left > .rcl-standard-form-control {
		text-align: left;
	}
	
	.text-right > .rcl-standard-form-control {
		text-align: right;
	}

	</style>

	<title>Call Report</title>


</head>
<body id="qtn001">

	<jsp:include page='../include/header.jsp'>
	    <jsp:param name="pageHeader" value="Prediction Setup"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0">

		<rcl:area id="s0-header" title=" Search" areaMode="search" buttonList="New Search Reset" onClickList="new search reset">

		  <div class="row" style="margin-top: 20px;">
		  		<rcl:text id="s0-userId" label="USer ID or Name" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(10)"></rcl:text>		
			
				<rcl:text id="s0-pol" label="Port" classes="div(col-sm-2 col-md-2 col-lg-2)"
					lookup="tbl(VRL_PORT) rco(CODE NAME) rid(s0-pol) s0-polName) sco(CODE) sva(s0-pol)">Port
				</rcl:text>
				
				<rcl:select id="s0-status"
						label="Status"
						classes="div(col-sm-2 col-md-2 col-lg-2)"
						selectTable="Direction" />	
		  </div>
		 
		  </rcl:area>
    </form>

	<div class="tblHeader">
	    <%-- film adjust --%>
	    <rcl:area id="t0-area" title="Person in charge List"
			classes="div(container-fluid pl-1 pr-1)" areaMode="table" >
					
			<div class="row border" style="margin-right: 0px; margin-left: 0px;">
				<div class="col-sm-1 col-md-1 text-center font-weight-bold" style="">
	                <label class="rcl-std">No#</label>
	            </div>
	            <div class="col-sm-3 col-md-1 text-center font-weight-bold">
	                <label class="rcl-std">Port</label>
	            </div>
	            <div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold">
	                <label class="rcl-std" >Operation Manager</label>
	            </div>
	            <div class="col-sm-6 col-md-1 col-lg-1 text-center font-weight-bold">
	                <label class="rcl-std">Mobile Phone</label>
	            </div>
	            <div class="col-sm-6 col-md-1 col-lg-1 text-center font-weight-bold">
	                <label class="rcl-std">Company Phone</label>
	            </div>
	            <div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold">
	                <label class="rcl-std">Email</label>
	            </div>
	            <div class="col-sm-6 col-md-1 text-center font-weight-bold">
	                <label class="rcl-std">Status</label>
	            </div>   
				
			</div>
		</rcl:area>
	</div>
	
   <div id="t1-area" class="tblArea container-fluid border border-secondary rounded shadow-lg pb-0 ascanList2">
        <div id="t1-row" class="tblRow row border" style="padding-top: 3px;">
			<rcl:text id="t1-no" name="no" classes="div(col-sm-1 col-md-1 col-lg-1 text-center label)" options="readonly" />
			<rcl:text id="t1-port" name="port" classes="div(col-sm-4 col-md-1 col-lg-1 text-center label)" options="readonly" />
			<rcl:text id="t1-operationManager" name="operationManager" classes="div(col-sm-6 col-md-2 col-lg-2 text-left label)" options="readonly" />
			<rcl:text id="t1-mobilePhone" name="mobilePhone" classes="div(col-sm-4 col-md-1 text-left label)" options="readonly" />
			<rcl:text id="t1-companyPhone" name="companyPhone" classes="div(col-sm-4 col-md-1 text-left label)" options="readonly" />
			<rcl:text id="t1-email" name="email" classes="div(col-sm-4 col-md-2 col-lg-2 text-left label)" options="readonly" />
			<rcl:text id="t1-status" name="status" classes="div(col-sm-6 col-md-1 text-center label)" options="readonly" />
		</div>
	</div>
		
	<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>

<script type="text/javascript">
$(document).ready(function() {
	setHeaderTitle("Person in charge setup");
	setHeaderOptions(true,"back","save");
	
	getPredictionSetupList();
})

	
function getPredictionSetupList() {
	//getDataFormServerToTable("WS_BKG_SEARCH_BOOKING", getDataAreaToObject("s0-header"), "t0-area");
	var data = [{
	  "no": 1,
	  "port": "EVA",
	  "operationManager": "edu.illinois.Fintone",
	  "mobilePhone": "38-396-4174",
	  "companyPhone": "82-888-8882",
	  "email": "smatthews0@cargocollective.com",
	  "status": "Inactive"
	}, {
	  "no": 2,
	  "port": "GIM",
	  "operationManager": "com.salon.Ronstring",
	  "mobilePhone": "71-504-9117",
	  "companyPhone": "95-551-4421",
	  "email": "rstranahan1@vistaprint.com",
	  "status": "Inactive"
	}, {
	  "no": 3,
	  "port": "PBIP",
	  "operationManager": "com.mediafire.Cardguard",
	  "mobilePhone": "40-220-1716",
	  "companyPhone": "90-221-6658",
	  "email": "dharmes2@redcross.org",
	  "status": "Inactive"
	}, {
	  "no": 4,
	  "port": "CACC",
	  "operationManager": "com.reference.Asoka",
	  "mobilePhone": "80-473-7707",
	  "companyPhone": "02-670-7971",
	  "email": "adanels3@bloglines.com",
	  "status": "Active"
	}, {
	  "no": 5,
	  "port": "NIM",
	  "operationManager": "com.flickr.Toughjoyfax",
	  "mobilePhone": "44-782-0601",
	  "companyPhone": "38-167-8084",
	  "email": "cdeangelis4@163.com",
	  "status": "Inactive"
	}, {
	  "no": 6,
	  "port": "FTA",
	  "operationManager": "com.canalblog.Keylex",
	  "mobilePhone": "92-146-9910",
	  "companyPhone": "68-101-5885",
	  "email": "cridsdale5@flickr.com",
	  "status": "Active"
	}, {
	  "no": 7,
	  "port": "BELFB",
	  "operationManager": "com.hp.Redhold",
	  "mobilePhone": "11-787-0540",
	  "companyPhone": "31-031-7921",
	  "email": "tfalvey6@instagram.com",
	  "status": "Inactive"
	}, {
	  "no": 8,
	  "port": "NEOT",
	  "operationManager": "edu.upenn.Ronstring",
	  "mobilePhone": "24-284-2626",
	  "companyPhone": "07-567-8503",
	  "email": "mpenhearow7@sohu.com",
	  "status": "Active"
	}, {
	  "no": 9,
	  "port": "IXYS",
	  "operationManager": "com.engadget.Home Ing",
	  "mobilePhone": "73-982-3566",
	  "companyPhone": "43-054-7798",
	  "email": "tsvanetti8@4shared.com",
	  "status": "Inactive"
	}, {
	  "no": 10,
	  "port": "GNCA",
	  "operationManager": "com.facebook.Konklab",
	  "mobilePhone": "02-547-1765",
	  "companyPhone": "32-629-0341",
	  "email": "ledel9@engadget.com",
	  "status": "Inactive"
	}, {
	  "no": 11,
	  "port": "CORR",
	  "operationManager": "com.dedecms.Alphazap",
	  "mobilePhone": "54-784-5624",
	  "companyPhone": "19-388-8646",
	  "email": "ebampfortha@rambler.ru",
	  "status": "Active"
	}, {
	  "no": 12,
	  "port": "CMRE^D",
	  "operationManager": "edu.upenn.Otcom",
	  "mobilePhone": "58-236-7075",
	  "companyPhone": "99-136-4328",
	  "email": "holearyb@bbc.co.uk",
	  "status": "Active"
	}, {
	  "no": 13,
	  "port": "PLYAW",
	  "operationManager": "gov.nps.Fintone",
	  "mobilePhone": "97-430-9064",
	  "companyPhone": "15-289-7781",
	  "email": "cprattenc@epa.gov",
	  "status": "Active"
	}, {
	  "no": 14,
	  "port": "BF.B",
	  "operationManager": "com.economist.Cookley",
	  "mobilePhone": "28-411-6369",
	  "companyPhone": "50-501-7469",
	  "email": "bsneakerd@columbia.edu",
	  "status": "Inactive"
	}, {
	  "no": 15,
	  "port": "EVLV",
	  "operationManager": "com.xinhuanet.Subin",
	  "mobilePhone": "56-396-8998",
	  "companyPhone": "29-762-3667",
	  "email": "awibrewe@ucla.edu",
	  "status": "Inactive"
	}, {
	  "no": 16,
	  "port": "EBSB",
	  "operationManager": "com.tripadvisor.Y-Solowarm",
	  "mobilePhone": "44-066-6715",
	  "companyPhone": "06-956-9641",
	  "email": "dhinchcliffef@altervista.org",
	  "status": "Active"
	}, {
	  "no": 17,
	  "port": "EQCO",
	  "operationManager": "com.blogger.Keylex",
	  "mobilePhone": "02-783-4081",
	  "companyPhone": "57-640-8278",
	  "email": "iplacidog@shop-pro.jp",
	  "status": "Inactive"
	}, {
	  "no": 18,
	  "port": "BRKS",
	  "operationManager": "com.cnn.Prodder",
	  "mobilePhone": "86-261-6908",
	  "companyPhone": "58-888-7573",
	  "email": "jtrivetth@wufoo.com",
	  "status": "Inactive"
	}, {
	  "no": 19,
	  "port": "VJET",
	  "operationManager": "org.drupal.Prodder",
	  "mobilePhone": "23-730-4741",
	  "companyPhone": "57-671-9691",
	  "email": "bwoolardi@theglobeandmail.com",
	  "status": "Inactive"
	}, {
	  "no": 20,
	  "port": "HWCC",
	  "operationManager": "com.theguardian.Ronstring",
	  "mobilePhone": "52-523-3471",
	  "companyPhone": "46-624-7241",
	  "email": "bbennj@devhub.com",
	  "status": "Inactive"
	}];
	
	rptTableInit("t1-area");
	rptAddData("t1-area", data);
	rptDisplayTable("t1-area");
}
</script>


</body>
</html>
