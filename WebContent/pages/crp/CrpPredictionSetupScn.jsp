<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Nutsu 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../include/tab_js.jsp" %>
	<%@include file="../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

	<script src="${context}/js/bkg/rutPowerTable.js"></script>
	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>

	<style>
	.ow-anywhere {
   		overflow-wrap: anywhere;
		}

 	.cssSection{
    	padding-right: 0px;
    	}

    .listsearch{
    	margin-bottom: 0px;
    	font-weight: 500;
    	word-wrap: break-word;
    }
	input[disabled].listsearch  {
		cursor: text;
		color: inherit;
		background: transparent;
	}

	</style>

	<title>Call Report</title>


</head>
<body id="qtn001">

	<jsp:include page='../include/header.jsp'>
	    <jsp:param name="pageHeader" value="Prediction Setup"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0">

		<rcl:area id="s0-header" title=" Search" areaMode="search" buttonList="Search Reset" onClickList="search reset">

		  <div class="row" style="margin-top: 20px;">
			 	<div class="col-sm-4 col-md-2 col-lg-2 pr-0">
			 		<label id="s0-labelApporver" style="padding-left: 15px;">Prediction name</label>
			 	</div>
			 	<rcl:text id="t1" classes="div(col-sm-2 col-md-2)"></rcl:text>
			
		  </div>
		 
		  </rcl:area>
    </form>

	<div class="tblHeader">
	    <%-- film adjust --%>
	    <rcl:area id="t0-area" title="Quotation List"
			classes="div(container-fluid pl-1 pr-1)" areaMode="table" >
	
			<div class="row border" style="margin-right: 0px; margin-left: 0px;">
	
				<div class="text-center" style="font-weight: bold; width:50px;">
					<label id="_t0-labelBooking" class="rcl-std">#</label>
				</div>
				
				<div class="col-sm-3 col-md-3 col-lg-3 pr-0 " style="font-weight: bold;">
					<label class="rcl-std">Prediction Name</label>
				</div>
				
				<div class="col-sm-3 col-md-2 pr-0 " style="font-weight: bold;">
					<label id="_t0-status" class="rcl-std">Allow to adjust</label>
				</div>
				
				<div class="col-sm-6 col-md-2 offset-sm-4 offset-md-0 "
					style="font-weight: bold;">
					<label class="rcl-std">Status</label>
				</div>
				
			</div>
		</rcl:area>
	</div>
	
	<div id="t1-container" class="tblArea" style="overflow: auto">
		<div id="t1-row" class="tblRow row pt-1">
			<div class="container-fluid" >
				<div class="row gridRow">
					<div class="text-center" style="font-weight: bold; width:50px;">
						<label id="t1-no" class="tblField"></label>
					</div>
					
					<div class="col-sm-3 col-md-3 col-lg-3 pr-0 " >
				  		 <p id="t1-predictionName" name="predictionName" class="tblField"></p>
					</div>
					
					<div class="col-sm-3 col-md-2 pr-0 ">
				  		 <input id="t1-allowAdjustFlag" name="allowAdjustFlag" type="checkbox" 
				  		 	class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField">
					</div>
					
					<div class="col-sm-6 col-md-2 offset-sm-4 offset-md-0 ">
				  		 <p id="t1-status" name="status" class="tblField"></p>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>

<script type="text/javascript">
$(document).ready(function() {
	setHeaderTitle("Prediction Setup");
	setHeaderOptions(true,"back","save");
	
	getPredictionSetupList();
})

	
function getPredictionSetupList() {
	//getDataFormServerToTable("WS_BKG_SEARCH_BOOKING", getDataAreaToObject("s0-header"), "t0-area");
	var data = [
		{ 
			"no" : "1",
			"predictionName" : "manufacturing and design, a mockup, or mock-up",
			"allowAdjustFlag" : true,
			"status" : "Active"
		},
		{ 
			"no" : "2",
			"predictionName" : "screen layouts, through realistic bitmaps, to semi functional user i",
			"allowAdjustFlag" : true,
			"status" : "Active"
		},
		{ 
			"no" : "3",
			"predictionName" : "manufacturing and design, a mockup, or mock-up",
			"allowAdjustFlag" : true,
			"status" : "Active"
		},
		{ 
			"no" : "4",
			"predictionName" : " is a scale or full-size model of a design or ..",
			"allowAdjustFlag" : true,
			"status" : "Active"
		},
		{ 
			"no" : "5",
			"predictionName" : "Software UI mockups can range from very simple hand drawn screen layouts",
			"allowAdjustFlag" : true,
			"status" : "Active"
		}
	
	];
	
	rptTableInit("t1-container");
	rptAddData("t1-container", data);
	rptDisplayTable("t1-container");
}
</script>


</body>
</html>
