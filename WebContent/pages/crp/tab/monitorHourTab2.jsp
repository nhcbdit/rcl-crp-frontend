<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%-- <%@include file="/pages/misc/RcmInclude.jsp"%> --%>

<div class="tblHeader" style="margin:10px;">
 	<div id="t2-history" title="" class="container-fluid pl-0 pr-0"  >
		
		<div class="row border" style="margin-right: 0px; margin-left: 0px;">
			<div class="seqno text-center font-weight-bold">
                <label class="rcl-std">#</label>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std">Port call</label>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Service</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Vessel</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Voyage</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >ETA Date</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >ETA Time</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Previous port call</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Version</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Status</label>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" ></label>
            </div>
        </div>
	</div>
	
	 <div id="t2-tab2-data" class="tblArea container-fluid pb-0">
       <div id="t2-rows" class="tblRow row border" style="padding-top: 3px; overflow: auto">
			<div class="container-fluid" >
				<div class="row gridRow" style="padding-top:5px;">
				
					<div class="seqno text-center font-weight-bold">
		                <p id="t2-seqno" name="seqno" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		            <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-portCall" name="portCall" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		            <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-service" name="service" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		               <p id="t2-vessel" name="vessel" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-voyage" name="voyage" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-etaDate" name="etaDate" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-etaTime" name="etaTime" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-previousPortCall" name="previousPortCall" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-version" name="version" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		             <div class="col-sm-1 col-md-1 col-lg-1">
		                <p id="t2-status" name="status" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
		            </div>
		            
					<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 pl-2">
		                <a onclick="gotoMaintenance('#-RowId-#')"><em class="fa fa-edit"></em></a>
		            </div>
		            
				
				</div>
			</div>
		</div>
	</div>

	<div id="t2-paging" class="container-fluid pl-0 pr-0"></div>
	
</div>


<script type="text/javascript">
$(document).ready(function() {
	initial("t2-tab2-data");
})


</script>