<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%-- <%@include file="/pages/misc/RcmInclude.jsp"%> --%>

<style>

.dot-grey {
  height: 15px;
  width: 15px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
}

.dot-green {
  height: 15px;
  width: 15px;
  background-color: #36980a;
  border-radius: 50%;
  display: inline-block;
}

</style>
<div class="tblHeader" style="margin:10px;">
 	<div id="t5-history" title="" class="container-fluid pl-0 pr-0"  >	
		<div class="row border" style="margin-right: 0px; margin-left: 0px;">
			<div class="seqno text-center font-weight-bold">
                <label class="rcl-std">#</label>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std">Port call</label>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Service</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Vessel</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Voyage</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >ETA Date</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >ETA Time</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Previous port call</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Version</label>
            </div>
             <div class="col-sm-1 col-md-1 col-lg-1 text-center font-weight-bold">
                <label class="rcl-std" >Actual</label>
            </div>
            <div class="col-sm-1 col-md-1 col-lg-1 text-left font-weight-bold">
                <label class="rcl-std" >Send date time</label>
            </div>
        </div>
	</div>
	
 	<div id="t5-tab5-data" class="tblArea container-fluid pb-0">
        <div id="t5-row" class="tblRow row pt-1" style="padding-top: 3px; overflow: auto">
			<div class="container-fluid" >
				<div class="row gridRow" style="padding-top:5px;">
					<div class="seqno">
				  		<p id="t5-seqno" name="seqno" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-portCall" name="portCall" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-service" name="service" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-vessel" name="vessel" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-voyage" name="voyage" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-etaDate" name="etaDate" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-etaTime" name="etaTime" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-previousPortCall" name="previousPortCall" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField  text-center" data-ct="bt-text"></p>
					</div>
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-version" name="version" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField  text-center" data-ct="bt-text"></p>
					</div>		
					
					<div class="col-sm-1 col-md-1 col-lg-1 text-center label">
						<span id="t5-editFlag" class="dot-grey"></span>
					</div>
		<%-- 			<rcl:text id="t5-flagActual" name="flagActual" classes="div(col-sm-1 col-md-1 col-lg-1 text-center label)" options="readonly" /> --%>
					
					<div class="col-sm-1 col-md-1 col-lg-1">
				  		 <p id="t5-submitDate" name="submitDate" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField  text-center" data-ct="bt-text"></p>
					</div>	
					
					<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 pl-2">
		                <a onclick="gotoMaintenance('#-RowId-#')"><em class="fa fa-edit"></em></a>
		            </div>
			</div>
			</div>

		</div>
	</div>

	<div id="t5-paging" class="container-fluid pl-0 pr-0"></div>
	
</div>



<script type="text/javascript">
$(document).ready(function() {
	initial("t5-tab5-data");
})

</script>