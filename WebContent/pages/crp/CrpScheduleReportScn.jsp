<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Nutsu 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../include/tab_js.jsp" %>
	<%@include file="../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

<%-- 	<script src="${context}/js/bkg/rutPowerTable.js"></script> --%>
	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/qtnPowerTable.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>
	
	<script src="${context}/js/crpCommon.js"></script>

	<style>
	/* Nutsu */
	.rcl-standard-font {
		font-size : 10px;
	}
	
    .label > .rcl-standard-form-control {
    	background-color: #f8f9fa;
    }
    
    .rptEvenRow > .label > .rcl-standard-form-control {
	    background-color: #f8f9fa;
	}
    
    
	/* Nutsu */
	.text-center > .rcl-standard-form-control {
		text-align: center;
	}
	
	.text-left > .rcl-standard-form-control {
		text-align: left;
	}
	
	.text-right > .rcl-standard-form-control {
		text-align: right;
	}


.nav-tabs {
	margin : 0 20px 0 20px;
	width: 100%
}

.nav-item {
	width: 20%
}

.nav-tabs .nav-link {
    width: 100%;
}


/* Override tab bg color */
#p0-id-18hours10reports {
	color: #fff;
	background-color: #dc3545;
}

#p0-id-1924hours2reports {
	color: #fff;
	background-color: #ff7007;
}

#p0-id-2536hours5reports {
	color: #fff;
	background-color: #ffc107;
}

#p0-id-3748hours112reports {
	color: #fff;
	background-color: #28a745;
}

#p0-id-Vesseldepartured{
	color: #fff;
	background-color: #6c757d;
}

.seqno {
	width: 50px !important;
}

.w-5 {
	width: 5%!important;
}

.w-10 {
	width: 10%!important;
}
	</style>

	<title>Call Report</title>


</head>
<body>

	<jsp:include page='include/header.jsp'>
	    <jsp:param name="pageHeader" value="Prediction Setup"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0">

		<rcl:area id="s0-header" title=" Search" areaMode="search" buttonList="Export_to_Excel Reset" onClickList="doSearch doReset">
		  <div class="row" style="margin-top: 20px;">
				<rcl:text id="s0-port" label="Port" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(5) upc" 
					 lookup="tbl(VRL_PORT) rco(CODE) rid(s0-port)" />
							
				<rcl:text id="s0-service" label="Service" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(5) upc" 
					lookup="tbl(VRL_SERVICE)
	  			  		  rco(CODE)
	  			  		  rid(s0-service)" />
				
				<rcl:text id="s0-vessel" label="Vessel" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(5) upc" 
					lookup="tbl(VRL_VESSEL) rco(CODE) rid(s0-vessel)"/>

				<rcl:text id="s0-voyage" label="Voyage" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(10) upc" 
					lookup="tbl(VRL_VOYAGE) rco(CODE) rid(s0-voyage)" />

		  </div>

		  <div class="row">
				<rcl:text id="s0-region" label="Region" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(3) upc"
					  	lookup="tbl(VRL_CRP_REGION) rco(CODE) rid(s0-region)" />	
				
				<rcl:text id="s0-country" label="Country Code" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(2) upc"
					  	lookup="tbl(VRL_COUNTRY) rco(CODE) rid(s0-country)"/>	
					
				<rcl:select id="s0-status"
						label="Status"
						classes="div(col-sm-2 col-md-2 col-lg-2)"/>
								
				<rcl:text id="s0-servicePic" label="Service PIC" classes="div(col-sm-2 col-md-2 col-lg-2)" check="upc"/>	
		  </div>
		  
		  <div class="row">
		  		<rcl:date id="s0-etaFrom" label="ETA From" classes="div(col-sm-2 col-md-2 col-lg-2)" defaultValue="today"/>
		  		
		  		<rcl:date id="s0-etaTo" label="To" classes="div(col-sm-2 col-md-2 col-lg-2)" defaultValue="today"/>
		  		
		  </div>
		  
	   </rcl:area>
    </form>
	
		
	<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>

<script type="text/javascript">


var searchInfo = {
	"searchParam" : null
}

initialSearchParam();

//Fix IE
var userToken = '${userData.userToken}';

$(document).ready(function() {
	setHeaderTitle("Schedule Monitoring Report");
	setHeaderOptions(true);
	
	if( sessionStorage.getItem("userData") ){
		//console.log("exist userData = " + sessionStorage.getItem("userData"));
	}else{
		var userData = crpGetUserDataFromHeader();
		
		//Fix IE
		userData.userToken = userToken;
		
		// Encode the String
		var encodedString = btoa(JSON.stringify(userData));
		sessionStorage.setItem("userData", encodedString);
	}

	initilDropdown();
})
	
	
function initilDropdown(){
	console.log("initilDropdown ...");
	
	crpGetResultAjax("WS_CRP_STATUS", null)
	.done(function(response) {
		var paramOutData = response.P_O_DATA;
		
		var resultData = [];
		for (var i = 0; i < paramOutData.length; i++) {
			var row = paramOutData[i];
			
			var rowData = {
				"code" : row.STATUS_CODE,
				"desc" : row.DESCRIPTION
			};
			
			resultData.push(rowData);
		}
		
		//Set Dropdown
		var drowdownStatus = $('#s0-status');
		drowdownStatus.html("");
		
		$.each(resultData, function(index, item) {
			drowdownStatus.append(
		        $('<option></option>').val(item.code).html(item.desc)
		    );
		});
		
		drowdownStatus.val("");
		 
	});
	
}	


function initialSearchParam(){

	if( searchInfo.searchParam == null ){
		//Initial first
		var dataPost = {
		    "port" : "",
		    "service" : "",
		    "vessel" : "",
		    "voyage" : "",
		    "region" : "",
		    "country" : "",
		    "status" : "",
		    "servicePic" : "",
		    "etaFrom" : "",
		    "etaTo" : ""
		    
		};
	
		
		searchInfo.searchParam = dataPost;		
	}
	
	//doSearch();
}

function doReset(){
	$('#s0-port').val("");
	$('#s0-service').val("");
	$('#s0-vessel').val("");
	$('#s0-voyage').val("");
	
	$('#s0-region').val("");
	$('#s0-country').val("");
	$('#s0-status').val("G");
	$('#s0-servicePic').val("");
	
	$('#s0-etaFrom').val("");
	$('#s0-etaTo').val("");
	
	var emptyData = [];
	rptClearDisplay(tableDivId);
	rptAddData(tableDivId, emptyData);
	rptDisplayTable(tableDivId);
}

function doSearch(){

	//Get parameter 
	var dataPost = crp_GetDataAreaToObject("s0-header");
	searchInfo.searchParam = dataPost;
	fetchData();
}

function fetchData(){
	var searchParam = searchInfo.searchParam;
	
	//console.log(">" + JSON.stringify(searchInfo.searchParam));
	
	crpGetResultAjax("WS_CRP_SCHEDULE_REPORT", searchParam).done(handleResponseRpt);
}



function handleResponseRpt(results, responseText, xhr){
	if( results.resultCode === '000'){
		console.log('download = ' + xhr.getResponseHeader("download_url"));
		var url = xhr.getResponseHeader("download_url");
		
		console.log("URL : " + url);
		var a = document.createElement('A');
    	a.href = url;
    	document.body.appendChild(a);
    	a.click();
    	document.body.removeChild(a);	
	}
}

	
</script>


</body>
</html>