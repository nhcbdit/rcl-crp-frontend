<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Nutsu 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../include/tab_js.jsp" %>
	<%@include file="../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/qtnPowerTable.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>

	<style>
	.ow-anywhere {
   		overflow-wrap: anywhere;
		}

 	.cssSection{
    	padding-right: 0px;
    	}

    .listsearch{
    	margin-bottom: 0px;
    	font-weight: 500;
    	word-wrap: break-word;
    }
	input[disabled].listsearch  {
		cursor: text;
		color: inherit;
		background: transparent;
	}

	</style>

	<title>Call Report</title>


</head>
<body id="qtn001">

	<jsp:include page='../include/header.jsp'>
	    <jsp:param name="pageHeader" value="Quotation Search"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0">
<%-- 		<rcl:area id="t0-area" title="Global Setup" classes="div(container-fluid pl-1 pr-1)"> --%>
			<rcl:tabGroup  tabList="Time_Line Other" activeTab="TimeLine">
				<rcl:tabContentPane tabName="TimeLine">
					
					 <div class="row labelApporver" style="margin-top: 10px;">
					 	<div class="col-sm-4 col-md-3 pr-0">
					 		<label id="s0-labelApporver" style="padding-left: 15px;">Report generate before vessel arrive (hours)</label>
					 	</div>
					 	<rcl:text id="t1" classes="div(col-sm-1 col-md-1)"></rcl:text>
				
					 </div>
					 
					 <div class="row labelApporver" style="margin-top: 10px;">
					 	<div class="col-sm-4 col-md-3 pr-0">
					 		<label id="s0-labelApporver" style="padding-left: 15px;">Send alert to PIC before vessel arrive (hours)</label>
					 	</div>
					  	<rcl:text id="t1" classes="div(col-sm-1 col-md-1)"></rcl:text>
					 </div>
					 
					  <div class="row labelApporver" style="margin-top: 10px;">
					 	<div class="col-sm-4 col-md-3 pr-0">
					 		<label id="s0-labelApporver" style="padding-left: 15px;">1st Notification to service owner before vessel arrive (hours)</label>
					 	</div>
					  	<rcl:text id="t1" classes="div(col-sm-1 col-md-1)"></rcl:text>
					 </div>
					 
					  <div class="row labelApporver" style="margin-top: 10px;">
					 	<div class="col-sm-4 col-md-3 pr-0">
					 		<label id="s0-labelApporver" style="padding-left: 15px;">2nd Notification to service owner before vessel arrive (hours)</label>
					 	</div>
					  	<rcl:text id="t1" classes="div(col-sm-1 col-md-1)"></rcl:text>
					 </div>
					 
					  <div class="row labelApporver" style="margin-top: 10px;">
					 	<div class="col-sm-4 col-md-3 pr-0">
					 		<label id="s0-labelApporver" style="padding-left: 15px;">System auto submit report before vessel arrive (hours)</label>
					 	</div>
					  	<rcl:text id="t1" classes="div(col-sm-1 col-md-1)"></rcl:text>
					 </div>
					 
				</rcl:tabContentPane>
				
				<rcl:tabContentPane tabName="Other">
				</rcl:tabContentPane>
				
			</rcl:tabGroup>
<%-- 		</rcl:area> --%>
		
    </form>

	<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>


<script type="text/javascript">
$(document).ready(function() {
	setHeaderTitle("Global Setup");
	setHeaderOptions(true,"back","save");
})
</script>

</body>
</html>
