<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../../include/tab_js.jsp" %>
	
	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

	<script src="${context}/js/bkg/rutPowerTable.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/quotation.js"></script>
	
	<script src="${context}/js/crpCommon.js"></script>

	<style>

		body {
				overflow-x:hidden;
				overflow-y:hidden;
				margin: 0 !important;
			 }
			 
		/* Nutsu */
		.text-center > .rcl-standard-form-control {
			text-align: center;
		}
	</style>

<title>Revision</title>
</head>
<body style="margin:0px;">
	<jsp:include page='../include/header.jsp'></jsp:include>
<div class="tblHeader" style="padding:10px;">
	<rcl:area id="t0-requestAdviceArea" title="Revision" classes="div(container-fluid pl-1 pr-1)" areaMode="table" >
		<div class="row" style="padding-left:10px;">
	   	   <div class="col-sm-2 col-md-2 col-lg-1 text-center font-weight-bold">
	           <label class="rcl-std">#</label>
	       </div>
	
	       <div class="col-md-3" style="font-weight:bold;">
	           <label class="rcl-std">Subject</label>
	       </div>
	
	       <div class="col-md-3" style="font-weight:bold;">
	           <label class="rcl-std">Generated Data</label>
	       </div>
	
	       <div class="col-md-3" style="font-weight:bold;">
	           <label class="rcl-std" >Current Version</label>
	       </div>
	   </div>
	   
	</rcl:area>
	
	
	<div id="t1-requestAdviceArea" class="tblArea container-fluid border border-secondary rounded shadow-lg"> 
		<div id="t1-rows" class="tblRow row border" style="padding-top: 3px;">	 
			<rcl:text id="t1-seqno" name="seqno" classes="div(col-sm-2 col-md-2 col-lg-1 text-center font-weight-bold) ctr(tblField)" options="readonly" />
			<rcl:text id="t1-subject" name="subject" classes="div(col-md-3) ctr(tblField)" options="readonly"/> 
			<rcl:text id="t1-generatedData" name="generatedData" classes="div(col-md-3) ctr(tblField)" options="readonly"/> 
			<rcl:text id="t1-currentVersion" name="currentVersion" classes="div(col-md-3) ctr(tblField)" options="readonly" />
		</div> 

	</div> 
	
	<div class="row" style="margin-top:10px;">
		<div class="form-group col-sm-12 col-md-12 col-lg-12">
	    	<label for="txtArea1" class="rcl-std">Special Instruction</label>
	    	<textarea class="form-control rcl-std rcl-standard-textbox rcl-standard-font" id="txtSpecialInstruction" rows="2" style="resize: none;" readonly="readonly"></textarea>
		</div>
	</div>
		
	<div class="col-sm-12" style="text-align:right;">
		<button id="d0-close" type="button" class="rcl-standard-button" onclick="window.close();" style="margin-right:5px">Close</button>
	</div>
	
</div>


<script type="text/javascript">

$(document).ready(function() {
	setHeaderOptions(true);
	
	rptTableInit("t1-requestAdviceArea");

	var revisionNo = sessionStorage.getItem("revisionNo");
	var hisVoyageId = sessionStorage.getItem("hisVoyageId");
	
	document.getElementById("t0-requestAdviceAreaHeader").getElementsByTagName('label')[0].innerHTML = "Reviision#" + revisionNo;
	
	//var bookingNo = document.getElementById("hdfbookingNO").value;

	init(revisionNo, hisVoyageId);
});

function init(revisionNo, hisVoyageId) {
	var requestParam = { "voyageId" : hisVoyageId };
	
	crpGetResultAjax("WS_CRP_GET_CALL_REPORT_HIS_PREDICTION", requestParam)
	.done(function(response) {
		var paramOutData = response.P_O_DATA;

		console.log(" >> " + JSON.stringify(response) );
		
		var resultData = [];
		for (var i = 0; i < paramOutData.length; i++) {
			var row = paramOutData[i];
			
			var rowData = {
				"seqno" : i + 1,
				"subject" : row.IND_NAME,
				"generatedData" : row.HIST_PREDICTION_DATA,
				"currentVersion" : row.CURR_PREDICTION_DATA
			}
			resultData.push(rowData);
		}
		
		rptClearDisplay("t1-requestAdviceArea");
		rptAddData("t1-requestAdviceArea", resultData);
		rptDisplayTable("t1-requestAdviceArea");
	});

	
}

</script>


</body>
</html>