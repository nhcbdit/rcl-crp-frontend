<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Nutsu 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../include/tab_js.jsp" %>
	<%@include file="../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

	<script src="${context}/js/bkg/rutPowerTable.js"></script>
	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>

	<script src="${context}/js/crpCommon.js"></script>
	
	<style>
	/* Nutsu */
	
	.rcl-standard-font {
		font-size : 12px;
	}
	
    .label > .rcl-standard-form-control {
    	  background-color: #f8f9fa;
    }
    
    .rptEvenRow > .label > .rcl-standard-form-control {
	    background-color: #f8f9fa;
	}
    
	/* Nutsu */
	.text-center > .rcl-standard-form-control {
		text-align: center;
	}
	
	.text-left > .rcl-standard-form-control {
		text-align: left;
	}
	
	.text-right > .rcl-standard-form-control {
		text-align: right;
	}


	a.disabled {
		color: gray;
		pointer-events: none;
	}


	.rcl-standard-button.disabled,
	.rcl-standard-button[disabled]{
	  cursor: not-allowed;
	}
	
	
	a.my-button{
		text-decoration: underline !important;
		color:#7cc242;
		font-size:12px;
		text-align:right;
		text-decoration:none;
		display: inline-block;
		zoom:1;
		*display:inline;
		cursor: pointer;
	}

	/* unvisited link */
	a.my-button:link {
	  color: green !important;
	}
	
	/* visited link */
	a.my-button:visited {
	  color: green !important;
	}
	
	/* mouse over link */
	a.my-button:hover {
	  color: red !important;
	  text-decoration: underline;
	}
	
	/* selected link */
	a.my-button:active {
	  color: yellow !important;
	} 
	
body {
    font-size: 12px;
}

.rcl-standard-font {
	font-weight: normal !important;
}

p {
   margin-top: auto;
   margin-bottom: auto;
}

.my-tag-button {
	height: 20px; 
	width: 150px !important;
	font-size: 0.8rem; 
	padding: 2px 5px 2px 5px;
}
	</style>

	<title>Call Report</title>


</head>
<body>

	<jsp:include page='include/header.jsp'>
	    <jsp:param name="pageHeader" value="Quotation Search"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0" id="f1-info">
<!-- 		<div id="s1-details-s1-row" style="margin-top:10px; margin-bottom:10px"> -->
			<div id="t41row" class="tblRow">
				
			<ul class="pagination justify-content-center">
				<li class="page-item disabled" style="width: 100%; text-align: center;">
					<a id="btn-prev" class="page-link" href="javascript:void(0)" style="height: 100%" >
<!-- 						<i class="fa fa-arrow-left" aria-hidden="true"></i> -->
					</a>
				</li>
				<li class="page-item disabled" style="width: 100%; text-align: center;">
					<a id="btn-current" class="page-link" href="#" style="background-color: #3d77cb; color: white; height: 100%"></a>
				</li>
				<li class="page-item disabled" style="width: 100%; text-align: center;">
					<a id="btn-next" class="page-link" href="javascript:void(0)" style="height: 100%" >
<!-- 						<i class="fa fa-arrow-right" aria-hidden="true"></i> -->
					</a>
				</li>
			</ul>

			
			<div class="row">
				<div class="col-sm-2 col-md-1">
			 		<label for="f1-port" style="padding-left: 15px;" class="font-weight-bold">Port</label>
			 	</div>
			 	<rcl:text id="f1-port" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			 	
			 	<div class="col-sm-2 col-md-1">
			 		<label for="f1-terminal" style="padding-left: 15px;" class="font-weight-bold">Terminal</label>
			 	</div>
			 	<rcl:text id="f1-terminal" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			
				<div class="col-sm-4 col-md-6" style="text-align: right;">
					<h6><span class="badge badge-warning mr-2" id="voyageStatus"></span></h6>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-2 col-md-1">
			 		<label for="f1-service" style="padding-left: 15px;" class="font-weight-bold">Service</label>
			 	</div>
			 	<rcl:text id="f1-service" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			 	
			 	<div class="col-sm-2 col-md-1">
			 		<label for="f1-vessel" style="padding-left: 15px;" class="font-weight-bold">Vessel</label>
			 	</div>
			 	<rcl:text id="f1-vessel" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			 	
			 	<div class="col-sm-3 col-md-2">
			 		<label for="f1-voyage" style="padding-left: 15px;" class="font-weight-bold">Voyage</label>
			 	</div>
			 	<rcl:text id="f1-voyage" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			</div>
			
			<div class="row">
				<div class="col-sm-2 col-md-1">
			 		<label for="f1-eta" style="padding-left: 15px;" class="font-weight-bold">ETA</label>
			 	</div>
			 	<rcl:text id="f1-eta" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			 	
			 	<div class="col-sm-2 col-md-1">
			 		<label for="f1-etd" style="padding-left: 15px;" class="font-weight-bold">ETD</label>
			 	</div>
			 	<rcl:text id="f1-etd" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			 	
			 	<div class="col-sm-3 col-md-2">
			 		<label for="f1-servicePic" style="padding-left: 15px;" class="font-weight-bold">Service PIC</label>
			 	</div>
			 	<rcl:text id="f1-servicePic" classes="div(col-sm-2 col-md-2) ctr(dtlField)" check="upc len(5)" options="readonly"></rcl:text>
			</div>
			</div>
		
		
		
		
		<div class="tblHeader" style="margin-top:20px;">
		    <%-- film adjust --%>
		    <rcl:area id="t0-dataPrediction" title="Data Prediction revision#X"
 				classes="div(container-fluid pl-1 pr-1)" areaMode="table" > 
		
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-6" style="margin-top: 5px; margin-bottom: 5px;">
						<a id="d0-printRptLoc" class="btn-sm rcl-standard-button my-tag-button" href="">Print Location Report</a>
						<a id="d0-printRptHq" class="btn-sm rcl-standard-button my-tag-button" href="">Print HQ Report</a>    
					</div>
					
					<div class="col-sm-6 col-md-6 col-lg-6" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
						<button id="d0-save" type="button" class="rcl-standard-button"
							onclick="doSave();">Save
						</button>
						
						<button id="d0-reset" type="button" class="rcl-standard-button"
							onclick="doReset();">Reset
						</button>
						
						<button id="d0-send" type="button" class="rcl-standard-button"
							onclick="doSend();">Send
						</button>
					</div>
				</div>
			
				<div class="row border" style="margin-right: 0px; margin-left: 0px;">
		
					<div class="col-sm-2 col-md-2 col-lg-1 text-center font-weight-bold" style="">
						<label id="_t0-labelSeqno" class="rcl-std">#</label> 
					</div> 
					
					<div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold"> 
						<label class="rcl-std">Subject</label> 
					</div> 
					
					<div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold">
						<label id="_t0-status" class="rcl-std">Generated Data</label> 
					</div> 
					
					<div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold"> 
						<label class="rcl-std">Actual</label> 
					</div> 
					
					<div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold">
						<label class="rcl-std">Performance</label> 
					</div> 
				</div> 
			
				<div id="t1-dataPrediction" class="tblArea container-fluid border border-secondary rounded shadow-lg pb-0 ascanList2"> 
			        <div id="t1-rows" class="tblRow row border" style="padding-top: 3px; overflow: auto">
						<div class="container-fluid" >
							<div class="row gridRow" style="padding-top:5px;">
			        
					        	<div class="col-sm-2 col-md-2 col-lg-1 ">
						  			<p id="t1-seqno" name="seqno" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-center" data-ct="bt-text"></p>
								</div>
								<div class="col-sm-4 col-md-2 col-lg-2">
							  		 <p id="t1-subject" name="subject" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
								</div>
								
			 					<rcl:text id="t1-generatedData" name="generatedData" classes="div(col-sm-4 col-md-2 col-lg-2 text-left label)" /> 
			 					
			 					<div class="col-sm-4 col-md-2 col-lg-2">
							  		 <p id="t1-actualData" name="actualData" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-left" data-ct="bt-text"></p>
								</div>
								
								<div class="col-sm-4 col-md-2 col-lg-2">
							  		 <p id="t1-performance" name="performance" class="listsearch rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField text-right" data-ct="bt-text"></p>
								</div>
							</div>
						</div>
					</div> 
				</div> 
				
				<div class="row" style="margin-top:10px;" >
					<div class="form-group col-sm-12 col-md-12 col-lg-12">
				    	<label for="txtArea1" class="rcl-std">Special Instruction</label>
				    	<textarea class="form-control rcl-std rcl-standard-textbox rcl-standard-font" id="txtSpecialInstruction" rows="3" style="resize: none;"></textarea>
					</div>
				</div>
				
				<div class="row" style="margin-top:0px;" >
					<div class="form-group col-sm-12 col-md-12 col-lg-12">
				    	<label for="txtArea2" class="rcl-std">Delay Reason</label>
				    	<textarea class="form-control rcl-std rcl-standard-textbox rcl-standard-font" id="txtDelayReason" rows="1" style="resize: none;" readonly="readonly"></textarea>
					</div>
				</div>
				
 			</rcl:area> 
			
			
		</div>
		
		
		
		
		<div class="tblHeader">
		 	<rcl:area id="t0-history" title="History"
				classes="div(container-fluid pl-1 pr-1)" areaMode="table" >
				
				<div class="row border" style="margin-right: 0px; margin-left: 0px;">
					<div class="col-sm-2 col-md-2 col-lg-1 text-center font-weight-bold" style="">
		                <label class="rcl-std">Revision#</label>
		            </div>
		            <div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold">
		                <label class="rcl-std">Revision date</label>
		            </div>
		            <div class="col-sm-4 col-md-2 col-lg-2 text-center font-weight-bold">
		                <label class="rcl-std" >User</label>
		            </div>
	            </div>
			</rcl:area>

			<div id="t1-history" class="tblArea container-fluid border border-secondary rounded shadow-lg pb-0 ascanList2">
		        <div id="t1-row" class="tblRow row border" style="padding-top: 3px;">
			        <div class="col-sm-2 col-md-2 col-lg-1" style="text-align: center;">
		                <a id="t1-revisionNo" class="my-button" onclick="onClickHisRevision('#-RowId-#')">1</a>
		            </div>
					<rcl:text id="t1-revisionDate" name="revisionDate" classes="div(col-sm-4 col-md-2 col-lg-2 text-center label)" options="readonly" />
					<rcl:text id="t1-revisionUser" name="user" classes="div(col-sm-4 col-md-2 col-lg-2 text-left label)" options="readonly" />
				</div>
			</div>
			
		</div>
    </form>
    
    


	<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>


<script type="text/javascript">

var requestParam = { };
var readonlyFlag = true;

$(document).ready(function() {
	setHeaderTitle("Schedule Maintenance");
	setHeaderOptions(true,"back");
	
	rptTableInit("t1-dataPrediction");
	rptTableInit("t1-history");
	
	
	var voyageId = sessionStorage.getItem("voyageId");
	//console.log("voyageId :: "+ voyageId);
	//console.log("userData :: "+JSON.stringify(userData));
	
	requestParam.voyageId = voyageId;
	requestParam.nextPrev = "CURR";
	
	fetchData();
	
	//Prepare link download report
	var encodedString = getAuthorization();
	
	var voyageId = sessionStorage.getItem("voyageId");
	var rptEndpoint =  serviceContextPath + "/data-prediction/rpt/" + voyageId;
	rptEndpoint += "?token="+encodedString;

	$("#d0-printRptLoc").attr("href", rptEndpoint + "&type=location");
	$("#d0-printRptHq").attr("href", rptEndpoint + "&type=hq"); 
	
	
	
	
	$("#btn-next").on("click", function(){
		console.log("Next!!");
		requestParam.voyageId = voyageId;
		requestParam.nextPrev = "NEXT";
		
		fetchData();
	})
	
	
	
})

function addRowOverall(sumData){
  var element = document.getElementById("overall");
  if( element ){
	  element.remove();
  }
  
  var sumDataStr = '';
  if(isNaN(sumData)){
	 sumDataStr = '';
  }else{
	 sumDataStr = sumData.toFixed(2) + ' %';
  }
  
  const div = document.createElement('div');
  div.className = 'row overall';
  div.id = "overall";
  
  var strHtml = '<label class="col-sm-12 col-md-8 col-lg-7 text-right rcl-standard-font">Overall Prediction Accuracy</label>';
  strHtml += '<label class="col-sm-2 col-md-2 col-lg-2 text-right rcl-standard-font">' + sumDataStr + '</label>';
  
  div.innerHTML = strHtml;
  
  document.getElementById('t1-dataPrediction').appendChild(div);
}

/** -------------------------------------------------------------------
 * Load Data 
 */

function fetchData(){
	crpGetResultAjax("WS_CRP_GET_CURRENT_PORT", requestParam).done(handleCurrentPortResponse);

	getDataPrediction(requestParam);
	getHistoryList(requestParam);
	
}

function handleCurrentPortResponse(response){
	console.log("Callback!!!.");
	var paramOutData = response.P_O_DATA;
	
	var row = paramOutData[0];
	var portInfo = {
		//"seqno" : row.PK_VOYAGE_ID,
		"port" : row.PORT,
		"terminal" : row.TERMINAL,
		"service" : row.SERVICE,
		"vessel" : row.VESSEL,
		"voyage" : row.VOYAGE,
		"eta" : ( row.ETA_DATE !== null ? formatDateWithSepartor(row.ETA_DATE.toString(),'-') + " " + formatTime(row.ETA_TIME.toString()) : ''),
		"etd" : ( row.ETD_DATE !== null ? formatDateWithSepartor(row.ETD_DATE.toString(),'-') + " " + formatTime(row.ETD_TIME.toString()) : ''),
		"servicePic" : row.SERVICE_PIC_NAME
  	};
	
	$('#f1-port').val(portInfo.port);
	$('#f1-terminal').val(portInfo.terminal);
	$('#f1-service').val(portInfo.service);
	$('#f1-vessel').val(portInfo.vessel);
	$('#f1-voyage').val(portInfo.voyage);
	$('#f1-eta').val(portInfo.eta);
	$('#f1-etd').val(portInfo.etd);
	$('#f1-servicePic').val(portInfo.servicePic);
	

	$('#txtSpecialInstruction').val(row.SPECIAL_INSTRUCTION);
	$('#txtDelayReason').val(row.DELAY_REASON_DESC);
	addRowOverall(row.OVERALL_RATING);
	
	//Set Button
	readonlyFlag = row.FLAG_READ_ONLY;
	if( row.FLAG_READ_ONLY === "Y" ){
		$('#d0-save').addClass( "disabled" );
		$('#d0-save').prop('disabled', true);
		
		$('#d0-reset').addClass( "disabled" );
		$('#d0-reset').prop('disabled', true);
		
		$('#d0-send').addClass( "disabled" );
		$('#d0-send').prop('disabled', true);
	}
	
	//Set Current
	$("#btn-current")[0].innerHTML = row.PORT;
	
	//Set Prev Next button
	if( row.FLAG_PREV_PORT === "N" ){
		$('#btn-prev').addClass("disabled");
	}
	
	if( row.FLAG_NEXT_PORT === "N" ){
		$('#btn-next').addClass("disabled");
	}
	
	//Set 
	$('#t0-dataPredictionHeader')[0].innerHTML = "Data Prediction revision#" + row.REVISION_NO;
	
	//Set Status badge 
	$('#voyageStatus').html(row.VOYAGE_STATUS_DESC);
	
}

function getDataPrediction(requestParam){
	crpGetResultAjax("WS_CRP_GET_DATA_PREDICTION", requestParam)
	.done(function(response) {
		var paramOutData = response.P_O_DATA;
		
		var resultData = [];
		for (var i = 0; i < paramOutData.length; i++) {
			var row = paramOutData[i];
			var ratingStr = '';
			
			if(isNaN(row.RATING)){
				ratingStr = '';
			}else{
				ratingStr = row.RATING.toFixed(2) + ' %';
			}
			
			var rowData = {
				"seqno" : i + 1,
				"indId" : row.PK_IND_ID,
				"subject" : row.IND_NAME,
				"generatedData" : row.PREDICTION_DATA,
				"orig-generatedData" : row.PREDICTION_DATA,
				"actualData" : row.ACTUAL_DATA,
				"performance" : ratingStr,
				"dataType" : row.IND_DATA_TYPE
			}
			resultData.push(rowData);
			
		}
		
		
		rptClearDisplay("t1-dataPrediction");
		rptAddData("t1-dataPrediction", resultData);
		rptDisplayTable("t1-dataPrediction");
		
		if(readonlyFlag == 'Y') {
			var eleGenerateDataList = document.getElementsByName("generatedData");
			for(var x=0; x < eleGenerateDataList.length; x++)   // comparison should be "<" not "<="
			{
				//eleGenerateDataList[x].value = "1234";
				eleGenerateDataList[x].readOnly = true;
			}	
		}
		
		
		
		//Set Css default
		$("#t1-generatedData-1").css("background-color", "rgb(255,255, 0, 0.3)");
		$("#t1-generatedData-4").css("background-color", "rgb(255,255, 0, 0.3)");
		
		//Set css for ETA has change display red color.
		$("#t1-generatedData-1").on("change paste keyup", function() {
			 //  alert($(this).val()); 
	   		var rowData = rptGetDataFromDisplay("t1-dataPrediction", $(this)[0].id);
	   		rowData.rowId = $(this)[0].id;
			console.log("RowID " + rowData.rowId);
			
			var currentValue = $(this).val();
			if(rowData["orig-generatedData"] !== currentValue){
				//alert("The text has been changed.");
				$("#" + rowData.rowId).css("background-color", "rgb(255,0, 0, 0.15)");
			}else{
				$("#" + rowData.rowId).css("background-color", "rgb(255,255, 0, 0.3)");
			}
		});
		
		//Set css for Move/Hour/Crane has change display red color.
		$("#t1-generatedData-4").on("change paste keyup", function() {
			 //  alert($(this).val()); 
	   		var rowData = rptGetDataFromDisplay("t1-dataPrediction", $(this)[0].id);
	   		rowData.rowId = $(this)[0].id;
			console.log("RowID " + rowData.rowId);
			
			var currentValue = $(this).val();
			if(rowData["orig-generatedData"] !== currentValue){
				//alert("The text has been changed.");
				$("#" + rowData.rowId).css("background-color", "rgb(255,0, 0, 0.15)");
			}else{
				$("#" + rowData.rowId).css("background-color", "rgb(255,255, 0, 0.3)");
			}
		});
	});
	
	
	
 
}

function getHistoryList(requestParam) {
	crpGetResultAjax("WS_CRP_GET_CALL_REPORT_HIS", requestParam)
	.done(function(response) {
		var paramOutData = response.P_O_DATA;

		var resultData = [];
		for (var i = 0; i < paramOutData.length; i++) {
			var row = paramOutData[i];
			
			var d = new Date(row.REVISION_DATE); // The 0 there is the key, which sets the date to the epoch
			
			var dformat = [ d.getDate().padLeft(), (d.getMonth()+1).padLeft(), d.getFullYear() ].join('/') + ' ' +
		                  [ d.getHours().padLeft(), d.getMinutes().padLeft(), d.getSeconds().padLeft() ].join(':');
			
			var rowData = {
				"voyageId" : row.PK_VOYAGE_ID,
				"revisionNo" : row.REVISION_NO,
				"revisionDate" : dformat,
				"revisionUser" : row.REVISION_USER
			}
			resultData.push(rowData);
			
		}
		
		rptClearDisplay("t1-history");
		rptAddData("t1-history", resultData);
		rptDisplayTable("t1-history");
		
		for (var i = 0; i < resultData.length; i++) {
			var rowItem = resultData[i];
			
			//t1-row-0 = t1-row-i
			var divRowId = "t1-row-" + i;
			document.getElementById(divRowId).getElementsByTagName('a')[0].innerHTML= rowItem.revisionNo;
		}
		
	});

}


function doSave(){
	console.log("doSave");
	
	var functionName = "WS_CRP_SAVE_CALL_REPORT";
	var gridDataPrediction = rptGetDataFromDisplayAll("t1-dataPrediction");
	var siText = $('#txtSpecialInstruction').val();
	
	//console.log(">>>>" + JSON.stringify(obj) );
	var hasError = false;
	var arrPredictionList = [];
	for (i = 0; i < gridDataPrediction.length; i++) {
	    var row = gridDataPrediction[i];
	    console.log("###" + row.subject );
	    var tempValue = row.generatedData;

	    var preMsg =  "The value must be ";
	    
	    if( row.dataType == 'D') {
	    	preMsg += " date format.<br/>";
	    	
	    	//Check Datetime format
    		//var pattern = new RegExp("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4} (2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$");
    		var pattern = new RegExp("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4} (2[0-3]|[01]?[0-9]):([0-5]?[0-9])$");
    		if (  pattern.test(tempValue) ){
    		    // date is exact format and valid
    		}else{
    			var errorMsg = preMsg  + "Input " + tempValue + " is not a datetime (DD/MM/YYYY HH:MM)<br/>";
				dialogGeneric("Warning", errorMsg, "Ok");
	    		hasError = true;
	    		
	    		return ;
    		}
    		
    		
	    } else if ( row.dataType == 'N') {
	    	preMsg += " numeric.<br/>";
	    	
	    	//Check number format
	    	
	    	if(isNaN(tempValue)){
	    		var errorMsg =  preMsg  + "Input " + tempValue + " is not a number <br/>";
	    		dialogGeneric("Error", errorMsg, "Ok");
	    		hasError = true;
	    		
	    		return ;
	    	}
	    }
	    
	    var item = {
	    	"indId" : row.indId,
	    	"predictionData" : row.generatedData
	    }
	    
	    arrPredictionList.push( item );	    
	}
	
	var voyageId = sessionStorage.getItem("voyageId");
	var requestParam = {
		"voyageId" : voyageId,
		"dataPrediction" : arrPredictionList,
		"specialInstruction" : siText
	};
	
	
	
	console.log("> Save Call Report Request = " + JSON.stringify(requestParam) );
	crpGetResultAjax(functionName, requestParam)
	.done(function(response) {
		var resultCode = response.resultCode;
		console.log("resultCode = " + resultCode);
		console.log("resultMsg = " + response.resultMsg);
		
		
		if(resultCode == "000") {
			dialogGenericv2("Info", response.resultMsg, "Ok"); 
			
			fetchData();
		} else {
			dialogGenericv2("Error", response.resultMsg, "Ok");			
		}
		
	});
	
}

function doSend(){
	console.log("doSend");
	
	var functionName = "WS_CRP_SEND_CALL_REPORT";
	
	var gridDataPrediction = rptGetDataFromDisplayAll("t1-dataPrediction");
	var siText = $('#txtSpecialInstruction').val();
	
	//console.log(">>>>" + JSON.stringify(obj) );
	var arrPredictionList = [];
	for (i = 0; i < gridDataPrediction.length; i++) {
	    var row = gridDataPrediction[i];
	    var item = {
	    	"indId" : row.indId,
	    	"predictionData" : row.generatedData
	    }
	    
	    arrPredictionList.push( item );	    
	}
	
	var voyageId = sessionStorage.getItem("voyageId");
	var requestParam = {
		"voyageId" : voyageId,
		"dataPrediction" : arrPredictionList,
		"specialInstruction" : siText
	};
	

	console.log("> Send Call Report Request = " + JSON.stringify(requestParam) );
	crpGetResultAjax(functionName, requestParam)
	.done(function(response) {
		var resultCode = response.resultCode;
		console.log("resultCode = " + resultCode);
		console.log("resultMsg = " + response.resultMsg);
		
		if(resultCode == "000") {
			dialogGenericv2("Info", response.resultMsg, "Ok"); 
		} else {
			dialogGenericv2("Error", response.resultMsg, "Ok");			
		}
		
	});
}


function doReset(){
	console.log("doReset");
	
	getDataPrediction(requestParam);
	
}

function onClickBack(){
	window.history.back();
}

function onClickHisRevision(rowId){
	//console.log("onClickHisRevision : " + rowId);
	var revisionNo = rptGetDataFromDisplay("t1-history", rowId).revisionNo;
	var hisVoyageId = rptGetDataFromDisplay("t1-history", rowId).voyageId;
	
	
	//console.log("onClickHisRevision revisionNo : " + revisionNo);
	sessionStorage.setItem("revisionNo", revisionNo);
	sessionStorage.setItem("hisVoyageId", hisVoyageId);
	
	window.open('<%=servURL%>/RrcGenericSrv?service=ui.crp.CrpScheduleMaintenanceSvc&pageAction=historyRevisionPopup',
			'_blank', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,width=850,height=450,scrollbars=1');
}
	
</script>

</body>
</html>
