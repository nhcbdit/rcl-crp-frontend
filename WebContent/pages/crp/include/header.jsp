<%--
---------------------------------------------------------------------------------------------------------
header.jsp
---------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
---------------------------------------------------------------------------------------------------------
Author Thanapong Tienniem 08/10/2018
- Change Log---------------------------------------------------------------------------------------------
## 		DD/MM/YY 		-User- 		-TaskRef-       -ShortDescription
01		04/12/2018		Pichit						Restructure header file
02		11/09/2019		nawrat1						Remove usage of userData.country
---------------------------------------------------------------------------------------------------------
--%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="userData" value="${sessionScope.userData}" />
<c:set var="authData" value="${sessionScope.user.getAuthority(sessionScope.module)}" />
<%@include file="/pages/misc/RcmInclude.jsp"%>
<%--
<c:set var="icons" value="${fn:length(param.icons) > 0 ? fn:split(param.icons, ',') : []}" />
<c:set var="labels" value="${fn:length(param.labels) > 0 ? fn:split(param.labels, ',') : []}" />
 --%>

<div id="h0-pageheader" class="rcl-standard-page-header container-fluid">
	<div class="row">
		<div class="col-sm-5">
			<label id="h0-title"><c:out value="${param.pageHeader}" /></label>
<%-- 			<label id="h0-title"></label> --%>
		</div>
		<div id="h0-userData" class="col-sm-7" style="font-size:10px;text-align:right;">
 			<c:out value="${userDesc} (${userData.userId}) - ${userData.fscCode} - ${userData.line}/${userData.trade}/${userData.agent}" />
		</div>
	</div>
</div>

<div id="h1-pageheader" class="rcl-standard-page-header-2">
	<%--
	<c:if test="${fn:length(icons) > 0}">
		<c:forEach begin="0" end="${fn:length(icons)-1}" var="i">
			<c:set var="id" value="${fn:toLowerCase(labels[i])}" />
			<button id="h1-${id}" type="button" class="rcl-standard-button-header" onclick="${id}()">
				<i class="${icons[i]}"></i> <c:out value="${labels[i]}" />
			</button>
		</c:forEach>
	</c:if>
	 --%>

	<button id="h1-back" type="button" class="rcl-standard-button-header" onclick="onClickBack();" style="display:none;">
		<i class="fas fa-arrow-left"></i> Back
	</button>


	<button id="h1-final" type="button" class="rcl-standard-button-header" onclick="btnfinal()" style="display:none;">
		<i class="fas fa-check-circle" style="color: green"></i> Final
	</button>
	<%-- button id="h1-confirm" type="button" class="rcl-standard-button-header" onclick="confirm()" style="display:none;">
		<i class="fas fa-check"></i> Confirm
	</button--%>
	<button id="h1-save" type="button" class="rcl-standard-button-header" onclick="save()" style="display:none;">
		<i class="far fa-save"></i> Save
	</button>

	<button id="h1-Finalize" type="button" class="rcl-standard-button-header" onclick="rutOpenDialog('b8-finalarea')" style="display:none;">
		<i class="fa fa-check-square" style="color: green"></i> Finalize
	</button>
	<button id="h1-Decline" type="button" class="rcl-standard-button-header" onclick="fnDecline();" style="display:none;">
		<i class="fa fa-user-times" style="color: red"></i> Decline
	</button>

	<button id="h1-hold" type="button" class="rcl-standard-button-header" onclick="save('H')" style="display:none;">
		<i class="fas fa-hand-point-up"></i> Hold
	</button>
	<button id="h1-unhold" type="button" class="rcl-standard-button-header" onclick="save('O')" style="display:none;">
		<i class="far fa-hand-point-up"></i> UnHold
	</button>
	
	<button id="h1-cancel" type="button" class="rcl-standard-button-header" onclick="btnCancel()" style="display:none;">
		<i class="far fa-calendar-times" style="color: red"></i> Cancel
	</button>
	<button id="h1-close" type="button" class="rcl-standard-button-header" onclick="btnClosed()" style="display:none;">
		<i class="far fa-times-circle" style="color: red"></i> Closed
	</button>
	<button id="h1-help" type="button" class="rcl-standard-button-header"  onclick="help()">
		<i class="far fa-question-circle"></i> Help
	</button>
	<button id="h1-tool" type="button" class="rcl-standard-button-header"  onclick="rutToolMenue()" >
		<i class="fas fa-toolbox"></i> Tool
	</button>
	<button id="h1-closepage" type="button" class="rcl-standard-button-header" onclick="onClickClose()">
		<i class="fa fa-times" style="color: red"></i>
	</button>
</div>

<div class="rcl-standard-version-header">
	<label id="h2-versionheader" style="margin-bottom: 0px;">V 1.14.3</label>
</div>

<div style="display:none;">
	<rcl:area id="h3-hidden">
		<input type="text" id="h3-userToken" class="dtlField" value="${userData.userToken}" />
		<input type="text" id="h3-userId" class="dtlField" value="${userData.userId}" />
		<input type="text" id="h3-line" class="dtlField" value="${userData.line}" />
		<input type="text" id="h3-trade" class="dtlField" value="${userData.trade}" />
		<input type="text" id="h3-agent" class="dtlField" value="${userData.agent}" />
		<input type="text" id="h3-fscCode" class="dtlField" value="${userData.fscCode}" />
		<input type="text" id="h3-country" class="dtlField" value="${userData.country}" /> 
	</rcl:area>

	<input type="text" id="publicToken" value="${publicToken}" />

</div>

<div style="display:none;">
	<rcl:area id="h4-hidden">
		<input type="text" id="h4-menuId" class="dtlField" value="${authData.getMenuId()}" />
		<input type="text" id="h4-canCreate" class="dtlField" value="${authData.canCreate() == true ? 'Y' : 'N'}" />
		<input type="text" id="h4-canRead" class="dtlField" value="${authData.canRead() == true ? 'Y' : 'N'}" />
		<input type="text" id="h4-canUpdate" class="dtlField" value="${authData.canUpdate() == true ? 'Y' : 'N'}" />
		<input type="text" id="h4-canDelete" class="dtlField" value="${authData.canDelete() == true ? 'Y' : 'N'}" />
		<input type="text" id="h4-canView" class="dtlField" value="${authData.canView() == true ? 'Y' : 'N'}" />
	</rcl:area>
</div>




<script type="text/javascript">
var mapSize = 6;
var buttonNameIndexMap = {
		back: 0,
		confirm: 1,
		save: 2,
		help: 3,
		tool: 4,
		close: 5
};

function onClickBack(){

	window.location = '<%=servURL%>/RrcStandardSrv?service=ui.qtn.QtnSearchSvc';
}

function getUserDataFromHeader(){
	return rptGetDataFromSingleArea("h3-hidden");
}

function getUserDataFromHeaderv2(){
	return rptGetDataFromSingleArea("h99hidden");
}

function setHeaderTitle(label){
	$("#h0-title").html(label);
}

function setHeaderOptions(visible, options){
	var listButtonShow = [];
	$("#h1-pageheader button").each(function(index) {
		listButtonShow.push(this.style.display);
	});
	for(var i=1; i<arguments.length; i++){
		listButtonShow[buttonNameIndexMap[arguments[i]]] = visible ? "" : "none";
	}
	$("#h1-pageheader button").each(function(index) {
		this.style.display = listButtonShow[index];
	});
}

function onClickClose(){
	if(!rut.urlParms || !rut.urlParms.rclp){
		 close();
	}else{
		rutBack();
	}
}

function btnCancel(){
	dialogGeneric("Warning", "Quotation that is cancelled cannot be changed. Do you want to proceed?" , "Ok", "No").then(
			function(overwrite){
				if(overwrite){
					save('L');
				}else{
					return false;
				}
			});
}


function fnDecline(){
	var ArrayChargesDetail = rptGetDataFromDisplayAll("t52ChargesDetailsArea");

		var x = 0;
		for(var i = 0;i < ArrayChargesDetail.length; i++){
			if(ArrayChargesDetail.action != "d"){
				x++;
			}
		}

		rutOpenDialog('b7-confirmDeclinearea');

		if(x > 0){
			$('#d93confirmDeclinelbl').text("This operation will remove all the charges that you'e entered and send this quotation back to Agent. Are you sure, you want to proceed?");
		}else{
			$('#d93confirmDeclinelbl').text('This operation will send this quotation back to Agent. Are you sure, you want to proceed?');
		}
}


//getResultAjax("BookingService/WS_KEEP_ALIVE", { userData: getUserDataFromHeaderv2() }, false, false, true);
</script>