<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Nutsu 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../include/tab_js.jsp" %>
	<%@include file="../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

<%-- 	<script src="${context}/js/bkg/rutPowerTable.js"></script> --%>
	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/qtnPowerTable.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>
	
	<script src="${context}/js/crpCommon.js"></script>

	<style>
	/* Nutsu */
	.rcl-standard-font {
		font-size : 10px;
	}
	
    .label > .rcl-standard-form-control {
    	background-color: #f8f9fa;
    }
    
    .rptEvenRow > .label > .rcl-standard-form-control {
	    background-color: #f8f9fa;
	}
    
    
	/* Nutsu */
	.text-center > .rcl-standard-form-control {
		text-align: center;
	}
	
	.text-left > .rcl-standard-form-control {
		text-align: left;
	}
	
	.text-right > .rcl-standard-form-control {
		text-align: right;
	}


.nav-tabs {
	margin : 0 20px 0 20px;
	width: 100%
}

.nav-item {
	width: 20%
}

.nav-tabs .nav-link {
    width: 100%;
}


/* Override tab bg color */
#p0-id-18hours {
	color: #fff;
	background-color: #dc3545;
}

#p0-id-1924hours {
	color: #fff;
	background-color: #ff7007;
}

#p0-id-2536hours {
	color: #fff;
	background-color: #ffc107;
}

#p0-id-3748hours {
	color: #fff;
	background-color: #28a745;
}

#p0-id-Vesseldepartured{
	color: #fff;
	background-color: #6c757d;
}

/* Style the active class (and buttons on mouse-over) */
#p0-id-18hours,
#p0-id-1924hours,
#p0-id-3748hours,
#p0-id-2536hours,
#p0-id-Vesseldepartured
{
	opacity: 0.2;
}

/* Style the active class (and buttons on mouse-over) */
#p0-id-18hours[class*="active"], #p0-id-18hours:hover,
#p0-id-1924hours[class*="active"], #p0-id-1924hours:hover,
#p0-id-3748hours[class*="active"], #p0-id-3748hours:hover,
#p0-id-2536hours[class*="active"], #p0-id-2536hours:hover,
#p0-id-Vesseldepartured[class*="active"], #p0-id-Vesseldepartured:hover 
{
	opacity: 1;
}


.seqno {
	width: 50px !important;
}

.w-5 {
	width: 5%!important;
}

.w-10 {
	width: 10%!important;
}

/* element.style { */
/*     vertical-align: middle; */
/*     height: 20px; */
/*     display: table-cell; */
/* } */

.rcl-standard-font {
    font-size: 10px;
	font-weight: normal !important;
}

.rcl-standard-font-bold {
    font-weight: bold !important;
}

p {
   margin-top: auto;
   margin-bottom: auto;
}
	</style>

	<title>Call Report</title>


</head>
<body>

	<jsp:include page='include/header.jsp'>
	    <jsp:param name="pageHeader" value="Prediction Setup"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0">

		<rcl:area id="s0-header" title=" Search" areaMode="search" buttonList="Search Reset" onClickList="doSearch doReset">
		  <div class="row" style="margin-top: 20px;">
				<rcl:text id="s0-port" label="Port" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(5) upc" 
					 lookup="tbl(VRL_PORT) rco(CODE) rid(s0-port)" />
							
				<rcl:text id="s0-service" label="Service" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(5) upc" 
					lookup="tbl(VRL_SERVICE)
	  			  		  rco(CODE)
	  			  		  rid(s0-service)" />
				
				<rcl:text id="s0-vessel" label="Vessel" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(5) upc" 
					lookup="tbl(VRL_VESSEL) rco(CODE) rid(s0-vessel)"/>

				<rcl:text id="s0-voyage" label="Voyage" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(10) upc" 
					lookup="tbl(VRL_VOYAGE) rco(CODE) rid(s0-voyage)" />

		  </div>

		  <div class="row">
				<rcl:text id="s0-region" label="Region" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(3) upc"
					  	lookup="tbl(VRL_CRP_REGION) rco(CODE) rid(s0-region)" />	
				
				<rcl:text id="s0-country" label="Country Code" classes="div(col-sm-2 col-md-2 col-lg-2)" check="len(2) upc"
					  	lookup="tbl(VRL_COUNTRY) rco(CODE) rid(s0-country)"/>	
					

				<div class="col-sm-2 col-md-2 col-lg-2 pr-0 ">
					<label class="rcl-standard-font rcl-standard-font-bold" for="s0-status">Status</label>
					<select id="s0-status" class="rcl-standard-form-control rcl-standard-component tblField">
                 	</select>
               	</div>
                 			
				<rcl:text id="s0-servicePic" label="Service PIC" classes="div(col-sm-2 col-md-2 col-lg-2)" check="upc"/>	
		  </div>
	   </rcl:area>
    </form>

	<div class="tblHeader">
	    <%-- film adjust --%>
	    <rcl:area id="t0-area" title="Schedule List"
			classes="div(container-fluid pl-1 pr-1)" areaMode="table" >
					
			<rcl:tabGroup tabList="+18_hours 19-24_hours 25-36_hours 37-48_hours Vessel_departured" activeTab="+18_hours">
				<rcl:tabContentPane tabName="+18_hours">
					<jsp:include page='tab/monitorHourTab1.jsp'></jsp:include>
				</rcl:tabContentPane>
				<rcl:tabContentPane tabName="19-24_hours">
					<jsp:include page='tab/monitorHourTab2.jsp'></jsp:include>
				</rcl:tabContentPane>
				<rcl:tabContentPane tabName="25-36_hours">
					<jsp:include page='tab/monitorHourTab3.jsp'></jsp:include>
				</rcl:tabContentPane>
				<rcl:tabContentPane tabName="37-48_hours">
					<jsp:include page='tab/monitorHourTab4.jsp'></jsp:include>
				</rcl:tabContentPane>
				<rcl:tabContentPane tabName="Vessel_departured">
					<jsp:include page='tab/monitorVesselTab.jsp'></jsp:include>
				</rcl:tabContentPane>
			</rcl:tabGroup>
	
	
		</rcl:area>
		
	</div>
	
		
	<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>

<script type="text/javascript">

var userData;
var searchInfo = {
	"searchParam" : null,	
	"tabNumber" : 1,
	"tableDivId" : "t1-tab1-data",
	"paging" : null
}

//Fix IE
var userToken = '${userData.userToken}';

$(document).ready(function() {
	setHeaderTitle("Schedule Maintenance");
	setHeaderOptions(true);
	
	if( sessionStorage.getItem("userData") ){
		//console.log("exist userData = " + sessionStorage.getItem("userData"));
	}else{
		var userData = crpGetUserDataFromHeader();
		
		//Fix IE
		userData.userToken = userToken;
		
		// Encode the String
		var encodedString = btoa(JSON.stringify(userData));
		sessionStorage.setItem("userData", encodedString);
	}
	
	
	//Clear 
	sessionStorage.removeItem("voyageId");
	
	//Event click on tab.
	$('#t0-area li a').on('click',function(){
		var ee = $(this);
		var id = ee[0].id;
		console.log('this tab is already active ' + id);
		
		switch( id ){
		case "p0-id-18hours" : 
			searchInfo.tabNumber = 1;
			searchInfo.tableDivId = "t1-tab1-data";
			break;
		case "p0-id-1924hours" : 
			searchInfo.tabNumber = 2;
			searchInfo.tableDivId = "t2-tab2-data";
			break;
		case "p0-id-2536hours" : 
			searchInfo.tabNumber = 3;
			searchInfo.tableDivId = "t3-tab3-data";
			break;
		case "p0-id-3748hours" : 
			searchInfo.tabNumber = 4;
			searchInfo.tableDivId = "t4-tab4-data";
			break;
		case "p0-id-Vesseldepartured" : 
			searchInfo.tabNumber = 5;
			searchInfo.tableDivId = "t5-tab5-data";
			break;
		}
		
		
		//Hide Schedule-Field
		if(id === "p0-id-Vesseldepartured" ){
			$('#s0-servicePic').hide();
			$('label[for="s0-servicePic"]').hide();
		}else {
			$('#s0-servicePic').show();
			$('label[for="s0-servicePic"]').show();
		}
		
		
		//if( !searchInfo.hasOwnProperty('loadResult') ){
			searchInfo.searchParam.pageNo = 1;
			doSearch();
		//}
	});
	

	doReset();
	initilDropdown( initialSearchParam );
	
})
	
function settingTable(isvalue){
	var settings = getTableSettings(isvalue);
	settings.paging.pageSize = 15;
	settings.paging.activePage = 0;
	
	return settings;
}
	
/*
 * Common function initial div table.
 */
function initial(divId){
	rptTableInit(divId);
	settingTable(divId);
	
}
	
function initilDropdown(callbackFn){
	console.log("initilDropdown ...");
	

	crpGetResultAjax("WS_CRP_STATUS", null)
	.done(function(response) {
		var paramOutData = response.P_O_DATA;
		
		var resultData = [];
		for (var i = 0; i < paramOutData.length; i++) {
			var row = paramOutData[i];
			
			var rowData = {
				"code" : row.STATUS_CODE,
				"desc" : row.DESCRIPTION
			};
			
			resultData.push(rowData);
		}
		
		//Set Dropdown
		var drowdownStatus = $('#s0-status');
		drowdownStatus.html("");
		
		$.each(resultData, function(index, item) {
			drowdownStatus.append(
		        $('<option></option>').val(item.code).html(item.desc)
		    );
		});
		
		drowdownStatus.val("");
		 
		
		callbackFn();
	});
	
}	


function initialSearchParam(){

	if( searchInfo.searchParam == null ){
		//Initial first
		var dataPost = {
			"pageRows" : 10,
		    "pageNo" : 1,
		    "sort" : "",
		    "ascDesc" : "ASC",
		    "duration" : 18,
		    "port" : "",
		    "service" : "",
		    "vessel" : "",
		    "voyage" : "",
		    "region" : "",
		    "country" : "",
		    "status" : "",
		    "servicePic" : ""
		};
	
		
		searchInfo.searchParam = dataPost;		
	}
	
	
	//doSearch();
	//Force event click on tab menu No. 1
	var defaultTab = 'p0-id-18hours';
	//var defaultTab = "p0-id-Vesseldepartured";
	document.getElementById(defaultTab).click();
}

function doReset(){
	$('#s0-port').val("");
	$('#s0-service').val("");
	$('#s0-vessel').val("");
	$('#s0-voyage').val("");
	
	$('#s0-region').val("");
	$('#s0-country').val("");
	$('#s0-status').val("G");
	$('#s0-servicePic').val("");
	
	$("#p0-id-18hours").html("+6 hours");
	$("#p0-id-1924hours").html("6-24 hours");
	$("#p0-id-2536hours").html("25-36 hours");
	$("#p0-id-3748hours").html("37-48 hours");



	var emptyData = [];
	var tableDivId = searchInfo.tableDivId;
	rptClearDisplay(tableDivId);
	rptAddData(tableDivId, emptyData);
	rptDisplayTable(tableDivId);
	
	searchInfo.paging = {
		  "startOffset":0,
		  "previousOffset":-10,
		  "pageNo":0,
		  "pageSize":10,
		  "totalPage":0,
		  "lastOffset":0,
		  "totalRecord":0,
		  "nextOffset":0
	   };
	
	crpResetPagingValues(tableDivId, searchInfo.paging);
}

function doSearch(){
	var duration = null;
	if( searchInfo.tabNumber == 1 ) {
		duration = 18;
	}else if( searchInfo.tabNumber == 2 ) {
		duration = 24;
	}else if( searchInfo.tabNumber == 3) {
		duration = 36;
	}else if( searchInfo.tabNumber == 4) {
		duration = 48;
	}else if( searchInfo.tabNumber == 5) {
		duration = 0;
	}
	
	
	//Get parameter 
	var postData = crp_GetDataAreaToObject("s0-header");
	
	var temp = searchInfo.searchParam;
	searchInfo.searchParam = postData;
	searchInfo.searchParam.pageRows = temp.pageRows;
	searchInfo.searchParam.pageNo = temp.pageNo;
	searchInfo.searchParam.sort = temp.sort;
	searchInfo.searchParam.ascDesc = temp.ascDesc;

	searchInfo.searchParam.duration = duration;
	
	fetchData();
}

function gotoPage(containerId, pageNo){
	console.log("GotoPage > containerId:" + containerId + ", pageNo:"+ pageNo);
	
	var pageNumber = null;
	if( pageNo === 'first') {
		pageNumber = 1;
	}else if( pageNo === 'last') {
		pageNumber = searchInfo.paging.totalPage;
	}else if( pageNo === 'previous') {
		if( searchInfo.paging.pageNo == 1 ){
			return;
		}else{
			pageNumber = searchInfo.paging.pageNo - 1 ;
		}
		
	}else if( pageNo === 'next') {
		if( searchInfo.paging.pageNo == searchInfo.paging.totalPage ){
			return;
		}else{
			pageNumber = searchInfo.paging.pageNo + 1 ;
		}

	} else if( pageNo == 'goto') {
		var settings=rptGetTableSettings(containerId);
	    var prefix=settings.idPrefix;
	   
	    var gotoInput = $("#"+prefix+"pgGotoField").val();
	    pageNumber = parseInt(gotoInput)
	    
	    //Check input must be range 1 - totalPage
	    if( pageNumber < 1 || pageNumber > searchInfo.paging.totalPage ) {
	    	console.error("Invalid input page number. " + pageNumber)
	    	return;
	    }
	    
	}else {
		pageNumber = pageNo;
	}
	
	console.log("Search Param PageNo = " + pageNumber);
	searchInfo.searchParam.pageNo = pageNumber;
	
	fetchData();
}

// function nextPage(){
// 	var pagingInfo = searchInfo.paging;
// 	pagingInfo.
// }

// function previousPage(){
	
// }

function fetchData(){
	var searchParam = searchInfo.searchParam;
	
	searchParam.userData = userData;
	
	crpGetResultAjax("WS_CRP_SEARCH", searchParam).done(handleResponse);
}



function handleResponse(response){
// 	if(response.resultCode == "200"){
// 		var jsonOption = response.resultContent;
// 	}
	var tableDivId = searchInfo.tableDivId;

	var paramOutData = response.P_O_DATA;
	var paramOutRowTotal = response.P_O_ROW_TOTAL;
	var paramOutPaging = response.pagingInfo;
	
	searchInfo.paging = paramOutPaging;
	searchInfo.loadResult = paramOutData;
	
	var resultData = [];
	if(  paramOutData != null && paramOutData.length > 0){
		for (var i = 0; i < paramOutData.length; i++) {
			var row = paramOutData[i];
		  	//console.log("resultData : " + row.PK_VOYAGE_ID);
		  	
		  	
		  	var rowData = {
				"voyageId" : row.PK_VOYAGE_ID,
				"seqno" : paramOutPaging.startOffset + 1 + i,
				"portCall" : row.PORT + '/' + row.TERMINAL,
				"service" : row.SERVICE,
				"vessel" : row.VESSEL,
				"voyage" : row.VOYAGE,
				"etaDate" : ( row.ETA_DATE !== null ? formatDate(row.ETA_DATE.toString()) : ''),
				"etaTime" : ( row.ETA_TIME !== null ? formatTime(row.ETA_TIME.toString()) : ''),
				"previousPortCall" : (row.PREV_PORT !== null ) ? row.PREV_PORT + '/' + row.PREV_TERMINAL : "",
				"version" : row.REVISION_NO,
				"status" : row.VOYAGE_STATUS_DESC,
				"editFlag" : (row.FLAG_ACTUAL === 'Y') ? true : false,
				"submitDate" : ( row.SUBMIT_DATE !== null ? formatDate(row.SUBMIT_DATE.toString()) : '')
		  	};
		  	
		  	
		  	
		  	resultData.push(rowData);
		} 
		
	}
	
	
	var wordingTab =  " (" + paramOutRowTotal + " reports)";
	if( searchInfo.tabNumber == 1 ) {
		$("#p0-id-18hours").html("+6 hours" + wordingTab);
	}else if( searchInfo.tabNumber == 2 ) {
		$("#p0-id-1924hours").html("6-24 hours" + wordingTab);
	}else if( searchInfo.tabNumber == 3) {
		$("#p0-id-2536hours").html("25-36 hours" + wordingTab);
	}else if( searchInfo.tabNumber == 4) {
		$("#p0-id-3748hours").html("37-48 hours" + wordingTab);
	}
	

	rptClearDisplay(tableDivId);
	rptAddData(tableDivId, resultData);
	rptDisplayTable(tableDivId);

	
	if( searchInfo.tabNumber == 5) {
		//Set Actual color
		var powerTableId = tableDivId;
		$("#" + powerTableId).find(".tblRow").not(':last').each(function(){

	   		var rowData = rptGetDataFromDisplay(powerTableId, $(this)[0].id);
	   		rowData.rowId = $(this)[0].id;
			console.log("RowID " + rowData.rowId);
			
			//Set t5-editFlag icon
			var getrow = rowData.rowId.split("-");
			var prefix = getrow[0];
			var rowNum = getrow[2];
			var setToInputId = "@-editFlag-#";
			setToInputId = setToInputId.replace(/@/g, prefix);
			setToInputId = setToInputId.replace(/#/g, rowNum);
				
			if(rowData["editFlag"] == true){
				var x = document.getElementById(setToInputId);
				x.classList.remove("dot-grey");
				x.classList.add("dot-green");
			}else{
				var x = document.getElementById(setToInputId);
				x.classList.remove("dot-green");
				x.classList.add("dot-grey");
			}
		});
		
	}
	
	
	
	crpResetPagingValues(tableDivId, searchInfo.paging);
}


function gotoMaintenance(rowId){
	console.log("gotoMaintenance : " + rowId + " div:" + searchInfo.tableDivId);
	var voyageId = rptGetDataFromDisplay(searchInfo.tableDivId, rowId).voyageId;
	//flagAppove = $('#s0-formLoc').val();	
	sessionStorage.setItem("voyageId", voyageId);
	window.location = '<%=servURL%>/RrcGenericSrv?service=ui.crp.CrpScheduleMaintenanceSvc';
	
	//Create form
	var form = document.createElement("form");
    var element1 = document.createElement("input");

    form.method = "POST";
    form.action = '<%=servURL%>/RrcGenericSrv?service=ui.crp.CrpScheduleMaintenanceSvc';   

    element1.value = voyageId;
    element1.name = "voyageId";
    form.appendChild(element1);  

    document.body.appendChild(form);

    form.submit();
}
	
</script>


</body>
</html>