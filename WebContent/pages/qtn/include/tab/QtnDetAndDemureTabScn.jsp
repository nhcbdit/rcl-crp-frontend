<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>
<style>
.displayDND{
display : none;
}
</style>
<rcl:area id="d60QuotationHeader" classes="div(container-fluid)" title="Quotation Header" collapsible='Y'>
	<div class="row">
		<rcl:text id="d60quotationNo" classes="div(col-md-2) ctr(dtlField)"
			label="Quotation#" options="disabled" check="upc" />
		<rcl:text id="d60ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" options="disabled" check="upc" />
		<%--  rcl:select id="d60status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/--%>
		<rcl:select id="d60status" label="Status"
					classes="div(col-md-2) ctr(dtlField)"
					options="data-rdo"
					optionsList="Open Need_Approval Quote_Again Close Final Hold Cancel"
					valueList="O N Q S C H L" />
		<rcl:text id="d60slm" classes="div(col-md-2) ctr(dtlField)"
			label="Slm#" options="disabled" check="upc" />
		<rcl:text id="d60qtdBy" classes="div(col-md-2) ctr(dtlField)"
			label="QtdBy" options="disabled" check="upc" />
		<rcl:text id="d60pos" classes="div(col-md-2) ctr(dtlField)"
			label="POS" options="disabled" check="upc" />
	</div>
</rcl:area>

<rcl:area id="t61RoutingHeader" classes="div(container-fluid)" title="Routing List" collapsible='Y' areaMode="table">
<div id="routhingScroll">
	<div id="t61row" class="row tblRow t61row">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t61row');" class="row">
				<div class="col-md-12 container-fluid">
					<div class="row">
						<rcl:text id="t61porHaulLoc" classes="div(col-md-2) ctr(dtlField)"	label="POR Haul. Loc." options="disabled" check="upc" />
						<rcl:text id="t61por" classes="div(col-md-1) ctr(dtlField)" label="POR" options="disabled"  check="upc" />
						<rcl:text id="t61pol" classes="div(col-md-1) ctr(dtlField)" options="disabled" label="POL"  />
						<rcl:text id="t61pot1" classes="div(col-md-1) ctr(dtlField)" label="POT1" options="disabled"  check="upc" />
						<rcl:text id="t61pot2" classes="div(col-md-1) ctr(dtlField)" label="POT2" options="disabled" check="upc" />
						<rcl:text id="t61pot3" classes="div(col-md-1) ctr(dtlField)" label="POT3" options="disabled" check="upc" />
						<rcl:text id="t61pod" classes="div(col-md-1) ctr(dtlField)" label="POD" options="disabled" />
						<rcl:text id="t61del" classes="div(col-md-1) ctr(dtlField)" label="DEL" options="disabled" check="upc" />
						<rcl:text id="t61delHaulLoc" classes="div(col-md-2) ctr(dtlField)" label="DEL Haul. Loc." options="disabled" check="upc" />
						<rcl:select id="t61socCoc"
										label="SOC/COC"
										classes="div(col-md-1) ctr(dtlField)"
										selectTable="SocCoc"
										options="data-dis"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
		<div id="t61subjectGrcLable" class="col-md-4" >
				<label>Detention/Demurrage Rates in Quotation Print (Y/N)</label>
				<input type="checkbox" id="printDetDemFlag" class="dtlField" />
		</div>
		<div class="col-md-2">
			<div id="t61GTALable">
					<label>Global Tender Account</label>
					<input type="checkbox" id="gtaFlag" name="GTA"  class="dtlField" onchange="TrigGTA()"/>
			</div>
		</div>

		<div class="col-md-2">
			<div id="t61subjectGrcLable">
					<label>Subject to Change - DND</label>
					<input type="checkbox" id="subjDNDFlag" class="dtlField" />
			</div>
		</div>
		<div class="col-md-3 mt-2">
				<button id="_t61getDNDBtn" type="button" class="rcl-standard-button" onclick="Get_DND()">
				Get Detention & Demurrage Details
				</button>
		</div>
	</div>
	<div class="row appoverDND" style="display:none;">
	<div class="col-md-4" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
	</div>
		<div class="col-md-2"  style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
		<button id="QuoteAgainDND" type="button" style="display:none;"
						class="rcl-standard-button appoverDND"
					onclick="qtn_Approval('Q')">Quote Again</button>
		<button id="AppoverDND" type="button" style="display:none;"
					class="rcl-standard-button appoverDND"
					onclick="qtn_Approval('S')">Appove</button>
		</div>
		<div class="col-md-6" style="text-align: right; margin-top: 5px; margin-bottom: 5px;"></div>
	</div>
</rcl:area>

<br>

<rcl:tabGroup tabList="Export_Detention Export_Demurrage Import_Detention Import_Demurrage" activeTab="ExportDetention">
<div class="rcl-standard-widget-header dtlHeader">
	<span><i class="fas fa-chevron-right"></i></span>
</div>
<div class="pl-3 ml-2">
<br>
		<div class="row border-bottom">
			<div class="col-0x5" style="font-weight: bold;">
				<label class="rcl-std">Service</label>
			</div>
			<div class="col-0x5 " style="font-weight: bold;">
				<label class="rcl-std">Currency</label>
			</div>
			<div class="col-0x5 " style="font-weight: bold;">
				<label class="rcl-std">Port/ Point</label>
			</div>
			<div class="col-0x5" style="font-weight: bold;">
				<label class="rcl-std">Area</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Mode Of Transport</label>
			</div>

			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">From Activity</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">To Activity</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Start Date</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Expiry Data</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Ready for Picup Days</label>
			</div>

			<div class="col-0x5" style="font-weight: bold;">
				<label class="rcl-std">Exclude</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Free Days Exclude</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Cut-Off By</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Cut-Off Time</label>
			</div>
			<div class="col-0x5" style="font-weight: bold;">
				<label class="rcl-std">Combine DnD</label>
			</div>

			<div class="col-0x5" style="font-weight: bold;">
				<label class="rcl-std">Include Free Days in Slab</label>
			</div>
			<div class="col-0x5" style="font-weight: bold;">
				<label class="rcl-std">Ground Rent Icl DnD Invoice</label>
			</div>
			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">Weekend Day</label>
			</div>

	  </div>
 </div>


		<rcl:tabContentPane tabName="ExportDetention">
			<rcl:area id="d62ExportDetention" classes="div(container-fluid)" title="">
				<div class="row ExportDetention" style="margin-left: 0px;">
					<div class="col-md-12 container-fluid">
						<div class="row">
						<input id="d62action" type="text" class="dtlField" style="display: none;" />
						<input id="d62terminal" type="text" class="dtlField" style="display:none;" />
							<rcl:text id="d62service" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d62currency" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"
							lookup="tbl(VRL_CURRENCY)
									rco(CODE)
									rid(d62currency)
									sco(CODE*main)
									sva(d62currency)
									sop(LIKE)"/>
							<rcl:text id="d62pointPort" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d62area" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d62modeOfTransport" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:text id="d62fromActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d62toActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d62startDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d62expiryDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:number id="d62readyForPickupDays" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:select id="d62exclude" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationExclude" />

							<rcl:select id="d62freeDaysExclude" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationFreeDaysExclude" />

							<rcl:select id="d62cutOffBy" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationCutOffBy" />

							<rcl:time id="d62cutOffTime" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d62combineDnd" class="dtlField" />
							</div>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d62includeFreeDaysInSlab" class="dtlField" />
							</div>
							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d62groundRentIclDnDInvoice" class="dtlField" />
							</div>

							<rcl:select id="d62weekendDay1" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay1" />
							<rcl:select id="d62weekendDay2" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay2" />


							<input id="d62contractRefNo" type="text" class="dtlField"  style="display:none;" />
						</div>
					</div>
				</div>
			</rcl:area>
		</rcl:tabContentPane>

		<rcl:tabContentPane tabName="Export_Demurrage">
			<rcl:area id="d63ExportDemurrage" classes="div(container-fluid)" title="">
				<div class="row  ExportDemurrage"  style="margin-left: 0px;">
					<div class="col-md-12 container-fluid">
						<div class="row">
						<input id="d63action" type="text" class="dtlField" style="display: none;" />
						<input id="d63terminal" type="text" class="dtlField" style="display:none;" />
							<rcl:text id="d63service" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d63currency" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"
							lookup="tbl(VRL_CURRENCY)
									rco(CODE)
									rid(d63currency)
									sco(CODE*main)
									sva(d63currency)
									sop(LIKE)"/>
							<rcl:text id="d63pointPort" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d63area" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d63modeOfTransport" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:text id="d63fromActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d63toActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d63startDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d63expiryDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:number id="d63readyForPickupDays" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:select id="d63exclude" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationExclude" />

							<rcl:select id="d63freeDaysExclude" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationFreeDaysExclude" />

							<rcl:select id="d63cutOffBy" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationCutOffBy" />

							<rcl:time id="d63cutOffTime" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d63combineDnd" class="dtlField" />
							</div>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d63includeFreeDaysInSlab" class="dtlField" />
							</div>
							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d63groundRentIclDnDInvoice" class="dtlField" />
							</div>

							<rcl:select id="d63weekendDay1" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay1" />
							<rcl:select id="d63weekendDay2" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay2" />


							<input id="d63contractRefNo" type="text" class="dtlField"  style="display:none;" />

						</div>
					</div>
				</div>
			</rcl:area>
		</rcl:tabContentPane>

		<rcl:tabContentPane tabName="Import_Detention">
			<rcl:area id="d64ImportDetention" classes="div(container-fluid)" title="">
				<div class="row ImportDetention" style="margin-left: 0px;">
					<div class="col-md-12 container-fluid">
						<div class="row">
						<input id="d64action" type="text" class="dtlField" style="display: none;" />
						<input id="d64terminal" type="text" class="dtlField" style="display:none;" />
							<rcl:text id="d64service" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d64currency" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"
							lookup="tbl(VRL_CURRENCY)
									rco(CODE)
									rid(d64currency)
									sco(CODE*main)
									sva(d64currency)
									sop(LIKE)"/>
							<rcl:text id="d64pointPort" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d64area" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d64modeOfTransport" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:text id="d64fromActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d64toActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d64startDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date  id="d64expiryDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:number id="d64readyForPickupDays" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:select id="d64exclude" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationExclude" />

							<rcl:select id="d64freeDaysExclude" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationFreeDaysExclude" />

							<rcl:select id="d64cutOffBy" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationCutOffBy" />

							<rcl:time id="d64cutOffTime" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d64combineDnd" class="dtlField" />
							</div>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d64includeFreeDaysInSlab" class="dtlField" />
							</div>
							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d64groundRentIclDnDInvoice" class="dtlField" />
							</div>

							<rcl:select id="d64weekendDay1" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay1" />
							<rcl:select id="d64weekendDay2" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay2" />


							<input id="d64contractRefNo" type="text" class="dtlField"  style="display:none;" />
						</div>
					</div>
				</div>
			</rcl:area>
		</rcl:tabContentPane>

		<rcl:tabContentPane tabName="Import_Demurrage">
			<rcl:area id="d65ImportDemurrage" classes="div(container-fluid)" title="">
				<div class="row ImportDemurrage" style="margin-left: 0px;">
					<div class="col-md-12 container-fluid">
						<div class="row">
						<input id="d65action" type="text" class="dtlField" style="display: none;" />
						<input id="d65terminal" type="text" class="dtlField" style="display:none;" />
							<rcl:text id="d65service" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d65currency" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"
							lookup="tbl(VRL_CURRENCY)
									rco(CODE)
									rid(d65currency)
									sco(CODE*main)
									sva(d65currency)
									sop(LIKE)"/>
							<rcl:text id="d65pointPort" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d65area" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d65modeOfTransport" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:text id="d65fromActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:text id="d65toActivity" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d65startDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:date id="d65expiryDate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>
							<rcl:number id="d65readyForPickupDays" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<rcl:select id="d65exclude" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationExclude" />

							<rcl:select id="d65freeDaysExclude" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationFreeDaysExclude" />

							<rcl:select id="d65cutOffBy" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationCutOffBy" />

							<rcl:time id="d65cutOffTime" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)"/>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d65combineDnd" class="dtlField" />
							</div>

							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d65includeFreeDaysInSlab" class="dtlField" />
							</div>
							<div class="col-0x5 pl-1 pr-0" style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="d65groundRentIclDnDInvoice" class="dtlField" />
							</div>

							<rcl:select id="d65weekendDay1" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay1" />
							<rcl:select id="d65weekendDay2" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" selectTable="QuotationWeekendDay2" />


							<input id="d65contractRefNo" type="text" class="dtlField"  style="display:none;" />

						</div>
					</div>
				</div>
			</rcl:area>
		</rcl:tabContentPane>
</rcl:tabGroup>

<br>

<rcl:tabGroup tabList="Additional_Free_Days Charge" activeTab="AdditionalFreeDays">
<div class="rcl-standard-widget-header dtlHeader">
	<span><i class="fas fa-chevron-right"></i></span>
</div>

		<rcl:tabContentPane tabName="AdditionalFreeDays">
		<div class="pl-3 ml-2">
		<div class="row border-bottom">
			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">From</label>
			</div>
			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">To</label>
			</div>
			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">FreeDays</label>
			</div>
			<div class="col-md-1 pl-1 pr-2">
					<label>
						Delete
					</label>
			</div>
		</div>
</div>
			<rcl:area id="t66AdditionalFreeDays_D" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll" >
					<div id="t66row" class="row tblRow t66row pt-2">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t66row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row" >
									<input id="t66freeDaysSeqNo" type="number" class="tblField pk" style="display:none;" />
										<rcl:number id="t66from" classes="div(col-md-1 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t66to" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t66freeDays" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<div id="t66selectDeleteLabel" class="col-0x5" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
											<input id="t66selectDelete" type="checkbox" class="tblField" style="margin-top: 0px;"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: center; margin-top: -5; margin-bottom: -5px;">
						<button id="_t66addBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="newAdditionalFreeDays('t66AdditionalFreeDays_D')">Add</button>
						<button id="_t66deleteBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="deleteAdditionalFreeDays('t66AdditionalFreeDays_D')">Delete</button>
					</div>
				</div>
			</rcl:area>

			<rcl:area id="t67AdditionalFreeDays_E" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t67row" class="row tblRow t67row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t67row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<input id="t67freeDaysSeqNo" type="number" class="tblField pk" style="display:none;" />
										<rcl:number id="t67from" classes="div(col-md-1 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t67to" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t67freeDays" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<div id="t67selectDeleteLabel" class="col-0x5" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
											<input id="t67selectDelete" type="checkbox" class="tblField" style="margin-top: 0px;"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: center; margin-top: -5; margin-bottom: -5px;">
						<button id="_t67addBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="newAdditionalFreeDays('t67AdditionalFreeDays_E')">Add</button>
						<button id="_t67deleteBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="deleteAdditionalFreeDays('t67AdditionalFreeDays_E')">Delete</button>
					</div>
				</div>
			</rcl:area>

			<rcl:area id="t68AdditionalFreeDays_I" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t68row" class="row tblRow t68row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t68row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<input id="t68freeDaysSeqNo" type="number" class="tblField pk" style="display:none;" />
										<rcl:number id="t68from" classes="div(col-md-1 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t68to" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t68freeDays" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<div id="t68selectDeleteLabel" class="col-0x5" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
											<input id="t68selectDelete" type="checkbox" class="tblField" style="margin-top: 0px;"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: center; margin-top: -5; margin-bottom: -5px;">
						<button id="_t68addBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="newAdditionalFreeDays('t68AdditionalFreeDays_I')">Add</button>
						<button id="_t68deleteBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="deleteAdditionalFreeDays('t68AdditionalFreeDays_I')">Delete</button>
					</div>
				</div>
			</rcl:area>

			<rcl:area id="t69AdditionalFreeDays_M" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t69row" class="row tblRow t69row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t69row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<input id="t69freeDaysSeqNo" type="number" class="tblField pk" style="display:none;" />
										<rcl:number id="t69from" classes="div(col-md-1 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t69to" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<rcl:number id="t69freeDays" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="dec(4,0)"/>
										<div id="t69selectDeleteLabel" class="col-0x5" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
											<input id="t69selectDelete" type="checkbox" class="tblField" style="margin-top: 0px;"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: center; margin-top: -5; margin-bottom: -5px;">
						<button id="_t69addBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="newAdditionalFreeDays('t69AdditionalFreeDays_M')">Add</button>
						<button id="_t69deleteBtn" type="button" class="rcl-standard-button" style="display:none;" onclick="deleteAdditionalFreeDays('t69AdditionalFreeDays_M')">Delete</button>
					</div>
				</div>
			</rcl:area>
		</rcl:tabContentPane>

		<rcl:tabContentPane tabName="Charge">

<div class="pl-3 ml-2">
		<div class="row border-bottom">
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Charge Code</label>
			</div>
			<div class="col-0x75 " style="font-weight: bold;">
				<label class="rcl-std">Charge Type</label>
			</div>
			<div class="col-0x5 " style="font-weight: bold;">
				<label class="rcl-std">Subledger Category</label>
			</div>
			<div class="col-0x5 pl-2" style="font-weight: bold;">
				<label class="rcl-std">Subledger Code</label>
			</div>
			<div class="col-0x75 pl-3" style="font-weight: bold;">
				<label class="rcl-std">Size</label>
			</div>

			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Type</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Master Free Days</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Free Days</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Rate Exclude</label>
			</div>
			<div class="col-0x75" style="font-weight: bold;">
				<label class="rcl-std">Special Handling</label>
			</div>

			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">Slab 1</label>
			</div>

			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">Slab 2</label>
			</div>
			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">Slab 3</label>
			</div>
			<div class="col-md-1" style="font-weight: bold;">
				<label class="rcl-std">Slab 4</label>
			</div>
			<div class="col-0x75  pl-0 pr-0" style="font-weight: bold; text-align: center;">
				<label class="rcl-std">Slab 5</label>
			</div>
		</div>

		<div class="row border-bottom" style="margin-top: 10px; margin-bottom: 5px;">
		<div class="offset-7"></div>
			<div class="col-0x5">
				<label class="rcl-std">Days</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Rate</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Days</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Rate</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Days</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Rate</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Days</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Rate</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Days</label>
			</div>
			<div class="col-0x5">
				<label class="rcl-std">Rate</label>
			</div>


		</div>

 </div>
			<rcl:area id="t81Charge_D" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t81row" class="row tblRow t81row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t81row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<%-- input id="t81area" type="text" class="tblField" style="display:none;" /--%>
										<rcl:text id="t81chargeCode" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:select id="t81chargeType" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="ChargeType" />
										<rcl:text id="t81subledgerCategory" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t81subledgerCode" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t81size" classes="div(col-0x5 pl-1 pr-0 ml-1) ctr(dtlField)"/>

										<rcl:text id="t81type" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:text id="t81masterFreeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:number id="t81freeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(3,0)"/>
										<rcl:select id="t81rateExclude" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" selectTable="RateExclude" />
										<rcl:select id="t81specialHandling" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="SpecialHandling" />

										<rcl:number id="t81slab1Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t81slab1Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t81slab2Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t81slab2Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t81slab3Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>

										<rcl:number id="t81slab3Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t81slab4Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t81slab4Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t81slab5Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t81slab5Rate" classes="div(col-0x5 pl-1 pr-2) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</rcl:area>

			<rcl:area id="t82Charge_E" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t82row" class="row tblRow t82row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t82row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<%-- input id="t82area" type="text" class="tblField" style="display:none;" /--%>
										<rcl:text id="t82chargeCode" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:select id="t82chargeType" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="ChargeType" />
										<rcl:text id="t82subledgerCategory" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t82subledgerCode" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t82size" classes="div(col-0x5 pl-1 pr-0 ml-1) ctr(dtlField)"/>

										<rcl:text id="t82type" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:text id="t82masterFreeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:number id="t82freeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(3,0)"/>
										<rcl:select id="t82rateExclude" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" selectTable="RateExclude"/>
										<rcl:select id="t82specialHandling" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="SpecialHandling" />

										<rcl:number id="t82slab1Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t82slab1Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t82slab2Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t82slab2Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t82slab3Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>

										<rcl:number id="t82slab3Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t82slab4Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t82slab4Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t82slab5Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t82slab5Rate" classes="div(col-0x5 pl-1 pr-2) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</rcl:area>

			<rcl:area id="t83Charge_I" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t83row" class="row tblRow t83row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t83row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<%-- input id="t83area" type="text" class="tblField" style="display:none;" /--%>
										<rcl:text id="t83chargeCode" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:select id="t83chargeType" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="ChargeType" />
										<rcl:text id="t83subledgerCategory" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t83subledgerCode" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t83size" classes="div(col-0x5 pl-1 pr-0 ml-1) ctr(dtlField)"/>

										<rcl:text id="t83type" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:text id="t83masterFreeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:number id="t83freeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(3,0)"/>
										<rcl:select id="t83rateExclude" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" selectTable="RateExclude"/>
										<rcl:select id="t83specialHandling" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="SpecialHandling" />

										<rcl:number id="t83slab1Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t83slab1Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t83slab2Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t83slab2Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t83slab3Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>

										<rcl:number id="t83slab3Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t83slab4Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t83slab4Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t83slab5Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t83slab5Rate" classes="div(col-0x5 pl-1 pr-2) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</rcl:area>

			<rcl:area id="t84Charge_M" classes="div(container-fluid targetDiv)" title="" areaMode="table">
				<div id="routhingScroll">
					<div id="t84row" class="row tblRow t84row">
						<div class="container-fluid">
							<div onclick="onselectBG('#-RowId-#','t84row');" class="row">
								<div class="col-md-12 container-fluid">
									<div class="row">
									<%-- input id="t84area" type="text" class="tblField" style="display:none;" /--%>

										<rcl:text id="t84chargeCode" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:select id="t84chargeType" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="ChargeType" />
										<rcl:text id="t84subledgerCategory" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t84subledgerCode" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)"/>
										<rcl:text id="t84size" classes="div(col-0x5 pl-1 pr-0 ml-1) ctr(dtlField)"/>

										<rcl:text id="t84type" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:text id="t84masterFreeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)"/>
										<rcl:number id="t84freeDays" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" check="dec(3,0)"/>
										<rcl:select id="t84rateExclude" classes="div(col-0x75 pl-1 pr-0 ml-1) ctr(dtlField)" selectTable="RateExclude"/>
										<rcl:select id="t84specialHandling" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" selectTable="SpecialHandling" />

										<rcl:number id="t84slab1Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t84slab1Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t84slab2Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t84slab2Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t84slab3Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>

										<rcl:number id="t84slab3Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t84slab4Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t84slab4Rate" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
										<rcl:number id="t84slab5Days" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" defaultValue="0"/>
										<rcl:number id="t84slab5Rate" classes="div(col-0x5 pl-1 pr-2) ctr(dtlField)" defaultValue="0" check="dec(6,3)"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</rcl:area>
		</rcl:tabContentPane>


</rcl:tabGroup>


<script type="text/javascript">
var getSaveData = "";
var OrgDataExportDetention  = {};
var OrgDataExportDemurrage  = {};
var OrgImportDetention  = {};
var OrgImportDemurrage  = {};

var OrgAdditionalFreeDays_D  = [];
var OrgAdditionalFreeDays_E  = [];
var OrgAdditionalFreeDays_I  = [];
var OrgAdditionalFreeDays_M  = [];

var OrgCharge_D  = [];
var OrgCharge_E  = [];
var OrgCharge_I  = [];
var OrgCharge_M  = [];

//-----Start OrgDataSave-----//
var OrgDataExportDetention_s  = {};
var OrgDataExportDemurrage_s  = {};
var OrgImportDetention_s  = {};
var OrgImportDemurrage_s  = {};

var OrgAdditionalFreeDays_D_s  = [];
var OrgAdditionalFreeDays_E_s  = [];
var OrgAdditionalFreeDays_I_s  = [];
var OrgAdditionalFreeDays_M_s  = [];

var OrgCharge_D_s  = [];
var OrgCharge_E_s  = [];
var OrgCharge_I_s  = [];
var OrgCharge_M_s  = [];
//-----End OrgDataSave-----//

var GetOrgGTA = {};

var gtaFlag = '';
var isGetDnd = '';
	$(document).ready(function() {


		//ischeckGtaDND();

		rptTableInit("t61RoutingHeader", { disabledSortControl: true });
		$('#d62ExportDetentionHeader').removeClass("rcl-standard-widget-header");
		$('#d63ExportDemurrageHeader').removeClass("rcl-standard-widget-header");
		$('#d64ImportDetentionHeader').removeClass("rcl-standard-widget-header");
		$('#d65ImportDemurrageHeader').removeClass("rcl-standard-widget-header");

		$('#t66AdditionalFreeDays_DHeader').removeClass("rcl-standard-widget-header");
		$('#t67AdditionalFreeDays_EHeader').removeClass("rcl-standard-widget-header");
		$('#t68AdditionalFreeDays_IHeader').removeClass("rcl-standard-widget-header");
		$('#t69AdditionalFreeDays_MHeader').removeClass("rcl-standard-widget-header");
		$('#t81Charge_DHeader').removeClass("rcl-standard-widget-header");
		$('#t82Charge_EHeader').removeClass("rcl-standard-widget-header");
		$('#t83Charge_IHeader').removeClass("rcl-standard-widget-header");
		$('#t84Charge_MHeader').removeClass("rcl-standard-widget-header");

		rptTableInit("t66AdditionalFreeDays_D", { disabledSortControl: true });
		rptTableInit("t67AdditionalFreeDays_E", { disabledSortControl: true });
		rptTableInit("t68AdditionalFreeDays_I", { disabledSortControl: true });
		rptTableInit("t69AdditionalFreeDays_M", { disabledSortControl: true });
		rptTableInit("t81Charge_D", { disabledSortControl: true });
		rptTableInit("t82Charge_E", { disabledSortControl: true });
		rptTableInit("t83Charge_I", { disabledSortControl: true });
		rptTableInit("t84Charge_M", { disabledSortControl: true });



		$('a[data-toggle="tab"]').on('shown.bs.tab', function(event){

			var activeTab = $(event.target).text();         // active tab
		    var previousTab = $(event.relatedTarget).text();  // previous tab
		    //ischeckGtaDND();

		    if(!IsFsc || IsAppover){
		    	IsDisplayview();
			}
			    if(activeTab == "Export Detention"){
			    	$('.targetDiv').hide();
			    	$('#t66AdditionalFreeDays_D').show();
					$("#t81Charge_D").show();
				}else if(activeTab == "Export Demurrage"){
			    	$('.targetDiv').hide();
			    	$('#t69AdditionalFreeDays_M').show();
					$("#t84Charge_M").show();
				}else if(activeTab == "Import Detention"){
					$('.targetDiv').hide();
					$('#t68AdditionalFreeDays_I').show();
					$("#t83Charge_I").show();
				}else if(activeTab == "Import Demurrage"){
					$('.targetDiv').hide();
					$('#t67AdditionalFreeDays_E').show();
					$("#t82Charge_E").show();
				}else{

				 if(activeTab == "Det. & Demure."){
					$('.targetDiv').hide();
					$('#t66AdditionalFreeDays_D').show();
					$("#t81Charge_D").show();
				}
		    }
		});

	});



	 function TrigGTA(flag) {
		 ischeckGtaDND();
		   if (document.getElementById('gtaFlag').checked == true) {
			 isCheckGTA('Y');
	          return false;
	      } else {
	    	 if(flag != undefined){
	    		 isCheckGTA('N');

	    	 }else{
	    		 dialogGeneric("Warning", "Please Re-Extract DND Details" , "Ok", "No").then(
	    				 function(overwrite){
								if(overwrite){
									isCheckGTA('N');
									//ClearDisplayDND();
									//GetDisplayDataDND(GetOrgGTA);
									Get_DND();
									return true;
								}else{
									isCheckGTA('Y');
									document.getElementById('gtaFlag').checked = true;
									 return false;

								}

		     			 });
	    	 }

	    }
	 }


	function isCheckGTA(flag){
		console.log("isCheckGTA :: "+flag);
		var ExpDT = rptGetDataFromSingleArea("d62ExportDetention");
		var ExpDR = rptGetDataFromSingleArea("d63ExportDemurrage");
		var ImpDT = rptGetDataFromSingleArea("d64ImportDetention");
		var ImpDR = rptGetDataFromSingleArea("d65ImportDemurrage");
		if(flag == 'Y'){
			$.each(ExpDT, function(k, v) {
				if(k == 'currency' || k == "combineDnd" || k == "includeFreeDaysInSlab"){
					setCustomTagProperty("d62"+k, {dis:false});
				}
			});
			$.each(ExpDR, function(k, v) {
				if(k == 'currency' || k == "combineDnd" || k == "includeFreeDaysInSlab"){
					setCustomTagProperty("d63"+k, {dis:false});
				}
			});
			$.each(ImpDT, function(k, v) {
				if(k == 'currency' || k == "combineDnd" || k == "includeFreeDaysInSlab"){
					setCustomTagProperty("d64"+k, {dis:false});
				}
			});
			$.each(ImpDR, function(k, v) {
				if(k == 'currency' || k == "combineDnd" || k == "includeFreeDaysInSlab"){
					setCustomTagProperty("d65"+k, {dis:false});
				}
			});

			//var gettabcharge = {''}
			var charges_D = rptGetDataFromDisplayAll("t81Charge_D");
			var charges_E = rptGetDataFromDisplayAll("t82Charge_E");
			var charges_I = rptGetDataFromDisplayAll("t83Charge_I");
			var charges_M = rptGetDataFromDisplayAll("t84Charge_M");
			for (var f = 0; f < charges_D.length; f++) {
				$.each(charges_D[f], function(k, v) {
					if(k == 'slab1Days' || k == "slab1Rate" || k == 'slab2Days' || k == "slab2Rate" || k == 'slab3Days' || k == "slab3Rate"
						|| k == 'slab4Days' || k == "slab4Rate" || k == 'slab5Days' || k == "slab5Rate"){
						setCustomTagProperty("t81"+k+"-"+f, {dis:false});
					}
				});
			}

			for (var g = 0; g < charges_E.length; g++) {
				$.each(charges_E[g], function(k, v) {
					if(k == 'slab1Days' || k == "slab1Rate" || k == 'slab2Days' || k == "slab2Rate" || k == 'slab3Days' || k == "slab3Rate"
						|| k == 'slab4Days' || k == "slab4Rate" || k == 'slab5Days' || k == "slab5Rate"){
						setCustomTagProperty("t82"+k+"-"+g, {dis:false});
					}
				});
			}

			for (var h = 0; h < charges_I.length; h++) {
				$.each(charges_I[h], function(k, v) {
					if(k == 'slab1Days' || k == "slab1Rate" || k == 'slab2Days' || k == "slab2Rate" || k == 'slab3Days' || k == "slab3Rate"
						|| k == 'slab4Days' || k == "slab4Rate" || k == 'slab5Days' || k == "slab5Rate"){
						setCustomTagProperty("t83"+k+"-"+h, {dis:false});
					}
				});
			}
			for (var j = 0; j < charges_M.length; j++) {
				$.each(charges_M[j], function(k, v) {
					if(k == 'slab1Days' || k == "slab1Rate" || k == 'slab2Days' || k == "slab2Rate" || k == 'slab3Days' || k == "slab3Rate"
						|| k == 'slab4Days' || k == "slab4Rate" || k == 'slab5Days' || k == "slab5Rate"){
						setCustomTagProperty("t84"+k+"-"+j, {dis:false});
					}
				});

			}

		}else{
			ischeckGtaDND();

		}
	}

	function ischeckGtaDND(){
		console.log("ischeckGtaDND")
		var ExpDT = rptGetDataFromSingleArea("d62ExportDetention");
		var ExpDR = rptGetDataFromSingleArea("d63ExportDemurrage");
		var ImpDT = rptGetDataFromSingleArea("d64ImportDetention");
		var ImpDR = rptGetDataFromSingleArea("d65ImportDemurrage");
		$.each(ExpDT, function(k, v) {
			if(k != "includeFreeDaysInSlab" && k != 'currency' ){
				setCustomTagProperty("d62"+k, {dis:true});
			}else{
				setCustomTagProperty("d62"+k, {dis:false});
			}
		});
		$.each(ExpDR, function(k, v) {
			if(k != "includeFreeDaysInSlab" && k != 'currency' ){
				setCustomTagProperty("d63"+k, {dis:true});
			}else{
				setCustomTagProperty("d63"+k, {dis:false});
			}
		});
		$.each(ImpDT, function(k, v) {
			if(k != "includeFreeDaysInSlab" && k != 'currency' ){
				setCustomTagProperty("d64"+k, {dis:true});
			}else{
				setCustomTagProperty("d64"+k, {dis:false});
			}
		});
		$.each(ImpDR, function(k, v) {
			if(k != "includeFreeDaysInSlab" && k != 'currency' ){
				setCustomTagProperty("d65"+k, {dis:true});
			}else{
				setCustomTagProperty("d65"+k, {dis:false});
			}
		});

		var charges_D = rptGetDataFromDisplayAll("t81Charge_D");
		var charges_E = rptGetDataFromDisplayAll("t82Charge_E");
		var charges_I = rptGetDataFromDisplayAll("t83Charge_I");
		var charges_M = rptGetDataFromDisplayAll("t84Charge_M");
		for (var f = 0; f < charges_D.length; f++) {
			$.each(charges_D[f], function(k, v) {//
				if(k != "freeDays"){
					setCustomTagProperty("t81"+k+"-"+f, {dis:true});
				}

			});
		}

		for (var g = 0; g < charges_E.length; g++) {
			$.each(charges_E[g], function(k, v) {
				if(k != "freeDays"){
					setCustomTagProperty("t82"+k+"-"+g, {dis:true});
				}

			});
		}

		for (var h = 0; h < charges_I.length; h++) {
			$.each(charges_I[h], function(k, v) {
				if(k != "freeDays"){
					setCustomTagProperty("t83"+k+"-"+h, {dis:true});
				}

			});
		}
		for (var j = 0; j < charges_M.length; j++) {
			$.each(charges_M[j], function(k, v) {
				if(k != "freeDays"){
					setCustomTagProperty("t84"+k+"-"+j, {dis:true});
				}
			});

		}
	}

	function isDisplayHeader(IScase,IsData){
		if(userData != undefined){
			if(userData.fscCode == 'R'){
				$("#t61GTALable").show();
				//$("#t61subjectGrcLable").show();
			}else{
				$("#t61GTALable").hide();
				//$("#t61subjectGrcLable").hide();
			}
		}else{
			$("#t61GTALable").hide();
			//$("#t61subjectGrcLable").hide();
		}


		if(IScase ==  'N'){// case onload

			$('.ExportDetention').addClass('displayDND');
			$('.ExportDemurrage').addClass('displayDND');
			$('.ImportDetention').addClass('displayDND');
			$('.ImportDemurrage').addClass('displayDND');

		}else if(IScase ==  'E' || IScase ==  'A'){// case Edit
			$('.ExportDetention').removeClass('displayDND');
			$('.ExportDemurrage').removeClass('displayDND');
			$('.ImportDetention').removeClass('displayDND');
			$('.ImportDemurrage').removeClass('displayDND');

			if(isEmptyObject(IsData.exportDetentionDetails)){
				$('.ExportDetention').addClass('displayDND');
			}

			if(isEmptyObject(IsData.exportDemurrageDetails)){
				$('.ExportDemurrage').addClass('displayDND');
			}

			if(isEmptyObject(IsData.importDetentionDetails)){
				$('.ImportDetention').addClass('displayDND');
			}

			if(isEmptyObject(IsData.importDemurrageDetails)){
				$('.ImportDemurrage').addClass('displayDND');
			}

		}

	}

	function setDNDRowViewForNewRow(){

	}

	function fetchDataDetentionDemurrageTab(Iscontent){
		var content = {};
		var contentcheck = {};

		content = Iscontent.DNDdata;
		contentcheck = Iscontent.DNDdataCheck;
		//gtaFlag = '';
		fnsetcheckbox(content.gtaFlag,'gtaFlag');
		isGetDnd = 'N';
		SaveData(content);
		isDisplayHeader('E',content);
		$('.targetDiv').hide();


		var exportDetentionDetails = IssetTime(content.exportDetentionDetails);
		var exportDemurrageDetails = IssetTime(content.exportDemurrageDetails);
		var importDetentionDetails = IssetTime(content.importDetentionDetails);
		var importDemurrageDetails = IssetTime(content.importDemurrageDetails);

		///test
		rptSetSingleAreaValues("d62ExportDetention",SetAction_edit_Object(replaceNulltoEmpty(exportDetentionDetails)));
		rptSetSingleAreaValues("d63ExportDemurrage",SetAction_edit_Object(replaceNulltoEmpty(exportDemurrageDetails)));
		rptSetSingleAreaValues("d64ImportDetention",SetAction_edit_Object(replaceNulltoEmpty(importDetentionDetails)));
		rptSetSingleAreaValues("d65ImportDemurrage",SetAction_edit_Object(replaceNulltoEmpty(importDemurrageDetails)));
		fetchDataAdditionalFreeDays("t66AdditionalFreeDays_D",content.additionalFreeDays_D);
		fetchDataCharge("t81Charge_D",content.charges_D);
		fetchDataAdditionalFreeDays("t67AdditionalFreeDays_E",getSaveData.additionalFreeDays_E);
	    fetchDataCharge("t82Charge_E",getSaveData.charges_E);
	    fetchDataAdditionalFreeDays("t68AdditionalFreeDays_I",getSaveData.additionalFreeDays_I);
		fetchDataCharge("t83Charge_I",getSaveData.charges_I);
		fetchDataAdditionalFreeDays("t69AdditionalFreeDays_M",getSaveData.additionalFreeDays_M);
		fetchDataCharge("t84Charge_M",getSaveData.charges_M);
		if(IsAppover){
			IsCheckAdjustDisplay(content,contentcheck);
		}
		setOrgData();
		setOrgData_SavePreview();
		setOrgData_GTA(content);
		//ischeckGtaDND();
		TrigGTA("onload");

		if(flagDND){
			ischeckhighlighting(content,contentcheck);
		}

		manebtn(content);
	}

	function IssetTime(content){
		var Isdata = {};
		Isdata = content;
		if(!isEmptyObject(Isdata)){
			if(Isdata.cutOffTime == undefined || Isdata.cutOffTime == '0'){
				Isdata.cutOffTime = settime_qnt(Isdata.cutOffTime);
			}else{
				var settime = "";
				settime = Isdata.cutOffTime
				Isdata.cutOffTime =  Isdata.cutOffTime.toString();
			}
		}
		return Isdata;
	}

	function ischeckhighlighting(IsData,IsDataCheck){
		var chkFreeDays_I = IsDataCheck.additionalFreeDays_I;
		var chkFreeDays_E = IsDataCheck.additionalFreeDays_E;
		var chkFreeDays_D = IsDataCheck.additionalFreeDays_D;
		var chkFreeDays_M = IsDataCheck.additionalFreeDays_M;

		if(chkFreeDays_I.length > 0){
			ischeckhighlightingLoop(chkFreeDays_I,"t68");
		}
		if(chkFreeDays_E.length > 0){
			ischeckhighlightingLoop(chkFreeDays_E,"t67");
		}
		if(chkFreeDays_D.length > 0){
			ischeckhighlightingLoop(chkFreeDays_D,"t66");
		}
		if(chkFreeDays_M.length > 0){
			ischeckhighlightingLoop(chkFreeDays_M,"t69");
		}
	}

	function ischeckhighlightingLoop(IsData,namerow){
		for(var m = 0; m < IsData.length; m++){
				if(IsData[m].freeDays == 'Y' || IsData[m].from == 'Y' || IsData[m].to == 'Y'){
					$('#'+namerow+"from-"+m).toggleClass(" Newrate ");
					$('#'+namerow+"to-"+m).toggleClass(" Newrate ");
					$('#'+namerow+"freeDays-"+m).toggleClass(" Newrate ");

					$("#"+namerow+"selectDeleteLabel-"+m).remove();

				}
		}
	}


	function IsCheckAdjustDisplay(IsData,IsDataCheck){
		//check Import Demurrage
		$.each(IsDataCheck.importDemurrageDetails, function(k, v) {
			if(hilighcolor(v,k)){
				$('#d65'+k).addClass('Newrate');
			}
		});

		//check Import Detention
		$.each(IsDataCheck.importDetentionDetails, function(k, v) {
			if(hilighcolor(v,k)){
				$('#d64'+k).addClass('Newrate');
			}
		});

		//check Export Detention
		$.each(IsDataCheck.exportDemurrageDetails, function(k, v) {
			if(hilighcolor(v,k)){
				$('#d63'+k).addClass('Newrate');
			}
		});

		//check Export Demurrage
		$.each(IsDataCheck.exportDetentionDetails, function(k, v) {
			if(hilighcolor(v,k)){
				$('#d62'+k).addClass('Newrate');
			}
		});

		//t81Charge_D
		for (var d = 0; d < IsDataCheck.charges_D.length; d++) {
			$.each(IsDataCheck.charges_D[d], function(k, v) {
				if(hilighcolor(v,k)){
					$('#t81'+k+'-'+d).addClass('Newrate');
				}
			});
		}

		//t82Charge_E
		for (var e = 0; e < IsDataCheck.charges_E.length; e++) {
			$.each(IsDataCheck.charges_E[e], function(k, v) {
				if(hilighcolor(v,k)){
					$('#t82'+k+'-'+e).addClass('Newrate');
				}
			});
		}

		//t83Charge_I
		for (var i = 0; i < IsDataCheck.charges_I.length; i++) {
			$.each(IsDataCheck.charges_I[i], function(k, v) {
				if(hilighcolor(v,k)){
					$('#t83'+k+'-'+i).addClass('Newrate');
				}
			});
		}

		//t84Charge_M
		for (var m = 0; m < IsDataCheck.charges_M.length; m++) {
			$.each(IsDataCheck.charges_M[m], function(k, v) {
				if(hilighcolor(v,k)){
					$('#t84'+k+'-'+m).addClass('Newrate');
				}
			});
		}


}

	function hilighcolor(value,key){
		if(value == 'Y'){
			return true;
		}else{
			return false;
		}
	}


	function SaveData(content){
		getSaveData = content;
	}

	function fetchDataAdditionalFreeDays(Area,Data){
		fetchGoblaData(Area,Data)
	}

	function fetchDataCharge(Area,Data){
		fetchGoblaData(Area,Data)
	}

	function fetchGoblaData(Area,Data){
		rptAddDataWithCache(Area, SetAction_edit(Data));
		rptDisplayTable(Area);

	}


	function newAdditionalFreeDays(area) {

		rptInsertNewRowBefore(area, 'end');
		rptForeachRow(area, setDNDRowViewForNewRow, false, ":last");
		 var newRow = $("#"+area).find(".tblRow").not(":last").last();
		 var data = rptGetDataFromDisplay(area, newRow.attr("id"));
		 data.action = 'i';
		}

	var newArrayAdditionalFreeDays_D = [];
	var newArrayAdditionalFreeDays_E = [];
	var newArrayAdditionalFreeDays_I = [];
	var newArrayAdditionalFreeDays_M = [];
	function deleteAdditionalFreeDays(area){
		if(area == "t66AdditionalFreeDays_D"){
			setarrayDeletAll(area,newArrayAdditionalFreeDays_D);
		}else if(area == "t67AdditionalFreeDays_E"){
			setarrayDeletAll(area,newArrayAdditionalFreeDays_E);
		}else if(area == "t68AdditionalFreeDays_I"){
			setarrayDeletAll(area,newArrayAdditionalFreeDays_I);
		}else{
			setarrayDeletAll(area,newArrayAdditionalFreeDays_M);
		}
		rptTryToDeleteSelectedRows(area, "selectDelete", true);
		rptForeachRow(area, setDNDRowViewForNewRow, false, ":last");
	}

	function Get_DND(){
		result = true;
		var getRouting = getRouting_New();
		if(GetequipmentDtl().length < 1){
			result = false;
			errorMsg = "Equipment details not found for the selected corridor";
		}else if(getRouting.length < 1){
			result = false;
			errorMsg = "Routing not found for the selected corridor";
		}

		if(!result){
			dialogGeneric("Warning", errorMsg , "Ok");
			return false;
		}else{
			var getMaintainacetab = rptGetDataFromSingleArea("d10MaintenanceHeader");
			var getRoutingHeader = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
			//var getRountingSection = GetObjectSeletor(rptGetDataFromDisplayAll("t31containerArea"),isSelectCntr);
			var getContainerSection = rptGetDataFromDisplayAll("t31containerArea");

			var dataRequest = {};
			for(var i = 0; i < getRoutingHeader.length; i++){
				if(i == isSelect){
					
					var dgApprovalId = "";
					if(getRoutingHeader[i].inqType == "D"){
						dgApprovalId = getRoutingHeader[i].oogBkkInq;
						}
					
					dataRequest = {
							"userData" : userData,
							"expiryDate": splitDate(getMaintainacetab.expDate)
							,"pod": getRoutingHeader[i].pod
							,"pol": getRoutingHeader[i].pol
							,"del": getRoutingHeader[i].del
							,"por": getRoutingHeader[i].por
							,"socCoc": getRoutingHeader[i].socCoc
							,"effectiveDate":splitDate(getMaintainacetab.effDate)
							,"equipmentDtl": GetequipmentDtl()
							,"routing" : getRouting
							,"dgApprovalId" : dgApprovalId

						}

					//getResultAjax("WS_QTN_GET_DND", dataRequesttest).done(handleDisplayDND);
					getResultAjax("WS_QTN_GET_DND", dataRequest).done(handleDisplayDND);
				}
			}

			function handleDisplayDND(response) {
				if(response.resultCode == "200"){
					//console.log("handleDisplayDND response:"+JSON.stringify(response.resultContent));
					_addDND(response.resultContent);
				}else if(response.resultCode == '401'){

					window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
					
				}else{
					dialogGeneric("Warning", response.resultMessage, "Ok");
				}

			}

			}


	}

	function Get_DND_ForCopyDG(){
		result = true;
		var getRouting = getRouting_New();
		if(GetequipmentDtl().length < 1){
			result = false;
			errorMsg = "Equipment details not found for the selected corridor";
		}else if(getRouting.length < 1){
			result = false;
			errorMsg = "Routing not found for the selected corridor";
		}

		if(!result){
			dialogGeneric("Warning", errorMsg , "Ok");
			return false;
		}else{
			var getMaintainacetab = rptGetDataFromSingleArea("d10MaintenanceHeader");
			var getRoutingHeader = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
			//var getRountingSection = GetObjectSeletor(rptGetDataFromDisplayAll("t31containerArea"),isSelectCntr);
			var getContainerSection = rptGetDataFromDisplayAll("t31containerArea");

			var dataRequest = {};
			for(var i = 0; i < getRoutingHeader.length; i++){
				if(i == isSelect){
					dataRequest = {
							"userData" : userData,
							"expiryDate": splitDate(getMaintainacetab.expDate)
							,"pod": getRoutingHeader[i].pod
							,"pol": getRoutingHeader[i].pol
							,"del": getRoutingHeader[i].del
							,"por": getRoutingHeader[i].por
							,"socCoc": getRoutingHeader[i].socCoc
							,"effectiveDate":splitDate(getMaintainacetab.effDate)
							,"equipmentDtl": GetequipmentDtl()
							,"routing" : getRouting

						}
				}
			}

			return dataRequest;

		}
	}



	function GetequipmentDtl(){
		var Newoject = [];
		var getContainerSection = rptGetDataFromDisplayAll("t31containerArea");
		for(var z = 0; z < getContainerSection.length; z++){
			if(getContainerSection[z].action != 'd'){
				var data = {};
				data.equipmentSize = getContainerSection[z].size
				data.equipmentType = getContainerSection[z].type
				data.rateType = getContainerSection[z].rateType
				data.shipmentType = getContainerSection[z].shipmentType
				data.polStatus = getContainerSection[z].polStatus
				data.podStatus = getContainerSection[z].podStatus
				Newoject.push(data);
			}
		}

		return Newoject;
	}

	function ISsetisGetDnd(flag){
		isGetDnd = flag;
	}

	function _addDND(dataDND){
		isDisplayHeader('A',dataDND);

		isGetDnd = 'Y';

		var exportDetentionDetails = dataDND.exportDetentionDetails;
		var additionalFreeDays_D = dataDND.additionalFreeDays_D;
		var charges_D = dataDND.charges_D;

		var exportDemurrageDetails = dataDND.exportDemurrageDetails;
		var additionalFreeDays_M = dataDND.additionalFreeDays_M;
		var charges_M = dataDND.charges_M;

		var importDetentionDetails = dataDND.importDetentionDetails;
		var additionalFreeDays_I = dataDND.additionalFreeDays_I;
		var charges_I = dataDND.charges_I;

		var importDemurrageDetails = dataDND.importDemurrageDetails;
		var additionalFreeDays_E = dataDND.additionalFreeDays_E;
		var charges_E = dataDND.charges_E;

		/*dataDND.exportDetentionDetails.action = "i";
		dataDND.exportDemurrageDetails.action = "i";
		dataDND.importDetentionDetails.action = "i";
		dataDND.importDemurrageDetails.action = "i";*/



		ClearDisplayDND();

		GetDisplayDataDND(dataDND);
		setOrgData_GTA(dataDND);

	}

	function ClearDisplayDNDHeader(){
		ClearDisplayDND();
	}

	function ClearDisplayDND(){
		check_ClearDisplay(rptGetDataFromDisplayAll("t66AdditionalFreeDays_D"),"t66AdditionalFreeDays_D");
		check_ClearDisplay(rptGetDataFromDisplayAll("t81Charge_D"),"t81Charge_D");
		check_ClearDisplay(rptGetDataFromDisplayAll("t67AdditionalFreeDays_E"),"t67AdditionalFreeDays_E");
		check_ClearDisplay(rptGetDataFromDisplayAll("t82Charge_E"),"t82Charge_E");
		check_ClearDisplay(rptGetDataFromDisplayAll("t68AdditionalFreeDays_I"),"t68AdditionalFreeDays_I");
		check_ClearDisplay(rptGetDataFromDisplayAll("t83Charge_I"),"t83Charge_I");
		check_ClearDisplay(rptGetDataFromDisplayAll("t69AdditionalFreeDays_M"),"t69AdditionalFreeDays_M");
		check_ClearDisplay(rptGetDataFromDisplayAll("t84Charge_M"),"t84Charge_M");
	}

	function ClearDisplayDNDOnchange(){
		ClearDisplayDND();
		rptSetSingleAreaValues("d62ExportDetention",{});
		rptSetSingleAreaValues("d63ExportDemurrage",{});
		rptSetSingleAreaValues("d64ImportDetention",{});
		rptSetSingleAreaValues("d65ImportDemurrage",{});

		$('.ExportDetention').addClass('displayDND');
		$('.ExportDemurrage').addClass('displayDND');
		$('.ImportDetention').addClass('displayDND');
		$('.ImportDemurrage').addClass('displayDND');

		clearBtn();
	}


	function check_ClearDisplay(object,area){
		if(object.length > 0){
			rptClearDisplay(area);
		}
	}

	function SetAction_object_insertDND(object){
		if(! isEmptyObject(object)){
			object.action = 'i';
		}
		return object;
	}

	function GetDisplayDataDND(content){
		//var getHeand = Setaction_I(content);

		rptSetSingleAreaValues("d62ExportDetention",IssetTime(SetAction_object_insertDND(replaceNulltoEmpty(content.exportDetentionDetails))) || {}); //D
		rptSetSingleAreaValues("d63ExportDemurrage",IssetTime(SetAction_object_insertDND(replaceNulltoEmpty(content.exportDemurrageDetails))) || {}); //M
		rptSetSingleAreaValues("d64ImportDetention",IssetTime(SetAction_object_insertDND(replaceNulltoEmpty(content.importDetentionDetails))) || {}); //I
		rptSetSingleAreaValues("d65ImportDemurrage",IssetTime(SetAction_object_insertDND(replaceNulltoEmpty(content.importDemurrageDetails))) || {}); //E
		/*console.log("exportDetentionDetails :: "+ JSON.stringify(content.exportDetentionDetails));
		console.log("d62ExportDetention :: "+ JSON.stringify(rptGetDataFromSingleArea('d62ExportDetention')));
		console.log("exportDemurrageDetails :: "+ JSON.stringify(content.exportDemurrageDetails));
		console.log("d63ExportDemurrage :: "+ JSON.stringify(rptGetDataFromSingleArea("d63ExportDemurrage")));*/
		///test
		Display_GetDnD("t66AdditionalFreeDays_D",content.additionalFreeDays_D);
		Display_GetDnD("t81Charge_D",replaceNulltoEmpty(SetAction_insert(content.charges_D)));

		Display_GetDnD("t69AdditionalFreeDays_M",content.additionalFreeDays_E);
		Display_GetDnD("t84Charge_M",replaceNulltoEmpty(SetAction_insert(content.charges_M)));

		Display_GetDnD("t68AdditionalFreeDays_I",content.additionalFreeDays_I);
		Display_GetDnD("t83Charge_I",replaceNulltoEmpty(SetAction_insert(content.charges_I)));

		Display_GetDnD("t67AdditionalFreeDays_E",content.additionalFreeDays_M);
		Display_GetDnD("t82Charge_E",replaceNulltoEmpty(SetAction_insert(content.charges_E)));

		setOrgData();
		//TrigGTA();
		ischeckGtaDND();
		manebtn(content);


	}

	function setOrgData_SavePreview(){
		var exportDetentionDetails = rptGetDataFromSingleArea("d62ExportDetention");
		var exportDemurrageDetails = rptGetDataFromSingleArea("d63ExportDemurrage");
		var importDetentionDetails = rptGetDataFromSingleArea("d64ImportDetention");
		var importDemurrageDetails = rptGetDataFromSingleArea("d65ImportDemurrage");

		exportDetentionDetails.includeFreeDaysInSlab = fngetcheck('d62includeFreeDaysInSlab');
		exportDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d63includeFreeDaysInSlab');
		importDetentionDetails.includeFreeDaysInSlab = fngetcheck('d64includeFreeDaysInSlab');
		importDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d65includeFreeDaysInSlab');

		exportDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d62groundRentIclDnDInvoice');
		exportDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d63groundRentIclDnDInvoice');
		importDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d64groundRentIclDnDInvoice');
		importDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d65groundRentIclDnDInvoice');



		OrgDataExportDetention_s = JSON.stringify(exportDetentionDetails);
		OrgDataExportDemurrage_s = JSON.stringify(exportDemurrageDetails);
		OrgImportDetention_s = JSON.stringify(importDetentionDetails);
		OrgImportDemurrage_s = JSON.stringify(importDemurrageDetails);

		OrgAdditionalFreeDays_D_s  = JSON.stringify(rptGetDataFromDisplayAll("t66AdditionalFreeDays_D"));
		OrgAdditionalFreeDays_E_s  = JSON.stringify(rptGetDataFromDisplayAll("t67AdditionalFreeDays_E"));
		OrgAdditionalFreeDays_I_s  = JSON.stringify(rptGetDataFromDisplayAll("t68AdditionalFreeDays_I"));
		OrgAdditionalFreeDays_M_s  = JSON.stringify(rptGetDataFromDisplayAll("t69AdditionalFreeDays_M"));

		OrgCharge_D_s  = JSON.stringify(rptGetDataFromDisplayAll("t81Charge_D"));
		OrgCharge_E_s  = JSON.stringify(rptGetDataFromDisplayAll("t82Charge_E"));
		OrgCharge_I_s = JSON.stringify(rptGetDataFromDisplayAll("t83Charge_I"));
		OrgCharge_M_s  = JSON.stringify(rptGetDataFromDisplayAll("t84Charge_M"));
	}
	
	function setOrgData(){
		var exportDetentionDetails = rptGetDataFromSingleArea("d62ExportDetention");
		var exportDemurrageDetails = rptGetDataFromSingleArea("d63ExportDemurrage");
		var importDetentionDetails = rptGetDataFromSingleArea("d64ImportDetention");
		var importDemurrageDetails = rptGetDataFromSingleArea("d65ImportDemurrage");

		exportDetentionDetails.includeFreeDaysInSlab = fngetcheck('d62includeFreeDaysInSlab');
		exportDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d63includeFreeDaysInSlab');
		importDetentionDetails.includeFreeDaysInSlab = fngetcheck('d64includeFreeDaysInSlab');
		importDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d65includeFreeDaysInSlab');

		exportDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d62groundRentIclDnDInvoice');
		exportDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d63groundRentIclDnDInvoice');
		importDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d64groundRentIclDnDInvoice');
		importDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d65groundRentIclDnDInvoice');



		OrgDataExportDetention = JSON.stringify(exportDetentionDetails);
		OrgDataExportDemurrage = JSON.stringify(exportDemurrageDetails);
		OrgImportDetention = JSON.stringify(importDetentionDetails);
		OrgImportDemurrage = JSON.stringify(importDemurrageDetails);

		OrgAdditionalFreeDays_D  = JSON.stringify(rptGetDataFromDisplayAll("t66AdditionalFreeDays_D"));
		OrgAdditionalFreeDays_E  = JSON.stringify(rptGetDataFromDisplayAll("t67AdditionalFreeDays_E"));
		OrgAdditionalFreeDays_I  = JSON.stringify(rptGetDataFromDisplayAll("t68AdditionalFreeDays_I"));
		OrgAdditionalFreeDays_M  = JSON.stringify(rptGetDataFromDisplayAll("t69AdditionalFreeDays_M"));

		OrgCharge_D  = JSON.stringify(rptGetDataFromDisplayAll("t81Charge_D"));
		OrgCharge_E  = JSON.stringify(rptGetDataFromDisplayAll("t82Charge_E"));
		OrgCharge_I  = JSON.stringify(rptGetDataFromDisplayAll("t83Charge_I"));
		OrgCharge_M  = JSON.stringify(rptGetDataFromDisplayAll("t84Charge_M"));

	}

	function setOrgData_GTA(IsData){
		GetOrgGTA = IsData;

	}

	function Setaction_I(IsData){
		var data = {}
		var exDt = IsData.exportDetentionDetails;
		var exDD = IsData.exportDemurrageDetails;
		var imDt = IsData.importDetentionDetails;
		var imDD = IsData.importDemurrageDetails
		exDt.action = "i";
		exDD.action = "i";
		imDt.action = "i";
		imDD.action = "i";

		data = {
				exportDetentionDetails : exDt,
				exportDemurrageDetails : exDD,
				importDetentionDetails : imDt,
				importDemurrageDetails : imDt
		}

		return data;
	}

	function Display_GetDnD(Area,Data){
		rptAddDataWithCache(Area,Data);
		rptDisplayTable(Area);
	}

	function getRouting_DNDTab(){
		return rptGetDataFromDisplayAll("t61RoutingHeader");
	}

	function GetDataDNDCheckAddcoridor(){
		var data = {};
		var exportDetentionDetails = rptGetDataFromSingleArea("d62ExportDetention");
		var exportDemurrageDetails = rptGetDataFromSingleArea("d63ExportDemurrage");
		var importDetentionDetails = rptGetDataFromSingleArea("d64ImportDetention");
		var importDemurrageDetails = rptGetDataFromSingleArea("d65ImportDemurrage");

		exportDetentionDetails.includeFreeDaysInSlab = fngetcheck('d62includeFreeDaysInSlab');
		exportDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d63includeFreeDaysInSlab');
		importDetentionDetails.includeFreeDaysInSlab = fngetcheck('d64includeFreeDaysInSlab');
		importDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d65includeFreeDaysInSlab');

		exportDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d62groundRentIclDnDInvoice');
		exportDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d63groundRentIclDnDInvoice');
		importDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d64groundRentIclDnDInvoice');
		importDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d65groundRentIclDnDInvoice');
		data = {
				exportDetentionDetails : exportDetentionDetails,
				exportDemurrageDetails : exportDemurrageDetails,
				importDetentionDetails : importDetentionDetails,
				importDemurrageDetails : importDemurrageDetails,
				charges_D : rptGetDataFromDisplayAll("t81Charge_D"),
				charges_E : rptGetDataFromDisplayAll("t82Charge_E"),
				charges_I : rptGetDataFromDisplayAll("t83Charge_I"),
				charges_M : rptGetDataFromDisplayAll("t84Charge_M"),
				ArrayadditionalFreeDays_D : rptGetDataFromDisplayAll("t66AdditionalFreeDays_D"),
				ArrayadditionalFreeDays_E : rptGetDataFromDisplayAll("t67AdditionalFreeDays_E"),
				ArrayadditionalFreeDays_I : rptGetDataFromDisplayAll("t68AdditionalFreeDays_I"),
				ArrayadditionalFreeDays_M : rptGetDataFromDisplayAll("t69AdditionalFreeDays_M")
		};

		return data;
	};

	function getDetentionDemurrageTabOutput(){
		var data = {};
		var ArrayadditionalFreeDays_D = [];
		var ArrayadditionalFreeDays_E = [];
		var ArrayadditionalFreeDays_I = [];
		var ArrayadditionalFreeDays_M = [];
		ArrayadditionalFreeDays_D = rptGetDataFromDisplayAll("t66AdditionalFreeDays_D");
		ArrayadditionalFreeDays_E = rptGetDataFromDisplayAll("t67AdditionalFreeDays_E");
		ArrayadditionalFreeDays_I = rptGetDataFromDisplayAll("t68AdditionalFreeDays_I");
		ArrayadditionalFreeDays_M = rptGetDataFromDisplayAll("t69AdditionalFreeDays_M");

		if(!isEmpty(newArrayAdditionalFreeDays_D)){
			for(var i = 0;i < newArrayAdditionalFreeDays_D.length; i++){
				if(!isEmpty(newArrayAdditionalFreeDays_D[i])){
					ArrayadditionalFreeDays_D.push(newArrayAdditionalFreeDays_D[i]);
				}
			}
		}

		if(!isEmpty(newArrayAdditionalFreeDays_E)){
			for(var i = 0;i < newArrayAdditionalFreeDays_E.length; i++){
				if(!isEmpty(newArrayAdditionalFreeDays_E[i])){
					ArrayadditionalFreeDays_E.push(newArrayAdditionalFreeDays_E[i]);
				}
			}
		}

		if(!isEmpty(newArrayAdditionalFreeDays_I)){
			for(var i = 0;i < newArrayAdditionalFreeDays_I.length; i++){
				if(!isEmpty(newArrayAdditionalFreeDays_I[i])){
					ArrayadditionalFreeDays_I.push(newArrayAdditionalFreeDays_I[i]);
				}
			}
		}

		if(!isEmpty(newArrayAdditionalFreeDays_M)){
			for(var i = 0;i < newArrayAdditionalFreeDays_M.length; i++){
				if(!isEmpty(newArrayAdditionalFreeDays_M[i])){
					ArrayadditionalFreeDays_M.push(newArrayAdditionalFreeDays_M[i]);
				}
			}
		}


		var exportDetentionDetails = rptGetDataFromSingleArea("d62ExportDetention");
		var exportDemurrageDetails = rptGetDataFromSingleArea("d63ExportDemurrage");
		var importDetentionDetails = rptGetDataFromSingleArea("d64ImportDetention");
		var importDemurrageDetails = rptGetDataFromSingleArea("d65ImportDemurrage");

		exportDetentionDetails.includeFreeDaysInSlab = fngetcheck('d62includeFreeDaysInSlab');
		exportDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d63includeFreeDaysInSlab');
		importDetentionDetails.includeFreeDaysInSlab = fngetcheck('d64includeFreeDaysInSlab');
		importDemurrageDetails.includeFreeDaysInSlab = fngetcheck('d65includeFreeDaysInSlab');

		exportDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d62groundRentIclDnDInvoice');
		exportDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d63groundRentIclDnDInvoice');
		importDetentionDetails.groundRentIclDnDInvoice = fngetcheck('d64groundRentIclDnDInvoice');
		importDemurrageDetails.groundRentIclDnDInvoice = fngetcheck('d65groundRentIclDnDInvoice');
		/*exportDetentionDetails.startDate = conCatDate(exportDetentionDetails.startDate);
		exportDetentionDetails.expiryDate = conCatDate(exportDetentionDetails.expiryDate);

		exportDemurrageDetails.startDate = conCatDate(exportDemurrageDetails.startDate);
		exportDemurrageDetails.expiryDate = conCatDate(exportDemurrageDetails.expiryDate);

		importDetentionDetails.startDate = conCatDate(importDetentionDetails.startDate);
		importDetentionDetails.expiryDate = conCatDate(importDetentionDetails.expiryDate);

		importDemurrageDetails.startDate = conCatDate(importDemurrageDetails.startDate);
		importDemurrageDetails.expiryDate = conCatDate(importDemurrageDetails.expiryDate);*/

		var exportDetentionDetails = CheckDataOnChange_ObjectDND(OrgDataExportDetention,exportDetentionDetails);
		var exportDemurrageDetails = CheckDataOnChange_Object(OrgDataExportDemurrage,exportDemurrageDetails);
		var importDetentionDetails = CheckDataOnChange_Object(OrgImportDetention,importDetentionDetails);
		var importDemurrageDetails = CheckDataOnChange_Object(OrgImportDemurrage,importDemurrageDetails);

		var additionalFreeDays_D = CheckDataOnChange_ObjectListNew(OrgAdditionalFreeDays_D,ArrayadditionalFreeDays_D,"freeDaysSeqNo");
		var additionalFreeDays_E = CheckDataOnChange_ObjectListNew(OrgAdditionalFreeDays_E,ArrayadditionalFreeDays_E,"freeDaysSeqNo");
		var additionalFreeDays_I = CheckDataOnChange_ObjectListNew(OrgAdditionalFreeDays_I,ArrayadditionalFreeDays_I,"freeDaysSeqNo");
		var additionalFreeDays_M = CheckDataOnChange_ObjectListNew(OrgAdditionalFreeDays_M,ArrayadditionalFreeDays_M,"freeDaysSeqNo");

		var charges_D = CheckDataOnChange_ObjectListNew(OrgCharge_D,rptGetDataFromDisplayAll("t81Charge_D"),"");
		var charges_E = CheckDataOnChange_ObjectListNew(OrgCharge_E,rptGetDataFromDisplayAll("t82Charge_E"),"");
		var charges_I = CheckDataOnChange_ObjectListNew(OrgCharge_I,rptGetDataFromDisplayAll("t83Charge_I"),"");
		var charges_M = CheckDataOnChange_ObjectListNew(OrgCharge_M,rptGetDataFromDisplayAll("t84Charge_M"),"");


		exportDetentionDetails.startDate = splitDate(exportDetentionDetails.startDate);
		exportDetentionDetails.expiryDate = splitDate(exportDetentionDetails.expiryDate);

		exportDemurrageDetails.startDate = splitDate(exportDemurrageDetails.startDate);
		exportDemurrageDetails.expiryDate = splitDate(exportDemurrageDetails.expiryDate);

		importDetentionDetails.startDate = splitDate(importDetentionDetails.startDate);
		importDetentionDetails.expiryDate = splitDate(importDetentionDetails.expiryDate);

		importDemurrageDetails.startDate = splitDate(importDemurrageDetails.startDate);
		importDemurrageDetails.expiryDate = splitDate(importDemurrageDetails.expiryDate);

		data = {
				"gtaFlag":fngetcheck('gtaFlag'),
				"isGetDnd":isGetDnd,
				exportDetentionDetails : exportDetentionDetails,
				additionalFreeDays_D : additionalFreeDays_D,
				charges_D : charges_D,
				exportDemurrageDetails : exportDemurrageDetails,
				additionalFreeDays_E : additionalFreeDays_E,
				charges_E : charges_E,
				importDetentionDetails : importDetentionDetails,
				additionalFreeDays_I : additionalFreeDays_I,
				charges_I : charges_I,
				importDemurrageDetails : importDemurrageDetails,
				additionalFreeDays_M : additionalFreeDays_M,
				charges_M : charges_M
		}
		return data;
	}

	 function CheckDataOnChange_ObjectDND(oldData,newDate){
	     var NewArraySetAction = {};
	     NewArraySetAction = newDate;
	       if(!isEmpty(oldData)){
		    	if(!jsonEqual(oldData, newDate)){
		    		console.log("You Want To Save Data On Change?");
		    		if(NewArraySetAction.action != 'i' && NewArraySetAction.action != 'd'){
		    			NewArraySetAction.action = "u";
		    		}
		    	}else{
		    		if(NewArraySetAction.action == 'u'){
		    			NewArraySetAction.action = "";
		    		}
		    	}
		    }

	       return NewArraySetAction;
	     }

	function ischeckfscDND(){
		var ExpDT = rptGetDataFromSingleArea("d62ExportDetention");
		var ExpDR = rptGetDataFromSingleArea("d63ExportDemurrage");
		var ImpDT = rptGetDataFromSingleArea("d64ImportDetention");
		var ImpDR = rptGetDataFromSingleArea("d65ImportDemurrage");
		$.each(ExpDT, function(k, v) {
			setCustomTagProperty("d62"+k, {dis:true});
		});
		$.each(ExpDR, function(k, v) {
			setCustomTagProperty("d63"+k, {dis:true});
		});
		$.each(ImpDT, function(k, v) {
			setCustomTagProperty("d64"+k, {dis:true});
		});
		$.each(ImpDR, function(k, v) {
			setCustomTagProperty("d65"+k, {dis:true});
		});

		var Addfeeday_D = rptGetDataFromDisplayAll("t66AdditionalFreeDays_D");
		var Addfeeday_E = rptGetDataFromDisplayAll("t67AdditionalFreeDays_E");
		var Addfeeday_I = rptGetDataFromDisplayAll("t68AdditionalFreeDays_I");
		var Addfeeday_M = rptGetDataFromDisplayAll("t69AdditionalFreeDays_M");

		for (var a = 0; a < Addfeeday_D.length; a++) {
			$("#t65selectDeleteLabel-"+a).remove();
			$.each(Addfeeday_D[a], function(k, v) {
				setCustomTagProperty("t66"+k+"-"+a, {dis:true});
			});
		}
		for (var b = 0; b < Addfeeday_E.length; b++) {
			$("#t67selectDeleteLabel-"+b).remove();
			$.each(Addfeeday_E[b], function(k, v) {
				setCustomTagProperty("t67"+k+"-"+b, {dis:true});
			});
		}

		for (var c = 0; c < Addfeeday_I.length; c++) {
			$("#t68selectDeleteLabel-"+c).remove();
			$.each(Addfeeday_I[c], function(k, v) {
				setCustomTagProperty("t68"+k+"-"+c, {dis:true});
			});
		}

		for (var d = 0; d < Addfeeday_M.length; d++) {
			$("#t69selectDeleteLabel-"+d).remove();
			$.each(Addfeeday_M[d], function(k, v) {
				setCustomTagProperty("t69"+k+"-"+d, {dis:true});
			});
		}
		var charges_D = rptGetDataFromDisplayAll("t81Charge_D");
		var charges_E = rptGetDataFromDisplayAll("t82Charge_E");
		var charges_I = rptGetDataFromDisplayAll("t83Charge_I");
		var charges_M = rptGetDataFromDisplayAll("t84Charge_M");
		for (var f = 0; f < charges_D.length; f++) {
			$.each(charges_D[f], function(k, v) {
				setCustomTagProperty("t81"+k+"-"+f, {dis:true});
			});
		}

		for (var g = 0; g < charges_E.length; g++) {
			$.each(charges_E[g], function(k, v) {
				setCustomTagProperty("t82"+k+"-"+g, {dis:true});
			});
		}

		for (var h = 0; h < charges_I.length; h++) {
			$.each(charges_I[h], function(k, v) {
				setCustomTagProperty("t83"+k+"-"+h, {dis:true});
			});
		}
		for (var j = 0; j < charges_M.length; j++) {
			$.each(charges_M[j], function(k, v) {
				setCustomTagProperty("t84"+k+"-"+j, {dis:true});
			});

		}

		$("#_t66addBtn").remove();
		$("#_t66deleteBtn").remove();
		$("#_t67addBtn").remove();
		$("#_t67deleteBtn").remove();
		$("#_t68addBtn").remove();
		$("#_t68deleteBtn").remove();
		$("#_t69addBtn").remove();
		$("#_t69deleteBtn").remove();
	}

	function ischeckfscroutingDND(){
		$("#_t61getDNDBtn").remove();
		/*$("#t61subjectGrcLable").remove();
		$("#t61GTALable").remove();
		$("#t61subjectGrcLable").remove();*/
		setCustomTagProperty("printDetDemFlag", {dis:true});
		setCustomTagProperty("gtaFlag", {dis:true});
		setCustomTagProperty("subjDNDFlag", {dis:true});
	}

	function getfromdisplayDND(){
		var ExpDT = rptGetDataFromSingleArea("d62ExportDetention");
		var ExpDR = rptGetDataFromSingleArea("d63ExportDemurrage");
		var ImpDT = rptGetDataFromSingleArea("d64ImportDetention");
		var ImpDR = rptGetDataFromSingleArea("d65ImportDemurrage");

		return (checkProperties(ExpDT)
				&checkProperties(ExpDR)
				&checkProperties(ImpDT)
				&checkProperties(ImpDR)
				)
	}

	function isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}

	function checkProperties(obj) {
	    for (var key in obj) {
	        if (obj[key] !== null && obj[key] != "")
	            return false;
	    }
	    return true;
	}

	function manebtn(content){
		var exportDetentionDetails = content.exportDetentionDetails;
		var exportDemurrageDetails = content.exportDemurrageDetails;
		var importDetentionDetails = content.importDetentionDetails;
		var importDemurrageDetails = content.importDemurrageDetails;
		if(!isEmptyObject(exportDetentionDetails)){
			$('#_t66addBtn').show();
			$('#_t66deleteBtn').show();
		}
		if(!isEmptyObject(exportDemurrageDetails)){
			$('#_t69addBtn').show();
			$('#_t69deleteBtn').show();
		}
		if(!isEmptyObject(importDetentionDetails)){
			$('#_t68addBtn').show();
			$('#_t68deleteBtn').show();
		}
		if(!isEmptyObject(importDemurrageDetails)){
			$('#_t67addBtn').show();
			$('#_t67deleteBtn').show();
		}

	}

	function clearBtn(){
		$("#_t66addBtn").hide();
		$("#_t66deleteBtn").hide();
		$("#_t67addBtn").hide();
		$("#_t67deleteBtn").hide();
		$("#_t68addBtn").hide();
		$("#_t68deleteBtn").hide();
		$("#_t69addBtn").hide();
		$("#_t69deleteBtn").hide();
	}



</script>