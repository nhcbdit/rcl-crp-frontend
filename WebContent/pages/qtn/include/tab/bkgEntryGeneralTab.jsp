<%@ taglib prefix = "rcl" uri = "/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<!-- General Header -->
<rcl:area id="d0-generalArea" collapsible="Y" title="General Header">
	<div class="row">

		<rcl:text id="d0-bookingNo" label="Booking#" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-on" label="On" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-by" label="By" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-at" label="At" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-blFSC" label="For B/L FSC" classes="div(col-md-2) ctr(dtlField)" check="req len(3) upc" lookup="tbl-getPOL" />

		<rcl:text id="d0-bkType" label="BK Type" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

	</div>

	<div class="row">

		<rcl:text id="d0-status" label="Status" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-opCode" label="Op Code" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-bookingParty" label="Booking Party" classes="div(col-md-2) ctr(dtlField)" check="req len(10) upc" lookup="tbl-getPOL" />

		<rcl:text id="d0-name" label="Name" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-quotationNo" label="Quotation#" classes="div(col-md-2) ctr(dtlField)" check="len(9) upc" lookup="tbl-getPOL" />

		<rcl:text id="d0-contractNo" label="Contract#" classes="div(col-md-2) ctr(dtlField)" check="len(15)" />

	</div>

	<div class="row">

		<rcl:text id="d0-contact" label="Contact" classes="div(col-md-2) ctr(dtlField)" check="len(25)" />

		<rcl:text id="d0-telNo" label="Tel#" classes="div(col-md-2) ctr(dtlField)" check="len(17)" />

		<rcl:text id="d0-faxNo" label="Fax#" classes="div(col-md-2) ctr(dtlField)" check="len(17)" />

		<rcl:text id="d0-email" label="E-Mail" classes="div(col-md-2) ctr(dtlField)" check="len(80)" />

		<rcl:text id="d0-initativeFSC" label="Initiative FSC" classes="div(col-md-2) ctr(dtlField)" check="len(3)" />

		<div class="col-md-2">
			<label>Cross Booking </label> 
			<input type="checkbox" id="d0-crossBooking" class="dtlField" />
		</div>

	</div>

	<div class="row">

		<rcl:text id="d0-refNo" label="Ref#" classes="div(col-md-2) ctr(dtlField)" check="len(17)" />

		<rcl:text id="d0-confirmedBy" label="Confirmed By" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:date id="d0-confirmedDate" label="On" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-finalForUC" label="Final Weight/Measurement for UC" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-poNumber" label="PO Number" classes="div(col-md-2) ctr(dtlField)" check="len(70)" />

	</div>

	<div class="row">

		<rcl:text id="d0-remarks" label="Ref#" classes="div(col-md-4) ctr(dtlField)" check="len(60)" icon="fa-plus" iconClick="popPrintRemark(hdfPrintRemark)" />

		<rcl:select id="d0-rateFor" 
					label="Rate For"
					classes="div(col-md-2) ctr(dtlField)"
					selectTable="CustomerType" />

		<rcl:text id="d0-slotAgreementNo" label="Slot Agreement#" classes="div(col-md-2) ctr(dtlField)" check="len(15) upc" lookup="tbl-getPOL" />

		<rcl:text id="d0-slotPartnerCode" label="Slot Partner Code" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d0-slotPartnerName" label="Slot Partner Name" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

	</div>

	<hr>

	<div class="row">
		<div class="col-md-12" style="text-align: right;">
			<button id="d0-popupAdvice" type="button" class="rcl-standard-button" onclick="rutOpenDialog('b1-AdivceArea')">
				Request Advice Details
			</button>
			<button id="d0-popupWeb" type="button" class="rcl-standard-button" onclick="rutOpenDialog('b2-WebBookingArea')">
				Web Booking Details
			</button>
			<button id="d0-popupApproval" type="button" class="rcl-standard-button" onclick="rutOpenDialog('Approval')">
				Approval Details
			</button>
		</div>
	</div>
</rcl:area>
<!-- End General Header -->


<!-- Shipment Type & Corridor Header -->
<rcl:area id="d1-shipmentArea" title="Shipment Type & Corridor Details" collapsible="Y">
	<div class="row">

		<rcl:text id="d1-term" label="Term" classes="div(col-md-1) ctr(dtlField)" check="req len(4) upc" lookup="tbl-getPOL" />

		<rcl:select id="d1-pm" 
					label="P/M" 
					classes="div(col-md-1) ctr(dtlField)" 
					check="req" 
					selectTable="ImperialMetric" />

		<rcl:select id="d1-shipmentType" 
					label="P/M" 
					classes="div(col-md-2) ctr(dtlField)" 
					check="req" 	
					selectTable="ShipmentType" />

		<rcl:number id="d1-grossContainerWeight" label="Gross Container Weight" classes="div(col-md-2) ctr(dtlField)" check="req dec(14,2)" />

		<rcl:number id="d1-measurement" label="Measurement" classes="div(col-md-2) ctr(dtlField)" check="dec(14,4)" />

		<rcl:number id="d1-teus" label="TEUS" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:number id="d1-pkgs" label="Pkgs" classes="div(col-md-1) ctr(dtlField)" check="min(0)" />

		<rcl:number id="d1-rorArea" label="ROR Area" classes="div(col-md-2) ctr(dtlField)" check="dec(14,4)" />

	</div>

	<div class="row">

		<rcl:text id="d1-porHaulLoc" label="POR Haul. Loc." classes="div(col-md-2) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="d1-por" label="POR" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d1-pol" label="POL" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d1-hitchmentPOL" label="Hitchment POL" classes="div(col-md-2) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="d1-pot1" label="POT1" classes="div(col-md-1) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="d1-pot2" label="POT2" classes="div(col-md-1) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="d1-pot3" label="POT3" classes="div(col-md-1) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="d1-pod" label="POD" classes="div(col-md-1) ctr(dtlField)" check="req len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="d1-del" label="DEL" classes="div(col-md-2) ctr(dtlField)" check="req len(5) upc" lookup="tbl-getPOL" />

	</div>

	<div class="row">

		<rcl:text id="d1-delHaulLoc" label="DEL" classes="div(col-md-2) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:date id="d1-cargoAvlDate" label="Cargo Avl. Date" classes="div(col-md-2) ctr(dtlField)" check="req" />

		<rcl:select id="d1-porHaul" 
					label="POR Haul."
					classes="div(col-md-1) ctr(dtlField)" 
					selectTable="Haulage" />

		<rcl:select id="d1-delHaul" 
					label="DEL Haul."
					classes="div(col-md-1) ctr(dtlField)" 
					selectTable="Haulage" />

		<rcl:text id="d1-optionalPOD" label="Optional POD" classes="div(col-md-2) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:date id="d1-delETA" label="DEL ETA" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:date id="d1-preferredETA" label="Preferred ETA" classes="div(col-md-2) ctr(dtlField)" />

	</div>

	<div class="row">

		<rcl:select id="d1-rateBasis" 
					label="Rate Basis"
					classes="div(col-md-2) ctr(dtlField)"
					selectTable="LcLRateBasis" />
		
		<rcl:text id="d1-returnPoint" label="Return Point" classes="div(col-md-2) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:date id="d1-returnPointETA" label="Return Point ETA" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d1-returnTerminal" label="Return Terminal" classes="div(col-md-2) ctr(dtlField)" check="len(5) upc" lookup="tbl-getPOL" />

	</div>
</rcl:area>
<!-- End Shipment Type & Corridor Details -->


<!-- Main Line Operator (MLO) Incoming Voyage detail -->
<rcl:area id="d2-mloArea" collapsible="Y" title="Main Line Operator (MLO) Incoming Voyage Details">
	<div class="row">

		<rcl:text id="d2-vessel" label="Vessel" classes="div(col-md-1) ctr(dtlField)" check="len(5)" />

		<rcl:text id="d2-voyage" label="Voyage" classes="div(col-md-1) ctr(dtlField)" check="len(10)" />

		<rcl:date id="d2-etaDate" label="ETA Date" classes="div(col-md-1) ctr(dtlField)" check="req" />

		<rcl:text id="d2-etaTime" label="ETA Time" classes="div(col-md-1) ctr(dtlField)" check="len(4)" />

		<rcl:date id="d2-dischargeEndDate" label="Discharge End Date" classes="div(col-md-1) ctr(dtlField)" check="req" />

		<rcl:text id="d2-dischargeEndTime" label="Discharge End Time" classes="div(col-md-1) ctr(dtlField)" check="len(4)" />

		<rcl:date id="d2-etdDate" label="ETD Date" classes="div(col-md-1) ctr(dtlField)" check="req" />

		<rcl:text id="d2-etdTime" label="ETD Time" classes="div(col-md-1) ctr(dtlField)" check="len(4)" />

	</div>
</rcl:area>
<!-- End Main Line Operator (MLO) Incoming Voyage detail -->


<!-- First Voyage detail -->
<rcl:area id="d3-voyageArea" collapsible="Y" title="First Voyage Details">
	<div class="row">

		<rcl:text id="d3-service" label="Service" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d3-vessel" label="Vessel" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d3-voyage" label="Voyage" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d3-direction" label="Direction" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d3-pol" label="POL" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d3-pod" label="POD" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:date id="d3-polETADate" label="POL ETA" classes="div(col-md-2 pr-0) ctr(dtlField)" options="disabled" />
		
		<rcl:text id="d3-polETATime" label="&nbsp;" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:date id="d3-polETDDate" label="POL ETD" classes="div(col-md-2 pr-0) ctr(dtlField)" options="disabled" />

		<rcl:text id="d3-polETDTime" label="&nbsp;" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

	</div>
</rcl:area>


<!-- Commodity Details Head -->
<div id="t0-commodityHeader" class="rcl-standard-widget-header" data-toggle="collapse" data-target="#t0-commodityArea">
	<span><i class="fas fa-chevron-right"></i></span> 
	<label>Commodity Details </label>
</div>

<!-- Commodity Table Area, Container  -->
<div id="t0-commodityArea" class="container-fluid row ascanList1 rcl-standard-search-header collapse show tblArea">
	<div id="t0-row" class="row tblRow border">

		<rcl:text id="t0-commodityCode" label="Commodity Code" name="CommodityCode" classes="div(col-md-2 pb-0 pl-1 pr-0) ctr(tblField) upc" check="req len(15)" lookup="tbl-getPOL" />

		<rcl:text id="t0-description" label="Description" name="Description" classes="div(col-md-2 pb-0 pl-1 pr-0) ctr(tblField)" options="disabled" />

		<rcl:text id="t0-commGrp" label="Comm. Grp" name="CommGrp" classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" check="req len(5)" />

		<rcl:number id="t0-pkgs" label="Pkgs" name="Pkgs" classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)" check="req dec(6,0) min(0)" />

		<rcl:select id="t0-ratetype" 
					label="Rate Type" 
					name="RateType" 
					classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)"
					selectTable="RateType" icon="fa-plus"
					iconClick="popRateType('#-RowId-#', 't0-ratetype')" />

		<rcl:text id="t0-kind" label="Kind" name="Kind" classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)" check="len(5) upc" lookup="tbl-getPOL" />

		<rcl:text id="t0-rorArea" label="ROR Area" name="RORArea" classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" check="dec(14,4)" lookup="tbl-getPOL" />

		<rcl:select id="t0-rortype" 
					label="ROR Type" 
					name="RORType"
					classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)"
					optionsList="1 2 3 4" 
					valueList="1 2 3 4" />

		<rcl:text id="t0-voidslots" label="Void Slots" name="VoidSlots" classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" options="disabled" />

		<div class="col-md-1 pt-3 pb-0 pl-1 pr-0">
			<div class="form-check form-check-inline">
				<label class="form-check-label"> 
					<input id="t0-rfFlag" name="RF" type="checkbox" class="form-check-input tblField">RF
				</label> 
				<i class="fa fa-plus"></i>
			</div>	
		</div>

		<rcl:text id="t0-containerweight" label="Gross Container Weight" name="GrossContainerWeight" classes="div(col-md-2 offset-md-2 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-measurement" label="Measurement" name="Measurement" classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-netweitht" label="Net Weight" name="NetWeight" classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-netmsmt" label="Net Msmt" name="NetMsmt" classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-length" label="Length" name="Length" classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-height" label="Height" name="Height" classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-width" label="Width" name="Width" classes="div(col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<rcl:text id="t0-diameter" label="Diameter" name="Diameter" classes="div(col-md-1 pb-0 pl-1 pr-0) ctr(tblField)" options="required" />

		<div class="col-md-1 pb-0 pl-1 pr-0">
			<a id="deleteBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2"
				style="font-size: 10px;" data-toggle="tooltip" data-placement="top"
				title="Removes current row from display and data"
				onclick='rptRemoveRow("t0-container", "#-RowId-#")'>Delete</a>
		</div>

	</div>

	<hr>
	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: -5; margin-bottom: -5px;">
			<button type="button" class="rcl-standard-button"
				onclick="newCommodity()">Add</button>
			<button type="button" class="rcl-standard-button" onclick="get()">Get</button>
		</div>
	</div>
</div>

<!-- End of table Container -->


<!-- Popup General Tab -->
<rcl:dialog id="b1-AdivceArea" width="1000" title="Request Advice" buttonList="" onClickList="">
	<rcl:area id="d4-AdivceArea" collapsible="Y" title="Request Advice">
		<div class="row">
	
			<rcl:text id="d4-specialCargo" label="Special Cargo" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d4-bbk" label="BBK" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d4-oog" label="OOG" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d4-uc" label="Direction" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
		</div>
	</rcl:area>
</rcl:dialog>

<rcl:dialog id="b2-WebBookingArea" width="1000" title="Request Advice" buttonList="" onClickList="">
	<rcl:area id="d5-WebBookingArea" collapsible="Y" title="Web Booking Details">
		<div class="row">
	
			<rcl:text id="d5-created" label="Created" classes="div(col-md-4) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d5-por" label="POR" classes="div(col-md-4) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d5-modified" label="Modified" classes="div(col-md-4) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d5-del" label="DEL" classes="div(col-md-4) ctr(dtlField)" options="disabled" />
			
			<rcl:text id="d5-webRefNo" label="Web Reference#" classes="div(col-md-4) ctr(dtlField)" options="disabled" />
	
		</div>
	</rcl:area>
</rcl:dialog>

<rcl:dialog id="b3-AdivceArea" width="1000" title="Request Advice" buttonList="" onClickList="">
	<rcl:area id="d4-AdivceArea" collapsible="Y" title="First Voyage Details">
		<div class="row">
	
			<rcl:text id="d4-specialCargo" label="Special Cargo" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d4-bbk" label="BBK" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d4-oog" label="OOG" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
			<rcl:text id="d4-uc" label="UC" classes="div(col-md-3) ctr(dtlField)" options="disabled" />
	
		</div>
	</rcl:area>
</rcl:dialog>




<!-- End Popup General Tab -->

<input id="hdfPrintRemark" type="hidden" />

<script type="text/javascript">

	let booking = { bookingNo : sessionStorage.getItem("viewBookingNo") };
	
	$(document).ready(function() {
		
		getResultAjax("WS_BKG_GET_ADVICE_DETAILS", booking).done(handleSetDataRequestAdvice);
		getResultAjax("WS_BKG_GET_WEB_BOOKING_DETAILS", booking).done(handleSetDataWebBooking);
		getResultAjax("WS_BKG_GET_APPROVAL_DETAILS", booking).done(handleSetDataApproval);
		
		function handleSetDataRequestAdvice(response){
			//rptSetSingleAreaValues("d4-AdivceArea", response)
		}
		
		function handleSetDataWebBooking(response){
			//rptSetSingleAreaValues("d4-AdivceArea", response)
		}
		
		function handleSetDataApproval(response){
			//rptSetSingleAreaValues("d4-AdivceArea", response)
		}
	});
			
	function getBookingList() {		
		
	}
	

	function process(target){
		
		var w;
		
		switch (target){
			case 'Advice' : w = window.open('<%=servURL%>/RrcGenericSrv?service=ui.bkg.entry.BookingMaintenanceSvc&pageAction=bkgEntryRequestAdvicePop',
								'_blank', 'width=1025,height=290,scrollbars=1');
							break;
		}
		
// 		w.onload = function() {

//  			 var data = w.document.getElementById("hdfbookingNO");  
//  		     data.value = bookingNo;
// 	  	};
		
		w.focus();
	}


	function popRateType(row, element){
		var obj = rptGetDataFromDisplay("t0-commodityArea", row)

		var wr;
		switch (obj.ratetype) {
			case 'D1': 
				wr = window.open('<%=servURL%>/RrcGenericSrv?service=ui.bkg.entry.BookingMaintenanceSvc&pageAction=bkgEntryDgPop',
					 '_blank', 'width=1200,height=100,scrollbars=0');
				break;
				
			case 'OG': 
				wr = window.open('<%=servURL%>/RrcGenericSrv?service=ui.bkg.entry.BookingMaintenanceSvc&pageAction=bkgEntryDgPop',
					 '_blank', 'width=1200,height=100,scrollbars=0');
				break;
			
			default:
				break;
		}
		
		
// 		console.log(row, rutGetElementValue(element));
<%-- 		var w = window.open('<%=servURL%>/RrcGenericSrv?service=ui.bkg.entry.BookingMaintenanceSvc&pageAction=bkgEntryDgPop', --%>
// 				'_blank', 'width=525,height=290,scrollbars=0');

// 		w.targetField = targetField;
		
// 		w.onload = function() {
// 		 var data = w.document.getElementById("d0-printRemarks");  
// 		 data.value = document.getElementById("hdfPrintRemark").value;
// 		};
		
// 		w.focus();
		
	}
			
	function newCommodity(){
		rptInsertNewRowBefore("t0-commodityArea", 'end');
	}
	
	function popPrintRemark(targetField){
	
		var w = window.open('<%=servURL%>/RrcGenericSrv?service=ui.bkg.entry.BookingMaintenanceSvc&pageAction=bkgEntryPrintRemarkPop',
							'_blank', 'width=525,height=290,scrollbars=0');
	
		w.targetField = targetField;
		
		w.onload = function() {
			 var data = w.document.getElementById("d0-printRemarks");  
		     data.value = document.getElementById("hdfPrintRemark").value;
	  	};
		
		w.focus();
	}

	function setResultPrintRemark(targetField, returnValue){
	    targetField.value = returnValue;
	    window.focus();
	}
		      
</script>