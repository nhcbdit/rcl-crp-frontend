<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<%-- Customers Header --%>
<rcl:area id="d70QuotationHeader" classes="div(container-fluid)" title="Quotation Header" collapsible='Y'>
	<div class="row">
		<rcl:text id="d70quotationNo" classes="div(col-md-2) ctr(dtlField)"
			label="Quotation#" options="disabled" check="upc" />
		<rcl:text id="d70ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" options="disabled" check="upc" />
		<%-- rcl:select id="d70status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/--%>
		<rcl:select id="d70status" label="Status"
					classes="div(col-md-2) ctr(dtlField)"
					options="data-rdo"
					optionsList="Open Need_Approval Quote_Again Close Final Hold Cancel"
					valueList="O N Q S C H L" />
		<rcl:text id="d70slm" classes="div(col-md-2) ctr(dtlField)"
			label="Slm#" options="disabled" check="upc" />
		<rcl:text id="d70qtdBy" classes="div(col-md-2) ctr(dtlField)"
			label="QtdBy" options="disabled" check="upc" />
		<rcl:text id="d70pos" classes="div(col-md-2) ctr(dtlField)"
			label="POS" options="disabled" check="upc" />
	</div>
</rcl:area>

<rcl:area id="t71RoutingHeader" classes="div(container-fluid)" title="Routing List" collapsible='Y' areaMode="table">
<div id="routhingScroll">
	<div id="t71row" class="row tblRow t71row">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t71row');" class="row">
				<div class="col-md-12 container-fluid">
					<div class="row">
						<rcl:text id="t71porHaulLoc" classes="div(col-md-2) ctr(dtlField)"	label="POR Haul. Loc." options="disabled" check="upc" />
						<rcl:text id="t71por" classes="div(col-md-1) ctr(dtlField)" label="POR" options="disabled"  check="upc" />
						<rcl:text id="t71pol" classes="div(col-md-1) ctr(dtlField)" options="disabled" label="POL"  />
						<rcl:text id="t71pot1" classes="div(col-md-1) ctr(dtlField)" label="POT1" options="disabled"  check="upc" />
						<rcl:text id="t71pot2" classes="div(col-md-1) ctr(dtlField)" label="POT2" options="disabled" check="upc" />
						<rcl:text id="t71pot3" classes="div(col-md-1) ctr(dtlField)" label="POT3" options="disabled" check="upc" />
						<rcl:text id="t71pod" classes="div(col-md-1) ctr(dtlField)" label="POD" options="disabled" />
						<rcl:text id="t71del" classes="div(col-md-1) ctr(dtlField)" label="DEL" options="disabled" check="upc" />
						<rcl:text id="t71delHaulLoc" classes="div(col-md-2) ctr(dtlField)" label="DEL Haul. Loc." options="disabled" check="upc" />
						<rcl:select id="t71socCoc"
										label="SOC/COC"
										classes="div(col-md-1) ctr(dtlField)"
										selectTable="SocCoc"
										options="data-dis"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</rcl:area>


<rcl:area id="d72QuotationPrint" classes="div(container-fluid)" title="Quotation Print" collapsible='Y'>
	<div class="row">

		<rcl:select id="d72Template" label="Print Template" classes="div(col-md-2) ctr(dtlField)" optionsList="Quotation Contract System_Default" valueList="Q C S" check="req" defaultValue="Q"/>

			<div class="col-md-2" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
				<label>Print Customer Function List</label>
				<input type="checkbox" id="d72printFunctionListFlag" class="dtlField" />
			</div>


			<div class="col-md-2" style="text-align: center; margin-top: 5px; padding-left: 2px; margin-bottom: 5px;">
				<label>Print Demurrage & Detention</label>
				<input type="checkbox" id="d72printDnDFlag" class="dtlField" />
			</div>


			<div class="col-md-2" style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
				<label>Print in Dual Language</label>
				<input type="checkbox" id="d72printDualLangFlag" class="dtlField" />
			</div>

		<rcl:text id="d72localLanguage" classes="div(col-md-2) ctr(dtlField)" label="Local Language"  check="upc" lookup="tbl-getPOL" />

		<div class="col-md-2"
			style="text-align: center; margin-top: 5px; margin-bottom: 5px; padding-top: 7px;">
			<button type="button" class="rcl-standard-button"
				onclick="printPreview()">Print Preview</button>
		</div>

	</div>

</rcl:area>



<rcl:area id="t73PrintClausesArea" classes="div(container-fluid)" title="Print Clauses" collapsible='Y' areaMode="table">
<%-- div id="PrintClausesAreaScroll" --%>
	<div id="t73row" class="row tblRow t73row" style="padding-bottom: 0px;">
		<div class="container-fluid" >
			<div onclick="onselectBG('#-RowId-#','t73row'); OnChangePrintClauses('#-RowId-#');" class="row"  style="padding-bottom: 0px;">

				<div class="col-md-3">
				<input id="t73printClauseSeqNo" type="number" class="tblField"  style="display: none;"/>
				<input id="t73clauseHeaderSeqNo" type="text" class="tblField"   style="display: none;"/>
				</div>
					<input id="t73selectclauseTitle"
           		   type="radio"
           		   name="select"
           		   onclick="setClauseDetais('#-RowId-#')"
           		   class="tblField" style="display: none;"/>

				<%-- div class="col-md-10">
				<label>Clause Titles</label>
					<div class="rcl-standard-input-wrapper">
					<div style="padding-right:20px">
					<input id="t73clauseTitle" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField dtlField" readonly onkeyup="upc();" data-ct="bt-text">
					</div>
						<i class="fa fa-search" onclick="rutOpenLookupTableWithReturnAction('VRL_QTN_PRINT_CLAUSE ','TEXT SEQ_NO','t73clauseTitle#-RowId-# t73clauseHeaderSeqNo#-RowId-#','','','','');"></i>
					</div>
				</div--%>

				<rcl:text id="t73clauseTitle" classes="div(col-md-10) ctr(dtlField)" label="Clause Titles" options="onkeyup='upc();'" check="len(255)"
				lookup="tbl(VRL_QTN_PRINT_CLAUSE)
						rco(TEXT SEQ_NO)
						rid(t73clauseTitle#-RowId-# t73clauseHeaderSeqNo#-RowId-#)"/>

				<%-- rcl:text id="t73clauseHeaderSeqNo" classes="div(col-md-10) ctr(dtlField)" label="Clause Titles"  check="upc"
				lookup="tbl(VRL_PRINT_CLAUSE)
						rco(TEXT SEQ_NO)
						rid(t73clauseHeaderSeqNo#-RowId-# t73printClauseSeqNo#-RowId-#)"/--%>



				<!-- div id="t73selectDeleteLabel" class="col-md-1 pl-1 pr-0 mb-2" style="text-align: center;">
					<label>Delete</label>
					<div>
						<input id="t73selectDelete" type="checkbox" class="tblField mt-2" />
					</div>
				</div-->

			</div>
		</div>
	</div>
<%--/div--%>

	<!-- div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t73addBtn" type="button" class="rcl-standard-button" onclick="newPrintClauses()">Add</button>
			<button id="_t73deleteBtn" type="button" class="rcl-standard-button" onclick="deletePrintClauses()">Delete</button>
		</div>
	</div-->
</rcl:area>

<rcl:area id="t74ClausesDetails" classes="div(container-fluid)" title="Clauses Details" collapsible='Y' areaMode="table">
	<div id="t74row" class="row tblRow" style="padding-left: 10px;">
		<input id="t74clauseHeaderSeqNo" type="text" class="tblField"  style="display:none;" />
		<input id="t74clauseDtlSeqNo" type="number" class="tblField"  style="display:none;" />
			<rcl:text id="t74SeqNo" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" label="seq#"   />
			<rcl:text id="t74clause" classes="div(col-md-11 pl-1 pr-0) ctr(dtlField)" label="Clause"   />
	</div>
	<%--div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t74addBtn" type="button" class="rcl-standard-button" onclick="newClausesDetails()">Add</button>
			<button id="_t74deleteBtn" type="button" class="rcl-standard-button" onclick="deleteClausesDetails()">Delete</button>
		</div>
	</div--%>
</rcl:area>


<rcl:area id="d75ClausesDetails" classes="div(container-fluid)" title="Quotation Comments" collapsible='Y' >
	<div class="row" style="margin-left: 0px;">

		<rcl:text id="d75PrintPreText" classes="div(col-md-11 pl-1 pr-0) ctr(dtlField)" label="Quotation Print Pre Text"   check="len(80)"/>

		<rcl:text id="d75PrintPostText" classes="div(col-md-11 pl-1 pr-0) ctr(dtlField)" label="Quotation Print Post Text"   check="len(80)"/>

	</div>
</rcl:area>

<rcl:area id="t76QuotationComments" classes="div(container-fluid)" title="Quotation Comments" collapsible='Y' areaMode="table">
	<div id="t76row" class="row tblRow ">
	<input id="t76commentSeqNo" type="number" class="tblField"  style="display:none;" />
		<rcl:text id="t76comments" classes="div(col-md-7 pl-1 pr-0) ctr(dtlField)" label="Comments"   check="len(80)"/>

		<div class="col-1x5 pl-1 pr-0 mb-2" style="text-align: center;">
			<label>Term & Condition</label>
			<div>
				<input id="t76termsConditions" type="checkbox" class="tblField mt-2" />
			</div>
		</div>
		<rcl:text id="t76createdUser" classes="div(col-1x25 pl-1 pr-0) ctr(dtlField)" options="disabled" check="len(10)"
			label="Created User"   />
		<rcl:date id="t76createdDate" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Created Date"  options="disabled" defaultValue="today"/>

		<div id="t76selectDeleteLable"class="col-md-1 pl-1 pr-0 mb-2" style="text-align: center;">
			<label>Delete</label>
			<div>
				<input id="t76selectDelete" type="checkbox" class="tblField mt-2" />
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t76addBtn" type="button" class="rcl-standard-button" onclick="newQuotationComments()">Add</button>
			<button id="_t76deleteBtn" type="button" class="rcl-standard-button" onclick="deleteQuotationComments()">Delete</button>
		</div>
	</div>
</rcl:area>

<script type="text/javascript">
var OrgPrintClausesArea  = [];
var OrgQuotationComments  = [];
var OrgqtnPrintPreText = "";
var OrgqtnPrintPostText = "";

var GetDataPrintClauses = [];
	$(document).ready(function() {
		rptTableInit("t71RoutingHeader", { disabledSortControl: true });
		rptTableInit("t73PrintClausesArea", { disabledSortControl: true });
		rptTableInit("t74ClausesDetails", { disabledSortControl: true });
		rptTableInit("t76QuotationComments", { disabledSortControl: true });
		//rptTableInit("t32commodityArea");
	});

	function fetchDataCommentAndPrintTab(content){
		rptAddDataWithCache("t73PrintClausesArea", SetAction_edit(content.printClause));
		rptDisplayTable("t73PrintClausesArea");
		rptAddDataWithCache("t76QuotationComments", SetAction_edit(content.comment));
		rptDisplayTable("t76QuotationComments");
		rutSetElementValue("d75PrintPreText", content.qtnPrintPreText);
		rutSetElementValue("d75PrintPostText", content.qtnPostPreText);
		checkRecodeScroll('t73PrintClausesArea','PrintClausesAreaScroll','PrintClausesArea-scroll',4);
		/*setRowView("t31containerArea","t31corridorSeq");
		setRowView("t32commodityArea","t32corridorSeq");*/

		OrgPrintClausesArea  = JSON.stringify(rptGetDataFromDisplayAll("t73PrintClausesArea"));
		OrgQuotationComments  = JSON.stringify(rptGetDataFromDisplayAll("t76QuotationComments"));

		OrgqtnPrintPreText = rutGetElementValue("d75PrintPreText");
		OrgqtnPrintPostText = rutGetElementValue("d75PrintPostText");
		fnGetDataPrintClauses(content.clausesDetail);
		//AddDetailPrinclauses(content.clausesDetail);
		OnChangePrintClauses("t73row-0");
		onselectBG('t73row-0','t73row');
	}

	function AddDetailPrinclauses(result){

		var detailPrintClause = getCurrentPtDataWithAction("t73PrintClausesArea", false);
		var newData = detailPrintClause;
		for (i = 0; i < result.length; i++) {
			if(result[i].clauseHeaderSeqNo != ""){
				for (x = 0; x < detailPrintClause.length; x++) {
					var datadetel = [];
					if(result[i].clauseHeaderSeqNo == detailPrintClause[x].clauseHeaderSeqNo){
						datadetel.push(result[i]);
						//newData[x].Detail = datadetel;
						//console.log(detailPrintClause[x].clauseHeaderSeqNo +" :: "+ JSON.stringify(result[i]));
					}
				}
				//console.log(result[i].clauseHeaderSeqNo + "AddDetailPrinclauses :: "+JSON.stringify(datadetel));
			}
		}


		//console.log("AddDetailPrinclauses :: "+JSON.stringify(newData));
	}

	function fnGetDataPrintClauses(data){
		GetDataPrintClauses = data;
	}

	function DisplayClausesDetails(data,seletor){
		var newArray = [];
		var newobjet = {};
		//console.log(seletor);
		for(var i = 0; i < data.length; i++){

			if(data[i].clauseHeaderSeqNo == seletor){

				newobjet = data[i];
				newArray.push(newobjet);
			}
		}

		var ClauseDetail = rptGetDataFromDisplayAll("t74ClausesDetails");
		if(! isEmpty(ClauseDetail)){
			rptClearDisplay("t74ClausesDetails");
		}
		rptAddDataWithCache("t74ClausesDetails",newArray);
		rptDisplayTable("t74ClausesDetails");
		setRowView("t74ClausesDetails","t74SeqNo");

		if(!Isview){
			ischeckfscClausDetail();
		}

	}
	var crrunt = "" ,percrrunt = "";
	function OnChangePrintClauses(rowid){
		percrrunt = crrunt;
		crrunt = rowid;
		var PrintClauses = rptGetDataFromDisplay("t73PrintClausesArea", rowid);
				//var Selector = rowid.substring(rowid.lastIndexOf('-') + 1);
				if(PrintClauses != undefined){
					DisplayClausesDetails(GetDataPrintClauses,PrintClauses.clauseHeaderSeqNo);

				}


	}

	function setPrintRowViewForNewRow(rowId, idNum, index, data){
		setRowView("t73PrintClausesArea","t73printClauseSeqNo");

	}

	function setCommentRowViewForNewRow(rowId, idNum, index, data){
		setRowView("t76QuotationComments","t76commentSeqNo");

	}

	function newClausesDetails() {
		rptInsertNewRowBefore("t74ClausesDetails", 'end');
		var newRow = $("#t74ClausesDetails").find(".tblRow").not(":last").last();
		var data = rptGetDataFromDisplay("t74ClausesDetails", newRow.attr("id"));
		data.action = 'i';
		rptForeachRow('t74ClausesDetails', setPrintRowViewForNewRow, false, ":last");
		setRowView("t74ClausesDetails","t74SeqNo");

		}


	function newPrintClauses() {
		rptInsertNewRowBefore("t73PrintClausesArea", 'end');
		var newRow = $("#t73PrintClausesArea").find(".tblRow").not(":last").last();
		var data = rptGetDataFromDisplay("t73PrintClausesArea", newRow.attr("id"));
		data.action = 'i';
		setRowView("t73PrintClausesArea","t73printClauseSeqNo");
		rptForeachRow('t73PrintClausesArea', setPrintRowViewForNewRow, false, ":last");

		}

	var newArrayPrintClauses = [];
	function deletePrintClauses(){
		//newArrayPrintClauses.push(HanderGetDataDeleteAll('t73PrintClausesArea', "selectDelete"));
		setarrayDeletAll('t73PrintClausesArea',newArrayPrintClauses);
		rptTryToDeleteSelectedRows('t73PrintClausesArea', "selectDelete", true);
		rptForeachRow('t73PrintClausesArea', setPrintRowViewForNewRow, false, ":last");
		setRowView("t73PrintClausesArea","t73printClauseSeqNo");
	}

	function newQuotationComments() {
		rptInsertNewRowBefore("t76QuotationComments", 'end');
		var newRow = $("#t76QuotationComments").find(".tblRow").not(":last").last();
		var data = rptGetDataFromDisplay("t76QuotationComments", newRow.attr("id"));
		data.action = 'i';
		var getrow = newRow.attr("id").split("-");
		setRowView("t76QuotationComments","t76commentSeqNo");
		rutSetElementValue("t76createdUser-"+getrow[1], userData.userId);
		rptForeachRow('t76QuotationComments', setCommentRowViewForNewRow, false, ":last");

		}

	var newArrayComments = [];
	function deleteQuotationComments(){
		//newArrayComments.push(HanderGetDataDeleteAll('t76QuotationComments', "selectDelete"));
		setarrayDeletAll('t76QuotationComments',newArrayComments);
		rptTryToDeleteSelectedRows('t76QuotationComments', "selectDelete", true);
		rptForeachRow('t76QuotationComments', setCommentRowViewForNewRow, false, ":last");
		setRowView("t76QuotationComments","t76commentSeqNo");
	}

	function getCommentPrintTabOutput(){
		var data = {};
		var ArrayPrintClause = [];
		var ArrayComment = [];
		ArrayPrintClause = rptGetDataFromDisplayAll("t73PrintClausesArea");
		ArrayComment = rptGetDataFromDisplayAll("t76QuotationComments");

		if(!isEmpty(newArrayPrintClauses)){
			for(var i = 0;i < newArrayPrintClauses.length; i++){
				if(!isEmpty(newArrayPrintClauses[i])){
					ArrayPrintClause.push(newArrayPrintClauses[i]);
				}
			}
		}

		if(!isEmpty(newArrayComments)){
			for(var i = 0;i < newArrayComments.length; i++){
				if(!isEmpty(newArrayComments[i])){
					ArrayComment.push(newArrayComments[i]);
				}
			}
		}

		/*for (var x = 0; x < ArrayComment.length; x++) {
			ArrayComment[x].createdDate = conCatDate(ArrayComment[x].createdDate);

		}*/
		ArrayComment = converntDateBeforeChkChangeData_ComAndPrint(ArrayComment);

		//console.log("ArrayPrintClause :: "+JSON.stringify(ArrayPrintClause));
		//console.log("ArrayComment :: "+Jc);

		var PrintClause = CheckDataOnChange_ObjectListNewtest(OrgPrintClausesArea,ArrayPrintClause,"printClauseSeqNo");
		var Comment = CheckDataOnChange_ObjectListNewtest(OrgQuotationComments,ArrayComment,"commentSeqNo");
		for (i = 0; i < PrintClause.length; i++) {
			delete PrintClause[i].printClauseHdrSeqNo;
			delete PrintClause[i].selectclauseTitle;
			delete PrintClause[i].Detail;
		}

		for (i = 0; i < Comment.length; i++) {
			Comment[i].createdDate = splitDate(Comment[i].createdDate);
		}

		data = {
				"qtnPrintPreText": rutGetElementValue("d75PrintPreText"),
				"qtnPrintPostText": rutGetElementValue("d75PrintPostText"),
				"printClause" : PrintClause,
				"comment" : Comment
		}
		return data;
	}

	function converntDateBeforeChkChangeData_ComAndPrint(Isdata){
	console.log("comment :: "+JSON.stringify(Isdata));
		for (var x = 0; x < Isdata.length; x++) {
			Isdata[x].createdDate = conCatDate(Isdata[x].createdDate);

		}
			return Isdata;
	}
	
	
	function checkPrePostPrint(){
		return (OrgqtnPrintPreText == rutGetElementValue("d75PrintPreText") 
				&& OrgqtnPrintPostText == rutGetElementValue("d75PrintPostText"));
	}

	function GetDataComAndPrintCheckAddcoridor(){

		var data = {};

		data = {
				PrintClause	: rptGetDataFromDisplayAll("t73PrintClausesArea"),
				Comment : converntDateBeforeChkChangeData_ComAndPrint(rptGetDataFromDisplayAll("t76QuotationComments")),
				qtnPrintPreText : rutGetElementValue("d75PrintPreText"),
				qtnPrintPostText : rutGetElementValue("d75PrintPostText")
		}

		return data;
	}


	function CheckDataOnChange_ObjectListNewtest(oldData,newDate,sortBy){
 		var NewArraySetAction = [];
 	    NewArraySetAction = newDate;
 	    
 	  NewArraySetAction.sort(function(a, b) {
 		 	if(sortBy == "commentSeqNo"){
  		  		var x = a.commentSeqNo, y = b.commentSeqNo;
 		 	}else if(sortBy == "printClauseSeqNo"){
  		  		var x = a.printClauseSeqNo, y = b.printClauseSeqNo;
 		 	}else if(sortBy == "sequenceNo"){
 				var x = a.sequenceNo, y = b.sequenceNo;
 		 	}else{
 				var x = a.rowId, y = b.rowId;
 		 	}
  		   return x < y ? -1 : x > y ? 1 : 0;
  	  });
 	 if(!isEmpty(oldData)){
 	    	oldData = JSON.parse(oldData);
 	    	if(NewArraySetAction.length == oldData.length){
 	    		if(!isEmpty(oldData) && !isEmpty(NewArraySetAction)){
		 	    	for(var b = 0; b < oldData.length; b++){ //Loop ORG DATA for get object.
		 	    		 if(!jsonEqual(JSON.stringify(oldData[b]), NewArraySetAction[b])){ // check Diff Json object.
		 	    			if(NewArraySetAction[b].action != 'i' && NewArraySetAction[b].action != 'd'){ //Add action u
		 	   			 		NewArraySetAction[b].action = "u";
		 		    	    }
		 	    		 }
		 	    	 }
 	    		}
 	    	}
 	  }

 	       return NewArraySetAction;
 }

	function setClauseDetais(row){
		var listClause = rptGetDataFromDisplay('t73PrintClausesArea', row).Detail || [];
		console.log("listClause :: "+JSON.stringify(listClause));
		rptClearDisplay("t74ClausesDetails");
		rptAddData("t74ClausesDetails", addIndexTable(listClause));
		rptDisplayTable("t74ClausesDetails");
		setRowView("t74ClausesDetails","t74SeqNo");
	}

	function setDisableisMandatory(){

		var allDisplay = rptGetDataFromDisplayAll("t73PrintClausesArea");

		allDisplay.forEach(function(data, index) {
			if(data.isMandatory == "Y"){
				$('#t73clauseTitle-' + index).attr('readonly', 'true');
				$('#t70row-' + index + ' i').hide();
				$('#' + data.rowId).find('[name="selectClause"]').attr('disabled', 'disabled');
			}
		});
	}

	function ischeckfscComAndPrint(){
		setCustomTagProperty("d75PrintPreText", {dis:true});
		setCustomTagProperty("d75PrintPostText", {dis:true});
		//removeGeneralSearchButton("d72localLanguage");
		// $.each(rptGetDataFromSingleArea("d72QuotationPrint"), function(k, v) {
		// 	setCustomTagProperty("d72"+k, {dis:true});
		// });
		var princlaus = rptGetDataFromDisplayAll("t73PrintClausesArea");
		for (var z = 0; z < princlaus.length; z++) {
			$("#t73selectDeleteLabel-"+z).remove();
			setCustomTagProperty("t73clauseHeaderSeqNo-"+z, {dis:true});
			removeGeneralSearchButton("t73clauseHeaderSeqNo-"+z);
		}
		ischeckfscClausDetail();
		if(!IsAppover){
			var comment = rptGetDataFromDisplayAll("t76QuotationComments");
				for (var r = 0; r < comment.length; r++) {
					$.each(comment[r], function(k, v) {
						setCustomTagProperty("t76"+k+"-"+r, {dis:true});
					});
					$("#t76selectDeleteLable-"+r).remove();
				}

				$("#_t76addBtn").remove();
				$("#_t76deleteBtn").remove();
		}

		$("#_t73addBtn").remove();
		$("#_t73deleteBtn").remove();

	}


	function ischeckfscClausDetail(){
		var clausDetail = rptGetDataFromDisplayAll("t74ClausesDetails");
				for (var h = 0; h < clausDetail.length; h++) {
					setCustomTagProperty("t74SeqNo-"+h, {dis:true});
					setCustomTagProperty("t74clause-"+h, {dis:true});

				}
	}

	function ClearCommentNPrint(){
		rptSetSingleAreaValues("d72QuotationPrint",{});

	}

	function printPreview() {
		
		if (fnIsChangeDataPrintPreview()) { // Not Adjust Data
			_printPreview();
		} else {
			dialogGeneric('Warning', 'Quotation needs to be saved before viewing the Quotation Print. Do you still want to proceed?', 'Yes', 'No')
			.then(function(yes) {
				if (yes) {
					save('preview');
				}else{
					return false;
				}
			});
		}
	}
	
	

	function _printPreview(){
		var report = "QTN001.rdf";
		var p_reference_no = "";
		var p_quotation_no = $('#d10quotationNo').val();
		var p_prnt_cust_fun_lst = fngetcheckfieldJquery($('#d72printFunctionListFlag').prop("checked"));
		var p_print_dem_det = fngetcheckfieldJquery($('#d72printDnDFlag').prop("checked"));
		var p_print_dual_lang = fngetcheckfieldJquery($('#d72printDualLangFlag').prop("checked"));
		var p_local_lang = $('#d72localLanguage').val();
		var p_clause_title = "Y";
		var p_user_code = userData.userId

		//p_quotation_no = "SIN054719";

		var url = "http://"+GetUrlReport.serverName+"/reports/rwservlet?repenvp&report="+report
				  +"&p_reference_no="+p_reference_no
				  +"&p_quotation_no="+p_quotation_no
				  +"&p_prnt_cust_fun_lst="+p_prnt_cust_fun_lst
				  +"&p_print_dem_det="+p_print_dem_det
				  +"&p_print_dual_lang="+p_print_dual_lang
				  +"&p_local_lang="+p_local_lang
				  +"&p_clause_title="+p_clause_title
				  +"&p_user_code="+p_user_code;


		console.log(url);
			/*"http://marlin-csreport.rclgroup.com/reports/rwservlet?repenvp&report=quotation_print.rep&p_reference_no=&p_quotation_no=SIN054704
					&p_prnt_cust_fun_lst=N
					&p_print_dem_det=N&p_print_dual_lang=N&p_local_lang=&p_clause_title=&p_user_code=DEV_TEAM";*/

		window.open(url,"print","width=1200,height=600,resizable=yes,scrollbars=yes,toolbar=yes,left=0,top=0");
	}
</script>