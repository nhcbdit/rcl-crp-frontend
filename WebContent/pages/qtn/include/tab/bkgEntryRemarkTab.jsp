<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<rcl:area id="d60remarkArea" collapsible="Y" title="Remarks">
	<div class="row">

		<rcl:text id="d60bookingNo" label="Booking#" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60bkType" label="BK Type" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60status" label="Status" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60por" label="POR" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60pol" label="POL" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60pod" label="POD" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60del" label="DEL" classes="div(col-md-1) ctr(dtlField)" options="disabled" />

		<rcl:text id="d60shipmentType" label="Shipment Type" classes="div(col-md-2) ctr(dtlField)" options="disabled" />

	</div>
</rcl:area>


	
<!-- Remarks Category Head -->
<div id="t60remarksCategoryHead" class="rcl-standard-widget-header tblHeader">
	<label>Remarks Category </label>
</div>
	
<div class="container-fluid border">
   <div class="row">
   
   	   <div class="col-md-3" style="font-weight:bold;text-align:center;">
           <label class="rcl-std">Select</label>
       </div>
       
       <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std">Category</label>
       </div>
       
       <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std">Send/Receive</label>
       </div>
       
       <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std" >Status</label>
       </div>
       
   </div>
</div>
	
<div id="t60remarksCategoryArea" class="container-fluid ascanList1 rcl-standard-search-header tblArea">
	<div id="t60row" class="row tblRow">
	
		<div class="col-md-3">
           	<input id="t61RemarksCategory.selectRemarkCategory" 
           		   type="radio" 
           		   name="selectRemarkCategory" 
           		   onclick="setRemark('#-RowId-#')"
           		   class="tblField" />
        </div>
			
		<rcl:text id="t60RemarksCategory.category" name="Category" classes="div(col-md-3) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t60RemarksCategory.sendReceive" name="Send/Receive" classes="div(col-md-3) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t60RemarksCategory.status" name="Status" classes="div(col-md-3) ctr(tblField)" options="readonly" />
				
	</div>
</div>
<!-- Remarks Category End -->


		
<!-- Remarks Received Head -->
<div id="t61remarksReceivedHead" class="rcl-standard-widget-header tblHeader">
	<label>Remarks Received </label>
</div>
	
<div class="container-fluid border">
   <div class="row">
   
   	   <div class="col-md-1" style="font-weight:bold;">
           <label class="rcl-std">Seq#</label>
       </div>
       
       <div class="col-md-5" style="font-weight:bold;">
           <label class="rcl-std">Remarks</label>
       </div>
       
       <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std">Terms & Conditions</label>
       </div>
       
       <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std" >Added By</label>
       </div>
       
       <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std" >Added Date</label>
       </div>
       
   </div>
</div>
	
<div id="t61remarksReceivedArea" class="container-fluid ascanList1 rcl-standard-search-header tblArea">
	<div id="t61row" class="row tblRow">
	
		<rcl:text id="t61seqNo" name="Seq" classes="div(col-md-1) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t61remarks" name="Remarks" classes="div(col-md-5) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t61termConditions" name="Terms & Conditions" classes="div(col-md-2) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t61addedBy" name="Added By" classes="div(col-md-2) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t61addedDate" name="Added Date" classes="div(col-md-2) ctr(tblField)" options="readonly" />
		
	</div>
</div>
<!-- Remarks Received End -->

		
<!-- Remarks Sent Head -->
<div id="t62remarksSentHead" class="rcl-standard-widget-header tblHeader">
	<label>Remarks Sent </label>
</div>
	
<div class="container-fluid border">
   <div class="row">
   
   	   <div class="col-md-1" style="font-weight:bold;text-align:center;">
           <label class="rcl-std">Seq#</label>
       </div>
       
       <div class="col-md-4" style="font-weight:bold;">
           <label class="rcl-std">Remarks</label>
       </div>
       
       <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std">Terms & Conditions</label>
       </div>
       
       <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std" >Added By</label>
       </div>
       
       <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std" >Added Date</label>
       </div>
       
       <div class="col-md-1" style="font-weight:bold;text-align:center;">
           <label class="rcl-std" >Delete</label>
       </div>
     
   </div>
</div>
	
<div id="t62remarksSentArea" class="container-fluid ascanList1 rcl-standard-search-header tblArea">
	<div id="t62row" class="row tblRow">
	
		<rcl:text id="t62seqNo" name="Seq" classes="div(col-md-1) ctr(tblField)" options="readonly" />
		
		<div class="col-md-4">
           <textarea id="t62remarks" 
           			 class="rcl-standard-rcl-standard-form-control tblField" 
           			 style="width:100%;height:100px">
           </textarea>		    
        </div>	
				
		<rcl:text id="t62termConditions" name="Terms & Conditions" classes="div(col-md-2) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t62addedBy" name="Added By" classes="div(col-md-2) ctr(tblField)" options="readonly" />
		
		<rcl:text id="t62addedDate" name="Added Date" classes="div(col-md-2) ctr(tblField)" options="readonly" />
	
		<div class="col-md-1">
           	<input id="t62RselectDelete" 
           		   type="checkbox" 
           		   name="selectRemarkCategory" 
           		   class="tblField" />
        </div>				
	</div>

   	<hr>
    <div class="row">
        <div class="col-md-12" style="text-align: right;">
            <button id="t62Add" type="button" class="rcl-standard-button" onclick="addRemarksSent()">Add</button>
            <button id="t62Delete" type="button" class="rcl-standard-button" onclick="delRemarksSent()">Delete</button>
        </div>
    </div>
</div>
<!-- Remarks Category End -->

<script type="text/javascript">

	function setRemark(row){
		rptClearDisplay("t61remarksReceivedArea");
		rptAddData("t61remarksReceivedArea", rptGetDataFromDisplay('t60remarksCategoryArea', row).RemarksReceived || []);
		rptDisplayTable("t61remarksReceivedArea");
		
		rptClearDisplay("t62remarksSentArea");
		rptAddData("t62remarksSentArea", rptGetDataFromDisplay('t60remarksCategoryArea', row).RemarksSent || []);
		rptDisplayTable("t62remarksSentArea");
	}
	
	function addRemarksSent(){
		rptInsertNewRowBefore("t62remarksSentArea", 'end');
	}
	
	function delRemarksSent(){	
		
	}
	
</script>
	
	
