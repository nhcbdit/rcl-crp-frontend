<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>
<style>
.bkdialog{
background-color: #fff;
}
</style>

<%-- Customers Header --%>
<rcl:area id="d10MaintenanceHeader" collapsible="Y" title="Quotation Header" >
	<div class="row">
		<input id="d10action" type="text" class="dtlField" style="display: none;" />
		<input id="d10subjRRFlag" type="text" class="dtlField" style="display: none;" />
		<input id="d10subjDNDFlag" type="text" class="dtlField" style="display: none;" />
		<input id="d10printDetDemFlag" type="text" class="dtlField" style="display: none;" />
		<input id="d10updateTime" type="text" class="dtlField" style="display: none;" />
		<rcl:text id="d10quotationNo" classes="div(col-md-2) ctr(dtlField)" check="len(9)"
			label="Quotation#" options="disabled" />
		<rcl:text id="d10slm" classes="div(col-md-2) ctr(dtlField)" check="len(5)"
			label="Slm#" options="disabled" />
		<rcl:text id="d10qtdBy" classes="div(col-md-2) ctr(dtlField)" check="len(15)"
			label="Qtd By" options="disabled" />
		<rcl:text id="d10pos" classes="div(col-md-2) ctr(dtlField)" check="len(3)"
			label="POS" options="disabled" />
		<rcl:select id="d10status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/>
		<%-- div class="col-md-2">
			<label>One-Off </label>
			<div>
				<input type="checkbox" id="d10oneOff" class="dtlField mt-2" />
			</div>
		</div--%>
		<div class="col-md-2">
			<label>One-Off </label> <input type="checkbox"
				id="d10oneOff" class="dtlField mt-2"/>
		</div>



	</div>
	<div class="row">
		<%-- rcl:text id="d10contactPartyCode" classes="div(col-md-2) ctr(dtlField)"
			label="Contact Party Code"  check="req upc len(10)"
				lookup="tbl(vrl_customers)
						rco(CODE NAME EMAIL TEL FAX SLM OPCODE)
						rid(d10contactPartyCode d10contactPartyName d10mail d10tel d10fax d10slm d10opCode)
						sco(CODE*min[3],main)
						sva(d10contactPartyCode)
						sop(LIKE)"

			autoLookup="sco(CODE)
						sva(d10contactPartyCode)"/--%>
		<div class=" col-md-2">
			<label for="d10contactPartyCode">Contact Party Code</label>
				<div class="rcl-standard-input-wrapper">
					<div style="padding-right:20px">
						<input id="d10contactPartyCode" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font dtlField dtlField"
						onblur="rutLookupByKey('vrl_customers','CODE NAME EMAIL TEL FAX SLM OPCODE','d10contactPartyCode d10contactPartyName d10mail d10tel d10fax d10slm d10opCode','CODE*min[3],main CODE','d10contactPartyCode d10contactPartyCode','LIKE','d10contactPartyCode');" required="true" maxlength="10" data-check="upc " data-ct="bt-text">
					</div>
					<i class="fa fa-search" onclick="qtn_OpenLookupTable('vrl_customers','CODE NAME EMAIL TEL FAX SLM OPCODE','d10contactPartyCode d10contactPartyName d10mail d10tel d10fax d10slm d10opCode','CODE*min[3],main','d10contactPartyCode','LIKE','','OR','NAME');"></i>
				</div>
		</div>

		<rcl:text id="d10contactPartyName" classes="div(col-md-2) ctr(dtlField)"
			label="Contact Party Name" check="req upc"  />
		<rcl:text id="d10ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" check="req upc len(17)"/>
		<rcl:text id="d10custContact" classes="div(col-md-2) ctr(dtlField)"
			label="Cust-Contact Ref#" check="req upc len(17)" />
		<rcl:text id="d10opCode" classes="div(col-md-2) ctr(dtlField)"
			label="Op. Code"  check="upc len(4)" />
		<rcl:select id="d10custType"
						label="Cust. Type"
						classes="div(col-md-2) ctr(dtlField)"
						selectTable="CustomerType"
						defaultValue="S"
						check="req"
						options="onchange=\"IsClearChangeAndDnd('custType',this.value);\""/>
	</div>
	<div class="row">

		<rcl:text id="d10inqDateTime" label="Inq Date Time" classes="div(col-md-2)"  options="disabled" />
		<%--input type="datetime-local" class="div(col-md-2)"  id="publishDate" /--%>
		<%--div class=" col-md-2">
			<label for="d10inqDateTime">Inq Date Time</label>
			<input id="d10inqDateTime" type="datetime-local" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font dtlField " disabled="" data-ct="bt-date">
		</div--%>
		<rcl:date id="d10effDate" label="Eff. Date" classes="div(col-md-2)" check="req" options="onchange=\"IsClearChangeAndDnd('effDate',this.value); ClearRouting();\"" defaultValue="today"/>
		<rcl:date id="d10expDate" label="Exp. Date" classes="div(col-md-2)" options="onchange=\"ckCurrentDate(this.value);validateExtenAndExpireButtion(this.value);IsClearChangeAndDnd('expDate',this.value); ClearRouting(); \"" check="req" />
		<rcl:date id="d10repDate" label="Rep. Date" classes="div(col-md-2)" options="onchange='ckrepDate(this.value)'" />
		<rcl:date id="d10follDate" label="Foll. Up Date" classes="div(col-md-2)" options="onchange='ckfollDate(this.value)'"/>
		<div class="col-md-2">
			<label>Revision </label> <input type="checkbox"
				id="d10revision" class="dtlField mt-2" />
		</div>

	</div>
	<div class="row">
		<rcl:text id="d10contact" classes="div(col-md-2) ctr(dtlField)"
			label="Contact"  check="upc len(25)" />
		<rcl:email id="d10mail" classes="div(col-md-2) ctr(dtlField)"
			label="E-Mail" check="upc len(80)" options="onchange='validateEmail(this.id,this.value)'"/>
		<rcl:tel id="d10tel" classes="div(col-md-2) ctr(dtlField)"
			label="Tel" check="upc len(17)"/>
		<rcl:tel id="d10fax" classes="div(col-md-2) ctr(dtlField)"
			label="Fax" check="upc len(17)" />
		<rcl:select id="d10commPref"
						label="Comm. Pref"
						classes="div(col-md-2) ctr(dtlField)"
						selectTable="Communication"
						defaultValue="E" />
		<rcl:text id="d10roeTable" classes="div(col-md-2) ctr(dtlField)"
			label="ROE Table"  check="upc len(20)"
			lookup="tbl(vrl_roe_table)
				    rco(CODE)
				    rid(d10roeTable)"
		   options="onchange=\"IsClearChangeAndDnd('roeTable',this.value);\""/>
	</div>
	<div class="row">
		<rcl:text id="d10remarks" classes="div(col-md-2) ctr(dtlField)"
			label="Remarks"  check="upc len(50)" />
		<div class="col-md-2">
			<label>Interim </label>
			<input type="checkbox" id="d10interim" class="dtlField mt-2" disabled/>
		</div>
		<rcl:text id="d10supersedeQuotation" classes="div(col-md-2) ctr(dtlField)"
			label="Supersede Quotation#" check="upc len(9)" options="disabled"/>

		<div class="col-md-6" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="d0-extendQuotation" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="extendQuotation();">Extend Quotation
			</button>
			<button id="d0-expireQuotation" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="exexpireQuotation()">Expire Quotation
			</button>
			<button id="d0-importFromExcel" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="">Import From Excel</button>

			<button id="d0-Attechment" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="attechment();">Attechment</button>

			<button id="d0-popupApproval" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="getAprrovalDetail();">Activity Details
			</button>
			<button id="d0-popupCreateInterim" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="OpenDialogCreateInterim();">Create Interim Quotation
			</button>

			<button id="d0-QuotationDetails" type="button" style="display:none;"
				class="rcl-standard-button"
				onclick="">Quotation Details</button>
		</div>
	</div>

	<div class="row appover" style="display:none;">
	<div class="col-md-6" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
	</div>
	<div class="col-md-2"  style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
		<button id="d0-QuoteAgain" type="button" style="display:none;"
						class="rcl-standard-button appover"
					onclick="qtn_Approval('Q')">Quote Again</button>
		<button id="d0-Appover" type="button" style="display:none;"
					class="rcl-standard-button appover"
					onclick="qtn_Approval('S')">Appove</button>
	</div>

	</div>
</rcl:area>

<rcl:area id="t10shipTypeDetailArea" classes="div(container-fluid ascanList1 rcl-standard-search-header collapse show tblArea)"
collapsible="Y"  title="Shipment Type & Corridor Details" areaMode="table">
<%--  div id="shipTypeDetailAreaScroll"--%>
	<div id="t10row" class="row tblRow t10row" style="padding-left: 10px; margin-right: 0px;">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t10row');" class="row">
				<div class="col-md-12 container-fluid">
					<div class="row">

						<input id="t10activeFlag" class="tblField pk" style="display:none;"  />
					   	<input id="t10corridorSeqNo" class="tblField pk" style="display:none;"  />
					   	<%--input id="t10SeqNo" class="tblField" style="display:none;"  /--%>
					   	<input id="t10shipDateOrg" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField hasDatepicker" value="" data-type="date" style="display:none;" />
						<%-- rcl:select id="t10shipmentType" label="Shipment Type1" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" selectTable="ShipmentType" check="req" options="onchange=\"selectShipmentType(null)\"" /--%>
						<rcl:stdShipmentType id="t10shipmentType" label="Shipment Type"
						classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" check="req" defaultValue="FCL" options="onchange=\"selectShipmentType(null)\"" />
						<rcl:number id="t10weight" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="Weight" check="dec(12,2)" defaultValue = "0.00"/>
						<rcl:number id="t10measurement" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="Measurement" check="dec(14,4)" defaultValue = "0.0000"/>
						<rcl:number id="t10teu" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="TEU's" check="dec(6,2)" options="readonly" />
						<rcl:number id="t10rorArea" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="ROR Area" check="dec(14,4)" defaultValue = "0.0000"/>
						<%-- rcl:text id="t10term" classes="div(col-md-1 pl-1 pr-0 ml-2)" label="Term" check="req" lookup="tbl-getBooking)rid-s0-bookingNo" /--%>
						<rcl:text id="t10term" label="Term"
							classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(dtlField)" check="req len(4)"  options="onkeyup='upc();' onchange='ClearRouting(); ClearChange();'"
							lookup="tbl(VRL_SHIPMENT_TERM)
								    rco(CODE)
								    rid(t10term#-RowId-#)"/>
						<rcl:select id="t10socCoc" label="SOC/COC" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(dtlField)" selectTable="SocCoc" check="req"
						 options="onchange='ClearChange(); MansetpolStatus(this.value);' "/>

						<rcl:select id="t10pm" label="P/M"
						classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(dtlField)" check="req"
						selectTable="ImperialMetric"
						options="onchange='ClearChange();'"/>
						<rcl:date id="t10shipDate" classes="div(col-1x25 pl-2 pr-0 ml-3) ctr(tblField)" label="Shpt. Date" check="req" defaultValue="today" options="onchange='ClearRouting(); ClearChange();'"/>
						<rcl:select id="t10rateBasis" label="Rate Basis" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" optionsList="POL-->POD" valueList="1" check="req" />

						<div id="t10selectDeleteLabel" class="col-0x5 pl-1 pr-0" style="text-align: center;">
							<label>Delete</label>
							<input id="t10selectDelete" type="checkbox" class="tblField mt-2" />
						</div>


			</div>

			<div class="row">
						<rcl:select id="t10porHaul" label="POR Haul." classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="req" selectTable="Haulage" defaultValue = "C"/>
						<rcl:text id="t10por" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="POR" check="req len(5)"
						lookup="tbl(VRL_POINT)
						  rco(CODE)
						  rid(t10por#-RowId-#)"
						  options="onblur='lookuponblurepor(this.id,this.value); isSetPorPol(this.id,this.value);' onkeyup='upc();'"/>

						<rcl:text id="t10pol" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="POL" check="req len(5)" lookup="tbl(VRL_PORT)
						  rco(CODE)
						  rid(t10pol#-RowId-#)"
						  options="onblur='isSetPorPol(this.id,this.value);' onkeyup='upc();' onchange='ClearRouting(); ClearChange();' "/>

						<rcl:text id="t10pot1" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="POT1"  check="len(5)" options="onkeyup='upc();'"
						lookup="tbl(VRL_PORT)
							rco(CODE)
							rid(t10pot1#-RowId-#)"/>

						<rcl:text id="t10pot2" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)"  label="POT2"  check="len(5)" options="onkeyup='upc();'"
						lookup="tbl(VRL_PORT)
							rco(CODE)
							rid(t10pot2#-RowId-#)"/>
						<rcl:text id="t10pot3" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="POT3"  check="len(5)" options="onkeyup='upc();'"
						lookup="tbl(VRL_PORT)
							rco(CODE)
							rid(t10pot3#-RowId-#)"/>
						<rcl:text id="t10pod" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="POD" check="req len(5)"
						lookup="tbl(VRL_PORT)
							rco(CODE)
							rid(t10pod#-RowId-#)"
							options="onblur='isSetPodDel(this.id,this.value);' onkeyup='upc();' onchange='ClearRouting(); ClearChange();' "/>
						<rcl:text id="t10del" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="DEL" check="req len(5)" lookup="tbl(VRL_POINT)
							rco(CODE)
							rid(t10del#-RowId-#)"
							options="onblur='lookuponbluredel(this.id,this.value); isSetPodDel(this.id,this.value);' onkeyup='upc();' "/>
						<rcl:select id="t10delHaul" label="DEL Haul." classes="div(col-1x25 pl-2 pr-0 ml-3) ctr(tblField)" check="req" selectTable="Haulage" defaultValue = "C"/>
						<div class="col-md-1 pl-1 pr-0 ml-3" style="text-align: center">
							<label>Known Haul. Loc. </label>
							<input type="checkbox" id="t10knowHalLoc" class="tblField mt-2"/>
						</div>



			</div>

			<div class="row">

				<rcl:text id="t10porHaulLoc" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" label="POR Haul. Loc."  options="onkeyup='upc();'"  check='len(5)'
				lookup="tbl(VRL_HAULAGE_LOCATION)
						  rco(CODE)
						  rid(t10porHaulLoc#-RowId-#)
						  sco(INLAND_POINT)
						  sva(t10por#-RowId-#)
						  sop(=)"/>

				<rcl:text id="t10delHaulLoc" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="DEL Haul. Loc."  options="onkeyup='upc();'" check='len(5)'
				lookup="tbl(VRL_HAULAGE_LOCATION)
							rco(CODE)
							rid(t10delHaulLoc#-RowId-#)
							sco(INLAND_POINT)
							sva(t10del#-RowId-#)
							sop(=)"/>
				<rcl:text id="t10supplier" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)"  label="Supplier" options="onkeyup='upc();'" check='len(10)'
				lookup="tbl(VRL_QTN_SUPPLIER)
						rco(CUSTOMER_CODE)
						rid(t10supplier#-RowId-#)
						sco(CUSTOMER_CODE*min[3])
						sva(t10supplier#-RowId-#)
						sop(LIKE)" />


				<rcl:text id="t10supplierLoc" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="Supplier Loc." check="len(10)" options="onkeyup='upc();'"
							lookup="tbl(vrl_point)
							rco(CODE)
							rid(t10supplierLoc#-RowId-#)
							sco(CODE*main)
							sva(t10supplierLoc#-RowId-#)
							sop(LIKE)"/>

				<rcl:text id="t10pickupDepot" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="Pickup Depot"  options="onkeyup='upc();'" check="len(5)"
				lookup="tbl(VRL_QTN_TERMINAL_DEPOT)
									rco(CODE)
									rid(t10pickupDepot#-RowId-#)
									sco(PORT_POINT)
									sva(t10por#-RowId-#)
									sop(=)"/>

				<rcl:text id="t10dropOffDepot" label="Drop-Off Depot" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(dtlField)"  options="onkeyup='upc();'" check="len(5)"
				lookup="tbl(VRL_QTN_TERMINAL_DEPOT)
									rco(CODE)
									rid(t10dropOffDepot#-RowId-#)
									sco(PORT_POINT)
									sva(t10del#-RowId-#)
									sop(=)"/>

				<rcl:select id="t10inqType" classes="div(col-md-1 pl-1 pr-0 ml-2) ctr(tblField)" label="Inq. Type" optionsList="BBK OOG DG" valueList="B O D" />
				<div class="col-1x25 pl-1 pr-2 ml-2">
					<div class="cssinline">
					   <div class=" pl-0 pr-1 ml-0">
							<label for="t10oogBkkInq">OOG/BBK Inq.#</label>
								<div class="rcl-standard-input-wrapper">
									<div style="padding-right:20px">
										<input id="t10oogBkkInq" type="text" maxlength="17" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField tblField" data-ct="bt-text">
									</div>
									<i class="fa fa-search" onclick="lookupoogBkk('#-RowId-#')"></i>
								</div>
						</div>

					    <div style="padding-top: 14px; font-size: 16px;">
					    	<a id="t10coppy" onclick="copyOOG('#-RowId-#')" class="pointer" title="Copy"><em class="fas fa-copy" ></em></a>
					    </div>
					</div>
				</div>
				<rcl:select id="t10facApplicable" label="FAC Applicable" classes="div(col-1x25 pl-0 pr-2) ctr(tblField)" defaultValue="N" optionsList="Yes No" check="req"  valueList="Y N"/>
				<div class="col-md-1 pl-1 pr-0 ml-2" style="text-align: center;">
					<label>Counter Proposal</label>
					<input type="checkbox" id="t10counterProposal" class="tblField mt-2"/>
				</div>
			</div>

				</div>
			</div>
		</div>
	</div>
<%--  /div--%>


	<hr style="margin-top: 0px">
	<div class="row">
		<div class="col-md-6" style="text-align: right; margin-top: 5px; margin-bottom: 5px;"></div>
		<div class="col-md-2"  style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="QuoteAgain" type="button" style="display:none;"
							class="rcl-standard-button appovercorridor"
						onclick="qtn_Approval()">Quote Again</button>
			<button id="Appover" type="button" style="display:none;"
						class="rcl-standard-button appovercorridor"
						onclick="qtn_Approval()">Appove</button>
		</div>
		<div class="col-md-4" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t10addBtn" type="button" class="rcl-standard-button" onclick="addCoridor()">Add</button>
			<button id="_t10deleteBtn" type="button" class="rcl-standard-button" onclick="deleteCoridor()">Delete</button></div>
	</div>
</rcl:area>
<%-- End of table customers list area --%>


<rcl:dialog id="b4-createInterimArea" width="250"
	title="Create Interim Quotation" buttonList="OK" onClickList="createInterim()">

	<rcl:area id="d90createInterimmArea" collapsible="N" useHeader="N" >
	<div class="row">
		<div class="cssinline">
			<div class="col-md-6">
			<label>Effective Date</label>
			</div>
			<div class="col-md-6 pl-1">
				<input id="d90effDate" type="date" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font dtlField "  data-ct="bt-date">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="cssinline">
			<div class="col-md-6">
			<label>Expipry Date</label>
			</div>
			<div class="col-md-6 pl-1">
				<input id="d90expDate" type="date" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font dtlField "  data-ct="bt-date">
			</div>
		</div>
	</div>
	 </rcl:area>

</rcl:dialog>

<rcl:dialog id="b5-CopyOogArea" width="400"
	title="Copy OOG/BBK Inp." buttonList="OK Cancel" onClickList="copyoogcon('Y'); copyoogcon('N'); ">

	<rcl:area id="d91copyOogbbkArea" collapsible="N" useHeader="N" classes="div(bkdialog)">
	<div class="row">
	<div class="col-md-2">
	<img border="0" src="../../../../images/qq.jpg" width="50" height="50">
	</div>
		<div class="col-md-10">
			<label>Do you want to Overwrite or Append to existing Size/Type recodrd? <br>
			(OK = Overwrite, Cancel = Append)
			</label>
		</div>
	</div>

	 </rcl:area>

</rcl:dialog>

<rcl:dialog id="b3-AprrovalArea" width="1000"
	title="Quotation Activity Details" buttonList="" onClickList="">

	<rcl:area id="t90approvaldetailArea" classes="div(container-fluid)"
		useHeader="N"  areaMode="table">

		<div class="border-bottom"
			style="margin-left: 0px; margin-right: 0px;">
			<div class="row pl-3 pr-2">
				<div class="col-0x5 p-0 " style="font-weight: bold;">
					<label class="rcl-std">Seq</label>
				</div>
				<div class="col-md-4 p-0"
					style="font-weight: bold;">
					<label class="rcl-std">Activity Details</label>
				</div>
				<div class="col-md-2 p-0"
					style="font-weight: bold;">
					<label class="rcl-std">By</label>
				</div>
				<div class="col-md-3 p-0"
					style="font-weight: bold;">
					<label class="rcl-std">Remarks</label>
				</div>
				<div class="col-md-2 p-0 pr-2"
					style="font-weight: bold;">
					<label class="rcl-std">Date Time</label>
				</div>
			</div>
		</div>


		<%-- div class="row pt-2 pl-3 pr-2 border-bottom tblArea"
			style="margin-left: 0px; margin-right: 0px;" --%>

			<div id="t90row" class="tblRow  row pt-2 pl-3 pr-2 border-bottom">

				<div class="container-fluid" >

					<div class="row gridRow">

					<rcl:text id="t90Seq"
					classes="div(col-0x5 pl-0 pr-1) ctr(dtlField)" options="disabled" />

					<rcl:text id="t90activityDetails"
					classes="div(col-md-4 pl-1) ctr(dtlField)" options="disabled" />

					<rcl:text id="t90by" classes="div(col-md-2 pl-0) ctr(dtlField)"
					options="disabled" />

					<rcl:text id="t90remark" classes="div(col-md-3 pl-0) ctr(dtlField)"
					options="disabled" />

					<rcl:text id="t90dateTime" classes="div(col-md-2 pl-0 pr-0) ctr(dtlField)"
					options="disabled" />
					</div>

				</div>

			</div>

		<%--  /div--%>


	 </rcl:area>


</rcl:dialog>


<rcl:dialog id="b6-ExtenQuotationArea" width="300"
	title="Extend Quotation" buttonList="OK" onClickList="extenQuotation()">

	<rcl:area id="d92extenQuotationArea" collapsible="N" useHeader="N" >
	<div class="row">
		<div class="cssinline">
			<div class="col-md-6">
			<label>New Expipry Date</label>
			</div>
			<div class="col-md-6 pl-1">
				<input id="d92expipryDate" type="date" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font dtlField "  data-ct="bt-date">
			</div>
		</div>
	</div>
	</rcl:area>

</rcl:dialog>


<rcl:dialog id="b7-confirmDeclinearea" width="450"
	title="confirmDecline" buttonList="OK" onClickList="qtn_Approval('Q','','','b7-confirmDeclinearea','Decline')">

	<rcl:area id="d93confirmDeclineArea" collapsible="N" useHeader="N" classes="div(bkdialog)">
	<div class="row">
	<div class="col-md-2">
	<img border="0" src="../../../../images/qq.jpg" width="50" height="50">
	</div>
		<div class="col-md-10">
			<label id="d93confirmDeclinelbl"></label>
		</div>
	</div>

	 </rcl:area>

</rcl:dialog>

<rcl:dialog id="b8-finalarea" width="450"
	title="final" buttonList="OK" onClickList="qtn_Approval('S','','','b8-finalarea','final')">

	<rcl:area id="d94finalareaArea" collapsible="N" useHeader="N" classes="div(bkdialog)">
	<div class="row">
	<div class="col-md-2">
	<img border="0" src="../../../../images/qq.jpg" width="50" height="50">
	</div>
		<div class="col-md-10">
			<label>This operation will close and finalize the quotation.<br>No Changes will be allowed on the quotation after this. <br> Are you sure to proceed on Finalize?
			</label>
		</div>
	</div>

	 </rcl:area>

</rcl:dialog>


<script type="text/javascript">
	var eqCorridorPk;
	var prevCorridor;
	var currectCorridor;
	var OrgDataMaintenanceHeader  = {};
	var OrgDataObjCorridor = [];
	var deleting = {};
	var deletingList = {};
	var getactiveFlag = "";
	var GetperCorridor = "";
	var getshipmentCorridor = {};


	var oldHdr;

	function initMaintenanceTab(){
		initViewMaintenanceTabByMenu();
	}
	function initViewMaintenanceTabByMenu() {
		rptTableInit("t10shipTypeDetailArea", { disabledSortControl: true });
		//rptTableInit("t90approvaldetailArea");
	}



	$(document).ready(function() {

		rptTableInit("t90approvaldetailArea", { disabledSortControl: true });
		serverDate = getCurrentServerDate();
		//var perCorridor = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
		//rptTableInit("t90approvaldetailArea");
		eqCorridorPk = 0;
		prevCorridor = undefined;
		$('a[data-toggle="tab"]').on('shown.bs.tab', function(event){
			var activeTab = $(event.target).text();         // active tab
		    var previousTab = $(event.relatedTarget).text();  // previous tab
		    var headerArea = rptGetDataFromSingleArea("d10MaintenanceHeader");
		    var containerArea = rptGetDataFromDisplayAll("t10shipTypeDetailArea");

		    var tab = ["Maintenance","Customer","Container/Commodity","Routing","Charges","Det. & Demure.","Comment & Print"];
		    console.log("previousTab :: "+previousTab)
		    if(previousTab == "Maintenance"){
		    	var getcorridor = getMaintenanceDelete();
		    	 if(getcorridor.length > 0){
		    		 var u = 0;
		    		 for (t = 0; t < getcorridor.length; t++) {
		    			 if(getcorridor[t].action == 'd'){
		    				 u ++;
		    			 }
		    		 }
		    		 if(u > 0){
		    			 dialogGenericv3("Maintenance","Warning", "Changes deleted, save quotation now?" , "Ok", "No").then(
		    						function(overwrite){
		    							if(overwrite){
		    								save();
		    							}else{
		    								window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
		    							}
		    						});

		    		 }
		    	 }
		    }

		   if(containerArea.length > 0){
			   fetchNewRoutingTab(headerArea,containerArea);
		    		if(checkBeforeInsertCoridorvalidate("checkdiffcuridor",previousTab)){
		    			for (i = 0; i < tab.length; i++) {

							    	 if(activeTab == tab[i]){
							    		 if(!IsFsc || IsAppover){
							    			 IsDisplayview();
											}

							    		getresponseshipmentCorridor("",containerArea);

							    		if(activeTab == "Det. & Demure."){

							    			//TrigGTA();
							    			curContainer = rptGetDataFromDisplayAll("t31containerArea");
							    			/*if(!isEmpty(perContainer)){
							    				perContainer = JSON.parse(perContainer);
							    				if(perContainer.length == curContainer.length){
							    					if(isChangeContainer(perContainer,curContainer)){
							    						console.log("have change size type");
							    						showdialogIschange();
							    					}
							    				}else{
							    					console.log("size > currant");
							    					showdialogIschange();
							    				}
							    			}
							    			/*getfromdisplayDND();
							    			if*/
							    			/*console.log("dnd" +getfromdisplayDND());
							    			if(getfromdisplayDND() > 0){
								    			if(isChangeCorridorUsingDND()){
								    				console.log("have change corridor")
								    				showdialogIschange();
								    			}
							    			}*/

							    			perContainer = JSON.stringify(curContainer);

							    			function showdialogIschange(){
							    				dialogGeneric("Warning", "DND details is invalid due to the Container for Size, Type or Rate Type field has been changed. Please retrieve DND Details again." , "Ok");
							    				//ClearDisplayDNDOnchange();
							    				IsClearChangeAndDnd();

							    			}


							    		}




							    	}
							    }
	    				}
		    		if(activeTab == "Container/Commodity"){
		    			var GetcontainerArea = rptGetDataFromDisplayAll("t31containerArea");
		    			if(GetcontainerArea.length > 0){
			    			for(var z = 0; z < GetcontainerArea.length; z++){
			    				ondisplayOOg(z,GetcontainerArea[z].isCopyOog);
					    		if(z == isSelect){
					    			//SetCntrAndCommTab(containerArea[z].shipmentType);
					    			//SetShipmentTypeOnchangeMan(containerArea[z].shipmentType);
				    			}
				    		}
		    			}

			    	}

		    		if(activeTab == "Charges"){
		    			check_shipmentType(containerArea);

		    			Ischksocandcoc();
		    		}

		    		if(activeTab == "Routing"){
		    			/*if(isChangeCorridor()){ //have change corridor
		    				var objRountingDetail = rptGetDataFromDisplayAll("t40rountingDetailArea");
		    				if(!isEmpty(objRountingDetail)){
			    				dialogGeneric("Warning", "The routing is invalid due to the routing field has been changed. Please retrieve routing again for corridor." , "Ok");
			    				rptClearDisplay("t40rountingDetailArea");
			    				clearfiled();
		    				}

		    			}*/


		    		}
		    	}else{
		    		dialogGenericv3("Maintenance","Warning", "Please add at least one corridor detail." , "Ok");
		    	}

		});

		//alert("bookingNo:"+bookingNo);
		//alert("oneOffFlag:"+oneOffFlag);

	//	var dgobj =
	//	{
	//			oneOff : oneOffFlag
	//     };
		//alert("oneOFf :"+dgobj.oneOff);
	//	rptSetSingleAreaValues("d10MaintenanceHeader", dgobj);
		var obj = [{

				por : "THBKK",
				del : "SGSIN",
				shipmentType : "FCL",
				pm : "M",
				socCoc : "C",
				action : "i"
		}];
	//	alert("por :"+obj.por);
		//rptTableInit("t10shipTypeDetailArea");
	//	addCoridor();
		//rptSetSingleAreaValues("t10shipTypeDetailArea", obj);

	//	rptAddData("t10shipTypeDetailArea", obj);
		//rptDisplayTable("t10shipTypeDetailArea");



	//	var newRow = $("#t10shipTypeDetailArea").find(".tblRow").not(":last").last();
	//	newRow.find("input.pk").val("#" + eqCorridorPk);

	//	var data = rptGetDataFromDisplay("t10shipTypeDetailArea", newRow.attr("id"));

	//	data.action = 'i';



	//	eqCorridorPk++;

	$('.hasDatepicker').change(function(){
		validatedateFormate(this.id,this.value);

	});

	});

	function extendQuotation(){
		rutSetElementValue("d92expipryDate", $('#d10expDate').val());
		rutOpenDialog('b6-ExtenQuotationArea');
	}

	function IsSetHeaderNew(){
			rutSetElementValue("d10qtdBy", userData.userId);
			rutSetElementValue("d10pos", userData.fscCode);
			//rutSetElementValue("d10inqDateTime", qtnGetCurrentDateTime());
			rutSetElementValue("d10inqDateTime", calcTime('Singapore', '+8'));
			console.log(calcTime('Singapore', '+8'));
			rutSetElementValue("d10repDate", addDays(new Date(), 7));
			rutSetElementValue("d10follDate", addDays(new Date(), 9));
			rutSetElementValue("d10expDate", addDays(new Date(), 30));
	}

	function ckCurrentDate(Isdate){
		var errorMsg = "";
		var currDate = new Date(convertDatevalidate(serverDate));
		var param = new Date(convertDatevalidate(Isdate));
		var effDate = new Date(convertDatevalidate($("#d10effDate").val()));

		if(currDate > param){
			dialogGeneric("Warning", "Expiry Date should be greater or equal to System Date." , "Ok");
		}
		if((effDate > param || effDate.toDateString() === param.toDateString())){
			dialogGeneric("Warning", "Expiry Date should be greater than Effective Date." , "Ok");
		}
	}

	function ckfollDate(isdate){
		var follDate = new Date(convertDatevalidate(isdate));
		var expDate = new Date(convertDatevalidate($("#d10expDate").val()));

		if((follDate > expDate || follDate.toDateString() === expDate.toDateString())){
			dialogGeneric("Warning", "Follow up Date should be less than Expiry date*Reply date should be less than Expiry date" , "Ok");
		}
	}

	function ckrepDate(isdate){
		var repDate = new Date(convertDatevalidate(isdate));
		var effDate = new Date(convertDatevalidate($("#d10effDate").val()));
		var expDate = new Date(convertDatevalidate($("#d10expDate").val()));
		if((repDate < effDate || repDate.toDateString() === effDate.toDateString())){
			dialogGeneric("Warning", "Reply date should be greater than Effective date." , "Ok");
		}

		if((repDate > expDate || repDate.toDateString() === expDate.toDateString())){
			dialogGeneric("Warning", "Reply date should be less than Expiry date." , "Ok");
		}
	}

	function ckpol(isvalue){
		console.log(isvalue);
	}

	function getAprrovalDetail(){
		//rptTableInit("t90approvaldetailArea");
		//getqtnNo = "HKG043284";
		var data = [];
		var datainput = {quotationNo : getqtnNo};
		getResultAjax("WS_QTN_ACTIVITY_DTL",
				{
					userData : userData,
					quotationNo : getqtnNo
				}).done(handleDisplayActivity);

			function handleDisplayActivity(response) {
				if(response.resultCode == "200"){
					rutOpenDialog('b3-AprrovalArea');
					_AddDataAppovalDetail(response.resultContent);
				}else if(response.resultCode == '401'){

					window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

				}else{
					dialogGeneric("Warning", response.resultMessage, "Ok");
				}
			}
	}

	function _AddDataAppovalDetail(Content){

		var DataApproval = rptGetDataFromDisplayAll("t90approvaldetailArea");
		if(!Isempty(DataApproval)){
			rptClearDisplay("t90approvaldetailArea");
		}
		rptAddDataWithCache("t90approvaldetailArea",Content);
		rptDisplayTable("t90approvaldetailArea");
		setRowView("t90approvaldetailArea","t90Seq");
	}

	function OpenDialogCreateInterim(){
		rutOpenDialog('b4-createInterimArea');
		$('#d90effDate').val($('#d10effDate').val());
		$('#d90expDate').val($('#d10expDate').val());

	}

	function createInterim(){
		if(validatefiled()){
			//getqtnNo = "R061323"
			var user = Getuser();
			var effDate = splitDate($('#d90effDate').val());
				//effDate = effDate[0]+""+effDate[1]+""+effDate[2];
			var expDate = splitDate($('#d90expDate').val());
				//expDate = expDate[0]+""+expDate[1]+""+expDate[2];



			var data = {
					userData : userData,
					"line" : userData.line
					,"trade" : userData.trade
					,"agent" : userData.agent
					,"fscCode" : userData.fscCode
					,"userId" : userData.userId
					,"qtnNo" : getqtnNo
					,"effDate" :  effDate
					,"expDate" :  expDate
					,"oneoffFlag" : "N"
					,"createInterim" : "Y"
			}
			console.log(JSON.stringify(data));
			//validate expDate
			var errorMsg = "";
			var param = new Date(convertDatevalidate($("#d90expDate").val()));
			var effDate = new Date(convertDatevalidate($("#d90effDate").val()));
			if((effDate > param || effDate.toDateString() === param.toDateString())){
				dialogGeneric("Warning", "Expiry Date should be greater than Effective Date." , "Ok");
			}else{
				getResultAjax("WS_QTN_CREATE_INTERIM", data).done(handlecreateinterim);
			}



			function handlecreateinterim(response){
				if(response.resultCode == "200"){
					rutCloseDialog('b4-createInterimArea');
					dialogGeneric("Sucress", response.resultMessage , "Ok");

				}else if(response.resultCode == '401'){

					window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

				}else{
					dialogGeneric("Warning", response.resultMessage, "Ok");
				}
			}
		}

	}

	function validatefiled(){
		var result = true;
		if($('#d90effDate').val() == ""){
			result = false;
			errorMsg = "Place of Effective Date is mandatory for Create Interim Quotation. ";
		}
		if($('#d90effDate').val() == ""){
			result = false;
			errorMsg = "Place of Expipry Date is mandatory for Create Interim Quotation ";
		}
		/*if(conCatDate(rutGetElementValue(d10follDate)) > conCatDate(rutGetElementValue(d10expDate))){
			result = false;
			errorMsg = "*Follow Up date should be less than Expiry date ";
		}*/

		if(!result){
			dialogGeneric("Warning", errorMsg , "Ok");
		}
		return result;
	}


	 function extenQuotation(){

		 var effDate = new Date(convertDatevalidate($("#d10effDate").val()));
		 var expipryD = new Date(convertDatevalidate($("#d10expDate").val()));
		 var expipryDate = new Date(convertDatevalidate($("#d92expipryDate").val()));
		 var curDate = new Date(getCurrentServerDateTH());
		 var result = true;
		 if(expipryDate < expipryD || expipryD.toDateString() == expipryDate.toDateString() ){
			 dialogGeneric("Warning", "Expiry Date should be greater than Quotation's Expiry Date" , "Ok");
			 result = false;
		 }else if(expipryDate <  curDate){
			 dialogGeneric("Warning", "Expiry Date should be greater than system date" , "Ok");
			 result = false;
		 }else if(expipryDate <  effDate){
			 dialogGeneric("Warning", "Expiry Date should be greater than Quotation's Effective Date" , "Ok");
			 result = false;
		 }

		 if(result){
			 qtn_Approval("",splitDate($('#d92expipryDate').val()),"extend","b6-ExtenQuotationArea",'EE');
		 }

	}

	 function exexpireQuotation(){
		 qtn_Approval("",splitDate(deleteDays(new Date(), 1)),"expire","",'EE');
	}


	function addCoridor() {

		 if(checkBeforeInsertCoridor('Adddcor')){
			 addCoridortest();
		 }

	}

	function addCoridortest(){
		 rptInsertNewRowBefore("t10shipTypeDetailArea", "end");
		 var newRow = $("#t10shipTypeDetailArea").find(".tblRow").not(":last").last();
		 var data = rptGetDataFromDisplay("t10shipTypeDetailArea", newRow.attr("id"));
		 data.action = 'i';
		 currectCorridor = newRow.attr("id");

		//setRowView("t10shipTypeDetailArea","t10SeqNo");
		var corridorArr = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
		var rowId = 't10row-'+parseInt(corridorArr.length - 1);
		onselectBG(newRow.attr("id"),'t10row');
		disableField("t10shipTypeDetailArea",newRow.attr("id"));
		checkRecodeScroll('t10shipTypeDetailArea','t10shipTypeDetailArea','shipTypeDetailArea-scroll',2);
		setSipmentCorridor(newRow.attr("id"));
		isSelect = parseInt(corridorArr.length - 1);
		ClearDataDisplayCaseNewCorridor();


	}

	function setOrgEmpty(){
		//containAndComm
		OrgDataContainerArea = [];
		OrgDataCommodityArea = [];
		//runthing
		OrgDataShipmeCommodity = [];
		//charge
		OrgDataChargesDetails = [];
		//DND
		OrgDataExportDetention  = {};
		OrgDataExportDemurrage  = {};
		OrgImportDetention  = {};
		OrgImportDemurrage  = {};

		OrgAdditionalFreeDays_D  = [];
		OrgAdditionalFreeDays_E  = [];
		OrgAdditionalFreeDays_I  = [];
		OrgAdditionalFreeDays_M  = [];

		OrgCharge_D  = [];
		OrgCharge_E  = [];
		OrgCharge_I  = [];
		OrgCharge_M  = [];
		//commentAndprint
		/*OrgPrintClausesArea  = [];
		OrgQuotationComments  = [];
		OrgqtnPrintPreText = "";
		OrgqtnPrintPostText = "";*/
	}



	function checkBeforeInsertCoridorvalidate(type,tab){
		if(tab == 'Maintenance'){
			if(getmode == "edit"){
				checkBeforeInsertCoridor_onchangeTab(type);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}

	function checkBeforeInsertCoridor_onchangeTab(){

		if(getmode == "edit"){
			if(fnIschangeData()){// Check onchange Data before Add coridor
				//check ship delete
				if(!deleteship()){
					IschangeDataCheckcoridor(type);
				}else{
					return true;
				}

			}else{
				return true;
			}
		}else{
			return true;
		}
	}

	function deleteship(){
		var result = true;
		var getship = getMaintenanceTabOutput();
			for (i = 0; i < getship.length; i++) {
				if(getship[i].action == 'd'){
					result = false;
					break;
				}
			}
		return result;
	}

	function checkBeforeInsertCoridor(type){
			if(IscheckAddNewcoridor()){
				if(getmode == "edit"){

						if(fnIschangeData()){// Check onchange Data before Add coridor
							IschangeDataCheckcoridor(type);
						}else{
							return true;
						}
				}else{
					return true;
				}
			}else{
				IschangeDataCheckcoridor(type);
			}

	}

	function IschangeDataCheckcoridor(type){
		if(type == "checkdiffcuridor"){
			console.log("checkdiffcuridor");
			dialogGenericv3("Maintenance","Warning", "Changes deleted, save quotation now?" , "Ok", "No").then(
					function(overwrite){
						if(overwrite){
							//check dift data org with cur data
							save();
						}else{
							return false;
						}
					});
		}else{
			console.log("!checkdiffcuridor");
			dialogGeneric("Warning", "Changes deleted, save quotation now?", "Yes", "No").then(
					function(overwrite){
						if(overwrite){
							//check dift data org with cur data
							if(type != undefined){
								console.log("!checkdiffcuridor "+type);
								save(type);
							}else{
								console.log("!checkdiffcuridor no type");
								save();
							}

						}else{
							return false;
						}
					});
		}

	}

	function IscheckHeader(){

		var data = {};

		data = {
				dataquotationHeader : rptGetDataFromSingleArea("d10MaintenanceHeader"),
				dataCustomer : rptGetDataFromDisplayAll("t21customerArea"),
				dataCommentAndPrint : GetDataComAndPrintCheckAddcoridor()

		}
		
		data.dataquotationHeader.subjRRFlag = fngetcheck("subjRRFlag");
		data.dataquotationHeader.subjDNDFlag = fngetcheck("subjDNDFlag");
		data.dataquotationHeader.printDetDemFlag = fngetcheck("printDetDemFlag");
		
		/*console.log("OrgDataMaintenanceHeader :: "+OrgDataMaintenanceHeader);
		console.log("data.dataquotationHeader :: "+JSON.stringify(data.dataquotationHeader));*/

		return (ISOnChange_Object(OrgDataMaintenanceHeader,data.dataquotationHeader)
				|| ISOnChange_ObjectList(OrgDataCustomer,data.dataCustomer,"customerSeqNo")
				|| ISOnChange_ObjectList(OrgPrintClausesArea,data.dataCommentAndPrint.PrintClause,"printClauseSeqNo")
				|| ISOnChange_ObjectList(OrgQuotationComments,data.dataCommentAndPrint.Comment,"commentSeqNo")
				|| !(OrgqtnPrintPreText === data.dataCommentAndPrint.qtnPrintPreText)
				|| !(OrgqtnPrintPostText === data.dataCommentAndPrint.qtnPrintPostText)
				);
	}

	function IscheckAddNewcoridor(){

		var dataOrgDataObjCorridor = new Array();
		if(!isEmpty(OrgDataObjCorridor)){
			dataOrgDataObjCorridor =JSON.parse(OrgDataObjCorridor);
		}

		var shipmentCorridor = rptGetDataFromDisplayAll("t10shipTypeDetailArea");

		//console.log(dataOrgDataObjCorridor)
		//console.log(shipmentCorridor)

		return (dataOrgDataObjCorridor.length === shipmentCorridor.length)
	}
	
	function fnIsChangeDataPrintPreview(){
			//fnIschangeData() And IscheckHeader()., When Adjust Data return ture.
		return (!fnIschangeData() && !IscheckHeader());
		//return (!fnIschangeData());
				
	}
	
	function IsSetDataForCheckChangeData(){
		var data = {};

		data = {
			datashipmentCorridor : converntDateBeforeChkChangeData(rptGetDataFromDisplayAll("t10shipTypeDetailArea")),
			dataContainer : rptGetDataFromDisplayAll("t31containerArea"),
			dataCommodity : rptGetDataFromDisplayAll("t32commodityArea"),
			dataCharge : converntDateBeforeChkChangeData_Charge(rptGetDataFromDisplayAll("t52ChargesDetailsArea")),
			dataDND : GetDataDNDCheckAddcoridor()
		};
		
		return data;
	}

	function fnIschangeData(){

		var data = IsSetDataForCheckChangeData();

			//console.log(ISOnChange_ObjectList(OrgDataObjCorridor,data.datashipmentCorridor,"corridorSeqNo"))
			
			return (ISOnChange_ObjectList(OrgDataObjCorridor,data.datashipmentCorridor,"corridorSeqNo")
					|| ISOnChange_ObjectList(OrgDataContainerArea,data.dataContainer,"containerSeqNo")
					|| ISOnChange_ObjectList(OrgDataCommodityArea,data.dataCommodity,"commoditySeqNo")
					|| ISOnChange_ObjectList(OrgDataChargesDetails,data.dataCharge,"orderby")
					|| ISOnChange_Object(OrgDataExportDetention_s,data.dataDND.exportDetentionDetails)
					|| ISOnChange_Object(OrgDataExportDemurrage_s,data.dataDND.exportDemurrageDetails)
					|| ISOnChange_Object(OrgImportDetention_s,data.dataDND.importDetentionDetails)
					|| ISOnChange_Object(OrgImportDemurrage_s,data.dataDND.importDemurrageDetails)
					|| ISOnChange_ObjectList(OrgAdditionalFreeDays_D_s,data.dataDND.ArrayadditionalFreeDays_D,"freeDaysSeqNo")
					|| ISOnChange_ObjectList(OrgAdditionalFreeDays_E_s,data.dataDND.ArrayadditionalFreeDays_E,"freeDaysSeqNo")
					|| ISOnChange_ObjectList(OrgAdditionalFreeDays_I_s,data.dataDND.ArrayadditionalFreeDays_I,"freeDaysSeqNo")
					|| ISOnChange_ObjectList(OrgAdditionalFreeDays_M_s,data.dataDND.ArrayadditionalFreeDays_M,"freeDaysSeqNo")
					|| ISOnChange_ObjectList(OrgCharge_D_s,data.dataDND.charges_D,"")
					|| ISOnChange_ObjectList(OrgCharge_E_s,data.dataDND.charges_E,"")
					|| ISOnChange_ObjectList(OrgCharge_I_s,data.dataDND.charges_I,"")
					|| ISOnChange_ObjectList(OrgCharge_M_s,data.dataDND.charges_M,"")
					);

	}



	function ClearDataDisplayCaseNewCorridor(){
		var arrrpower = ["t31containerArea","t32commodityArea","t40rountingDetailArea","t52ChargesDetailsArea"
						,"t66AdditionalFreeDays_D","t69AdditionalFreeDays_M","t68AdditionalFreeDays_I","t67AdditionalFreeDays_E"
						,"t81Charge_D","t82Charge_E","t83Charge_I","t84Charge_M"];


		for (i = 0; i < arrrpower.length; i++) {
			rptClearDisplay(arrrpower[i]);
		}
		//Header
		ClearDisplayDNDOnchange();
		//ClearCommentNPrint();
		//setOrgEmpty();

	}

	function setfeildHeader(){
		var quotaionHdr = {
		        "contact": "",
		        "contactPartyCode": "DEARPZ0001",
		        "contactPartyName": "******RN ************L ***P.,***.",
		        "custContact": "AND1",
		        "custType": "C",
		        "effDate": "2018-01-01",
				"expDate": "2018-12-31",
		        "fax": "******2",
		       // "follDate": "2019-02-27",
		        "inqDateTime": "18/02/2019 14:00",
		        "interim": "N",
		        "mail": "",
		        "oneOff": "N",
		        "opCode": "",
		        "pos": "BKK",
		        "qtdBy": "SUCSRI1",
		       // "quotationNo": "",
		        "ref": "RRQ/BKK/SYD/001",
		        "remarks": "",
		       // "repDate": "2019-02-25",
		        "revision": "N",
		        "roeTable": "",
		        "slm": "SAB",
		        "status": "O",
		        "supersedeQuotation": "",
		        "tel": "******0-9",
		        "action": "i"
		};
		return quotaionHdr;
	}

	function setSizeTypeRowView(){}

	function deleteCoridor(){
		dialogGeneric("Warning", "The selected corridor and its relevant data will be deleted. Do you want to continue?" , "Ok", "No").then(
				function(overwrite){
					if(overwrite){
						HanderGetDataDeleteList('t10shipTypeDetailArea', "selectDelete");
						var rowactondelete = Isonsetdelete('t10shipTypeDetailArea', "selectDelete");
						var deletingRow = GetDeleteRowDelete("t10shipTypeDetailArea","selectDelete");
						rptTryToDeleteSelectedRows('t10shipTypeDetailArea', "selectDelete", true);
						rptForeachRow('t10shipTypeDetailArea', setSizeTypeRowView, false, ":last");
						//setRowView("t10shipTypeDetailArea","t10SeqNo");
						qtn_selectRow(0,deletingRow);
						//onselectRouting();
						checkRecodeScroll('t10shipTypeDetailArea','t10shipTypeDetailArea','shipTypeDetailArea-scroll',2);

						if(rowactondelete == 'i'){
							window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
						}

					}else{
						return false;
					}
				});
	}


	function Isonsetdelete(tableId,checkboxIdWithoutPrefix){
		var allData = rptGetDataFromDisplayAll(tableId);
		var deleting = "";
		var checkboxIdWithoutPrefix = "selectDelete";
		allData.forEach(function(row){
			if(row[checkboxIdWithoutPrefix] === 'Y'){
				deleting = row.action;
			}
		});
		return deleting;
	}

	function GetDeleteRowDelete(tableId, checkboxIdWithoutPrefix){
		var allData = rptGetDataFromDisplayAll(tableId);
		var deleting = "";
		var checkboxIdWithoutPrefix = "selectDelete";
		allData.forEach(function(row){
			if(row[checkboxIdWithoutPrefix] === 'Y'){
				deleting = row.rowId;
			}
		});

		return deleting;
	}

	function HanderGetDataDeleteSingle(tableId, checkboxIdWithoutPrefix){
		var allData = rptGetDataFromDisplayAll(tableId);
		allData.forEach(function(row){
			if(row[checkboxIdWithoutPrefix] === 'Y'){
				if(row.action != 'i'){
					deleting = row;
					deleting.action = "d";
				}
			}
		});
	}

	function HanderGetDataDeleteList(tableId, checkboxIdWithoutPrefix){
		var allDataList = rptGetDataFromDisplayAll(tableId);
		allDataList.forEach(function(rowlist){
			if(rowlist[checkboxIdWithoutPrefix] === 'Y'){
				if(rowlist.action != 'i'){
					deletingList = rowlist;
					deletingList.action = "d";
				}
			}
		});
	}

	function setarrayDelet(object, newarray){
		var getdelete = HanderGetDataDeleteAllCntr(object, "selectDelete");
		if(getdelete.length > 0){
			for (var i = 0; i < getdelete.length; i++) {
				newarray.push(getdelete[i]);
			}
		}
		return newarray;
	}

	function setarrayDeletAll(object, newarray){
		var getdelete = HanderGetDataDeleteAllv2(object, "selectDelete");
		if(getdelete.length > 0){
			for (var i = 0; i < getdelete.length; i++) {
				newarray.push(getdelete[i]);
			}
		}
		return newarray;
	}

	function HanderGetDataDeleteAllv2(tableId, checkboxIdWithoutPrefix){
		var allData = rptGetDataFromDisplayAll(tableId);
		var setdata = [];
				allData.forEach(function(row){
					if(row[checkboxIdWithoutPrefix] === 'Y'){
						if(row.action != 'i'){
							deleting = row;
							deleting.action = "d";
						}
						setdata.push(deleting);
					}

				});
		return setdata;
	}


	function HanderGetDataDeleteAllCntr(tableId, checkboxIdWithoutPrefix){
		var allData = rptGetDataFromDisplayAll(tableId);
		var setdata = [];

				allData.forEach(function(row){
					if(row[checkboxIdWithoutPrefix] === 'Y'){
						ch = false;

						if(row.action != 'i'){
							ch = true;
						}

						if(row.action == 'i' && row.isChkCopyOog == 'Y'){
							ch = true;
						}

						if(ch){
							deleting = row;
							deleting.action = "d";
							setdata.push(deleting);
						}

					}
				});
		return setdata;
	}

	function HanderGetDataDeleteAll(tableId, checkboxIdWithoutPrefix){
		var allData = rptGetDataFromDisplayAll(tableId);
				allData.forEach(function(row){
					if(row[checkboxIdWithoutPrefix] === 'Y'){
						if(row.action != 'i'){
							deleting = row;
							deleting.action = "d";
						}
					}
				});
		return deleting;
	}

	function isOnChangeCorridor(rowId,classId){

		prevCorridor = currectCorridor;
		var getprevCorridor = prevCorridor.split("-");
		var getcurrectCorridor = rowId.split("-");
		if(getprevCorridor[1] != getcurrectCorridor[1]){
			if(checkBeforeInsertCoridor()){
				currectCorridor = rowId;
				disableField("t10shipTypeDetailArea",rowId);

				onselectBG(rowId,classId);

				OnchangeRouting(rowId);

				onselectRouting();

			}
		}


	}


	function onSelectCorridor(rowId) {
			prevCorridor = currectCorridor;
			var getprevCorridor = prevCorridor.split("-");
			var getcurrectCorridor = rowId.split("-");
			if(getprevCorridor[1] != getcurrectCorridor[1]){
				currectCorridor = rowId;
				disableField("t10shipTypeDetailArea",rowId);
				OnchangeRouting(rowId);
				onselectRouting();
			}
	}

	function OnchangeRouting(rowId){
		var getrowid = rowId.split("-");
		var arrClass = ["t10row","t22row","t30row","t41row","t51row","t61row","t71row"];
		for (y = 0; y < arrClass.length; y++) {
			onselectBG(arrClass[y]+"-"+getrowid[1],arrClass[y]);
		}


	}

	function setactiveFac(rowId){
		getactiveFlag = rowId;
		//rutSetElementValue("t10activeFlag-"+rowId,"Y");
	}


	function onselectRouting(){
		//var getRoutingHeader = getRouting();

		var getRoutingHeader = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
		for(var i = 0; i < getRoutingHeader.length; i++){
			if(i == isSelect){

				var quotationNo = rutGetElementValue("d10quotationNo");
				var corridorSeqNo = getRoutingHeader[i].corridorSeqNo;
				console.log("viewCorridorSeq :: "+corridorSeqNo)
					sessionStorage.setItem("viewQtnQuotationNo", quotationNo);
					sessionStorage.setItem("viewCorridorSeq", corridorSeqNo);
					sessionStorage.setItem("viewRowId", isSelect);
					window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
				}
			}
	}


	function fetchDataMaintenanceTab(content){
		curCorridor	= content.shipmentCorridor;
		oldHdr = content.quotationHeader;
		setOption(curCorridor);
		rptSetSingleAreaValues("d10MaintenanceHeader", SetAction_edit_Object(content.quotationHeader));
		var getdatat10 = SetAction_edit(content.shipmentCorridor);
		rptAddDataWithCache("t10shipTypeDetailArea", setShipmentDateOrg(getdatat10));
		rptDisplayTable("t10shipTypeDetailArea");
		setDisplayMaintence();
		//setRowView("t10shipTypeDetailArea","t10SeqNo");
		DataOrgMaintenanTab("d10MaintenanceHeader","t10shipTypeDetailArea");
		selectFirstRow();
		perCorridor = JSON.parse(JSON.stringify(rptGetDataFromDisplayAll("t10shipTypeDetailArea")));
		IsSetShipmentType(content.shipmentCorridor);
		if(IsOneOff){
			$("#_t10deleteBtn").remove();
			$("#_t10addBtn").remove();
		}

		chekdisplayflagDndAndCharge(content.quotationHeader,"E");

	}

	function setOption(data){

	}

	function chekdisplayflagDndAndCharge(content,flag){
		if(flag == 'E'){
			fnsetcheckbox(content.subjRRFlag,"subjRRFlag");
			fnsetcheckbox(content.subjDNDFlag,"subjDNDFlag");
			if(content.pos == 'R'){
				setCustomTagProperty("subjRRFlag", {dis:false});
				setCustomTagProperty("subjDNDFlag", {dis:false});
			}else{
				setCustomTagProperty("subjRRFlag", {dis:true});
				setCustomTagProperty("subjDNDFlag", {dis:true});
			}

		}else if(flag == 'N'){
			if(userData.fscCode == 'R'){
				setCustomTagProperty("subjRRFlag", {dis:false});
				setCustomTagProperty("subjDNDFlag", {dis:false});
				document.getElementById("subjRRFlag").checked=false;
				document.getElementById("subjDNDFlag").checked=false;
			}else{
				setCustomTagProperty("subjRRFlag", {dis:true});
				setCustomTagProperty("subjDNDFlag", {dis:true});
				document.getElementById("subjRRFlag").checked=true;
				document.getElementById("subjDNDFlag").checked=true;
			}
		}

		fnsetcheckbox(content.printDetDemFlag,"printDetDemFlag");


	}

	function setDisplayMaintence(){
		var getMaintence = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
		if(IsOneOff){
			console.log("IsOneOff >> ");
			for (var a = 0; a < getMaintence.length; a++) {
				$.each(getMaintence[a], function(k, v) {

					if(k == "teu"){
						console.log(k)
						setCustomTagProperty("t10"+k+"-"+a, {rdo:false});
					}

				});
			}
		}else{
			for (var a = 0; a < getMaintence.length; a++) {
				$.each(getMaintence[a], function(k, v) {
					if(k == "teu"){
						setCustomTagProperty("t10"+k+"-"+a, {rdo:true});
					}

				});
			}
		}
	}

	function removeGeneralSearchButton(elementName){
		$("#" + elementName).parent().css("padding-right", "0px");
		$("#" + elementName).parent().next().remove();
	}

	function DataOrgMaintenanTab(tab1,tab2){
		var getMaintenanHeader = rptGetDataFromSingleArea(tab1);

		getMaintenanHeader.subjRRFlag = fngetcheckfield(getMaintenanHeader.subjRRFlag);
		getMaintenanHeader.subjDNDFlag = fngetcheckfield(getMaintenanHeader.subjDNDFlag);
		getMaintenanHeader.printDetDemFlag = fngetcheckfield(getMaintenanHeader.printDetDemFlag);

		OrgDataMaintenanceHeader = JSON.stringify(getMaintenanHeader);
		/*OrgDataObjCorridor = rptGetDataFromDisplayAll(tab2);
		for (i = 0; i < OrgDataObjCorridor.length; i++) {
			OrgDataObjCorridor[i].shipDate = splitDate(OrgDataObjCorridor[i].shipDate);
		}*/
		OrgDataObjCorridor = JSON.stringify(rptGetDataFromDisplayAll(tab2));
		//console.log("OrgDataObjCorridor :: " +OrgDataObjCorridor);
	}

	function deleteCustomer(rowId) {
		rptRemoveRow("t10shipTypeDetailArea", rowId);
	}

	function testCustomer(){
		let change = rptGetInsertsAndChanges("t10shipTypeDetailArea", true);

		//console.log(rptGetDataFromDisplay("t10customersListArea", "t10row-3"));
		//console.log(getChangedData("t10shipTypeDetailArea", false, preChangedCustomer));
	}

	function refresh(){
		let bookingNo = "BTAOC09001193";//sessionStorage.getItem("viewBookingNo");
		fetchBooking(bookingNo);
	}

	function getMaintenanceDelete() {
		var shipmentCorridor = [];

		shipmentCorridor = rptGetDataFromDisplayAll("t10shipTypeDetailArea");

		if(!isEmpty(deletingList)){
			shipmentCorridor.push(deletingList);
		}

		return shipmentCorridor;
	}

	//ShipmentCorridor : rptGetDataFromSingleArea("t10shipTypeDetailArea
	function getMaintenanceTabOutput() {

		var quotationHeader = rptGetDataFromSingleArea("d10MaintenanceHeader");

		quotationHeader.subjRRFlag = fngetcheck("subjRRFlag");
		quotationHeader.subjDNDFlag = fngetcheck("subjDNDFlag");
		quotationHeader.printDetDemFlag = fngetcheck("printDetDemFlag");

		var shipmentCorridor = [];

		shipmentCorridor = rptGetDataFromDisplayAll("t10shipTypeDetailArea");

	/*	console.log("OrgDataObjCorridor1 :: " +OrgDataObjCorridor);

		console.log("shipmentCorridorpre1 :: "+JSON.stringify(shipmentCorridor));*/

		//getDatabeforadjust(shipmentCorridor);

		if(!isEmpty(deletingList)){
			shipmentCorridor.push(deletingList);
		}

		//console.log("shipmentCorridor push Delete : "+JSON.stringify(shipmentCorridor));



		shipmentCorridor = converntDateBeforeChkChangeData(shipmentCorridor);

			for (y = 0; y < shipmentCorridor.length; y++) {
				delete shipmentCorridor[y].activeFlag;
			}

		//console.log("OrgDataMaintenanceHeader ::"+OrgDataMaintenanceHeader);
		//console.log("quotationHeader ::"+JSON.stringify(quotationHeader));
		var quotationHeader = CheckDataOnChange_Object(OrgDataMaintenanceHeader,quotationHeader);
		//console.log("shipmentCorridorpre1 :: "+JSON.stringify(shipmentCorridor));

		shipmentCorridor = CheckDataOnChange_ObjectListNew(OrgDataObjCorridor,shipmentCorridor,"corridorSeqNo");

			//console.log("OrgDataObjCorridor :: " +OrgDataObjCorridor);

			//console.log("shipmentCorridorpre :: "+JSON.stringify(shipmentCorridor));

		for (i = 0; i < shipmentCorridor.length; i++) {

			if(i == getactiveFlag){
				shipmentCorridor[i].activeFlag = "Y";
			}else{
				shipmentCorridor[i].activeFlag = "";
			}

			shipmentCorridor[i].shipDate = splitDate(shipmentCorridor[i].shipDate);

			if(shipmentCorridor[i].shipDateOrg != ""){

				shipmentCorridor[i].shipDateOrg = splitDate(shipmentCorridor[i].shipDateOrg);

			}


		}

		quotationHeader.effDate = splitDate(quotationHeader.effDate);
		quotationHeader.expDate = splitDate(quotationHeader.expDate);
		quotationHeader.repDate = splitDate(quotationHeader.repDate);
		quotationHeader.follDate = splitDate(quotationHeader.follDate);

		//console.log("quotationHeader :: "+JSON.stringify(quotationHeader));

		//console.log("shipmentCorridor :: "+JSON.stringify(shipmentCorridor));

		var objMaintenance = {
				quotationHeader : quotationHeader,
				shipmentCorridor : shipmentCorridor
		};

		return objMaintenance;
	}

	function converntDateBeforeChkChangeData(Isdata){

		for (var x = 0; x < Isdata.length; x++) {

			Isdata[x].shipDate = conCatDate(Isdata[x].shipDate);

			if(Isdata[x].shipDateOrg != ""){

				Isdata[x].shipDateOrg = conCatDate(Isdata[x].shipDateOrg);

			}

		}
		return Isdata;
	}


	function getDatabeforadjust(IsDate){
		getshipmentCorridor = JSON.stringify(IsDate);
	}

	function qtn_selectRow(rownum,rowId){

		var getrow = rowId.split("-");
		var settingCorridor = rptGetTableSettings("t10shipTypeDetailArea");
		var setRownum = settingCorridor.rowId+"-"+rownum;

		 $('.t10row').removeClass('selected');
		 $('#'+setRownum).toggleClass(" selected ");
		 //disableField("t10shipTypeDetailArea",currectCorridor);
		 setactiveFac(getrow[1]);

	}

	function selectFirstRow(){
		var setrow = curCorridorSeq;
		if(curCorridorSeq > 0){
			setrow = curCorridorSeq - 1;
		}

		if(viewRowId != undefined && viewRowId != ""){
			setrow = viewRowId;
		}

		if(setrow == null || setrow == 'null'){
			setrow = 0;
		}
		console.log("selectFirstRow :: "+setrow);

		var settingCorridor = rptGetTableSettings("t10shipTypeDetailArea");
		currectCorridor = settingCorridor.rowId+"-"+setrow;
		onselectBG(currectCorridor,'t10row');
		//$('#'+currectCorridor).find("input[type='radio']").trigger("click");
		disableField("t10shipTypeDetailArea",currectCorridor);

	}

	function disableField(containerId,tableRowId){
		if(!Isview){
			$("#" + containerId).find(".tblRow").not(':last').each(function(){
				//alert("tableRowId "+tableRowId);
				var rowData = $(this)[0].id;
				var getrow = rowData.split("-");
				if(tableRowId != rowData){
					$("#"+$(this)[0].id).find(".tblField").each(function(){
						//alert("NOde "+this.nodeName);
						//console.log("Type "+this.type);
						//console.log("Id "+this.id);
						//console.log("Id disabled"+this.id);
						//alert("Id "+this.id.substring(0, this.id.indexOf("-")));
						//alert("value "+this.value);
						$("#" + this.id).prop('disabled', true);
						selectShipmentType(rowData);
					});
					$("#t10selectDelete-" + getrow[1]).prop('checked', false);
					$("#t10coppy-" + getrow[1]).toggleClass('Adisabled');
				}else{
					$("#"+$(this)[0].id).find(".tblField").each(function(){
						$("#" + this.id).prop('disabled', false);
						selectShipmentType(rowData);
					});
					$("#t10coppy-" + getrow[1]).removeClass('Adisabled');

				}
		   	});
		}
	}

	function SelectCoppy(rowid,classId){

	}

	function setSipmentCorridor(isId){
		 var rawRowNumber = isId.substring(isId.lastIndexOf('-'));

		 selectShipmentType(isId);

		rutSetElementValue("t10weight"+rawRowNumber, "0.00");
		rutSetElementValue("t10measurement"+rawRowNumber, "0.0000");
		rutSetElementValue("t10rorArea"+rawRowNumber, "0.0000");
		rutSetElementValue("t10socCoc"+rawRowNumber, "C");
		rutSetElementValue("t10pm"+rawRowNumber, "M");

	}

	function IsSetShipmentType(IsData){
		for(var i=0; i<IsData.length; i++){

			var isId = IsData[i].rowId;

			var rawRowNumber = isId.substring(isId.lastIndexOf('-'));

			selectShipmentType(IsData[i].rowId);

			//$('#t10shipDateOrg'+rawRowNumber).val(IsData[i].shipDate);

			//rutSetElementValue("t10shipDateOrg"+rawRowNumber, IsData[i].shipDate);

		}
	}

	function setShipmentDateOrg(ISDate){
		for(var i = 0; i < ISDate.length; i++){
			ISDate[i].shipDateOrg = ISDate[i].shipDate;
		}

		return ISDate;
	}


	 function selectShipmentType(isId){
		  if(isId != null){
			 var rawRowNumber = isId.substring(isId.lastIndexOf('-'));

			 var shipmTypeId = 't10shipmentType'+rawRowNumber;

			 var rateBasisId='t10rateBasis'+rawRowNumber;

			 var rateBasisElement=document.getElementById(rateBasisId);

			 var value=rutGetElementValue(shipmTypeId);

		}else{
			 var shipmTypeId=document.activeElement.id;

			 var rawRowNumber=shipmTypeId.substring(shipmTypeId.lastIndexOf('-')); //get row suffix wich is the suffix of each element in the same row

	         var rateBasisId='t10rateBasis'+rawRowNumber; //get id of rateBase element

	         var rateBasisElement=document.getElementById(rateBasisId);

	         var value=rutGetElementValue(shipmTypeId);

	         fnSetonchangeShipmentType(value);

		 }


		 if (value=='UC'){

			 	 rateBasisElement.innerHTML='<option value="W">Weight</option><option value="M">Measurement</option>'+

                 '<option value="R">Revenue Ton</option><option value="U">Unit</option>';


             rateBasisElement.removeAttribute('disabled');

         } //endif Shipment type UC

         else {
        	 rateBasisElement.setAttribute('disabled', 'disabled');

           //  rateBasisElement.innerHTML='<option value="1">POL--%>POD</option>';


             rateBasisElement.innerHTML='<option value="1">POR -->  POD</option>'+

             '"<option value="2">POR -->  DEL</option>'+

             '"<option selected="" value="3">POL -->  POD</option>'+

             '"<option value="4">POL -->  DEL</option>';

         } //endelse: shipment type not UC

         var getship = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
         if(getship.length > 0){
        	 if(isId != null){
	        	 for (var a = 0; a < getship.length; a++) {
	        		$('#t10rateBasis-'+a).val(getship[a].rateBasis);
	 			  }
        	 }

        	 check_shipmentType(getship);

         }


    } //end function selectShipmentType


     function fnSetonchangeShipmentType(Isvalue){
    	// console.log("fnSetonchangeShipmentType")
    	 rptClearDisplay("t52ChargesDetailsArea");
    	 SetCntrAndCommTab(Isvalue);
    	 onChangeShipMan();
    }

     function ISOnChange_Object(oldData,newDate){
         var NewArraySetAction = {};
         NewArraySetAction = newDate;
           if(!isEmpty(oldData)){
        	 // console.log("oldData :: "+oldData);
        	//   console.log("newDate :: "+JSON.stringify(newDate));
    	    	if(!jsonEqual(oldData, newDate)){
    	    		console.log("You have adjust Data Want To Save Data On Change?");
    	    		return true;
    	    	}else{
    	    		return false;
    	    	}
    	    }
         }

     function ISOnChange_ObjectList(oldData,newDate,sortBy){
    	 var result = false;
    	 var i = 0;
    	 var NewArraySetAction = [];
	 	    NewArraySetAction = newDate;
	 	 	oldData = JSON.parse(oldData);
	 	if(newDate.length == oldData.length){
	 		//console.log("1")
	 		if(!isEmpty(oldData) && !isEmpty(newDate)){
	 			//console.log("2")
	    		 NewArraySetAction.sort(function(a, b) {
	    			
			 		 	if(sortBy == "seqNo"){
			  		  		var x = a.seqNo, y = b.seqNo;
			  		  	}else if(sortBy == "corridorSeqNo"){
			 				var x = a.corridorSeqNo, y = b.corridorSeqNo;
			 		 	}else if(sortBy == "customerSeqNo"){
			 				var x = a.customerSeqNo, y = b.customerSeqNo;
			 		 	}else if(sortBy == "containerSeqNo"){
			 				var x = a.containerSeqNo, y = b.containerSeqNo;
			 		 	}else if(sortBy == "commoditySeqNo"){
			 				var x = a.commoditySeqNo, y = b.commoditySeqNo;
			 		 	}else if(sortBy == "voyageSeqNo"){
			 				var x = a.voyageSeqNo, y = b.voyageSeqNo;
			 		 	}else if(sortBy == "chargeSeqNo"){
			 				var x = a.chargeSeqNo, y = b.chargeSeqNo;
			 		 	}else if(sortBy == "orderby"){
			 				var x = a.orderby, y = b.orderby;
			 		 	}else if(sortBy == "freeDaysSeqNo"){
			 				var x = a.freeDaysSeqNo, y = b.freeDaysSeqNo;
			 		 	}else if(sortBy == "printClauseSeqNo"){
			 				var x = a.printClauseSeqNo, y = b.printClauseSeqNo;
			 		 	}else if(sortBy == "commentSeqNo"){
			 				var x = a.commentSeqNo, y = b.commentSeqNo;
			 		 	}else{
			 				var x = a.rowId, y = b.rowId;
			 		 	}
			  		   return x < y ? -1 : x > y ? 1 : 0;
			  	  });

	 			//console.log("orgi :"+JSON.stringify(oldData));
				//console.log("new :"+JSON.stringify(NewArraySetAction));

			 		for(var b = 0; b < oldData.length; b++){ //Loop ORG DATA for get object.

		 	    		delete NewArraySetAction[b].selectDelete;

			 	    		if(!jsonEqual( JSON.stringify(oldData[b]) , NewArraySetAction[b] )){ // check Diff Json object.
			 	    			if(NewArraySetAction[b].action != 'i' && NewArraySetAction[b].action != 'd'){ //Add action u
			 	    				console.log("You have adjust Data Want To Save Data On Change?");
			 	    	    		i ++;
			 		    	    }
			 	    		 }
			 		 }
    	 	}else{
    	 		return false;
    	 	}
	     }else{
	    	 return true;
	 	 }

	 	if(i > 0){
	 		return true;
	 	}else{
	 		return false;
	 	}

 	 }

     function CheckDataOnChange_Object(oldData,newDate){
     var NewArraySetAction = {};
     NewArraySetAction = newDate;

       if(!isEmpty(oldData)){
	    	if(!jsonEqual(oldData, newDate)){
	    		console.log("You Want To Save Data On Change?");
	    		if(NewArraySetAction.action != 'i' && NewArraySetAction.action != 'd'){
	    			NewArraySetAction.action = "u";
	    		}
	    	}else{
	    		if(NewArraySetAction.action == 'u'){
	    			NewArraySetAction.action = "";
	    		}
	    	}
	    }

       return NewArraySetAction;
     }

     function CheckDataOnChange_ObjectListNew(oldData,newDate,sortBy){
    	 var NewArraySetAction = [];
	 	    NewArraySetAction = newDate;
	 	    if(!isEmpty(oldData)){
		 	   oldData = JSON.parse(oldData);
			 	   if(NewArraySetAction.length == oldData.length){
				 		if(!isEmpty(oldData) && !isEmpty(NewArraySetAction)){

						 		NewArraySetAction.sort(function(a, b) {
						 		 	if(sortBy == "seqNo"){
						  		  		var x = a.seqNo, y = b.seqNo;
						  		  	}else if(sortBy == "corridorSeqNo"){
						 				var x = a.corridorSeqNo, y = b.corridorSeqNo;
						 		 	}else if(sortBy == "customerSeqNo"){
						 				var x = a.customerSeqNo, y = b.customerSeqNo;
						 		 	}else if(sortBy == "containerSeqNo"){
						 				var x = a.containerSeqNo, y = b.containerSeqNo;
						 		 	}else if(sortBy == "commoditySeqNo"){
						 				var x = a.commoditySeqNo, y = b.commoditySeqNo;
						 		 	}else if(sortBy == "voyageSeqNo"){
						 				var x = a.voyageSeqNo, y = b.voyageSeqNo;
						 		 	}else if(sortBy == "chargeSeqNo"){
						 				var x = a.chargeSeqNo, y = b.chargeSeqNo;
						 		 	}else if(sortBy == "freeDaysSeqNo"){
						 				var x = a.freeDaysSeqNo, y = b.freeDaysSeqNo;
						 		 	}else if(sortBy == "printClauseSeqNo"){
						 				var x = a.printClauseSeqNo, y = b.printClauseSeqNo;
						 		 	}else if(sortBy == "commentSeqNo"){
						 				var x = a.commentSeqNo, y = b.commentSeqNo;
						 		 	}else{
						 				var x = a.rowId, y = b.rowId;
						 		 	}
						  		   return x < y ? -1 : x > y ? 1 : 0;
						  	  });


			 	   		for(var b = 0; b < oldData.length; b++){ //Loop ORG DATA for get object.
			 	    		 if(!jsonEqual(JSON.stringify(oldData[b]), NewArraySetAction[b])){ // check Diff Json object.
			 	    			 console.log("You Want To Save Data On Change?");
			 	    			 console.log(JSON.stringify(NewArraySetAction[b]))
			 	    			if(NewArraySetAction[b].action != 'i' && NewArraySetAction[b].action != 'd'){ //Add action u
			 	   			 		NewArraySetAction[b].action = "u";
			 		    	    }
			 	    		 }else{
			 		    		console.log("CheckDataOnChange_ObjectListNew  Not Change?");
			 		    		if(NewArraySetAction[b].action == 'u'){
			 		    			NewArraySetAction[b].action = "";
			 		    		}
			 		    	}
			 	    	 }
				 	}
		     	}
	 	    }
 	       return NewArraySetAction;
 	 }

     var quotationNo = "";
     function copyOOG(rowid){
    	 var setrowid = "t10row-"+isSelect;
		 var getshipType = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid);
		 var getconAndcomm = getContainerCommodityTabOutput();
	   	 var container = getconAndcomm.container;
	   	 var commodity = getconAndcomm.commodity;

		 	if(validateOOGandBBK(rowid)){
			 	if(validateCopyOOG(getshipType)){
			 		quotationNo = rptGetDataFromDisplay("t10shipTypeDetailArea", rowid);
		    		 if(container.length > 0 || commodity.length){
		    			 rutOpenDialog('b5-CopyOogArea');
		    		 }else{
		    			 copyoogcon('Y');
		    		 }

			    }
    	 	}

     }

     function validateOOGandBBK(rowid){
    	 var result = true;
    	 var errorMsg = "";
    	 var rowId = rowid.split("-");
   	  	 var getpol = rutGetElementValue("t10pol-"+rowId[1]);
	     var getpod = rutGetElementValue("t10pod-"+rowId[1]);
	     var getinqType = rutGetElementValue("t10inqType-"+rowId[1]);

		 if(getinqType == ""){
			 result = false;
			 errorMsg = "Inq. Type is required.";
		 }

		 if(getinqType == 'D'){
			 var contactPartyCode = rutGetElementValue("d10contactPartyCode");
			
			 if(contactPartyCode == ""){
					 result = false;
					 errorMsg = "ContactPartyCode is required.";

				 }
		 }else{
			 if(getpol == ""){
				 result = false;
				 errorMsg = "POL is required.";
			 }

			 if(getpod == ""){
				 result = false;
				 errorMsg = "POD is required.";
			 }

		}
			if(!result){
				 dialogGeneric("Warning", errorMsg, "Ok");
				}

		
    	 return result;
     }

     function lookupoogBkk(rowid){
    	 if(validateOOGandBBK(rowid)){
    		 var rowId = rowid.split("-");
    		 var getinqType = rutGetElementValue("t10inqType-"+rowId[1]);
    		 if(getinqType == 'B'){
    			 rutOpenLookupTable('VRL_QTN_BKK_INQ','INQUIRY_REF_NO','t10oogBkkInq'+rowid,'INQUIRY_REF_NO*main POL POD','t10oogBkkInq'+rowid+' t10pol'+rowid+' t10pod'+rowid,'LIKE = =','');
    		 }else if(getinqType == 'O'){
    			 rutOpenLookupTable('VRL_QTN_OOG_INQ','INQUIRY_REF_NO','t10oogBkkInq'+rowid,'INQUIRY_REF_NO*main POL POD','t10oogBkkInq'+rowid+' t10pol'+rowid+' t10pod'+rowid,'LIKE = =','');
			 }else if(getinqType == 'D'){
				 var contactPartyCode = rutGetElementValue("d10contactPartyCode");
				// var POR = "t10por-"+rowId[1];
				 var POL = "t10pol-"+rowId[1];
				 var POT1 = "t10pot1-"+rowId[1];
				 var POT2 = "t10pot2-"+rowId[1];
				 var POD = "t10pod-"+rowId[1];
				 var DEL = "t10del-"+rowId[1];
				 
				 var sco,sva,sop;
				 

				 	 sco = 'DGS_APPROVAL_ID*main CONTRACT_PARTY_CODE COC_SOC';
				 	 sva = 't10oogBkkInq'+rowid+' d10contactPartyCode t10oogBkkInq'+rowid;
				 	 sop = 'LIKE = =';
				 	
				 if(rutGetElementValue("t10pol-"+rowId[1]) != ""){
					 sco += ' FK_POL';
					 sva += ' '+POL
					 sop += ' =';
						
				 }

			/*	 if(rutGetElementValue("t10por-"+rowId[1]) != ""){
					 sco += ' FK_POR';
					 sva += ' '+POL; 
					 sop += ' =';
						
				 }
*/
				 if(rutGetElementValue("t10pot1-"+rowId[1]) != ""){
					 sco += ' FK_POT1';
					 sva += ' '+POT1; 
					 sop += ' =';
						
				 }

				 if(rutGetElementValue("t10pot2-"+rowId[1]) != ""){
					 sco += ' FK_POT2';
					 sva += ' '+POT2; 
					 sop += ' =';
						
				 }

				 if(rutGetElementValue("t10pod-"+rowId[1]) != ""){
					 sco += ' FK_POD';
					 sva += ' '+POD; 
					 sop += ' =';
						
				 }

				 if(rutGetElementValue("t10del-"+rowId[1]) != ""){
					 sco += ' FK_DEL';
					 sva += ' '+DEL; 
					 sop += ' =';
						
				 }

				 console.log(sco);
				
	    		// rutOpenLookupTable('VRL_QTN_DG_APPROVAL_ID','DGS_APPROVAL_ID','t10oogBkkInq'+rowid,sco,sva,sop);
	    		rutOpenLookupTable('VRL_QTN_DG_APPROVAL_ID','DGS_APPROVAL_ID','t10oogBkkInq'+rowid,sco,sva,sop);
				 
        	 }

    	 }


     }

     function copyoogcon(isvalue){
    	 //console.log("copyoogcon :: "+isvalue);
    	 var inqType = $('#t10inqType-'+isSelect).val();
    	 console.log("inqType :: "+inqType)
    	 var datajson = {};
    	 var getconAndcomm = GetDatacontainerAndcomm();
    	 var setrowid = "t10row-"+isSelect;
		 var getshipType = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid);

		 if(getshipType.counterProposal == ''){
			 getshipType.counterProposal = 'N';
		 }

		 datajson = {
					"userData" : userData,
	  			  	"inquiryType": getshipType.inqType,
	  	    	  	"oogBbkInquiryNo": getshipType.oogBkkInq,
	  	    	  	"counterProposal": getshipType.counterProposal,
	  	    	 	"overwriteFlag":isvalue,
	  	    	  	"containerAndCommodityTab":{
			  	    		  						"container": getconAndcomm.container,
			  	 			  						"commodity": getconAndcomm.commodity
	  	    	  								}
			 };

		 if(inqType == 'D'){
			 rutCloseDialog('b5-CopyOogArea');
			 var getcustomer = GetDataCustomer();
			 var getDND = getDetentionDemurrageTabOutput();
			// var IsGetDND = Get_DND_ForCopyDG();

			 datajson.detentionDemurrageTab = getDND;
			// datajson.detentionDemurrageTab.dgApprovalId = '';
			// datajson.getDND = IsGetDND;
			 datajson.customer = getcustomer;
			 datajson.isNew = isNew;
			// datajson.DGS_APPROVAL_ID = '';
		}

		//console.log("getDG" + JSON.stringify(datajson));

    	getResultAjax("WS_QTN_COPY_OOG_BBK", datajson).done(handlecopyOog);


    	function handlecopyOog(response){
    		 if(response.resultCode == "200"){
    			 _addconAndcomm(response.resultContent.containerAndCommodityTab);
    			 IsClearChangeAndDnd('','');
    			 ClearDeleteContainer();
 			}else if(response.resultCode == '401'){

				window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

			}else{
				dialogGeneric("Warning", response.resultMessage, "Ok");
			}
    	 }

    	 function _addconAndcomm(pcRoute){

    		 rutSetElementValue("t10teu-"+isSelect, pcRoute.teu);

    		 	setEmptyconAndcomm();
    		 	setConAndcommCopyOog(pcRoute);
    		 	SetCntrAndCommTabV2();

    			/*rptAddDataWithCache("t31containerArea", SetAction_insert(pcRoute.container));
    			rptAddDataWithCache("t32commodityArea", SetAction_insert(pcRoute.commodity));
    			rptDisplayTable("t31containerArea");
    			rptDisplayTable("t32commodityArea");*/
    			//setDisplayConComm();

    			rutCloseDialog('b5-CopyOogArea');
    		}
     }

     function setEmptyconAndcomm(){
    	 var ArraycontainerArea = rptGetDataFromDisplayAll("t31containerArea");
		 var ArraycommodityArea = rptGetDataFromDisplayAll("t32commodityArea");

		 if(!isEmpty(ArraycontainerArea)){
			 rptClearDisplay("t31containerArea");
		 }
		if(!isEmpty(ArraycommodityArea)){
			rptClearDisplay("t32commodityArea");
		}
     }

   function validateCopyOOG(getshipType){
	   var status = rptGetDataFromSingleArea("d10MaintenanceHeader").status;
	   var rowId = (parseInt(isSelect) + parseInt(1));
	   var errorMsg = "";
	   var result = true;

	   if(status == 'C'){
		    result = false;
			errorMsg = "Inquiry is not applicable for quotation with status Final ";
	   }else if(getshipType.inqType != ""){
			   if(getshipType.oogBkkInq != ""){
				   if(getshipType.shipmentType != 'FCL' && getshipType.shipmentType != 'BBK'){
					   result = false;
					   errorMsg = "Inquiry is not applicable for LCL/ROR shipment ";
				   }else{
					   if(getshipType.inqType == "B"){
							if(getshipType.shipmentType == "FCL"){
								result = false;
								errorMsg = "For FCL Shipment, only OOG inquiry is applicable for corridor at row "+ rowId;
							}
				  		}else if(getshipType.inqType == "O"){
								if(getshipType.shipmentType == "BBK"){
									result = false;
									errorMsg = "For BBK Shipment, only BBK inquiry is applicable for corridor at row "+ rowId;
								}
				  		}
				   }
		  	 }else{
		  		 result = false;
				 errorMsg = "Inquiry type and BBK/OOG inquiry no required";
		  	 }

	   }


			if(!result){
				rutCloseDialog('b5-CopyOogArea');
				dialogGenericv3("Maintenance","Warning", errorMsg , "Ok");

			}

			return result;
   } 

   function GetDataCustomer(){
	   var DataCustomer = getCustomerTabOutput();
	   
	   return DataCustomer;
	}

   function GetDataDetentionDemurrage(){
	    var DataDetentionDemurrage = getDetentionDemurrageTabOutput();

		return DataDetentionDemurrage;
    }

   function GetDatacontainerAndcomm(){
	   	var getconAndcomm = getContainerCommodityTabOutput();
	   	var container = getconAndcomm.container;
	   	var commodity = getconAndcomm.commodity;
	   	var setcon = [];
	   	var setcomm = [];
		for(var i = 0;i < container.length; i++){
	   		if(container[i].action != 'd'){
	   			setcon.push({
	   				"containerSeqNo": container[i].containerSeqNo,
			        "shipmentType": container[i].shipmentType,
			        "polStatus": container[i].polStatus,
			        "size": container[i].size,
			        "type": container[i].type,
			        "rateType": container[i].rateType,
			        "fullQty": container[i].fullQty,
			        "emptyQty": container[i].emptyQty,
			        "dgQty": container[i].dgQty,
			        "reeferQty": container[i].reeferQty,
			        "oogQty": container[i].oogQty,
			        "voidSlot": container[i].voidSlot,
			        "teus": container[i].teus,
			        "bundle": container[i].bundle,
			        "podStatus": container[i].podStatus,
			        "oogInqRef": container[i].oogInqRef,
			        "oh": container[i].oh,
			        "owl": container[i].owl,
			        "owr": container[i].owr,
			        "ola": container[i].ola,
			        "olf": container[i].olf
	   			});
	   		}

	   	}



	   	for(var x = 0;x < commodity.length; x++){
	   		if(commodity[x].action != 'd'){
	   			setcomm.push({
		   			"commoditySeqNo": commodity[x].commoditySeqNo,
			        "shipmentType": commodity[x].shipmentType,
			        "commCode": commodity[x].commCode,
			        "commGroup": commodity[x].commGroup,
			        "rateType": commodity[x].rateType,
			        "reefer": commodity[x].reefer,
			        "imdgClass": commodity[x].imdgClass,
			        "unno": commodity[x].unno,
			        "variant": commodity[x].variant,
			        "portClass": commodity[x].portClass,
			        "oogFlag": commodity[x].oogFlag,
			        "measurement": commodity[x].measurement,
			        "weight": commodity[x].weight,
			        "rorArea": commodity[x].rorArea,
			        "pkgs": commodity[x].pkgs,
			        "remarks": commodity[x].remarks
	   			});
	   		}
	   	}


	   	return {
	   		"container" : setcon
	   	   ,"commodity" : setcomm
	   	}

   	}

   function GetDatacontainerAndcommDG(){
	   	var getconAndcomm = getContainerCommodityTabOutput();
	   	var container = getconAndcomm.container;
	   	var commodity = getconAndcomm.commodity;
	   	var setcon = [];
	   	var setcomm = [];
		for(var i = 0;i < container.length; i++){
	   		if(container[i].action != 'd'){
	   			setcon.push({
	   				"containerSeqNo": container[i].containerSeqNo,
			        "shipmentType": container[i].shipmentType,
			        "polStatus": container[i].polStatus,
			        "size": container[i].size,
			        "type": container[i].type,
			        "rateType": container[i].rateType,
			        "fullQty": container[i].fullQty,
			        "emptyQty": container[i].emptyQty,
			        "dgQty": container[i].dgQty,
			        "reeferQty": container[i].reeferQty,
			        "oogQty": container[i].oogQty,
			        "voidSlot": container[i].voidSlot,
			        "teus": container[i].teus,
			        "bundle": container[i].bundle,
			        "podStatus": container[i].podStatus,
			        "oogInqRef": container[i].oogInqRef,
			        "oh": container[i].oh,
			        "owl": container[i].owl,
			        "owr": container[i].owr,
			        "ola": container[i].ola,
			        "olf": container[i].olf
	   			});
	   		}

	   	}



	   	for(var x = 0;x < commodity.length; x++){
	   		if(commodity[x].action != 'd'){
	   			setcomm.push({
		   			"commoditySeqNo": commodity[x].commoditySeqNo,
			        "shipmentType": commodity[x].shipmentType,
			        "commCode": commodity[x].commCode,
			        "commGroup": commodity[x].commGroup,
			        "rateType": commodity[x].rateType,
			        "reefer": commodity[x].reefer,
			        "imdgClass": commodity[x].imdgClass,
			        "unno": commodity[x].unno,
			        "variant": commodity[x].variant,
			        "portClass": commodity[x].portClass,
			        "oogFlag": commodity[x].oogFlag,
			        "measurement": commodity[x].measurement,
			        "weight": commodity[x].weight,
			        "rorArea": commodity[x].rorArea,
			        "pkgs": commodity[x].pkgs,
			        "remarks": commodity[x].remarks
	   			});
	   		}
	   	}


	   	return {
	   		"container" : setcon
	   	   ,"commodity" : setcomm
	   	}

  	}

   function validateExtenAndExpireButtion(ExpDate){
	   /*var ExpDate;
	   var Current = new Date();
	   if(ExpDate != undefined){
		   ExpDate = new Date(new Date(conCatDate($("#d10expDate").val())));
	   }else{
		  ExpDate = new Date(new Date(conCatDate(ExpDate)));
	   }

	   if(Current > ExpDate){
		   $('#d0-extendQuotation').show();
		   $('#d0-expireQuotation').show();

	   }else{
		  $('#d0-extendQuotation').hide();
		  $('#d0-expireQuotation').hide();
	   }*/
   }


   function validatebuttionAll_v2(){
	   var status = $("#d10status").val();
	   $('#h1-hold').hide();
	   $('#h1-unhold').hide();

	   $('#d0-importFromExcel').hide();

	   $('#d0-popupApproval').hide();
	   $('#d0-popupCreateInterim').hide();
	   $('#d0-QuotationDetails').hide();

	   if(getqtnNo != "" && getqtnNo != null){
		   $('#d0-Attechment').show();
	   }else{
		   $('#d0-Attechment').hide();
	   }
	   console.log("validatebuttionAll_v2 >> IsFsc : "+ IsFsc +", Isview : "+ Isview + " , status : "+status+ " , Appoval : "+Appoval+ "ee_wait_approval : "+getee_wait_approval)
	   if(Isview){//Mode View
		   if(status == 'H'){// Hold
			   $('#h1-hold').hide();
			   $('#h1-save').hide();
			   $('#h1-tool').show();

			   $('#h1-unhold').show();
			   $('#h1-cancel').show();
			   $('#h1-back').show();
			   $('#h1-help').show();
			   $('#h1-close').hide();

			  // $('#d0-importFromExcel').hide();
			 //  $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();

			   //$('#d0-QuotationDetails').show();
			   $('#h1-final').hide();

		   }else if(status == 'C'){// Final
			   if(Appoval == 'EE'){
				   $('#h1-final').hide();
				   $('#h1-cancel').hide();
				   $('#h1-back').show();
				   $('#h1-help').show();
				   $('#h1-close').hide();

				   $('#h1-save').hide();
				   $('#h1-hold').hide();
				   $('#h1-tool').show();
				   $('#h1-unhold').hide();

				//   $('#d0-importFromExcel').hide();
				 //  $('#d0-Attechment').hide();
				   $('#d0-popupApproval').show();
				   $('#d0-popupCreateInterim').hide();

				  // $('#d0-QuotationDetails').show();

			   }else{
				   $('#h1-save').hide();
				   $('#h1-hold').hide();
				   $('#h1-tool').show();

				   $('#h1-cancel').show();
				   $('#h1-back').show();
				   $('#h1-help').show();
				   $('#h1-close').hide();

				 //  $('#d0-importFromExcel').hide();
				 //  $('#d0-Attechment').hide();
				   $('#d0-popupApproval').show();
				   $('#d0-popupCreateInterim').show();
				  // $('#d0-QuotationDetails').show();
			   }

			if(getee_wait_approval == 'N'){
				$('#d0-extendQuotation').show();
				$('#d0-expireQuotation').show();
			}else if(getee_wait_approval == 'Y'){
				$('#d0-extendQuotation').hide();
				$('#d0-expireQuotation').hide();
			}

		/*	console.log("Appoval :: "+Appoval)
			if(Appoval == 'EE'){
				$('#d0-extendQuotation').hide();
				$('#d0-expireQuotation').hide();
			}
		   	   */
		   }else if(status == 'L'){// Cancel
			   $('#h1-final').hide();
			   $('#h1-back').show();
			   $('#h1-help').show();
			   $('#h1-close').hide();

			   $('#h1-hold').hide();
			   $('#h1-save').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();
			   $('#h1-cancel').hide();

			  // $('#d0-importFromExcel').hide();
			  // $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').hide();


			  // $('#d0-QuotationDetails').show();
		   }else if(status == 'O'){
			   $('#h1-save').show();
			   $('#h1-cancel').show();
			   $('#h1-close').show();
			   $('#h1-back').show();
			   $('#h1-hold').show();
			   $('#h1-help').show();

			   $('#h1-final').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();

			  // $('#d0-importFromExcel').show();
			  // $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();
			 //  $('#d0-QuotationDetails').show();

			}else if(status == 'Q'){
			   $('#h1-save').show();
			   $('#h1-back').show();
			   $('#h1-help').show();
			   $('#h1-close').show();
			   $('#h1-cancel').show();

			   $('#h1-hold').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();


			  // $('#d0-importFromExcel').hide();
			 //  $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();

			   //$('#d0-QuotationDetails').show();
			   $('#h1-final').hide();

		   }else if(status == 'S'){
			   $('#h1-final').show();
			   $('#h1-cancel').show();
			   $('#h1-back').show();
			   $('#h1-help').show();

			   $('#h1-close').hide();
			   $('#h1-save').hide();
			   $('#h1-hold').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();

			 //  $('#d0-importFromExcel').hide();
			//   $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();

			   $('#d0-QuoteAgain').hide();
			   //$('#d0-QuotationDetails').show();

			 }else if(status == 'N'){// Need Approval
			   if(Appoval == 'BBK'){
					  $('#h1-save').show();
					  $('#h1-Finalize').show();
					  $('#h1-Decline').show();
					  $('#h1-close').hide();
					  $('#h1-cancel').show();
					  $('#h1-tool').show();
					 // $('#d0-QuotationDetails').show();
					  $('#d0-popupApproval').show();
				  }else{
					   $('#h1-final').hide();
					   $('#h1-cancel').hide();
					   $('#h1-back').show();
					   $('#h1-help').show();
					   $('#h1-close').hide();

					   $('#h1-save').hide();
					   $('#h1-hold').hide();
					   $('#h1-tool').show();
					   $('#h1-unhold').hide();

					//   $('#d0-importFromExcel').hide();
					//   $('#d0-Attechment').hide();
					   $('#d0-popupApproval').show();
					   $('#d0-popupCreateInterim').hide();

					  // $('#d0-QuotationDetails').show();
				  }
			}
	   
		   if(!IsFsc){
			   $('#h1-hold').hide();
			   $('#h1-save').hide();
			   $('#h1-close').hide();
			   $('#h1-cancel').hide();
		   }
		  

	   }else{
		   if(status == 'O'){
			   $('#h1-save').show();
			   $('#h1-cancel').show();
			   $('#h1-close').show();
			   $('#h1-back').show();
			   $('#h1-hold').show();
			   $('#h1-help').show();

			   $('#h1-final').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();

			 //  $('#d0-importFromExcel').show();
			 //  $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();
			  // $('#d0-QuotationDetails').show();

			}
		   /*else if(status == 'H'){
			   $('#h1-hold').hide();
			   $('#h1-save').hide();
			   $('#h1-tool').hide();

			   $('#h1-unhold').show();
			   $('#h1-cancel').show();
			   $('#h1-back').show();
			   $('#h1-help').show();
			   $('#h1-close').show();

			   $('#d0-importFromExcel').hide();
			   $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();

			//   $('#d0-QuotationDetails').show();
			   $('#h1-final').hide();

			}*/
		   else if(status == 'Q'){
			   $('#h1-save').show();
			   $('#h1-back').show();
			   $('#h1-help').show();
			   $('#h1-close').show();
			   $('#h1-cancel').show();

			   $('#h1-hold').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();


			 //  $('#d0-importFromExcel').hide();
			//   $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();

			  // $('#d0-QuotationDetails').show();
			   $('#h1-final').hide();

		   }else if(status == 'S'){
			   $('#h1-final').show();
			   $('#h1-save').show();
			   $('#h1-cancel').show();
			   $('#h1-back').show();
			   $('#h1-help').show();

			   $('#h1-close').hide();

			   $('#h1-hold').hide();
			   $('#h1-tool').show();
			   $('#h1-unhold').hide();

			//   $('#d0-importFromExcel').hide();
			 //  $('#d0-Attechment').hide();
			   $('#d0-popupApproval').show();
			   $('#d0-popupCreateInterim').show();

			   $('#d0-QuoteAgain').hide();
			 //  $('#d0-QuotationDetails').show();
			 }
	   }
   }



   function lookuponblurepor(rowId,Isvalue){
		if(Isvalue != ""){

			 var rawRowNumber=rowId.substring(rowId.lastIndexOf('-'));

			    getResultAjax("WS_QTN_GET_PREFER_DEPOT",
							{
				   				userData : userData,
				   				por_del_flag : "POR",
				   				point_code : Isvalue
							}).done(function(response){
								 if(response.resultCode == "200"){
							   			rutSetElementValue("t10pickupDepot"+rawRowNumber,response.resultContent);
							   	}else if(response.resultCode == '401'){

									window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

								}else{
									dialogGeneric("Warning", response.resultMessage, "Ok");
								}
							});
		}
	}

   function lookuponbluredel(rowId,Isvalue){
	   if(Isvalue != ""){

		   var rawRowNumber=rowId.substring(rowId.lastIndexOf('-'));

		   getResultAjax("WS_QTN_GET_PREFER_DEPOT",
						{
			   				userData : userData,
			   				por_del_flag : "DEL",
			   				point_code : Isvalue
						}).done(function(response){
							 if(response.resultCode == "200"){
						   			rutSetElementValue("t10dropOffDepot"+rawRowNumber,response.resultContent);
						   	 }else if(response.resultCode == '401'){

									window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

							}else{
								dialogGeneric("Warning", response.resultMessage, "Ok");
							}
						});
	   }

   }

   function isSetPorPol(rowId,value){

	    var rowid =rowId.substring(rowId.lastIndexOf('-'));

		var pro = rutGetElementValue("t10por"+rowid);
		var pol = rutGetElementValue("t10pol"+rowid);

		if(pol == ""){
			rutSetElementValue("t10pol"+rowid, value);
		}

		if(pro == ""){
			rutSetElementValue("t10por"+rowid, value);
		}

	}

   function isSetPodDel(rowId,value){

	   var rowid = rowId.substring(rowId.lastIndexOf('-'));

		var pod = rutGetElementValue("t10pod"+rowid);
		var del = rutGetElementValue("t10del"+rowid);

		if(pod == ""){
			rutSetElementValue("t10pod"+rowid, value);
		}

		if(del == ""){
			rutSetElementValue("t10del"+rowid, value);
		}

	}

   function ischeckfscMaintenance(){
	  	var HedManRow = rptGetDataFromSingleArea("d10MaintenanceHeader");
		var AreaShipDet = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
			removeGeneralSearchButton("d10contactPartyCode");
			removeGeneralSearchButton("d10opCode");
			removeGeneralSearchButton("d10roeTable");
			$.each(HedManRow, function(k, v) {
				setCustomTagProperty("d10"+k, {dis:true});
			});

			for (var x = 0; x < AreaShipDet.length; x++) {

				removeGeneralSearchButton("t10term-"+x);
				removeGeneralSearchButton("t10por-"+x);
				removeGeneralSearchButton("t10pol-"+x);
				removeGeneralSearchButton("t10pot1-"+x);
				removeGeneralSearchButton("t10pot2-"+x);
				removeGeneralSearchButton("t10pot3-"+x);
				removeGeneralSearchButton("t10pod-"+x);
				removeGeneralSearchButton("t10del-"+x);
				removeGeneralSearchButton("t10porHaulLoc-"+x);
				removeGeneralSearchButton("t10delHaulLoc-"+x);
				removeGeneralSearchButton("t10supplier-"+x);
				removeGeneralSearchButton("t10supplierLoc-"+x);
				removeGeneralSearchButton("t10pickupDepot-"+x);
				removeGeneralSearchButton("t10dropOffDepot-"+x);
				removeGeneralSearchButton("t10oogBkkInq-"+x);
				$("#t10coppy-"+x).remove();
				$("#t10selectDeleteLabel-"+x).remove();


				$.each(AreaShipDet[x], function(k, v) {
					setCustomTagProperty("t10"+k+"-"+x, {dis:true});
				});

			}
			$("#_t10deleteBtn").remove();
			$("#_t10addBtn").remove();

	}


   function IsClearChange(){
	   var chargeRow = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
		if(chargeRow.length > 0){
			rptClearDisplay("t52ChargesDetailsArea");
		}

   }

   function ClearRouting(){
	   var routingRow = rptGetDataFromDisplayAll("t40rountingDetailArea");
		if(routingRow.length > 0){
			rptClearDisplay("t40rountingDetailArea");
			clearfiled();
		}
   }

   function IsClearChangeAndDnd(isId,isValue){
	    ClearChange();
		ClearDisplayDNDOnchange();

		ISsetisGetDnd('Y');
   }

   function attechment(){
	   var title = "File";
	   var content = "Copy as one-off quotation?";
	   var UserData = JSON.stringify(userData);
	   var Gstatus = rutGetElementValue("d10status");
	   sessionStorage.setItem("userData", UserData);
	   PopupCenter("../popup/attechmentFile.jsp?title="+title+"&qnt="+getqtnNo+'&status='+Gstatus,'_blank','1000','600');
   }


   function attechmentExpir(){
	 window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
   }



</script>