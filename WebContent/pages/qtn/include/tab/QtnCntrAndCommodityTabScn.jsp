<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<%-- Customers Header --%>
<rcl:area id="d30QuotationHeader" classes="div(container-fluid)" title="Quotation Header" collapsible='Y'>
	<div class="row">
		<rcl:text id="d30quotationNo" classes="div(col-md-2) ctr(dtlField)"
			label="Quotation#" options="disabled" check="upc" />
		<rcl:text id="d30ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" options="disabled" check="upc" />
		<%-- rcl:select id="d30status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/--%>
		<rcl:select id="d30status" label="Status"
					classes="div(col-md-2) ctr(dtlField)"
					options="data-rdo"
					optionsList="Open Need_Approval Quote_Again Close Final Hold Cancel"
					valueList="O N Q S C H L" />
		<rcl:text id="d30slm" classes="div(col-md-2) ctr(dtlField)"
			label="Slm#" options="disabled" check="upc" />
		<rcl:text id="d30qtdBy" classes="div(col-md-2) ctr(dtlField)"
			label="QtdBy" options="disabled" check="upc" />
		<rcl:text id="d30pos" classes="div(col-md-2) ctr(dtlField)"
			label="POS" options="disabled" check="upc" />
	</div>
</rcl:area>
<rcl:area id="t30RoutingHeader" classes="div(container-fluid)" title="Routing List" collapsible='Y' areaMode="table">
<div id="routhingScroll">
	<div id="t30row" class="row tblRow t30row">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t30row');" class="row">
				<div class="col-md-12 container-fluid">
					<div class="row">
						<rcl:text id="t30porHaulLoc" classes="div(col-md-2) ctr(dtlField)" label="POR Haul. Loc." options="disabled" />
						<rcl:text id="t30por" classes="div(col-md-1)" label="POR"  options="disabled" check="upc" />
						<rcl:text id="t30pol" classes="div(col-md-1)" label="POL" options="disabled" />
						<rcl:text id="t30pot1" classes="div(col-md-1)" label="POT1" options="disabled" check="upc" />
						<rcl:text id="t30pot2" classes="div(col-md-1)" label="POT2" options="disabled" check="upc" />
						<rcl:text id="t30pot3" classes="div(col-md-1)" label="POT3" options="disabled" check="upc" />
						<rcl:text id="t30pod" classes="div(col-md-1)" label="POD" options="disabled" />
						<rcl:text id="t30del" classes="div(col-md-1)" label="DEL" options="disabled" check="upc" />
						<rcl:text id="t30delHaulLoc" classes="div(col-md-2)" label="DEL Haul. Loc." options="disabled" check="upc" />
						<rcl:select id="t30socCoc"
										label="SOC/COC"
										classes="div(col-md-1) ctr(dtlField)"
										selectTable="SocCoc"
										options="data-dis"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</rcl:area>


<rcl:area id="t31containerArea" classes="div(container-fluid)" title="Container Details" collapsible='Y' areaMode="table">
<%--div id="ContainerDetailsScroll"--%>
	<div id="t31row" class="row tblRow t31row" style="padding-bottom: 0px;">
		<div class="container-fluid" style="margin-left: 10px;">
			<div onclick="onselectBG('#-RowId-#','t31row')" class="row"  style="padding-bottom: 0px;">
				<div class="col-md-12 col-lg-6" style="padding-left: 0px;">
					<div class="row row-Container" style="padding-left: 0px;">
					<input id="t31containerSeqNo" type="number" class="tblField" style="display:none;"  />
					<%-- input id="t31olf" type="text" class="tblField" style="display:none;"  /--%>
						<rcl:text id="t31shipmentType" classes="div(col-md-2 pl-1 pr-0) ctr(dtlField)" label="Shipment Type" options="disabled" check="len(3)"/>
						<rcl:select id="t31polStatus" label="POL Status" classes="div(col-md-2 pl-1 pr-0) ctr(dtlField)" selectTable="PortStatus"  check="req" defaultValue="L"/>
						<%-- rcl:select id="t31size" label="Size" classes="div(col-1x5 pl-1 pr-0) ctr(dtlField)" selectTable="EqSize" options="onchange='changeTeu(this.id)'"/--%>
						<%-- rcl:select id="t31size" label="Size" classes="div(col-1x5 pl-1 pr-0) ctr(dtlField)"
						optionsList="0 20 40 45"
						valueList="0 20 40 45"
						options="onchange='changeTeu(this.id);  changeTeuvalue(this.id); calculateTEUs(this.id,this.value)'"
						defaultValue="0"/--%>
						<rcl:select id="t31size" label="Size" classes="div(col-1x5 pl-1 pr-0) ctr(dtlField)"
						optionsList="0 20 40 45"
						valueList="0 20 40 45"
						options="onchange='calculateTEUs(this.id,this.value); IsClearChangeAndDnd();'"
						defaultValue="0"/>

						<rcl:text id="t31type" classes="div(col-1x5 pl-1 pr-0) ctr(dtlField)" label="Type"  check="len(2)"
						lookup="tbl(vrl_equipment_type)
						  rco(CODE)
						  rid(t31type#-RowId-#)
						  sco(CODE*main)
						  sva(t31type#-RowId-#)
						  sop(LIKE)"
						  options="onblur=\"changeTeuV2(this.id); isReefer(this.id); copyQuantity(this.id,'Y'); isFlatRack(this.id); ClearChange();\" onchange=\"(this.id); isReefer(this.id); copyQuantity(this.id,'Y'); isFlatRack(this.id); IsClearChangeAndDnd();\" onkeyup=\"upc();\""
						 />
						 <%-- options="onblur='changeTeu(this.id);' onchange='changeTeu(this.id)' onkeyup='upc();'"/--%>




						<div class=" col-md-2 pl-1 pr-0">
							<div class="cssinline">
								<div class=" pl-0 pr-1 ml-0">
									<label class="rcl-standard-font " for="t31rateType-0">Rate Type</label>
									<rcl:select id="t31rateType" classes="ctr(dtlField)"
									options="onchange=\"onChangeRateTypeCustom(this.id,this.value); calculateTEUs(this.id,this.value); copyQuantity(this.id,'Y'); IsClearChangeAndDnd('','')\""
									selectTable="RateTypeEquip" defaultValue="N" check="req"/>
								</div>
								<div style="padding-top: 10px;">
								<input onclick="opendetail(this.id)" disabled class="FormBtnClrHelp" type="button" value="+" id="detailcoppy" name="detailcoppy">
							    	<%-- a id="detailcoppy" onclick="opendetail(this.id)" class="pointer"><em class="fa fa-plus" ></em></a--%>
							    </div>
							</div>
						</div>

						<rcl:number id="t31fullQty" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Full Qty" check="dec(3,0)" defaultValue="1"
						options="onblur=\"defaultField(this.id,this.value,0); copyQuantity(this.id,'Y'); calculateTEUs(this.id,this.value); ClearChange();\""/> <%-- options="onblur='changeTeuvalue(this.id)'"/--%>
						<rcl:number id="t31emptyQty" classes="div(col-md-2 pl-1 pr-0) ctr(dtlField)"  label="Empty Qty" check="dec(3,0)" defaultValue="0"
						options="onblur='calculateTEUs(this.id,this.value); ClearChange();'"/> <%-- options="onblur='changeTeuvalue(this.id)'"/--%>

					</div>
				</div>

				<div class="col-md-12 col-lg-6">
					<div class="row">
					<rcl:number id="t31dgQty" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)"  label="DG Qty." check="dec(3,0)" defaultValue="0" />
					<rcl:number id="t31reeferQty" classes="div(col-md-2 pl-1 pr-0) ctr(dtlField)"  label="Reefer Qty." check="dec(3,0)" defaultValue="0" />
					<rcl:number id="t31oogQty" classes="div(col-1x25 pl-1 pr-0) ctr(dtlField)"  label="OOG Qty" check="dec(3,0)" defaultValue="0" />
					<rcl:number id="t31voidSlot" classes="div(col-1x25 pl-1 pr-0) ctr(dtlField)"  label="Void Slots" check="dec(3,0)" defaultValue="0"
					options="onblur='calculateTEUs(this.id,this.value)'"/>
					<%-- options="onblur='changeTeuvalue(this.id)'"/--%>
					<rcl:number id="t31teus" classes="div(col-1x25 pl-1 pr-0) ctr(dtlField)"  label="TEU's" check="dec(6,2)"  defaultValue="0.00" options="disabled" />
					<rcl:number id="t31bundle" classes="div(col-md-2 pl-1 pr-0) ctr(dtlField)" label="Bundle" check="dec(8,0)" defaultValue="0" options="disabled onblur='calculateTEUs(this.id,this.value); ClearChange();'"/>
					<rcl:select id="t31podStatus" label="POD Status" classes="div(col-md-2 pl-1 pr-0) ctr(dtlField)" name="POD Status"
								selectTable="PortStatus" check="req" defaultValue="L" />

						<div id="t31selectDeleteLabel" class="col-md-1 pl-1 pr-0 mb-2" style="text-align: center;">
							<label>Delete</label>
							<input id="t31selectDelete" type="checkbox" class="tblField mt-2" />
						</div>

						<rcl:text id="t31oogInqRef" name="Inq Seq" options="style='display:none;'" />
						<rcl:number id="t31oh" name="Oh" classes="ctr(dtlField)" options="style='display:none;'" />
						<rcl:number id="t31owl" name="Owl" classes="ctr(dtlField)" options="style='display:none;'" />
						<rcl:number id="t31owr" name="Owr" classes="ctr(dtlField)" options="style='display:none;'" />
						<rcl:number id="t31ola" name="Ola" classes="ctr(dtlField)" options="style='display:none;'" />
						<rcl:number id="t31olf" name="Olr" classes="ctr(dtlField)" options="style='display:none;'" />

					</div>
				</div>
			</div>
		</div>
	</div>
<%--/div--%>

	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t31addBtn" type="button" class="rcl-standard-button" onclick="newContainer()">Add</button>
			<button id="_t31deleteBtn" type="button" class="rcl-standard-button" onclick="deleteContainer()">Delete</button>
		</div>
	</div>

	<%-- end of table table--%>
</rcl:area>

<%-- End of table customers list area --%>

<rcl:area id="t32commodityArea" classes="div(container-fluid)" title="Commodity Details" collapsible='Y' areaMode="table">
<%--div id="CommodityDetailsScroll"  --%>
	<div id="t32row" class="row tblRow t32row" style="padding-bottom: 0px;">
		<div class="container-fluid" style="margin-left: 10px;">

		<div onclick="onselectBG('#-RowId-#','t32row')">
			<div class="row row-Commodity"  style="padding-left: 0px;">
			<input id="t32commoditySeqNo" type="number" class="tblField" style="display:none;"  />
			<rcl:text id="t32shipmentType" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Shipment Type" options="disabled" check="len(3)"/>
			<rcl:text id="t32commCode" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Comm Code."
			options="onblur='ClearChange();'  onchange='ClearChange();'"
			check="req len(15)"
				  lookup="tbl(VRL_COMMODITY)
						  rco(CODE GROUP_CODE)
						  rid(t32commCode#-RowId-# t32commGroup#-RowId-#)"
				  autoLookup="sco(CODE)
		  			  sva(t32commCode#-RowId-#)"/>

			<rcl:text id="t32commGroup" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Comm Group." check="req len(5)"/>
			<rcl:select id="t32rateType" label="Rate Type" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)"
			selectTable="RateType" check="req"  defaultValue="N"
			options="onchange='setCommodity(this.id); ClearChange();'"/>
			<div class="col-0x50 pl-1 pr-1" style="text-align: center;">
				<label>Reefer</label>
				<div>
					<input id="t32reefer" type="checkbox" class="tblField" style="margin-top: 5px;"/>
				</div>
			</div>

			<rcl:text id="t32imdgClass" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="IMDG Class" check="len(5)"/>

			<rcl:text id="t32unno" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="UNNO" check="len(4)"/>
			<rcl:text id="t32variant" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Variant" check="len(1)"
					lookup="tbl(vrl_unno)
						  rco(UNNO IMDG VARIANT)
						  rid(t32unno#-RowId-# t32imdgClass#-RowId-# t32variant#-RowId-#) "  />
			<!-- rcl:text id="t32portClass" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Port Class" check="len(5) upc"
				lookup="tbl(VRL_PORT_CLASS)
						  rco(UNNO IMDG VARIANT PORT_CLASS_CODE)
						  rid(t32unno#-RowId-# t32imdgClass#-RowId-# t32variant#-RowId-# t32portClass#-RowId-#) " /-->
			<div class=" col-md-1 pl-1 pr-0">
						<label for="t32portClass-0">Port Class</label>
						<div class="rcl-standard-input-wrapper">
						<div style="padding-right:20px">
						<input id="t32portClass" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField dtlField" maxlength="5" data-check="upc " data-ct="bt-text" required="">
						</div>
							<i class="fa fa-search" data-onclick="portClassLookup('t32unno#-RowId-# t32imdgClass#-RowId-# t32variant#-RowId-# t32portClass#-RowId-#','UNNO IMDG VARIANT PORT_CLASS_CODE','pol,pot1,pot2,pot3,pod');" onclick="portClassLookup('t32unno#-RowId-# t32imdgClass#-RowId-# t32variant#-RowId-# t32portClass#-RowId-#','UNNO IMDG VARIANT PORT_CLASS_CODE','pol,pot1,pot2,pot3,pod');"></i>
						</div>
					</div>

			<div class="col-0x50 pl-1 pr-1" style="text-align: center;">
				<label>OOG</label>
				<div>
					<input id="t32oogFlag" type="checkbox" class="tblField" style="margin-top: 5px;"/>
				</div>
			</div>

			<rcl:number id="t32measurement" classes="div(col-md-1 pl-2 pr-0) ctr(dtlField)" label="Measuement" check="dec(14,4)" defaultValue="0.0000"/>
			<rcl:number id="t32weight" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="Weight" check="dec(12,2)" defaultValue="0.00"/>
			<rcl:number id="t32rorArea" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="ROR Area" check="dec(8,2)" defaultValue="0.00"/>
			<rcl:number id="t32pkgs" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="Pkgs" check="dec(6,0)" defaultValue="0"/>
			<div id="t32selectDeleteLabel" class="col-0x50 pl-1 pr-0 mb-2" style="text-align: center;">
				<label>Delete</label>
				<div>
					<input id="t32selectDelete" type="checkbox" class="tblField mt-2" />
				</div>
			</div>


			</div>
			</div>
		</div>
	</div>
<%--/div--%>
	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t32addBtn" type="button" class="rcl-standard-button" onclick="newCommodity()">Add</button>
			<button id="_t32deleteBtn" type="button" class="rcl-standard-button" onclick="deleteCommodity()">Delete</button>
			<%-- button id="_t32deleteBtn" type="button" class="rcl-standard-button" onclick="getContainerCommodityTabOutput()">Delete</button--%>
		</div>
	</div>
</rcl:area>

<rcl:dialog id="b9-detailcopyArea" width="1000"
	title="OOG Dimension Details" buttonList="" onClickList="">

	<rcl:area id="d71detailcopyArea" classes="div(container-fluid)"
		useHeader="N"  areaMode="table">

		<div class=""
			style="margin-left: 0px; margin-right: 0px;">
			<div class="row pl-3 pr-2">
				<rcl:number id="d71oh" classes="div(col-md-2) ctr(dtlField)" label="OH(c.m.)" options="disabled"/>

				<rcl:number id="d71owl" classes="div(col-md-2) ctr(dtlField)" label="OWL(c.m.)" options="disabled"/>

				<rcl:number id="d71owr" classes="div(col-md-2) ctr(dtlField)" label="OWR(c.m.)" options="disabled"/>

				<rcl:number id="d71ola" classes="div(col-md-2) ctr(dtlField)" label="OLA(c.m.)" options="disabled"/>

				<rcl:number id="d71olf" classes="div(col-md-2) ctr(dtlField)" label="OLF(c.m.)" options="disabled"/>

				<rcl:number id="d71voidSlot" classes="div(col-md-2) ctr(dtlField)" label="Void Slots" options="disabled"/>


			</div>
		</div>


	</rcl:area>
</rcl:dialog>

<script type="text/javascript">
var OrgDataContainerArea = [];
var OrgDataCommodityArea = [];
var IshaveDataCtn = false;
$(document).ready(function() {
	rptTableInit("t30RoutingHeader", { disabledSortControl: true });
	rptTableInit("t31containerArea", { disabledSortControl: true });
	rptTableInit("t32commodityArea", { disabledSortControl: true });



});


function opendetail(isid){
	var getrowid = splitId(isid,'-');
	var getIscopy = rptGetDataFromDisplay("t31containerArea",'t31row-'+getrowid);
	//console.log("getIscopy "+  JSON.stringify(getIscopy));
	if(!Isempty(getIscopy)){
		rutOpenDialog('b9-detailcopyArea');
		rptSetSingleAreaValues("d71detailcopyArea",getIscopy);
	}
	//
	//rptSetSingleAreaValues("d71detailcopyArea",getIscopy);
}


function SetCntrAndCommTab(isValue){
	console.log("SetCntrAndCommTab");
	var HedConRow = rptGetDataFromDisplayAll("t31containerArea");
	//setDisplayConComm();
	if(HedConRow.length > 0){
		for (var x = 0; x < HedConRow.length; x++) {
			var getrow = HedConRow[x].rowId.split("-");
			HedConRow[x].shipmentType = isValue;
			/*rutSetElementValue("t31shipmentType-"+getrow[1], isValue);*/
			$("#t31shipmentType-"+getrow[1]).val(isValue);

			//if($("#t31shipmentType-"+getrow[1]).val() != isValue){
				setshipmenttype("t31containerArea","t31shipmentType",HedConRow[x].rowId);
			//}
		}
		// console.log("t31containerArea "+  JSON.stringify(HedConRow));
		 rptClearDisplay("t31containerArea");
	}


	var HedCommRow = rptGetDataFromDisplayAll("t32commodityArea");
	if(HedCommRow.length > 0){
		for (var i = 0; i < HedCommRow.length; i++) {
			var getrow = HedCommRow[i].rowId.split("-");
			/*rutSetElementValue("t32shipmentType-"+getrow[1], isValue);*/
			HedCommRow[i].shipmentType = isValue;
			$("#t32shipmentType-"+getrow[1]).val(isValue);

		}

		 rptClearDisplay("t32commodityArea");
	}

	var data = {
			container : HedConRow,
			commodity : HedCommRow
	}

	//console.log("SetCntrAndCommTab "+JSON.stringify(data));
	fetchDataContainerAndCommodityTab(data);
}

function ondisplayOOg(Isid,copyoog){
	//var rowid = splitId(Isid);
	//var getIscopy = rptGetDataFromDisplay("t31containerArea",'t31row-'+rowid).isCopyOog;
	var rowid = Isid;
	var getIscopy = copyoog;
	if(getIscopy != undefined){
		if(getIscopy == 'Y'){
			$("#detailcoppy-"+rowid).addClass('FormBtnClrHelp-active');
			setCustomTagProperty("detailcoppy-"+rowid, {dis:false});
		}else if(getIscopy == 'N'){
			setCustomTagProperty("detailcoppy-"+rowid, {dis:true});
		}
	}else{
		setCustomTagProperty("detailcoppy-"+rowid, {dis:true});
	}

}

function setConAndcommCopyOog(pcRoute){
	//console.log("setConAndcommCopyOog "+JSON.stringify(pcRoute));
	var setcontainer = SetAction_IsChkCopyOog(pcRoute.container);
	var setcommodity = SetAction_IsChkCopyOog(pcRoute.commodity);

	rptAddDataWithCache("t31containerArea", SetAction_insert(setcontainer));
	rptAddDataWithCache("t32commodityArea", SetAction_insert(setcommodity));
	rptDisplayTable("t31containerArea");
	rptDisplayTable("t32commodityArea");

	//console.log("t31containerArea "+JSON.stringify(rptGetDataFromDisplayAll("t31containerArea")));
	//console.log("t32commodityArea "+JSON.stringify(rptGetDataFromDisplayAll("t32commodityArea")));
}

function SetAction_IsChkCopyOog(object){
	if(object.length > 0){
		for(var i = 0; i < object.length; i++){
			object[i].isChkCopyOog = 'Y';
		}
	}
	return object;
}



function ischeckfscCrtComm(){
	var HedConRow = rptGetDataFromDisplayAll("t31containerArea");
	var HedCommRow = rptGetDataFromDisplayAll("t32commodityArea");

	for (var x = 0; x < HedConRow.length; x++) {
		removeGeneralSearchButton("t31type-"+x);
		$("#t31selectDeleteLabel-"+x).remove();
		$.each(HedConRow[x], function(k, v) {
			setCustomTagProperty("t31"+k+"-"+x, {dis:true});
			setCustomTagProperty("detailcoppy-"+x, {dis:true});
		});
	}
	$("#_t31deleteBtn").remove();
	$("#_t31addBtn").remove();

	for (var i = 0; i < HedCommRow.length; i++) {
		removeGeneralSearchButton("t32commCode-"+i);
		removeGeneralSearchButton("t32variant-"+i);
		removeGeneralSearchButton("t32portClass-"+i);
		$("#t32selectDeleteLabel-"+i).remove();
		$.each(HedCommRow[i], function(k, v) {
			setCustomTagProperty("t32"+k+"-"+i, {dis:true});
		});
	}
	$("#_t32deleteBtn").remove();
	$("#_t32addBtn").remove();
}


function fetchDataContainerAndCommodityTab(content){
	if(content.container.length > 0){ // case Edit
		IshaveDataCtn = true;
	}
	//set : t31containerArea
	rptAddDataWithCache("t31containerArea", SetAction_edit(content.container));
	rptDisplayTable("t31containerArea");
	perContainer = JSON.stringify(rptGetDataFromDisplayAll("t31containerArea"));
	curContainer = rptGetDataFromDisplayAll("t31containerArea");

	//set :
	rptAddDataWithCache("t32commodityArea", SetAction_edit(content.commodity));
	rptDisplayTable("t32commodityArea");
	setRowView("t31containerArea","t31containerSeqNo");
	setRowView("t32commodityArea","t32commoditySeqNo");
	var Getcon = rptGetDataFromDisplayAll("t31containerArea");
	var Getcomm = rptGetDataFromDisplayAll("t32commodityArea");

	//setDisplayConComm();

	if(Getcon.length > 0){
		for (var i = 0; i < Getcon.length; i++) {
			var getrow = Getcon[i].rowId.split("-");
			$('#t31size-'+getrow[1]).val(Getcon[i].size);
			changeTeu(Getcon[i].rowId,'edit');
			setselectorRateType(i);
			//set Cntr
			onChangeRateType("row-"+i,Getcon[i].rateType);
			changeTeuV2(Getcon[i].rowId);
			setpolStatus(i);
			$('#t31polStatus-'+getrow[1]).val(Getcon[i].polStatus);
			$('#t31podStatus-'+getrow[1]).val(Getcon[i].podStatus);

		}
	}

	if(Getcomm.length > 0){
		for (var x = 0; x < Getcomm.length; x++) {
			setCommodity(Getcomm[x].rowId);
		}
	}

	OrgDataContainerArea = JSON.stringify(rptGetDataFromDisplayAll("t31containerArea"));
	OrgDataCommodityArea  = JSON.stringify(rptGetDataFromDisplayAll("t32commodityArea"));

}



function setDisplayConComm(){

	var chargeRow = rptGetDataFromDisplayAll("t31containerArea");
		if(chargeRow.length > 0){
			for (var x = 0; x < chargeRow.length; x++) {
				var getrow = chargeRow[x].rowId.split("-");
				$('#t31size-'+getrow[1]).val(chargeRow[x].size);

			}
		}
}


function setCntrAndConRowViewForNewRow(rowId, idNum, index, data){
	//setCustomerRowView(rowId, idNum, index, data);
	setRowView("t31containerArea","t31containerSeqNo");
	setRowView("t32commodityArea","t32commoditySeqNo");

}

function newContainer() {
		if(IsChkShipUC()){
			rptInsertNewRowBefore("t31containerArea", 'end');
			var newRow = $("#t31containerArea").find(".tblRow").not(":last").last();
			var data = rptGetDataFromDisplay("t31containerArea", newRow.attr("id"));
			data.action = 'i';
			onselectBG(newRow.attr("id"),'t31row');
			rptForeachRow('t31containerArea', setCntrAndConRowViewForNewRow, false, ":last");
			setRowView("t31containerArea","t31containerSeqNo");
			checkRecodeScroll('t31containerArea','ContainerDetailsScroll','ContainerDetails-scroll',2);
			setshipmenttype("t31containerArea","t31shipmentType",newRow.attr("id"));
			//onChangeRateType(newRow.attr("id"),'N');
			var getrow = newRow.attr("id").split("-");
			setselectorRateType(getrow[1]);
			setpolStatus(getrow[1]);
		//	setfieldContener();
		ClearChange()
		}
	}

	function IsChkShipUC(){
		var result = true;
		var getship = $('#t10shipmentType-'+isSelect).val();
		if(getship == 'UC'){
			if(!IsChkcontainNotDelete()){
				dialogGeneric("Warning", "Only one size/type record is allowed for UC shipment type", "Yes");
				result = false;
			}
		}
		return result;
	}

	function IsChkcontainNotDelete(){
		var result = true;
		var getcont = rptGetDataFromDisplayAll("t31containerArea");
		/*if(!isEmpty(newArrayDeleteContainer)){
		for(var i = 0;i < newArrayDeleteContainer.length; i++){
		if(!isEmpty(newArrayDeleteContainer[i])){
			ArraycontainerArea.push(newArrayDeleteContainer[i]);
		}
	}
}*/     var i = 0;
		if(getcont.length > 0){
			for(var z = 0; z < getcont.length; z++){
				if(getcont[z].action != 'd'){
					i++;
				}
			}
		}

		if(i > 0){
			result = false;
		}

		return result;
	}

	function setshipmenttype(area,field,rowid){
		console.log("setshipmenttype :: ");
		var getrow = rowid.split("-");
		//changeTeu
		var getship = rptGetDataFromDisplayAll("t10shipTypeDetailArea");
		var getRoutingHeader = rptGetDataFromDisplayAll(area);
		//var HedConRow = rptGetDataFromDisplayAll("t31containerArea");

		function setvoidSlot(rowid){
			var getvoidS = 0;
			if(getRoutingHeader.length > 0){
				for(var z = 0; z < getRoutingHeader.length; z++){
					if(z == rowid){
						if(getRoutingHeader[z].voidSlot > 0){
							getvoidS = getRoutingHeader[z].voidSlot;
						}else{
							getvoidS = 0;
						}
					}

				}
			}else{
				getvoidS = 0;
			}

			return getvoidS;
		}
			var getship = $('#t10shipmentType-'+isSelect).val();

			rutSetElementValue(field+"-"+getrow[1], getship);
console.log("setshipmenttype :: "+getship);
				if(getship == 'BBK'){
					$('#t31voidSlot-'+getrow[1]).val(setvoidSlot(getrow[1]));
					setCustomTagProperty("t31dgQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31reeferQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31oogQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:false});
				}else if(getship == 'FCL'){
					$('#t31voidSlot-'+getrow[1]).val(setvoidSlot(getrow[1]));
					setCustomTagProperty("t31size-" + getrow[1], {dis:false});
					setCustomTagProperty("t31type-" + getrow[1], {dis:false});
					setCustomTagProperty("t31dgQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31reeferQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31oogQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:true});
				}else if(getship == 'LCL'){
					//console.log("LCL")
					$('#t31size-'+getrow[1]).val(setvoidSlot(getrow[1]));
					$('#t31type-'+getrow[1]).val("**");
					$('#t31voidSlot-'+getrow[1]).val(setvoidSlot(getrow[1]));
					setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:true});
					setCustomTagProperty("t31size-" + getrow[1], {dis:true});
					setCustomTagProperty("t31type-" + getrow[1], {dis:true});
					setCustomTagProperty("t31fullQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31dgQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31reeferQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31oogQty-" + getrow[1], {dis:true});
				}else if(getship == 'UC'){
					//console.log("UC")
					$('#t31size-'+getrow[1]).val(setvoidSlot(getrow[1]));
					$('#t31type-'+getrow[1]).val("**");
					$('#t31voidSlot-'+getrow[1]).val(1);
					setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:false});
					setCustomTagProperty("t31size-" + getrow[1], {dis:true});
					setCustomTagProperty("t31type-" + getrow[1], {dis:true});
					setCustomTagProperty("t31fullQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31dgQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31reeferQty-" + getrow[1], {dis:true});
					setCustomTagProperty("t31oogQty-" + getrow[1], {dis:true});
				}

			//changeTeu(getRoutingHeader[i].rowId);

	}


	function setselectorRateType(rawRowNumber){
		console.log("setselectorRateType");

		var shipmTypeId=document.activeElement.id;

		 var rateBasisId='t31rateType-'+rawRowNumber; //get id of rateBase element

	     var rateBasisElement=document.getElementById(rateBasisId);

	     var value = $('#t10shipmentType-'+isSelect).val();//rutGetElementValue('t10shipmentType-'+rawRowNumber);


	    if (value=='UC'){

			rateBasisElement.innerHTML='<option value="N">Normal</option><option value="D1">DG</option>';

         }

	    var contain = rptGetDataFromDisplayAll("t31containerArea");
        if(contain.length > 0){

       	 for (var c = 0; c < contain.length; c++) {
				$('#t31rateType-'+c).val(contain[c].rateType);
			  }

        }

	}



var newArrayDeleteContainer = [];
function deleteContainer(){
	setarrayDelet('t31containerArea',newArrayDeleteContainer);
	console.log("newArrayDeleteContainer :" + JSON.stringify(newArrayDeleteContainer));
	rptTryToDeleteSelectedRows('t31containerArea', "selectDelete", true);
	rptForeachRow('t31containerArea', setCntrAndConRowViewForNewRow, false, ":last");
	setRowView("t31containerArea","t31containerSeqNo");
	checkRecodeScroll('t31containerArea','ContainerDetailsScroll','ContainerDetails-scroll',2);
	ClearChange();
	checkClearRounting();
	

}
	function checkClearRounting(){
		var countCon = rptGetDataFromDisplayAll("t31containerArea");
		if(countCon.length < 1){
			ClearRouting();
		}
	}


function ClearDeleteContainer(){
	newArrayDeleteContainer = [];
	newArrayDeleteCommodity = [];
}

function newCommodity() {
	rptInsertNewRowBefore("t32commodityArea", 'end');
	var newRow = $("#t32commodityArea").find(".tblRow").not(":last").last();
	var data = rptGetDataFromDisplay("t32commodityArea", newRow.attr("id"));
	data.action = 'i';
	onselectBG(newRow.attr("id"),'t32row');
	rptForeachRow('t32commodityArea', setCntrAndConRowViewForNewRow, false, ":last");
	setRowView("t32commodityArea","t32commoditySeqNo");
	checkRecodeScroll('t32commodityArea','CommodityDetailsScroll','CommodityDetails-scroll',2);
	setshipmenttype("t32commodityArea","t32shipmentType",newRow.attr("id"))
	setCommodity(newRow.attr("id"));
	ClearChange();
	}
var newArrayDeleteCommodity = [];
function deleteCommodity(){
	setarrayDelet('t32commodityArea',newArrayDeleteCommodity);
	console.log("newArrayDeleteCommodity :" + JSON.stringify(newArrayDeleteCommodity));
	rptTryToDeleteSelectedRows('t32commodityArea', "selectDelete", true);
	rptForeachRow('t32commodityArea', setCntrAndConRowViewForNewRow, false, ":last");
	setRowView("t32commodityArea","t32commoditySeqNo");
	checkRecodeScroll('t32commodityArea','CommodityDetailsScroll','CommodityDetails-scroll',2);
	ClearChange();
}

function setfieldContener(){
	rutSetElementValue("t31bundle-0", "0");
	rutSetElementValue("t31dgQty-0", "0");
	rutSetElementValue("t31emptyQty-0", "0");
	rutSetElementValue("t31fullQty-0", "1");
	rutSetElementValue("t31oogQty-0", "0");
	rutSetElementValue("t31podStatus-0", "L");
	rutSetElementValue("t31polStatus-0", "L");
	rutSetElementValue("t31rateType-0", "N");
	rutSetElementValue("t31reeferQty-0", "0");
	//rutSetElementValue("t31shipmentType-0", "FCL");
	rutSetElementValue("t31size-0", "20");
	//rutSetElementValue("t31teus-0", "1.00");
	rutSetElementValue("t31type-0", "GP");
	rutSetElementValue("t31voidSlot-0", "0");
}

function setCommodity(rowid){
	var rowId = rowid.substring(rowid.lastIndexOf('-'));
	setrateType("t32rateType"+rowId,rowid);
	/*rutSetElementValue("t32commCode-0", "010310");
	rutSetElementValue("t32commGroup-0", "0105");
	rutSetElementValue("t32measurement-0", "0.0000");
	rutSetElementValue("t32pkgs-0", "99");
	rutSetElementValue("t32rorArea-0", "0.00");
	rutSetElementValue("t32weight-0", "0.00");*/


}

function setrateType(filed,rowid){
	var varRateType = rutGetElementValue(filed);
	var rowId = rowid.substring(rowid.lastIndexOf('-'));
	var i = rowid.substring(rowid.lastIndexOf('-'));

	setCustomTagProperty("t32imdgClass"+i, {dis:true});
	setCustomTagProperty("t32unno"+i, {dis:true});

	if (varRateType === 'D1') { // DG
		setCustomTagProperty('t32variant' + i, { dis: false });
		setCustomTagProperty('t32imdgClass' + i, { dis: false });
		setCustomTagProperty('t32unno' + i, { dis: false });
		setCustomTagProperty('t32portClass' + i, { dis: false, req: true });
	} else {
		setCustomTagProperty('t32variant' + i, { dis: true });
		setCustomTagProperty('t32portClass' + i, { dis: true, req: false });
		rutSetElementValue("t32variant"+i, "");
		rutSetElementValue("t32portClass"+i, "");
		if(varRateType == 'RF'){
			$('#t32reefer'+rowId).prop('checked', true);
			$('#t32oogFlag'+rowId).prop('checked', false);
		}else if(varRateType == 'O0'){
			$('#t32reefer'+rowId).prop('checked', false);
			$('#t32oogFlag'+rowId).prop('checked', true);
		}else{
			$('#t32reefer'+rowId).prop('checked', false);
			$('#t32oogFlag'+rowId).prop('checked', false);

		}
	}

}

	function onChangeShipMan(){
		var Getcon = rptGetDataFromDisplayAll("t31containerArea");
		if(Getcon.length > 0){
			IshaveDataCtn = true;
			for (var i = 0; i < Getcon.length; i++) {
				onChangeRateType("row-"+i,Getcon[i].rateType);
			}
		}

	}

	function onChangeRateTypeCustom(rowid,value){
		var RowNumber = rowid.substring(rowid.lastIndexOf('-'));
		var shipmentType = $('#t10shipmentType-'+isSelect).val();
		setDefultconCustom(RowNumber);

		if(shipmentType == 'LCL'){
			if(value == 'D1'){
				CustomPropertyAndValue("t31dgQty" + RowNumber,true,'1');
			}else if(value == 'O0'){
				CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
				CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'0');
			}else{
				setDefultconCustom(RowNumber);
			}

	}else if(shipmentType == 'ROR'){
		CustomPropertyAndValue("t31fullQty" + RowNumber,false,'1');
		if(value == 'D1'){
			CustomPropertyAndValue("t31dgQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
		}else if(value == 'O0'){
			CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
			CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
		}
	}else if(shipmentType == 'UC'){
		if(value == 'D1'){
			CustomPropertyAndValue("t31dgQty" + RowNumber,true,'1');
			CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
		}else{
			CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
		}

	}else if(shipmentType == 'FCL'){
		CustomPropertyAndValue("t31fullQty" + RowNumber,false,'1');
		if(value == 'N'){
			CustomPropertyAndValue("t31emptyQty" + RowNumber,false,'0');
		}else if(value == 'D1'){
			CustomPropertyAndValue("t31dgQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
		}else if(value == 'O0'){
			CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
			CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
		}
		$('#t31emptyQty'+RowNumber).val(0);
	}else if(shipmentType == 'BBK'){
		//console.log("shipmentType test")
		CustomPropertyAndValue("t31fullQty" + RowNumber,false,'1');
		CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'0');
		 if(value == 'D1'){
				CustomPropertyAndValue("t31dgQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
		}else if(value == 'O0'){
			CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
		}
	}
}


	function onChangeRateType(rowid,value){
		console.log("onChangeRateType");
		var RowNumber = rowid.substring(rowid.lastIndexOf('-'));
		var shipmentType = $('#t10shipmentType-'+isSelect).val();
		setDefultcon(RowNumber);
		if(shipmentType == 'LCL'){
				if(value == 'D1'){
					CustomPropertyAndValue("t31dgQty" + RowNumber,true,'1');
				}else if(value == 'O0'){
					CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
					CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'0');
				}else{
					setDefultcon(RowNumber);
				}

		}else if(shipmentType == 'ROR'){
			CustomPropertyAndValue("t31size" + RowNumber,false,'0');
			CustomPropertyAndValue("t31type" + RowNumber,false,'');
			CustomPropertyAndValue("t31fullQty" + RowNumber,false,'1');
			if(value == 'D1'){
				CustomPropertyAndValue("t31dgQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
			}else if(value == 'O0'){
				CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
				CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
			}
		}else if(shipmentType == 'UC'){

			if(value == 'D1'){
				CustomPropertyAndValue("t31dgQty" + RowNumber,true,'1');
				CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
			}else{
				CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
				setCustomTagProperty("t31voidSlot" + RowNumber, {dis:false});
			}

		}else if(shipmentType == 'FCL'){
			CustomPropertyAndValue("t31size" + RowNumber,false,'0');
			CustomPropertyAndValue("t31type" + RowNumber,false,'');
			CustomPropertyAndValue("t31fullQty" + RowNumber,false,'1');
			if(value == 'N'){
				CustomPropertyAndValue("t31emptyQty" + RowNumber,false,'0');
			}else if(value == 'D1'){
				CustomPropertyAndValue("t31dgQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
			}else if(value == 'O0'){
				CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
				CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'1');
			}
		}else if(shipmentType == 'BBK'){
			CustomPropertyAndValue("t31size" + RowNumber,false,'0');
			CustomPropertyAndValue("t31type" + RowNumber,false,'');
			CustomPropertyAndValue("t31fullQty" + RowNumber,false,'1');
			CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'0');
			 if(value == 'D1'){
					CustomPropertyAndValue("t31dgQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
			}else if(value == 'O0'){
				CustomPropertyAndValue("t31oogQty" + RowNumber,true,$('#t31fullQty' + RowNumber).val());
			}
			 CustomPropertyAndValue("t31voidSlot" + RowNumber,false,'0');
		}

	}

	function setDefultconCustom(RowNumber){

		    CustomPropertyAndValue("t31fullQty" + RowNumber,true,'1');
			CustomPropertyAndValue("t31emptyQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31dgQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31reeferQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31oogQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31voidSlot" + RowNumber,true,'0');
			CustomPropertyAndValue("t31teus" + RowNumber,true,'0.00');
			CustomPropertyAndValue("t31bundle" + RowNumber,true,'0');
	}

	function setDefultcon(RowNumber){

			CustomPropertyAndValue("t31size" + RowNumber,true,'0');
			CustomPropertyAndValue("t31type" + RowNumber,true,'**');
			CustomPropertyAndValue("t31fullQty" + RowNumber,true,'1');
			CustomPropertyAndValue("t31emptyQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31dgQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31reeferQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31oogQty" + RowNumber,true,'0');
			CustomPropertyAndValue("t31voidSlot" + RowNumber,true,'0');
			CustomPropertyAndValue("t31teus" + RowNumber,true,'0.00');
			CustomPropertyAndValue("t31bundle" + RowNumber,true,'0');

	}

	function CustomPropertyAndValue(filed,flag,value){
		setCustomTagProperty(filed, {dis:flag});
		if(!IshaveDataCtn){ // case Edit
			$('#'+filed).val(value);
		}
	}

/*function changeTeuV2(rowid){
	console.log("shipmentType :: changeTeuV2 ")
	var getrow = rowid.split("-");

	if(type == 'RE' || type == 'RH'){
		setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
		setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
		setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
		setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
		setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
		if(shipmentType == 'BBK' || shipmentType == 'UC'){
			setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
		}else{
			setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
		}
		setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});

	}else{

		setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
		if(type == 'PC'){

			setCustomTagProperty("t31bundle-" + getrow[1], {dis:false});
		}else{

			setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
		}
		checkrateType(rateType);

	}


	function checkrateType(value){
		console.log("shipmentType :: checkrateType "+shipmentType)
		if(value == 'N'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK' || shipmentType == 'UC'){
				console.log("shipmentType :: "+shipmentType)
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}

		}else if(value == 'D1' || value == 'DA' || value == 'NR' || value == 'OD'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK'  || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}

		}else if(value == 'O0'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});

		}

	}
}*/

function changeTeu(rowid,flag){
	var RowNumber = rowid.substring(rowid.lastIndexOf('-'));
	var fullQty = 0,emptyQty = 0,dgQty = 0,reeferQty = 0,oogQty = 0,voidSlot = 0,teus = 0,bundle=0;
	var getteus = 0.00;
	var getcontanin = rptGetDataFromDisplay('t31containerArea', rowid);
	var size = getcontanin.size;
	var type = getcontanin.type;
	//var rateType = getcontanin.rateType;
	var shipmentType = $('#t31shipmentType'+RowNumber).val();
	var rateType = $('#t31rateType'+RowNumber).val();
		fullQty = getcontanin.fullQty;
		emptyQty = getcontanin.emptyQty;
		dgQty = getcontanin.dgQty;
		reeferQty = getcontanin.reeferQty;
		oogQty = getcontanin.oogQty;
		voidSlot = getcontanin.voidSlot;

	var getrow = rowid.split("-");

	//i"getrow " ''){
		if(type == 'RE' || type == 'RH'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK' || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}
			setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
			oogQty = 0;
			dgQty = 0;
			bundle = 0;
			if(rateType == 'N'){
				reeferQty = fullQty;
			}else{
				reeferQty = 0;
			}
		}else{
			if(shipmentType == 'BBK' || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}

			setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
			if(type == 'PC'){
				bundle = 0;
				setCustomTagProperty("t31bundle-" + getrow[1], {dis:false});
			}else{
				bundle = 0;
				setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
			}
			checkrateType(rateType,flag);

		}
	//}


	function checkrateType(value,flag){
		reeferQty = 0;
		if(value == 'N'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK'  || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}
			oogQty = 0;
			dgQty = 0;
			voidSlot = 0;
		}else if(value == 'D1' || value == 'DA' || value == 'NR' || value == 'OD'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK'  || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}

			oogQty = 0;
			if(shipmentType == 'UC'){
				voidSlot = 1;
			}else{
				voidSlot = 0;
				}

			if(value == 'D1'){
				dgQty = fullQty;
			}else{
				dgQty = 0;
			}
		}else if(value == 'O0'){
			//console.log("value :: "+value);
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			dgQty = 0;
			oogQty = fullQty;
			voidSlot = 1;
		}else{
			voidSlot = 0;
			oogQty = 0;
		}


	}


	if(flag == undefined){ // it not edit case.

		$('#t31fullQty-'+getrow[1]).val(fullQty);
		//$('#t31size-'+getrow[1]).val(size);
		$('#t31emptyQty-'+getrow[1]).val(emptyQty);
		$('#t31dgQty-'+getrow[1]).val(dgQty);
		$('#t31reeferQty-'+getrow[1]).val(reeferQty);
		$('#t31oogQty-'+getrow[1]).val(oogQty);
		$('#t31voidSlot-'+getrow[1]).val(voidSlot);
		//$('#t31teus-'+getrow[1]).val(teus);
		$('#t31bundle-'+getrow[1]).val(bundle);
	}


	//console.log("getcontanin :: "+JSON.stringify(getcontanin));
	//console.log("changeTeu :: "+rptGetDataFromDisplay('t31containerArea', rowid).size);


}

function changeTeuvalueV2(rowid){

	var RowNumber = rowid.substring(rowid.lastIndexOf('-'));
	var fullQty = 0,emptyQty = 0,dgQty = 0,reeferQty = 0,oogQty = 0,voidSlot = 0,teus = 0,bundle=0;
	var getteus = 0.00;
	var getcontanin = rptGetDataFromDisplay('t31containerArea', rowid);
	var size = $('#t31size'+RowNumber).val();
	var type = $('#t31type'+RowNumber).val();

	var shipmentType = $('#t31shipmentType'+RowNumber).val();
	var rateType = $('#t31rateType'+RowNumber).val();
		fullQty = $('#t31fullQty'+RowNumber).val();
		emptyQty = $('#t31emptyQty'+RowNumber).val();
		dgQty = $('#t31dgQty'+RowNumber).val();
		reeferQty = $('#t31reeferQty'+RowNumber).val();
		oogQty = $('#t31oogQty'+RowNumber).val();
		voidSlot = $('#t31voidSlot'+RowNumber).val();

		if(size != "" && size > 0){
			if(size == '20'){
				getteus = 1;
			}else if(size == '40'){
				getteus = 2;
			}else{
				getteus = 2.25;
			}
		}else{
			getteus = 0.00;
			}


		teus = parseFloat(getteus) * (parseInt(fullQty) + parseInt(emptyQty))+ parseInt(voidSlot);
		//(parseInt(fullQty) + parseInt(emptyQty) + parseInt(dgQty)  + parseInt(reeferQty)  + parseInt(oogQty))
		if(teus > 0){
			teus = teus;
		}else{
			teus = "0.00";
		}

	$('#t31teus'+RowNumber).val(teus);
	if(!IsOneOff){
		callteuManten();
	}
}


function changeTeuvalue(rowid){

	var RowNumber = rowid.substring(rowid.lastIndexOf('-'));
	var fullQty = 0,emptyQty = 0,dgQty = 0,reeferQty = 0,oogQty = 0,voidSlot = 0,teus = 0,bundle=0;
	var getteus = 0.00;
	var getcontanin = rptGetDataFromDisplay('t31containerArea', rowid);
	var size = $('#t31size'+RowNumber).val();
	var type = $('#t31type'+RowNumber).val();

	var shipmentType = $('#t31shipmentType'+RowNumber).val();
	var rateType = $('#t31rateType'+RowNumber).val();
		fullQty = $('#t31fullQty'+RowNumber).val();
		emptyQty = $('#t31emptyQty'+RowNumber).val();
		dgQty = $('#t31dgQty'+RowNumber).val();
		reeferQty = $('#t31reeferQty'+RowNumber).val();
		oogQty = $('#t31oogQty'+RowNumber).val();
		voidSlot = $('#t31voidSlot'+RowNumber).val();

		if(size != "" && size > 0){
			if(size == '20'){
				getteus = 1;
			}else if(size == '40'){
				getteus = 2;
			}else{
				getteus = 2.25;
			}
		}else{
			getteus = 0.00;
			}


		teus = (parseFloat(getteus) * (parseInt(fullQty) + parseInt(emptyQty)))+ parseInt(voidSlot);
		//(parseInt(fullQty) + parseInt(emptyQty) + parseInt(dgQty)  + parseInt(reeferQty)  + parseInt(oogQty))
		if(teus > 0){
			teus = teus;
		}else{
			teus = "0.00";
		}

	$('#t31teus'+RowNumber).val(teus);
	if(!IsOneOff){
		callteuManten();
	}
}


function callteuManten(){
	 var getcon = rptGetDataFromDisplayAll("t31containerArea");
	 var setrowid = "t10row-"+isSelect;
	 var getshipType = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid);
	 var sumteus = 0;
	 if(!isEmpty(getcon)){
			for(var i = 0;i < getcon.length; i++){
				sumteus = parseInt(sumteus) + parseInt(getcon[i].teus);
			}
	}

	 getshipType.teu = sumteus;
	 $('#t10teu-'+isSelect).val(sumteus);
	// console.log("getshipType ::"+JSON.stringify(getshipType));
}


function getContainerCommodityTabOutput(){
	var ArraycontainerArea = [];
	var ArraycommodityArea = [];
	ArraycontainerArea = rptGetDataFromDisplayAll("t31containerArea");
	ArraycommodityArea = rptGetDataFromDisplayAll("t32commodityArea");
	console.log("getContainerCommodityTabOutput newArrayDeleteContainer ::"+JSON.stringify(newArrayDeleteContainer));
	if(!isEmpty(newArrayDeleteContainer)){
		for(var i = 0;i < newArrayDeleteContainer.length; i++){
			if(!isEmpty(newArrayDeleteContainer[i])){
				ArraycontainerArea.push(newArrayDeleteContainer[i]);
			}
		}
	}

	if(!isEmpty(newArrayDeleteCommodity)){
		for(var x = 0;x < newArrayDeleteCommodity.length; x++){
			if(!isEmpty(newArrayDeleteCommodity[x])){
				ArraycommodityArea.push(newArrayDeleteCommodity[x]);
			}
		}
	}

	var containerArea = CheckDataOnChange_ObjectListNew(OrgDataContainerArea,ArraycontainerArea,"containerSeqNo");
	var commodityArea = CheckDataOnChange_ObjectListNew(OrgDataCommodityArea,ArraycommodityArea,"commoditySeqNo");
	console.log("containerArea :: " + JSON.stringify(containerArea));
	//console.log("commodityArea :: " + JSON.stringify(commodityArea));
	var data = {};

	data = {
			container : containerArea,
			commodity : commodityArea
		};

	return data;
}

function SetCntrAndCommTabV2(){
	var HedConRow = rptGetDataFromDisplayAll("t31containerArea");
	if(HedConRow.length > 0){
		rptClearDisplay("t31containerArea");
	}
	var HedCommRow = rptGetDataFromDisplayAll("t32commodityArea");
	if(HedCommRow.length > 0){
		rptClearDisplay("t32commodityArea");
	}

	var data = {
			container : HedConRow,
			commodity : HedCommRow
	}

	fetchDataContainerAndCommodityTab_oog(data);
}

function fetchDataContainerAndCommodityTab_oog(content){

	rptAddDataWithCache("t31containerArea", SetAction_insert(content.container));
	rptDisplayTable("t31containerArea");
	perContainer = JSON.stringify(rptGetDataFromDisplayAll("t31containerArea"));
	curContainer = rptGetDataFromDisplayAll("t31containerArea");

	//set :
	rptAddDataWithCache("t32commodityArea", SetAction_insert(content.commodity));
	rptDisplayTable("t32commodityArea");
	setRowView("t31containerArea","t31containerSeqNo");
	setRowView("t32commodityArea","t32commoditySeqNo");
	var Getcon = rptGetDataFromDisplayAll("t31containerArea");
	var Getcomm = rptGetDataFromDisplayAll("t32commodityArea");

	//setDisplayConComm();

	if(Getcon.length > 0){
		for (var i = 0; i < Getcon.length; i++) {
			var getrow = Getcon[i].rowId.split("-");
			$('#t31size-'+getrow[1]).val(Getcon[i].size);
			changeTeuV2(Getcon[i].rowId);
			setpolStatus(i);
			$('#t31polStatus-'+getrow[1]).val(Getcon[i].polStatus);
			$('#t31podStatus-'+getrow[1]).val(Getcon[i].podStatus);

		}
	}

	if(Getcomm.length > 0){
		for (var x = 0; x < Getcomm.length; x++) {
			setCommodity(Getcomm[x].rowId);
		}
	}


}


function changeTeuV2(rowid){
	console.log("changeTeuV2 : ");
	var RowNumber = rowid.substring(rowid.lastIndexOf('-'));
	var getcontanin = rptGetDataFromDisplay('t31containerArea', rowid);
	var size = getcontanin.size;
	var type = getcontanin.type;
	var shipmentType = $('#t31shipmentType'+RowNumber).val();
	var rateType = $('#t31rateType'+RowNumber).val();
	var getrow = rowid.split("-");

		if(type == 'RE' || type == 'RH'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}
			setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});

		}else{

			setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
			if(type == 'PC'){
				setCustomTagProperty("t31bundle-" + getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
			}
			checkrateTypeV2(rateType);

		}


	function checkrateTypeV2(value){
		if(value == 'N'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK' || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}
		}else if(value == 'D1' || value == 'DA' || value == 'NR' || value == 'OD'){
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			if(shipmentType == 'BBK' || shipmentType == 'UC'){
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});
			}else{
				setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:true});
			}

		}else if(value == 'O0'){
			//console.log("value :: "+value);
			setCustomTagProperty("t31fullQty-" + getrow[1], {dis:false});
			setCustomTagProperty("t31emptyQty-" + getrow[1], {dis:true});
			setCustomTagProperty("t31dgQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31reeferQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31oogQty-"+getrow[1], {dis:true});
			setCustomTagProperty("t31voidSlot-"+getrow[1], {dis:false});

		}


	}


}

function calculateTEUs(rowid,value){
	var getrow = rowid.split("-");
	var size;
	var containerType;
	var fullQuantity;
	var emptyQuantity;
	var voidSlotsQuantity;
	var bundleQuantity;
	var isBundle;
	var volumnQty;
	var teus = 0;
	var imperialTeu40HQ;
	var imperialTeu45;
console.log("GetReferType :: "+JSON.stringify(GetReferType));
	size = $('#t31size-'+getrow[1]).val();
    containerType = $('#t31type-'+getrow[1]).val();
    fullQuantity = parseInt(removeComma($('#t31fullQty-'+getrow[1]).val()), 10);
    emptyQuantity = parseInt(removeComma($('#t31emptyQty-'+getrow[1]).val()), 10);
    voidSlotsQuantity = parseInt(removeComma($('#t31voidSlot-'+getrow[1]).val()), 10);
    bundleQuantity = parseInt(removeComma($('#t31bundle-'+getrow[1]).val()), 10);
    imperialTeu40HQ = parseInt(GetReferType.DefaultVal[0].IMPERIAL_TEU_40HQ, 10);
	imperialTeu45 = parseFloat((GetReferType.DefaultVal[0].IMPERIAL_TEU_45).toFixed(2));
    //isBundle=frm.isBundle.value;
    //console.log(imperialTeu45)

	if(isNaN(fullQuantity)) {
        fullQuantity = 0;
    }

    if(isNaN(emptyQuantity)) {
        emptyQuantity = 0;
    }

    if(isNaN(voidSlotsQuantity)) {
        voidSlotsQuantity = 0;
    }

    if(isNaN(bundleQuantity)) {
        bundleQuantity = 0;
    }

    volumnQty = fullQuantity + emptyQuantity;
	 if(size != "" && parseInt(volumnQty) > 0) {
    	if(size == "20"){
    		teus = (1 * volumnQty) + voidSlotsQuantity;
    	}else if(size == "40"){
    		teus = (imperialTeu40HQ * volumnQty) + voidSlotsQuantity;
    	}else if(size == "45"){
    		teus = (imperialTeu45 * volumnQty) + voidSlotsQuantity;
    	}

	} else {
		teus = "0";
	}
	$('#t31teus-'+getrow[1]).val(teus == 0 ? "0.00" : teus.toFixed(2));

}

function isReefer(rowid){
	var getrow = rowid.split("-");
	var Reefer = GetReferType.Reefer;
	var containerTypeValue = $('#t31type-'+getrow[1]).val();

	for (var f = 0; f < Reefer.length; f++) {
		if(containerTypeValue == Reefer[f].reeferType){
			var options = document.getElementById("t31rateType-"+getrow[1]).options;
			 for(var j = 0; j < options.length; j++) {
                 if(options[j].value == "N") {
                	 document.getElementById("t31rateType-"+getrow[1]).selectedIndex = j;
                 }
             }

		}

	}
}

function copyQuantity(rowid,reset){
	var getrow = rowid.split("-");
	var dgQuantity = "0";
	var reeferQuantity = "0";
	var oogQuantity = "0";
	var voidSlots;
	var fullQuantity = $('#t31fullQty-'+getrow[1]).val();
	var RateType = $('#t31rateType-'+getrow[1]).val();
	var shipmentType = $('#t31shipmentType-'+getrow[1]).val();
	var containerTypeValue = $('#t31type-'+getrow[1]).val();
	var Reefer = GetReferType.Reefer;

	if(RateType == 'D1'){
		dgQuantity = fullQuantity;
		if(reset == 'Y') {
			if (shipmentType == 'UC') {
				voidSlots = "1";
        	} else {
        		voidSlots = "0";
        	}
		}
	}else if(RateType == 'O0'){
		oogQuantity = fullQuantity;
	}

	 if (shipmentType != 'LCL') {
     	if (reset == 'Y') {
	        	if (RateType == 'O0' ||
	        		RateType == 'O7' ||
	        		shipmentType == 'UC') {
	        		voidSlots = "1";
     			} else {
     				voidSlots = "0";
     			}
    		}
     }else{
	    voidSlots="0";
     	//setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:true});
     }

	 if(RateType != 'O0') {
		// setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:true});
     } else {
    	 //setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:false});
     }

	 if (shipmentType == 'BBK') {
		 setCustomTagProperty("t31voidSlot-" + getrow[1], {dis:false});
	 }

	 for(var i = 0; i < Reefer.length; i++) {
		 if(containerTypeValue == Reefer[i].reeferType){
			 reeferQuantity = fullQuantity;
        }

	 }

	 //sesion set filed
	 $('#t31reeferQty-'+getrow[1]).val(reeferQuantity);
	 $('#t31voidSlot-'+getrow[1]).val(voidSlots);
	 $('#t31oogQty-'+getrow[1]).val(oogQuantity);
	 $('#t31dgQty-'+getrow[1]).val(dgQuantity);

}

function isFlatRack(rowid){
	var getrow = rowid.split("-");
	var Flatrack = GetReferType.Flatrack[0].flatrackType;
	var containerTypeValue = $('#t31type-'+getrow[1]).val();
	if(containerTypeValue == Flatrack){
		$('#t31bundle-'+getrow[1]).val(0);
		setCustomTagProperty("t31bundle-" + getrow[1], {dis:false});
	}else{
		$('#t31bundle-'+getrow[1]).val(0);
		setCustomTagProperty("t31bundle-" + getrow[1], {dis:true});
	}
	calculateTEUs(rowid,"");
}

	function MansetpolStatus(val){
		 var contanin = rptGetDataFromDisplayAll('t31containerArea');
		 for(var i = 0; i < contanin.length; i++) {
			 setpolStatus(i,val);
			 $('#t31polStatus-'+i).val(contanin[i].polStatus);
			 $('#t31podStatus-'+i).val(contanin[i].podStatus);
		 }

	}

	function setpolStatus(rowid,val){
		var setval ="";

		if(val != undefined){

			setval = val

		}else{

			var setrowid = "t10row-"+isSelect;

			setval = rptGetDataFromDisplay('t10shipTypeDetailArea', setrowid).socCoc;
		}

		var podStatusId = 't31podStatus-'+rowid;

		var polStatusId = 't31polStatus-'+rowid;

		var podStatusElement = document.getElementById(podStatusId);

		var polStatusElement = document.getElementById(polStatusId);

		polStatusElement.innerHTML="";

		if(setval == 'C'){
			podStatusElement.innerHTML='<option value="L" selected>Local</option>';
			polStatusElement.innerHTML='<option value="L" selected>Local</option>';
		}else{
			podStatusElement.innerHTML='<option value="L" selected>Local</option><option value="T">T/S</option>';
			polStatusElement.innerHTML='<option value="L" selected>Local</option><option value="T">T/S</option>';
		}


	}


</script>
