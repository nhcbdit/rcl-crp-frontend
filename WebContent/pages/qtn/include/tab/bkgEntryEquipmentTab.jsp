<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>

<div class="tab-pane" id="p0-EquipmentTab">

	<!-- Equipment Header -->
	<div id="d0-equipmentHeader" class="rcl-standard-widget-header"
		data-toggle="collapse" data-target="#d0-equipmentArea">
		<span><i class="fas fa-chevron-right"></i></span> <label>Equipments
			Header</label>
	</div>

	<!-- Start Equipment Header Area -->
	<div id="d0-equipmentArea" class="collapse show">
		<form id="frm">
			<div class="rcl-standard-search-header">

				<div class="row">

					<rcl:text id="d0-bookingNo" label="Booking#"
						classes="div(col-sm-2)" options="disabled" />

					<rcl:text id="d0-type" label="BK Type" classes="div(col-sm-1)"
						options="disabled" />

					<rcl:text id="d0-status" label="Status" classes="div(col-sm-1)"
						options="disabled" />

					<rcl:text id="d0-POR" label="POR" classes="div(col-sm-1)"
						options="disabled" />

					<rcl:text id="d0-POL" label="POL" classes="div(col-sm-1)"
						options="disabled" />

					<rcl:text id="d0-POD" label="POD" classes="div(col-sm-1)"
						options="disabled" />

					<rcl:text id="d0-DEL" label="DEL" classes="div(col-sm-1)"
						options="disabled" />

					<rcl:text id="d0-ShipmentType " label="Shipment Type"
						classes="div(col-sm-2)" options="disabled" />

				</div>
			</div>
		</form>
	</div>

	<!-- Size-Type Table Header -->
	<div id="t1-sizeTypetHeader"
		class="rcl-standard-widget-header tblHeader" data-toggle="collapse"
		data-target="#t1-sizeTypeArea">
		<span><i class="fas fa-chevron-right"></i></span> <label>Size-Type
			Details</label>
	</div>

	<!-- Size-Type Table Details -->
	<div id="t1-sizeTypeArea" class="collapse show tblArea">
		<form id="frm">
			<div class="rcl-standard-search-header">

				<div id="t1-row" class="row tblRow">

					<div class="col-sm-1 col-md-1 pb-0 pl-1 pr-2"
						style="text-align: center">
						<label>Select</label>
						<div class="rcl-standard-input-wrapper">
							<input id="t1-sizeTypeSelect" name="Select" type="radio"
								class="rcl-standard-form-control" placeholder="">
						</div>
					</div>

					<rcl:select id="t1-size" label="Size"
						classes="div(col-sm-1) tblField" optionsList="20 40 45"
						valueList="20 40 45" />

					<rcl:text id="t1-type" label="Type"
						classes="div(col-sm-1) tblField" check="req len(55)"
						lookup="tbl-getEQ" />

					<rcl:text id="t1-teus" label="TEUS"
						classes="div(col-sm-1) tblField" check="len(55)" />

					<rcl:select id="t1-rateType" label="Rate Type"
						classes="div(col-sm-1) tblField"
						optionsList="DG DoorAjar Non-Reefer Normal OOG OpenDoor Reefer"
						valueList="t1 DA NR N O0 OD RF" />

					<rcl:number id="t1-laden" label="Laden"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<rcl:number id="t1-dg" label="DG" classes="div(col-sm-1) tblField"
						check="dec(10,0)" />

					<rcl:number id="t1-rf" label="RF" classes="div(col-sm-1) tblField"
						check="dec(10,0)" />

					<rcl:number id="t1-oog" label="OOG"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<rcl:number id="t1-mt" label="MT" classes="div(col-sm-1) tblField"
						check="dec(10,0)" />

					<rcl:number id="t1-reeferPoints" label="Reefer Points"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<div class="col-sm-1 col-md-1 pb-0">
						<!--<a id="updateBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2" style="font-size:10px;" data-toggle="tooltip" data-placement="top" title="Moves input to data and returns them for use" onclick='rptGetDataFromDisplay("t1-container", "#-RowId-#");'>upd</a>-->
						<a id="deleteBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2"
							style="font-size: 10px;" data-toggle="tooltip"
							data-placement="top"
							title="Removes current row from display and data"
							onclick='rptRemoveRow("t1-sizeTypeArea", "#-RowId-#")'>Delete</a>
					</div>

					<!--//--------------------------------------------------------//  -->

					<rcl:text id="t1-specialCargoCode" label="Special Cargo Code"
						classes="div(col-sm-1 offset-sm-1) tblField" check="req len(10)"
						lookup="tbl-getEQ" />

					<rcl:text id="t1-handling1" label="Handling 1"
						classes="div(col-sm-1) tblField" check="req len(10)"
						lookup="tbl-getEQ" />

					<rcl:text id="t1-handling2" label="Handling 2"
						classes="div(col-sm-1) tblField" check="req len(10)"
						lookup="tbl-getEQ" />

					<rcl:text id="t1-handling3" label="Handling 3"
						classes="div(col-sm-1) tblField" check="req len(10)"
						lookup="tbl-getEQ" />

					<rcl:text id="t1-clr1" label="CLR1"
						classes="div(col-sm-1) tblField" check="req len(10)"
						lookup="tbl-getEQ" />

					<rcl:text id="t1-clr2" label="CLR2"
						classes="div(col-sm-1) tblField" check="req len(10)"
						lookup="tbl-getEQ" />

					<div class="col-sm-6 col-md-1 pb-0">
						<label>Description</label>
						<div>
							<!--<input id="t1-Description" name="CommodityCode" type="text" class="rcl-standard-form-control tblField" autocomplete="on" required placeholder="999990">-->
							<i class="fa fa-plus " aria-hidden="true"></i>
						</div>
					</div>

					<rcl:number id="t1-bundle" label="Bundle"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<rcl:select id="t1-polStatus" label="POL Status"
						classes="div(col-sm-1) tblField" optionsList="Local T/S"
						valueList="L T" />

					<rcl:select id="t1-podStatus" label="POD Status"
						classes="div(col-sm-1) tblField" optionsList="Local T/S"
						valueList="L T" />

					<!--//--------------------------------------------------------//  -->

					<rcl:number id="t1-comVoidSlots" label="Com. Void Slot"
						classes="div(col-sm-1 offset-sm-2) tblField" check="dec(10,0)" />

					<rcl:number id="t1-oprVoidSlots" label="Opr. Void Slot"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<div class="col-sm-6 col-md-1 pb-0">
						<label>Food Grades</label>
						<div>
							<!--<input id="t1-foodGrades" name="foodGrades" type="text" class="rcl-standard-form-control tblField" autocomplete="on" required placeholder="999990">-->
							<i class="fa fa-plus " aria-hidden="true"></i>
						</div>
					</div>

					<div class="col-sm-6 col-md-1 pb-0">
						<label>Equipment Grades</label>
						<div>
							<!--<input id="t1-equipmentGrades" name="equipmentGrades" type="text" class="rcl-standard-form-control tblField" autocomplete="on" required placeholder="999990">-->
							<i class="fa fa-plus " aria-hidden="true"></i>
						</div>
					</div>

				</div>
				<!-- End Row -->
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: right; margin-top: -5; margin-bottom: -5px;">
						<button type="button" class="rcl-standard-button"
							onclick="addLastEmptytb1()">Add</button>
					</div>
				</div>

			</div>
		</form>
	</div>

	<div id="t2-supplierHeader"
		class="rcl-standard-widget-header tblHeader" data-toggle="collapse"
		data-target="#t2-supplierArea">
		<span><i class="fas fa-chevron-right"></i></span> <label>Supplier
			Details</label>
	</div>
	
	<!-- Supplier Table Details -->
	<div id="t2-supplierArea" class="collapse show tblArea">
		<form id="frm">
			<div class="rcl-standard-search-header">

				<div id="t2-row" class="row tblRow">

					<div class="col-sm-1 col-md-1 pb-0 pl-1 pr-2"
						style="text-align: center">
						<label>Select</label>
						<div class="rcl-standard-input-wrapper ">
							<input id="t2-sizeTypeSelect" name="Select" type="radio"
								class="rcl-standard-form-control " placeholder="">
						</div>
					</div>

					<rcl:text id="t2-supplier" label="Supplier"
						classes="div(col-sm-1) tblField" check="req len(20)"
						lookup="tbl-getSup" />

					<rcl:text id="t2-name" label="Name"
						classes="div(col-sm-2) tblField" check="len(50)" />

					<rcl:text id="t2-positioningPOR" label="POR" classes="div(col-sm-1) tblField"
						check="req len(20)" lookup="tbl-getSup" />

					<rcl:text id="t2-mtPickup" label="MT Pickup Depot/Terminal"
						classes="div(col-sm-2) tblField" check="len(50)" />

					<rcl:date id="t2-mtDate" label="MT Date " classes="div(col-sm-1)"
						check="req" />

					<rcl:text id="t2-time" label="Time"
						classes="div(col-sm-1) tblField" check="req" />

					<rcl:select id="t2-rateType" label="Rate Type"
						classes="div(col-sm-1) tblField"
						optionsList="DG DoorAjar Non-Reefer Normal OOG OpenDoor Reefer"
						valueList="t2 DA NR N O0 OD RF" />

					<rcl:number id="t2-laden" label="Laden"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<div class="col-sm-1">
						<label>Reserve</label> <input type="checkbox"
							id="t2-reserve" />
					</div>

					<!--//--------------------------------------------------------//  -->

					<rcl:text id="t2-forwardingAgent" label="Forwarding Agent"
						classes="div(col-sm-1 offset-sm-1) tblField" check="req len(20)"
						lookup="tbl-getSup" />

					<rcl:text id="t2-forwardingAgentName" label="Name"
						classes="div(col-sm-2) tblField" check="len(50)" />

					<rcl:text id="t2-supplierLoc" label="Supplier Loc."
						classes="div(col-sm-1) tblField" check="req len(20)"
						lookup="tbl-getSup" />

					<div class="col-sm-1">
						<label>Haul. from Shp. Loc.</label> <input type="checkbox"
							id="t2-haulFromShpLoc" />
					</div>

					<rcl:number id="t2-rf" label="RF" classes="div(col-sm-1) tblField"
						check="dec(10,0)" />

					<rcl:number id="t2-dg" label="DG" classes="div(col-sm-1) tblField"
						check="dec(10,0)" />

					<rcl:number id="t2-oog" label="OOG"
						classes="div(col-sm-1) tblField" check="dec(10,0)" />

					<rcl:number id="t2-mt" label="MT" classes="div(col-sm-1) tblField"
						check="dec(10,0)" />

					<div class="col-sm-1" style="text-align: center;">
						<label>Avl. Chk.</label> <a id="t2-avlChk"
							class="btn btn-sm btn-light text-dark ml-2 pl-2"
							style="font-size: 10px;" data-toggle="tooltip"
							data-placement="top" title="Avl. Chk." onclick=''>Go</a>
					</div>

					<div class="col-sm-1 col-md-1 pb-0">
						<a id="t2-deleteBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2"
							style="font-size: 10px;" data-toggle="tooltip"
							data-placement="top"
							title="Removes current row from display and data"
							onclick='rptRemoveRow("t2-supplierArea", "#-RowId-#")'>Delete</a>
					</div>

				</div>
				<!-- End Row Template -->
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: right; margin-top: -5; margin-bottom: -5px;">
						<button type="button" class="rcl-standard-button"
							onclick="addLastEmptytb2()">Add</button>
					</div>
				</div>

			</div>
		</form>
	</div>

	<div id="t3-equipmentDetailHeader"
		class="rcl-standard-widget-header tblHeader" data-toggle="collapse"
		data-target="#t3-equipmentDetailArea">
		<span><i class="fas fa-chevron-right"></i></span> <label>Equipment
			Details</label>
	</div>
	
	<!-- Supplier Table Details -->
	<div id="t3-equipmentDetailArea" class="collapse show tblArea">
		<form id="frm">
			<div class="rcl-standard-search-header">

				<div id="t3-row" class="row tblRow">

					<rcl:text id="t3-containerNo" label="Container#"
						classes="div(col-sm-2) tblField" check="req len(50)"
						lookup="tbl-getSup" />

					<rcl:text id="t3-fullMT" label="Full/MT"
						classes="div(col-sm-1) tblField" check="len(20)" />
					
					<rcl:number id="t3-grossContainerWeight" label="Gross Container Weight"
						classes="div(col-sm-2) tblField" check="dec(20,0)" />

					<rcl:select id="t3-polStatus" label="POL Status"
						classes="div(col-sm-1) tblField" optionsList="Local T/S"
						valueList="L T" />

					<rcl:select id="t3-podStatus" label="POD Status"
						classes="div(col-sm-1) tblField" optionsList="Local T/S"
						valueList="L T" />

					<div class="col-sm-1 col-md-1 pb-0 offset-sm-4">
						<a id="t3-deleteBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2"
							style="font-size: 10px;" data-toggle="tooltip"
							data-placement="top"
							title="Removes current row from display and data"
							onclick='rptRemoveRow("t3-equipmentDetailArea", "#-RowId-#")'>Delete</a>
					</div>
					
					
					<!--//--------------------------------------------------------//  -->
					
					<div class="col-sm-1">
						<label>RF</label> <input type="checkbox"
							id="t3-rf"  />
					</div>

					<rcl:number id="t3-temperature" label="Temperature"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:select id="t3-cf" label="CF"
						classes="div(col-sm-1) tblField"
						optionsList="key1 key2"
						valueList="val1 val2" />
						
					<rcl:number id="t3-ventilation" label="Ventilation"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:number id="t3-humidity" label="Humidity"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<div class="col-sm-1">
						<label>Controlled Atmsphere</label> <input type="checkbox"
							id="t3-controlledAtm"  />
					</div>
					
					<rcl:number id="t3-nitrogen" label="Nitrogen"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
						
					<rcl:number id="t3-oxygen" label="Oxygen"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:select id="t3-refrMode" label="Refr. Mode"
						classes="div(col-sm-1) tblField"
						optionsList="key1 key2"
						valueList="val1 val2" />
						
					<div class="col-sm-1">
						<label>Pre-Cooled</label> <input type="checkbox"
							id="t3-preCooled" />
					</div>
					
					<!--//--------------------------------------------------------//  -->
					
					<rcl:number id="t3-oh" label="OH"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:number id="t3-owl" label="OWL"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
						
					<rcl:number id="t3-owr" label="OWR"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>	
					
					<rcl:number id="t3-ow" label="OW"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:number id="t3-ola" label="OLA"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:number id="t3-olf" label="OLF"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:number id="t3-ol" label="OL"
						classes="div(col-sm-1) tblField" check="dec(10,2)"
						options="disabled"/>
					
					<rcl:number id="t3-vgm" label="VGM"
						classes="div(col-sm-1) tblField" check="dec(10,2)"/>
					
					<rcl:select id="t3-category" label="Category"
						classes="div(col-sm-2) tblField"
						optionsList="key1 key2"
						valueList="val1 val2" />
						
					
							
				</div>
				<!-- End Row Template -->
				<hr style="margin-top: 0px">
				<div class="row">
					<div class="col-md-12"
						style="text-align: right; margin-top: -5; margin-bottom: -5px;">
						<button type="button" class="rcl-standard-button"
							onclick="addLastEmptytb3()">Add</button>
					</div>
				</div>

			</div>
		</form>
	</div>

	

</div>
<!-- ========================================================= START of Scripts -->

<script language="javascript">
	var data="";

	data = [								
		{							
			size : 1234,						
			type : "string",						
			teus : 1234,						
			rateType : "string",						
			laden : 1234,						
			dg : 1234,						
			rf : 1234,						
			oog : 1234,						
			mt : 1234,						
			reeferPoints : 1234,						
			comVoidSlots : 1234,						
			oprVoidSlots : 1234,						
			specialCargoCode : "string",						
			handling1 : "string",						
			handling2 : "string",						
			handling3 : "string",						
			clr1 : "string",						
			clr2 : "string",						
			bundle : 1234,						
			polStatus : "string",						
			podStatus : "string",						
			foodGrades : {						
				grade1 : "string",					
				grade2 : "string",					
				grade3 : "string",					
				grade4 : "string",					
				grade5 : "string",					
				maxAge : 1234					
			},						
			equipmentGrades : {						
				grade1 : "string",					
				grade2 : "string",					
				grade3 : "string",					
				grade4 : "string",					
				grade5 : "string",					
				maxAge : 1234					
			},						
			description : {						
				specialCargoCode : "string",					
				specialCargoDescription : "string",					
				handling1Code : "string",					
				handling1Description : "string",					
				handling2Code : "string",					
				handling2Description : "string",					
				handling3Code : "string",					
				handling3Description : "string",					
				loadRemarksCode1 : "string",					
				loadRemarksDescription1 : "string",					
				loadRemarksCode2 : "string",					
				loadRemarksDescription2 : "string",					
			},						
			supplier : [						
				{					
					supplier : "string",				
					name : "string",				
					positioningPOR : "string",				
					mtPickup : "string",				
					mtDate : "string",				
					time : "string",				
					rateType : "string",				
					laden : 1234,				
					reserve : true,				
					forwardingAgent : "string",				
					forwardingAgentName : "string",				
					supplierLoc : "string",				
					haulFromShpLoc : true,				
					rf : 1234,				
					dg : 1234,				
					oog : 1234,				
					mt : 1234,				
					equipment : [				
						{			
							containerNo : "string",		
							packageNo : "string",		
							fullMT : "string",		
							grossContainerWeight : 1234,		
							polStatus : "string",		
							podStatus : "string",		
							rf : true,		
							temperature : 1234,		
							cf : "string",		
							ventilation : 1234,		
							humidity : 1234,		
							controlledAtm : true,		
							nitrogen : 1234,		
							oxygen : 1234,		
							co2 : 1234,		
							refrMode : "string",		
							preCooled : true,		
							oh : 1234,		
							owl : 1234,		
							owr : 1234,		
							ola : 1234,		
							olf : 1234,		
							vgm : 1234,		
							category : "string",		
							markDesc : {		
								packageNo : "string",	
								details : [{	
									markNum : "string",
									desc : "string"
								}]	
							}		
						}			
					]				
				}					
			]						
		}							
	];
</script>


<script language="javascript">
	var table1Settings;
	var table2Settings;
	var table3Settings;
	$(document).ready(function() {
		//This activates the popover
		//$('[data-toggle="popover"]').popover();
		table1Settings = rptTableInit("t1-sizeTypeArea");
		/* table2Settings = rptTableInit("t2-supplierArea");
		table3Settings = rptTableInit("t3-equipmentDetailArea"); */
		rptAddData("t1-sizeTypeArea", data);
		/* rptAddData("t2-supplierArea", data);
		rptAddData("t3-equipmentDetailArea", data); */
		rptDisplayTable("t1-sizeTypeArea");
		/* rptDisplayTable("t2-supplierArea");
		rptDisplayTable("t3-equipmentDetailArea"); */
		//console.log(tableSettings);
		//console.log(tableSettings.data);
		//console.log(countDisplay("t1-sizeTypeArea"));
	});

	function addLastEmptytb1() {
		rptCreateNewTableRow(table1Settings, countDisplayRow('t1-',table1Settings.rowId));
	}
	function addLastEmptytb2() {
		rptCreateNewTableRow(table2Settings, countDisplayRow('t2-',table2Settings.rowId));
	}
	function addLastEmptytb3() {
		rptCreateNewTableRow(table3Settings, countDisplayRow('t3-',table3Settings.rowId));
	}
	function countDisplayRow(tb, rowId) {
		return $('*[id^="' + tb + rowId + '-"]').length;
	}
</script>

