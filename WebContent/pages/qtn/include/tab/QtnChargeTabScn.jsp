<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<%-- Customers Header --%>
<rcl:area id="d50QuotationHeader" classes="div(container-fluid)" title="Quotation Header" collapsible='Y'>
	<div class="row">
		<rcl:text id="d50quotationNo" classes="div(col-md-2) ctr(dtlField)"
			label="Quotation#" options="disabled" check="upc" />
		<rcl:text id="d50ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" options="disabled" check="upc" />
		<%-- rcl:select id="d50status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/--%>
		<rcl:select id="d50status" label="Status"
					classes="div(col-md-2) ctr(dtlField)"
					options="data-rdo"
					optionsList="Open Need_Approval Quote_Again Close Final Hold Cancel"
					valueList="O N Q S C H L" />
		<rcl:text id="d50slm" classes="div(col-md-2) ctr(dtlField)"
			label="Slm#" options="disabled" check="upc" />
		<rcl:text id="d50qtdBy" classes="div(col-md-2) ctr(dtlField)"
			label="QtdBy" options="disabled" check="upc" />
		<rcl:text id="d50pos" classes="div(col-md-2) ctr(dtlField)"
			label="POS" options="disabled" check="upc" />
	</div>

</rcl:area>

<rcl:area id="t51RoutingHeader" classes="div(container-fluid)" title="Routing List" collapsible='Y' areaMode="table">
<%-- div id="routhingScroll" --%>
	<div id="t51row" class="row tblRow t51row">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t51row');" class="row">
				<div class="col-md-12 container-fluid routhing-changes">
					<div class="row">
						<rcl:text id="t51porHaulLoc" classes="div(col-md-2) ctr(dtlField)" label="POR Haul. Loc." options="disabled" check="upc" />
						<rcl:text id="t51por" classes="div(col-md-1)" label="POR"  options="disabled" check="upc" />
						<rcl:text id="t51pol" classes="div(col-md-1)" label="POL" options="disabled" />
						<rcl:text id="t51pot1" classes="div(col-md-1)" label="POT1" options="disabled" check="upc" />
						<rcl:text id="t51pot2" classes="div(col-md-1)" label="POT2" options="disabled" check="upc" />
						<rcl:text id="t51pot3" classes="div(col-md-1)" label="POT3" options="disabled" check="upc" />
						<rcl:text id="t51pod" classes="div(col-md-1)" label="POD" options="disabled" />
						<rcl:text id="t51del" classes="div(col-md-1)" label="DEL" options="disabled" check="upc" />
						<rcl:text id="t51delHaulLoc" classes="div(col-md-2)" label="DEL Haul. Loc." options="disabled" check="upc" />
						<rcl:select id="t51socCoc"
										label="SOC/COC"
										classes="div(col-md-1) ctr(dtlField)"
										selectTable="SocCoc"
										options="data-dis"/>
					</div>
				</div>
			</div>
		</div>
	</div>
<%--/div --%>

	<div class="row">
	<div id="t51subjectGrcLabel" class="col-md-2" >
			<label>Subject to change GRC - RR</label>
			<input type="checkbox" id="subjRRFlag" class="dtlField" />
		</div>
		<div class="col-md-10"
			style="text-align: center; margin-top: 5px; margin-bottom: 5px;">
			<button type="button" style="display:none" class="rcl-standard-button"
				onclick="addCustomer()">Exchange Rates</button>
			<button id="_t51getrate" type="button" class="rcl-standard-button"
				onclick="Get_Rates()">Get Rates</button>
		</div>
	</div>
	<div class="row appoverCharge" style="display:none;">
	<div class="col-md-4" style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
	</div>
		<div class="col-md-2"  style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
		<button id="QuoteAgainCharge" type="button" style="display:none;"
						class="rcl-standard-button appoverCharge"
					onclick="qtn_Approval('Q')">Quote Again</button>
		<button id="AppoverCharge" type="button" style="display:none;"
					class="rcl-standard-button appoverCharge"
					onclick="qtn_Approval('S')">Appove</button>
		</div>
		<div class="col-md-6" style="text-align: right; margin-top: 5px; margin-bottom: 5px;"></div>
	</div>
</rcl:area>

<rcl:area id="t52ChargesDetailsArea" classes="div(container-fluid)" title="Charges Details" collapsible='Y' areaMode="table">
	<div id="t52row" class="row tblRow t52row border" style="padding-bottom: 0px;margin-right: 0px;">
		<div class="container-fluid" style="margin-left: 10px;">
			<div onclick="onselectBG('#-RowId-#','t52row')" class="row"  style="padding-bottom: 0px;">
			<div class="container-fluid">
				<div class="row" style="padding-left: 0px;">
						<input id="t52portClassCodeIn" class="tblField" style="display:none;"  />
						<input id="t52seqNo" class="tblField" style="display:none;"  />
						<rcl:number id="t52chargeSeqNo" label="seq." classes="div(col-0x25 pl-0 pr-1) ctr(dtlField)" options="disabled"/>
						<rcl:select id="t52freightOrSurchageFlag" classes="div(col-md-1  pl-1 pr-0) ctr(dtlField)" label="Frt/Sch" check="req" defaultValue="S" selectTable="FreightSurcharge" options="data-dis onchange='fnonchangefreig(this.id,this.value);'" />
						<rcl:stdShipmentType id="t52shipmentType" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Shipment Type" check="req len(3)" options="data-dis"/>
						<rcl:select id="t52rateType" classes="div(col-md-1 pl-1 pr-0) ctr(tblField rateType)" label="Rate Type" selectTable="RateTypeCharge" defaultValue="D1" options="onchange='fnonchangeratecharge(this.id,this.value);'"/>
						<rcl:select id="t52emptyOrLadenFlag" classes="div(col-md-1 pl-1 pr-0) tblField ctr(dtlField)" label="Laden/Mt" selectTable="LadenEmpty"  defaultValue="F"/>
						<rcl:select id="t52equipmentSize" classes="div(col-0x5 pl-1 pr-0) tblField ctr(dtlField)" label="Size" selectTable="EqSize" defaultValue="0"/>

						<rcl:text id="t52equipmentType" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" label="Type" options="onkeyup='upc();'" check="len(2)"
						lookup="tbl(vrl_equipment_type)
								rco(CODE)
								rid(t52equipmentType#-RowId-#)
								sco(CODE*main)
								sva(t52equipmentType#-RowId-#)
								sop(LIKE)" />
						<rcl:text id="t52freightOrSurchargeCode" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Code" check="req len(5)"
						lookup="tbl(VRL_QTN_FREIGHT_SURCHARGE)
							  rco(SURCHARGE_CODE DESCRIPTION DISCOUNT_CHARGE)
							  rid(t52freightOrSurchargeCode#-RowId-# t52shortDescription#-RowId-# t52chargeOrDiscountFlag#-RowId-#)
							  sco(CHARGE_TYPE)
							  sva(t52freightOrSurchageFlag#-RowId-#)
							  sop(=)"/>
						<%-- div class=" col-md-1 pl-1 pr-0">
							<label for="t52freightOrSurchargeCode-0">Code</label>
							<div class="rcl-standard-input-wrapper">
							<div style="padding-right:20px">
							<input id="t52freightOrSurchargeCode" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField dtlField" data-ct="bt-text">
							</div>
								<i class="fa fa-search" onclick="lookup_freightOrSurchargeCode('#-RowId-#',t52freightOrSurchargeCode,t52freightOrSurchageFlag)"></i>
							</div>
						</div--%>
						<rcl:text id="t52shortDescription" classes="div(col-1x5 pl-1 pr-0) ctr(dtlField)" label="Charge Description" options="disabled" />
						<rcl:select id="t52calculateBasis"
						options="onchange=\"onchangeBasie('#-RowId-#',this.value); chgCalculateBasis('#-RowId-#',this.value);\""
						classes="div(col-md-1 pl-1 pr-0) tblField ctr(dtlField)" label="Basis" selectTable="FreightSurchargeBasis" defaultValue="B"/>
						<%--rcl:text id="t52rateUom" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" label="UOM"  check="len(3)"
						lookup="tbl(VRL_UNIT_MEASURE)
								rco(CODE)
								rid(t52rateUom#-RowId-#)
								sco(CODE*main IMPERIAL_METRIC TYPE)
								sva(t52rateUom#-RowId-# t52calculateBasis#-RowId-# t52calculateBasis#-RowId-#)
								sop(LIKE = =)" /--%>
							<div class=" col-0x5 pl-1 pr-0">
								<label for="t52rateUom">UOM</label>
								<div class="rcl-standard-input-wrapper">
								<div style="padding-right:20px">
								<input id="t52rateUom" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField dtlField" maxlength="3" data-ct="bt-text" >
								</div>
									<i class="fa fa-search" id="lookupMEASURE" onclick="openRutup('#-RowId-#')"></i>
								</div>
							</div>
						<div class=" col-0x5 pl-1 pr-0">
							<label for="t52currency">Currency</label>
							<div class="rcl-standard-input-wrapper">
								<div style="padding-right:20px">
									<input id="t52currency" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField dtlField" required maxlength="3" onkeyup="upc();" onblur="hilighcolorcharges(this.id,this.value,'#-RowId-#'); hilighcolorchargesCheckOrg(this.id,this.value);" data-ct="bt-text">
								</div>
									<i class="fa fa-search" onclick="fnonfocus('t52currency','#-RowId-#'); rutOpenLookupTable('VRL_CURRENCY','CODE','t52currency#-RowId-#','CODE*main','t52currency#-RowId-#','LIKE','');"></i>
							</div>
						</div>

							<%-- rcl:text id="t52currency" classes="div(col-0x5 pl-1 pr-0) ctr(dtlField)" label="Currency" check="req len(3) upc" options="onblur= hilighcolorcharges(this.id,this.value,'#-RowId-#')"
											lookup="tbl(VRL_CURRENCY)
													rco(CODE)
													rid(t52currency#-RowId-#)
													sco(CODE*main)
													sva(t52currency#-RowId-#)
													sop(LIKE)"/--%>
						<rcl:number id="t52rate" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Rate" check="dec(11,2)" defaultValue = "0.00" options="onblur='hilighcolorcharges(this.id,this.value); hilighcolorchargesCheckOrg(this.id,this.value); IsChkSubtoChange(this.id,this.value);' "/>

						<rcl:select id="t52prepaidOrCollect" classes="div(col-md-1 pl-1 pr-0) tblField ctr(dtlField)" label="Payment Term" selectTable="PrepaidCollect" defaultValue = "P"  options="onchange='checkfsc(this.id,this.value); hilighcolorcharges(this.id,this.value); hilighcolorchargesCheckOrg(this.id,this.value);' "/>
						<input id="t52prepaidOrCollect_org" class="tblField" style="display:none;" />

						<rcl:select id="t52orgDestTs" classes="div(col-md-1 offset-0x25 pl-1 pr-0) tblField ctr(dtlField)"
											label="Org/Dest/TS" selectTable="OriginDestination" defaultValue="O" options="onchange='checkorgDestTs(this.id,this.value); resetValueForRateAppliedTo(this.id,this.value);'"/>
						<rcl:select id="t52tsLocalFlag"
											classes="div(col-0x5 pl-1 pr-0) tblField ctr(dtlField)" label="Port Sts"
											selectTable="PortStatus" defaultValue="L"/>
						<rcl:select id="t52effBasis" classes="div(col-0x5 pl-1 pr-0) tblField ctr(dtlField)" label="Eff Bas" optionsList="ETA_POD ETD_POD ETA_POL ETD_POL" valueList="APOD DPOD APOL DPOL" defaultValue="APOL"/>
						<rcl:text id="t52commodityGroupCode" classes="div(col-md-1 pl-1 pr-0) tblField" label="Comm.Group" check="len(5)"
							lookup="tbl(VRL_COMMODITY_GROUP)
						  	rco(COMMODITY_GROUP_CODE)
						  	rid(t52commodityGroupCode#-RowId-#)" />
							<%-- lookup="tbl(VRL_COMMODITY)
						  	rco(CODE)
						  	rid(t52commodityGroupCode#-RowId-#)" 
						  	/--%>
						<%--rcl:text id="t52portClassCode" classes="div(col-md-1 pl-1 pr-0) tblField" label="port Class" check="len(5)"
											lookup="tbl(VRL_BKG_PORT_CLASS)
													rco(PORT_CLASS_CODE)
													rid(t52portClassCode#-RowId-#)" /--%>
						<div class=" col-md-1 pl-1 pr-0">
							<label for="t52portClassCode">port Class</label>
							<div class="rcl-standard-input-wrapper">
							<div style="padding-right:20px">
							<input id="t52portClassCode" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField " maxlength="5" data-ct="bt-text">
							</div>
								<i class="fa fa-search" onclick="portClassLookup('t52portClassCode#-RowId-#','PORT_CLASS_CODE','pol,pot1,pot2,pot3,pod');" data-onclick="portClassLookup('t52portClassCode#-RowId-#','PORT_CLASS_CODE','pol,pot1,pot2,pot3,pod');"></i>
							</div>
						</div>
						
						<rcl:number id="t52unit" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="unit"  defaultValue="0.0000" check="dec(15,4)" options="disabled"/>

						<div class=" col-md-1 pl-1 pr-0">
							<label for="t52placeOfSettlement">Pls of Settlement</label>
							<div class="rcl-standard-input-wrapper">
								<div style="padding-right:20px">
									<input id="t52placeOfSettlement" type="text" maxlength="3" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField dtlField"  onblur="hilighcolorcharges(this.id,this.value,'#-RowId-#'); hilighcolorchargesCheckOrg(this.id,this.value); " onkeyup="upc();" data-ct="bt-text" required="true">
								</div>
									<i class="fa fa-search" onclick="fnonfocus('t52placeOfSettlement','#-RowId-#'); rutOpenLookupTable('VRL_FSC','CODE','t52placeOfSettlement#-RowId-#','','','','');"></i>
							</div>
						</div>
						<input id="t52placeOfSettlement_org" class="tblField"  style="display:none;"/>
						<%-- rcl:text id="t52placeOfSettlement" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Pls of Settlement"  check="req"
						options="onblur= hilighcolorcharges(this.id,this.value,'#-RowId-#')"
						lookup="tbl-getBooking)rid-s0-bookingNo" /--%>

						<rcl:text id="t52guidelineCurrency" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="Currency" options="disabled" check="len(3)"/>
						<rcl:number id="t52guidelineRate" classes="div(col-0x75 pl-1 pr-0) ctr(dtlField)" label="Rate" options="disabled" defaultValue="0.00" check="dec(11,2)"/>
						<rcl:date id="t52effectiveDate" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Effective Date"/>
						<rcl:date id="t52expiryDate" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Expiry Date"/>
						<rcl:text id="t52billToParty" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Bill to Party"  check="len(10)" options="onchange='hilighcolorchargesCheckOrg(this.id,this.value);'"
													lookup="tbl(vrl_customers)
															rco(CODE)
															rid(t52billToParty#-RowId-#)
															sco(CODE*min[3],main)
															sva(t52billToParty#-RowId-#)
															sop(LIKE)"/>

						<input id="t52billToParty_org" class="tblField"  style="display:none;"/>

							<div id="t52selectDeleteLabel"  class="col-0x75 pl-2 pr-0" style="text-align: center;">
									<label for="t52selectDelete">
										Delete <input id="t52selectDelete" title="Removes current row from display and data" type="checkbox" class="tblField" />
									</label>
							</div>

						<div class="col-0x25 pb-0 pl-1 pr-0" style="padding-top: 15px;">
									<a
										style="font-size: 12px; font-weight: bold; color: blue; right: 0;"
										onclick="rutToggle('t52details','t52details-'+'#-RowId-#');">
										<span><i class="fas fa-chevron-down"></i></span>
									</a>
								</div>





				</div>
				</div>
			</div>



			<%--------------------------- Start additional panel --------------------------------%>
			<div id="t52details-t52row" style="display:none;">
				<div class="container-fluid" style="padding-left: 0px; margin-left: 0px;">
					<div class="row">
							<rcl:text id="t52percentCode1" classes="div(col-md-1 offset-0x25 pl-1 pr-0 mr-1) ctr(dtlField percontcode)" label="Code1"   check="len(5)" options="disabled"
										lookup="tbl(VRL_QTN_SURCHARGE)
												rco(CODE)
												rid(t52percentCode1#-RowId-#)
												sco(CODE*main)
												sva(t52percentCode1#-RowId-#)
												sop(LIKE)"/>
									<rcl:text id="t52percentCode2" classes="div(col-md-1 pl-1 pr-0 mr-1) ctr(dtlField percontcode)" label="Code2"   check="len(5)" options="disabled"
														lookup="tbl(VRL_QTN_SURCHARGE)
																rco(CODE)
																rid(t52percentCode2#-RowId-#)
																sco(CODE*main)
																sva(t52percentCode2#-RowId-#)
																sop(LIKE)"/>
									<rcl:text id="t52percentCode3" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField percontcode)" label="Code3"   check="len(5)" options="disabled"
														lookup="tbl(VRL_QTN_SURCHARGE)
																rco(CODE)
																rid(t52percentCode3#-RowId-#)
																sco(CODE*main)
																sva(t52percentCode3#-RowId-#)
																sop(LIKE)"/>
									<rcl:text id="t52percentCode4" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField percontcode)" label="Code4"  check="len(5)" options="disabled"
														lookup="tbl(VRL_QTN_SURCHARGE)
																rco(CODE)
																rid(t52percentCode4#-RowId-#)
																sco(CODE*main)
																sva(t52percentCode4#-RowId-#)
																sop(LIKE)"/>
									<rcl:text id="t52percentCode5" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField percontcode)" label="Code5"  check="len(5)" options="disabled"
														lookup="tbl(VRL_QTN_SURCHARGE)
																rco(CODE)
																rid(t52percentCode5#-RowId-#)
																sco(CODE*main)
																sva(t52percentCode5#-RowId-#)
																sop(LIKE)"/>
							<div class="col-md-1 pl-1 pr-0" style="text-align: center">
								<label>Rev.Flag</label>
								<div style="margin-top: -5px;">
									<input type="checkbox" id="t52revisedFlag" class="tblField"	/>
								</div>
							</div>
							<rcl:text id="t52revisedBy" classes="div(col-1x5 pl-1 pr-0) ctr(dtlField)" options="disabled" check="len(10)" label="Rev. By" />
							<div class="col-0x5 pl-1 pr-0" style="text-align: center;">
							<label>Latest</label>
								<div style="text-align: center; margin-top: -5px;">
									<input type="checkbox" id="t52latestFlag" class="tblField"  checked/>
								</div>
							</div>
							<div class="col-0x5 pl-1 pr-0" style="text-align: center">
							<label>Print</label>
								<div  style="margin-top: -5px;">
									<input type="checkbox" id="t52printFlag" class="tblField" onclick="disableSubjToChg('#-RowId-#');" checked/>
								</div>

							</div>
							<div class="col-md-1 pl-1 pr-0" style="text-align: center">
							<label>Sub. To Change</label>
								<div style="margin-top: -5px;">
									 <input type="checkbox" id="t52subjectToChange" class="tblField"/>
								</div>
							</div>
							<rcl:select id="t52chargeOrDiscountFlag" classes="div(col-md-1 pl-1 pr-0) ctr(dtlField)" label="Charge/Discount" optionsList="Charge Discount" valueList="C D"  options="disabled"/>

							<rcl:text id="t52chargeAmount" name="chargeAmount" classes="ctr(dtlField)"  options="style='display:none;'" />
							<rcl:text id="t52referenceNo" name="referenceNo" classes="ctr(dtlField)" options="style='display:none;'" />
							<rcl:text id="t52impMetFlag" name="impMetFlag" classes="ctr(dtlField)" options="style='display:none;'" />
							<rcl:text id="t52revenueTypeFlag" name="revenueTypeFlag" classes="ctr(dtlField)" options="style='display:none;'" />
							<rcl:text id="t52fromLevel" name="fromLevel" classes="ctr(dtlField)" options="style='display:none;'" />
							<rcl:text id="t52toLevel" name="toLevel" classes="ctr(dtlField)" options="style='display:none;'" />
					</div>
				</div>
			</div>

		</div>
	</div>
	<hr style="margin-top: 0px">

	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button id="_t52addBtn" type="button" class="rcl-standard-button" onclick="addchanges()">Add</button>
			<button id="_t52deleteBtn" type="button" class="rcl-standard-button" onclick="deletechanges()">Delete</button>
		</div>
	</div>
</rcl:area>


<script type="text/javascript">
var OrgDataChargesDetails = [];
var getshipmentType = "";
var getRate = {};
var getRatecharges = {};
$(document).ready(function() {

	rptTableInit("t51RoutingHeader", { disabledSortControl: true });
	rptTableInit("t52ChargesDetailsArea", { disabledSortControl: true });



});

function check_shipmentType(isvalue){

		for(var i=0; i<isvalue.length; i++){
			if(i == isSelect){
				getshipmentType = isvalue[i].shipmentType
				if(getshipmentType == 'BBK'){
					$("#_t51getrate").hide();
					$("#t51subjectGrcLabel").hide();
					$("#_t52deleteBtn").hide();
					$("#_t52addBtn").hide();
				}else{
					$("#_t51getrate").show();
					$("#t51subjectGrcLabel").show();
					$("#_t52deleteBtn").show();
					$("#_t52addBtn").show();
				}
			}
		}
	}



function fetchDataChargeTab(content){

	rptAddDataWithCache("t52ChargesDetailsArea", orderByCharge(SetAction_edit(content)));
	rptDisplayTable("t52ChargesDetailsArea");
	setDisplayCharge();
	setrateuom();
	//setRowView("t52ChargesDetailsArea","t52Seq");
	getRate = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	getRatecharges = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	IsSetDisplayRate('F');
	sethilihgcolor();
	setRowView("t52ChargesDetailsArea","t52chargeSeqNo");
	loopDisplayAll();
	OrgDataChargesDetails = JSON.stringify(rptGetDataFromDisplayAll("t52ChargesDetailsArea"));
	//setRateOption();

}

function orderByCharge(data){
	var getData = [];
	if(data.length > 0){
		for (var x = 0; x < data.length; x++) {
			data[x].orderby = x;
		}
		getData = data;
	}
	return getData;
}
	function setrateuom(){
		var chargeRow = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
		if(chargeRow.length > 0){
			for (var x = 0; x < chargeRow.length; x++) {
				var getrow = chargeRow[x].rowId.split("-");
				onchangeBasie(getrow[1],chargeRow[x].calculateBasis);
				chgCalculateBasis(chargeRow[x].rowId,chargeRow[x].calculateBasis);

			}
		}
	}


function setDisplayCharge(){

	var chargeRow = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	if(chargeRow.length > 0){
		for (var x = 0; x < chargeRow.length; x++) {
			var getrow = chargeRow[x].rowId.split("-");
			$("#t52equipmentSize-"+getrow[1]).val(chargeRow[x].equipmentSize);

			if(chargeRow[x].auto_manual == 'M' || chargeRow[x].auto_manual == ''){
				$('#t52row-'+x).toggleClass(" Newrate ");
			}
			
			//hilight color row copy quotation case
			if(chargeRow[x].flagColor != null){
				if(chargeRow[x].flagColor == 'B'){
					$('#t52row-'+x).toggleClass(" ChargCaseB ");
				}else if(chargeRow[x].flagColor == 'P'){
					$('#t52row-'+x).toggleClass(" ChargCaseP ");
				}
			}
			
		}
	}
}

function loopDisplayAll(){
	var datalist = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	for(var i=0; i<datalist.length; i++){
		var getrow = datalist[i].rowId.split("-");
		//rutSetElementValue("t52freightOrSurchageFlag-"+getrow[1], datalist[i].rowId);
		if(datalist[i].calculateBasis == '%'){
			$("#Percentage-"+getrow[1]).show();
		}

	}
}

function setChangesRowViewForNewRow(rowId, idNum, index, data){

	//setRowView("t52ChargesDetailsArea","t52Seq");
	setRowView("t52ChargesDetailsArea","t52chargeSeqNo");

}
function addchanges() {
	rptInsertNewRowBefore("t52ChargesDetailsArea", 'end');
	 var newRow = $("#t52ChargesDetailsArea").find(".tblRow").not(":last").last();
	 var data = rptGetDataFromDisplay("t52ChargesDetailsArea", newRow.attr("id"));
	 data.action = 'i';
	// data.ISadd = 'Y';
	data.auto_manual  = 'M';
	rptForeachRow('t52ChargesDetailsArea', setChangesRowViewForNewRow, false, ":last");
	setshipmenttypechager("t52ChargesDetailsArea","t52shipmentType",newRow.attr("id"));
	$('#'+newRow.attr("id")).toggleClass(" Newrate ");
	onchangeBasie(newRow.attr("id"),"");
	chgCalculateBasis(newRow.attr("id"),"");
	setRateOption();
	var getrow = newRow.attr("id").split("-");
	var rowId = getrow[1];
	rutSetElementValue("t52effectiveDate-"+rowId, $('#d10effDate').val());
	rutSetElementValue("t52expiryDate-"+rowId, $('#d10expDate').val());
	setFiledDisplay("addchanges",newRow);
	settsLocalFlag("t52tsLocalFlag-"+rowId);
	var freightOrSurchageValue = $('#t52freightOrSurchageFlag-' + rowId).val();
	if (freightOrSurchageValue === 'F') {
		setCustomTagProperty('t52commodityGroupCode-' + rowId, { dis: false });
	} else {
		setCustomTagProperty('t52commodityGroupCode-' + rowId, { dis: true });
	}

}

	function setFiledDisplay(flag,newRow){
		var getrow = newRow.attr("id").split("-");
		if(flag == 'addchanges'){
			setCustomTagProperty("t52shortDescription-"+getrow[1], {dis:true});
			setCustomTagProperty("t52portClassCode-"+getrow[1], {dis:false});
			setCustomTagProperty("t52revisedFlag-"+getrow[1], {dis:true});
			setCustomTagProperty("t52latestFlag-"+getrow[1], {dis:true});
			setCustomTagProperty("t52subjectToChange-"+getrow[1], {dis:true});

			if(flagBBK){
				setCustomTagProperty("t52printFlag-"+getrow[1], {dis:false});
				setCustomTagProperty("t52freightOrSurchageFlag-"+getrow[1], {dis:false});
				setCustomTagProperty("t52portClassCode-"+getrow[1], {dis:false});
				removeEmtryOptions("t52freightOrSurchageFlag-"+getrow[1]);
			}
		}else if(flag == "displayCD"){
			//wait
		}

	}



	function Ischksocandcoc(){
		var setrowid = "t10row-"+isSelect;
		var getcoc = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid).socCoc;

		var getChargesArray = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
			if(getChargesArray.length > 0 && getcoc != ""){
				for(var i = 0; i < getChargesArray.length; i++){
					if(getChargesArray[i].tsLocalFlag != "" && getChargesArray[i].tsLocalFlag != "null" && getChargesArray[i].tsLocalFlag != null){
						settsLocalFlag("t52tsLocalFlag-"+i);
					}

				}
			}

	}

	function settsLocalFlag(elementId){
		var setrowid = "t10row-"+isSelect;
		var coc = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid).socCoc;

		$("#"+elementId+" option").remove();
		$("#"+elementId).append(' <option value="T">T/S</option> <option value="L">Local</option>');
		if(coc == 'C'){
			document.getElementById(elementId).remove(0);
		}
	}

function setshipmenttypechager(area,field,rowid){
	var getrow = rowid.split("-");
	var getship = getRouting();
	var getRoutingHeader = rptGetDataFromDisplayAll(area);
	for(var i = 0; i < getRoutingHeader.length; i++){
		if(i == isSelect){
			rutSetElementValue(field+"-"+getrow[1], getship[i].shipmentType);
		}
	}
}

var newArrayDeleteCharges = [];
function deletechanges(){
	//console.log("allData "+JSON.stringify(rptGetDataFromDisplayAll("t52ChargesDetailsArea")));
	//console.log("t52ChargesDetailsArea ::")
	//newArrayDeleteCharges.push(HanderGetDataDeleteAll('t52ChargesDetailsArea', "selectDelete"));
	setarrayDeletAll('t52ChargesDetailsArea',newArrayDeleteCharges);
	rptTryToDeleteSelectedRows('t52ChargesDetailsArea', "selectDelete", true);
	rptForeachRow('t52ChargesDetailsArea', setChangesRowViewForNewRow, false, ":last");
}

function hiding(fieldId){
	$("#Percentage-"+fieldId).toggle();
}

function openRutup(rowid){
	var setrowid = "t10row-"+isSelect

	rutOpenLookupTable('VRL_UNIT_MEASURE','CODE','t52rateUom'+rowid,'CODE*main IMPERIAL_METRIC TYPE','t52rateUom'+rowid+ ' t10pm'+setrowid+' t52calculateBasis'+rowid,'LIKE = =','');

}



function Get_Rates(){
	var data = {};
	userData.fscCode = $('#d10pos').val();
	if(ValidateGetRate()){
	
		var quotationData = {
				userData : userData,
				//qtnData: GetqtnDatatest()
				qtnData : GetqtnData()

		};

		data = {
				referenceType : "GLR",
				referenceNo : null,
				quotationData : quotationData
		};
		console.log("GetqtnData():"+JSON.stringify(data));
		
		getResultAjax("WS_QTN_GET_RATE", data).done(handleGetRate);
		function handleGetRate(response) {
			if(response.resultCode == "200"){
					 console.log("handleDisplayDND response:"+JSON.stringify(response.resultContent));

					_addCD(response.resultContent || []);
				
			}else{
				dialogGeneric("Warning", response.resultMessage, "Ok");
			}



			}
			//GetqtnData();
		//_addCD(jsonCharge());
	}
	
}


function GetPolPodPori(object){
	var data = {};
	var polPori = "" ,podPori = "";
	if(object.rateBasis == '1'){
		 polPori = "I";
	     podPori = "P";
	}else if(object.rateBasis == '2'){
		polPori = "I";
	    podPori = "I";
	}else if(object.rateBasis == '3'){
		polPori = "P";
	    podPori = "P";
	}
	else if(object.rateBasis == '4'){
		 polPori = "P";
	     podPori = "I";
	}
	data.polPori = polPori;
	data.podPori = podPori;

	return data
}

function ValidateGetRate(){
	var resulet = true;
	var strMsg = "";
	var strTab = "";
	var term = rptGetDataFromDisplay("t10shipTypeDetailArea", "t10row-"+isSelect).term;
	if(term == ""){
		resulet = false;
		strMsg = "*Term is mandatory";
		strTab = "Maintenance";
	}
	
	var containerArea = rptGetDataFromDisplayAll("t31containerArea");
	if(containerArea.length < 1){
		resulet = false;
		strMsg = "*Container Detail is mandatory";
		strTab = "ContainerCommodity";
	}
	
	if(!resulet){
		dialogGenericv3(strTab,"Warning", strMsg , "Ok");
	}
	return resulet;
}

function GetqtnData(){
	var getMaintainacetab = rptGetDataFromSingleArea("d10MaintenanceHeader");
	var getRoutingHeader = rptGetDataFromDisplayAll("t10shipTypeDetailArea");

	var getData = "";
	var data = {};

	var polpodPori = {};
	for(var i = 0; i < getRoutingHeader.length; i++){
		if(i == isSelect){
			polpodPori = GetPolPodPori(getRoutingHeader[i]);
			//getData.test = "filmtest";
			var dgApprovalId = "";
			if(getRoutingHeader[i].inqType == "D"){
				dgApprovalId = getRoutingHeader[i].oogBkkInq;
				}
			data = {
			  "corridorSeq": isSelect,
			  "pickupDepot":getRoutingHeader[i].pickupDepot,//New,
			  "dropOffDepot":getRoutingHeader[i].dropOffDepot,//New,
			  "facApplicable":getRoutingHeader[i].facApplicable,//New
			  "ucRateBase": getRoutingHeader[i].rateBasis,//New
			  "effectiveDate": splitDate(getMaintainacetab.effDate),
		      "expiryDate": splitDate(getMaintainacetab.expDate),
		      "function": getMaintainacetab.custType,
		      "por": getRoutingHeader[i].por,
		      "pol": getRoutingHeader[i].pol,
		      "pot1": getRoutingHeader[i].pot1,
		      "pot2": getRoutingHeader[i].pot2,
		      "pot3": getRoutingHeader[i].pot3,
		      "pod": getRoutingHeader[i].pod,
		      "del": getRoutingHeader[i].del,
		      "soc_Coc": getRoutingHeader[i].socCoc,
		      "term": getRoutingHeader[i].term,
		      "rateBasis": getRoutingHeader[i].rateBasis,
		      "shipmentType": getRoutingHeader[i].shipmentType,
		      "containerDtl": GetCrn(),
		      "commodityDtl": GetComm(),
		      "porHaul": getRoutingHeader[i].porHaul,
		      "delHaul": getRoutingHeader[i].delHaul,
		      "facApplicable": getRoutingHeader[i].facApplicable,
		      "tariffExists": null,
		      "shipmentType1Or2": null,
		      "polPori": polpodPori.polPori,
		      "podPori": polpodPori.podPori,
		      "pm": getRoutingHeader[i].pm,
		      "weight": getRoutingHeader[i].weight,
		      "measurement": getRoutingHeader[i].measurement,
		      "area": getRoutingHeader[i].rorArea,
		      "shtp2Weight": null,
		      "shtp2Measurement": null,
		      "shtp2Area": null,
		      "polEta": null,
		      "polEtd": null,
		      "podEta": null,
		      "podEtd": null,
		      "exRateTrxCode": "QTN",
		      "roeTable": getMaintainacetab.roeTable,
		      "exRateProceedFlag": "R",
		      "userFsc": "",/*****************************/
		      "qtnCreateDate": "",/*****************************/
		      "corridorRateBasis": getRoutingHeader[i].rateBasis,
		      "routing" : getRouting_New(),
		      "dgApprovalId" : dgApprovalId
			};
		}
	}

	return data;
}

function GetCrn(){

	var Newoject = [];
	var getContainer = rptGetDataFromDisplayAll("t31containerArea");
	for(var z = 0; z < getContainer.length; z++){
		var data = {};
		data.equipmentSize = getContainer[z].size
        data.equipmentType = getContainer[z].type
        data.rateType = getContainer[z].rateType
        data.fullQty = getContainer[z].fullQty
        data.emptyQty = getContainer[z].emptyQty
        data.voildSlot = getContainer[z].voidSlot
        data.polStatus = getContainer[z].polStatus
        data.podStatus = getContainer[z].podStatus
        data.bundleQty = getContainer[z].bundle
         Newoject.push(data);
     }

	//console.log("GetCrn :: " + JSON.stringify(Newoject));
	return Newoject;
}

function GetComm(){

	var Newoject = [];
	var getCommSection = rptGetDataFromDisplayAll("t32commodityArea");
	for(var z = 0; z < getCommSection.length; z++){
		var data = {};
		data.commGroup = getCommSection[z].commGroup
        data.commCode = getCommSection[z].commCode
        data.commMeasurement = getCommSection[z].measurement
        data.commShipmentType = getCommSection[z].shipmentType
        data.portClass = getCommSection[z].portClass
        data.commArea = getCommSection[z].rorArea
        data.pkgs = getCommSection[z].pkgs
        data.commWeight = getCommSection[z].weight
        data.commRateType = getCommSection[z].rateType
        data.imdgClass = getCommSection[z].imdgClass
        //New parameter
        data.commoditySeqNo = getCommSection[z].commoditySeqNo
        data.reefer = getCommSection[z].reefer
        data.unno = getCommSection[z].unno
        data.variant = getCommSection[z].variant
        data.oogFlag = getCommSection[z].oogFlag
        data.portClass = getCommSection[z].portClass


        Newoject.push(data);
	}

	//console.log("GetComm :: " + JSON.stringify(Newoject));
	return Newoject;
}


function _addCD(cdRoute){
	getRate = cdRoute;
	getRatecharges = cdRoute;
	rptClearDisplay("t52ChargesDetailsArea");
	rptAddDataWithCache("t52ChargesDetailsArea", SetAction_insert_charge(cdRoute));
	rptDisplayTable("t52ChargesDetailsArea");
	setRowView("t52ChargesDetailsArea","t52chargeSeqNo");
	setDisplayCharge();
	IsManetSetDisplayChargeAll_V2();
	IsSetDisplayRate('R');
	Ischksocandcoc();
	//setCustomTagProperty("t52"+k+"-"+a, {dis:true});
	/*setDisplayCharge();*/
	/*var setcharg = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	if(setcharg.length > 0){
		for(var i = 0; i < setcharg.length; i++){
			//setFiledDisplay("displayCD","rowid-"+i);
			console.log(setcharg[i].chargeSeqNo)
		}
	}*/
}

function SetAction_insert_charge(object){
	console.log("SetAction_insert_charge");
	if(object.length > 0){
		for(var i = 0; i < object.length; i++){
			object[i].action = 'i';
			object[i].auto_manual = 'A';
			object[i].equipmentSize = object[i].equipmentSize.toString();
		}
	}

	return object;
}

function getChargeTabOutput(){

	var ArrayChargesDetail = [];
	ArrayChargesDetail = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	console.log("ArrayChargesDetail :: "+JSON.stringify(ArrayChargesDetail));
	if(!isEmpty(newArrayDeleteCharges)){
		for(var i = 0;i < newArrayDeleteCharges.length; i++){
			if(!isEmpty(newArrayDeleteCharges[i])){
				ArrayChargesDetail.push(newArrayDeleteCharges[i]);
			}
		}
	}
	ArrayChargesDetail = converntDateBeforeChkChangeData_Charge(ArrayChargesDetail);

	var objChargesDetail = CheckDataOnChange_test(OrgDataChargesDetails,ArrayChargesDetail,"orderby");
	/*console.log("OrgDataChargesDetails :: "+JSON.stringify(OrgDataChargesDetails));
	console.log("ArrayChargesDetail :: "+JSON.stringify(ArrayChargesDetail));*/
	for (i = 0; i < objChargesDetail.length; i++) {
		objChargesDetail[i].effectiveDate = splitDate(objChargesDetail[i].effectiveDate);
		objChargesDetail[i].expiryDate = splitDate(objChargesDetail[i].expiryDate);

		if(objChargesDetail[i].subjectToChange == ""){
			objChargesDetail[i].subjectToChange = 'N';
		}
	}



	console.log("getChargeTabOutput :: "+JSON.stringify(objChargesDetail));

	return objChargesDetail;
}

function converntDateBeforeChkChangeData_Charge(Isdata){

	for (var x = 0; x < Isdata.length; x++) {
		Isdata[x].effectiveDate = conCatDate(Isdata[x].effectiveDate);
		Isdata[x].expiryDate = conCatDate(Isdata[x].expiryDate);

	}
	return Isdata;
}

function CheckDataOnChange_test(oldData,newDate,sortBy){
	var NewArraySetAction = [];
	    NewArraySetAction = newDate;

	var GetDatatest = newDate;
	    if(!isEmpty(oldData)){
			    oldData = JSON.parse(oldData);
			    if(NewArraySetAction.length == oldData.length){
			 		if(!isEmpty(oldData) && !isEmpty(NewArraySetAction)){

							NewArraySetAction.sort(function(a, b) {
								var setSortA = eval("a."+sortBy);
					 			var setSortB = eval("b."+sortBy);

								var x = setSortA, y = setSortB;

					 		   return x < y ? -1 : x > y ? 1 : 0;
					 	  });


			    	for(var b = 0; b < oldData.length; b++){ //Loop ORG DATA for get object.
			    		  if(!jsonEqual(JSON.stringify(oldData[b]), NewArraySetAction[b])){ // check Diff Json object.
			    			if(NewArraySetAction[b].action != 'i' && NewArraySetAction[b].action != 'd'){ //Add action u
			   			 		NewArraySetAction[b].action = "u";
				    	    }
			    		 }
			    	 }
			 	}
			}
	    }
       return NewArraySetAction;
 }


function ischeckfscChange(){
	ischeckfscroutingChange();
	if(!flagBBK){
		var HedChange = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
		for (var z = 0; z < HedChange.length; z++) {
			if(HedChange[z].action != 'i'){
				setCustomTagProperty("t52lastest-"+z, {dis:true});
				setCustomTagProperty("t52print-"+z, {dis:true});
				$("#t52selectDeleteLabel-"+z).remove();
				removeGeneralSearchButton("t52equipmentType-"+z);
				removeGeneralSearchButton("t52freightOrSurchargeCode-"+z);
				removeGeneralSearchButton("t52rateUom-"+z);
				removeGeneralSearchButton("t52currency-"+z);
				removeGeneralSearchButton("t52placeOfSettlement-"+z);
				removeGeneralSearchButton("t52billToParty-"+z);
				removeGeneralSearchButton("t52percentCode1-"+z);
				removeGeneralSearchButton("t52percentCode2-"+z);
				removeGeneralSearchButton("t52percentCode3-"+z);
				removeGeneralSearchButton("t52percentCode4-"+z);
				removeGeneralSearchButton("t52percentCode5-"+z);
				$.each(HedChange[z], function(k, v) {
					setCustomTagProperty("t52"+k+"-"+z, {dis:true});
				});
			}

		}
	}
	//IsSetDisplayRate

	if(flagBBK){
		$("#_t52deleteBtn").show();
		$("#_t52addBtn").show();
	}else{
		$("#_t52deleteBtn").remove();
		$("#_t52addBtn").remove();
	}
}

function ischeckfscroutingChange(){
	var HedRouting = rptGetDataFromDisplayAll("t51RoutingHeader");
	for (var x = 0; x < HedRouting.length; x++) {
		setCustomTagProperty("t51transShipment-"+x, {dis:true});
	}
	$("#_t51getrate").remove();
	//$("#t51subjectGrcLabel").remove();
	setCustomTagProperty("subjRRFlag", {dis:true});
}


function hilighcolorcharges(isid,isval,rowid){ //comment wait
	/*if(getRate.length > 0){
		for (var x = 0; x < getRate.length; x++) {
			if(rowid == getRate[x].rowId){
				$.each(getRate[x], function(k, v) {
					var setid = "t52"+k+"-"+x;
					if(setid == isid){
						if(isval != v){
							$('#'+isid).addClass('Newrate');
						}else{
							$('#'+isid).removeClass('Newrate');
						}
					}
				});
			}
		}
	}*/
}

function hilighcolorchargesCheckOrg(isid,isval){
	var rowid = isid.split("-");
		rowid = "t52row-"+rowid[1];
	for (var x = 0; x < getRatecharges.length; x++) {
			if(rowid == getRatecharges[x].rowId){
				$.each(getRatecharges[x], function(k, v) {
					var setid = "t52"+k+"-"+x;
					if(isid == 't52rate-'+x){//rate,guidelineRate
						if(setid == 't52guidelineRate-'+x){
							checkhiligh(isid,isval,v)
						}
					}
					if(isid == 't52currency-'+x){//currency,guidelineCurrency
						if(setid == 't52guidelineCurrency-'+x){
							checkhiligh(isid,isval,v)
						}
					}
					if(isid == 't52prepaidOrCollect-'+x){//prepaidOrCollect,prepaidOrCollect_org
						if(setid == 't52prepaidOrCollect_org-'+x){
							checkhiligh(isid,isval,v)
						}
					}
					if(isid == 't52placeOfSettlement-'+x){//placeOfSettlement,placeOfSettlement_org
						if(setid == 't52placeOfSettlement_org-'+x){
							checkhiligh(isid,isval,v)
						}
					}
					if(isid == 't52billToParty-'+x){//placeOfSettlement,placeOfSettlement_org
						if(setid == 't52billToParty_org-'+x){
							checkhiligh(isid,isval,v)
						}
					}

				});
			}
		}


}

	function sethilihgcolor(){
		for (var x = 0; x < getRatecharges.length; x++) {
			checkhiligh("t52rate-"+x,getRatecharges[x].rate,getRatecharges[x].guidelineRate);
			checkhiligh("t52currency-"+x,getRatecharges[x].currency,getRatecharges[x].guidelineCurrency);
			checkhiligh("t52prepaidOrCollect-"+x,getRatecharges[x].prepaidOrCollect,getRatecharges[x].prepaidOrCollect_org);
			checkhiligh("t52placeOfSettlement-"+x,getRatecharges[x].placeOfSettlement,getRatecharges[x].placeOfSettlement_org);
			checkhiligh("t52billToParty-"+x,getRatecharges[x].billToParty,getRatecharges[x].billToParty_org);
		}
	}

	function checkhiligh(isid,isval,isV){
		//console.log("sethilihgcolor :: "+isid + " >> "+isval+ " >> "+isV)
		if(isval != isV){
			$('#'+isid).addClass('Newrate');
		}else{
			$('#'+isid).removeClass('Newrate');
		}
	}

	function checkfsc(isid,isval){
		fnonfocus('t52placeOfSettlement',isid);
		var rowId = isid.split("-");
		var getpor = '';
		var dataRequest = {};

		if(isval == 'P'){
			getpor = $('#t51pol-'+isSelect).val();
		}else{
			getpor = $('#t51pod-'+isSelect).val();
		}

		dataRequest = {
							"userData" : userData,
							"por" : getpor
					  }

		getResultAjax("WS_QTN_GET_FSC_PORT", dataRequest).done(handleDisplaytPlaces);

		function handleDisplaytPlaces(response){
			if(response.resultCode == "200"){
				$('#t52placeOfSettlement-'+rowId[1]).val(response.resultContent.fscpor);
				fnonfocus('t52prepaidOrCollect',isid);
			}else if(response.resultCode == '401'){

				window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
				
			}else{
				dialogGeneric("Warning", response.resultMessage, "Ok");
			}
		}
	}

function IsSetDisplayRate(flag){
	//console.log("getRate ::"+JSON.stringify(getRatecharges));
	/*if(getRate.length > 0){
		for (var x = 0; x < getRate.length; x++) {
			var rowId = getRate[x].rowid.split("-");
			if(getRate[x].org_rate != ""){
				$('#t52rate-'+rowId).addClass('Newrate');
			}
			if(getRate[x].org_currency != ""){
				$('#t52currency-'+rowId).addClass('Newrate');
			}
			if(getRate[x].org_placeOfSettlement != ""){
				$('#t52placeOfSettlement-'+rowId).addClass('Newrate');
			}
		}
	}*/

	for (var a = 0; a < getRatecharges.length; a++) {
			$.each(getRatecharges[a], function(k, v) {
				if(flagBBK){
					if(k == "tsLocalFlag" || k == "effBasis"
						|| k == "currency" || k == "rate"
						|| k == "placeOfSettlement" || k == "printFlag" || k == "billToParty"
						|| k == "prepaidOrCollect" || k == "selectDelete"){

						setCustomTagProperty("t52"+k+"-"+a, {dis:false});
					}else{
						setCustomTagProperty("t52"+k+"-"+a, {dis:true});
					}

					fnonchangefreig('row-'+a,getRatecharges[a].freightOrSurchageFlag,"E");

				}else{
					if(k != "placeOfSettlement" && k != "currency" && k != "rate"&& k != "prepaidOrCollect" && k != "billToParty"){
							setCustomTagProperty("t52"+k+"-"+a, {dis:true});

						}

					if(getRatecharges[a].auto_manual == 'A'){
						setCustomTagProperty("t52selectDelete-"+a, {dis:true});
					}



				}
			});

			if(getRatecharges[a].auto_manual == 'M'){
				if(getRatecharges[a].freightOrSurchageFlag == 'S'){
					//console.log("test >>" +getRatecharges[a].freightOrSurchageFlag)
					setCustomTagProperty("t52effectiveDate-"+a, {dis:false});
					setCustomTagProperty("t52expiryDate-"+a, {dis:false});
					setCustomTagProperty("t52orgDestTs-"+a, {dis:false});
				}

				resetValueForRateAppliedTo("t52orgDestTs-"+a,getRatecharges[a].orgDestTs);
			}

			disablePrint('t52row-'+a);
			disableSubjToChg('t52row-'+a);
		}

}



function setRateOption(){
	$(".rateType option[value='O7']").remove();
}

function IsSetDisplayChargeAll_V2(id,value){
	if(value == 'F'){
		setCustomTagProperty("t52revisedFlag-"+id, {dis:true});
		setCustomTagProperty("t52latestFlag-"+id, {dis:true});
		setCustomTagProperty("t52printFlag-"+id, {dis:true});
		setCustomTagProperty("t52subjectToChange-"+id, {dis:true});
	}else{
		setCustomTagProperty("t52revisedFlag-"+id, {dis:true});
		setCustomTagProperty("t52latestFlag-"+id, {dis:true});
		setCustomTagProperty("t52printFlag-"+id, {dis:false});
		setCustomTagProperty("t52subjectToChange-"+id, {dis:false});
		disablePrint('rowid-'+id);
	}
}


function disableSubjToChg(rateId){
	var autoOrManual = rptGetDataFromDisplay("t52ChargesDetailsArea", rateId).auto_manual;
	var getrowid = rateId.split("-");

	if(document.getElementById("t52printFlag-"+getrowid[1]).checked){
		if(document.getElementById("t52freightOrSurchageFlag-"+getrowid[1]).value=="F" || autoOrManual=="M"){
			document.getElementById("t52subjectToChange-"+getrowid[1]).checked=false;
			document.getElementById("t52subjectToChange-"+getrowid[1]).disabled=true;
		}else{
			document.getElementById("t52subjectToChange-"+getrowid[1]).disabled=false;
			if($('#t52tsLocalFlag-'+getrowid[1]).val() != 'L'){
				document.getElementById("t52subjectToChange-"+getrowid[1]).checked=true;
			}
		}
	}else{
		document.getElementById("t52subjectToChange-"+getrowid[1]).checked=false;
		document.getElementById("t52subjectToChange-"+getrowid[1]).disabled=true;
	}
}

function disablePrint(rowid){
	var getrow = rowid.split("-");
	if(flagBBK){
		setCustomTagProperty("t52printFlag-"+getrow[1], {dis:false});
		setCustomTagProperty("t52subjectToChange-"+getrow[1], {dis:true});
	}else{

		if($('#t52freightOrSurchageFlag-'+getrow[1]).val() != 'F'){
			if($('#t52calculateBasis-'+getrow[1]).val() == 'X'){
				setCustomTagProperty("t52printFlag-"+getrow[1], {dis:true});
				setCustomTagProperty("t52subjectToChange-"+getrow[1], {dis:true});
				document.getElementById("t52subjectToChange-"+getrow[1]).checked=false;
			}else{

				setCustomTagProperty("t52printFlag-"+getrow[1], {dis:false});
			}
			if($('#t52tsLocalFlag-'+getrow[1]).val() != 'L'){
				setCustomTagProperty("t52printFlag-"+getrow[1], {dis:true});
			}
		}else{
			setCustomTagProperty("t52printFlag-"+getrow[1], {dis:true});
			setCustomTagProperty("t52subjectToChange-"+getrow[1], {dis:true});
			document.getElementById("t52subjectToChange-"+getrow[1]).checked=false;
		}
	}

}

function checkorgDestTs(id,value){
	/*var getrow = id.split("-");
	if(flagBBK){
		if(value == 'T'){
			setCustomTagProperty("t52effBasis-"+getrow[1], {dis:true});
			setCustomTagProperty("t52tsLocalFlag-"+getrow[1], {dis:true});
		}else{
			setCustomTagProperty("t52effBasis-"+getrow[1], {dis:false});
			setCustomTagProperty("t52tsLocalFlag-"+getrow[1], {dis:false});
		}
	}*/
}

function resetValueForRateAppliedTo(rowId, rowRate) {

	var getrow = rowId.split("-");
	console.log("resetValueForRateAppliedTo  " +$('#t52freightOrSurchageFlag-'+getrow[1]).val() )
	if($('#t52freightOrSurchageFlag-'+getrow[1]).val() == 'S'){
		if(rowRate!='T'){
			setCustomTagProperty("t52tsLocalFlag-"+getrow[1], {dis:false});
			setCustomTagProperty("t52effBasis-"+getrow[1], {dis:false});
		}else {
			setCustomTagProperty("t52tsLocalFlag-"+getrow[1], {dis:true});
			setCustomTagProperty("t52effBasis-"+getrow[1], {dis:true});
			document.getElementById("t52tsLocalFlag-"+getrow[1]).value="T";
			document.getElementById("t52effBasis-"+getrow[1]).value="APOD";
		}
	}
}

function fnonchangefreig(id,value,flag){
	var getrow = id.split("-");

	if(value == 'F'){
		$("#t52calculateBasis-"+getrow[1]+" option[value='%']").remove();
		$("#t52rateType-"+getrow[1]).append("<option value='O7'>Void Slot</option>");
		fnsetfildechangeFreig(getrow[1],true);
	}else{
		$("#t52calculateBasis-"+getrow[1]).append("<option value='%'>Percent</option>");
		$("#t52rateType-"+getrow[1]+" option[value='O7']").remove();
		fnsetfildechangeFreig(getrow[1],false);
	}

	if(flag == undefined){
		document.getElementById("t52rateType-"+getrow[1]).value='D1';
		document.getElementById("t52freightOrSurchageFlag-"+getrow[1]).readOnly=false;
		setCustomTagProperty("t52calculateBasis-"+getrow[1], {dis:false});
		document.getElementById("t52calculateBasis-"+getrow[1]).value='B';
		setCustomTagProperty("t52freightOrSurchargeCode-"+getrow[1], {dis:false});
	}


}


function fnsetfildechangeFreig(id,falg){
	if(falg){
		setCustomTagProperty("t52commodityGroupCode-"+id, {dis:false});
		setCustomTagProperty("t52tsLocalFlag-"+id, {dis:true});
		setCustomTagProperty("t52effBasis-"+id, {dis:true});
		setCustomTagProperty("t52portClassCode-"+id, {dis:true});
		setCustomTagProperty("t52orgDestTs-"+id, {dis:true});
		setCustomTagProperty("t52effectiveDate-"+id, {dis:true});
		setCustomTagProperty("t52expiryDate-"+id, {dis:true});
	}else{
		setCustomTagProperty("t52commodityGroupCode-"+id, {dis:true});
		setCustomTagProperty("t52tsLocalFlag-"+id, {dis:false});
		setCustomTagProperty("t52effBasis-"+id, {dis:false});
		setCustomTagProperty("t52orgDestTs-"+id, {dis:false});
		setCustomTagProperty("t52effectiveDate-"+id, {dis:false});
		setCustomTagProperty("t52expiryDate-"+id, {dis:false});
	}
}

function fnonchangeratecharge(id,value){
	var getrow = id.split("-");
	if(value == 'O7'){
		$("#t52freightOrSurchargeCode-"+getrow[1]).val("VOIDB");
		$("#t52calculateBasis-"+getrow[1]).val("T");
		setCustomTagProperty("t52calculateBasis-"+getrow[1], {dis:true});
		$("#t52shortDescription-"+getrow[1]).val("OID SLOT BBK");
		$("#t52chargeOrDiscountFlag-"+getrow[1]).val("C");
		setCustomTagProperty("t52portClassCode-"+getrow[1], {dis:true});
	}else if(value == 'D1'){
		setCustomTagProperty("t52portClassCode-"+getrow[1], {dis:false});
	}else{
		setCustomTagProperty("t52portClassCode-"+getrow[1], {dis:true});
		setCustomTagProperty("t52calculateBasis-"+getrow[1], {dis:false});
	}
}

function IsManetSetDisplayChargeAll_V2(){
	var data = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	if(data.length > 0){
		for(var i = 0; i < data.length; i++){
			IsSetDisplayChargeAll_V2(i,data[i].freightOrSurchageFlag);
		}
	}
}

function onchangeBasie(rowid,val){
	/*var getrow = rowid.split("-");
	if(val == '%'){
		setCustomTagProperty("t52percentCode1-"+getrow[1], {dis:false});
		setCustomTagProperty("t52percentCode2-"+getrow[1], {dis:false});
		setCustomTagProperty("t52percentCode3-"+getrow[1], {dis:false});
		setCustomTagProperty("t52percentCode4-"+getrow[1], {dis:false});
		setCustomTagProperty("t52percentCode5-"+getrow[1], {dis:false});

	}else{
		setCustomTagProperty("t52percentCode1-"+getrow[1], {dis:true});
		setCustomTagProperty("t52percentCode2-"+getrow[1], {dis:true});
		setCustomTagProperty("t52percentCode3-"+getrow[1], {dis:true});
		setCustomTagProperty("t52percentCode4-"+getrow[1], {dis:true});
		setCustomTagProperty("t52percentCode5-"+getrow[1], {dis:true});
	}

	if(val == "M" || val == "W"){
		setCustomTagProperty("t52rateUom-"+getrow[1], {dis:false});
		$("#lookupMEASURE-"+getrow[1]).show();
	}else{
		setCustomTagProperty("t52rateUom-"+getrow[1], {dis:true});
		$("#lookupMEASURE-"+getrow[1]).hide();
	}

	disablePrint(rowid);*/

}

function chgCalculateBasis(rowid,val){
	var getrow = splitId(rowid,'-');
	if(val == "X"){
		setCustomTagProperty("t52printFlag-"+getrow, {dis:true});
		document.getElementById("t52printFlag-"+getrow).checked=true;
		console.log("t52freightOrSurchageFlag :: "+$("#t52freightOrSurchageFlag-"+getrow).val())
		if($("#t52freightOrSurchageFlag-"+getrow).val() == 'F'){
			document.getElementById("t52tsLocalFlag-"+getrow).value="";
			setCustomTagProperty("t52tsLocalFlag-"+getrow, {dis:true});

			document.getElementById("t52portClassCode-"+getrow).value="";
			setCustomTagProperty("t52portClassCode-"+getrow, {dis:true});

		}else if($("#t52freightOrSurchageFlag-"+getrow).val() == 'S'){
			//document.getElementById("t52tsLocalFlag-"+getrow).value="hidden";
			setCustomTagProperty("t52tsLocalFlag-"+getrow, {dis:true});

			document.getElementById("t52portClassCode-"+getrow).value="";
			setCustomTagProperty("t52portClassCode-"+getrow, {dis:true});
		}

	}else{
		setCustomTagProperty("t52printFlag-"+getrow, {dis:false});

		if($("#t52freightOrSurchageFlag-"+getrow).val()=='F'){
			document.getElementById("t52tsLocalFlag-"+getrow).value="L";
			setCustomTagProperty("t52tsLocalFlag-"+getrow, {dis:true});

			document.getElementById("t52portClassCode-"+getrow).value="";
			setCustomTagProperty("t52portClassCode-"+getrow, {dis:true});

		}else if($("#t52freightOrSurchageFlag-"+getrow).val()=='S'){

			if($("#t52rateType-"+getrow).val()=='D1'){

				setCustomTagProperty("t52portClassCode-"+getrow, {dis:false});
			}

			if($("#t10socCoc-"+isSelect).val() == 'S'){
				if(document.getElementById("t52tsLocalFlag-"+getrow).value == ""){
					//document.getElementById("t52tsLocalFlag-"+getrow).value = "T";
					document.getElementById("t52tsLocalFlag-"+getrow).value="T";
				}
				//document.getElementById("t52tsLocalFlag-"+getrow).style.visibility="visible";
				setCustomTagProperty("t52tsLocalFlag-"+getrow, {dis:false});
			}else if($("#t10socCoc-"+isSelect).val()  == 'C'){
				document.getElementById("t52tsLocalFlag-"+getrow).value="L";
				setCustomTagProperty("t52tsLocalFlag-"+getrow, {dis:false});
				/*document.getElementById("t52tsLocalFlag-"+getrow).value="L";
				document.getElementById("t52tsLocalFlag-"+getrow).style.visibility="visible";*/
			}
		}
	}


	if(val == "M" || val == "W"){
		setCustomTagProperty("t52rateUom-"+getrow, {dis:false});
		$("#lookupMEASURE-"+getrow).show();
	}else{
		setCustomTagProperty("t52rateUom-"+getrow, {dis:true});
		$("#lookupMEASURE-"+getrow).hide();
	}


	if(val == '%'){
		setCustomTagProperty("t52percentCode1-"+getrow, {dis:false});
		setCustomTagProperty("t52percentCode2-"+getrow, {dis:false});
		setCustomTagProperty("t52percentCode3-"+getrow, {dis:false});
		setCustomTagProperty("t52percentCode4-"+getrow, {dis:false});
		setCustomTagProperty("t52percentCode5-"+getrow, {dis:false});

	}else{
		setCustomTagProperty("t52percentCode1-"+getrow, {dis:true});
		setCustomTagProperty("t52percentCode2-"+getrow, {dis:true});
		setCustomTagProperty("t52percentCode3-"+getrow, {dis:true});
		setCustomTagProperty("t52percentCode4-"+getrow, {dis:true});
		setCustomTagProperty("t52percentCode5-"+getrow, {dis:true});
	}


}

function portClassLookup(field,retrunfiled,fieldIn){
	var colInArray = [];
	var colIn = fieldIn.replace(/,/g, "','");
	colIn = eval("['"+colIn+"']");
	var getValueIn = "";
	for(var i = 0; i < colIn.length; i++){
		
			if(i < (colIn.length - 1)){
				var getvalue = $('#t10'+colIn[i]+'-'+isSelect).val();
				if(getvalue != ""){
					getValueIn += "'"+getvalue+"',";
				}
			}else{
				var getvalue = $('#t10'+colIn[i]+'-'+isSelect).val();
				if(getvalue != ""){
					getValueIn += "'"+getvalue+"'";
				}
			}
	}
	var conCatValue = "("+getValueIn+")";
	qtn_OpenLookupTable('VRL_BKG_PORT_CLASS'
			,retrunfiled
			,field
			,'HIDE_PORT_CODE'
			,conCatValue
			,'IN'
			,''
			,'','');
}

function IsChkSubtoChange(rowid,val){
	var getrow = splitId(rowid,'-');
	var setrowid = "t52row-"+getrow;
	var getAction = rptGetDataFromDisplay("t52ChargesDetailsArea", setrowid).action;
	if(getAction != 'i'){
		if($("#t52rate-"+getrow).val() != $("#t52guidelineRate-"+getrow).val()){
			document.getElementById("t52subjectToChange-"+getrow).checked=false;
		}else{
			document.getElementById("t52subjectToChange-"+getrow).checked=true;
		}
	}
		
}

</script>