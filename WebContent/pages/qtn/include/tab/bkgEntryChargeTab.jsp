<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%
	boolean useCostRecovery = true;
%>

<!--  Charges Header -->
<rcl:area id="d40chargesHeader" classes="div(container-fluid)" title="Charges" collapsible='Y'>
	<div class="row">
		<rcl:text id="d40bookingNo" classes="div(col-md-2) ctr(dtlField)"
			label="Booking#" options="disabled" check="upc" />
		<rcl:text id="d40bkType" classes="div(col-md-1) ctr(dtlField)"
			label="BK Type" options="disabled" check="upc" />
		<rcl:text id="d40status" classes="div(col-md-1) ctr(dtlField)"
			label="Status" options="disabled" />
		<rcl:text id="d40por" classes="div(col-md-1) ctr(dtlField)"
			label="POR" options="disabled" check="upc" />
		<rcl:text id="d40pol" classes="div(col-md-1) ctr(dtlField)"
			label="POL" options="disabled" check="upc" />
		<rcl:text id="d40pod" classes="div(col-md-1) ctr(dtlField)"
			label="POD" options="disabled" check="upc" />
		<rcl:text id="d40del" classes="div(col-md-1) ctr(dtlField)"
			label="DEL" options="disabled" check="upc" />
		<rcl:text id="d40shipmentType" classes="div(col-md-2) ctr(dtlField)"
			label="Shipment Type" options="disabled" check="upc" />
	</div>
	<div class="row">
		<div class="col-md-12" style="text-align:center;">
			<button type="button" class="rcl-standard-button">Get Rates</button>
		</div>
	</div>
</rcl:area>


<!--  Charges Detail -->
<!-- If auto gen, readonly except ppd/col, coll FSC, Bill To Party. Record is not deletable -->
<div id="t40chargesListHeader" class="rcl-standard-widget-header tblHeader">Charges Detail</div>
<div id="t40chargesListArea" class="container-fluid rcl-standard-search-header tblArea">
	<div id="t40chargesListRow" class="row tblRow">
		<span id="t40seqNo" class="tblField pk" style="display:none;"></span>
		<rcl:number id="t40rowNo" classes="div(col-sm-1 col-md-1 pr-0) ctr(tblField)" 
			label="Seq#" options="disabled" />
		<rcl:text id="t40fs" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)" 
			label="F/S" check="req" options="disabled" /> <!-- Flag value with display -->
		<rcl:text id="t40fsCode" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)" 
			label="F/S Code" check="req len(5) upc" 
			lookup="tbl(SURCHARGE_CODE)" />
		<rcl:text id="t40chargeDescription" classes="div(col-sm-4 col-md-2 pr-0) ctr(tblField)" 
			label="Charge Description" check="req" options="disabled" />
		<rcl:text id="t40commodity" classes="div(col-sm-3 col-md-2 pr-0) ctr(tblField)" 
			label="Commodity" options="disabled" />
		<!-- Default to booking's -->
		<rcl:select id="t40shipmentType" classes="div(offset-sm-1 offset-md-0 col-sm-2 col-md-1 pr-0) ctr(tblField pk)"
			label="Shtp." check="req" selectTable="ShipmentType" options="disabled"/>
		<!-- FCL/BBK only, Need option 0, seems to pair with ** type -->
		<rcl:select id="t40size" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)" 
			label="Size" check="req" selectTable="EqSize" />
		<!-- FCL/BBK only -->
		<rcl:text id="t40equipmentType" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)" 
			label="Type" check="req len(3) upc" 
			lookup="tbl(EQUIP_TYPE)" />
		<!-- Need Empty option, FCL/BBK only, Reefer should not able to select manually -->
		<rcl:select id="t40rateType" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)" 
			label="Rate Type" check="req" selectTable="RateType" />
		<rcl:select id="t40basis" classes="div(col-sm-3 col-md-1) ctr(tblField)" 
			label="Basis" check="req" selectTable="FreightSurchargeBasis" />
		
		
		<!-- Readonly for freight charge -->
		<rcl:number id="t40units" classes="div(offset-sm-1 col-sm-3 col-md-1 pr-0) ctr(tblField)" 
			label="Units" check="req dec(14,4)" />
		<rcl:text id="t40uom" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="UOM" check="len(3) upc"
			lookup="tbl(UOM)" />
		<rcl:number id="t40rate" classes="div(col-sm-3 col-md-1 pr-0) ctr(tblField)"
			label="Rate" check="req dec(10,2)"/>
		<div class="col-sm-3 col-md-1 pr-0">
			<label>Subject to Change</label>
			<input id="t40subjectToChange" class="tblField" type="checkbox" disabled />
		</div>
		<rcl:number id="t40amount" classes="div(offset-sm-1 offset-md-0 col-sm-3 col-md-1 pr-0) ctr(tblField)" 
			label="Amount" check="req dec(12,2)" />
		<rcl:text id="t40currency" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="Currency" check="req len(3) upc" 
			lookup="tbl(COUNTRY_MASTER)" /> <!-- Shouldn't be CURRENCY MASTER ? -->
		<!-- Need POR, DEL options -->
		<rcl:select id="t40orgDstSea" classes="div(col-sm-3 col-md-2 pr-0) ctr(tblField)"
			label="Org/Dst/Sea" check="req" selectTable="OriginDestination" />
		<!-- Required if manual add -->
		<rcl:select id="t40portStatus" classes="div(col-sm-3 col-md-1 pr-0) ctr(tblField)"
			label="Port Status" check="req" selectTable="PortStatus" />
		<!-- Required if DG rate type -->
		<rcl:text id="t40portClass" classes="div(offset-sm-1 offset-md-0 col-sm-3 col-md-1 pr-0) ctr(tblField)"
			label="Port Class" check="len(5) upc" 
			lookup="tbl(PORT_MASTER)" />
		<!-- Flag value with display -->
		<rcl:text id="t40discountCharge" classes="div(col-sm-3 col-md-1) ctr(tblField)"
			label="Discount/Charge" options="disabled" />
		<!-- sm continue (7) -->
		
		
		<rcl:select id="t40ppdCol" classes="div(offset-sm-0 offset-md-1 col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="Ppd/Col" check="req" selectTable="PrepaidCollect" />
		<rcl:text id="t40collFSC" classes="div(col-sm-3 col-md-1 pr-0) ctr(tblField)"
			label="Coll. FSC" />
		<!-- Disable all codes if not % based surcharge -->
		<rcl:text id="t40code1" classes="div(offset-sm-1 offset-md-0 col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="% Charge Code 1" check="len(5) upc"
			lookup="tbl(SURCHARGE_TYPE_MASTER)" />
		<rcl:text id="t40code2" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="% Charge Code 2" check="len(5) upc"
			lookup="tbl(SURCHARGE_TYPE_MASTER)" />
		<rcl:text id="t40code3" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)" 
			label="% Charge Code 3" check="len(5) upc"
			lookup="tbl(SURCHARGE_TYPE_MASTER)" />
		<rcl:text id="t40code4" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="% Charge Code 4" check="len(5) upc"
			lookup="tbl(SURCHARGE_TYPE_MASTER)" />
		<rcl:text id="t40code5" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="% Charge Code 5" check="len(5) upc"
			lookup="tbl(SURCHARGE_TYPE_MASTER)" />
		<rcl:text id="t40billToParty" classes="div(offset-sm-1 offset-md-0 col-sm-3 col-md-2 pr-0) ctr(tblField)"
			label="Bill To Party" check="len(10) upc"
			lookup="tbl(CUSTOMER_MASTER)" />
		<rcl:number id="t40alternateRate" classes="div(col-sm-2 col-md-1 pr-0) ctr(tblField)"
			label="Alternate Rate" />
		<!-- Flag with display value -->
		<rcl:text id="t40autoManual" classes="div(col-sm-2 col-md-1) ctr(tblField)"
			label="Auto/Manual" check="req" options="disabled" />
		<!-- sm continue (8) -->
		
		
		<%
			if(useCostRecovery){
		%>
			<div class="offset-sm-0 offset-md-10 col-sm-2 col-md-1 pr-0">
				<label>Cost Recovery</label>
				<input id="t40costRecovery" class="tblField" type="checkbox" readonly />
			</div>
		<%
			}else{
		%>
			<div class="col-sm-2 col-md-11 pr-0"></div>
		<% 
			} 
		%>
		<!-- Must be disabled for auto-generated row -->
		<div class="col-sm-2 col-md-1 pb-0 pl-1" style="margin-top:10px">
			<a id="t40deleteBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2"
				style="font-size: 10px;" data-toggle="tooltip" data-placement="top"
				title="Removes current row"
				onclick='deleteCharge("#-RowId-#")'>Delete</a>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button type="button" class="rcl-standard-button"
				onclick="addCharge()">Add</button>
			<button type="button" class="rcl-standard-button"
				onclick="testCharge()">Test</button>
		</div>
	</div>
</div>

<script type="text/javascript">

	function decorateJsonDataChargeTab(data){
		let rowNo = 1;
		data.forEach(function(chargeData){
			chargeData.rowNo = rowNo;
			rowNo++;
		});
		console.log(data);
	}

	function addCharge(){
		rptInsertNewRowBefore('t40chargesListArea', 'end');
	}
	
	function testCharge(){
		console.log(getChangedData("t40chargesListArea", false, false));
	}
	
	function deleteCharge(rowId) {
		rptRemoveRow("t40chargesListArea", rowId);
	}
	
</script>