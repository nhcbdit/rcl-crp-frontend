<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<!-- Customers Header -->
<rcl:area id="d10customersHeader" classes="div(container-fluid)"
	title="Customers" collapsible='N'>
	<div class="row">
		<rcl:text id="d10bookingNo" classes="div(col-md-2) ctr(dtlField)"
			label="Booking#" options="readonly" check="upc" />
		<rcl:text id="d10bkType" classes="div(col-md-1) ctr(dtlField)"
			label="BK Type" options="readonly" check="upc" />
		<rcl:text id="d10status" classes="div(col-md-1) ctr(dtlField)"
			label="Status" options="readonly" />
		<rcl:text id="d10por" classes="div(col-md-1) ctr(dtlField)"
			label="POR" options="readonly" check="upc" />
		<rcl:text id="d10pol" classes="div(col-md-1) ctr(dtlField)"
			label="POL" options="readonly" check="upc" />
		<rcl:text id="d10pod" classes="div(col-md-1) ctr(dtlField)"
			label="POD" options="readonly" check="upc" />
		<rcl:text id="d10del" classes="div(col-md-1) ctr(dtlField)"
			label="DEL" options="readonly" check="upc" />
		<rcl:text id="d10shipmentType" classes="div(col-md-2) ctr(dtlField)"
			label="Shipment Type" options="readonly" check="upc" />
	</div>
</rcl:area>

<div id="t10customersListHeader"
	class="rcl-standard-widget-header tblHeader">Customer List</div>

<div id="t10customersListArea" class="container-fluid rcl-standard-search-header tblArea">
	<div id="t10row" class="row border tblRow">
		<rcl:select id="t10func"
			classes="div(col-sm-4 col-md-1 pb-0 pl-1 pr-0) ctr(tblField pk)"
			label="Function" name="Function" selectTable="CustomerFunction"
			defaultValue="O" check="req" />
		<rcl:text id="t10code"
			classes="div(col-sm-6 col-md-1 pb-0 pl-1 pr-0) ctr(tblField pk)"
			label="Code" name="Code" check="req len(10)"
			lookup="tbl(CUSTOMER) rco(CODE,ADDR1,ADDR2,ADDR3,ADDR4,CITY,STATE_CODE,COUNTRY_CODE,COUNTRY_NAME,ZIP,TEL,FAX,EMAIL)rci(t10code,t10address1,t10address2,t10address3,t10address4,t10city,t10state,t10country,t10countryName,t10zip,t10telNo,t10faxNo,t10email)" />
		<rcl:text id="t10name"
			classes="div(col-sm-3 col-md-2 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Name" name="Name" check="req len(45)" />
		<rcl:text id="t10address1"
			classes="div(col-sm-3 col-md-2 offset-md-0 offset-sm-4 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Address" name="Address1" check="upc len(35)" />
		<rcl:text id="t10address2"
			classes="div(col-sm-3 col-md-2 offset-md-0 offset-sm-4 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Address" name="Address2" check="upc len(35)" />
		<rcl:text id="t10address3"
			classes="div(col-sm-3 col-md-2 offset-md-0 offset-sm-4 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Address" name="Address3" check="upc len(35)" />
		<rcl:text id="t10address4"
			classes="div(col-sm-3 col-md-2 offset-md-0 offset-sm-4 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Address" name="Address4" check="upc len(35)" />
		<rcl:text id="t10city"
			classes="div(col-sm-3 col-md-1 offset-sm-4 offset-md-2 pb-0 pl-1 pr-0) ctr(tblField)"
			label="City" name="City" check="upc len(25)" />
		<rcl:text id="t10state"
			classes="div(col-sm-3 col-md-1 pb-0 pl-1 pr-0) ctr(tblField)"
			label="State" name="State" check="len(2)"
			lookup="tbl(STATE) rco(CODE) rci(t20State) sco(CODE,NAME)" />
		<rcl:text id="t10country"
			classes="div(col-sm-3 col-md-1 offset-md-0 offset-sm-4  pb-0 pl-1 pr-0) ctr(tblField)"
			label="Country Code" name="Country Code" check="req len(2)"
			lookup="tbl(COUNTRY) rco(CODE,DESC) rci(t20country,t20countryName) sco(CODE,DESC,RECORD_STATUS)" />
		<rcl:text id="t10countryName"
			classes="div(col-sm-3 col-md-1 offset-md-0 offset-sm-4  pb-0 pl-1 pr-0) ctr(tblField)"
			label="Country Name" check="upc" options="readonly" />
		<rcl:text id="t10zip"
			classes="div(col-sm-3 col-md-1 offset-md-0 offset-sm-4 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Zip" check="upc len(15)" />
		<rcl:text id="t10telNo"
			classes="div(col-sm-3 col-md-1 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Telephone#" check="upc len(17)" />
		<rcl:text id="t10faxNo"
			classes="div(col-sm-3 col-md-1  offset-md-0 offset-sm-4 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Fax#" check="upc len(17)" />
		<rcl:text id="t10email"
			classes="div(col-sm-3 col-md-2 pb-0 pl-1 pr-0) ctr(tblField)"
			label="Email" check="upc len(80)" />
		<div class="col-sm-3 col-md-1 offset-md-8 offset-sm-4  pb-0 pl-1 pr-0">
			<label>Exp. Detention</label> <input id="t10expDetention"
				name="Exp Detention" type="checkbox" class="tblField" required>
		</div>
		<div class="col-sm-3 col-md-2 offset-md-0 offset-sm-4  pb-0 pl-1 pr-0">
			<label>Imp. Det. Dem.</label> <input id="t10impDetDem"
				name="Imp Det Dem" type="checkbox" class="tblField" required>
		</div>
		<div class="col-sm-1 col-md-1 pb-0 pl-1 pr-0">
			<a id="t10deleteBtn" class="btn btn-sm btn-light text-dark ml-2 pl-2"
				style="font-size: 10px;" data-toggle="tooltip" data-placement="top"
				title="Removes current row"
				onclick='deleteCustomer("#-RowId-#")'>Delete</a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<button type="button" class="rcl-standard-button"
				onclick="addCustomer()">Add</button>
			<button type="button" class="rcl-standard-button"
				onclick="testCustomer()">Test</button>
			<button type="button" class="rcl-standard-button"
				onclick="refresh()">Refresh</button>
		</div>
	</div>
	<!-- end of table table-->
</div>
<!-- End of table customers list area -->

<script type="text/javascript">

	function addCustomer() {
		rptInsertNewRowBefore('t10customersListArea', 'end');
	}

	function deleteCustomer(rowId) {
		rptRemoveRow("t10customersListArea", rowId);
	}
	
	function testCustomer(){
		let change = rptGetInsertsAndChanges("t10customersListArea", false);
		console.log(change);
		//console.log(rptGetDataFromDisplay("t10customersListArea", "t10row-3"));
		console.log(getChangedData("t10customersListArea", false, preChangedCustomer));
	}
	
	function refresh(){
		let bookingNo = "BTAOC09001193";//sessionStorage.getItem("viewBookingNo");
		fetchBooking(bookingNo);
	}
</script>