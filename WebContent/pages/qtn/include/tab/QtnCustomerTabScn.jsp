<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<%-- Customers Header --%>
<rcl:area id="d20QuotationHeader" classes="div(container-fluid)" title="Quotation Header" collapsible='Y'>
	<div class="row">
		<rcl:text id="d20quotationNo" classes="div(col-md-2) ctr(dtlField)"
			label="Quotation#" options="disabled" check="upc" />
		<rcl:text id="d20ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" options="disabled" check="upc" />
		<%-- rcl:select id="d20status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/--%>

		<rcl:select id="d20status" label="Status"
					classes="div(col-md-2) ctr(dtlField)"
					options="data-rdo"
					optionsList="Open Need_Approval Quote_Again Close Final Hold Cancel"
					valueList="O N Q S C H L" />

		<rcl:text id="d20slm" classes="div(col-md-2) ctr(dtlField)"
			label="Slm#" options="disabled" check="upc" />
		<rcl:text id="d20qtdBy" classes="div(col-md-2) ctr(dtlField)"
			label="QtdBy" options="disabled" check="upc" />
		<rcl:text id="d20pos" classes="div(col-md-2) ctr(dtlField)"
			label="POS" options="disabled" check="upc" />
	</div>
</rcl:area>


<rcl:area id="t22RoutingHeader" classes="div(container-fluid)" title="Routing List" collapsible='Y' areaMode="table">
<div id="routhingScroll_routingTab">
	<div id="t22row" class="row tblRow t22row">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t22row');" class="row">
				<div class="col-md-12 container-fluid">
					<div class="row">
					<rcl:text id="t22porHaulLoc" classes="div(col-md-2)" label="POR Haul. Loc." options="disabled" check="upc" />
						<rcl:text id="t22por" classes="div(col-md-1)" label="POR"  options="disabled" check="upc" />
						<rcl:text id="t22pol" classes="div(col-md-1)" label="POL" options="disabled" />
						<rcl:text id="t22pot1" classes="div(col-md-1)" label="POT1" options="disabled"  check="upc" />
						<rcl:text id="t22pot2" classes="div(col-md-1)" label="POT2" options="disabled" check="upc" />
						<rcl:text id="t22pot3" classes="div(col-md-1)" label="POT3" options="disabled" check="upc" />
						<rcl:text id="t22pod" classes="div(col-md-1)" label="POD" options="disabled" />
						<rcl:text id="t22del" classes="div(col-md-1)" label="DEL" options="disabled" check="upc" />
						<rcl:text id="t22delHaulLoc" classes="div(col-md-2)" label="DEL Haul. Loc." options="disabled" check="upc" />
						<rcl:select id="t22socCoc"
										label="SOC/COC"
										classes="div(col-md-1) ctr(dtlField)"
										selectTable="SocCoc"
										options="data-dis"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</rcl:area>

<rcl:area id="t21customerArea" classes="div(container-fluid)" title="Quotation Customers" collapsible='Y' areaMode="table">
	<div id="t21row" class="row tblRow border">
		<rcl:select id="t21function"
					label="Function"
					classes="div(col-md-1  offset-0x25 pl-1 pr-0) ctr(tblField)"
					selectTable="CustomerFunction"
					check="req" />


		<%-- rcl:text id="t21code" classes="div(col-sm-2 pl-1 pr-0) ctr(tblField)"
			label="Code" check="req len(10)"
			lookup="tbl(vrl_customers)
							rco(CODE NAME EMAIL TEL FAX CITY ADDRESS_LINE1 ADDRESS_LINE2 ADDRESS_LINE3 ADDRESS_LINE4 COUNTRY CITY STATE ZIP)
							rid(t21code#-RowId-# t21name#-RowId-# t21email#-RowId-# t21telephone#-RowId-# t21fax#-RowId-# t21city#-RowId-# t21address1#-RowId-# t21address2#-RowId-# t21address3#-RowId-# t21address4#-RowId-# t21countryCode#-RowId-# t21countryName#-RowId-# t21state#-RowId-# t21zip#-RowId-#)
							sco(CODE*min[3],main)
							sva(t21code#-RowId-#)
							sop(LIKE)"
							autoLookup="
									sco(CODE)
									sva(t21code#-RowId-#)"/--%>
		<div class=" col-sm-2 pl-1 pr-0">
			<label for="t21code">Code</label>
			<div class="rcl-standard-input-wrapper">
				<div style="padding-right:20px">
					<input id="t21code" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField tblField" onblur="rutLookupByKey('vrl_customers','CODE NAME EMAIL TEL FAX CITY ADDRESS_LINE1 ADDRESS_LINE2 ADDRESS_LINE3 ADDRESS_LINE4 COUNTRY CITY STATE ZIP','t21code#-RowId-# t21name#-RowId-# t21email#-RowId-# t21telephone#-RowId-# t21fax#-RowId-# t21city#-RowId-# t21address1#-RowId-# t21address2#-RowId-# t21address3#-RowId-# t21address4#-RowId-# t21countryCode#-RowId-# t21countryName#-RowId-# t21state#-RowId-# t21zip#-RowId-#','CODE*min[3],main CODE','t21code#-RowId-# t21code#-RowId-#','LIKE','t21code#-RowId-#');" required="true" maxlength="10" data-ct="bt-text">
				</div>
					<i class="fa fa-search" onclick="qtn_OpenLookupTable('vrl_customers','CODE NAME EMAIL TEL FAX CITY ADDRESS_LINE1 ADDRESS_LINE2 ADDRESS_LINE3 ADDRESS_LINE4 COUNTRY CITY STATE ZIP','t21code#-RowId-# t21name#-RowId-# t21email#-RowId-# t21telephone#-RowId-# t21fax#-RowId-# t21city#-RowId-# t21address1#-RowId-# t21address2#-RowId-# t21address3#-RowId-# t21address4#-RowId-# t21countryCode#-RowId-# t21countryName#-RowId-# t21state#-RowId-# t21zip#-RowId-#','CODE*min[3],main','t21code#-RowId-#','LIKE','','OR','NAME');"></i>
				</div>
		</div>

		<rcl:text id="t21name" classes="div(col-sm-8 pl-1 pr-0) ctr(tblField)"
			label="Name" check="req len(45)" options="onkeyup='upc();'"/>


		<div class="col-0x5 pl-1 pr-2 mr-2"></div>

		<div class="col-0x25 col-md-1 pb-0  pl-1 pr-0 mt-3"
			style="text-align: center">
			<rcl:text id="t21customerSeqNo" classes="ctr(dtlField)"
			label="" options="disabled"/>
		</div>

		<rcl:text id="t21address1" classes="div(col-md-4 col-lg-3 pl-1 pr-0) ctr(tblField)"
			label="Address1" check="len(35)" />
		<rcl:text id="t21address2" classes="div(col-md-4 col-lg-3 pl-1 pr-0) ctr(tblField)"
			label="Address2" check="len(35)" />
		<rcl:text id="t21address3" classes="div(col-md-4 col-lg-3 pl-1 pr-0) ctr(tblField)"
			label="Address3" check="len(35)" />
		<rcl:text id="t21address4" classes="div(col-md-3 col-lg-2 pl-1 pr-0) ctr(tblField)"
			label="Address4" check="len(35)" />


			<div id="t21selectDeleteLabel"  class="col-0x50 pl-1 pr-2">
					<label for="t21selectDelete">
						Delete
						<div><input id="t21selectDelete" type="checkbox" class="tblField" /></div>
					</label>
			</div>


		<rcl:text id="t21city" classes="div(col-sm-2 offset-0x25 pl-1 pr-0) ctr(tblField)"
			label="City" check="len(25)"  options="onkeyup='upc();'"/>

		<rcl:text id="t21state" classes="div(col-sm-1 pl-1 pr-0) ctr(tblField)"
			label="State" check="len(2)"
			lookup="tbl(VRL_STATE)
							rco(CODE)
							rid(t21state#-RowId-#)
							sco(CODE*main)
							sva(t21state#-RowId-#)
							sop(LIKE)"/>

		<rcl:text id="t21countryCode" classes="div(col-sm-1 pl-1 pr-0) ctr(tblField)"
			label="Country Code" check="req len(2)" options="onkeyup='upc();'"
			lookup="tbl(VRL_COUNTRY)
							rco(CODE NAME)
							rid(t21countryCode#-RowId-# t21countryName#-RowId-#)
							sco(CODE*main)
							sva(t21countryCode#-RowId-#)
							sop(LIKE)"/>

		<rcl:text id="t21countryName" classes="div(col-sm-2 pl-1 pr-0) ctr(tblField)"
			label="Country Name" />

		<rcl:number id="t21zip" classes="div(col-sm-1 pl-1 pr-0) ctr(tblField)"
			label="Zip" check="len(15)" />

		<rcl:number id="t21telephone" classes="div(col-sm-1 pl-1 pr-0) ctr(tblField)"
			label="Telephone#" check="len(17)" />

		<rcl:number id="t21fax" classes="div(col-sm-1 pl-1 pr-0) ctr(tblField)"
			label="Fax#" check="len(17)" />

		<%-- Req if dg size-type --%>
		<rcl:email id="t21email" classes="div(col-sm-2 pl-1 pr-0) ctr(tblField)"
			label="E-Mail" check="upc len(80)" options="onchange='validateEmail(this.id,this.value)'"/>



	</div>
	<%-- End Row Template --%>

	<hr style="margin-top: 0px">
	<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: -5; margin-bottom: -5px;">
			<button id="_t21addBtn" type="button" class="rcl-standard-button" onclick="newCustomer()">Add</button>
			<button id="_t21deleteBtn" type="button" class="rcl-standard-button" onclick="deleteCustomer()">Delete</button>
		</div>
	</div>
</rcl:area>

<script type="text/javascript">
var OrgDataCustomer = [];
$(document).ready(function() {
	rptTableInit("t21customerArea", { disabledSortControl: true });
	rptTableInit("t22RoutingHeader", { disabledSortControl: true });


});

function ischeckfscCustomer(){
	var HedCustRow = rptGetDataFromDisplayAll("t21customerArea");

		for (var x = 0; x < HedCustRow.length; x++) {
			removeGeneralSearchButton("t21code-"+x);
			$("#t21selectDeleteLabel-"+x).remove();
			$.each(HedCustRow[x], function(k, v) {
				setCustomTagProperty("t21"+k+"-"+x, {dis:true});
			});

		}
		$("#_t21deleteBtn").remove();
		$("#_t21addBtn").remove();
}

function setCustomerRowViewForNewRow(rowId, idNum, index, data){
	setRowView("t21customerArea","t21customerSeqNo");
	setCustomerRowView(rowId, idNum, index, data);


	/*setCustomerRowView(rowId, idNum, index, data);

	$("#t20expDetention-" + idNum).change(function(e){
		setExpDetention(idNum, e.target);
	});
	$("#t20impDetDem-" + idNum).change(function(e){
		setImpExpDetention(idNum, e.target);
	});*/
}

function fetchDataCustomerTab(content){
	rptAddDataWithCache("t21customerArea", SetAction_edit(content));
	rptDisplayTable("t21customerArea");
	setRowView("t21customerArea","t21customerSeqNo");
	OrgDataCustomer = JSON.stringify(rptGetDataFromDisplayAll("t21customerArea"));


}

function setCustomerRowViewFordelete(rowId, idNum, index, data){

}

function setCustomerRowView(rowId, idNum, index, data){
	rutSetElementValue("t21function-"+idNum,"S");
	/*var corridorArr = rptGetDataFromDisplayAll("t21customerArea");

	for(var i=0; i<corridorArr.length; i++){
				var corridorObj = corridorArr[i];
				$("#t21customerSeqNo-"+idNum).val(i + 1);
			}*/
}

function newCustomer() {

	rptInsertNewRowBefore("t21customerArea", 'end');
	var newRow = $("#t21customerArea").find(".tblRow").not(":last").last();
	var data = rptGetDataFromDisplay("t21customerArea", newRow.attr("id"));
	data.action = 'i';
	setRowView("t21customerArea","t21customerSeqNo");
	rptForeachRow('t21customerArea', setCustomerRowViewForNewRow, false, ":last");
	//setfieldCustomer();
	checkRecodeScroll('t21customerArea','t21customerArea','shipTypeDetailArea-scroll',2);

	}

var NewDataCustomer = [];
function deleteCustomer(){
	//NewDataCustomer.push(HanderGetDataDeleteAll('t21customerArea', "selectDelete"));
	setarrayDeletAll('t21customerArea',NewDataCustomer);
	rptTryToDeleteSelectedRows('t21customerArea', "selectDelete", true);
	rptForeachRow('t21customerArea', setCustomerRowViewFordelete, false, ":last");
	setRowView("t21customerArea","t21customerSeqNo");
	//HanderGetDataDeleteAll
}

function getCustomerTabOutput(){
	var ArrayquotationCustomer = [];
	ArrayquotationCustomer = rptGetDataFromDisplayAll("t21customerArea");

	if(!isEmpty(NewDataCustomer)){
		for(var i = 0;i < NewDataCustomer.length; i++){
			if(!isEmpty(NewDataCustomer[i])){
				ArrayquotationCustomer.push(NewDataCustomer[i]);
			}

		}
	}

	var quotationCustomer = CheckDataOnChange_ObjectListNew(OrgDataCustomer,ArrayquotationCustomer,"customerSeqNo");


	return quotationCustomer;
}

function setfieldCustomer(){
	rutSetElementValue("t21address1-0", "5/68, ***RD ***OR, *** ***ZA, ***AM");
	rutSetElementValue("t21address2-0", "***D,***OL ***H, *** ***H -,");
	rutSetElementValue("t21address3-0", "-110005.*** ***E:0509059791");
	rutSetElementValue("t21code-0", "030JAM0001");
	rutSetElementValue("t21countryCode-0", "IN");
	rutSetElementValue("t21countryName-0", "INDIA");
	rutSetElementValue("t21fax-0", "2");
	rutSetElementValue("t21function-0", "S");
	rutSetElementValue("t21name-0", "***CH *********S *** *** .");
	rutSetElementValue("t21telephone-0", "1");
}



</script>
