<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>
<%-- Customers Header --%>
<rcl:area id="d40QuotationHeader" classes="div(container-fluid)" title="Quotation Header" collapsible='Y'>
	<div class="row">
		<rcl:text id="d40quotationNo" classes="div(col-md-2) ctr(dtlField)"
			label="Quotation#" options="disabled" check="upc" />
		<rcl:text id="d40ref" classes="div(col-md-2) ctr(dtlField)"
			label="Ref#" options="disabled" check="upc" />
		<%-- rcl:select id="d40status"
				label="Status"
				classes="div(col-md-2) ctr(dtlField)"
				options="data-rdo"
				selectTable="QuotationStatus"/--%>
		<rcl:select id="d40status" label="Status"
					classes="div(col-md-2) ctr(dtlField)"
					options="data-rdo"
					optionsList="Open Need_Approval Quote_Again Close Final Hold Cancel"
					valueList="O N Q S C H L" />
		<rcl:text id="d40slm" classes="div(col-md-2) ctr(dtlField)"
			label="Slm#" options="disabled" check="upc" />
		<rcl:text id="d40qtdBy" classes="div(col-md-2) ctr(dtlField)"
			label="QtdBy" options="disabled" check="upc" />
		<rcl:text id="d40pos" classes="div(col-md-2) ctr(dtlField)"
			label="POS" options="disabled" check="upc" />
	</div>
</rcl:area>

<rcl:area id="t41RoutingHeader" classes="div(container-fluid)" title="Routing List" collapsible='Y' areaMode="table">
<div id="routhingScroll_routingTab">
	<div id="t41row" class="row tblRow t41row">
		<div class="container-fluid">
			<div onclick="isOnChangeCorridor('#-RowId-#','t41row');" class="row">
				<div class="col-md-12 container-fluid">
					<div class="row">

						<rcl:text id="t41porHaulLoc" classes="div(col-1x5) ctr(dtlField)"	label="POR Haul. Loc." options="disabled" check="upc" />
						<rcl:text id="t41por" classes="div(col-md-1) ctr(dtlField)" label="POR" options="disabled"  check="upc" />
						<rcl:text id="t41pol" classes="div(col-md-1) ctr(dtlField)" label="POL"  options="disabled" />
						<rcl:text id="t41pot1" classes="div(col-md-1) ctr(dtlField)" label="POT1" options="disabled" check="upc" />
						<rcl:text id="t41pot2" classes="div(col-md-1) ctr(dtlField)" label="POT2" options="disabled" check="upc" />
						<rcl:text id="t41pot3" classes="div(col-md-1) ctr(dtlField)" label="POT3" options="disabled"  check="upc" />
						<rcl:text id="t41pod" classes="div(col-md-1) ctr(dtlField)" label="POD" options="disabled" />
						<rcl:text id="t41del" classes="div(col-md-1) ctr(dtlField)" label="DEL" options="disabled" check="upc" />
						<rcl:text id="t41delHaulLoc" classes="div(col-1x5) ctr(dtlField)" label="DEL Haul. Loc." options="disabled"  check="upc" />
						<rcl:select id="t41socCoc"
										label="SOC/COC"
										classes="div(col-0x5) ctr(dtlField)"
										selectTable="SocCoc"
										options="data-dis"/>
						<rcl:select id="t41transShipment" label="Direct/Transshipment" classes="div(col-1x5 ) ctr(dtlField)" selectTable="DirectTransFlag" check="req" defaultValue="D"/>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
		<div class="col-md-12"
			style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
			<!-- button id="_t41Getproduct" type="button" class="rcl-standard-button"
				onclick="openProductCatalog('#-RowId-#')">Product Catalogue old</button-->
			<button id="_t41Getproduct" type="button" class="rcl-standard-button"
				onclick="openProductCatalog_New('#-RowId-#')">Product Catalogue</button>
			<!-- openProductCatalog_New -->
			<!-- openProductCatalog -->
		</div>
	</div>
</rcl:area>


<rcl:area id="t40rountingDetailArea" classes="div(container-fluid)" title="ShipmeCommodity Details" collapsible='Y' areaMode="table">
	<div class="pl-3">
					<div id="IsOnoff" class="row border-bottom" style="display:none;" >
						<div class="col-0x25 " style="font-weight: bold;">
							<label class="rcl-std">Seq#</label>
						</div>

						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">From Loc Tp.</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">From Loc Code.</label>
						</div>

						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">To Loc Tp.</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">To Loc Code.</label>
						</div>

						<div class="col-md-1" style="font-weight: bold;">
							<label class="rcl-std">	ETD</label>
						</div>
						<div class="col-md-1" style="font-weight: bold;">
							<label class="rcl-std"> ETA</label>
						</div>
						<div class="col-md-1 offset-1" style="font-weight: bold;">
							<label class="rcl-std">Service</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">Vessel</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">Voyage</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">Direction</label>
						</div>
						<div class="col-0x75" style="font-weight: bold;">
							<label class="rcl-std">Restriction</label>
						</div>
				  </div>

				  <div id="NotIsOnoff" class="row border-bottom">
						<div class="col-0x25 " style="font-weight: bold;">
							<label class="rcl-std">Seq#</label>
						</div>

						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">From Loc Tp.</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">From Loc Code.</label>
						</div>

						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">To Loc Tp.</label>
						</div>
						<div class="col-md-1" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">To Loc Code.</label>
						</div>

						<div class="col-md-1" style="font-weight: bold;">
							<label class="rcl-std"></label>
						</div>
						<div class="col-md-1" style="font-weight: bold;">
							<label class="rcl-std"></label>
						</div>
						<div class="col-md-1 offset-1" style="font-weight: bold;">
							<label class="rcl-std">Service</label>
						</div>
						<div class="col-md-3" style="text-align: center; font-weight: bold;">
							<label class="rcl-std">Direction</label>
						</div>
						<div class="col-0x75" style="font-weight: bold;">
							<label class="rcl-std">Restriction</label>
						</div>
				  </div>



				  <div class="row border-bottom"
						style="margin-top: 10px; margin-bottom: 5px;">
						<div class="offset-0x25"></div>


						<div class="col-md-1 offset-4"
							style="font-weight: bold; margin-top: -5px;">
							<label class="rcl-std">Mode of Trans.</label>
						</div>
						<div class="col-md-2"
							style="text-align: center; font-weight: bold; margin-top: -5px;">
							<label class="rcl-std">Transport Vendor/Drayage</label>
						</div>

						<div class="col-md-1"
							style="font-weight: bold; margin-top: -5px;">
							<label class="rcl-std">Operator Code</label>
						</div>
						<div class="col-md-1"
							style="font-weight: bold; margin-top: -5px;">
							<label class="rcl-std">Waiting Time</label>
						</div>

						<div class="col-md-1"
							style="font-weight: bold; margin-top: -5px;">
							<label class="rcl-std">Trans. Time</label>
						</div>
						<div class="col-md-1"
							style="font-weight: bold; margin-top: -5px;">
							<label class="rcl-std">Total Time</label>
						</div>
					</div>

		<div id="t40row" class="row tblRow pt-1">
			<div class="container-fluid">
				<div class="row">
					<input id="t40routingRefNo" type="text" style="display:none;" readonly />
					<input id="t40corridorSeq" class="tblField" type="text" style="display:none;" readonly /> <%-- adjust  corridorSeqNo--%>
					<input id="t40routingRef" type="text" style="display:none;" readonly />
					<input id="t40carrierName" type="text" style="display:none;" readonly />
					<input id="t40drayage" type="text" style="display:none;" readonly />
					<rcl:text id="t40voyageSeqNo" name="Seq#" classes="div(col-0x25 pl-1 pr-0) ctr(tblField)"/>
					<rcl:select id="t40fromLocTp" name="From Loc. Tp" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" selectTable="InlandLocationTypeWithTruck" />
					<rcl:text id="t40fromLoc" name="From Loc." classes="div(col-md-1 pl-1 pr-0) ctr(tblField)"  check="len(5)" />
					<rcl:select id="t40toLocTp" name="To Loc. Tp" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)"  selectTable="InlandLocationTypeWithTruck" />
					<rcl:text id="t40toLoc" name="From Terminal" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="len(5)" />

					<rcl:date id="t40etd" classes="div(col-md-1 pl-1 pr-0 isoneoff) ctr(tblField)" options="disabled"/>
					<rcl:date id="t40eta" classes="div(col-md-2 pl-1 pr-0 isoneoff) ctr(tblField)" options="disabled"/>

					<div id="service" class="col-md-1 pl-1 pr-0">
						<rcl:text id="t40service" classes="ctr(tblField)" check="len(5)" />
					</div>

					<rcl:text id="t40vessel" classes="div(col-md-1 pl-1 pr-0 isoneoff) ctr(tblField)" options="disabled"/>
					<rcl:text id="t40voyage" classes="div(col-md-1 pl-1 pr-0 isoneoff) ctr(tblField)" options="disabled"/>
					<div id="direction" class="pl-1 pr-0">
						<rcl:select id="t40direction" classes="ctr(tblField)"  selectTable="Direction" />

					</div>
					<div class="col-0x5 pl-2 pr-0" style="text-align: center;">
						<i id="_t40restrictionIcon" class="fa fa-plus " aria-hidden="true"
						onclick=""></i>
					</div>
				</div>

				<div class="row">
					<div class="offset-0x25"></div>

					<rcl:text id="t40fromLocType" name="From Loc. Termina" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" options="disabled"/>
					<rcl:text id="t40fromTerminal" name="From Terminal" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" />

					<rcl:text id="t40toLocType" name="To Loc. Terminal" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" options="disabled"/>
					<rcl:text id="t40toTerminal" name="From Terminal" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)"  />

					<rcl:select id="t40transportationMode" name="Transportation Mode" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" selectTable="ModeOfTransport" />
					<rcl:text id="t40transportCarrierDrayage" name="transportCarrierDrayage" classes="div(col-md-2 pl-1 pr-0) ctr(tblField)"  check="len(10)" />

					<rcl:text id="t40operatorCode" name="operatorCode" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)"  check="len(4)" />
					<rcl:number id="t40waitingTime" name="waitingTime" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="dec(4,2)" defaultValue="0.00" />
					<rcl:number id="t40transitTime" name="transitTime" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="dec(4,2)" defaultValue="0.00" />
					<rcl:number id="t40totalTime" name="totalTimel" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="dec(4,2)" defaultValue="0.00" />
				</div>
			</div>
		</div>

		<div class="row border-bottom pt-1">
			<div class="container-fluid">
				<div class="row">
						<div class="offset-md-7"></div>
						<div class="offset-0x25"></div>
						<div class="col-md-1" style="font-weight: bold; text-align: right;  padding-right: 0px;">
							<label class="rcl-std">Total</label>
						</div>
						<rcl:number id="t40sumwaitingTime" name="sumwaitingTime" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="dec(4,2) dis" defaultValue="0.00" />
						<rcl:number id="t40sumtransitTime" name="sumtransitTime" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="dec(4,2) dis" defaultValue="0.00" />
						<rcl:number id="t40sumtotalTime" name="sumtotalTime" classes="div(col-md-1 pl-1 pr-0) ctr(tblField)" check="dec(4,2) dis" defaultValue="0.00" />
				</div>
			</div>
		</div>



	</div>
</rcl:area>
<%-- End of table customers list area --%>

<%-- Popup Routing Tab --%>

<jsp:include page="../../include/QtnProductCatalog.jsp">
	<jsp:param value="b1-" name="dialogId"/>
	<jsp:param value="s0-" name="searchId"/>
	<jsp:param value="t42" name="areaId"/>
</jsp:include>

<script type="text/javascript">
var OrgDataShipmeCommodity = [];
var gettransShipment = "";
$(document).ready(function() {
	rptTableInit("t41RoutingHeader", { disabledSortControl: true });
	rptTableInit("t40rountingDetailArea", { disabledSortControl: true });


});

function setRouthingRowViewForNewRow(rowId, idNum, index, data){
	//setRowView("t21customerArea","t21custSeq");
}

function fetchDataRoutingTab(content){

	rptAddDataWithCache("t40rountingDetailArea", SetAction_edit(content));
	rptDisplayTable("t40rountingDetailArea");
	Isdisplayrounting();
	setRowView("t40rountingDetailArea","t40voyageSeqNo");
	SumtotalTime("t40rountingDetailArea","t40totalTime");
	OrgDataShipmeCommodity = JSON.stringify(rptGetDataFromDisplayAll("t40rountingDetailArea"));
	IsviewRountingDetail();
	IsCheckOneOff();
	sumTotal(rptGetDataFromDisplayAll("t40rountingDetailArea"));

}



function Isdisplayrounting(){
	var getdata = rptGetDataFromDisplayAll("t40rountingDetailArea");
	for(var i=0; i<getdata.length; i++){
		var getrow = getdata[i].rowId.split("-");
		var getfromLocTp = $("option:selected", $("#t40fromLocTp-"+getrow[1])).text();
		var gettoLocTp = $("option:selected", $("#t40toLocTp-"+getrow[1])).text();
		rutSetElementValue("t40fromLocType"+"-"+getrow[1],"From "+getfromLocTp);
		rutSetElementValue("t40toLocType"+"-"+getrow[1],"To "+gettoLocTp);
	}

	var firstRow = true;
	rptForeachRow("t40rountingDetailArea", function(rowId, idNum, index){
		if(!firstRow){
			$("#_t40restrictionIcon-" + idNum).remove();
		}
		firstRow = false;
	});
}
/*
function setRowView(area,field){
	var corridorArr = rptGetDataFromDisplayAll(area);
	var seq = 1;
	for(var i=0; i<corridorArr.length; i++){
		var getrow = corridorArr[i].rowId.split("-");
		rutSetElementValue(field+"-"+getrow[1], seq);
		seq ++;
	}
}*/
 
function IsCheckOneOff(){
	var getrunt = rptGetDataFromDisplayAll("t40rountingDetailArea")
	if(getqtnNo != null){ // case edit
		if(IsOneOff){
			$('#IsOnoff').show();
			$('#NotIsOnoff').hide();
				for (var x = 0; x < getrunt.length; x++) {
					$('.isoneoff').show();
					$('#service-'+x).removeClass('offset-3');
					$('#direction-'+x).removeClass( "col-md-3" ).addClass( "col-md-1" );
				}
		}else{
			$('#IsOnoff').hide();
			$('#NotIsOnoff').show();
			for (var x = 0; x < getrunt.length; x++) {
				$('.isoneoff').hide();
				$('#service-'+x).addClass('offset-3');
				$('#direction-'+x).addClass( "col-md-3" ).removeClass( "col-md-1" );
			}
		}

	}


}
function SumtotalTime(area,field){
	var corridorArr = rptGetDataFromDisplayAll(area);
	var totalTime = 0;
	for(var i=0; i<corridorArr.length; i++){
		totalTime = corridorArr[i].waitingTime + corridorArr[i].transitTime;
		var getrow = corridorArr[i].rowId.split("-");
		rutSetElementValue(field+"-"+getrow[1], totalTime);
	}
}

var limitRow = 50;
var sessionId = 1;
var modeOfTransportAtPor = "";
var modeOfTransportAtDel = "";
function openProductCatalog_New(){
	var getRoutingHeader = getRouting();
	var req = {};
	for(var i = 0; i < getRouting().length; i++){
		if(i == isSelect){
			var por = "";
			var pol = getRoutingHeader[i].pol;
			var pod =  getRoutingHeader[i].pod;
			var del = "";

			if(!pol){dfsf
				dialogGeneric("Warning", "POL is required for Product Catalog", "Ok");
				return;
			}
			if(!pod){
				dialogGeneric("Warning", "POD is required for Product Catalog", "Ok");
				return;
			}
			//console.log("getRoutingHeader[i].porHaul :: "+getRoutingHeader[i].porHaul);
			if(getRoutingHeader[i].porHaul === "C"){
				por = getRoutingHeader[i].por;
			}
			//console.log("getRoutingHeader[i].delHaul :: "+getRoutingHeader[i].delHaul);
			if(getRoutingHeader[i].delHaul === "C"){
				del = getRoutingHeader[i].del;
			}
			
			
			var porHaul = getRoutingHeader[i].porHaul;
			var delHaul = getRoutingHeader[i].delHaul;
			var pot1 = getRoutingHeader[i].pot1;
			var pot2 = getRoutingHeader[i].pot2;
			var pot3 = getRoutingHeader[i].pot3;
			var directTransFlag = $('#t41transShipment-'+isSelect).val();
			var saildate = convertDateCheckDiff(getRoutingHeader[i].shipDate);
			var customerId = rutGetElementValue("d10contactPartyCode");
			var term = getRoutingHeader[i].term;
			var byTerminal = "Y";
			
			/*var modeOfTransportAtPor = (getRoutingHeader[i].porHaul == 'C') ? "T" : "";
			var modeOfTransportAtDel = (getRoutingHeader[i].delHaul == 'C') ? "T" : "";*/
			
			//GET_DEFAULT_MOT(por,del); //#get default modeOfTransportAt Del and Por
			
			getResultAjax("WS_GET_DEFAULT_MOT", 
					{
						userData : setUserData(),
						por : por,
						del : del
					}).done(handleSetMot);
					
			function handleSetMot(response){
				
				if(response.resultStatus == 'S'){
						modeOfTransportAtPor = response.resultContent.porMot;
						modeOfTransportAtDel = response.resultContent.delMot;
						
						console.log("modeOfTransportAtPor  >> "+modeOfTransportAtPor);
						console.log("modeOfTransportAtDel  >> "+modeOfTransportAtDel);
						setParamSearch(getRoutingHeader[isSelect].por, getRoutingHeader[isSelect].pol, getRoutingHeader[isSelect].pod, getRoutingHeader[isSelect].del, getRoutingHeader[isSelect].porHaul, getRoutingHeader[isSelect].delHaul, getRoutingHeader[isSelect].pot1, getRoutingHeader[isSelect].pot2, getRoutingHeader[isSelect].pot3,
								term, directTransFlag, true, saildate, customerId, byTerminal, modeOfTransportAtPor, modeOfTransportAtDel);	
				
								req = setFndoSearch(por, pol, pod, del, porHaul, delHaul, pot1, pot2, pot3,
										term, directTransFlag, saildate, customerId, byTerminal, term, directTransFlag, modeOfTransportAtPor, modeOfTransportAtDel);
								
								SetfildSearch(IsParamSearch);
								
								getResultAjax("WS_NEW_PCL", req).done(
										function(response){
											if(response.statusCode == '0'){
												decorateProductCatalogData(response.item.routingList);
												displayProductCatalog_New(response.item.routingList);
												rutOpenDialog('b1-ProductCatalogue');
												$("#s2-pol").attr("required", "true");
												$("#s2-pod").attr("required", "true");
												gettransShipment = $('#s2-directTransshipment').val();
											}
											//console.log("New PCl response :: "+JSON.stringify(response));
										}
								);
						
					}else{
						dialogGeneric("Warning", response.resultMessage, "Ok");
					}
					
			}
			
			
			
		}
	}
	
	
}


function setFndoSearch(por, pol, pod, del, porHaul, delHaul, pot1, pot2, pot3,
		term, directTransFlag, saildate, customerId, byTerminal, term, directTransFlag, modeOfTransportAtPor, modeOfTransportAtDel){
	
	console.log(saildate)
	var data = {
		"userId" : userData.userId,
		"sessionId" : sessionId,
		"limitRow" : limitRow,
		"pol" : pol,
		'fromTerminal' : null,
		"pod" : pod,
		'toTerminal' : null,
		"shipmentEtd" : parseInt(saildate),//0,
		//"flagDirect": (directTransFlag == 'D' || directTransFlag == 'B' ? 'Y':'N'),
		"flagDirect": 'Y',
		"flag1ts": (directTransFlag == '1' || directTransFlag == 'B' || directTransFlag == 'T' ? 'Y':'N'),
		"flag2ts": (directTransFlag == '2' || directTransFlag == 'B' || directTransFlag == 'T' ? 'Y':'N'),
		"flag3ts": (directTransFlag == 'T' || directTransFlag == 'B' ? 'Y':'N'),
		"por" : por,
    	"modeOfTransportAtPor" : null,
    	"del" : del,
    	"modeOfTransportAtDel" : null,
    	"pot1" : pot1,
    	"pot2" : pot2,
    	"pot3" : pot3,
    	"service1" : null,
    	"service2" : null,
    	"service3" : null,
    	"service4" : null,
		"flagByTerminal" : byTerminal,
		"flagArrivalSail" : "Y",
		"shipmentTerm" : term,
		"modeOfTransportAtPor": modeOfTransportAtPor,
		"modeOfTransportAtDel": modeOfTransportAtDel
	}
	
	return data;
	
}

function openProductCatalog(){
	$("#s2-pol").attr("required", "true");
	$("#s2-pod").attr("required", "true");
	var getRoutingHeader = getRouting();
	for(var i = 0; i < getRouting().length; i++){
		if(i == isSelect){

			//var Rpor = getRoutingHeader[i].por;
			var por = "";
			var pol = getRoutingHeader[i].pol;
			//var pol = "SGSIN";
			var pod =  getRoutingHeader[i].pod;
			//var pod = "MYPKG";
			var del =  getRoutingHeader[i].del;
			//var del = "";

			if(!pol){dfsf
				dialogGeneric("Warning", "POL is required for Product Catalog", "Ok");
				return;
			}
			if(!pod){
				dialogGeneric("Warning", "POD is required for Product Catalog", "Ok");
				return;
			}
			console.log("getRoutingHeader[i].porHaul :: "+getRoutingHeader[i].porHaul);
			if(getRoutingHeader[i].porHaul === "C"){
				por = getRoutingHeader[i].por;
			}
			console.log("getRoutingHeader[i].delHaul :: "+getRoutingHeader[i].delHaul);
			if(getRoutingHeader[i].delHaul === "C"){
				del =getRoutingHeader[i].del;
			}
			var porHaul = getRoutingHeader[i].porHaul;
			var delHaul = getRoutingHeader[i].delHaul;
			var pot1 = getRoutingHeader[i].pot1;
			var pot2 = getRoutingHeader[i].pot2;
			var pot3 = getRoutingHeader[i].pot3;
			//--var term = getRoutingHeader[i].term;
			var directTransFlag = $('#t41transShipment-'+isSelect).val();
			var saildate = convertformatDatereg(getRoutingHeader[i].shipDate);
			//Rpor = "";
			//del = "";
			
			var customerId = rutGetElementValue("d10contactPartyCode");
			var term = getRoutingHeader[i].term;
			var byTerminal = "Y";

			/*searchProductCatalog(Rpor, pol, pod, del, porHaul, delHaul, pot1, pot2, pot3,
					term, directTransFlag, saildate,"",byTerminal);*/
			setParamSearch(getRoutingHeader[i].por, getRoutingHeader[i].pol, getRoutingHeader[i].pod, getRoutingHeader[i].del, getRoutingHeader[i].porHaul, getRoutingHeader[i].delHaul, getRoutingHeader[i].pot1, getRoutingHeader[i].pot2, getRoutingHeader[i].pot3,
					term, directTransFlag, true, saildate, customerId, byTerminal);	
					
			searchProductCatalogAnyWS("WS_BKG_PC", por, pol, pod, del, porHaul, delHaul, pot1, pot2, pot3,
					term, directTransFlag, true, saildate, customerId, byTerminal);
			
			SetfildSearch(IsParamSearch);
			
			

		}
	}
}

function getRouting() {
	return rptGetDataFromDisplayAll("t41RoutingHeader");
}

function addSelectedRouteFromCatalog(){
	var selectProductCat = getSelectedRouteFromCatalog();
	//Ask question for replacement
	if(isRouteChanged(selectProductCat)){
		dialogGeneric("Warning", "Corridor will be overwritten by selected route. Confirm ?"
				+ " (info.overwritecorridordtlfromrouting.prompt)", "Yes", "No").then(
			function(overwrite){
				if(overwrite){
					_addPC(selectProductCat);
				}else{
					rutCloseDialog('b1-ProductCatalogue');
					$("#s2-pol").attr("required", "false");
					$("#s2-pod").attr("required", "false");
				}
			});
	}else{

		_addPC(selectProductCat);
	}

	function _addPC(pcRoute){
		//console.log("selectProductCat "+JSON.stringify(pcRoute));
		rptClearDisplay("t40rountingDetailArea");
		rptAddDataWithCache("t40rountingDetailArea", setObjProCat(selectProductCat));
		rptDisplayTable("t40rountingDetailArea");
		Isdisplayrounting();
		IsCheckOneOff();
		sumTotal(rptGetDataFromDisplayAll("t40rountingDetailArea"));
		setHeaderOnchangeCorridor(setDataHaeder(pcRoute));
		//set transShipment
		if(gettransShipment == ""){
			gettransShipment = 'D';
		}
		console.log("gettransShipment :: "+gettransShipment);
		$('#t41transShipment-'+isSelect).val(gettransShipment);
		//console.log("gettransShipment :: "+$('#t41transShipment-'+isSelect).val());
		$("#s2-pol").attr("required", "false");
		$("#s2-pod").attr("required", "false");
		rutCloseDialog('b1-ProductCatalogue');
		ClearChange();//ClearChange
		IsviewRountingDetail();
	}
}

function ClearChange(){
	var chargeRow = rptGetDataFromDisplayAll("t52ChargesDetailsArea");
	if(chargeRow.length > 0){
		dialogGeneric("Warning", "*Please retrieve charges again for corridor" , "Ok");
		rptClearDisplay("t52ChargesDetailsArea");
	}

}

function setObjProCat(dataList){
	var arrayProCat = [];
	for(var i = 0 ; i < dataList.length ; i++){
		//console.log(JSON.stringify(dataList));
		arrayProCat.push({
			     voyageSeqNo : dataList[i].rowNo
			    //voyageSeqNo : dataList[i].isqno
				,fromLocTp : dataList[i].fromLocTp
				,fromLoc : dataList[i].fromLoc
				,toLocTp : dataList[i].toLocTp
				,toLoc : dataList[i].toLoc
				,transportationMode : dataList[i].transportationMode
				,operatorCode : ""
				,service : dataList[i].service
				,vessel : dataList[i].vessel
				,voyage : dataList[i].voyage
				,direction : dataList[i].direction
				//,transportCarrierDrayage : dataList[i].transportCarrierDrayage
				,transportCarrierDrayage : dataList[i].drayage //#map filed carrierName for display.
				,fromTerminal : dataList[i].fromTerminal
				,toTerminal : dataList[i].toTerminal
				,waitingTime : dataList[i].waitingTime
				,transitTime : dataList[i].transitTime
				,totalTime : dataList[i].transitTime + dataList[i].waitingTime
				,etd : dataList[i].etd
				,eta : dataList[i].eta
				,routingRef : dataList[i].routingRefNo
				,carrierName : dataList[i].drayage  ///----*****
				,drayage : dataList[i].transportCarrierDrayage
				,action: "i"

			});
		}
	return arrayProCat;
}

function isRouteChanged(pcRoute){

		var afterSeaLeg = false;
		var por, pol, pot1, pot2, pot3, pod, del;
		var seaLegCount = 0;

		//Collect port information
		for(var i=0; i<pcRoute.length; i++){
			if("M" === pcRoute.transportMode || "F" === pcRoute.transportMode ||
					"B" === pcRoute.transportMode){
				switch(seaLegCount){
				case 0:
					pol = pcRoute[i].fromLoc;
					break;
				case 1:
					pot1 = pcRoute[i].fromLoc;
					break;
				case 2:
					pot2 = pcRoute[i].fromLoc;
					break;
				case 3:
					pot3 = pcRoute[i].fromLoc;
					break;
				}
				seaLegCount++;
				afterSeaLeg = true;
				pod = pcRoute[i].toLoc;
			}else if("R" === pcRoute.transportMode || "T" === pcRoute.transportMode){
				if(!afterSeaLeg){
					por = pcRoute[i].fromLoc;
				}else{
					del = pcRoute[i].fromLoc;
				}
			}
		}

		if(!por){
			por = pol;
		}
		if(!del){
			del = pod;
		}

		//Compare port
		var getRoutingHeader = getRouting();
		for(var i = 0; i < getRoutingHeader.length; i++){
			if(i == isSelect){
				return (por !== getRoutingHeader[i].por || pol !== getRoutingHeader[i].d1-pol ||
						pot1 !== getRoutingHeader[i].pot1 || pot2 !== getRoutingHeader[i].d1-pot2 ||
						pot3 !== getRoutingHeader[i].d1-pot3 || del !== getRoutingHeader[i].d1-del);
				}
			}

	}

function setDataHaeder(data){
	var rounting = {};
	//console.log("setDataHaeder data :: " +JSON.stringify(data));
	for(var i=0; i<data.length; i++){
		if(i == 0){
			rounting = {
					por : (data[i].por == null) ? data[i].pol : data[i].por,
					pol : data[i].pol,
					pod : data[i].pod,
					pot1 : data[i].pot1,
					pot2 : data[i].pot2,
					pot3 : data[i].pot3,
					del : (data[i].del == null) ? data[i].pod : data[i].del,
					porHaul : (data[i].por == null) ? 'M' : 'C',
					delHaul : (data[i].del == null) ? 'M' : 'C'
					
			}
		}
	}
	
	//console.log("setDataHaeder :: " +JSON.stringify(rounting));
	
	return rounting;
}

function sumTotal(data){
	var getdataSum = data;
	var sumwaitingTime = 0;
	var sumtransitTime = 0;
	var sumtotalTime = 0;
	for(var i=0; i<getdataSum.length; i++){
		sumwaitingTime = sumwaitingTime + getdataSum[i].waitingTime;
		sumtransitTime = sumtransitTime + getdataSum[i].transitTime;
		sumtotalTime = sumtotalTime + (getdataSum[i].waitingTime + getdataSum[i].transitTime);
	}
	rutSetElementValue("t40sumwaitingTime", sumwaitingTime);
	rutSetElementValue("t40sumtransitTime", sumtransitTime);
	rutSetElementValue("t40sumtotalTime", sumtotalTime);
}

function getRouting_New(){
	var objRountingDetail = rptGetDataFromDisplayAll("t40rountingDetailArea");
	console.log("objRountingDetail :: "+JSON.stringify(objRountingDetail));
	for (i = 0; i < objRountingDetail.length; i++) {
		objRountingDetail[i].etd = splitDateonRegYMD(objRountingDetail[i].etd);
		objRountingDetail[i].eta = splitDateonRegYMD(objRountingDetail[i].eta);
	}

	

	return objRountingDetail;
}

function splitDateonRegYMD(getDate) {

	var NewDate = "";
	var reg = /[.,\/ -]/;

	if (getDate != "") {
		if (getDate != undefined) {

			if (getDate.charAt(2) == '/') {

				NewDate = getDate.split(reg).reverse().join('');

			} else if (getDate.charAt(4) == '-') {

				NewDate = NewDate = getDate.split(reg).join('');

			} else {
				NewDate = getDate;
			}

		}
	}
	return NewDate;
}


function getRoutingTabOutput(){
	var objRountingDetail = rptGetDataFromDisplayAll("t40rountingDetailArea");
	for (i = 0; i < objRountingDetail.length; i++) {
		objRountingDetail[i].etd = convertDateCheckDiff(objRountingDetail[i].etd);
		objRountingDetail[i].eta = convertDateCheckDiff(objRountingDetail[i].eta);


	}
	console.log("objRountingDetail :: "+JSON.stringify(objRountingDetail));
	return objRountingDetail;
}

function CheckDataOnChange_ObjectListNewtt(oldData,newDate,sortBy){
	 var NewArraySetAction = [];
	    NewArraySetAction = newDate;
	 if(!isEmpty(oldData)){
		NewArraySetAction.sort(function(a, b) {
		 	if(sortBy == "seqNo"){
 		  		var x = a.seqNo, y = b.seqNo;
		 	}else if(sortBy == "chargeSeqNo"){
				var x = a.chargeSeqNo, y = b.chargeSeqNo;
		 	}else if(sortBy == "voyageSeqNo"){
		 		var x = a.voyageSeqNo, y = b.voyageSeqNo;
		 	}else{
				var x = a.rowId, y = b.rowId;
		 	}
 		   return x < y ? -1 : x > y ? 1 : 0;
 	  });

	    if(!isEmpty(oldData)){
	    	oldData = JSON.parse(oldData);
	    	for(var b = 0; b < oldData.length; b++){ //Loop ORG DATA for get object.
	    		  if(!jsonEqual(JSON.stringify(oldData[b]), NewArraySetAction[b])){ // check Diff Json object.
	    			console.log(b + " oldData :: "+JSON.stringify(oldData));
		 	    	console.log(b + " NewArraySetAction :: "+JSON.stringify(NewArraySetAction));
	    			if(NewArraySetAction[b].action != 'i' && NewArraySetAction[b].action != 'd'){ //Add action u
	   			 		NewArraySetAction[b].action = "u";
		    	    }
	    		 }
	    	 }
	    		//console.log(" NewArraySetAction test "+JSON.stringify(NewArraySetAction));
	      }
	}
       return NewArraySetAction;
 }

	function clearfiled(){
		rutSetElementValue("t40sumwaitingTime", "0.00");
		rutSetElementValue("t40sumtransitTime", "0.00");
		rutSetElementValue("t40sumtotalTime", "0.00");
	}

	function ischeckfscRounting(){
		var HedRouting = rptGetDataFromDisplayAll("t41RoutingHeader");
		for (var x = 0; x < HedRouting.length; x++) {
			setCustomTagProperty("t41transShipment-"+x, {dis:true});
		}
		$("#_t41Getproduct").remove();

	}

	function IsviewRountingDetail(){
		var HedRoutinDetail = rptGetDataFromDisplayAll("t40rountingDetailArea");
		for (var i = 0; i < HedRoutinDetail.length; i++) {
			setCustomTagProperty("t40totalTime"+"-"+i, {dis:true});
			$.each(HedRoutinDetail[i], function(k, v) {
				setCustomTagProperty("t40"+k+"-"+i, {dis:true});
			});
		}
	}


</script>