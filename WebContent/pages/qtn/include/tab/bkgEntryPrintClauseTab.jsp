<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@include file="/pages/misc/RcmInclude.jsp"%>

<rcl:area id="d70printClauseArea" collapsible="Y" title="Print Clause">
	<div class="row"> 

		<rcl:text id="d70PrintClauseHeader.bookingNo" label="Booking#" classes="div(col-md-3) ctr(dtlField)" options="disabled" />

		<rcl:text id="d70PrintClauseHeader.country" label="Country" classes="div(col-md-3) ctr(dtlField)" options="disabled" />

		<rcl:text id="d70PrintClauseHeader.fsc" label="FSC" classes="div(col-md-3) ctr(dtlField)" options="disabled" />

		<rcl:text id="d70PrintClauseHeader.documentType" label="Document Type" classes="div(col-md-3) ctr(dtlField)" options="disabled" />

	</div>
</rcl:area>

<!-- Clause Head -->
<div id="t70clauseHead" class="rcl-standard-widget-header tblHeader">
	<label>Clause </label>
</div>
	
<div class="container-fluid border">
   <div class="row">
   
   	   <div class="col-md-4" style="font-weight:bold;">
           <label class="rcl-std">Select</label>
       </div>
       
       <div class="col-md-4" style="font-weight:bold;">
           <label class="rcl-std">Clause Title</label>
       </div>
       
       <div class="col-md-4" style="font-weight:bold;">
           <label class="rcl-std">Delete </label>
       </div>
       
   </div>
</div>
	
<div id="t70clauseArea" class="container-fluid ascanList1 rcl-standard-search-header tblArea">
	<div id="t70row" class="row tblRow">
	
		<div class="col-md-4">
           	<input id="t70Clause.selectclauseTitle" 
           		   type="radio" 
           		   name="select" 
           		   onclick="setClauseDetais('#-RowId-#')"
           		   class="tblField" />
        </div>
			
		<rcl:text id="t70Clause.clauseTitle" name="Clause Title" classes="div(col-md-4) ctr(tblField)" options="readonly" />
		
		<div class="col-md-4">
           	<input id="t70Clause.selectDelete" 
           		   type="checkbox" 
           		   name="selectClause" 
           		   class="tblField" />
        </div>	
						
	</div>
</div>
<!-- Clause End -->


<!-- Clause Details Header  -->
<div id="t71clausedetailsHead" class="rcl-standard-widget-header tblHeader">
	<label>Clause Details </label>
</div>
	
<div class="container-fluid border">
   <div class="row">
   
   	   <div class="col-md-2" style="font-weight:bold;">
           <label class="rcl-std">Seq#</label>
       </div>
       
       <div class="col-md-10" style="font-weight:bold;">
           <label class="rcl-std">Clause</label>
       </div>
       
   </div>
</div>
	
<div id="t71clausedetailsArea" class="container-fluid ascanList1 rcl-standard-search-header tblArea">
	<div id="t71row" class="row tblRow">
	
		<rcl:text id="t71index" name="Index" classes="div(col-md-2) ctr(tblField)" options="readonly" />
			
		<rcl:text id="t71clause" name="Clause" classes="div(col-md-10) ctr(tblField)" options="readonly" />
		
	</div>
</div>
<!-- Clause Details End -->
	

<script type="text/javascript">

	function setClauseDetais(row){
		
		var listClause = rptGetDataFromDisplay('t70clauseArea', row).ClauseDetails || [];

		rptClearDisplay("t71clausedetailsArea");
		rptAddData("t71clausedetailsArea", addIndexTable(listClause));
		rptDisplayTable("t71clausedetailsArea");
	}
		
</script>
	
	
