

<meta http-equiv="X-UA-Compatible" content="IE=11" />
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%
	response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	String title=request.getParameter("title");
	String content=request.getParameter("content");
	String effdate =request.getParameter("effdate"); 
	

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<TITLE><%=title%></TITLE>
	<META content="text/html; charset=windows-1252" http-equiv=Content-Type>
	
	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../../include/tab_js.jsp" %>
	<%@include file="../../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/rclcustom.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/loader.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/Quotation.css">

	<script src="../../../../js/rutUtilities.js"></script>
	<script src="../../../../js/qtnPowerTable.js"></script>
	<script src="../../../../js/RutHelp.js"></script>

	<script src="../../../../js/rutLoader.js"></script>

	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/esnAjax.js"></script>

	<script src="../../../../js/FileSaver.min.js"></script>
	<script src="../../../../js/FileSaver.js"></script>

	<script src="../../../../js/quotation.js"></script>
	
	<style>
	.copyDate{
			width:30%;
	}
	</style>

</HEAD>

<BODY class="TableLeftWhiteRepHead">
	<br><br>
	<table width="100%">
		<tr style="text-align: center;">
				<td><img border="0" src="../../../../images/qq.jpg" width="50" height="50"><span id="content" class="TableLeftWhiteRepHead">Copy as one-off quotation?</span></td>
				
		</tr>
	</table>
	<center>
		<div id="copydialog">
		
			<input id="comm_effdate" name="comm_effdate" value="<%=effdate%>" style="display:none;"  />
			<button type="button" class="rcl-standard-button" onclick="setAction('Y');window.close();">Yes</button>
			&nbsp;<button type="button" class="rcl-standard-button" onclick="setAction('N');">No</button>
			&nbsp;<button type="button" class="rcl-standard-button" onclick="window.close();">Cancel</button>
		</div>
			<br>
		<div class="copyQuotY" id="copyQuotY" style="display:none; margin-top: 5px;" >
		<!-- rcl:date id="c0-date" label="Expiry date" classes="div(col-sm-2)" options="onchange='chkDate(this.value);'"/-->
			<div class="row">
				<div class="col-md-2">
					<label class="rcl-std" style="font-weight: bold;">Expiry date</label>
					<input id="c0-date" type="text" style="width:30%;" autocomplete="off" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font searchField hasDatepicker" data-ct="bt-date" data-type="date" onchange="chkDate(this.value);">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<button id="c0-btnK" type="button" class="rcl-standard-button" onclick="setAction('K');window.close();" style="margin-left: 0px; margin-right: 15px">Ok</button>
				</div>
			</div>
		</div>
	</center>
	
	
</BODY>
<SCRIPT>


//$('#c0-date').addClass('copyDate');
var futureDate = "";

$('.hasDatepicker').change(function(){
	validatedateFormatecopy(this.id,this.value);
	//validatedateFormate(this.id,this.value);

});

function chkDate(isValue){
	//var dateValue = new Date(conValidatedateFormatecopy(isValue));
	var dateValue = new Date(conValidatedateFormate(isValue));
	var dateToday = new Date();
	var dateEff = new Date($('#comm_effdate').val());
	var setStr = "";
	if(dateValue < dateToday){
		setStr = "Expiry date should be greater than "+ converntDateShow(dateToday);
	}else if(dateValue < dateEff){
		setStr = "Expiry date should be greater than "+ converntDateShow(dateEff);
	}
	if(setStr != ""){
		dialogGeneric("Warning", setStr , "Ok");
		 document.getElementById('c0-date').value = futureDate;
	}
	 
	
	
}

	function converntDateShow(Isdate){
		var today = Isdate;
		var month = today.getMonth() + 1;
		var day = today.getDate();
		var date = (day < 10 ? '0' : '') + day +
			'/' +(month < 10 ? '0' : '') + month + '/' +
			today.getFullYear();
		
		return date;
	}
	 
	function setAction(reply) {
		
		var date = new Date();
		date.setDate(date.getDate() + 30);
		var twoDigitFutDate = (((date.getDate() + "").length === 1) ? '0' + date.getDate() : date.getDate());
		var twoDigitFutMonth = (((date.getMonth().length + 1) === 1) ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1));

		if(twoDigitFutMonth.length >= 3){
			twoDigitFutMonth = twoDigitFutMonth.substring(1);
		}
		
		//var futureDate = date.getFullYear() + "-" + twoDigitFutMonth + "-" + twoDigitFutDate;
		futureDate = twoDigitFutDate + "/" + twoDigitFutMonth + "/" + date.getFullYear();

		var x = document.getElementById('copyQuotY');
		
		if (reply == 'N') {
			 x.style.display = 'block';
			 document.getElementById('c0-date').value = futureDate;
		}else if(reply == 'K'){
			opener.copyQuoNocase(document.getElementById('c0-date').value);
		} else {
			opener.gotoPages(reply);
		}
	}
	
	//old function 
	/*function setAction(reply) {
		console.log(reply)
		if (reply == 'N') {
			opener.copyQuoNocase();
		} else {
			opener.gotoPages(reply);
		}
	}*/
	
	function conValidatedateFormatecopy(getDate) {
		var NewDate = "";
		var reg = /[.,\/ -]/;

		if (getDate != undefined) {

			NewDate = getDate.split(reg);
			NewDate = NewDate[1] + "/" + NewDate[0] + "/" + NewDate[2];


		}

		return NewDate;
	}
	
	function validatedateFormatecopy(inputid, inputText) {
		//var getdate = conValidatedateFormatecopy(inputText);
		var getdate = conValidatedateFormate(inputText);
		var date = new Date(getdate);
		if (date == 'Invalid Date') {
			$('#' + inputid).val("");
			dialogGeneric("Warning", "Invalid date format!", "OK").then(function (result) {
				$('#' + inputid).focus();
			});
		}

	}
	
	
</SCRIPT>

</HTML>