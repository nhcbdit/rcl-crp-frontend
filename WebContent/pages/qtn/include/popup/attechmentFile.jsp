<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
	String title=request.getParameter("title");
	String qnt=request.getParameter("qnt");
	String status = request.getParameter("status");
	String userData = request.getParameter("userData");

%>

	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../../include/tab_js.jsp" %>
	<%@include file="../../include/datetimepicker_js.jsp" %>


	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/loader.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/Quotation.css">

	<script src="../../../../js/rutUtilities.js"></script>
	<script src="../../../../js/qtnPowerTable.js"></script>
	<script src="../../../../js/RutHelp.js"></script>

	<script src="../../../../js/rutLoader.js"></script>

	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/esnAjax.js"></script>

	<script src="../../../../js/FileSaver.min.js"></script>
	<script src="../../../../js/FileSaver.js"></script>

	<script src="../../../../js/quotation.js"></script>


	<TITLE><%=title%></TITLE>
	<style>
.inputclass{
	background: transparent !important;
    border: none !important;
}
.listsearch{
	margin-bottom: 0px;
    font-weight: 500;
    word-wrap: break-word;
}
</style>
</head>
<body id="qtn002">
<input id="qnt" type="text" value="<%=qnt%>" style="display:none;">

<form name="frm1"  class="upload-box">

        <rcl:area id="d81attechment" classes="div(container-fluid)" title="File">

		  <div class="row" style="margin-right: 0px;">


		  	<rcl:text id="d81businessObject" label="Business Object" classes="div(col-sm-3) ctr(dtlField)"  options="disabled" />

		  	<rcl:select id="d81contentType"
						label="Content Type"
						classes="div(col-sm-3) ctr(dtlField)"/>

		  	<rcl:text id="d81quotationNo" classes="div(col-sm-3) ctr(dtlField)"  label="Quotation No" options="disabled" />
		  	<div class=" col-1x0" style="text-align: right;">
				<label for="d81confidential">Confidential</label>
				<input type="checkbox" id="d81confidential" class="dtlField"/>
			</div>
		  </div>

		   <div class="row" style="margin-right: 0px;">
		  	<input id="s0-userId" type="text" class="tblField" style="display:none;" value="DEV_TEAM" />
		  	<div class="col-sm-6">
					<div class="cssinline">
					   <div class="col-md-9 pl-0 pr-0 ml-0">
							<label for="d81fileName">File Name</label>
								<div class="rcl-standard-input-wrapper">
									<input type=file name="browse" id="browse" onchange="fileSelected();" style="display: none;">
									<input id="d81fileName" name="fileName" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font dtlField dtlField" />
								</div>
						</div>

					    <div style="padding-top: 14px; font-size: 12px;">
					    	<input type="button" id="get_file" value="Browse..." style="margin-left: 1px;margin-right: 1px;">
					    </div>
					     <div style="padding-top: 14px; font-size: 12px;">
					    	<input type=button id="btnuploadFile" onClick="uploadFile()" value="Upload" style="margin-left: 3px;">
					    </div>
					</div>
				</div>

		  	<rcl:text id="d81fileDescription" label="File Description" classes="div(col-sm-6) ctr(dtlField)" />

		  </div>

		   <%-- div id="fileName" ></div>
		  <div id="fileSize" ></div>
		  <div id="fileType" ></div>
		  <div id="progress" ></div>
		  <div id="log" ></div--%>

		  </rcl:area>


	<rcl:area id="t89UploadedFile" title="Uploaded File" classes="div(container-fluid pl-1 pr-1)" areaMode="table" >
		<div class="row border"  style="margin-right: 0px;margin-left: 0px;">

			<div class="col-sm-4 col-md-2  offset-0x5" style="font-weight: bold;">
				<label class="rcl-std">File Name</label>
			</div>
			<div class="col-sm-3 col-md-1 pr-0 pl-0 " style="font-weight: bold;">
				<label class="rcl-std">QuotationNo</label>
			</div>
			<div class="col-sm-3 col-md-1 pr-0 " style="font-weight: bold;">
				<label class="rcl-std">Content Type</label>
			</div>
			<div class="col-sm-3 col-md-1 pr-0 " style="font-weight: bold;">
				<label  class="rcl-std">Confidential</label>
			</div>
			<div class="col-sm-6 col-md-3"
				style="font-weight: bold;">
				<label class="rcl-std">File Description</label>
			</div>
			<div class="col-sm-6 col-1x5 "
				style="font-weight: bold;">
				<label class="rcl-std">Uploaded DateTime</label>
			</div>
			<div class="col-sm-6 col-1x25"
				style="font-weight: bold;">
				<label class="rcl-std">Uploaded User</label>
			</div>


			<div class="col-sm-6 col-0x5" style="font-weight: bold; padding-left: 0px;">
				<label class="rcl-std">Delete</label>
				<input type="checkbox" id="select_all" style="margin-left: 10px;"/>
			</div>


		</div>


		<div class="tblArea">
			<div id="t89row" class="row tblRow pt-1" style="margin-right: 0px;">
				<div class="container-fluid" >
					<div class="row gridRow" >
					<div class="col-0x5 pr-0 pl-1">
							<input id="t89fileattachMentId" type="text" class="tblField" style="display: none;" />
							<input type="radio" id="t89selectordow" name="selectordow" value="Y" class="tblField">
				    </div>
						<div class="col-sm-4 col-md-2 pr-0">
							<p id="fileName" data-ct="bt-text" class="listsearch"></p>
							<input id="t89fileName" type="text" class="tblField" style="display: none;" />

						</div>
						<%-- div class="col-sm-3 col-md-1 pr-0 pl-0">
							<p id="t89quotationNo" data-ct="bt-text" class="tblField"></p>
						</div--%>
						<rcl:text id="t89quotationNo" classes="div(col-sm-3 col-md-1 pr-0 pl-0) ctr(tblField inputclass)" options="disabled" />
						<%-- div class="col-sm-3 col-md-1 pr-0 " >
							<p id="t89contentType" data-ct="bt-text" class="tblField"></p>
						</div--%>
						<rcl:text id="t89contentTypeValue" classes="div(col-sm-3 col-1x25 pr-0 pl-0) ctr(tblField inputclass)" options="disabled" />
						<input id="t89contentType" type="text" class="tblField" style="display: none;" />
						<%-- div class="col-sm-3 col-md-1 pr-0 " >
							<p id="t89confidential" data-ct="bt-text" class="tblField"></p>
						</div--%>
						<rcl:text id="t89confidentialValue" classes="div(col-sm-3 col-0x75 pr-0 pl-0) ctr(tblField inputclass)" options="disabled" />
						<input id="t89confidential" type="text" class="tblField" style="display: none;" />

						<div class="col-sm-6 col-md-3 pr-0">
							<p id="fileDescription" data-ct="bt-text" class="listsearch"></p>
							<input id="t89fileDescription" type="text" class="tblField" style="display: none;" />
						</div>

						<%-- div class="col-sm-6 col-1x5 pr-0">
							<p id="t89uploadedDateTime" data-ct="bt-text" class="tblField"></p>
						</div--%>
						<rcl:text id="t89updatedDateTime" classes="div(col-sm-6 col-1x5 pr-0 pl-0) ctr(tblField inputclass)" options="disabled" />
						<%-- div class="col-sm-6 col-1x5 pr-0">
							<p id="t89uploadedUser" data-ct="bt-text" class="tblField"></p>
						</div--%>
						<rcl:text id="t89updatedUser" classes="div(col-sm-6 col-1x25 pr-0 pl-0) ctr(tblField inputclass)" options="disabled" />
						<div id="t89selectDeleteLabel" class="col-sm-6 col-0x5" style="text-align: center; padding-left: 0px;">
								<input id="t89selectDelete" type="checkbox" class="tblField mt-2 checkbox" />
						</div>
					</div>
				</div>
			</div>
		</div>


			<div class="row" style="margin-right: 0px;">
				<div class="col-md-12"
					style="text-align: right; margin-top: 5px; margin-bottom: 5px;">
					<button id="_t89addBtn" type="button" class="rcl-standard-button" onclick="Dowloadfile()">Download</button>
					<button id="_t89deleteBtn" type="button" class="rcl-standard-button" onclick="Deletefile()">Delete</button>
				</div>
			</div>

	</rcl:area>

    </form>

</body>

<script type="text/javascript">
var GetuserData;
var qnt;
var status;

$(document).ready(function() {
	rptTableInit("t89UploadedFile");
	fnCheckbookAll();
	qnt = '<%=qnt%>';
	status = '<%=status%>';
    GetuserData = JSON.parse(sessionStorage.getItem("userData"));
	
    
    if(qnt != "" || qnt != undefined){

    	getContentType();

    	var data = {
    			   "userId":GetuserData.userId,
    			   "businessObject":"Quotation Entry",
    			   "quotationNo":qnt,
    			   "userData":GetuserData
    			};

    	getResultAjaxUploadFile("WS_QTN_GET_ATTACHMENT", data).done(handleDisplayAttechmentFile);

			function handleDisplayAttechmentFile(response) {
				if(response.Success && response.resultCode != '500'){
					if(response.resultContent != null){
						fetchDataAttechment(response.resultContent);
					}
				}else{ 
						if(response.resultCode == '401'){
							opener.attechmentExpir();
							window.close();
						}
				}
			}

			//fetchDataAttechment();

    }

	console.log($('#qnt').val());
	console.log("userData :"+JSON.stringify(GetuserData));
	$('#d81quotationNo').val(qnt);
	$('#d81businessObject').val("Quotation Entry");
	document.getElementById('get_file').onclick = function() {
	    document.getElementById('browse').click();
	};
	
	IssetButtonUpAndDel();

	 $('.checkbox').on('click',function(){
		if($('.checkbox:checked').length == $('.checkbox').length){
	            $('#select_all').prop('checked',true);
	        }else{
	            $('#select_all').prop('checked',false);
	       }
	    });
});

function IssetButtonUpAndDel(){
	if(status == 'L' || status == 'H'){
		$('#get_file').css('display', 'none');
		$('#btnuploadFile').css('display', 'none');
		$('#_t89deleteBtn').css('display', 'none');
		$('#d81fileName').attr('readonly', 'readonly');
	}
	
}

function fetchDataAttechment(Content){
	var getdata = [];
		//getdata = DataList();
		getdata = Content;
	if(getdata.length > 0){
		for(var x = 0; x < getdata.length; x++){
			if(x == 0){
				getdata[x].selectordow = 'Y';
			}


		}
	}

	rptAddDataWithCache("t89UploadedFile", getdata);
	rptDisplayTable("t89UploadedFile");
	var getlist = rptGetDataFromDisplayAll("t89UploadedFile");
	IssetDisplay(getlist);

	if(getlist.length > 0){
		$('#t89selectordow-0').prop('checked',true);
	}

}

function IssetDisplay(data){
	if(data.length > 0){
		for(var x = 0; x < data.length; x++){
			$('#fileName-'+x).text(data[x].fileName);
			$('#fileDescription-'+x).text(data[x].fileDescription);

			$('#t89confidential-'+x).css('text-align','center');
			$('#t89confidentialValue-'+x).val(issetdata(data[x].confidential));

		}
	}
}

function issetdata(data){
	var result = "Yes";
	if(data == 'N'){
		result = "No"
	}

	return result;
}

function getContentType(){
	getResultAjaxUploadFile("WS_QTN_GET_CONTENTYPE", {
		   "userId":GetuserData.userId,
		   "businessObject":"Quotation Entry",
		   "quotationNo":qnt,
		   "userData":GetuserData}
	).done(handleDisplayContentType);

	function handleDisplayContentType (response){
		if(response.Success){
			if(response.resultContent != null){
				var getdata = response.resultContent;
				 var ContentTypeElement=document.getElementById("d81contentType");
				var getop = '';
				for(var z = 0; z < getdata.length; z++){
					getop += '<option value='+getdata[z].CONTENT_CODE+'>'+getdata[z].CONTENT_TYPE+'</option>';
				}
				ContentTypeElement.innerHTML = getop;

			}
		}
	}
}

function fnCheckbookAll(){
	$('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
                isForLoopCheckDelete('T');
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
                isForLoopCheckDelete('F');
            });
        }
    });


}

	function isForLoopCheckDelete(flage){
		var allData = rptGetDataFromDisplayAll("t89UploadedFile");
		console.log("isForLoopCheckDelete() ::"+JSON.stringify(allData));
		if(allData.length > 0){
			allData.forEach(function(row){
				if(flage == 'T'){
					row.selectDelete = "Y";
				}else if(flage == 'F'){
					row.selectDelete = "";
				}
			});

		}

	}

function fileSelected() {
	  var fullpaht = $('input[type=file]').val();
	  var file = document.getElementById('browse').files[0];

	  if (file) {
		//************//
		 var fileSize = 0;
	    if (file.size > 1024 * 1024)
	      fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
	    else
	    fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
	   	/*document.getElementById('fileName').innerHTML = 'Name: ' + file.name;
	    document.getElementById('fileSize').innerHTML = 'Size: ' + fileSize;
	    document.getElementById('fileType').innerHTML = 'Type: ' + file.type;*/
	  //************//

		 var SizeFile = file.size / 1024 / 1024; // in MB
		 if (SizeFile > 1) {
			 $('#browse').val("");
			 dialogGeneric("Warning", "File size exceeded maximum limit of 1MB.", "Ok");
		}else{
			 $('#d81fileName').val(file.name);

		 }

		}

	}


function uploadFile() {
	var file = document.getElementById('browse').files[0];

	  if (!file) {
		  dialogGeneric("Warning", "Input File no required.", "Ok");
	  }else{
			var quotationAttehment = {};
			quotationAttehment = rptGetDataFromSingleArea("d81attechment");
			var orgName = quotationAttehment.fileName;
			var fileName = IsChangeFileName(quotationAttehment.fileName);
			var setDataCheckDup = {};
			var dataDelet = {};
			var setfileName = [];

			quotationAttehment.fileName = fileName;
			quotationAttehment.confidential = fngetcheckfield(quotationAttehment.confidential);
			quotationAttehment.action = 'i';
			var setdata = [];
			setdata.push(quotationAttehment);

			setDataCheckDup = {
					"userData" : GetuserData,
					"quotationNo":quotationAttehment.quotationNo,
					"fileName":fileName
			}

			setfileName.push({
				"name" : fileName
			});
			dataDelet = {
					"listFile": setfileName
			}
			 console.log("setDataCheckDup ::"+JSON.stringify(setDataCheckDup));

			getResultAjaxUploadFile("WS_QTN_GET_DUPFILMNAME", setDataCheckDup).done(handleGetDup);

			function handleGetDup(response){
				 if(response.Success){
					 if(response.resultContent < 1){
						 uploadFiletoServ();
					 }else{
						 dialogGeneric("Warning", orgName + " already exist under Business Object Quotation <br> Entry, Do you Want to overwrite it?" , "Ok", "No").then(
		    						function(overwrite){
		    							if(overwrite){
		    								getResultAjaxUploadFile("delete", dataDelet).done(handleDeleteFile);
		    							function handleDeleteFile(response) {
		    								if(response.Success){
		    									 uploadFiletoServ();
		    								}
		    							}

		    							}
		    						});
					 }

			   	}
			}

			function uploadFiletoServ(){
				var file_data = document.getElementById('browse').files[0];
				var newFileName = file_data.name.split(".");
				var fileType = newFileName[1];
					newFileName = qnt +"_"+ newFileName[0]+'.'+fileType;
				var fd = new FormData();
				fd.append("file", file_data, newFileName);

				 getResultAjaxUploadFile("upload", fd).done(handleDisplay);

				 function handleDisplay(response){
					 console.log("response upload ++ "+response)
					 response = JSON.parse(response);
					 if(response.resultStatus == 'S'){
						 IsUploadFile();
					} else {
						if(response.responseCode == "401"){
							opener.attechmentExpir();
							window.close();
						}
				   	}
				 }
			}

			function IsUploadFile(){

				 var data = {
							"userData" : GetuserData,
							"dataContain":setdata

						};

				 console.log("uploadFile ::"+JSON.stringify(data));

				 getResultAjaxUploadFile("WS_QTN_SAVE_ATTACHMENT", data).done(handleSaveAttechmentFile);

				 function handleSaveAttechmentFile(response) {
						if(response.resultCode == '200'){
							 dialogGeneric("Info", response.resultMessage, "OK").then(function(result){
									location.reload();
								});
						}else {
							if(response.resultCode == '401'){
								opener.attechmentExpir();
								window.close();
							}
						}
					}
			 }
	  }
}

function IsChangeFileName(FileName){
	var newFileName = FileName.split(".");
	var fileType = newFileName[1];
		newFileName = qnt +"_"+ newFileName[0]+'.'+fileType;
	return newFileName;
}

function Dowloadfile(){
	var getbaseurl = SetPathUpload().baseurl2+"QTNWSWebApp/rclwsupload/";
	
	/*console.log("SetPathUpload().upfile :; "+SetPathUpload().urlUpfile);*/
	//console.log("getbaseurl :; "+getbaseurl);
	var url = getbaseurl+"Quotationupload/download/";
	var allData = rptGetDataFromDisplayAll("t89UploadedFile");
	var deleting = "";
	var checkboxIdWithoutPrefix = "selectordow";
	allData.forEach(function(row){
		if(row[checkboxIdWithoutPrefix] === 'Y'){
			console.log("GetDowloadRow ::"+JSON.stringify(row));
			window.location.assign(url+row.fileName);

		}
	})
}

function setarrayDeletAll(object, newarray){
	var getdelete = HanderGetDataDeleteAllv2(object, "selectDelete");
	if(getdelete.length > 0){
		for (var i = 0; i < getdelete.length; i++) {
			newarray.push(getdelete[i]);
		}
	}
	return newarray;
}

function Deletefile(){

	//HanderGetDataDeleteList("t89UploadedFile","selectDelete");
	var error = HanderGetDataDeleteList("t89UploadedFile","selectDelete");
	if(!error){
		return;
	}


	var allData = rptGetDataFromDisplayAll("t89UploadedFile");
	console.log("allData test ::"+JSON.stringify(allData));
	var setData = [];
	var setName = [];

	for (var i = 0; i < allData.length; i++) {
		var setobject = {};
		if(allData[i].action == 'd' ){
			setData.push(allData[i]);
			setobject.name = allData[i].fileName
			console.log("allData[i].fileName ::"+allData[i].fileName);
			setName.push(setobject);
		}

	}

	var data = {
		"userData" : GetuserData,
		"dataContain":setData

	};

	var dataDelet = {
			"listFile":setName
	}

	console.log("Deletefile ::"+JSON.stringify(dataDelet));
	getResultAjaxUploadFile("WS_QTN_SAVE_ATTACHMENT", data).done(handleDeleteAttechmentFile);

		function handleDeleteAttechmentFile(response) {
			if(response.resultCode == '200'){
				// delete File form server
				IsDeletfile();
			}else {
				
				if(response.resultCode == '401'){

					opener.attechmentExpir();
					window.close();
				}
				
			}
		}

		function IsDeletfile(){
			getResultAjaxUploadFile("delete", dataDelet).done(handleDeleteFile);
		}

		function handleDeleteFile(response) {
			if(response.Success){
				 var setValue = response.responseMessage.replace(/[.*+?^""|[\]\\]/g, "")
				 dialogGeneric("Info", setValue, "OK").then(function(result){
						location.reload();
					});
			}else{
				if(response.resultCode == '401'){
					opener.attechmentExpir();
					window.close();
				}
			}
		}


}


function HanderGetDataDeleteList(tableId, checkboxIdWithoutPrefix){
	var errorMsg = "";
	var result = true;
	var Alldata = [];
	var allDataList = rptGetDataFromDisplayAll(tableId);
	console.log("allDataList ::"+JSON.stringify(allDataList));
	allDataList.forEach(function(rowlist){
		if(rowlist[checkboxIdWithoutPrefix] === 'Y'){
			Alldata.push(rowlist);
			deletingList = rowlist;
			deletingList.action = "d";
		}
	});
	if(Alldata.length < 1 || Alldata.length == undefined){
		result = false;
		errorMsg = "The selected recode and its relevant data will be deleted. Do you want to continue?";
	}

	if(!result){
		dialogGeneric("Warning", errorMsg, "Ok");
	}
	return result;
}


</script>

</body>
</html>