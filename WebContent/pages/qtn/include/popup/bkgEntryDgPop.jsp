<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "rcl" uri = "/WEB-INF/custom.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<%@include file="../../include/tab_js.jsp" %>	
	
	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">
	
	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/rutUtilities.js"></script>
	
	<style>
	
		body { 
				overflow-x:hidden;
				overflow-y:hidden; 
				margin: 0 !important;
			 }
			 
	</style>

<title>DG Commodities</title>
</head>
<body style="margin: 0 px;">

	<div id="d0-printremarkHeader" class="rcl-standard-widget-header">
		<label>DG Commodities</label>
	</div>

	<!-- Start General Header Area -->
	<div id="d0-printremarkArea">
		<form id="frm">
			<div class="rcl-standard-search-header">

				<div class="row">
				
					<rcl:text id="d0-flashPoint" label="Flash Point" classes="div(col-sm-1)" check="len(6)" />
					
					<rcl:select id="d0-flashPointUnit"
								label="Unit"
								classes="div(col-sm-1 pl-0)"
								optionsList="Farenheit Celcius"
								valueList="F C" />
							
					<rcl:text id="d0-unno" label="UNNO" classes="div(col-sm-1 pl-0)" check="len(5)" />	
					
					<rcl:text id="d0-variant" label="Variant" classes="div(col-sm-1 pl-0)" check="len(1)" />
					
					<rcl:text id="d0-imdgClass" label="IMDG CLASS" classes="div(col-sm-2 pl-0)" check="len(4)" />
					
					<rcl:text id="d0-portClass" label="Port CLASS" classes="div(col-sm-2 pl-0)" check="len(5)" />
					
					<div class="col-sm-2 pl-0">
						<label>Residue Only </label>
						<input type="checkbox" id="d0-residueOnly" />
					</div>
					
					<div class="col-sm-2 pl-0">
						<label>Fumigation Only </label>
						<input type="checkbox" id="d0-fumigationOnly" />
					</div>
														            			
				</div>
								
				<div class="row">
					<div class="col-sm-12" style="text-align:right;">
						<button type="button" class="rcl-standard-button" onclick="returnResult('d0-printRemarks')">OK</button>
						<button type="button" class="rcl-standard-button" onclick="returnYourChoice('')" style="margin-right:5px">Close</button>
					</div>
				</div>
				
				<label id="lblFirstName"></label>
				<label id="lblLastName"></label>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
				
		function returnResult(elment) {
			opener.setResultPrintRemark(targetField, rutGetElementValue(elment));
			window.close();
		}
	
	</script>
	
</body>
</html>