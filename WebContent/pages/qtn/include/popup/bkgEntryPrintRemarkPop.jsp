<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<%@include file="../../include/tab_js.jsp" %>	
	
	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">
	
	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/rutUtilities.js"></script>
	
	<style>
	
		body { 
				overflow-x:hidden;
				overflow-y:hidden; 
				margin: 0 !important;
			 }
			 
	</style>

<title>Print Remark</title>
</head>
<body style="margin: 0 px;">

	<div id="d0-printremarkHeader" class="rcl-standard-widget-header">
		<label>Print Remark</label>
	</div>

	<!-- Start General Header Area -->
	<div id="d0-printremarkArea">
		<form id="frm">
			<div class="rcl-standard-search-header">

				<div class="row">
				
					<div class="col-sm-12">
		                <label>Print Remark</label>
		                <div class="rcl-standard-input-wrapper">
		                	<textarea class="rcl-standard-rcl-standard-form-control" id="d0-printRemarks" style="width:500px;height:200px"></textarea>		                 
		                </div>
		            </div>		
		            			
				</div>
				
				<div class="row">
				
					<div class="col-sm-12" style="text-align:right;">
						<button id="d0-saveResult" type="button" class="rcl-standard-button" onclick="returnResult('d0-printRemarks')">OK</button>
						<button id="d0-close" type="button" class="rcl-standard-button" onclick="window.close();" style="margin-right:5px">Close</button>
					</div>
					
				</div>
				
				<label id="lblFirstName"></label>
				<label id="lblLastName"></label>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
				
		function returnResult(elment) {
			opener.setResultPrintRemark(targetField, rutGetElementValue(elment));
			window.close();
		}
	
	</script>
	
</body>
</html>