<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "rcl" uri = "/WEB-INF/custom.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<%@include file="../../include/tab_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">

	<script src="../../../../js/esnAjax.js"></script>
	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/rutUtilities.js"></script>
	<script src="../../../../js/qtnPowerTable.js"></script>

	<style>

		body {
				overflow-x:hidden;
				overflow-y:hidden;
				margin: 0 !important;
			 }

	</style>

<title>Request Advice Details Pop-up</title>
</head>
<body style="margin:0px;">

<input id="hdfbookingNO" type="text">

<!-- Request Advice Head -->
<div id="t1-remarksCategoryHead" class="rcl-standard-widget-header tblHeader">
	<label>Request Advice</label>
</div>

<div class="container-fluid border">
   <div class="row">

   	   <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std">Special Cargo</label>
       </div>

       <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std">BBK</label>
       </div>

       <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std">OOG</label>
       </div>

       <div class="col-md-3" style="font-weight:bold;">
           <label class="rcl-std" >UC</label>
       </div>

   </div>
</div>

<div id="t0-requestAdviceArea" class="container-fluid ascanList1 rcl-standard-search-header tblArea">
	<div id="t0-row" class="row tblRow">

		<rcl:text id="t0-specialCargo" name="Special Cargo" classes="div(col-md-3) ctr(tblField)" options="readonly" />

		<rcl:text id="t0-bbk" name="BBK" classes="div(col-md-3) ctr(tblField)" options="readonly" />

		<rcl:text id="t0-oog" name="OOG" classes="div(col-md-3) ctr(tblField)" options="readonly" />

		<rcl:text id="t0-uc" name="UC" classes="div(col-md-3) ctr(tblField)" options="readonly" />

	</div>

	<hr>

	<div class="col-sm-12" style="text-align:right;">
		<button id="d0-close" type="button" class="rcl-standard-button" onclick="window.close();" style="margin-right:5px">Close</button>
	</div>

</div>
<!-- Remarks Category End -->

<script type="text/javascript">

$(document).ready(function() {

	rptTableInit("t0-requestAdviceArea");

	var bookingNo = document.getElementById("hdfbookingNO").value;

	init(bookingNo);
});

function init(bookingNo) {
	debugger;
	getResultAjax("WS_BKG_GET_ADVICE_DETAILS", { bookingNo : "BTAOC09001193" }).done(handleSetData);

	function handleSetData(response){
		console.log(response);
		rptAddData("t0-requestAdviceArea", response);
		rptDisplayTable("t0-requestAdviceArea");
	}

}

// $(document).ready(function() {
// 	var kkk = document.getElementById("hdfbookingNO").value;
// 	console.log(document.getElementById("hdfbookingNO").value);
// });

</script>


</body>
</html>