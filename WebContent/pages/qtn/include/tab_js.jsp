<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/start/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"> -->

<link rel="stylesheet" href="${context}/js/libs/bootstrap.min.css">
<link rel="stylesheet" href="${context}/js/libs/jquery-ui.css">
<script src="${context}/js/libs/jquery.min.js"></script>
<script src="${context}/js/libs/popper.min.js"></script>
<script src="${context}/js/libs/bootstrap.min.js"></script>
<script src="${context}/js/libs/jquery-ui.min.js"></script>
<link rel="stylesheet" href="${context}/js/libs/fontawesome/css/all.css">
