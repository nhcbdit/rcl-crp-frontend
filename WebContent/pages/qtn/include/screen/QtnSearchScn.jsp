<%-- --------------------------------------------------------------------------------------------------------
QtnSearchScn.jsp
-------------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2007
-------------------------------------------------------------------------------------------------------------
Author Sarawut Anuckwattana 02/07/2019
- Change Log ------------------------------------------------------------------------------------------------
##  DD/MM/YY       -User-              -TaskRef-            -ShortDescription-
1   04/07/19       Sarawut A.                 				Remove add FileSaver.js
2   11/09/19       nawrat1	                 				Remove usage of userData.coutnry
-------------------------------------------------------------------------------------------------------- --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../../include/tab_js.jsp" %>
	<%@include file="../../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="${context}/css/rcl.css">
	<link rel="stylesheet" type="text/css" href="${context}/css/loader.css">

	<script src="${context}/js/rutUtilities.js"></script>
	<script src="${context}/js/qtnPowerTable.js"></script>
	<script src="${context}/js/RutHelp.js"></script>
	<script src="${context}/js/rutLoader.js"></script>
	<script src="${context}/js/esnUtilities.js"></script>
	<script src="${context}/js/esnAjax.js"></script>
	<script src="${context}/js/FileSaver.min.js"></script>
	<script src="${context}/js/quotation.js"></script>

	<style>
	.ow-anywhere {
   		overflow-wrap: anywhere;
		}

 	.cssSection{
    	padding-right: 0px;
    	}

    .listsearch{
    	margin-bottom: 0px;
    	font-weight: 500;
    	word-wrap: break-word;
    }
	input[disabled].listsearch  {
		cursor: text;
		color: inherit;
		background: transparent;
	}

	</style>

	<title>Quotation</title>


</head>
<body id="qtn001">

	<jsp:include page='../../include/header.jsp'>
	    <jsp:param name="pageHeader" value="Quotation Search"/>
	</jsp:include>

	<%-- jsp:include page='../../include/header.jsp' /--%>


	<form name="frm0">


		<rcl:area id="s0-header" title="Quotation Search" areaMode="search" buttonList="New_Quotation Find Reset" onClickList="create find reset">

		  <div class="row">
		  	<%-- input id="s0-userId" type="text" class="tblField" style="display:none;" value="DEV_TEAM" /--%>

		  	<rcl:text id="s0-quotationNo"
		  			  label="Quotation No"
		  			  classes="div(col-sm-2)"
		  			  check="len(17) upc"
					  lookup="tbl(VRL_QUOTATION)
							rco(QUOTATION_NO)
							rid(s0-quotationNo)
							sco(QUOTATION_NO*min[3],main)
							sva(s0-quotationNo)
							sop(LIKE)"/>

		  	<rcl:text id="s0-pol"
		  			  name="txbPOL"
		  			  label="POL"
		  			  classes="div(col-sm-2)"
		  			  check="len(17) upc"
		  			  lookup="tbl(VRL_PORT)
							rco(CODE)
							rid(s0-pol)"/>

		  	<%-- div class="rcl-standard-input-wrapper">
				<div style="padding-right:20px">
				<input id="s0-pol1" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font searchField " name="txbPOL" maxlength="17" data-ct="bt-text">
				</div>
					<i class="fa fa-search" onclick="rutOpenLookupTable2('VRL_PORT','','s0-service|s0-vessel|s0-voyage|s0-direction','','','','');"></i>
		</div--%>

		  	<%--rcl:text id="s0-contractParty" label="Contract Party"
				classes="div(col-sm-2)" check="len(10) upc"
				lookup="tbl(VRL_CUSTOMER3)
				rco(CUSTOMER_CODE)
				rid(s0-contractParty)
				sco(CUSTOMER_CODE*min[3],main)
				sva(s0-contractParty)
				sop(LIKE)" /--%>
			<div class=" col-sm-2">
				<label for="s0-contractParty">Contract Party</label>
				<div class="rcl-standard-input-wrapper">
				<div style="padding-right:20px">
				<input id="s0-contractParty" type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font searchField " maxlength="10" data-check="upc " data-ct="bt-text">
				</div>
					<i class="fa fa-search" onclick="qtn_OpenLookupTable('VRL_CUSTOMER3','CUSTOMER_CODE','s0-contractParty','CUSTOMER_CODE*min[3],main','s0-contractParty','LIKE','','OR','DESCRIPTION');"></i>
				</div>
			</div>


		  	<rcl:select id="s0-status"
						label="Status"
						classes="div(col-sm-2)"
						selectTable="QuotationStatus"/>

			<%--rcl:select id="s0-dateType"
						label="Date Type"
						classes="div(col-sm-2)"
						selectTable="DateType"/--%>

			<rcl:select id="s0-dateType"
						label="Date Type"
						classes="div(col-sm-2)"
						optionsList="Available_Date Effective_Date Expiry_Date Follow_Up_Date"
						valueList="A E X F"/>

		  	<rcl:date id="s0-date" label="Date" classes="div(col-sm-2)" />

		  </div>
		 <div class="col-sm-2 col-md-2 offset-sm-2 offset-md-0 pl-2">
                <a id="s1-details" style="font-size: 10px;font-weight:bold;color:blue;right:0;"
                   onclick="rutToggle('s1-details_1','s1-details-s1-row');">
                   <span><%-- <i class="fas fa-chevron-down"></i> --%>Advance Search</span>
                </a>
              <%--    <button type="button" class="rcl-standard-button"
				onclick="copyQuotation('1')">Help</button> --%>
          </div>

		  <div id="s1-details-s1-row"  style="display:none">
				  <div class="row">

					  	<rcl:text id="s0-countryCode" label="Country Code" classes="div(col-sm-2)" check="len(17) upc"
					  	lookup="tbl(VRL_COUNTRY)
							rco(CODE)
							rid(s0-countryCode)"/>

					  	<rcl:text id="s0-pod" label="POD" classes="div(col-sm-2)" check="len(17) upc"
					  	lookup="tbl(VRL_PORT)
							rco(CODE)
							rid(s0-pod)"/>

					  	<rcl:text id="s0-fromCountry" label="From Country" classes="div(col-sm-2)" check="len(17) upc"
					  	lookup="tbl(VRL_COUNTRY)
							rco(CODE)
							rid(s0-fromCountry)"/>

					  	<rcl:text id="s0-toCountry" label="To Country" classes="div(col-sm-2)" check="len(17) upc"
					  	lookup="tbl(VRL_COUNTRY)
							rco(CODE)
							rid(s0-toCountry)"/>

					  	<rcl:text id="s0-salePerson" label="Sales Person" classes="div(col-sm-2)" check="len(17) upc"
						lookup="tbl(VRL_QTN_SALESMAN)
							rco(CODE)
							rid(s0-salePerson)"/>

					  	<rcl:text id="s0-service" label="Service" classes="div(col-sm-2) ctr(dtlField)" check="len(4) upc"
					  			  lookup="tbl(VRL_SERVICE)
					  			  		  rco(CODE)
					  			  		  rid(s0-service)"/>
				  </div>
				  <div class="row">
					  	<rcl:text id="s0-findBy" label="Find" classes="div(col-sm-2)" check="upc"/>

						<rcl:select id="s0-findIn"
									label="In"
									classes="div(col-sm-2)"
									selectTable="QtnSearchFindIn" />
				  </div>
		  	</div>

			 <div class="row labelApporver" style="margin-top: 10px;">
			  	<label id="s0-labelApporver" style="padding-left: 15px;">Approval Option</label>
			  					<rcl:select id="s0-formLoc"
								classes="div(col-sm-2)"
								options="onchange='checkApp(this.value)'"/>
			 </div>
		  </rcl:area>
    </form>




    <div class="tblHeader">
    <%-- film adjust --%>
    <rcl:area id="t0-area" title="Quotation List"
		classes="div(container-fluid pl-1 pr-1)" areaMode="table" >

		<div class="row border" style="margin-right: 0px; margin-left: 0px;">

			<div class="col-sm-4 col-md-1 " style="font-weight: bold;">
				<label id="_t0-labelBooking" class="rcl-std">Quotation#</label>
			</div>
			<div class="col-sm-3 col-md-1 pr-0 " style="font-weight: bold;">
				<label class="rcl-std">Ref#</label>
			</div>
			<div class="col-sm-3 col-md-1 pr-0 " style="font-weight: bold;">
				<label id="_t0-status" class="rcl-std">Contract Party Code</label>
			</div>
			<div class="col-sm-6 col-md-2 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Contract Party Name</label>
			</div>
			<div class="col-sm-4 col-0x75  "
				style="font-weight: bold;">
				<label class="rcl-std">Status</label>
			</div>
			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Effective Date</label>
			</div>
			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Expiry Date</label>
			</div>
			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Follow Up Date</label>
			</div>
			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Tel</label>
			</div>
			<div class="col-sm-6 col-0x75 pr-0 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label id="_t0-bookedTEUS" class="rcl-std">Email</label>
			</div>
			<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label id="_t0-stp1" class="rcl-std">Remarks</label>
			</div>
			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std"></label>
			</div>

		</div>


		<div class="tblArea" style="overflow: auto">

		<div id="t0-row" class="tblRow  row pt-1">

			<div class="container-fluid" >

				<div class="row gridRow" >
					<div class="col-sm-4 col-md-1 pr-0">
				  		 <p id="t0-quotationNo" name="quotationNo" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div class="col-sm-3 col-md-1 pr-0">
				  		 <p id="t0-refNo" name="refNo" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div class="col-sm-4 col-md-1 pr-0">
				  		 <p id="t0-contractPartyCode" name="contractPartyCode" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div class="col-sm-6 col-md-2 offset-sm-4 offset-md-0 pr-0">
				  		 <p id="t0-contractPartyName" name="contractPartyName" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div class="col-sm-4 col-0x75 pr-0">
				  		 <p id="t0-quotationStatus" name="quotationStatus" class="listsearch" data-ct="bt-text"></p>
					</div>
					<rcl:date id="t0-effectiveDate" name="effectiveDate" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 pr-0) ctr(date-data listsearch)" options="disabled" />
					<rcl:date id="t0-expiryDate" name="expiryDate" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 pr-0) ctr(date-data listsearch)" options="disabled" />
					<rcl:date id="t0-followUpDate" name="followUpDate" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 pr-0) ctr(date-data listsearch)" options="disabled" />
					<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 pr-0">
				  		 <p id="t0-telNo" name="telNo" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-0">
				  		 <p id="t0-email" name="email" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-0 ">
				  		 <p id="t0-remarks" name="remarks" class="listsearch" data-ct="bt-text"></p>
					</div>

					<div
						class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 p-0 mr-0 ml-1" >
						<a onclick="viewQuotationDetail('#-RowId-#')"><em class="fa fa-edit"></em></a>
	                	<%-- a id="copyQuotation" name="copyQuotation" style="cursor: pointer; display:none;" onclick="copyQuotation('#-RowId-#','onAp')"><em class="fas fa-copy"></em></a--%>
						<a id="t0-copyQuotation" name="copyQuotation" class="listsearch" style="cursor: pointer;" onclick="copyQuotation('#-RowId-#','onAp')"></a>
					</div>

				</div>

				</div>
				</div>


			</div>

		<div id="t0-paging" class="container-fluid pl-0 pr-0"></div>
	</rcl:area>
    <%-- End adjust --%>

     <rcl:area id="t1-area" title="Quotation List" classes="div(container-fluid pl-1 pr-1)" areaMode="table"  >
		<div class="row border" style="margin-right: 0px; margin-left: 0px;">

			<div class="col-sm-4 col-0x75" style="font-weight: bold;">
				<label id="_t1-labelBooking" class="rcl-std">Quotation#</label>
			</div>
			<div class="col-sm-3 col-0x75 pr-0 " style="font-weight: bold;">
				<label class="rcl-std">Ref#</label>
			</div>
			<div class="col-sm-3 col-0x75 pr-0 " style="font-weight: bold;">
				<label class="rcl-std">Supersede/ Interim</label>
			</div>
			<div class="col-sm-3 col-0x75 pr-0 " style="font-weight: bold;">
				<label id="_t1-status" class="rcl-std">Contract Party Code</label>
			</div>
			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Contract Party Name</label>
			</div>
			<div class="col-sm-4 col-0x5 pr-1 pl-0"
				style="font-weight: bold;">
				<label class="rcl-std">Status</label>
			</div>
			<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Effective Date</label>
			</div>
			<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Expiry Date</label>
			</div>
			<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std">Follow Up Date</label>
			</div>
			<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0"
				style="font-weight: bold;">
				<label class="rcl-std">Tel</label>
			</div>
			<div class="col-sm-6 col-md-1 pr-0 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label id="_t1-bookedTEUS" class="rcl-std">Email</label>
			</div>
			<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 pr-1 pl-0"
				style="font-weight: bold;">
				<label id="_t1-stp1" class="rcl-std">Remarks</label>
			</div>

			<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label  class="rcl-std">Qtd.By</label>
			</div>
			<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label  class="rcl-std">Inq.Date Time</label>
			</div>
			<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label  class="rcl-std">Submit By</label>
			</div>
			<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label  class="rcl-std">Submit Date</label>
			</div>

			<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 "
				style="font-weight: bold;">
				<label class="rcl-std"></label>
			</div>


		</div>


		<div class="tblArea" style="overflow: auto">

		<div id="t1-row" class="tblRow  row pt-1">

			<div class="container-fluid" >

				<div class="row gridRow">

					<div class="col-sm-4 col-0x75 pr-1 pl-1 ">
				  		 <p id="t1-quotationNo" name="quotationNo" data-ct="bt-text" class="listsearch"></p>
					</div>

					<div class="col-sm-3 col-0x75 pr-1 pl-0 ">
				  		 <p id="t1-refNo" name="refNo" data-ct="bt-text" class="listsearch"></p>
					</div>

					<div class="col-sm-4 col-0x75 pr-1 pl-0">
				  		 <p id="t1-supersedeInterim" name="supersedeInterim" data-ct="bt-text" class="listsearch"></p>
					</div>

					<div class="col-sm-4 col-0x75 pr-1 pl-0">
				  		 <p id="t1-contractPartyCode" name="contractPartyCode" data-ct="bt-text" class="listsearch"></p>
					</div>

				   <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-contractPartyName" name="contractPartyName" data-ct="bt-text" class="listsearch"></p>
					</div>

					 <div class="col-sm-4 col-0x5 pr-1 pl-0">
				  		 <p id="t1-quotationStatus" name="quotationStatus" data-ct="bt-text" class="listsearch"></p>
					</div>
					<rcl:date id="t1-effectiveDate" name="effectiveDate" classes="div(col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0) ctr(date-data listsearch)" options="disabled" />
					<rcl:date id="t1-expiryDate" name="expiryDate" classes="div(col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0) ctr(date-data listsearch)" options="disabled" />
					<rcl:date id="t1-followUpDate" name="followUpDate" classes="div(col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0) ctr(date-data listsearch)" options="disabled" />
					<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-telNo" name="telNo" data-ct="bt-text" class="listsearch"></p>
					</div>

					<div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-email" name="email" data-ct="bt-text" class="listsearch"></p>
					</div>

					<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-remarks" name="remarks" data-ct="bt-text" class="listsearch"></p>
					</div>

					<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-qtnBy" name="qtnBy" data-ct="bt-text" class="listsearch"></p>
					</div>
					<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-inqDateTime" name="inqDateTime" data-ct="bt-text" class="listsearch"></p>
					</div>
					<div class="col-sm-6 col-0x5 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-submitBy" name="submitBy" data-ct="bt-text" class="listsearch"></p>
					</div>
					<div class="col-sm-6 col-0x75 offset-sm-4 offset-md-0 pr-1 pl-0">
				  		 <p id="t1-submitDate" name="submitDate" data-ct="bt-text" class="listsearch"></p>
					</div>


					<div
						class="col-sm-6 col-0x25 offset-sm-4 offset-md-0 p-0 mr-0 ml-1" >
						<a onclick="viewQuotationDetailt1('#-RowId-#')"><em class="fa fa-edit"></em></a>
	                	<%-- a id="copyQuotation" name="copyQuotation" style="cursor: pointer; display:none;" onclick="copyQuotation('#-RowId-#','Ap')"><em class="fas fa-copy"></em></a--%>
					</div>
				</div>

			</div>
		</div>
	</div>



		<div id="t1-paging" class="container-fluid pl-0 pr-0"></div>
	</rcl:area>



    </div>

<jsp:include page="/pages/misc/RcmgetUrl.jsp"></jsp:include>


    <script type="text/javascript">

	    var userData;
	    var urlReport;
	    var flagAppove = "";

	    $(document).ready(function() {
	    	var referrer = document.referrer;
	    	// referrer = "http://marlin-cs.rclgroup.com/SealinerRCL/servlet/SpLoginServ?servaction=doLogin";
	    	GetUrl(referrer);
	    //	console.log("urlchildExpire :: "+sessionStorage.getItem("urlchildExpire"));
	    	//$().addClass()
	    	$("#t1-areaHeader").hide();
			$("#t1-area").hide();
	    	$('.listsearch').addClass('rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField')


	    	userData = getUserDataFromHeader();

	    	console.log("userData :: "+JSON.stringify(userData));

	    	var hdrObj =
			{
	    			status : ""
		     };
			//alert("oneOFf :"+dgobj.oneOff);
			rptSetSingleAreaValues("s0-header", hdrObj);
			rutSetElementValue("s0-countryCode", userData.country);
			rptTableInit("t0-area");
			rptTableInit("t1-area");
			sessionStorage.removeItem("viewQtnQuotationNo");
			//settingTable();
			//getQuotationList();

			Ischeckfsc();

			//Sittiporn START
			var optionText;

	        $('#s0-btn2').click(function(){
	        	//optionText = $("#s0-status option:selected").text();
	            location.reload(true);
	        });

	        removeEmtryOptions("s0-status");

	        /* $('#s0-formLoc').on('change',function(){
	            console.log("Selected Option Text: "+$( "#s0-status" ).val());
	        });

	        $('#s0-status').on('change', function() {
	        	console.log("Selected Option Text: "+this.value);
	        }); */
		    //END
		    
	        $('.hasDatepicker').change(function(){
	    		validatedateFormate(this.id,this.value);

	    	});

		});

	    function Ischeckfsc(){
	    	var fagappove = false;

	    	var rateBasisId='s0-formLoc';

	    	var rateBasisElement=document.getElementById(rateBasisId);

	    	getResultAjax("WS_QTN_GET_APPROVAL_OPTION", {
	    		P_I_USER_ID : userData.userId,
	    		userData : userData,
	    		//P_I_USER_ID : "KLINPAN1",
	    		P_O_DATA_RESULT  : ""
			}).done(handlegetappover);

	    	function handlegetappover(response){
	    		if(response.resultCode == "200"){

			    		var jsonOption = response.resultContent;
						var str = "";
						if(jsonOption.length > 0){
							for(var i = 0; i < jsonOption.length; i++){
				    			str += '<option value="'+jsonOption[i].code+'">'+jsonOption[i].description+'</option>';
				    		}
					    	//$('#s0-status').val('N');
					    }else{
					    	$('.labelApporver').css("display", "none");
					    	str += '<option value="INQUIRY"></option>';
					    }
			    		rateBasisElement.innerHTML=str;
			    		rateBasisElement.removeAttribute('disabled');
			    	}else if(response.resultCode == '401'){

						window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
						
					}else{
						dialogGeneric("Warning", response.resultMessage, "Ok");
					}
	    	}

	    }

	    function checkApp(isvalue){
	    	if(isvalue != 'INQUIRY'){
	    		if(isvalue == 'EE'){
	    			$('#s0-status').val('C');
	    		}else{
	    			$('#s0-status').val('N');
		    	}
	    		$('#s0-btn0').hide();
	    	}else{
	    		$('#s0-status').val('O');
	    		$('#s0-btn0').show();
	    		
	    	}
	    }

	    function getQuotationList() {
	    	//var obj = { };
			//getDataFormServerToTable("WS_QTN_SAVE", obj, "t0-area");
			//getDataFormServerToTable("WS_QTN_SEARCH", getDataAreaToObject("s0-header"), "t0-area");
			var dataPost = qnt_GetDataAreaToObject("s0-header");
			dataPost.userData = userData;

			if(dataPost.findBy != ""){
				dataPost.findBy = dataPost.findBy.toUpperCase();
			}


			if(dataPost.date != ""){
				dataPost.date = dataPost.date.split('/').reverse().join('');
			}

			var valueApporve = dataPost.formLoc;
			var settableShow = "";

			//valueApporve = 'Rate';
			if(valueApporve != 'INQUIRY'){
				$("#t1-areaHeader").show();
				$("#t1-area").show();
				$("#t0-areaHeader").hide();
				$("#t0-area").hide();
				settableShow = "t1-area";

			}else{
				$("#t0-areaHeader").show();
				$("#t0-area").show();
				$("#t1-areaHeader").hide();
				$("#t1-area").hide();
				settableShow = "t0-area";
			}
			settingTable(settableShow);
			console.log(JSON.stringify(dataPost));
			qtn_getDataFormServerToTable("WS_QTN_SEARCH", dataPost, settableShow, null, null);
		}

		function transformDates(data) {
			data.forEach(function(row) {
				['effectiveDate', 'expiryDate', 'followUpDate'].forEach(function(column) {
					if (row[column] && row[column].indexOf('/') === 2 && row[column].lastIndexOf('/') === 5) {
						row[column] = row[column].split('/').reverse().join('-');
					}
				})
			});
		}

	    function setbuttoncopy(tb,result){
	    	var setresult = [];
	    	/*for (i = 0; i < result.length; i++) {
	    		if(result[i].quotationStatus != 'Need Approval' ){
	    			result[i].copyQuotation = 'T';
	    			//$('#copyQuotation-'+i).css('display','inline');
	    			//$('#copyQuotation-'+i).show();
	    		}
	    	}*/
	    	result.forEach(function(row){
				if(row["quotationStatus"] === 'Need Approval'){
					row.copyQuotation = '';
				}else{
					row.copyQuotation = '📃';
				}
	    	});

	    	//console.log(tb +" >> "+JSON.stringify(shipmentCorridor));



	    }
	    
	    function settingTable(isvalue){
	    		var settings = getTableSettings(isvalue);
	    		settings.paging.pageSize = 15;
	    	}

	    var getstrAter = "";
		function find(){
			getstrAter = "";
			if(rutGetElementValue('s0-status') != 'N' && rutGetElementValue('s0-formLoc') == 'INQUIRY'){

				if(!rutGetElementValue('s0-pol') && !rutGetElementValue('s0-quotationNo') && !rutGetElementValue('s0-contractParty')){
					getstrAter = 'Contract Party Code or POL or QTN# is mandatory';
				}

			}

			if(rutGetElementValue('s0-date') != ""){
				if(rutGetElementValue('s0-dateType') == ""){
					getstrAter = "Please Select Date Type";
				}

			}

			if(rutGetElementValue('s0-dateType') != ""){
				if(rutGetElementValue('s0-date') == ""){
					getstrAter = "Please enter Date";
				}
			}

			if(rutGetElementValue('s0-findBy')){
				if(!rutGetElementValue('s0-findIn')){
					getstrAter = "Please Select One Value from Drop Down Box in the Search Criteria";
				}
			}

			if(rutGetElementValue('s0-findIn')){
				if(!rutGetElementValue('s0-findBy')){
					var getfindIn = $("#s0-findIn option:selected").text();
					getstrAter = "Please enter "+getfindIn;
				}

			}


			if(getstrAter != ""){
				dialogFadeout_N(getstrAter);
			}else{
				getQuotationList();
			}
		}

		function viewQuotationDetail(rowid){
			var quotationNo = rptGetDataFromDisplay("t0-area", rowid).quotationNo;
				flagAppove = $('#s0-formLoc').val();
			sessionStorage.setItem("viewQtnQuotationNo", quotationNo);
			sessionStorage.setItem("viewCorridorSeq", null);
			sessionStorage.setItem("viewRowId", "");
			sessionStorage.setItem("viewQtnAppover", flagAppove);
			window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
		}

		function viewQuotationDetailt1(rowid){
			var quotationNo = rptGetDataFromDisplay("t1-area", rowid).quotationNo;
				flagAppove = $('#s0-formLoc').val();
			sessionStorage.setItem("viewQtnQuotationNo", quotationNo);
			sessionStorage.setItem("viewCorridorSeq", null);
			sessionStorage.setItem("viewRowId", "");
			sessionStorage.setItem("viewQtnAppover", flagAppove);
			window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';

		}



	/* 	 function countryHelp() {
			var url = "http://marlin-cs.rclgroup.com/SealinerRCL/taglib/help/Help.jsp?formname=quoteList&discols=&title=Port&image=rep_inq1.jpg&tbxname=txbPOL";
			var w=window.open(url,"HelpWindow","width=560,height=400,resizable=yes,scrollbars=yes,toolbar=yes,left=0,top=0");
			w.focus();
			//commonHelp('customerCountry','COUNTRY');
		}  */

		 function countryHelp() {
            var intWidth = 600;
            var intHeight = 430;
            var strParam = "&usrPerm=";
            var strRetName = "txbPOL";

            var strUrl = "<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmPortHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frmEor&retName="+strRetName+strParam;
            //openHelpList(strUrl,intWidth,intHeight);

            openHelpList('/QTNWebApp/RrcStandardSrv?service=ui.misc.help.RcmPortHelpSvc&type=GET_GENERAL&pageAction=new&formName=frm0&retName=txbPOL');
        }
		var quotationNo = "";
		function copyQuotation(rowid,type){
			var area = "";

			var title = "Copy Quotation";
			var content = "Copy as one-off quotation?";

			var layout = "resizable:no;help:no;scroll:no;center:yes;status:no;dialogHeight:50;dialogWidth:70"

			if(type == 'onAp'){
				area = "t0-area";
			}else{
				area = "t1-area";
			}
			
			var copyFlag = rptGetDataFromDisplay(area, rowid).copyFlag
			
			if(copyFlag != 'Y'){
				dialogGeneric("Warning", "You have no authorize to copy this Quotation!" , "Ok");
			}else{
				quotationNo = rptGetDataFromDisplay(area, rowid).quotationNo;
				var  effdate = rptGetDataFromDisplay(area, rowid).effectiveDate;
				//sessionStorage.setItem("viewQtnQuotationNo", quotationNo);
				PopupCenter("../popup/commonDialog.jsp?title="+title+"&content="+content+"&effdate="+effdate,'_blank','350','200');
				
				/*var oneoff = window.open("../popup/commonDialog.jsp?title="+title+"&content="+content, "_blank","width=350px,height=150px,left=500,top=100");
				oneoff.focus();*/
			}

		
		}

		var curDate = $.datepicker.formatDate('yymmdd', new Date());

		function copyQuoNocase(ExpDate){
			// console.log("copyQuoNocase : "+ExpDate); 
			var data = {
						"userData" : userData,
						/*"line" : "R"
						,"trade" : "*"
						,"agent" : "***"
						,"fscCode" : "R"
						,"userId" : "DEV_TEAM"*/
						"qtnNo" : quotationNo
						,"effDate" :  curDate
						,"expDate" :  qtn_ConvertDateFormat(ExpDate)
						,"oneoffFlag" : "N"
						,"createInterim" : "N"
				}
			//console.log("data : "+JSON.stringify(data));
			getResultAjax("WS_QTN_COPY_QUOTATION", data).done(handlecopyquotation);

			function handlecopyquotation(response){
				if(response.resultCode == "200"){
					//console.log("response : "+JSON.stringify(response));
					var quotationNo = response.quotationNo;
					sessionStorage.setItem("viewQtnQuotationNo", quotationNo);
					sessionStorage.setItem("viewCorridorSeq", null);
					window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
				}else if(response.resultCode == '401'){

					window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

				}else{
					if(response.resultStatus == 'F'){
						dialogGeneric("Warning", response.resultMessage , "Ok");
					}
				}
			}

			sessionStorage.setItem("oneOffFlag", "N");
		}

		function convertDate(dateString){
			var p = dateString.split(/\D/g)
			return p[0]+p[1]+p[2]
		}

		function gotoPages(oneOffFlag){
			console.log("gotoPages : "+oneOffFlag);
			var data = {
					"userData" : userData,
					/*"line" : "R"
					,"trade" : "*"
					,"agent" : "***"
					,"fscCode" : "R"
					,"userId" : "DEV_TEAM"*/
					"qtnNo" : quotationNo
					,"effDate" :  curDate
					,"expDate" :  null
					,"oneoffFlag" : oneOffFlag
					,"createInterim" : "N"
			}

		getResultAjax("WS_QTN_COPY_QUOTATION", data).done(handlecopyquotation);

		function handlecopyquotation(response){
			if(response.resultCode == "200"){
				var quotationNo = response.quotationNo;
				sessionStorage.setItem("viewQtnQuotationNo", quotationNo);
				sessionStorage.setItem("viewCorridorSeq", null);
				window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
			}else if(response.resultCode == '401'){

				window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');

			}else{
				if(response.resultStatus == 'F'){
					dialogGeneric("Warning", response.resultMessage , "Ok");
				}
			}
		}

		sessionStorage.setItem("oneOffFlag", oneOffFlag);
		}

		function create(){
			window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
			/*url = window.location.protocol+"//"+location.hostname+':'+window.location.port+'<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
			
			var type = '2Tab';
			var rclp = new rutDialogFlow(type , url);
			
			rclp.openPage();*/

		}

    </script>
</body>
<html>
