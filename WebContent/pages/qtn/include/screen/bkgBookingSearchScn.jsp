<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../../include/tab_js.jsp" %>
	<%@include file="../../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/loader.css">

	<script src="../../../../js/rutUtilities.js"></script>
	<script src="../../../../js/qtnPowerTable.js"></script>
	<script src="../../../../js/RutHelp.js"></script>

	<script src="../../../../js/rutLoader.js"></script>

	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/esnAjax.js"></script>

	<title>Booking</title>

	<style>

	</style>

</head>
<body id="bkgBookingSearchScn">

	<jsp:include page='../../include/header.jsp'>
	    <jsp:param name="pageHeader" value="Booking Search"/>
	</jsp:include>

	<form name="frm0">

	<rcl:searchArea id="s0-header" title="Booking Search">

	  <div class="row">

			<rcl:text id="s0-bookingNo" label="Booking#" classes="div(col-sm-2)" check="len(17)" lookup="tbl-getBooking)rid-s0-bookingNo" />

			<rcl:text id="s0-service" label="Service" classes="div(col-sm-2)" check="len(5)" lookup="tbl-getSVVD)rid-s0-service|s0-vessel|s0-voyage|s0-direction" />

			<rcl:text id="s0-vessel" label="Vessel" classes="div(col-sm-2)" check="len(5)" lookup="tbl-getSVVD)rid-s0-service|s0-vessel|s0-voyage|s0-direction" />

			<rcl:text id="s0-voyage" label="Voyage" classes="div(col-sm-2)" check="len(10)" lookup="tbl-getSVVD)rid-s0-service|s0-vessel|s0-voyage|s0-direction" />

			<rcl:select id="s0-direction"
						label="Direction"
						classes="div(col-sm-2)"
						selectTable="Direction" />

			<rcl:text id="s0-contractParty" label="Contract Party" classes="div(col-sm-2)" check="len(10)" lookup="tbl-getContractParty)rid-s0-contractParty" />

        </div>

        <div class="row" >

   	    	<rcl:date id="s0-sailingDateFrom" label="Sailing Date From" classes="div(col-sm-2)" />

   	        <rcl:date id="s0-sailingDateTo" label="Sailing Date To" classes="div(col-sm-2)" />

           	<rcl:text id="s0-pol" label="POL" classes="div(col-sm-2)" check="len(5)" lookup="tbl-getPOL)rid-s0-pol" />

       		<rcl:text id="s0-pod" label="POD" classes="div(col-sm-2)" check="len(5)" lookup="tbl-getPOD)rid-s0-pod" />

       		<rcl:select id="s0-status"
						label="Status"
						classes="div(col-sm-2)"
						optionsList="ALL Open Confirmed"
						valueList="ALL O CF" />

        </div>

        <div class="row" >

        	<rcl:text id="s0-find" label="Find" classes="div(col-sm-2)" />

        	<rcl:select id="s0-criteria"
						label="In"
						classes="div(col-sm-2)"
						optionsList="Booking# BookingType"
						valueList="BABKNO DECODE(BOOKING_TYPE,'N','NORMAL','ER','EMPTYREPO.','TB','TEMPLATE','MB','MASTER','SP','SPONSORSHIP','FC','FEEDERCARGO')" />
        </div>

        <hr>

    </rcl:searchArea>

    </form>

    <div id="t0-header" class="tblHeader rcl-standard-widget-header">
        <label>Booking List</label>
    </div>

    <div class="container-fluid border">
        <div class="row">
            <div class="col-sm-4 col-md-1 p-0 " style="font-weight:bold;">
                <label class="rcl-std">Booking#</label>
            </div>
            <div class="col-sm-3 col-md-1 p-0" style="font-weight:bold;">
                <label class="rcl-std">Booking Type</label>
            </div>
            <div class="col-sm-4 col-md-1 p-0" style="font-weight:bold;">
                <label class="rcl-std" >Status</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">Srv.</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">Vess.</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">Voy.</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">Dir.</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">POR</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
               	<label class="rcl-std">POL</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
               	<label class="rcl-std">POD</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">DEL</label>
            </div>
            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0" style="font-weight:bold;">
                <label class="rcl-std">Option</label>
            </div>
        </div>
    </div>

   <div id="t0-area" class="tblArea container-fluid border border-secondary rounded shadow-lg pb-0 ascanList2">
        <div id="t0-row" class="tblRow row border">

	       	<rcl:text id="t0-bookingNo" name="BookingNo" classes="div(col-sm-4 col-md-1 p-0) ctr(tblField)" options="readonly" />

	       	<rcl:text id="t0-bookingType" name="Booking Type" classes="div(col-sm-3 col-md-1 p-0) ctr(tblField)" options="readonly" />

       	  	<rcl:text id="t0-status" name="Status" classes="div(col-sm-4 col-md-1 p-0) ctr(tblField)" options="readonly" />

   	  	 	<rcl:text id="t0-service" name="Service" classes="div(col-sm-6 col-md-1 offset-md-0 offset-sm-4 p-0) ctr(tblField)" options="readonly" />

	  	 	<rcl:text id="t0-vessel" name="Vessel" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0) ctr(tblField)" options="readonly" />

	  	 	<rcl:text id="t0-voyage" name="Voyage" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0) ctr(tblField)" options="readonly" />

		 	<rcl:text id="t0-directory" name="Directory" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0 pl-2) ctr(tblField)" options="readonly" />

        	<rcl:text id="t0-por" name="POR" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0 pl-2) ctr(tblField)" options="readonly" />

        	<rcl:text id="t0-pol" name="POL" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0 pl-2) ctr(tblField)" options="readonly" />

    		<rcl:text id="t0-pod" name="POD" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0 pl-2) ctr(tblField)" options="readonly" />

    		<rcl:text id="t0-del" name="DEL" classes="div(col-sm-6 col-md-1 offset-sm-4 offset-md-0 p-0 pl-2) ctr(tblField)" options="readonly" />

            <div class="col-sm-6 col-md-1 offset-sm-4 offset-md-0 pl-2">
                <a id="t0-details" style="font-size: 12px;font-weight:bold;color:blue;right:0;"
                   onclick="rutToggle('t0-details','t0-details-'+'#-RowId-#');">
                   <span><i class="fas fa-chevron-down"></i></span>
                </a>
                <a onclick="viewBookingDetail('#-RowId-#')"><em class="fa fa-edit"></em></a>
            </div>

            <div id="t0-details-t0-row" class="row" style="display:none">

           		<rcl:text id="t0-bookedBy" label="Booked By" name="BookingNo" classes="div(col-sm-6 col-md-3 offset-sm-1 offset-md-0 pb-0 pl-3 pr-0) ctr(tblField)" options="readonly" />

           		<rcl:text id="t0-bookedDate" label="Booked Date" name="BookedDate" classes="div(col-sm-3 col-md-2 offset-md-0 pb-0 pl-2 pr-0) ctr(tblField)" options="readonly" />

           		<rcl:text id="t0-quotationNo" label="Quotation#" name="Quotation" classes="div(col-sm-3 col-md-2 offset-md-0 pb-0 pl-2 pr-0) ctr(tblField)" options="readonly" />

      			<rcl:text id="t0-contractParty" label="Contract Party" name="ContractParty" classes="div(col-sm-3 col-md-2 offset-md-0 pb-0 pl-2 pr-0) ctr(tblField)" options="readonly" />

   				<rcl:text id="t0-pot1" label="POT1" name="POT1" classes="div(col-sm-3 col-md-1 offset-md-0 pb-0 pl-2 pr-0) ctr(tblField)" options="readonly" />

              	<rcl:text id="t0-stp1" label="STP1" name="STP1" classes="div(col-sm-3 col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" options="readonly" />

 				<rcl:text id="t0-soccoc" label="SOCCOC" name="SOCCOC" classes="div(col-sm-3 col-md-1 offset-md-0 pb-0 pl-1 pr-0) ctr(tblField)" options="readonly" />

            </div>
        </div>
    </div>

	<script type="text/javascript">

		$(document).ready(function() {
			rptTableInit("t0-area");
			getBookingList();
		});

		function getBookingList() {
			getDataFormServerToTable("WS_BKG_SEARCH_BOOKING", getDataAreaToObject("s0-header"), "t0-area");
		}

		function find(){
			getBookingList();
		}

		function viewBookingDetail(rowid){
			var bookingNo = rptGetDataFromDisplay("t0-area", rowid).bookingNo;
			sessionStorage.setItem("viewBookingNo", bookingNo);
			window.location = '<%=servURL%>/RrcGenericSrv?service=ui.bkg.entry.BookingMaintenanceSvc';
		}





	 	function getPOL(retIds){
	 		openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmPortHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=' + retIds);
	 	}

	 	function getPOD(retIds){
	 		openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmPortHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=' + retIds);
	 	}

	 	function getSVVD(retIds){
	 		openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmVesselScheduleHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=' + retIds);
	 	}

	 	function getContractParty(retIds){
	 		openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmCustomerHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=' + retIds);
	 	}

	 	function getBooking(retIds){
	 		openHelpList('<%=servURL%>/RrcStandardSrv?service=ui.misc.help.RcmBookingHelpSvc&type=<%=RrcStandardUim.GET_GENERAL%>&pageAction=new&formName=frm0&retName=' + retIds);
	 	}

	</script>

</body>
</html>
