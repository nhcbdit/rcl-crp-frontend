<%@ page language="java" contentType="text/html; charset=UTF-8" session="true" autoFlush ="true" errorPage="/pages/error/RcmErrorPage.jsp"%>
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<%@include file="/pages/misc/RcmInclude.jsp"%>
	<%@include file="../../include/tab_js.jsp" %>
	<%@include file="../../include/datetimepicker_js.jsp" %>

	<link rel="stylesheet" type="text/css" href="../../../../css/rcl.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/rclcustom.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/loader.css">
	<link rel="stylesheet" type="text/css" href="../../../../css/Quotation.css">

	<script src="../../../../js/rutUtilities.js"></script>
	<script src="../../../../js/qtnPowerTable.js"></script>
	<script src="../../../../js/RutHelp.js"></script>

	<script src="../../../../js/rutLoader.js"></script>

	<script src="../../../../js/esnUtilities.js"></script>
	<script src="../../../../js/esnAjax.js"></script>

	<script src="../../../../js/FileSaver.min.js"></script>
	<script src="../../../../js/FileSaver.js"></script>

	<script src="../../../../js/quotation.js"></script>
	<title>Quotation Maintenance</title>
</head>
<body id="qtn002">
	<jsp:include page='../../include/header.jsp'>
		<jsp:param name="pageHeader" value="Quotation Maintenance" />

	</jsp:include>


	<rcl:tabGroup tabList="Maintenance Customer Container/Commodity Routing Charges Det._&_Demure. Comment_&_Print" activeTab="Maintenance">
		<rcl:tabContentPane tabName="Maintenance">
			<jsp:include page='../tab/QtnMaintenanceTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>
	 	<rcl:tabContentPane tabName="Customer">
			<jsp:include page='../tab/QtnCustomerTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>
		<rcl:tabContentPane tabName="Container/Commodity">
			<jsp:include page='../tab/QtnCntrAndCommodityTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>
		<rcl:tabContentPane tabName="Routing">
			<jsp:include page='../tab/QtnRoutingTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>
		<rcl:tabContentPane tabName="Charges">
			<jsp:include page='../tab/QtnChargeTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>
		<rcl:tabContentPane tabName="Det._&_Demure.">
			<jsp:include page='../tab/QtnDetAndDemureTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>
		<rcl:tabContentPane tabName="Comment_&_Print">
			<jsp:include page='../tab/QtnCommentAndPrintTabScn.jsp'></jsp:include>
		</rcl:tabContentPane>




	</rcl:tabGroup>

<script type="text/javascript">
	var isNew = "";
	var routingList;
	var isSelect = '';
	var isSelectCntr = '';
	var isSelectCommty = '';
	var curCorridorSeq = null;
	var OriginalData = "";
	var getqtnNo = "";
	var serverDate = "";
	var serverTimezoneOffset = 8;
	var perCorridor = "" ,curCorridor = "";
	var perContainer = "" ,curContainer = "";
	var userData;
	var IsFsc = false;
	var Isview = false;
	var IsOneOff = false;
	var flagEE = false ,flagRR = false ,flagDND = false ,flagBB = false ,IsStatus = "";
	var flagBBK = false;
	var IsAppover = false;
	var Appoval = "";
	var getAppovValue = "";
	var getisvalue = {};
	var getmode = "";
	var getee_wait_approval = "";
	var viewRowId = "";
	var Issupersede = false;
	var GetReferType = {};
	var GetUrlReport = {};

	$(document).ready(function() {
		userData = getUserDataFromHeader();
		var getshipmentCorridor = "";

		var qtnNo = sessionStorage.getItem("viewQtnQuotationNo");
			Appoval = sessionStorage.getItem("viewQtnAppover");

		console.log("qtnNo :: "+qtnNo)
		console.log("Appoval :: "+Appoval)

		var setvalueHeader = "";
		if(qtnNo != null){
			if(Appoval != null){
				if(Appoval != 'INQUIRY'){
					IsAppover = true;
					setvalueHeader = Appoval+" Appoval";
					if(Appoval == 'EE'){
						flagEE = true;
					}
					else if(Appoval == 'RATE'){
						flagRR = true;
					}
					else if(Appoval == 'BB'){
						flagBB = true;
					}
					else if(Appoval == 'DND'){
						flagDND = true;

					}else if(Appoval == 'BBK'){
						flagBBK = true;


					}

				}else{

					setvalueHeader = "Entry";
				}

				//flagRR = true;

			}else{

				setvalueHeader = "Entry";
			}
		}else{
			setvalueHeader = "Entry";
		}

		setHeaderTitle("Quotation "+setvalueHeader);
		setHeaderOptions(true,"back","save");

		getqtnNo = qtnNo;
		var sesCorridorSeq = sessionStorage.getItem("viewCorridorSeq");
		viewRowId = sessionStorage.getItem("viewRowId");

		var editFrom = "";

		if(Appoval == 'EE'){
			editFrom = 'ee_approval';
		}else{
			editFrom = 'search_normal';
		}

			//curCorridorSeq = sesCorridorSeq;

		/*if(curCorridorSeq == null){
			curCorridorSeq = null;
		}*/
		/*if(sesCorridorSeq != "" && sesCorridorSeq != null  && sesCorridorSeq != "null"){
			curCorridorSeq = sesCorridorSeq;
		}else{
			curCorridorSeq = curCorridorSeq;
		}*/
		Isdisablebutton();

		var oneOffFlag = sessionStorage.getItem("oneOffFlag");

		rptTableInit("t10shipTypeDetailArea", { disabledSortControl: true });

		//alert("qtnNo >>>>>>>>>"+qtnNo);
		getWS_GET_QTN_DETAIL(qtnNo,sesCorridorSeq,editFrom);

		//======get refertype =======//
		getWS_GET_REEFER_TYPE();


	//	initMaintenanceTab();

		//rptTableInit("t10shipTypeDetailArea");
	});


	function getWS_GET_REEFER_TYPE(){
		getResultAjax("WS_GET_REEFER_TYPE",
				{
			userData : userData,
			P_I_USER_ID : userData.userId
				}).done(function(response){
					if(response.resultCode == "200"){
						fnSetrefertype(response.resultContent);
						fnSetUrlReport(response.resultContentUrl.urlReport);
					 }else if(response.resultCode == '401'){

							window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
							
					}else{
						dialogGeneric("Warning", response.resultMessage, "Ok");
					}
				});
	}

	function fnSetUrlReport(Content){
		GetUrlReport = Content;
	}

	function fnSetrefertype(Content){
		GetReferType = Content;
	}

	function Getuser(){
		var data = {
					"userToken": "QTNPCTEST",
					"userId": "DEV_TEAM",
					"line": "R",
					"trade": "*",
					"agent": "***",
					"fscCode": "HPH"
		}

		return data;
	}

	function Isdisablebutton()
	{
		if(getqtnNo != null && getqtnNo != ""){
			 $('#d0-popupApproval').css({"cursor": ""});
			 $('#d0-popupApproval').prop('disabled', false);
			 $('#d0-popupCreateInterim').css({"cursor": ""});
			 $('#d0-popupCreateInterim').prop('disabled', false);
		}else{
			$('#d0-popupApproval').css({"cursor": "not-allowed"});
			$('#d0-popupApproval').prop('disabled', true);
			$('#d0-popupCreateInterim').css({"cursor": "not-allowed"});
			$('#d0-popupCreateInterim').prop('disabled', true);
		}

	}

	function getWS_GET_QTN_DETAIL(qtnNo,setcurCorridorSeq,seteditFrom){
		if(qtnNo != null && qtnNo != "" && qtnNo != "null"){
			if(setcurCorridorSeq == "null"){
				setcurCorridorSeq = null;
			}
			//isNew = "N";
			//alert("call WS >>>>>>>>>");
			showQTNLoading();
			getResultAjax("WS_GET_QTN_DETAIL", {
				userData : userData,
				quotationNo : qtnNo,
				corridorSeqNo : setcurCorridorSeq,
				editFrom : seteditFrom
			}).done(function (response) {
				//check SESSION_EXPIRED
				if(response.resultCode == '200'){
					//var routing = getRoutingList(response.resultContent.MaintenanceTab.shipmentCorridor);
					//var qtnHdr = getObjQuotationHdr(response.resultContent.MaintenanceTab.quotationHeader);
					//console.log("response.resultContent :: "+JSON.stringify(response.resultContent.MaintenanceTab));
					serverDate = getCurrentServerDate();

					//console.log("original data ::"+JSON.stringify(response.resultContent))
					var mainTab = response.resultContent.maintenanceTab;
					IsStatus = mainTab.quotationHeader.status;
					setparam(mainTab.quotationHeader.oneOff,response.resultContent.mode,response.resultContent.ee_wait_approval);
					//--for test---//
					headerSetflag();

					if(!IsFsc || IsAppover){
						Isview = true;

					}
					//--for test---//
					getresponseshipmentCorridor(mainTab.quotationHeader,mainTab.shipmentCorridor);
					fetchHeaderAndRoutingAllTab(mainTab.quotationHeader,mainTab.shipmentCorridor);
					console.log('fetchDataMaintenanceTab...');
					fetchDataMaintenanceTab(mainTab);
					console.log('fetchDataCustomerTab...');
					fetchDataCustomerTab(response.resultContent.customerTab);
					console.log('fetchDataContainerAndCommodityTab...');
					fetchDataContainerAndCommodityTab(response.resultContent.containerAndCommodityTab);
					console.log('fetchDataRoutingTab...');
					fetchDataRoutingTab(response.resultContent.routingTab);
					console.log('fetchDataChargeTab...');
					fetchDataChargeTab(response.resultContent.chargeTab);
					var dataDNDTab = {
							"DNDdata" : response.resultContent.detentionDemurrageTab
							,"DNDdataCheck" : response.resultContent.detentionDemurrageTab_Check
					}
					console.log('fetchDataDetentionDemurrageTab...');
					fetchDataDetentionDemurrageTab(dataDNDTab);
					console.log('fetchDataCommentAndPrintTab...');
					fetchDataCommentAndPrintTab(response.resultContent.commentPrintTab);
					GetOriginalData(response.resultContent);
					isNew = "N";
					if(    (mainTab.quotationHeader.supersedeQuotation ==  null && (mainTab.quotationHeader.interim == 'N') )
						||  mainTab.quotationHeader.oneOff == 'Y'
						|| 	mainTab.quotationHeader.supersedeStatus == 'C' )
					{
						Issupersede = true; // hide
					}

					console.log('validateExtenAndExpireButtion...');
					validateExtenAndExpireButtion();
					console.log('validatebuttionAll_v2...');
					validatebuttionAll_v2();
					console.log("================== Start condition show popupCreateInterim ==================");
					console.log("supersedeQuotation == null	 :: "+mainTab.quotationHeader.supersedeQuotation);
					console.log("&& interim == N :: "+mainTab.quotationHeader.interim);
					console.log("|| oneOff == Y :: "+mainTab.quotationHeader.oneOff);
					console.log("|| supersedeStatus  == C :: "+mainTab.quotationHeader.supersedeStatus);
					console.log("if Issupersede == true.,  Not Show ")
					if(Issupersede){
						console.log("Issupersede = "+Issupersede+ " Not Show ");
							$('#d0-popupCreateInterim').hide();
					}else{
						console.log("Issupersede = "+Issupersede+ " IS Show ");
					}

					console.log("================== End condition show popupCreateInterim ==================");

					console.log("supersedeStatus :: "+mainTab.quotationHeader.supersedeStatus);


					if(mainTab.quotationHeader.supersedeStatus == 'C'){
						$('#d0-extendQuotation').hide();
						$('#d0-expireQuotation').hide();

					}

					// if(!IsFsc || IsAppover){
					// 	IsDisplayview();
					// }
					// edit by nawrat1: check this page only
					if ((!IsFsc || IsAppover) && getqtnNo !== null) {
						ischeckfscMaintenance();
					}


					console.log("IsFsc :: "+IsFsc);
					console.log("IsAppover :: "+IsAppover);
					console.log("IsStatus :: "+IsStatus);
					console.log("Isview :: "+Isview);
					console.log("IsOneOff :: "+IsOneOff);

					console.log('IsAppoverButton...');
					IsAppoverButton();

					document.getElementById("d10oneOff").disabled = true;

					hideQTNLoading();
				}else if(response.resultCode == '401'){s
					window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
				}
				
			});
		}else{
			isNew = "Y";
		//	defaultValueCorridor();
			validatebuttionAll_v2();
			IsCheckOneOff();
			IsSetHeaderNew();
			chekdisplayflagDndAndCharge("",'N');
			isDisplayHeader('N',null);
			//addCoridor();
		}
	}

	function headerSetflag(){
		IsAppover = IsAppover;
		//IsAppover = false;
		//IsFsc = true;
		//IsStatus = "N";
		//flagRR = true;
		/*IsFsc = true;
		IsOneOff = true;*/
	}



	function setparam(oneOff,mode,EE_wait){
		getmode = mode;
		getee_wait_approval = EE_wait;
		if(oneOff == 'Y'){
			IsOneOff = true;
		}else{
			IsOneOff = false;
		}

		if(mode == 'view'){
			IsFsc = false;
		}else{
			IsFsc = true;
		}
	}

	function IsDisplayview(){
		if(getqtnNo != null){ // case edit
			console.log('IsDisplayview...');
			ischeckfscMaintenance();
			ischeckfscCustomer();
			ischeckfscCrtComm();
			ischeckfscRounting();
			ischeckfscChange();
			ischeckfscDND();
			ischeckfscroutingDND()
			ischeckfscComAndPrint();
		}
	}

	function IsAppoverButton(){
	   $('.appover').hide();
	   $('.appovercorridor').hide();
	   $('.appoverCharge').hide();
	   $('.appoverDND').hide();
	   /*flagEE = true;
	   flagBB = true;
	   flagRR = true;
	   flagDND = true;*/

		if(IsStatus == 'N'){

		/*	if(flagBBK){
				$('.appovercorridor').show();
			}*/

			if(flagRR){
				console.log("IsStatus test :: "+IsStatus+ " >> "+flagRR);
				$('.appoverCharge').show();
			}
			if(flagDND){
				$('.appoverDND').show();
			}

		}else{
			if(flagEE){
				$('.appover').show();
			}
		}

	}




	function GetOriginalData(AllData){
		OriginalData = AllData;

	}

	function getresponseshipmentCorridor(quotationHeader,shipmentCorridor){
		getshipmentCorridor = shipmentCorridor;

	}

	function setHeaderOnchangeCorridor(data){
		var NewShipmentCorridor = getshipmentCorridor;
		console.log("NewShipmentCorridor :: " +JSON.stringify(data));
		for(var i=0; i<NewShipmentCorridor.length; i++){
			if(i == isSelect){
				NewShipmentCorridor[i].por = data.por;
				NewShipmentCorridor[i].pol = data.pol;
				NewShipmentCorridor[i].pod = data.pod;
				NewShipmentCorridor[i].pot1 = data.pot1;
				NewShipmentCorridor[i].pot2 = data.pot2;
				NewShipmentCorridor[i].pot3 = data.pot3;
				NewShipmentCorridor[i].del = data.del;
				NewShipmentCorridor[i].porHaul = data.porHaul;
				NewShipmentCorridor[i].delHaul = data.delHaul;

			}
		}

		var headerArea = rptGetDataFromSingleArea("d10MaintenanceHeader");
		fetchNewRoutingTab(headerArea,NewShipmentCorridor,'addpc');
	}

	/* function getRoutingList(corridorList){
		var arrayCorridor = [];
		console.log("corridorList.length:"+corridorList.length);
		if(corridorList != null){
			for(var i = 0 ; i < corridorList.length ; i++){
				arrayCorridor.push({
					porHaulLoc : corridorList[i].porHaulLoc
					,por : corridorList[i].por
					,pol : corridorList[i].pol
					,pot1 : corridorList[i].pot1
					,pot2 : corridorList[i].pot2
					,pot3 : corridorList[i].pot3
					,pod : corridorList[i].pod
					,del : corridorList[i].del
					,delHaulLoc : corridorList[i].delHaulLoc
					,socCoc : corridorList[i].socCoc
				});
			}
			console.log(JSON.stringify(arrayCorridor));
		}
		return arrayCorridor;
	}

	function getObjQuotationHdr(qtnHeader){
		var objQtnHdr;
		objQtnHdr = {
				quotationNo : qtnHeader.quotationNo,
				ref : qtnHeader.ref,
				status : qtnHeader.status,
				slm : qtnHeader.slm,
				qtdBy : qtnHeader.qtdBy,
				pos : qtnHeader.pos
		};
		return objQtnHdr;
	} */
	function btnfinal(){
		dialogGeneric("Warning", '<p>Are you sure to Close and Finalize this Quotation?</p>', "Yes", "No").then(
				function(overwrite){
					if(overwrite){
						if(checkactionAllNull()){
							save('C');
						}else{
							save('O');
						}
					}
				});

	}



	var getoutputAll = "" , getstatus = "";
	var getScreendata = {};
	function save(fg){
		if(flagBBK && (fg == "" || fg == undefined)){
			//saveQtnMaintenanceBBK();

			qtn_Approval();
		}else{
			/*getScreendata = getScreenOutputAll();
			getisvalue = JSON.parse(getScreendata);
			getoutputAll = JSON.parse(getScreendata);
			getstatus = fg;*/
			console.log("setDatasave");
			setDatasave();
			getstatus = fg;

			var error = validateValuequoV2();
			if(!error){
				return;
			}
			console.log("error :: "+error)

		/*	if(fg == 'S' || fg == 'C' || fg == 'O'){
				error = validateDataTab();
				console.log("errorS :: "+error)
				if(!error){
					return;
				}
			}*/

			error = validateEquipment();
				console.log("error :: "+error)
				if(!error){
				//	setdataIscaseError(JSON.parse(getshipmentCorridor));
					return;
				}

			folowexpry_validatechargeTa();


			//
		}
	}

	function setDatasave(){


			getScreendata = getScreenOutputAll();
			//console.log("getScreendata :: "+getScreendata);
			getisvalue = JSON.parse(getScreendata);
			getoutputAll = JSON.parse(getScreendata);

	}

	function iScontinueSave(){

		saveQtnMaintenance();

	}

	function saveQtnMaintenanceBBK() {
		console.log("getScreenBBKOutput :: "+JSON.stringify(getScreenBBKOutput()));
		getResultAjax("WS_QTN_APPROVAL", getScreenBBKOutput).done(handleSaveDataQtn);

		function handleSaveDataQtn(response) {
			console.log(" BBK response:"+JSON.stringify(response));
			if(response.resultCode == "200"){
				console.log("WS_QTN_SAVE IS True.");
				dialogGeneric("Info", response.resultMessage, "OK").then(function(result){
					sessionStorage.setItem("viewQtnQuotationNo", response.quotationNo);
					sessionStorage.setItem("viewCorridorSeq", null);
					sessionStorage.setItem("viewRowId", "");
					window.location = '<%=servURL%>/?service=ui.qtn.QtnMaintenanceSvc';
				});

			}else if(response.resultCode == '401'){

				window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
				
			}else{
				dialogGeneric("Warning", response.resultMessage, "Ok");
			}
		}

	}

	function setDeletefiled(data){
	/*	var getdata = data;
		var charge = getdata.chargeTab;
		for(var i = 0; i < charge.length; i++) {
			delete charge[i].ISadd;
		}

		getdata.chargeTab = charge;

		return getdata;*/
	}

	function MnfnSortDataBeforeInsetapp(IsData){
		var NewSetData = IsData;

		console.log("befor : "+JSON.stringify(NewSetData.chargeTab));
		//set sort charge
		NewSetData.chargeTab = fnSortDataBeforeInset(NewSetData.chargeTab,"action");
		console.log("befor : "+JSON.stringify(NewSetData));
		return NewSetData;
	}

	function MnfnSortDataBeforeInset(IsData){
		var NewSetData = IsData;

		//set sort container
		NewSetData.containerAndCommodityTab.container = fnSortDataBeforeInset(NewSetData.containerAndCommodityTab.container,"action");

		//set sort charge
		NewSetData.chargeTab = fnSortDataBeforeInset(NewSetData.chargeTab,"action");

		return NewSetData;
	}

	function fnSortDataBeforeInset(IsData,sortBy){
		 var Newdata = [];
		 	 Newdata = IsData;
		 	 if(!isEmpty(Newdata)){
		 		Newdata.sort(function(a, b) {
		 		 	if(sortBy == "action"){
		 		 		var x = a.action, y = b.action;
		 		 	}
		 		 	return x < y ? -1 : x > y ? 1 : 0;
		 	 	});
		 	 }

		 		return Newdata;
	}

	function saveQtnMaintenance() {
		var outputScreenSave = {};

		//outputScreenSave = MnfnSortDataBeforeInset(getisvalue);
		outputScreenSave = getisvalue;
		if(getstatus != "" && getstatus != undefined && (getstatus != 'Adddcor'  && getstatus != 'preview')){
			outputScreenSave.maintenanceTab.quotationHeader.status = getstatus;
		}

		outputScreenSave = IsLoopCheckfield(outputScreenSave);

    //	console.log("saveQtnMaintenance :: "+JSON.stringify(outputScreenSave));
    	if(getstatus != "" && getstatus != undefined && (getstatus != 'Adddcor' && getstatus != 'preview')){
    		getResultAjax("WS_QTN_SAVE", outputScreenSave).done(handleSaveUnhold);
    	}else{
    		if(checkactionAllNull()){
        		console.log("action All Null");
        		dialogGeneric("Info", "Process Successfully.", "OK").then(function(result){
        			sessionStorage.setItem("viewQtnQuotationNo", getqtnNo);
        			sessionStorage.setItem("viewCorridorSeq", null);
        			sessionStorage.setItem("viewRowId", "");
        			window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
        		});

        	}else{
        		getResultAjax("WS_QTN_SAVE", outputScreenSave).done(handleSaveDataQtn);
        	}
    	}
    	//console.log("checkactionAllNull() :: "+checkactionAllNull());

    	//getResultAjax("WS_QTN_SAVE", getScreenOutput()).done(handleSaveDataQtn);

		function handleSaveDataQtn(response) {
			console.log("getstatus :: "+getstatus);
			console.log("saveQtnMaintenance response:"+JSON.stringify(response));
			if(response.resultCode == "200"){
				/* if(getstatus == 'Adddcor'){
					 addCoridortest();
				 }else{
					 console.log("WS_QTN_SAVE IS True.");
						dialogGeneric("Info", response.resultMessage, "OK").then(function(result){
							sessionStorage.setItem("viewQtnQuotationNo", response.quotationNo);
							sessionStorage.setItem("viewCorridorSeq", null);
							sessionStorage.setItem("viewRowId", "");
							window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
						});
				 }*/
				 console.log("WS_QTN_SAVE IS True.");
				if(getstatus == 'preview'){// Save In Case pust preview button.
					_printPreview();
        		}else{
        			dialogGeneric("Info", response.resultMessage, "OK").then(function(result){
    					sessionStorage.setItem("viewQtnQuotationNo", response.quotationNo);
    					sessionStorage.setItem("viewCorridorSeq", null);
    					sessionStorage.setItem("viewRowId", "");
    					window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
    				});
        		}
				
				
			}else if(response.resultCode == '401'){

				window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
				
			}else{
				dialogGeneric("Warning", response.resultMessage, "Ok");
			}
		}

		function handleSaveUnhold(response) {
			console.log("saveQtnMaintenance Unhold response:"+JSON.stringify(response));
			if(response.resultCode == "200"){
				console.log("WS_QTN_SAVE IS True.");
					dialogGeneric("Info", response.resultMessage, "OK").then(function(result){
						window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnSearchSvc';
					});
			}else if(response.resultCode == '401'){

				window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
				
			}else{
				dialogGeneric("Warning", response.resultMessage, "Ok");
			}
		}
	}

	function IsLoopCheckfield(IsData){
		var maintenanceData = IsData.maintenanceTab.shipmentCorridor;
		for (var i = 0; i < maintenanceData.length; i++) {
					delete maintenanceData[i].shipDateOrg;
			}
		return IsData;
	}


	var eq = "";
	function validateEquipment(){
		setDatasave();
		eq = getoutputAll;
		//console.log("eq :: "+JSON.stringify(eq));
		var error = validatemaintenaecTab();
		if(!error){
			return;
		}

		error = validatecustomerTab();
		if(!error){
			return;
		}

		error = validatectrcommTab();
		if(!error){
			return;
		}

		error = validatechargeTab();
		if(!error){
			return;
		}


		return error;

		/*validatemaintenaecTab();
		validatecustomerTab();
		validatectrcommTab();
		validatechargeTab();*/

		//getScreenOutput();


	}

	function setdataIscaseError(Isdata){
		rptClearDisplay("t10shipTypeDetailArea");
		rptAddDataWithCache("t10shipTypeDetailArea", Isdata);
		rptDisplayTable("t10shipTypeDetailArea");
		setRowView("t10shipTypeDetailArea","t10corridorSeqNo");

		IsSetShipmentType(Isdata);
	}

	function validatemaintenaecTab(){
		var errorMsg = "";
		var result = true;
		var effDate = new Date(convertDatevalidate($("#d10effDate").val()));
		var expDate = new Date(convertDatevalidate($("#d10expDate").val()));
		var follDate = new Date(convertDatevalidate($("#d10follDate").val()));
		var status = rptGetDataFromSingleArea("d10MaintenanceHeader").status;

		var shipmentCorridor = eq.maintenanceTab.shipmentCorridor;

				if((follDate > expDate || follDate.toDateString() === expDate.toDateString())){
					result = false;
					errorMsg = "Follow up Date should be less than Expiry date*Reply date should be less than Expiry date";
				}else if(status  == 'C' && getstatus != 'L'){
					result = false;
					errorMsg = "Inquiry is not applicable for quotation with status Final ";
				}else{
						if(shipmentCorridor.length > 0){
							var x = 1;
							for (i = 0; i < shipmentCorridor.length; i++) {
								splitDatevalidate(shipmentCorridor[i].shipDate);
								if(shipmentCorridor[i].pol == shipmentCorridor[i].pod){
									result = false;
									errorMsg = "POL cannot be same with POD for corridor at row "+x;
									break;
								}

								if((shipmentCorridor[i].pot1 == shipmentCorridor[i].pol
										|| shipmentCorridor[i].pot2 == shipmentCorridor[i].pol
										|| shipmentCorridor[i].pot3 == shipmentCorridor[i].pol)){
									result = false;
									errorMsg = "POL and POT1/POT2/POT3 cannot be the same for corridor at row "+x;
									break;
								}

								if((shipmentCorridor[i].pot1 == shipmentCorridor[i].pod
										|| shipmentCorridor[i].pot2 == shipmentCorridor[i].pod
										|| shipmentCorridor[i].pot3 == shipmentCorridor[i].pod)){
									result = false;
									errorMsg = "POD and POT1/POT2/POT3 cannot be the same for corridor at row "+x;
									break;
								}

								//chekdup pot1/2/3
								var object = {};
								var myArray = [];
								$.each(shipmentCorridor[i], function(k, v) {
									 if(k == 'pot1' || k == 'pot2' || k == 'pot3'){
										if(v != "" && v != null){
											myArray.push(v);
											}
										}
								});

								var checkPot = myArray.filter(function(elem, index, self) {
									return index != self.indexOf(elem);
								});
								if(checkPot != ""){
									result = false;
									errorMsg = "POT1/POT2/POT3 cannot be the same for corridor at row "+x;
									break;
								}

								if(shipmentCorridor[i].inqType != "" && shipmentCorridor[i].inqType != null){
									console.log("shipmentCorridor[i].inqType > "+shipmentCorridor[i].inqType);
									if(shipmentCorridor[i].oogBkkInq == ""){
										result = false;
										errorMsg = "Missing OOG/BBK Inq.# for corridor at row "+x;
										break;
									}else{
										if(shipmentCorridor[i].shipmentType != 'FCL' && shipmentCorridor[i].shipmentType != 'BBK'){
											   result = false;
											   errorMsg = "Inquiry is not applicable for LCL/ROR shipment ";
										   }else{
											   if(shipmentCorridor[i].inqType == "B"){
													if(shipmentCorridor[i].shipmentType == "FCL"){
														result = false;
														errorMsg = "For FCL Shipment, only OOG inquiry is applicable for corridor at row "+x;
														break;
													}
												}

												if(shipmentCorridor[i].inqType == "O"){
													if(shipmentCorridor[i].shipmentType == "BBK"){
														result = false;
														errorMsg = "For BBK Shipment, only BBK inquiry is applicable for corridor at row "+x;
														break;
													}
												}

										   }

									}

								}

								if(shipmentCorridor[i].shipmentType == "UC" && shipmentCorridor[i].socCoc == "SOC"){
									result = false;
									errorMsg = "UC shipment type is not allowed for SOC-Equipment shipment at row "+x;
									break;
								}

								if(shipmentCorridor[i].shipDateOrg != ""){
									var diffDays = parseInt((splitDatevalidate(shipmentCorridor[i].shipDate) - splitDatevalidate(shipmentCorridor[i].shipDateOrg)) / (1000 * 60 * 60 * 24));
									if(diffDays != 0){
									var diffDays2 = parseInt((splitDatevalidate(shipmentCorridor[i].shipDate) - new Date(getCurrentServerDateTH())) / (1000 * 60 * 60 * 24));
										if(diffDays2 < 0){
											result = false;
											errorMsg = "Shipment date should be greater than or equal to system date for corridor at row "+x;
											break;
										}
									}
								}


								if(splitDatevalidate(shipmentCorridor[i].shipDate) < effDate){
									result = false;
									errorMsg = "Shipment date should be greater than effective date for corridor at row "+x;
									break;
								}

								if(splitDatevalidate(shipmentCorridor[i].shipDate) > expDate){
									result = false;
									errorMsg = "Shipment date should be less than expiry date for corridor at row "+x;
									break;
								}
								x++;
							}
					}else{
						result = false;
						errorMsg = "Please add at least one corridor detail";
					}
				}


			if(!result){
				dialogGenericv3("Maintenance","Warning", errorMsg , "Ok");
			}

		return result;
	}

	function validatecustomerTab(){
		var errorMsg = "";
		var result = true;
		var customer = eq.customerTab;
		var y = 1;
		var newArray = [];
		for (i = 0; i < customer.length; i++) {
			if(customer[i].action != 'd'){
				newArray.push(customer[i].function + customer[i].code);
			}

		}
			var checkPot = newArray.filter(function(elem, index, self) {
				return index != self.indexOf(elem);
			});

			if(checkPot.length > 0){
				result = false;
			    errorMsg = "Customer code with same function cannot be duplicated ";
			}
		if(!result){
			dialogGenericv3("Customer","Warning", errorMsg , "Ok");
		}

	return result;
	}

	function validatectrcommTab(){
		var errorMsg = "";
		var result = true;
		var ctr = eq.containerAndCommodityTab.container;
		var comm = eq.containerAndCommodityTab.commodity;
		var setrowid = "t10row-"+isSelect;
		var coridor = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid);
		var x = 1;
		var newArray = [];
		var newArraychkdup = [];
		var newArrayCommchkdup = [];
		var strTab = "ContainerCommodity";
		//chekdup shipment type, size, type & special handling
		if(ctr.length > 0){
			var t = 0;
			var D1 = 0;
			for (z = 0; z < ctr.length; z++) {
				if(ctr[z].action  != 'd'){
					newArraychkdup.push(ctr[z].shipmentType + ctr[z].polStatus + ctr[z].podStatus + ctr[z].size + ctr[z].type + ctr[z].rateType); ///**  check dup  container
				}
				if(ctr[z].action  != 'd'){
					t ++;
					if(ctr[z].rateType == 'D1'){
						D1 ++;
					}

				}
			}
			console.log("t "+t);
			var getchangeAndrounthing = changeAndrounthing(coridor);
			if(t > 0){
				var e = 0;
				var p_Mea = 0;
				var p_Weight = 0;
				var m_Mea = 0;
				var m_Weight = 0;
				var p_RMea = 0;
				var p_RWeight = 0;
				var m_RMea = 0;
				var m_RWeight = 0;
				var p_RMeaAndWeight = 0;
				var p_textAlert = "";
				var p_arrayAlert = [];
				var m_arrayAlert = [];
				var pkgs = 0;
				var commD1 = 0;
				if(comm.length > 0){
					for (r = 0; r < comm.length; r++) {
						if(comm[r].action != 'd'){
							
							newArrayCommchkdup.push(comm[r].commCode + comm[r].commGroup + comm[r].rateType + comm[r].portClass);
							
							if(coridor.shipmentType == 'UC'){
								if(coridor.rateBasis == 'U'){
									if(comm[r].pkgs == ""){
										pkgs ++;
									}else if(comm[r].pkgs <= 0){
										pkgs ++;
									}
								}
								console.log("coridor.pm :: "+coridor.pm + " >> "+coridor.rateBasis)

								if(coridor.pm == 'P'){
									if(coridor.rateBasis == 'M' || coridor.rateBasis == 'R'){
										if(coridor.rateBasis == 'R'){
											if(comm[r].measurement == ""){
												p_RMea ++;
												p_arrayAlert.push('*Commodity imperial measurement is mandatory for commodity at row 1');
											}else if(comm[r].measurement <= 0.0000){
												p_RMea ++;
												p_arrayAlert.push('*Commodity imperial measurement is mandatory for commodity at row 1');
											}

											if(comm[r].weight == ""){
												p_RWeight ++;
												p_arrayAlert.push('*Commodity imperial weight is mandatory for commodity at row 1');
											}else if(comm[r].weight <= 0.00){
												p_RWeight ++;
												p_arrayAlert.push('*Commodity imperial weight is mandatory for commodity at row 1');
											}
										}else{
											if(comm[r].measurement == ""){
												p_Mea ++;
											}else if(comm[r].measurement <= 0.0000){
												p_Mea ++;
											}
										}
									}else if(coridor.rateBasis == 'W' || coridor.rateBasis == 'R'){
										if(coridor.rateBasis == 'R'){
											if(comm[r].measurement == ""){
												p_RMea ++;
												p_arrayAlert.push('*Commodity imperial measurement is mandatory for commodity at row 1');
											}else if(comm[r].measurement <= 0.0000){
												p_RMea ++;
												p_arrayAlert.push('*Commodity imperial measurement is mandatory for commodity at row 1');
											}

											if(comm[r].weight == ""){
												p_RWeight ++;
												p_arrayAlert.push('*Commodity imperial weight is mandatory for commodity at row 1');
											}else if(comm[r].weight <= 0.00){
												p_RWeight ++;
												p_arrayAlert.push('*Commodity imperial weight is mandatory for commodity at row 1');
											}
										}else{
											if(comm[r].weight == ""){
												p_Weight ++;
											}else if(comm[r].weight <= 0.00){
												p_Weight ++;
											}
										}
									}
								}else if(coridor.pm == 'M'){
									if(coridor.rateBasis == 'M' || coridor.rateBasis == 'R'){
										if(coridor.rateBasis == 'R'){
											if(comm[r].measurement == ""){
												m_RMea ++;
												m_arrayAlert.push('*Commodity metric measurement is mandatory for commodity at row 1');
											}else if(comm[r].measurement <= 0.0000){
												m_RMea ++;
												m_arrayAlert.push('*Commodity metric measurement is mandatory for commodity at row 1');
											}
											if(comm[r].weight == ""){
												m_RWeight ++;
												m_arrayAlert.push('*Commodity metric weight is mandatory for commodity at row 1');
											}else if(comm[r].weight <= 0.00){
												m_RWeight ++;
												m_arrayAlert.push('*Commodity metric weight is mandatory for commodity at row 1');
											}
										}else{
											if(comm[r].measurement == ""){
												m_Mea ++;
											}else if(comm[r].measurement <= 0.0000){
												m_Mea ++;
											}
										}

									}else if(coridor.rateBasis == 'W' || coridor.rateBasis == 'R'){
										if(coridor.rateBasis == 'R'){
											if(comm[r].measurement == ""){
												m_RMea ++;
												m_arrayAlert.push('*Commodity metric measurement is mandatory for commodity at row 1');
											}else if(comm[r].measurement <= 0.0000){
												m_RMea ++;
												m_arrayAlert.push('*Commodity metric measurement is mandatory for commodity at row 1');
											}
											if(comm[r].weight == ""){
												m_RWeight ++;
												m_arrayAlert.push('*Commodity metric weight is mandatory for commodity at row 1');
											}else if(comm[r].weight <= 0.00){
												m_RWeight ++;
												m_arrayAlert.push('*Commodity metric weight is mandatory for commodity at row 1');
											}
										}else{
											if(comm[r].weight == ""){
												m_Weight ++;
											}else if(comm[r].weight <= 0.00){
												m_Weight ++;
											}
										}

									}
								}
							}

							e ++;
							if(D1 > 0){
								 if(comm[r].rateType == 'D1'){
									 commD1 ++;
								 }
							}
						}
					}
				}
				console.log("e "+e);
				console.log("m_RWeight "+m_RWeight);
				if(D1 > 0){
					if(commD1 > 0){
						if(e > 0){
							if(getchangeAndrounthing != ''){
								if(getchangeAndrounthing == '1'){
									errorMsg = "*Voyages is mandatory for corridor ";
									strTab = "Routing";
								}else if(getchangeAndrounthing == '2'){
									errorMsg = "*Charges is mandatory for corridor ";
									strTab = "Charges";
								}else if(getchangeAndrounthing == '3'){
									result = false;
									errorMsg = "Commodity detail is mandatory for corridor at row 1";
								}
								result = false;
							}
						}
					}else{
						result = false;
						errorMsg = "DG commodity details are not found in quotation for DG Container at corridor";
					}

					if(e > 0){
						if(pkgs > 0){
							result = false;
							errorMsg = "*Pkgs is mandatory for unit rate basis at row 1";
							strTab = "ContainerCommodity";
						}
						if(p_Mea > 0){
							result = false;
							errorMsg = "*Commodity imperial measurement is mandatory for commodity at row 1";
							strTab = "ContainerCommodity";
						}else if(p_Weight > 0){
							result = false;
							errorMsg = "*Commodity imperial weight is mandatory for commodity at row 1";
							strTab = "ContainerCommodity";
						}else if(p_arrayAlert.length > 0){
							result = false;
							errorMsg = JSON.stringify(p_arrayAlert);
							strTab = "ContainerCommodity";
						}

						if(m_Mea > 0){
							result = false;
							errorMsg = "*Commodity metric measurement is mandatory for commodity at row 1";
							strTab = "ContainerCommodity";
						}else if(m_Weight > 0){
							result = false;
							errorMsg = "*Commodity metric weight is mandatory for commodity at row 1";
							strTab = "ContainerCommodity";
						}else if(m_arrayAlert.length > 0){
							result = false;
							errorMsg = JSON.stringify(m_arrayAlert);
							strTab = "ContainerCommodity";
						}


					}

				}else{

						if(getchangeAndrounthing != ''){
							if(getchangeAndrounthing == '1'){
								errorMsg = "*Voyages is mandatory for corridor ";
								strTab = "Routing";
							}else if(getchangeAndrounthing == '2'){
								errorMsg = "*Charges is mandatory for corridor ";
								strTab = "Charges";
							}else if(getchangeAndrounthing == '3'){
								result = false;
								errorMsg = "Commodity detail is mandatory for corridor at row 1";
							}
							result = false;
						}

						if(e > 0){
							if(pkgs > 0){
								result = false;
								errorMsg = "*Pkgs is mandatory for unit rate basis at row 1";
								strTab = "ContainerCommodity";
							}
							if(p_Mea > 0){
								result = false;
								errorMsg = "*Commodity imperial measurement is mandatory for commodity at row 1";
								strTab = "ContainerCommodity";
							}else if(p_Weight > 0){
								result = false;
								errorMsg = "*Commodity imperial weight is mandatory for commodity at row 1";
								strTab = "ContainerCommodity";
							}else if(p_arrayAlert.length > 0){
								result = false;
								var getstrg = JSON.stringify(p_arrayAlert)
								errorMsg = getstrg.replace(/[.*+?^""|[\]\\]/g, "");
								strTab = "ContainerCommodity";
							}

							if(m_Mea > 0){
								result = false;
								errorMsg = "*Commodity metric measurement is mandatory for commodity at row 1";
								strTab = "ContainerCommodity";
							}else if(m_Weight > 0){
								result = false;
								errorMsg = "*Commodity metric weight is mandatory for commodity at row 1";
								strTab = "ContainerCommodity";
							}else if(m_arrayAlert.length > 0){
								result = false;
								var m_getstrg = JSON.stringify(m_arrayAlert);
								errorMsg = m_getstrg.replace(/[.*+?^""|[\]\\]/g, "");
								strTab = "ContainerCommodity";
							}


						}
				}
			}

		}


		var checkPot = newArraychkdup.filter(function(elem, index, self) {
			return index != self.indexOf(elem);
		});
		if(checkPot.length > 0){
			result = false;
			errorMsg = "Duplicate Shipment Type/Size/Type/Special Handling/POL Status/POD Status combination for container in corridor.";
		}

		var checkPotComm = newArrayCommchkdup.filter(function(elem, index, self) {
			return index != self.indexOf(elem);
		});
		if(checkPotComm.length > 0){
			result = false;
			errorMsg = "Duplicate Shipment CommCode/CommGroup/Ratetype/PortClass combination for commodity in corridor.";
		}
		
		for (i = 0; i < ctr.length; i++) {
			if((ctr[i].shipmentType != "LCL" && ctr[i].shipmentType != "UC")){
				if(ctr[i].type == "" && ctr[i].type == null){
					result = false;
					errorMsg = "Equipment type is mandatory for all shipment types except 'LCL/UC' in container at row 1 of corridor at row "+x;
					break;
				}
				if(ctr[i].size == "" || ctr[i].size == 0){
					result = false;
					errorMsg = "Equipment size is mandatory for all shipment types except 'LCL/UC' in container at row 1 of corridor at row "+x;
					break;
				}
			}

		}
		for (y = 0; y < comm.length; y++) {
			if(comm[y].rateType == "DG"){
				if(comm[y].portClass == "" || comm[y].portClass == null){
					result = false;
					errorMsg = "Port Class is mandatory for commodity at row "+x;
					break;
				}
			}
		}

		if(!result){
			dialogGenericv3(strTab,"Warning", errorMsg , "Ok");
		}

	return result;
	}

	function changeAndrounthing(coridor){
		var routing = eq.routingTab;
		var change = eq.chargeTab;
		var result = "";
		var setresult = true;
		var textresult = "";
		var strTab = "";

		//var setrowid = "t10row-"+isSelect;
		//var coridor = rptGetDataFromDisplay("t10shipTypeDetailArea", setrowid);

		if(routing.length < 1){
			result = "1";
		}else{
			if(change.length < 1){
				result = "2";
			}
		}

		 if(coridor.shipmentType == 'BBK' || coridor.shipmentType == 'ROR' || coridor.shipmentType == 'UC'){
			    var ctr = eq.containerAndCommodityTab.container;
				var comm = eq.containerAndCommodityTab.commodity;
				var e = 0;
				if(ctr.length > 0){
					for(var f = 0; f < ctr.length; f++) {
						if(ctr[f].action != 'd'){
							e ++;
						}
					}
				}
				//console.log("check E "+e)
					if(e > 0){
						if(comm.length > 0){
							var z = 0;
							for(var i = 0; i < comm.length; i++) {
								console.log(comm[i].action);
								if(comm[i].action != 'd'){
									z ++;
								}
							}
							if(z < 1){
								result = "3";
							}

						}else{
							result = "3";
						}
				}
			}

		 console.log(coridor.shipmentType + ">> " +result);

		if(result == '1'){
			textresult = "1";

		}

		if(result == '2'){
			if(coridor.shipmentType != 'BBK'){
					textresult = "2";
			}
		}

		if(result == '3'){
			textresult = "3";
		}

		 console.log("textresult >> " +textresult);

		return textresult;

	}

	function substrcharge(data){
		console.log("substrcharge :: "+data);
		return data.substring[1,data.length -1];
	}

	var setchargeTab = [];
	function validatechargeTab(){
		var errorMsg = "";
		var result = true;
		var charge = eq.chargeTab;
		var x = 1;

		//chekdup change
		var a = charge;
		for(var i = 0; i < a.length; i++) {
			//delete a[i].ISadd;
			delete a[i].action;
			delete a[i].chargeSeqNo;
			delete a[i].rowId;
			for(var j = i; j < a.length; j++) {
				//delete a[j].ISadd;
				delete a[j].action;
				delete a[j].chargeSeqNo;
				delete a[j].rowId;
				if(i != j){
					if((JSON.stringify(a[i]) === JSON.stringify(a[j]))){
						//console.log(i + "filmtest :: "+JSON.stringify(a[i]) +" == "+ j +JSON.stringify(a[j]))
						result = false;
						errorMsg = "Duplicate records for charges at rows "+(j+1);
						break;
					}
				}
			 }
	    }

		for (z = 0; z < charge.length; z++) {
			/*if(charge[z].unit <= 0){
				result = false;
				errorMsg = "Charge unit is invalid at for charges at row "+x;
				break;
			}*/
			if((charge[z].calculateBasis == 'B' || charge[z].calculateBasis == 'T') && $('#t10shipmentType-'+isSelect).val() != 'UC'){
				if(charge[z].equipmentSize == '0'){
					result = false;
					errorMsg = "For Basis Box/Teus, Size cannot be 0 for Charge at row n of corridor at row "+x;
					break;
				}
				if(charge[z].equipmentType == '**'){
					result = false;
					errorMsg = "For Basis Box/Teus, Type cannot be ** for Charge at row n of corridor at row "+x;
					break;
				}
			}

			if(charge[z].calculateBasis == 'X'){
				if(charge[z].equipmentType != 'Normal' && charge[z].emptyOrLadenFlag != 'F'){
					result = false;
					errorMsg = "For Basis is Lumpsum, Rate Type must be Normal and Laden only for Charge at row n of corridor at row "+x;
					break;
				}

			}

			//chekdup ChargeCode1/ ChargeCode2/ ChargeCode3/ ChargeCode4/ ChargeCode5
			var object = {};
			var myArray = [];
			$.each(charge[z], function(k, v) {
				if(k == 'percentCode1' || k == 'percentCode2' || k == 'percentCode3' || k == 'percentCode4' || k == 'percentCode5'){
					if(!Isempty(v)){
							if(v.trim() != ""){
								myArray.push(v);
							}
						}
					}
			});

			if(myArray.length > 0){
				var checkPot = myArray.filter(function(elem, index, self) {
					return index != self.indexOf(elem);
				});

				if(checkPot != ""){
					result = false;
					errorMsg = "Charge Code1, Code2, Code3, Code4 and Code5 should not be duplicated at row "+x;
					break;
				}
			}


			if(charge[z].AUTO_MANUAL == 'Y'){ //case manual
				if(charge[z].freightOrSurchargeCode == ''){
					result = false;
					errorMsg = "Charge code is mandatory for manual charge at row "+x;
					break;
				}
				if(charge[z].rate == ''){
					result = false;
					errorMsg = "Manual Rate cannot be zero for charges at row "+x;
					break;
				}
			}

			if(charge[z].freightOrSurchageFlag == 'F'){
				if(charge[z].rate <= 0){
					result = false;
					errorMsg = "Rate is invalid for Freight Charge at row "+x;
					break;
				}
			}


			//delete charge[z].ISadd;
			x++;
		}

		//console.log("validatechargeTab :: "+ JSON.stringify(charge));
		//setchargeTab = charge;

		if(!result){
			dialogGenericv3("Charges","Warning", errorMsg , "Ok");
		}

	return result;
	}

	function folowexpry_validatechargeTa(){
		var errorMsg = "";
		var result = false;
		var charge = eq.chargeTab;
		var diffdateMan = parseInt((splitDatevalidateV2($('#d10expDate').val()) - splitDatevalidateV2($('#d10effDate').val())) / (1000 * 60 * 60 * 24));
		//check Exp and Eff
		var savediff = [];
		var v = 0;
		if(charge.length > 0){
			for (j = 0; j < charge.length; j++) {
				var diffdatecharge = parseInt((splitDatevalidate(charge[j].expiryDate) - splitDatevalidate(charge[j].effectiveDate)) / (1000 * 60 * 60 * 24));
				if(diffdatecharge < diffdateMan){
					savediff.push(charge[j].freightOrSurchargeCode);
					v++;
				}
			}
			if(v > 0){
				var getjson = JSON.parse(JSON.stringify(checkIfDuplicateExists(savediff)));
				//console.log("getjson :: "+getjson);
				var points = getjson;
				errorMsg = "The following charges will expire before the quotation expiry date: "+getjson.sort(function(a, b){return a-b})+"<br> "+ "Quotation can still be saved, do you want to proceed?";
				dialogGenericv3("Charges","Warning", errorMsg , "Ok", "No").then(
		   				 function(overwrite){
							if(!overwrite){
								return;
							}else{
								iScontinueSave();

							}
						});
			}else{
				iScontinueSave();

			}
		}else{
			iScontinueSave();
		}
		//homes.sort(sort_by('price', true, parseInt));
	}

	function validateDNDTab(){
		var errorMsg = "";
		var result = true;
		var charge = eq.detentionDemurrageTab;

		if ($('#GTA').is(":checked"))
		{
			//console.log("charge.charges_D ::" +JSON.stringify(charge.charges_D));
			if(charge.exportDetentionDetails.currency == ""
				|| charge.exportDemurrageDetails.currency == ""
				|| charge.importDetentionDetails.currency == ""
				|| charge.importDemurrageDetails.currency == ""){

				result = false;
				errorMsg = "Currency in Export/Import Detention/Demurrage is Mandatory.";
			}
			var Charge_D = charge.charges_D;
			for (d = 0; d < Charge_D.length; d++) {

				if(Charge_D[d].slab1Days == ""){
					showmsgcharge(d+1);
					break;
				}
			}

			var Charge_E = charge.charges_E;
			for (e = 0; e < Charge_E.length; e++) {
				if(Charge_E[e].slab1Days == ""){
					showmsgcharge(e+1);
					break;
				}
			}

			var Charge_I = charge.charges_I;
			for (i = 0; i < Charge_I.length; i++) {
				if(Charge_I[i].slab1Days == ""){
					showmsgcharge(i+1);
					break;
				}
			}

			var Charge_M = charge.charges_M;
			for (m = 0; m < Charge_M.length; m++) {
				if(Charge_M[m].slab1Days == ""){
					showmsgcharge(m+1);
					break;
				}
			}
		}

		function showmsgcharge(row){
			result = false;
			errorMsg = "Slab 1 days is mandatory at row "+row;
		}

		if(!result){
			dialogGenericv3("DetDemure","Warning", errorMsg , "Ok");
		}

	return result;
	}

	function showObjectjQuery(obj) {
		  var result = "";
		  $.each(obj, function(k, v) {
		    result += k + " , " + v + "\n";
		  });
		  return result;
		}


	function checkactionAllNull(){
		var getData = {};
		//getData = JSON.parse(getScreendata);
		setDatasave();

		getData = getoutputAll;

		return (actionEmplyObj(getData.maintenanceTab.quotationHeader)
				&& actionEmplyObjList(getData.maintenanceTab.shipmentCorridor)
				&& actionEmplyObjList(getData.customerTab)
				&& actionEmplyObjList(getData.containerAndCommodityTab.container)
				&& actionEmplyObjList(getData.containerAndCommodityTab.commodity)
				&& actionEmplyObjList(getData.routingTab)
				&& actionEmplyObjList(getData.chargeTab)
				&& actionEmplyObj(getData.detentionDemurrageTab.exportDetentionDetails)
				&& actionEmplyObj(getData.detentionDemurrageTab.exportDemurrageDetails)
				&& actionEmplyObj(getData.detentionDemurrageTab.importDetentionDetails)
				&& actionEmplyObj(getData.detentionDemurrageTab.importDemurrageDetails)
				&& actionEmplyObjList(getData.detentionDemurrageTab.additionalFreeDays_D)
				&& actionEmplyObjList(getData.detentionDemurrageTab.additionalFreeDays_E)
				&& actionEmplyObjList(getData.detentionDemurrageTab.additionalFreeDays_I)
				&& actionEmplyObjList(getData.detentionDemurrageTab.additionalFreeDays_M)
				&& actionEmplyObjList(getData.detentionDemurrageTab.charges_D)
				&& actionEmplyObjList(getData.detentionDemurrageTab.charges_E)
				&& actionEmplyObjList(getData.detentionDemurrageTab.charges_I)
				&& actionEmplyObjList(getData.detentionDemurrageTab.charges_M)
				&& actionEmplyObjList(getData.commentPrintTab.printClause)
				&& actionEmplyObjList(getData.commentPrintTab.comment)
				&& checkPrePostPrint());
	}

	function actionEmplyObj(data){
		return (data.action == "");
	}

	function actionEmplyObjList(data){
		var x = 0;
		if(data.length > 0){
			for(var i=0; i<data.length; i++){
				if(data[i].action != ""){
					x = x + 1;
				}
			}

		}else{
			x = 0;
		}
		var result = true;
		if(x > 0){
			result = false;
		}
		return (result);
	}

	function getDifferences(getoldObj, newObj) {
		   var diff = {};
		   var oldObj =  JSON.parse(getoldObj);
		for (var k in oldObj) {
			  if (!(k in newObj)){
		         diff[k] = undefined;  // property gone so explicitly set it undefined

		      }else if (oldObj[k] !== newObj[k]){
		         diff[k] = newObj[k];  // property in both but has changed
		      }
		   }

		  for (k in newObj) {
		      if (!(k in oldObj))
		         diff[k] = newObj[k]; // property is new
		   }

		   return diff;
		}



	/*var getScreenOutputValidate = {};
	var getScreenOutput = {};*/
	function getScreenOutputAll(){
		data = {
				isNew : isNew,
				userData : userData,
				maintenanceTab : getMaintenanceTabOutput(),
				customerTab : getCustomerTabOutput(),
				containerAndCommodityTab : getContainerCommodityTabOutput(),
				routingTab : getRoutingTabOutput(),
				chargeTab : getChargeTabOutput(),
				detentionDemurrageTab : getDetentionDemurrageTabOutput(),
				commentPrintTab : getCommentPrintTabOutput()
		};
		return JSON.stringify(data);

	}

	function getScreenBBKOutput(){
		var data = {};
		var status = '';
		data = {
				userData : userData,
				chargeTab : getChargeTabOutput()
			};
		return data;
	}

	function fetchDataHeaderAndRouting(hdrId,routingId,quotationHdr,corridor,setHilight){
		rptSetSingleAreaValues(hdrId, quotationHdr);
		rptAddData(routingId, corridor);
		rptDisplayTable(routingId);
		if(setHilight){
			setfieldDefult(routingId);
		}
}
	function fetchDataNewRouting(hdrId,routingId,quotationHdr,Getcorridor,setHilight){

		var getcorridor = JSON.stringify(Getcorridor);
		rptSetSingleAreaValues(hdrId, quotationHdr);
		rptClearDisplay(routingId);
		rptAddDataWithCache(routingId, JSON.parse(getcorridor));
		rptDisplayTable(routingId);
		OnchangeRouting("row-"+isSelect);
		//IsSetShipmentType(Getcorridor);
	}

	function fetchDataNewRoutingOnselectPC(IScorridor){
		console.log("fetchDataNewRoutingOnselectPC start ");
		var getIscorridor = JSON.stringify(IScorridor);
		var arrrounting = ["t10shipTypeDetailArea","t22RoutingHeader","t30RoutingHeader","t41RoutingHeader","t51RoutingHeader","t61RoutingHeader","t71RoutingHeader"];
		for (i = 0; i < arrrounting.length; i++) {
			//console.log(arrrounting[i])
			console.log(getIscorridor);
			rptClearDisplay(arrrounting[i]);
			rptAddDataWithCache(arrrounting[i], JSON.parse(getIscorridor));
			rptDisplayTable(arrrounting[i]);
		 }
		 OnchangeRouting("row-"+isSelect);
		 IsSetShipmentType(JSON.parse(getIscorridor));

		// rptGetDataFromDisplay("t10shipTypeDetailArea", rowid);
		 perCorridor = rptGetDataFromDisplayAll("t10shipTypeDetailArea");

		 console.log("fetchDataNewRoutingOnselectPC End ");
	}

	function setfieldDefult(AreaID){

		var setrow = curCorridorSeq;
		if(viewRowId == null){
			viewRowId = 0;
		}
			if(setrow > 0){
				setrow = setrow - 1;
			}

		var settingCorridor = rptGetTableSettings(AreaID);
		currectCorridor = settingCorridor.rowId+"-"+viewRowId;
		$('#'+currectCorridor).toggleClass(" selected ");

	}


	function onselectBG(rowid,classId){
				 $('.'+classId).removeClass('selected');
				 $('#'+rowid).toggleClass(" selected ");

				 var getrowid = rowid.split("-");
				 if(getrowid[0] == 't10row'){
					 isSelect = getrowid[1];
				 }
				 if(getrowid[0] == 't31row'){
					 isSelectCntr  = getrowid[1];
				 }
				 if(getrowid[0] == 't32row'){
					 isSelectCommty  = getrowid[1];
				 }

				 var arrClass = ["t10row","t22row","t30row","t41row","t51row","t61row","t71row"];
					for (i = 0; i < arrClass.length; i++) {
						if(getrowid[0] == arrClass[i]){
							setactiveFac(getrowid[1]);
						}
					}
	}



	function checkRecodeScroll(areaID,scrollID,scrollClass,conAmt) {
		var RoutiongArr = rptGetDataFromDisplayAll(areaID);
		if(RoutiongArr.length > conAmt){
			$('#'+scrollID).addClass(scrollClass);
		}else{
			$('#'+scrollID).removeClass(scrollClass);
		}

	}

	function setRowView(area,field){
		var corridorArr = rptGetDataFromDisplayAll(area);
		var seq = 1;
		for(var i=0; i<corridorArr.length; i++){
			var getrow = corridorArr[i].rowId.split("-");
			rutSetElementValue(field+"-"+getrow[1], seq);
			seq ++;
		}
	}

	function fetchHeaderAndRoutingAllTab(quotationHdr,corridor){
		console.log("1")
		fetchDataHeaderAndRouting("d20QuotationHeader","t22RoutingHeader",quotationHdr,corridor,true);
		fetchDataHeaderAndRouting("d30QuotationHeader","t30RoutingHeader",quotationHdr,corridor,true);
		fetchDataHeaderAndRouting("d40QuotationHeader","t41RoutingHeader",quotationHdr,corridor,true);
		fetchDataHeaderAndRouting("d50QuotationHeader","t51RoutingHeader",quotationHdr,corridor,true);
		fetchDataHeaderAndRouting("d60QuotationHeader","t61RoutingHeader",quotationHdr,corridor,true);
		fetchDataHeaderAndRouting("d70QuotationHeader","t71RoutingHeader",quotationHdr,corridor,true);

		checkRecodeScroll("t22RoutingHeader","routhingScroll_customerTab","routhing-scroll",3);
		checkRecodeScroll("t30RoutingHeader","routhingScroll","routhing-scroll",3);
		checkRecodeScroll("t41RoutingHeader","routhingScroll_routingTab","routhing-scroll",3);
		checkRecodeScroll("t51RoutingHeader","routhingScroll_chargeTab","routhing-scroll",3);

		check_shipmentType(corridor);

	}

	function fetchNewRoutingTab(quotationHdr,corridor,type){
		var setcorridor = {};
		if(type == undefined){

			if(perCorridor == ""){
				console.log("case New perCorridor == null ");
				perCorridor = corridor;
			}

		}

		curCorridor = corridor;

		//console.log("perCorridortt : "+JSON.stringify(perCorridor))
		//console.log("curCorridortt : "+JSON.stringify(curCorridor))

		if(type == "addpc"){ // Newfetch case Get Product

				fetchDataNewRoutingOnselectPC(curCorridor);
				//fetchDataNewRouting("d10QuotationHeader","t10shipTypeDetailArea",quotationHdr,curCorridor,true);
				//console.log("type : "+JSON.stringify(perCorridor))

		}else{
			fetchDataNewRouting("d20QuotationHeader","t22RoutingHeader",quotationHdr,curCorridor,true);
			fetchDataNewRouting("d30QuotationHeader","t30RoutingHeader",quotationHdr,curCorridor,true);
			fetchDataNewRouting("d40QuotationHeader","t41RoutingHeader",quotationHdr,curCorridor,true);
			fetchDataNewRouting("d50QuotationHeader","t51RoutingHeader",quotationHdr,curCorridor,true);
			fetchDataNewRouting("d60QuotationHeader","t61RoutingHeader",quotationHdr,curCorridor,true);
			fetchDataNewRouting("d70QuotationHeader","t71RoutingHeader",quotationHdr,curCorridor,true);

			//checkRecodeScroll("t10shipTypeDetailArea","routhingScroll_customerTab","routhing-scroll",3);
			checkRecodeScroll("t22RoutingHeader","routhingScroll_customerTab","routhing-scroll",3);
			checkRecodeScroll("t30RoutingHeader","routhingScroll","routhing-scroll",3);
			checkRecodeScroll("t41RoutingHeader","routhingScroll_routingTab","routhing-scroll",3);
			checkRecodeScroll("t51RoutingHeader","routhingScroll_chargeTab","routhing-scroll",3);

		}


		check_shipmentType(corridor);
		if(Isview){
			IsDisplayview();
		}

		setDisplayMaintence();
	}


	function isChangeCorridor(){

		var result = false;

		if(perCorridor.length >  0 && curCorridor.length > 0){
			//console.log("perCorridor :: "+JSON.stringify(perCorridor));
			//console.log("curCorridor :: "+JSON.stringify(curCorridor));


			//console.log("curCorridor :: "+convertDateCheckDiff(perCorridor[0].shipDate) + " >> " +convertDateCheckDiff(curCorridor[0].shipDate));
			if(perCorridor[0].por !== curCorridor[0].por
					|| perCorridor[0].pol !== curCorridor[0].pol
					|| perCorridor[0].pot1 !== curCorridor[0].pot1
					|| perCorridor[0].pot2 !== curCorridor[0].pot2
					|| perCorridor[0].pot3 !== curCorridor[0].pot3
					|| perCorridor[0].del !== curCorridor[0].del
					|| perCorridor[0].porHaulLoc !== curCorridor[0].porHaulLoc
					|| perCorridor[0].delHaulLoc !== curCorridor[0].delHaulLoc
					|| perCorridor[0].pickupDepot !== curCorridor[0].pickupDepot
					|| perCorridor[0].dropOffDepot !== curCorridor[0].dropOffDepot
					|| perCorridor[0].porHaul !== curCorridor[0].porHaul
					|| perCorridor[0].delHaul !== curCorridor[0].delHaul
					|| convertDateCheckDiff(perCorridor[0].shipDate) !== convertDateCheckDiff(curCorridor[0].shipDate)
					|| perCorridor[0].term !== curCorridor[0].term)
				{
				result = true;
				}
			}


		return result;

		}

	function isChangeContainer(perContainer,curContainer){
		for(var i = 0; i < curContainer.length; i++){
			return (perContainer[i].size !== curContainer[i].size
					|| perContainer[i].type !== curContainer[i].type
					|| perContainer[i].rateType !== curContainer[i].rateType
				);
		}

	}

	function isChangeCorridorUsingDND(){
		var result = false;

		if(perCorridor.length >  0 && curCorridor.length > 0){
			if(perCorridor[0].socCoc !== curCorridor[0].socCoc
					|| perCorridor[0].por !== curCorridor[0].por
					|| perCorridor[0].pol !== curCorridor[0].pol
					|| perCorridor[0].pot1 !== curCorridor[0].pot1
					|| perCorridor[0].pot2 !== curCorridor[0].pot2
					|| perCorridor[0].pot3 !== curCorridor[0].pot3
					|| perCorridor[0].del !== curCorridor[0].del
					|| perCorridor[0].porHaulLoc !== curCorridor[0].porHaulLoc
					|| perCorridor[0].delHaulLoc !== curCorridor[0].delHaulLoc
					|| perCorridor[0].pickupDepot !== curCorridor[0].pickupDepot
					|| perCorridor[0].dropOffDepot !== curCorridor[0].dropOffDepot
					|| perCorridor[0].porHaul !== curCorridor[0].porHaul
					|| perCorridor[0].delHaul !== curCorridor[0].delHaul
					|| perCorridor[0].shipDate !== curCorridor[0].shipDate
					|| perCorridor[0].term !== curCorridor[0].term)
				{
				result = true;
				}
			}


		return result;
			}


	function SetAction_insert(object){
		if(object.length > 0){
			for(var i = 0; i < object.length; i++){
				object[i].action = 'i';
			}
		}

		return object;
	}

	function SetAction_object_insert(object){
		if(isEmptyObject(object)){
			object.action = '';
		}else{
			object.action = 'i';
		}
		return object;
	}

	function SetAction_edit(object){
		for(var i = 0; i < object.length; i++){
			object[i].action = '';
		}
		return object;
	}

	function SetAction_edit_Object(object){
		var data = {}
		data = object;
		data.action = '';
		return data;
	}

	function getServerDateObject(){
		//Get local date
		var date = new Date();
		//Convert to UTC
		date = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
				date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
		//UTC + serverOffset
		date.setHours(date.getHours() + serverTimezoneOffset);
		return date;
	}

	function getCurrentServerDate(useDBFormat){

		if(useDBFormat === undefined){
			useDBFormat = false;
		}


		var date = getServerDateObject();

		if(useDBFormat){
			return "" + date.getFullYear() + ("0" + (date.getMonth()+1)).slice(-2) +
				("0" + date.getDate()).slice(-2);
		}else{
			return ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth()+1)).slice(-2) + "/" +
				 date.getFullYear();
		}
	}

	function StrtoDate(isdate){
		var dateString = isdate; // Oct 23

		var dateParts = dateString.split("/");

		var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

		return dateObject;
	}

	function splitDatevalidate(getDate){
		var NewDate = "";
		if(getDate != undefined){
			NewDate = getDate.substring(0, 4) +"/"+ getDate.substring(4, 6) +"/"+ getDate.substring(6, 10);
		}
		return new Date(NewDate);
	}

	function splitDatevalidateV2(getDate){
		if(getDate != undefined){
			getDate = getDate.split('/');
			NewDate = getDate[2] +"/"+ getDate[1] +"/"+ getDate[0];
		}
		return new Date(NewDate);
	}

	function validateEmail(id,value) {
		if(value != ""){
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		    if (! testEmail.test(value)){
		    	$("#"+id).val("");
		    	dialogGeneric("Warning", "E-Mail is invlid." , "Ok");
		    }
		}

	}
	//qtn_Approval("",splitDate($('#d92expipryDate').val()),"extend","b6-ExtenQuotationArea",'');
	function qtn_Approval(flag,Isdate,approveAc,CloseDialog,flagtype){

		   var data = {};
		   var Getstatus = "";
		   var Getdate = "";
		   var GetapproveAction = "";

		if((flag != undefined || flag != "") && !flagEE){
			Getstatus = flag
		}else{
			if(flag == 'S'){
				Getstatus = rutGetElementValue('d10status');
			}else{
				Getstatus = flag;
			}

		}

		if(Isdate != undefined || Isdate != ""){
			Getdate = Isdate;
		}else{
			Getdate = splitDate(rutGetElementValue('d10expDate'));
		}

		if(approveAc != undefined || approveAc != ""){
			GetapproveAction = approveAc;
		}

			data = {
				userData : userData,
				approveType : Appoval,
				quotationNo  : getqtnNo,
				corridorSeqNo : parseInt(isSelect) + 1,
				qtnStatus : Getstatus,
				approveAction : approveAc,
				expireDate : Getdate,
				commentPrintTab : getCommentPrintTabOutput(),
				quotationHeader : rptGetDataFromSingleArea("d10MaintenanceHeader")

			};

			if(Appoval == 'BBK'){
				if(flagtype == 'final' || flagtype == 'Decline'){
					data.approveAction = '';
					if(flagtype  == 'final'){
						data.qtnStatus = 'C';
					}
				}else{
					data.approveAction = 'save';
				}
				data.chargeTab = getChargeTabOutput();
			}

			if(flagtype == 'EE'){
				data.approveType = 'EE';
			}

		/*	if(data.chargeTab.length > 0){
				for(var i = 0; i < data.chargeTab.length; i++) {
					delete data.chargeTab[i].ISadd;
				}
			}*/
			var outputScreenSaveapp = {};

			outputScreenSaveapp = MnfnSortDataBeforeInsetapp(data);

			getResultAjax("WS_QTN_APPROVAL", outputScreenSaveapp).done(handleqtnapproval);

		   function handleqtnapproval(response){
			 var getresultMessage = "Process Successfully.";
			   console.log("qtn_Approval "+  Appoval  +" response:"+JSON.stringify(response));
				if(response.resultCode == "200"){
					console.log("WS_QTN_APPROVAL IS True.");
					if(CloseDialog != undefined && CloseDialog != ""){
						rutCloseDialog(CloseDialog);
					}
					if(response.resultMessage != "" && response.resultMessage != null){
						getresultMessage = response.resultMessage;
					}
					dialogGeneric("Info", getresultMessage, "OK").then(function(result){
						sessionStorage.setItem("viewQtnQuotationNo",getqtnNo);
						sessionStorage.setItem("viewCorridorSeq", null);
						if(Appoval != 'BBK'){
							window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnSearchSvc';
						}else{
							window.location = '<%=servURL%>/RrcGenericSrv?service=ui.qtn.QtnMaintenanceSvc';
						}


					});
				}else if(response.resultCode == '401'){

					window.location.replace('<%=sealinerOraPageURL%>/childExpire.jsp');
					
				}else{
					if(CloseDialog != undefined){
						rutCloseDialog(CloseDialog);
					}
					if(response.resultMessage != "" && response.resultMessage != null){
						dialogGeneric("Warning", response.resultMessage, "Ok");
					}

				}
		   }
	   }

	function btnClosed(){
		dialogGeneric("Warning", "Are you sure to Close the Quotation ?" , "Ok", "No").then(
				function(overwrite){
					if(overwrite){
						save('S');
					}else{
						return false;
					}
				});

	}

	function showQTNLoading() {
		if (!$('#qtn_loading').length) {
			$('body').append('<div id="qtn_loading" class="qtn-loading"></div>');
		}
	}

	function hideQTNLoading() {
		$('#qtn_loading').remove();
	}

</script>

</body>
</html>