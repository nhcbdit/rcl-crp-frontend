<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<style>

.highlighted {
	background-color: rgba(64, 145, 215, 0.35) !important;
}
.classreadonly{
	border: 0px;
    background-color: #E8E8E8;
    padding-left: 5px;
}
</style>
<rcl:dialog id="b1-ProductCatalogue" width="1280"
	title="Product Catalogue" buttonList="Add"
	onClickList="addSelectedRouteFromCatalog()">
	<rcl:area id="s2-header" title="Search" collapsible='Y'
		areaMode='search'>
		<div class="row">
			<input id="s2-porHaul" type="text" class="dtlField"
				style="display: none;" /> <input id="s2-delHaul" type="text"
				class="dtlField" style="display: none;" />
			<rcl:text id="s2-por" label="POR" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_POINT)
					rco(CODE)
					rid(s2-por)"
				options="onblur='checkModof(this.id,this.value);' onkeyup='upc();'" />

			<rcl:text id="s2-pol" label="POL" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_PORT)
					rco(CODE)
					rid(s2-pol)"
				options="onkeyup='upc();'" />

			<rcl:text id="s2-pod" label="POD" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_PORT)
					rco(CODE)
					rid(s2-pod)"
				options="onkeyup='upc();'" />

			<rcl:text id="s2-del" label="DEL" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_POINT)
					rco(CODE)
					rid(s2-del)"
				options="onblur='checkModof(this.id,this.value);' onkeyup='upc();'" />

			<%-- New Line --%>
			<%-- <rcl:text id="s2-porHaulage" label="POR Haulage" classes="div(col-sm-3)" check="len(5)" options="onkeyup='upc();'"/> --%>
			<rcl:text id="s2-porHaulage" classes="div(col-sm-3)"
				label="POR Haulage" options="onkeyup='upc();'" check='len(5)'
				lookup="tbl(VRL_HAULAGE_LOCATION)
						  rco(CODE)
						  rid(s2-porHaulage)
						  sco(INLAND_POINT)
						  sva(s2-por)
						  sop(=)" />
						  
			<div class=" col-sm-3">
				<label class="rcl-standard-font " for="s2-modeOfTransportAtPor">Mode of Transport at Por</label>
					<select id="s2-modeOfTransportAtPor" class="rcl-standard-form-control rcl-standard-component searchField " data-ct="bt-select">
						<option value="">None</option>
						<option value="R">Rail</option>
						<option value="T">Truck</option>
					</select>
			</div>
			
			<div class=" col-sm-3">
				<label class="rcl-standard-font " for="s2-modeOfTransportAtDel">Mode of Transport at Del</label>
					<select id="s2-modeOfTransportAtDel" class="rcl-standard-form-control rcl-standard-component searchField " data-ct="bt-select">
						<option value="">None</option>
						<option value="R">Rail</option>
						<option value="T">Truck</option>
					</select>
			</div>		  		  
			
			<rcl:text id="s2-delHaulage" classes="div(col-sm-3)"
				label="DEL Haulage" options="onkeyup='upc();'" check='len(5)'
				lookup="tbl(VRL_HAULAGE_LOCATION)
							rco(CODE)
							rid(s2-delHaulage)
							sco(INLAND_POINT)
							sva(s2-del)
							sop(=)" />
							
		
			
			<%--	<rcl:text id="s2-delHaulage" label="DEL Haulage" classes="div(col-sm-3 offset-sm-6)" check="len(5)" lookup="tbl-getPOL)rid-s2-pol" options="onkeyup='upc();'"/>  --%>
			<%-- New Line --%>
			<rcl:text id="s2-pot1" label="POT1" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_PORT)
					rco(CODE)
					rid(s2-pot1)"
				options="onkeyup='upc();'" />
				
			
			

			<rcl:text id="s2-pot2" label="POT2" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_PORT)
					rco(CODE)
					rid(s2-pot2)"
				options="onkeyup='upc();'" />

			<rcl:text id="s2-pot3" label="POT3" classes="div(col-sm-3)"
				check="len(5)"
				lookup="tbl(VRL_PORT)
					rco(CODE)
					rid(s2-pot2)"
				options="onkeyup='upc();'" />

			<rcl:text id="s2-terminal" label="Term" classes="div(col-sm-3)"
				check="len(5)" 
				lookup="tbl(VRL_SHIPMENT_TERM)
						rco(CODE)
						rid(s2-terminal)"
				options="onkeyup='upc();'" />
			<%-- New Line --%>
			<%-- 			<rcl:select id="s2-directTransshipment" --%>
			<%-- 					label="Direct/Transshipment" --%>
			<%-- 					classes="div(col-md-3) ctr(dtlField)" --%>
			<%-- 					optionsList="Direct" --%>
			<%-- 					valueList="D" /> --%>
			<rcl:select id="s2-directTransshipment" label="Direct/Transshipment"
				classes="div(col-md-3) ctr(dtlField)" selectTable="DirectTransFlag"
				defaultValue="D" />

			<rcl:date id="s2-sailingDate" label="Approx. Sailing Dt."
				classes="div(col-sm-3)" />
			<div class="col-md-3">
				<label>By Terminal</label> <input type="checkbox" id="s2-byTerminal"
					class="dtlField" />
			</div>
		</div>
	</rcl:area>
	<rcl:area id="t42ProductCatalogueHighlightArea"
		classes="div(container-fluid)" title="" collapsible='N'
		areaMode="table" useHeader="Y">
		<div class="row">
			<rcl:text id="t42Vessel" label="Vessel" classes="div(col-sm-3)"
				check="len(5)" lookup="tbl(VRL_RCL_VESSEL)
					rco(CODE)
					rid(t42Vessel)" />
			<rcl:text id="t42Voyage" label="Voyage" classes="div(col-sm-3)"
				lookup="tbl(VRL_VOYAGE)
					rco(VOYAGE)
					rid(t42Voyage)" />
			<div class="col-md-3" style="text-align: center; margin-top: 15px;">
				<button type="button" class="rcl-standard-button"
					onclick="retrieveVesselVoyage()">Retrieve Vess/Voy</button>
			</div>
		</div>
	</rcl:area>
	<div id="t42RoutingDetailsHeader"
		class="tblHeader rcl-standard-widget-header">
		<label>Routing Details</label>
	</div>
	<div class="container-fluid border">
		<div class="row">
			<div class="col-md-6 text-center font-weight-bold">Product
				Catalogue Details</div>
			<div class="col-md-6 text-center font-weight-bold"><p id="DateShowPro"></p></div>
		</div>
		<hr style="margin-top: -5px;" />
		<div class="row">
			<div class="col-md-1 text-center font-weight-bold"
				style="margin-top: -5px;">
				<label class="rcl-std">Sq#</label>
			</div>
			<div class="col-md-3 text-center font-weight-bold"
				style="margin-top: -5px;">
				<label class="rcl-std">From</label>
			</div>
			<div class="col-md-3 text-center font-weight-bold"
				style="margin-top: -5px;">
				<label class="rcl-std">To</label>
			</div>
		</div>
		<hr style="margin-top: -5px;" />
		<div class="row">
			<div class="col-md-1 font-weight-bold offset-md-1"
				style="margin-top: -5px;">
				<label class="rcl-std">Location</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Terminal</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Type</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Location</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Terminal</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Type</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Mode</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Waiting Time</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Transit Time</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Total Time</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Rest.</label>
			</div>
		</div>
		<hr style="margin-top: -5px;" />
		<div class="row">
			<div class="col-md-1 font-weight-bold offset-md-1"
				style="margin-top: -5px;">
				<label class="rcl-std">Ser.</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Vess.</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Voy.</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Dir.</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Carrier/Drayage</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">ETD</label>
			</div>
			<div class="col-md-1 font-weight-bold"
				style="margin-top: -5px; left: 5px; ">
				<label class="rcl-std">ETA</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Multi.Vess.</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Multi.Vend.</label>
			</div>
		</div>
		<hr style="margin-top: -5px;" />
		<!-- div class="row">
			<div class="col-md-1 font-weight-bold offset-md-1"
				style="margin-top: -5px;">
				<label class="rcl-std">ETA</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">ETD</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Multi.Vess.</label>
			</div>
			<div class="col-md-1 font-weight-bold" style="margin-top: -5px;">
				<label class="rcl-std">Multi.Vend.</label>
			</div>
		</div-->
	</div>
	<div id="t42RoutingDetails"
		class="tblArea container-fluid border border-secondary rounded shadow-lg pb-0 ascanList2">

	</div>
</rcl:dialog>

<script type="text/javascript">
	$(document).ready(function() {
		//rptTableInit("s2-header");
		var SetString = "Date: "+ $.datepicker.formatDate('dd/mm/yy', new Date());
		$('#DateShowPro').text(SetString);
		
		
		$( "#s2-directTransshipment" ).change(function() {
			gettransShipment = this.value;
		});
	});

	var productcatList;
	var productcatListhil;
	var productcatPointer = -1;
	var IsParamSearch = {};

	function searchProductCatalog(por, pol, pod, del, porHaul, delHaul, pot1,
			pot2, pot3, term, directTransFlag, saildate) {
		//console.log("searchProductCatalog");
		searchProductCatalogAnyWS("WS_BKG_PC", por, pol, pod, del, porHaul,
				delHaul, pot1, pot2, pot3, term, directTransFlag, true,
				saildate, "", "N");
	}

	function searchProductCatalogAnyWS(ws, por, pol, pod, del, porHaul,
			delHaul, pot1, pot2, pot3, term, directTransFlag, isSailDate,
			cdate, customerId, byTerminal) {
		//Setup data before perform search
		console.log(ws + " , " + por + " , " + pol + " , " + pod + " , " + del
				+ " , " + porHaul + " , " + delHaul + " , " + pot1 + " , "
				+ pot2 + " , " + pot3, term + " , " + directTransFlag + " , "
				+ isSailDate + " , " + cdate + " , " + customerId + " , "
				+ byTerminal);

		var oceanPol = pol;
		var oceanPod = pod;
		var porPol = "";
		if (por && pol) {
			porPol = por;
		} else if (!por) {
			porPol = pol;
		} else if (!pol) {
			porPol = por;
		}
		var podDel = "";
		if (pod && del) {
			podDel = del;
		} else if (!del) {
			podDel = pod;
		} else if (!pod) {
			podDel = del;
		}

		//Setup rate basis
		var rateBasis = "";
		if (porHaul == 'M' && delHaul == 'M') {
			rateBasis = "3";
		}
		if (porHaul == 'M' && delHaul == 'C') {
			rateBasis = "4";
		}
		if (porHaul == 'C' && delHaul == 'M') {
			rateBasis = "1";
		}
		if (porHaul == 'C' && delHaul == 'C') {
			rateBasis = "2";
		}
		/*if(pol && pod){ mm
			rateBasis = "3";
		}
		if(pol && del){mc
			rateBasis = "4";
		}
		if(por && pol){
			rateBasis = "1";
		}
		if(por && del){
			rateBasis = "2";
		}*/
		//Call to webservice
		var productCatalogSearch = {
			//userData: getUserDataFromHeader(),

			userData : setUserData(),
			pol : porPol,
			pod : podDel,
			porHaul : "",
			delHaul : "",
			pot1 : pot1,
			pot2 : pot2,
			pot3 : pot3,
			term : term,
			directTranshipment : directTransFlag,
			rateBasis : rateBasis,
			actPol : oceanPol,
			actPod : oceanPod,
			customerId : customerId,
			byTerminal : byTerminal,
			arrivalSailFlag : (isSailDate ? "S" : "A"),
			saildate : cdate,
		};

		getResultAjax(ws, productCatalogSearch)
				.done(
						function(response) {
							if (response.Success) {
								//console.log("response.Success :: "+response.Content)
								decorateProductCatalogData(response.Content);
								displayProductCatalog(response.Content);
								rutOpenDialog('b1-ProductCatalogue');
								gettransShipment = $('#s2-directTransshipment').val();

							} else {
								//console.log("! response.Success :: "+response.resultCode)
								if (response.Content.search != undefined) {
									if (response.Content
											.search(/^ORA\-\d{1,5}/) >= 0) {
										//response.Content = response.Content.replace(/^(ORA-\d{1,5}\:)(.+)(ORA-\d{1,5}\:.*)*/, "$2").replace(/~t/g, " ").trim();
										response.Content = response.Content
												.replace(/ORA-\d{1,5}\:/g,
														"~~~").split("~~~")[1]
												.replace(/~t/g, " ").trim();
									} else if (response.Content
											.search(/ORA\-\d{0,5}\:/) > -1) {
										//cut response text to ora-xxxxx : *[message] ora-xxxxx : at [line] 
										response.Content = Content = response.Content
												.substring(response.Content
														.search(/ORA\-\d{0,5}\:/));
										//trim response text to ora-xxxxx : *[message]
										response.Content = response.Content
												.substring(
														0,
														response.Content
																.search(/ORA\-\d{0,5}\:\s*\at/));
										//trim ora-xxxxx : response text  
										response.Content = response.Content
												.replace(/ORA\-\d{0,5}\:/, '');

										//remove possible leftover ~t
										response.Content = response.Content
												.replace(/~tNULL~t/i, "")
												.trim();
										response.Content = response.Content
												.replace(/~t/i, "").trim();
									}
									dialogGeneric("Warning", response.Content,
											"Ok");
								} else {
									console.log("response.resultCode :: "
											+ response.resultCode)
									if (response.resultCode == '401') {
										location.href = expiryUrl;
									} else {
										dialogGeneric("Warning",
												response.Content, "Ok");
									}

								}
							}
						});
	}

	function setParamSearch(por, pol, pod, del, porHaul, delHaul, pot1, pot2,
			pot3, term, directTransFlag, isSailDate, cdate, customerId,
			byTerminal, modeOfTransportAtPor, modeOfTransportAtDel) {
		IsParamSearch = {
			por : por,
			pol : pol,
			pod : pod,
			del : del,
			porHaul : porHaul,
			delHaul : delHaul,
			pot1 : pot1,
			pot2 : pot2,
			pot3 : pot3,
			term : term,
			directTranshipment : directTransFlag,
			customerId : customerId,
			byTerminal : byTerminal,
			arrivalSailFlag : (isSailDate ? "S" : "A"),
			saildate : cdate,
			modeOfTransportAtPor : modeOfTransportAtPor,
			modeOfTransportAtDel : modeOfTransportAtDel
		};
	}

	function setUserData() {
		var user = userData;
		var data = {};
		/*var userToken = "";
		if(user.userId == 'DEV_TEAM'){
			userToken = "QTNPCTEST";
		}else if(user.userId == 'L_SIN'){
			userToken = "QTNPCTEST_LSIN";
		}*/

		data = {
			"userToken" : user.userToken,
			"userId" : user.userId,
			"line" : "R",
			"trade" : "*",
			"agent" : "***",
			"fscCode" : "HPH"
		}

		return data;

	}

	function SetfildSearch(paramSearch) {
		//console.log("SetfildSearch :: " + JSON.stringify(paramSearch));
		rutSetElementValue("s2-por",
				(paramSearch.porHaul == 'C') ? paramSearch.por : "");
		rutSetElementValue("s2-pol", paramSearch.pol);
		rutSetElementValue("s2-pod", paramSearch.pod);
		rutSetElementValue("s2-del", paramSearch.del);
		rutSetElementValue("s2-del",
				(paramSearch.delHaul == 'C') ? paramSearch.del : "");

		rutSetElementValue("s2-porHaul", paramSearch.porHaul);
		rutSetElementValue("s2-delHaul", paramSearch.delHaul);

		rutSetElementValue("s2-delHaulage", paramSearch.delHaulage);
		rutSetElementValue("s2-porHaulage", paramSearch.porHaulage);
		rutSetElementValue("s2-pot1", paramSearch.pot1);
		rutSetElementValue("s2-pot2", paramSearch.pot2);
		rutSetElementValue("s2-pot3", paramSearch.pot3);
		rutSetElementValue("s2-terminal", paramSearch.term);
		rutSetElementValue("s2-directTransshipment",paramSearch.directTranshipment);
		rutSetElementValue("s2-sailingDate", setDataDisplay(paramSearch.saildate));
		rutSetElementValue("s2-byTerminal", paramSearch.byTerminal);
		
		rutSetElementValue("s2-modeOfTransportAtPor", paramSearch.modeOfTransportAtPor);
		rutSetElementValue("s2-modeOfTransportAtDel", paramSearch.modeOfTransportAtDel);
		
		rutSetElementValue("t42Vessel", "");
		rutSetElementValue("t42Voyage", "");

		gettransShipment = $('#s2-directTransshipment').val();
	}

	function decorateProductCatalogData(content) {
		for (var i = 0; i < content.length; i++) {
			for (var j = 0; j < content[i].length; j++) {
				content[i][j].rowNo = j + 1;
				//content[i][j].seqNo = content[i][j].voyageSeqNo;
			}
		}
	}

	function displayProductCatalog(response) {
		console.log(JSON.stringify(response));
		productcatList = response;
		productcatListhil = response;
		productcatPointer = -1;
		
		var html = "";
		for (i = 0; i < response.length; i++) {
			var stackWaitingTime = 0;
			var stackTransitTime = 0;
			var stackTotalTime = 0;
			
			if(response[i].hilight){
				
				html += '<div class="tblRow border highlighted">';
			}else{
				html += '<div class="tblRow border">';
			}
			

			for (j = 0; j < response[i].length; j++) {

				var setclass = '';
				if (response[i].length > 1 && j != response[i].length - 1) {

					//setclass = 'border-bottom: 1px solid #dee2e6 !important;';

				}

				
					html += '<div class="row"  style="margin-left: 0px;">';

				switch (j) {
				case 0:
					html += '<div class="col-sm-1 col-md-1 pb-0 pl-1 pr-2" style="text-align: center;">';
					html += '<div class="rcl-standard-input-wrapper pt-1">'
					html += '<input name="SelectChangeRouting" type="radio" class="rcl-standard-form-control" placeholder="" onchange="onSelectChangeRouting('
							+ i + ')">';
					html += '</div>';
					html += '</div>';
					break;
				default:
					html += '<div class=" col-md-1">';
					html += '</div>';
					break;
				}

				//console.log("j :"+ j);
			html += writeProductCatalogueRouteList(response[i][j].fromLoc);

			html += writeProductCatalogueRouteList(response[i][j].fromTerminal);

			html += writeProductCatalogueRouteList(response[i][j].fromLocTp, "InlandLocationType");

			html += writeProductCatalogueRouteList(response[i][j].toLoc);

			html += writeProductCatalogueRouteList(response[i][j].toTerminal);

			html += writeProductCatalogueRouteList(response[i][j].toLocTp, "InlandLocationType");
			
			html += writeProductCatalogueRouteList((response[i][j].transportationMode.toUpperCase()) == 'V' ? 'M' : response[i][j].transportationMode, "ModeOfTransport");

				var waitingTime = parseFloat(response[i][j].waitingTime);
				stackWaitingTime += waitingTime;

				html += writeProductCatalogueRouteList(waitingTime.toFixed(2));

				var transitTime = parseFloat(response[i][j].transitTime);
				stackTransitTime += transitTime;

				html += writeProductCatalogueRouteList(transitTime.toFixed(2));

				var totalTime = waitingTime + transitTime;
				stackTotalTime += totalTime;

				html += writeProductCatalogueRouteList(totalTime.toFixed(2));

				if (j == 0) {
					html += writeButtonPlusFunction();
				} else {
					html += writeDummyCol();
				}

				html += writeDummyCol();

				html += writeProductCatalogueRouteList(response[i][j].service);

				html += writeProductCatalogueRouteList(response[i][j].vessel);

				html += writeProductCatalogueRouteList(response[i][j].voyage);

				html += writeProductCatalogueRouteList(response[i][j].direction, "Direction");

				html += writeProductCatalogueRouteList(response[i][j].transportCarrierDrayage);
				
				html += writeProductCatalogueRouteList(response[i][j].etd);
				
				html += writeProductCatalogueRouteList(response[i][j].eta);

				html += writeButtonPlusFunction();

				html += writeButtonPlusFunction();

			/*	//Dummy Sapce Row 2
				for (k = 0; k <= 6; k++) {
					html += writeDummyCol();
				}

				html += writeProductCatalogueRouteList(response[i][j].eta);

				html += writeProductCatalogueRouteList(response[i][j].etd);

				html += writeButtonPlusFunction();

				html += writeButtonPlusFunction();*/

				//Dummy Sapce Row 3
			/*	for (k = 0; k <= 6; k++) {
					html += writeDummyCol();
				}*/

				html += '</div>';

			}

			//Total By Row At End of Row
			html += '<div class="row"  style="margin-left: 0px;">';
			html += '<div class="col-md-1 offset-md-7 font-weight-bold" style="padding-left: 25px;">';
			html += '<label class="rcl-std">Total</label>';
			html += '</div>';
			html += '<div class="col-md-1 font-weight-bold" style="padding-left: 25px;">';
			html += '<label class="rcl-std">' + stackWaitingTime.toFixed(2)
					+ '</label>';
			html += '</div>';
			html += '<div class="col-md-1 font-weight-bold" style="padding-left: 25px;">';
			html += '<label class="rcl-std">' + stackTransitTime.toFixed(2)
					+ '</label>';
			html += '</div>';
			html += '<div class="col-md-1 font-weight-bold" style="padding-left: 25px;">';
			html += '<label class="rcl-std">' + stackTotalTime.toFixed(2)
					+ '</label>';
			html += '</div>';
			html += '</div>';
			html += '</div>';

			//End Row
			html += '</div>';
		}

		$('#t42RoutingDetails').html(html);

		function writeButtonPlusFunction(funcname, pointer) {
			var output = "";
			output += '<div class="col-md-1" style="text-align: center;">';
			output += '<div style="margin-top: 5px;"><i class="fa fa-plus " aria-hidden="true"'
					+ (funcname ? 'onclick="' + funcname + '(""' + pointer
							+ '")"' : '') + '></i></div>';
			output += '</div>';
			return output;
		}

		function writeDummyCol() {
			return '<div class=" col-md-1"></div>'
		}

		function writeProductCatalogueRouteList(value, mapSelectTable) {
			
			 if(mapSelectTable){
					value = mapValueToSelectTable(value, mapSelectTable);
				}
			 
			 
			var setvalue = (value != null) ? value : "";
			var output = "";
			output += '<div class=" col-md-1 mt-1 mb-1  pl-2 pr-0 ">';
			
			output += '<input type="text" class="rcl-standard-form-control rcl-standard-textbox rcl-standard-font tblField classreadonly" value="' + setvalue + '" readonly="" data-ct="bt-text">';
			
			output += '</div>';

			return output;
		}
		
		function mapValueToSelectTable(value, mapSelectTable){
			if(value){
				if(mapSelectTable == "Direction"){
					switch(value){
					case 'W':
						value = 'West';
						break;
					case 'N':
						value = 'North';
						break;
					case 'S':
						value = 'South';
						break;
					case 'E':
						value = 'East';
						break;
					case 'NW':
						value = 'North West';
						break;
					case 'NE':
						value = 'North East';
						break;
					case 'SW':
						value = 'South West';
						break;
					case 'SE':
						value = 'South East';
						break;
					default:
						break;
					}
				}
				else if(mapSelectTable == "InlandLocationType"){
					switch(value){
					case 'O':
						value = 'Port';
						break;
					case 'Y':
						value = 'Depot';
						break;
					case 'H':
						value = 'Haul.Loc';
						break;
					case 'P':
						value = 'Terminal';
						break;
					case 'D':
						value = 'Door';
						break;
					}
				}
				else if(mapSelectTable == "ModeOfTransport"){
					switch(value){
					case 'B':
						value = 'Barge';
						break;
					case 'F':
						value = 'Feeder';
						break;
					case 'M':
						value = 'Veseel';
						break;
					case 'R':
						value = 'Rail';
						break;
					case 'T':
						value = 'Truck';
						break;
					default:
						break;
					}
				}
			}
			
			return value;
		}
	}

	function find() {
		//setParamfindAction_old();

		setParamfindAction_New();

	}
	
	var modeOfTransportAtPor = "";
	var modeOfTransportAtDel = "";	
	
	function setParamfindAction_New() {
		var dataHeader = getDataAreaToObject("s2-header");
		var por = rutGetElementValue("s2-por");
		var pol = rutGetElementValue("s2-pol");
		var pod = rutGetElementValue("s2-pod");
		var del = rutGetElementValue("s2-del");
		var modeOfTransportAtPor = rutGetElementValue("s2-modeOfTransportAtPor");
		var modeOfTransportAtDel = rutGetElementValue("s2-modeOfTransportAtDel");
		
		if(por != ""){
			if(!modeOfTransportAtPor){
				dialogGeneric("Warning", "Please enter Mode of Transport at POR","Ok");
				return;
			}
		}
		
		if(del  != ""){
			if(!modeOfTransportAtDel){
				dialogGeneric("Warning", "Please enter Mode of Transport at DEL","Ok");
				return;
			}
		}

		if (!pol) {
			dialogGeneric("Warning", "POL is required for Product Catalog",
					"Ok");
			return;
		}
		if (!pod) {
			dialogGeneric("Warning", "POD is required for Product Catalog",
					"Ok");
			return;
		}
		var porHaul = rutGetElementValue("s2-porHaulage");
		var delHaul = rutGetElementValue("s2-delHaulage");
		var pot1 = rutGetElementValue("s2-pot1");
		var pot2 = rutGetElementValue("s2-pot2");
		var pot3 = rutGetElementValue("s2-pot3");
		var terminal = rutGetElementValue("s2-terminal");
		var directTransFlag = rutGetElementValue("s2-directTransshipment");
		
		
		var saildate = convertformatDatereg(rutGetElementValue("s2-sailingDate"));
		var term = "";
		var byTerminal = rutGetElementValue("s2-byTerminal")
		var customerId = rutGetElementValue("d10contactPartyCode");
		//var modeOfTransportAtPor = (IsParamSearch.porHaul == 'C') ? "T" : "";
		//var modeOfTransportAtDel = (IsParamSearch.delHaul == 'C') ? "T" : "";

		var getRoutingHeader = getRouting();
		for (var i = 0; i < getRouting().length; i++) {
			if (i == isSelect) {
				term = getRoutingHeader[i].term;
			}
		}
		
		getResultAjax("WS_GET_DEFAULT_MOT", 
				{
					userData : setUserData(),
					por : por,
					del : del
				}).done(handleSetMot);
		function handleSetMot(response){
			
			if(response.resultStatus == 'S'){
				//	modeOfTransportAtPor = response.resultContent.porMot;
				//	modeOfTransportAtDel = response.resultContent.delMot;
					
					req = setFndoSearch(por, pol, pod, del, porHaul, delHaul, pot1, pot2,
							pot3, term, directTransFlag, convertDateCheckDiff(saildate), customerId, byTerminal,
							term, directTransFlag, modeOfTransportAtPor, modeOfTransportAtDel);

					getResultAjax("WS_NEW_PCL", req).done(function(response) {
						if (response.statusCode == '0') {
							decorateProductCatalogData(response.item.routingList);
							displayProductCatalog_New(response.item.routingList);
							gettransShipment = $('#s2-directTransshipment').val();
							rutOpenDialog('b1-ProductCatalogue');
						}
					});
			}
		}
		
		
		
	}

	function setParamfindAction_old() {
		var dataHeader = getDataAreaToObject("s2-header");
		//console.log("s2-header :: "+JSON.stringify(dataHeader));
		var por = "";
		var pol = rutGetElementValue("s2-pol");
		var pod = rutGetElementValue("s2-pod");
		var del = "";

		if (!pol) {
			dialogGeneric("Warning", "POL is required for Product Catalog",
					"Ok");
			return;
		}
		if (!pod) {
			dialogGeneric("Warning", "POD is required for Product Catalog",
					"Ok");
			return;
		}
		var porHaul = rutGetElementValue("s2-porHaul");
		var delHaul = rutGetElementValue("s2-delHaul");

		var pot1 = rutGetElementValue("s2-pot1");
		var pot2 = rutGetElementValue("s2-pot2");
		var pot3 = rutGetElementValue("s2-pot3");
		var terminal = rutGetElementValue("s2-terminal");
		var directTransFlag = rutGetElementValue("s2-directTransshipment");
		//var saildate = rutGetElementValue("s2-sailingDate");
		// 		var saildate = setdate(rutGetElementValue("s2-sailingDate"));
		var saildate = convertformatDatereg(rutGetElementValue("s2-sailingDate"));
		var term = "";
		var byTerminal = rutGetElementValue("s2-byTerminal")
		var customerId = rutGetElementValue("d10contactPartyCode");

		var getRoutingHeader = getRouting();
		for (var i = 0; i < getRouting().length; i++) {
			if (i == isSelect) {
				term = getRoutingHeader[i].term;
			}
		}

		searchProductCatalogAnyWS("WS_BKG_PC", por, pol, pod, del, porHaul,
				delHaul, pot1, pot2, pot3, term, directTransFlag, true,
				saildate, customerId, byTerminal);

		// 		searchProductCatalog(por, pol, pod, del, porHaul, delHaul, pot1, pot2, pot3,
		// 				term, directTransFlag, saildate);
	}

	function onSelectChangeRouting(index) {
		productcatPointer = index;
	}

	function getSelectedRouteFromCatalog() {
		//Pick data from PC, maybe changed when switching to actual catalog
		return productcatList[productcatPointer];
	}

	function getDateCurrent(useDBFormat) {
		var getvalue = useDBFormat.split("-");
		var day = getvalue[2];
		var month = getvalue[1];
		var year = getvalue[0];
		var fullDate = year + "-" + (month) + "-" + (day);
		return fullDate;
	}
	
	function setDataDisplay(useDBFormat){
		var getvalue = useDBFormat;
		var day = getvalue.substring(6, 8);
		var month = getvalue.substring(4, 6);
		var year = getvalue.substring(0, 4);
		var fullDate = year + "-" + (month) + "-" + (day);
		return fullDate;
	}

	function setdate(value) {
		return value.split('/').reverse().join('-');
	}

	function retrieveVesselVoyage() {
		var paramVessel = ($('#t42Vessel').val() != "" ? $('#t42Vessel').val() : "").trim();
		var paramVoyage = ($('#t42Voyage').val() != "" ? $('#t42Voyage').val() : "").trim();
		
		var Vessel = paramVessel.toUpperCase();
		var Voyage = paramVoyage.toUpperCase();
		

		setDefhili();
		
		var data = '';
		for (i = 0; i < productcatListhil.length; i++) {
			for (j = 0; j < productcatListhil[i].length; j++) {
				if (Vessel != "" && Voyage != "") {
					if (productcatListhil[i][j].vessel == Vessel
							&& productcatListhil[i][j].voyage == Voyage) {
						//productcatListhil[i][j].hilight = true;
						productcatListhil[i].hilight = true

					}
				} else {
					if (productcatListhil[i][j].vessel == Vessel) {
						//productcatListhil[i][j].hilight = true;
						productcatListhil[i].hilight = true
						
					}

					if (productcatListhil[i][j].voyage == Voyage) {
						//productcatListhil[i][j].hilight = true;
						productcatListhil[i].hilight = true
					}
				}

			}
		}
		
		displayProductCatalog(productcatListhil);
		rutOpenDialog('b1-ProductCatalogue');

	}

	function setDefhili() {
		for (k = 0; k < productcatListhil.length; k++) {
			delete productcatListhil[k].hilight;
		}
	}
	
	function checkModof(Isid,Isvalue){
		if(Isvalue != ""){
			var data = {
					userData : setUserData(),
					por : $('#s2-por').val(),
					del : $('#s2-del').val()
			};
			
			getResultAjax("WS_GET_DEFAULT_MOT", data).done(handleSetMotPor);
			
			function handleSetMotPor(response){
				if(response.resultStatus == 'S'){
					if(response.resultContent.porMot != null){
						$('#s2-modeOfTransportAtPor').val(response.resultContent.porMot)
					}
					if(response.resultContent.delMot != null){
						$('#s2-modeOfTransportAtDel').val(response.resultContent.delMot)
					}
				}else{
					
					dialogGeneric("Warning", response.resultMessage, "Ok");
				}
			}
		}else{
			if(Isid.indexOf("por") > -1){
				$('#s2-modeOfTransportAtPor').val("");
			}
			if(Isid.indexOf("del") > -1){
				$('#s2-modeOfTransportAtDel').val("");
			}
		}
		
		
	}
</script>