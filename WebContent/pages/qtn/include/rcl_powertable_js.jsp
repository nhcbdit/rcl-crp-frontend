<script language="javascript">
    var tableSettings="";
    $(document).ready(function(){
        //This activates the popover
        $('[data-toggle="popover"]').popover(); 
    });
</script>
<script language="javascript"> 
    function toggle(sourceElementId, targetElementId) {
        /*sourceElementId: The element which has called this function
          targetElementId: The element which shall be collapsed/expanded*/
        var isVisible = $('#' + targetElementId).is(":visible");
        //$('#'+elementId).toggle('slow'); //simply it is there or away
        $('#' + targetElementId).slideToggle('slow'); //rollup
        //$('#'+elementId).fadeToggle('slow');  //fade away
        if (isVisible === true) {
            $('#'+sourceElementId).text("Expand");
        }
        else {
            $('#'+sourceElementId).text('Collapse');
        }
    }    
</script>
<script>
    //THIS script shall become an own rut utility file
    //See below marked Javadoc (/**) comments on what it does and how to apply
    /*-----------------------------------------------------------------------------------------------------------  
    .....js
    ------------------------------------------------------------------------------------------------------------- 
    Copyright RCL Public Co., Ltd. 2018 
    -------------------------------------------------------------------------------------------------------------
    Author Ascan Heydorn 13/08/2018
    - Change Log ------------------------------------------------------------------------------------------------  
    ## DD/MM/YY -User-     -TaskRef-      -Short Description
    01 13/08/18 aschey1                    Initial PoC
    */

    /**
    This Javascript provides a generic facility for creating lists of records in an html page and fill it with data.
    The tool requires a <div> (or any other suitable tag) of class "tblArea" (Bootstrap also need "container") to contain the whole list.
    It must have an html ID and this id must have a 3-character prefix and all id's within the container shall have same prefix.

    Inside this containers it requires  at some place one <div> (or any other suitable tag) of class "tblRow" 
    (Bootstrap also requires "row" or "form-row") to keep the template of one row of data. The row must also have an html ID with same prefix.
    Within this template row each tag with data must have class "tblField" and an id of this format "<prefix><attributeNameOfDataInClass>".

    After intitializing the tool with the function tableInit one can add the data to it with addData.
    The data would consist of an array of classes. The attribute names within the class must correspond to the IDs of the tblField's (minus prefix). 
    So if there is a tblField with ID "t0-booking" there should be an attribute named "booking".

    The tool supports <input>,<p>,<textarea> and also <select>. For <select> the data value must be in the html attribute value of each option.
    So when data is added it compares the it looks at <option value=CN">China</option> it would select this option only, if the value in
    the data is "CN".

    The tool uses a global variable named rutTableIndex
    The tool will suppress display of the template row and will create copies of it for each row in the data array. Any html ID in the
    template will be replaced in the copy with same ID-<index>, so ID="t0-booking" would become ID="t0-booking-0" ...
    The new rows are inserted before the template row. So all elements of the container other than the template row will remain untouched.

    @author Ascan Heydorn
    */
    var rutTableIndex = []; 
    /** Initializes the settings of a table based on the HTML. The functions scans the children of containerId and looks for 
    tags of the classes tblRow, tblField and their id's. From that it creates a table of columns and related tags.
    @author Ascan Heydorn
    @param containerId The ID attribute of the container in the html, which comprises the table
    @return the settings
    */
    function tableInit(containerId){
        var s=getTableSettings(containerId);
        if (s!==null) {
            alert('tableInit: Settings for table with id "'+containerId+'" are already set');
            return null;
        }
        var rowId=$("#"+containerId).find(".tblRow").prop('id') || false;
        if (!rowId) {
            alert('tableInit: Table container of id "'+containerId+'" does not have a row template of class "tblRow".');
                return null;
        }
        var settings = {
            "containerId" : containerId,
            //fields describing the template of the row header
            "rowId" : rowId, //this is the id of the row template, new rows get indexed ids rowId-0, rowId-1 ...
            "idPrefix": containerId.substr(1,3), //the prefix of the id of each field, after which the column name is show
            "rowOuterHtml" :"", //this is the html of one row                        
            "columns": [],  //name, tag, type, potential values of tag "input", //select, p, td, textarea 
                        // potential values of type: radio, text, checkbox, number, ""=not applicable
                        //fields are name, tag, type, conversionTable = am 2dim string array of name=date/value=display
                        //type=10 input other, 11=input text 12 input numeric 13 input checkbox, 14 input/radio
                        //type=50 SELECT
                        //99 anything other than the above. These will be treated like text
                        //0 undefined, will not be used
            "idList":[], //a list of all ids in a row. these Ids we must replace with indexed Ids before inersting into html
            "data": [], // an array of classes, each class provides data for one row. The field names of the class must match the columns names
        };           
        settings.rowOuterHtml=document.getElementById(rowId).outerHTML;
        //Find column definitions
        var columnName="", tag="",type="";
        var prefixLength=settings.idPrefix.length;
        $("#"+settings.rowId).find(".tblField").each(function(){
            columnName=this.id.substring(prefixLength);
            tag=this.nodeName;
            type=(tag=== "INPUT") ? this.type : "";

            if (tag==="INPUT"){
                if      (this.type==="checkbox") { type=13;}
                else if (this.type==="radio")    { type=14;}
                else if (this.type==="text")     { type=11;} 
                else if (this.type==="number")   { type=11;} //assign to value;
                else if (this.type==="usernamer"){ type=11;} //assign to value;
                else if (this.type==="password") { type=11;} //assign to value;
                else {type=0;}  //undefined, do not assign
            } 
            else if (tag==="SELECT") { type=50;} //compare with values in options
            else if (tag==="TEXTAREA") { type=99;} //assign to text
            else if (tag==="SPAN") { type=99;}
            else if (tag==="P") { type=99;}
            else if (tag==="TD") { type=99;}
            else { type=0;}
            
            settings.columns.push ({ 
                "name": columnName,
                "tag" : tag.toLowerCase(),
                "type": type
                }
            );
        });
        //Create a list of all ids of the row, so that we can replace them with indexed ids, once we create new entries
        var id="";
        settings.idList.push ( settings.rowId); // for easy programming we put the row id on top of the list
        $("#"+rowId).find("*").each(function(){
            if (this.id !=="") {
                settings.idList.push ( this.id );
            }
        });
        //Add settings to tableIndex
        rutTableIndex.push({"containerId": containerId, "settings": settings});
        //Make the template invisible
        $('#'+rowId).hide();

        return settings;
    } //end function tableInit

    /** Finds settings for a table in the rutTableIndex. Requires that tableInit was run before.
    @author Ascan Heydorn
    @parameter containerId The html id of the container comprising the table
    @return the settings or null if nothing found
    */
    function getTableSettings(containerId){
        var settings=null;
        for (var i=0;i<rutTableIndex.length;i++){
            if (rutTableIndex[i].containerId === containerId) {settings=rutTableIndex[i].settings;}
        }
        return settings;
    } //end function getTableSettings
    /** Adds the data to a table. Requires that tableInit was run before.
    @author Ascan Heydorn
    @parameter containerId The html id of the container comprising the table
    @parameter data An array of classes. The match between data and html is through the id of tags of class "tblField". 
    This id must be after the prefix identical to a attribute name of the class.
    If the tag has id t0-containerNumber then the class should have an attribute containerNumber. 
    */
    function addData(containerId, data){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'adData: Settings for table with id "'+containerId+'" are not create. Run initTableSettings first');
            return null;
        }
        settings.data = data;
        return null;
    } //end function addData
    /** Builds html and display it after settings and data are ready. Requires tableInit and addData to be run before.
    @parameter containerId The html id of the container comprising the table
    */
    function displayTable(containerId){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'displayTable: Settings for table with id "'+containerId+'" are not create. Run initTableSettings first');
            return null;
        }
        for (var i=0;i<settings.data.length;i++) {
            createNewTableRow(settings,i);
            moveDataToDisplay(settings,i);
        }
    } //end function displayTable
    //Replaces all id's in the inner html of a row with new index ids
    function createNewTableRow(settings, index){
        var html=settings.rowOuterHtml;
        //add index to all ids
        for (var i=0;i<settings.idList.length;i++){
            html=html.replace( 
                '"'+settings.idList[i]+'"',('"'+settings.idList[i]+"-"+index+'"') );
        }
        //we insert the row befor the template row. By this any additional controls within the container, which shall not repeat are preserved.
        $("#"+settings.rowId).before(html); 

        return html;
    }
    function moveDataToDisplay(settings, index3 ){
        var j=0;
        var fieldValue="";
        var value="";
        $("#"+settings.rowId+"-"+index3).find(".tblField").each(function(){
            //for the time being we believe that the sequence is same as in columns array
            column=settings.columns[j];
            fieldValue=eval("settings.data[index3]."+column.name);
            switch (column.type){
                case 11: $(this).val(fieldValue); break;
                case 13:
                case 14: 
                    if (fieldValue==="Y") { $(this).attr("checked","");}
                    else {$(this).removeAttr("checked");} 
                    break;
                case 50: //<select> here we need to compare the values in the options with the data
                    $(this).find("option").each(function(){
                        if (fieldValue===$(this).val()){
                            $(this).attr("selected","");
                        }
                        else {
                            $(this).removeAttr("selected");
                        }
                    });
                    break;
                case 99: $(this).text(fieldValue); break;
            }  //endswitch            
            j++;
        });
    }
    /**Adds a conversion table to a column in the settings
    @author Ascan Heydorn        
    */
    function addConversionTable(containerId,fieldId,table){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'addConversionTable: Settings for table with id "'+containerId+'" are not create. Run initTableSettings first');
            return null;
        }
        var found=false;
        for (var i=0;i<settings.columns.length;i++){
            if ( (settings.idPrefix+settings.columns[i].name)===fieldId) {
                settings.columns[i].conversionTable=table;
                found=true;
                break;
            }
        }
        if (!found) {
            alert('addConversionTable: fieldId "'+fieldId+'" not found in settings');
        }
        return null;
    } //end function addConversionTable
    /**Removes the display of all records
    @author Ascan Heydorn
    */
    function clearDisplay(containerId){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'addConversionTable: Settings for table with id "'+containerId+'" are not create. Run initTableSettings first');
            return null;
        }
        $('#'+containerId).find('.tblRow').each(function(){
            if (this.getAttribute('id')!==settings.rowId){
                $(this).remove();
            }
        });
        return null;
    } //end function clearDisplay
    function removeRow(idType,containerId, id){
        var settings=getTableSettings(containerId) || null;
        if (!settings) {
            alert( 'removeRow: Settings for table with id "'+containerId+'" are not create. Run initTableSettings first');
            return null;
        }
        console.log("removeRow");
        idType="index";
        index=-1;
        $("#"+containerId).find(".tblRow").each(function(){
            if (this.getAttribute('id')!==settings.rowId){
                console.log("removeRow:"+this.getAttribute('id')+"/"+index);
                index++;
                if (index===id){
                    console.log(index+"/"+id);
                    $(this).remove();
                    settings.data.splice(index,1);
                    return false;
                }
            }

        });         
    }
    /* other required functions
    removeRow         : removes one row in data and html
    insertRowBefore   : inserts one row into data and display before an index? a data record? what about sort?
    moveDisplayToData : extracts all data from display and puts it back to data (for all and for one row)
    */
</script>