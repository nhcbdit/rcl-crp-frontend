<!--
---------------------------------------------------------------------------------------------------------
header.jsp
---------------------------------------------------------------------------------------------------------
Copyright RCL Public Co., Ltd. 2009
---------------------------------------------------------------------------------------------------------
Author Thanapong Tienniem 08/10/2018
- Change Log---------------------------------------------------------------------------------------------
## 		DD/MM/YY 		-User- 		-TaskRef-       -ShortDescription
01		04/12/2018		Pichit						Restructure header file
---------------------------------------------------------------------------------------------------------
-->
<%@ taglib prefix="rcl" uri="/WEB-INF/custom.tld"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="userData" value="${sessionScope.userData}" />
<%--
<c:set var="icons" value="${fn:length(param.icons) > 0 ? fn:split(param.icons, ',') : []}" />
<c:set var="labels" value="${fn:length(param.labels) > 0 ? fn:split(param.labels, ',') : []}" />
 --%>

<div id="h0-pageheader" class="rcl-standard-page-header container-fluid">
	<div class="row">
		<div class="col-sm-5">
			<%--
			<label id="h0-title"><c:out value="${param.pageHeader}" /></label>
			 --%>
			<label id="h0-title"></label>
		</div>
		<div id="h0-userData" class="col-sm-7" style="font-size:10px;text-align:right;">
			<c:out value="${userData.description} (${userData.userId}) - ${userData.fscCode} - ${userData.line}/${userData.trade}/${userData.agent}" />
		</div>
	</div>
</div>

<div id="h1-pageheader" class="rcl-standard-page-header-2">
	<%--
	<c:if test="${fn:length(icons) > 0}">
		<c:forEach begin="0" end="${fn:length(icons)-1}" var="i">
			<c:set var="id" value="${fn:toLowerCase(labels[i])}" />
			<button id="h1-${id}" type="button" class="rcl-standard-button-header" onclick="${id}()">
				<i class="${icons[i]}"></i> <c:out value="${labels[i]}" />
			</button>
		</c:forEach>
	</c:if>
	 --%>

	<button id="h1-back" type="button" class="rcl-standard-button-header" onclick="window.history.back();" style="display:none;">
		<i class="fas fa-arrow-left"></i> Back
	</button>
	<button id="h1-confirm" type="button" class="rcl-standard-button-header" onclick="confirm()" style="display:none;">
		<i class="fas fa-check"></i> Confirm
	</button>
	<button id="h1-save" type="button" class="rcl-standard-button-header" onclick="save()" style="display:none;">
		<i class="far fa-save"></i> Save
	</button>
	<button id="h1-help" type="button" class="rcl-standard-button-header"  onclick="help()">
		<i class="far fa-question-circle"></i> Help
	</button>
	<button id="h1-tool" type="button" class="rcl-standard-button-header"  onclick="rutToolMenue()">
		<i class="fas fa-toolbox"></i> Tool
	</button>
	<button id="h1-close" type="button" class="rcl-standard-button-header" onclick="close()">
		<i class="far fa-times-circle" style="color: red"></i> Close
	</button>
</div>

<div class="rcl-standard-version-header">
	<label id="h2-versionheader" style="margin-bottom: 0px;">V 1.00 </label>
</div>

<div style="display:none;">
	<rcl:area id="h3-hidden">
		<input type="text" id="h3-userToken" class="dtlField" value="${userData.userToken}" />
		<input type="text" id="h3-userId" class="dtlField" value="${userData.userId}" />
		<input type="text" id="h3-line" class="dtlField" value="${userData.line}" />
		<input type="text" id="h3-trade" class="dtlField" value="${userData.trade}" />
		<input type="text" id="h3-agent" class="dtlField" value="${userData.agent}" />
		<input type="text" id="h3-fscCode" class="dtlField" value="${userData.fscCode}" />
		<input type="text" id="h3-country" class="dtlField" value="${userData.country}" />
	</rcl:area>
</div>

<script type="text/javascript">
var mapSize = 6;
var buttonNameIndexMap = {
		back: 0,
		confirm: 1,
		save: 2,
		help: 3,
		tool: 4,
		close: 5
};

function getUserDataFromHeader(){
	return rptGetDataFromSingleArea("h3-hidden");
}

function getCurrentServerDate(useDBFormat){
	if(useDBFormat === undefined){
		useDBFormat = false;
	}
	var date = new Date();

	if(useDBFormat){
		return "" + date.getUTCFullYear() + (date.getUTCMonth()+1) + date.getDate();
	}else{
		return date.getUTCFullYear() + "-" + (date.getUTCMonth()+1) + "-" + date.getDate();
	}
}

function getCurrentServerTime(useDBFormat){
	if(useDBFormat === undefined){
		useDBFormat = false;
	}
	var date = new Date();
	if(useDBFormat){
		return "" + date.getUTCHours() + date.getUTCMinutes();
	}else{
		return "" + date.getUTCHours() + ":" + date.getUTCMinutes();
	}
}

function setHeaderTitle(label){
	$("#h0-title").html(label);
}

function setHeaderOptions(visible, options){
	var listButtonShow = [];
	$("#h1-pageheader button").each(function(index) {
		listButtonShow.push(this.style.display);
	});
	for(var i=1; i<arguments.length; i++){
		//alert("arguments["+i+"] :"+arguments[i]);
		listButtonShow[buttonNameIndexMap[arguments[i]]] = visible ? "" : "none";
	}
	$("#h1-pageheader button").each(function(index) {
		this.style.display = listButtonShow[index];
	});
}
</script>