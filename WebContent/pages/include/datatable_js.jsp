<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script> -->


<link rel="stylesheet" href="${context}/js/libs/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="${context}/js/libs/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="${context}/js/libs/jquery.dataTables.min.css">
<link rel="stylesheet" href="${context}/js/libs/fixedHeader.dataTables.min.css">

<script type="text/javascript" src="${context}/js/libs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${context}/js/libs/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="${context}/js/libs/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="${context}/js/libs/jdataTables.responsive.min.js"></script>
